package com.alfarabi.base;

import android.content.Context;
import android.content.res.Configuration;
import android.support.multidex.MultiDexApplication;
import android.support.v7.app.AppCompatDelegate;


import com.alfarabi.alfalibs.AlfaLibsApplication;
import com.alfarabi.alfalibs.tools.ArrayCalendarSpinner;
import com.alfarabi.alfalibs.tools.UISimulation;
import com.alfarabi.base.model.Payment;
import com.alfarabi.base.realm.AppRealmModule;
import com.alfarabi.base.realm.RealmDao;
import com.hendraanggrian.bundler.Bundler;
import com.hendraanggrian.preferencer.Preferencer;
import com.hendraanggrian.preferencer.Saver;
import com.hendraanggrian.preferencer.annotations.BindPreferenceRes;

import net.soroushjavdan.customfontwidgets.FontUtils;

import java.util.Locale;

import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import pocketknife.PocketKnife;

/**
 * Created by Alfarabi on 6/15/17.
 */

public abstract class BaseApplication extends MultiDexApplication {

    public static String baseDomain = "";

    /**
     * {@link //BuildConfig#DEBUG} is always false on library module.
     * Therefore, we point debugging mode to this variable.
     */

    static {
        System.loadLibrary("batman");
    }

    public static boolean DEBUG;
    public static String VERSION_NAME;
    public static boolean DELETE_REALM_IF_NEEDED = true ;
    public static boolean MIGRATION = false ;


    protected abstract boolean getBuildConfigDebug();
    protected abstract String getBuildConfigVersionName();

    public static Context context ;

    @BindPreferenceRes(R2.string.appearance_dark) boolean appearanceDark;

    @BindPreferenceRes(R2.string.product_list_column) int productListColumn ;
    @BindPreferenceRes(R2.string.store_profile_list_column) int storeProfileListColumn ;
    protected Saver saver ;


    @Override
    public void onCreate() {
        super.onCreate();
        UISimulation.mode = UISimulation.DEV_REALDATA ;
        AlfaLibsApplication.DEBUG = true;
        DEBUG = getBuildConfigDebug();
        VERSION_NAME = getBuildConfigVersionName();
        ButterKnife.setDebug(DEBUG);
        Preferencer.setDebug(DEBUG);
        Bundler.setDebug(DEBUG);
//        Dispatcher.setDebug(DEBUG);
        PocketKnife.setDebug(DEBUG);

        saver = Preferencer.bind(this);
//        FontUtils.createFonts(this, "fonts");
        Realm.init(this);
        RealmConfiguration.Builder realmConfig = new RealmConfiguration.Builder()
                .name(getApplicationContext().getPackageName())
                .modules(new AppRealmModule());
        realmConfig.initialData(realm -> {
            initiateData(realm);
        });

        if(DELETE_REALM_IF_NEEDED){
            realmConfig.deleteRealmIfMigrationNeeded();
        }
        if(MIGRATION){
            realmConfig.migration((realm, oldVersion, newVersion) -> {
//                if(realm.where(Payment.class.getName()).findAll()==null){
//                    realm.createObject(Payment.class.getName(), RealmDao.nextId(realm, Payment.class));
//                }
            });
        }
        Realm.setDefaultConfiguration(realmConfig.build());
        BaseApplication.context = this ;
        Saver saver = Preferencer.bind(this);
        if(productListColumn==0){
            productListColumn = 2 ;
            saver.saveAll();
        }
        if(storeProfileListColumn==0){
            storeProfileListColumn = 2 ;
            saver.saveAll();
        }

//        appearanceDark = false ;
//        saver.saveAll();
//        AppCompatDelegate.setDefaultNightMode(appearanceDark ? AppCompatDelegate.MODE_NIGHT_YES : AppCompatDelegate.MODE_NIGHT_NO);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        FontUtils.createFonts(this, "fonts");
        FontUtils.setDefaultFont(this, "fonts", getString(R.string.roboto_regular));
        ArrayCalendarSpinner.init();
//        FontUtils.setDefaultFont(this, "fonts", "micross");
//        Toro.init(this);
//        try {
//            FFmpeg.getInstance(this).loadBinary(new LoadBinaryResponseHandler() {
//                @Override
//                public void onStart() {
//                    if (DEBUG)
//                        Log.d("FFmpeg", "onStart()");
//                }
//
//                @Override
//                public void onFailure() {
//                    if (DEBUG)
//                        Log.d("FFmpeg", "onFailure()");
//                }
//
//                @Override
//                public void onSuccess() {
//                    if (DEBUG)
//                        Log.d("FFmpeg", "onSuccess()");
//                }
//
//                @Override
//                public void onFinish() {
//                    if (DEBUG)
//                        Log.d("FFmpeg", "onFinish()");
//                }
//            });
//        } catch (FFmpegNotSupportedException e) {
//            if (DEBUG)
//                e.printStackTrace();
//        }
    }
    void initiateData(Realm realm){
//        if(realm.where(Payment.class).findAll()==null || realm.where(Payment.class).findAll().size()==0){
//            Payment payment = realm.createObject(Payment.class, RealmDao.nextId(realm, Payment.class));
//            payment.setGatewayName(getString(R.string.kredit_plus));
//        }
    }



    public static native String batman();
    public static native String robin();
    public static native String gatotkaca();
    public static native String bimasakti();
}
