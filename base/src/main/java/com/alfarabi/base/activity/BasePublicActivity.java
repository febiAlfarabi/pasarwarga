package com.alfarabi.base.activity;

/**
 * Created by Alfarabi on 6/15/17.
 */

public abstract class BasePublicActivity extends SimpleBaseActivityInitiator {

    public static final int REGISTER = 50;
    public static final int FORGET_PASSWORD = 51;

    @Override
    public String getTAG() {
        return TAG = getClass().getName();
    }
}
