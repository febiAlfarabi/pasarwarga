package com.alfarabi.base.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.base.model.Landing;
import com.alfarabi.base.model.MyProfile;
import com.alfarabi.base.realm.RealmDao;
import com.alfarabi.base.tools.Initial;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 6/15/17.
 */

public abstract class BaseUserActivity extends SimpleBaseActivityInitiator {

    public static final int REQUEST_LOGIN = 100;

    @Getter@Setter public Landing landing ;
    @Getter@Setter protected MyProfile myProfile ;

    @Override
    public String getTAG() {
        return TAG = getClass().getName();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initData();
    }

    public void initData(){
        landing = RealmDao.instance().get(Landing.class);
        landing = RealmDao.unmanage(landing);
        myProfile = RealmDao.instance().rowExist(MyProfile.class)?RealmDao.instance().get(MyProfile.class):null;

        if(myProfile!=null){
            HttpInstance.putHeaderMap(Initial.CONTENT_TYPE, "application/json");
            HttpInstance.putHeaderMap(Initial.TOKEN, myProfile.getUser().getToken());
        }
    }


}
