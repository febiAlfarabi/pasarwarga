package com.alfarabi.base.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.alfalibs.tools.WLog;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.BaseApplication;
import com.alfarabi.base.R;
import com.alfarabi.base.R2;
import com.alfarabi.base.tools.LanguageTools;
import com.github.florent37.viewtooltip.ViewTooltip;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.hendraanggrian.bundler.Bundler;
import com.hendraanggrian.preferencer.Preferencer;
import com.hendraanggrian.preferencer.annotations.BindPreferenceRes;
import com.yinglan.keyboard.HideUtil;

import java.io.EOFException;
import java.net.ConnectException;
import java.net.UnknownHostException;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeoutException;

import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import it.sephiroth.android.library.tooltip.Tooltip;
import lombok.Getter;
import lombok.Setter;
import retrofit2.HttpException;

/**
 * Created by Alfarabi on 6/15/17.
 */

public abstract class SimpleBaseActivityInitiator extends SimpleBaseActivity{// implements KeyboardHeightObserver


//    private KeyboardHeightProvider keyboardHeightProvider;
//    @BindPreferenceRes(R2.string.language) protected String language;

    @Getter@Setter protected int[] slideAnims = {R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right};

    public abstract CoordinatorLayout getCoordinatorLayout();

    protected FirebaseAnalytics firebaseAnalytics;

    @Override
    public String getTAG() {
        return TAG = getClass().getName();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguageTools.initLanguage(this);
        setContentView(contentXmlLayout());
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        ButterKnife.bind(this);
        Preferencer.bind(this);
        Bundler.bind(this);

        HideUtil.init(this);
//        hideSystemUI();
//        autoHide(10);
//        keyboardHeightProvider = new KeyboardHeightProvider(this);
//        getWindow().getDecorView().post(new Runnable() {
//            public void run() {
//                keyboardHeightProvider.start();
//            }
//        });
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

//    @Override
//    public void onKeyboardHeightChanged(int height, int orientation) {
//        String or = orientation == Configuration.ORIENTATION_PORTRAIT ? "portrait" : "landscape";
//        WLog.i(TAG, "onKeyboardHeightChanged in pixels: " + height + " " + or);
//    }


    private void hideSystemUI() {
        // Set the IMMERSIVE flag.
        // Set the content to appear under the system bars so that the content
        // doesn't resize when the system bars hide and show.
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);
    }
    private void showSystemUI() {
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    void autoHide(int timeMilis){
        getWindow().getDecorView().postDelayed(() -> {
            hideSystemUI();
            autoHide(timeMilis);
        }, timeMilis);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        WLog.i(TAG, "On Configuration Change");
//        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
//            Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
//        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
//            Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
//        }
    }

    public int[] getDimension(){
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        return new int[]{width, height};
    }

    protected Snackbar snackbar ;
    public void showSnackbar(String message, View.OnClickListener onClickListener){
        if(snackbar!=null && snackbar.isShown()){
            snackbar.dismiss();
            snackbar = null ;
        }
        if(getCoordinatorLayout()!=null){
            snackbar = Snackbar
                    .make(getCoordinatorLayout(), Html.fromHtml("<font color=\"#ffffff\">"+message+"</font>"), Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).setDuration(5000)
                    .setAction(getString(R.string.close), v -> {
                        snackbar.dismiss();
                        onClickListener.onClick(v);
                    });
            snackbar.show();
        }else{
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    }

    public void showTooltip(View view, String message, Tooltip.Gravity gravity){
//        Tooltip.TooltipView tooltipView = Tooltip.make(this, new Tooltip.Builder(101).anchor(view, gravity)
//                .closePolicy(new Tooltip.ClosePolicy().insidePolicy(true, false).outsidePolicy(true, false), 3000)
//                .activateDelay(800).showDelay(300).text(message).withStyleId(R.style.AppTheme_TooltipLayout)
//                .maxWidth(500).withArrow(true).withOverlay(true).floatingAnimation(Tooltip.AnimationBuilder.DEFAULT)
//                .build());
//        tooltipView.show();
        ViewTooltip.on(view).autoHide(true, 3000)
                .corner(30).textColor(view.getResources().getColor(R.color.white)).color(view.getResources().getColor(R.color.red_700)).position(ViewTooltip.Position.BOTTOM)
                .text(message)
                .show();
    }

    public void showSnackbar(Throwable throwable, View.OnClickListener onClickListener){
        String message = "" ;
        throwable.printStackTrace();
        if(throwable instanceof EOFException){
            message = getString(R.string.server_error);
        }else if(throwable instanceof UnknownHostException){
            message = getString(R.string.connection_timeout);
        }else if(throwable instanceof HttpException){
            if(throwable.getMessage().trim().contains("HTTP 503")){
                PackageManager pm = getPackageManager();
                Intent intent = new Intent(this, UnderMaintenanceActivity.class);
                intent.putExtra("error_code", String.valueOf("503"));
                getWindow().getDecorView().postDelayed(() -> {
                    finish();
                    startActivity(intent);
                }, 2000);
                message = throwable.getMessage();
            }else{
                message = getString(R.string.connection_timeout);
            }
        }else if(throwable instanceof TimeoutException){
            message = getString(R.string.connection_timeout);
        }else if(throwable instanceof ConnectException){
            message = getString(R.string.connection_timeout);
        }else if(throwable instanceof EOFException){
            message = getString(R.string.connection_timeout);
        }else if(throwable instanceof java.net.SocketTimeoutException){
            message = getString(R.string.connection_timeout);
        }else if(throwable instanceof java.net.SocketException){
            message = getString(R.string.connection_timeout);
        } else{
            message = throwable.getLocalizedMessage();
        }
        showSnackbar(message, onClickListener);
    }

    protected MaterialDialog materialDialog ;
    public void showDialog(String message, MaterialDialog.SingleButtonCallback singleButtonCallback){
        if(materialDialog!=null && materialDialog.isShowing()){
            materialDialog.dismiss();
            materialDialog = null ;
        }
        materialDialog = new MaterialDialog.Builder(this).title(R.string.alert).backgroundColorRes(R.color.white)
                .content(message).contentColor(Color.BLACK).titleColor(Color.BLACK).positiveColor(Color.BLUE)
                .positiveText(R.string.ok).theme(Theme.LIGHT).backgroundColor(Color.WHITE)
                .onPositive(singleButtonCallback)
                .show();
    }
    public void showDialog(String title, String message, MaterialDialog.SingleButtonCallback singleButtonCallback){
        if(materialDialog!=null && materialDialog.isShowing()){
            materialDialog.dismiss();
            materialDialog = null ;
        }
        materialDialog = new MaterialDialog.Builder(this).title(title).backgroundColorRes(R.color.white)
                .content(message).contentColor(Color.BLACK).titleColor(Color.BLACK).positiveColor(Color.BLUE)
                .positiveText(R.string.ok).theme(Theme.LIGHT).backgroundColor(Color.WHITE)
                .onPositive(singleButtonCallback)
                .show();
    }


    public void showDialog(String message, MaterialDialog.SingleButtonCallback okButtonCallback,MaterialDialog.SingleButtonCallback cancelButtonCallback) {
        if(materialDialog!=null && materialDialog.isShowing()){
            materialDialog.dismiss();
            materialDialog = null ;
        }
        materialDialog = new MaterialDialog.Builder(this).title(R.string.alert).backgroundColorRes(R.color.white)
                .content(message).contentColor(Color.BLACK).titleColor(Color.BLACK).positiveColor(Color.BLUE)
                .positiveText(R.string.ok).negativeText(R.string.cancel).theme(Theme.LIGHT).backgroundColor(Color.WHITE)
                .onPositive(okButtonCallback).onNegative(cancelButtonCallback)
                .show();
    }

    public void showDialog(String title, String message, MaterialDialog.SingleButtonCallback okButtonCallback,MaterialDialog.SingleButtonCallback cancelButtonCallback) {
        if(materialDialog!=null && materialDialog.isShowing()){
            materialDialog.dismiss();
            materialDialog = null ;
        }
        materialDialog = new MaterialDialog.Builder(this).title(title).backgroundColorRes(R.color.white)
                .content(message).contentColor(Color.BLACK).titleColor(Color.BLACK).positiveColor(Color.BLUE)
                .positiveText(R.string.ok).negativeText(R.string.cancel).theme(Theme.LIGHT).backgroundColor(Color.WHITE)
                .onPositive(okButtonCallback).onNegative(cancelButtonCallback)
                .show();
    }

    public void showDialog(Throwable throwable, MaterialDialog.SingleButtonCallback singleButtonCallback){
        String message = "" ;
        throwable.printStackTrace();
        if(throwable instanceof EOFException){
            message = getString(R.string.server_error);
        }else if(throwable instanceof UnknownHostException){
            message = getString(R.string.connection_timeout);
        }else if(throwable instanceof HttpException){
            if(throwable.getMessage().trim().contains("HTTP 503")){
                PackageManager pm = getPackageManager();
                Intent intent = new Intent(this, UnderMaintenanceActivity.class);
                intent.putExtra("error_code", String.valueOf("503"));
                getWindow().getDecorView().postDelayed(() -> {
                    finish();
                    startActivity(intent);
                }, 2000);
                message = throwable.getMessage();
            }else{
                message = getString(R.string.connection_timeout);
            }
        }else if(throwable instanceof TimeoutException){
            message = getString(R.string.connection_timeout);
        }else if(throwable instanceof ConnectException){
            message = getString(R.string.connection_timeout);
        }else if(throwable instanceof EOFException){
            message = getString(R.string.connection_timeout);
        }else if(throwable instanceof java.net.SocketTimeoutException){
            message = getString(R.string.connection_timeout);
        }else if(throwable instanceof java.net.SocketException){
            message = getString(R.string.connection_timeout);
        } else{
            message = throwable.getLocalizedMessage();
        }
        showDialog(message, singleButtonCallback);
    }

    public void showDialog(Throwable throwable, MaterialDialog.SingleButtonCallback okButtonCallback, MaterialDialog.SingleButtonCallback cancelButtonCallback){
        String message = "" ;
        throwable.printStackTrace();
        if(throwable instanceof EOFException){
            message = getString(R.string.server_error);
        }else if(throwable instanceof UnknownHostException){
            message = getString(R.string.connection_timeout);
        }else if(throwable instanceof HttpException){
            if(throwable.getMessage().trim().contains("HTTP 503")){
                PackageManager pm = getPackageManager();
                Intent intent = new Intent(this, UnderMaintenanceActivity.class);
                intent.putExtra("error_code", String.valueOf("503"));
                getWindow().getDecorView().postDelayed(() -> {
                    finish();
                    startActivity(intent);
                }, 2000);
                message = throwable.getMessage();
            }else{
                message = getString(R.string.connection_timeout);
            }
        }else if(throwable instanceof TimeoutException){
            message = getString(R.string.connection_timeout);
        }else if(throwable instanceof ConnectException){
            message = getString(R.string.connection_timeout);
        }else if(throwable instanceof EOFException){
            message = getString(R.string.connection_timeout);
        }else if(throwable instanceof java.net.SocketTimeoutException){
            message = getString(R.string.connection_timeout);
        }else if(throwable instanceof java.net.SocketException){
            message = getString(R.string.connection_timeout);
        } else{
            message = throwable.getLocalizedMessage();
        }
        showDialog(message, okButtonCallback, cancelButtonCallback);
    }

//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        Dispatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);
//    }


    @Override
    public void onBackPressed() {
//        freeMemory();
        super.onBackPressed();
    }

    public void freeMemory(){
        GlideApp.get(this).clearMemory();
//        AsyncTask.execute(() -> {
//            GlideApp.get(this).clearDiskCache();
//            WLog.d(TAG, "### FREE UP MEMORY ###");
//        });
        System.runFinalization();
        System.gc();
    }

    public <O> Disposable asObservable(Observable<O> observable){
        return observable.subscribeOn(Schedulers.newThread()).subscribe();
    }
    public <O> Disposable runAsObservable(Callable callable){
        return Observable.fromCallable(callable).subscribeOn(Schedulers.newThread()).subscribe();
    }
}
