package com.alfarabi.base.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.widget.TextView;

import com.alfarabi.alfalibs.tools.AssetUtils;
import com.alfarabi.base.R;
import com.alfarabi.base.R2;
import com.alfarabi.base.activity.BasePublicActivity;
import com.alfarabi.base.tools.UlTagHandler;
import com.hendraanggrian.bundler.annotations.BindExtra;

import net.soroushjavdan.customfontwidgets.CButton;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.nio.charset.Charset;

import butterknife.BindView;

public class UnderMaintenanceActivity extends BasePublicActivity {

    @BindView(R2.id.title_2_tv) TextView title2TextView ;
    @BindView(R2.id.html_textview) HtmlTextView htmlTextView ;
    @BindView(R2.id.button) CButton button ;

    @Nullable@pocketknife.BindExtra("error_code") String error ;

    @Override
    public CoordinatorLayout getCoordinatorLayout() {
        return null;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.activity_under_maintenance;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(error==null){
            error = "404" ; // Default
        }
        title2TextView.setText(String.format(getString(R.string.application_is_under_maintenance_2), error));
        htmlTextView.setHtml(AssetUtils.readHtmlFile(this, "maintenance_notice.html"));
        button.setOnClickListener(v -> {
            if(error.equals("503")){
                finish();
                freeMemory();
            }else{
                onBackPressed();
                freeMemory();
            }
        });
    }
}
