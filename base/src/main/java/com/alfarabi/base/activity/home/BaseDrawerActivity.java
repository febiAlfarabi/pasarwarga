package com.alfarabi.base.activity.home;

import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;

import com.appeaser.sublimenavigationviewlibrary.OnNavigationMenuEventListener;
import com.appeaser.sublimenavigationviewlibrary.SublimeBaseMenuItem;
import com.appeaser.sublimenavigationviewlibrary.SublimeNavigationView;
import com.alfarabi.base.activity.BaseUserActivity;

import lombok.Getter;
import lombok.Setter;

public abstract class BaseDrawerActivity extends BaseUserActivity implements OnNavigationMenuEventListener {


    public abstract DrawerLayout getDrawerLayout();
    public abstract SublimeNavigationView getSublimeNavigationView();
    protected @Getter@Setter SublimeBaseMenuItem currentMenuItem ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public boolean onNavigationMenuEvent(Event event, SublimeBaseMenuItem menuItem) {
        return false;
    }
}
