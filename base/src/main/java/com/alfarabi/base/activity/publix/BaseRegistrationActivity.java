package com.alfarabi.base.activity.publix;

import android.os.Bundle;

import com.alfarabi.base.activity.BasePublicActivity;

public abstract class BaseRegistrationActivity extends BasePublicActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
