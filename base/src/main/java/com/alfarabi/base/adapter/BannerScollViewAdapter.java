package com.alfarabi.base.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.alfalibs.tools.WLog;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.R;
import com.alfarabi.base.model.Banner;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.hendraanggrian.bundler.Bundler;
import com.squareup.picasso.Picasso;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by Alfarabi on 7/19/17.
 */

public class BannerScollViewAdapter extends RecyclerView.Adapter<BannerScollViewAdapter.ViewHolder> {

    public static final String TAG = BannerScollViewAdapter.class.getName();
    
    private List<Banner> banners = new ArrayList<>();
    private FragmentActivity context ;
    int layout = R.layout.fragment_banner;
    int imageDefault = R.drawable.banner_pasarwarga;

    public BannerScollViewAdapter(FragmentActivity context, List<Banner> banners, int layout, int imageDefault) {
        this.banners = banners;
        this.context = context ;
        this.layout = layout ;
        this.imageDefault = imageDefault ;
    }

    public BannerScollViewAdapter(FragmentActivity context, List<Banner> banners) {
        this.banners = banners;
        this.context = context ;
    }
    public BannerScollViewAdapter(FragmentActivity context, Banner Banner) {
        this.banners.add(Banner);
        this.context = context ;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(layout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        GlideApp.with(context).load(banners.get(position).getImageUrl()).error(imageDefault).placeholder(imageDefault)
                .skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return banners!=null?banners.size():0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.bannerview);
        }
    }
}
