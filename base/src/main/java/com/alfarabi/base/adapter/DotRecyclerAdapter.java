package com.alfarabi.base.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.alfarabi.base.R;
import com.alfarabi.base.adapter.viewholder.DotViewHolder;

import lombok.Getter;

/**
 * Created by alfarabi on 7/30/17.
 */

public class DotRecyclerAdapter extends RecyclerView.Adapter<DotViewHolder> {

    Activity activity ;
    Fragment fragment ;
    private Context context ;
    @Getter int focus ;
    private int size ;

    public DotRecyclerAdapter(Fragment fragment, int size, int focus){
        this.fragment = fragment ;
        this.context = fragment.getActivity();
        this.focus = focus;
        this.size = size ;
    }

    public DotRecyclerAdapter(Activity activity, int size, int focus){
        this.activity = activity ;
        this.context = activity;
        this.focus = focus;
        this.size = size ;
    }

    @Override
    public DotViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DotViewHolder(LayoutInflater.from(context).inflate(R.layout.fragment_dot, parent, false));
    }

    @Override
    public void onBindViewHolder(DotViewHolder holder, int position) {
        holder.focus(focus);
    }

    @Override
    public int getItemCount() {
        return size;
    }

    public void setFocus(int focus) {
        this.focus = focus;

        notifyItemRangeChanged(0, size);
    }
}
