package com.alfarabi.base.adapter;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.base.R;
import com.alfarabi.base.model.Banner;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alfarabi on 7/19/17.
 */

public class IntroductionScollViewAdapter extends RecyclerView.Adapter<IntroductionScollViewAdapter.ViewHolder> {

    public static final String TAG = IntroductionScollViewAdapter.class.getName();

    private int[] drawables = new int[]{};
    private FragmentActivity context ;

    public IntroductionScollViewAdapter(FragmentActivity context, int[] drawables) {
        this.drawables = drawables;
        this.context = context ;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.fragment_banner, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
//        GlideApp.with(context).load(banners.get(position).getImageUrl()).into(holder.imageView);
//        GlideApp.with(context).load(context.getResources().getDrawable(drawables[position])).into(holder.imageView);
        holder.imageView.setImageDrawable(context.getResources().getDrawable(drawables[position]));
    }

    @Override
    public int getItemCount() {
        return drawables.length;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.bannerview);
        }
    }
}
