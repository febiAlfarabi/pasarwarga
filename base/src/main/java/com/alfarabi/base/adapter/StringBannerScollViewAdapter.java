package com.alfarabi.base.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.base.R;
import com.alfarabi.base.fragment.FullScreenImageFragment;
import com.alfarabi.base.model.Banner;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.squareup.picasso.Picasso;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 7/19/17.
 */

public class StringBannerScollViewAdapter extends RecyclerView.Adapter<StringBannerScollViewAdapter.ViewHolder> {

    @Getter@Setter String[] bannerUrls ;
    private Context context ;
    private Fragment fragment ;
    private Activity activity ;
    int layout = R.layout.fragment_banner;
    int imageDefault = R.drawable.banner_pasarwarga;


    public StringBannerScollViewAdapter(Context context, String... bannerUrls) {
        this.bannerUrls = bannerUrls ;
        this.context = context ;
    }
    public StringBannerScollViewAdapter(Fragment fragment, String... bannerUrls) {
        this.bannerUrls = bannerUrls ;
        this.fragment = fragment ;
        this.context = fragment.getActivity();
    }

    public StringBannerScollViewAdapter(Activity activity, int layout, int imageDefault, String... bannerUrls) {
        this.bannerUrls = bannerUrls ;
        this.activity = activity ;
        this.context = activity;
        this.layout = layout ;
        this.imageDefault = imageDefault ;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(layout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        GlideApp.with(context).load(bannerUrls[position]).error(imageDefault).placeholder(imageDefault)
                .skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.imageView);
        holder.imageView.setOnClickListener(v -> {
            if(activity!=null || fragment!=null){
                if(this.context instanceof AppCompatActivity) {
                    Bitmap bitmap = holder.imageView.getDrawingCache();
                    FullScreenImageFragment fragment = FullScreenImageFragment.newInstance(bitmap);
                    fragment.show(((AppCompatActivity)this.context).getSupportFragmentManager(), "FS_TAG");
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return bannerUrls!=null?bannerUrls.length:0;
    }

    public int[] getDimension(){
        Display display = null ;

        if(activity!=null){
            display = activity.getWindowManager().getDefaultDisplay();
        }
        if(fragment!=null){
            display = fragment.getActivity().getWindowManager().getDefaultDisplay();
        }
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        return new int[]{width, height};
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.bannerview);
            imageView.setDrawingCacheEnabled(true);

        }
    }
}
