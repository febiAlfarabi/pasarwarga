package com.alfarabi.base.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;


import com.alfarabi.base.R;
import com.alfarabi.base.R2;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by alfarabi on 7/29/17.
 */

public class DotViewHolder extends RecyclerView.ViewHolder {

    @BindView(R2.id.dotview)
    ImageView dotview ;

    public DotViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void focus(int focus){
        if(focus==getAdapterPosition()){
            dotview.setImageDrawable(itemView.getResources().getDrawable(R.drawable.dot_shape_white));
        }else{
            dotview.setImageDrawable(itemView.getResources().getDrawable(R.drawable.dot_shape_transparent));

        }
    }

}
