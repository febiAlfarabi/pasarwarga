package com.alfarabi.base.adapter.viewpager;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.Display;

import com.alfarabi.base.fragment.banner.ProductBannerFragment;

/**
 * Created by Alfarabi on 8/28/17.
 */

public class ProductDetailBannerAdapter extends FragmentPagerAdapter {

    private String[] bannerUrls ;
    private Context context ;
    private Fragment fragment ;
    private Activity activity ;

    public ProductDetailBannerAdapter(FragmentManager fm, Context context, String... bannerUrls) {
        super(fm);
        this.bannerUrls = bannerUrls ;
        this.context = context ;
    }

    public ProductDetailBannerAdapter(FragmentManager fm, Fragment fragment, String... bannerUrls) {
        super(fm);
        this.bannerUrls = bannerUrls ;
        this.fragment = fragment ;
        this.context = fragment.getActivity();
    }

    public ProductDetailBannerAdapter(FragmentManager fm, Activity activity, String... bannerUrls) {
        super(fm);
        this.bannerUrls = bannerUrls ;
        this.activity = activity ;
        this.context = activity;
    }



    @Override
    public Fragment getItem(int position) {
        return ProductBannerFragment.instance(bannerUrls[position]);
    }

    @Override
    public int getCount() {
        return bannerUrls!=null? bannerUrls.length:0;
    }

    public int[] getDimension(){
        Display display = null ;

        if(activity!=null){
            display = activity.getWindowManager().getDefaultDisplay();
        }
        if(fragment!=null){
            display = fragment.getActivity().getWindowManager().getDefaultDisplay();
        }
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        return new int[]{width, height};
    }
}
