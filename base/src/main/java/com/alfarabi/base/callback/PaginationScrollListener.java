package com.alfarabi.base.callback;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import lombok.Getter;
import lombok.Setter;

public abstract class PaginationScrollListener extends RecyclerView.OnScrollListener {

    LinearLayoutManager layoutManager;
    @Getter@Setter boolean loading ;
//    private int page;
//
//    public int startPage() {
//        this.page = 1;
//        return this.page;
//    }
//
//    public int restartPage() {
//        return this.startPage();
//    }
//
//    public int currentPage() {
//        return this.page;
//    }
//
//    public int increasePage() {
//        ++this.page;
//        return this.page;
//    }
//
//    public int decreasePage() {
//        --this.page;
//        return this.page;
//    }
//
//    public int zeroPage() {
//        this.page = 0;
//        return this.page;
//    }


    public PaginationScrollListener(LinearLayoutManager layoutManager) {
        this.layoutManager = layoutManager;
    }

    public PaginationScrollListener(GridLayoutManager layoutManager) {
        this.layoutManager = layoutManager;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        int visibleItemCount = layoutManager.getChildCount();
        int totalItemCount = layoutManager.getItemCount();
        int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

        if (!isLoading()) {
            if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition >= 0) {
                loadMoreItems();
            }
        }
    }

    protected abstract void loadMoreItems();


}