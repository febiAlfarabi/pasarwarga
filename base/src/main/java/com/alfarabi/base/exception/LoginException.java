package com.alfarabi.base.exception;

/**
 * Created by Alfarabi on 8/15/17.
 */

public class LoginException extends RuntimeException {


    public LoginException(){
        super("Application need to login before using this feature");
    }
}
