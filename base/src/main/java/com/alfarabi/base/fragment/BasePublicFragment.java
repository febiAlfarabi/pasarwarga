package com.alfarabi.base.fragment;

/**
 * Created by Alfarabi on 6/15/17.
 */

public abstract class BasePublicFragment<O> extends SimpleBaseFragmentInitiator<O> {

    @Override
    public String getTAG() {
        return TAG = getClass().getName();
    }
}
