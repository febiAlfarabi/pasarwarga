package com.alfarabi.base.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.base.R;
import com.alfarabi.base.activity.BaseUserActivity;
import com.alfarabi.base.model.Landing;
import com.alfarabi.base.model.MyProfile;
import com.alfarabi.base.realm.RealmDao;
import com.alfarabi.base.tools.Initial;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 6/15/17.
 */

public abstract class BaseUserFragment<O> extends SimpleBaseFragmentInitiator<O> {

    protected String title ;
    public static final int REQUEST_LOGIN = 100;
    public static final int REQUEST_REGISTER_SHOP = 101;
    @Getter @Setter protected Landing landing ;
    @Getter@Setter protected MyProfile myProfile ;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        landing = RealmDao.instance().get(Landing.class);
        landing = RealmDao.unmanage(landing);
        myProfile = RealmDao.instance().rowExist(MyProfile.class)?RealmDao.instance().get(MyProfile.class):null;
    }

    @Override
    public void onResume() {
        super.onResume();
        landing = RealmDao.instance().get(Landing.class);
        landing = RealmDao.unmanage(landing);
        myProfile = RealmDao.instance().rowExist(MyProfile.class)?RealmDao.instance().get(MyProfile.class):null;
        activity(BaseUserActivity.class).setMyProfile(myProfile);
//        landing = RealmDao.instance().get(Landing.class);
//        landing = RealmDao.unmanage(landing);
//        myProfile = RealmDao.instance().rowExist(MyProfile.class)?RealmDao.instance().get(MyProfile.class):null;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initTitle();
    }

    @Override
    public String getTAG() {
        return TAG = getClass().getName();
    }

    public abstract String initTitle();

}
