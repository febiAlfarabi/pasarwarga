package com.alfarabi.base.fragment;//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//


import android.app.Dialog;
import android.content.ClipData;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;
import android.widget.ImageView;

import com.alfarabi.base.view.interfaze.DismissibleDragShadowBuilder;
import com.alfarabi.base.view.interfaze.DismissibleOnDragListener;
import com.dmallcott.dismissibleimageview.R.id;
import com.dmallcott.dismissibleimageview.R.layout;
import com.dmallcott.dismissibleimageview.R.style;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.squareup.picasso.Picasso.LoadedFrom;

public class FullScreenImageFragment extends DialogFragment {
    private static final String ARGUMENT_BITMAP = "ARGUMENT_BITMAP";
    private static final String ARGUMENT_URL = "ARGUMENT_URL";
    private Bitmap bitmap;
    private ImageView imageView;
    private View topBorderView;
    private View bottomBorderView;
    private View leftBorderView;
    private View rightBorderView;

    public FullScreenImageFragment() {
    }

    public static FullScreenImageFragment newInstance(@NonNull Bitmap bitmap) {
        FullScreenImageFragment fragment = new FullScreenImageFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("ARGUMENT_BITMAP", bitmap);
        fragment.setArguments(bundle);
        return fragment;
    }

    public static FullScreenImageFragment newInstance(@NonNull String url) {
        FullScreenImageFragment fragment = new FullScreenImageFragment();
        Bundle bundle = new Bundle();
        bundle.putString("ARGUMENT_URL", url);
        fragment.setArguments(bundle);
        return fragment;
    }

    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        LayoutInflater layoutInflater = (LayoutInflater)this.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view = layoutInflater.inflate(layout.fragment_full_screen_image, (ViewGroup)null);

        try {
            dialog.getWindow().requestFeature(1);
        } catch (NullPointerException var6) {
            ;
        }

        if(savedInstanceState != null) {
            this.bitmap = (Bitmap)savedInstanceState.getParcelable("ARGUMENT_BITMAP");
            this.initialiseViews(view, this.bitmap);
        } else if(this.getArguments().containsKey("ARGUMENT_BITMAP") && this.getArguments().getParcelable("ARGUMENT_BITMAP") != null) {
            this.bitmap = (Bitmap)this.getArguments().getParcelable("ARGUMENT_BITMAP");
            this.initialiseViews(view, this.bitmap);
        } else if(this.getArguments().containsKey("ARGUMENT_URL") && this.getArguments().getParcelable("ARGUMENT_URL") != null) {
            Picasso.with(this.getContext()).load(this.getArguments().getString("ARGUMENT_URL")).into(new Target() {
                public void onBitmapLoaded(Bitmap bm, LoadedFrom from) {
                    FullScreenImageFragment.this.bitmap = bm;
                    FullScreenImageFragment.this.initialiseViews(view, FullScreenImageFragment.this.bitmap);
                }

                public void onBitmapFailed(Drawable errorDrawable) {
                }

                public void onPrepareLoad(Drawable placeHolderDrawable) {
                }
            });
        }

        dialog.setContentView(view);
        return dialog;
    }

    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        if(this.getDialog() != null) {
            this.getDialog().getWindow().getAttributes().windowAnimations = style.DialogAnimation;
        }

    }

    public void onStart() {
        super.onStart();
        if(this.getDialog() != null) {
            this.getDialog().getWindow().setLayout(-1, -1);
            this.getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));
        }

    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("ARGUMENT_BITMAP", Bitmap.createBitmap(this.bitmap));
    }

    private void initialiseViews(@NonNull final View view, @NonNull Bitmap bitmap) {
        this.imageView = (ImageView)view.findViewById(id.fragment_full_screen_imageView);
        this.topBorderView = view.findViewById(id.fragment_full_screen_top_border);
        this.bottomBorderView = view.findViewById(id.fragment_full_screen_bottom_border);
        this.leftBorderView = view.findViewById(id.fragment_full_screen_left_border);
        this.rightBorderView = view.findViewById(id.fragment_full_screen_right_border);
        this.leftBorderView.setOnDragListener(new DismissibleOnDragListener(new DismissibleOnDragListener.OnDropListener() {
            public void onDragStarted() {
                FullScreenImageFragment.this.imageView.setVisibility(ImageView.INVISIBLE);
            }

            public void onDrop() {
                FullScreenImageFragment.this.dismiss();
            }

            public void onDragEnded() {
                FullScreenImageFragment.this.imageView.setVisibility(ImageView.VISIBLE);
                view.setAlpha(1.0F);
            }

            public void onDragLocation(float x, float y) {
                view.setAlpha(x / (float)FullScreenImageFragment.this.leftBorderView.getWidth());
            }
        }));
        this.rightBorderView.setOnDragListener(new DismissibleOnDragListener(new DismissibleOnDragListener.OnDropListener() {
            public void onDragStarted() {
                FullScreenImageFragment.this.imageView.setVisibility(ImageView.INVISIBLE);
            }

            public void onDrop() {
                FullScreenImageFragment.this.dismiss();
            }

            public void onDragEnded() {
                FullScreenImageFragment.this.imageView.setVisibility(ImageView.VISIBLE);
                view.setAlpha(1.0F);
            }

            public void onDragLocation(float x, float y) {
                view.setAlpha(1.0F - x / (float)FullScreenImageFragment.this.rightBorderView.getWidth());
            }
        }));
        this.topBorderView.setOnDragListener(new DismissibleOnDragListener(new DismissibleOnDragListener.OnDropListener() {
            public void onDragStarted() {
                FullScreenImageFragment.this.imageView.setVisibility(ImageView.INVISIBLE);
            }

            public void onDrop() {
                FullScreenImageFragment.this.dismiss();
            }

            public void onDragEnded() {
                FullScreenImageFragment.this.imageView.setVisibility(ImageView.VISIBLE);
                view.setAlpha(1.0F);
            }

            public void onDragLocation(float x, float y) {
                view.setAlpha(y / (float)FullScreenImageFragment.this.topBorderView.getHeight());
            }
        }));
        this.bottomBorderView.setOnDragListener(new DismissibleOnDragListener(new DismissibleOnDragListener.OnDropListener() {
            public void onDragStarted() {
                FullScreenImageFragment.this.imageView.setVisibility(ImageView.INVISIBLE);
            }

            public void onDrop() {
                FullScreenImageFragment.this.dismiss();
            }

            public void onDragEnded() {
                FullScreenImageFragment.this.imageView.setVisibility(ImageView.VISIBLE);
                view.setAlpha(1.0F);
            }

            public void onDragLocation(float x, float y) {
                view.setAlpha(1.0F - y / (float)FullScreenImageFragment.this.topBorderView.getHeight());
            }
        }));
        this.imageView.setAdjustViewBounds(true);
        this.imageView.setImageBitmap(bitmap);
        this.imageView.setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case 0:
                        Point offset = new Point((int)event.getX(), (int)event.getY());
                        DismissibleDragShadowBuilder shadowBuilder = new DismissibleDragShadowBuilder(FullScreenImageFragment.this.imageView, offset);
                        FullScreenImageFragment.this.imageView.startDrag((ClipData)null, shadowBuilder, FullScreenImageFragment.this.imageView, 0);
                        return true;
                    default:
                        return false;
                }
            }
        });
    }
}
