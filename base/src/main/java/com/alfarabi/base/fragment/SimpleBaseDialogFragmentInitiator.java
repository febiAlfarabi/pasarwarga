package com.alfarabi.base.fragment;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;

import com.alfarabi.alfalibs.fragments.SimpleBaseDialogFragment;
import com.alfarabi.alfalibs.fragments.SimpleBaseFragment;
import com.alfarabi.alfalibs.fragments.interfaze.SimpleFragmentCallback;
import com.alfarabi.base.R;
import com.alfarabi.base.activity.SimpleBaseActivityInitiator;
import com.alfarabi.base.tools.LanguageTools;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.hendraanggrian.bundler.Bundler;
import com.hendraanggrian.preferencer.Preferencer;
import com.hendraanggrian.preferencer.Saver;

import java.lang.reflect.InvocationTargetException;

import butterknife.ButterKnife;
import lombok.Getter;
import lombok.Setter;
import pocketknife.PocketKnife;

/**
 * Created by Alfarabi on 6/15/17.
 */

public abstract class SimpleBaseDialogFragmentInitiator<O> extends SimpleBaseDialogFragment implements SimpleFragmentCallback<O>{

    @Getter@Setter O object ;

//    static Activity activity ;
    protected Saver saver ;
    protected ShowcaseView.Builder showcaseViewBuilder ;


    public static <T extends SimpleBaseDialogFragmentInitiator, O> T instance(Class<T> clazz, O object, int styling, int theming){
        try {
            T t = clazz.getConstructor().newInstance();
            t.setObject(object);
            t.setStyling(styling);
            t.setTheming(theming);
            return t ;
        } catch (java.lang.InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return null ;
    }

    @Override
    public String getTAG() {
        return TAG = getClass().getName();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        final Context contextThemeWrapper = new ContextThemeWrapper(getActivity(), R.style.AppTheme);
//        LayoutInflater localInflater = inflater.cloneInContext(contextThemeWrapper);
//        return localInflater.inflate(contentXmlLayout(), container, false);
        return inflater.inflate(contentXmlLayout(), container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        SimpleBaseFragmentInitiator.activity = getActivity();
        if(view.getBackground()==null){
            view.setBackgroundColor(getResources().getColor(R.color.grey_100));
        }
        ButterKnife.bind(this, view);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguageTools.initLanguage(getActivity());
        if(getArguments()!=null){
            PocketKnife.bindArguments(this);
        }
        Bundler.bind(this);
        saver = Preferencer.bind(this);

    }

    public int[] getDimension(){
        return ((SimpleBaseActivityInitiator)getActivity()).getDimension();
    }


    public SearchView injectSearchView(Menu menu, SearchView.OnQueryTextListener onQueryTextListener){
        SearchManager searchManager = (SearchManager) getContext().getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search_menu).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setOnQueryTextListener(onQueryTextListener);
        return searchView ;
    }

    public Activity activity(){
        return getActivity();
    }
    public <T extends SimpleBaseActivityInitiator>  T activity(Class<T> tClass){
        return (T) activity();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        System.gc();
    }
}
