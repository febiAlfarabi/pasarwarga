package com.alfarabi.base.fragment;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;

import com.alfarabi.alfalibs.fragments.SimpleBaseFragment;
import com.alfarabi.alfalibs.fragments.interfaze.SimpleFragmentCallback;
import com.alfarabi.alfalibs.tools.Caster;
import com.alfarabi.alfalibs.tools.WLog;
import com.alfarabi.base.BaseApplication;
import com.alfarabi.base.R;
import com.alfarabi.base.activity.SimpleBaseActivity;
import com.alfarabi.base.activity.SimpleBaseActivityInitiator;
import com.alfarabi.base.activity.home.BaseDrawerActivity;
import com.alfarabi.base.tools.LanguageTools;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.google.gson.Gson;
import com.hendraanggrian.bundler.Bundler;
import com.hendraanggrian.preferencer.Preferencer;
import com.hendraanggrian.preferencer.Saver;

import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.Set;

import butterknife.ButterKnife;
import io.reactivex.disposables.Disposable;
import lombok.Getter;
import lombok.Setter;
import pocketknife.PocketKnife;

/**
 * Created by Alfarabi on 6/15/17.
 */

public abstract class SimpleBaseFragmentInitiator<O> extends SimpleBaseFragment implements SimpleFragmentCallback<O>{

    @Getter@Setter O object ;

//    static Activity activity ;
    protected Saver saver ;
    protected ShowcaseView.Builder showcaseViewBuilder ;
    @Getter@Setter protected int[] slideAnims = {R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right};
    @Getter@Setter protected int[] appearAnims = {R.anim.view_flipper_transition_in_left, R.anim.view_flipper_transition_out_right, R.anim.view_flipper_transition_in_right, R.anim.view_flipper_transition_out_left};




    public static <T extends SimpleBaseFragmentInitiator, O> T instance(Class<T> clazz, O object){
        try {
            T t = clazz.getConstructor().newInstance();
            t.setObject(object);
            return t ;
        } catch (java.lang.InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return null ;
    }

    @Override
    public String getTAG() {
        return TAG = getClass().getName();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        final Context contextThemeWrapper = new ContextThemeWrapper(getActivity(), R.style.AppTheme);
//        LayoutInflater localInflater = inflater.cloneInContext(contextThemeWrapper);
//        return localInflater.inflate(contentXmlLayout(), container, false);
        return inflater.inflate(contentXmlLayout(), container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        SimpleBaseFragmentInitiator.activity = getActivity();
        if(view.getBackground()==null){
            view.setBackgroundColor(getResources().getColor(R.color.grey_200));
        }
        ButterKnife.bind(this, view);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        freeMemory();
        LanguageTools.initLanguage(getActivity());
        if(getArguments()!=null){
            PocketKnife.bindArguments(this);
        }
        Bundler.bind(this);
        saver = Preferencer.bind(this);

    }

    public int[] getDimension(){
        return ((SimpleBaseActivityInitiator)getActivity()).getDimension();
    }


    public SearchView injectSearchView(Menu menu, SearchView.OnQueryTextListener onQueryTextListener){
        SearchManager searchManager = (SearchManager) getContext().getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search_menu).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setOnQueryTextListener(onQueryTextListener);
        return searchView ;
    }

    public Activity activity(){
        return getActivity();
    }
    public <T extends SimpleBaseActivityInitiator>  T activity(Class<T> tClass){
        return (T) activity();
    }


    @Override
    public void onStart() {
        super.onStart();
        freeMemory();
    }

    @Override
    public void onStop() {
        super.onStop();
        freeMemory();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        freeMemory();

    }

    public void freeMemory(){
        Caster.activity(activity(), SimpleBaseActivityInitiator.class).freeMemory();
    }


}
