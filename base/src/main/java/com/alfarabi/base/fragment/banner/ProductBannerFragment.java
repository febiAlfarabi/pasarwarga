package com.alfarabi.base.fragment.banner;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.base.R;
import com.alfarabi.base.fragment.FullScreenImageFragment;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 8/28/17.
 */

public class ProductBannerFragment extends Fragment {

    public ImageView imageView;
    @Getter@Setter String url ;
    public static ProductBannerFragment instance(String url){
        ProductBannerFragment productBannerFragment = new ProductBannerFragment();
        productBannerFragment.setUrl(url);
        return productBannerFragment ;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return LayoutInflater.from(getActivity()).inflate(R.layout.fragment_banner, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        imageView = (ImageView) view.findViewById(R.id.bannerview);
        imageView.setDrawingCacheEnabled(true);
        GlideApp.with(this).asBitmap().load(url).diskCacheStrategy(DiskCacheStrategy.ALL).into(imageView);
        imageView.setOnClickListener(v -> {
            if(getActivity()!=null){
                if(this.getActivity() instanceof AppCompatActivity) {
                    Bitmap bitmap = imageView.getDrawingCache();
                    FullScreenImageFragment fragment = FullScreenImageFragment.newInstance(bitmap);
                    fragment.show(((AppCompatActivity)this.getActivity()).getSupportFragmentManager(), "FS_TAG");
                }
            }
        });
    }
}
