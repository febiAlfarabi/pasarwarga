package com.alfarabi.base.fragment.home;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.TranslateAnimation;

import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.activity.SimpleBaseActivityInitiator;
import com.alfarabi.base.activity.home.BaseDrawerActivity;
import com.alfarabi.base.fragment.BaseUserFragment;
import com.alfarabi.base.model.Landing;
import com.alfarabi.base.realm.RealmDao;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 6/16/17.
 */

public abstract class BaseDrawerFragment<O> extends BaseUserFragment<O> implements Toolbar.OnMenuItemClickListener{

    public static final String TAG = BaseDrawerFragment.class.getName();
    
    protected float lastTranslate = 0.0f;

    public abstract Toolbar getToolbar() ;
    public abstract AppBarLayout getAppbarLayout() ;
    public abstract int getMenuId();

    protected ActionBarDrawerToggle drawerToggle;
    @Override
    public String getTAG() {
        return TAG ;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        switchMenu(true);
    }

    @Override
    public void onPause() {
        switchMenu(false);
        super.onPause();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(keepSideMenu()){
            try{
                if(getToolbar()==null){
                    return;
                }
                drawerToggle = new ActionBarDrawerToggle(activity(), activity(BaseDrawerActivity.class).getDrawerLayout(), getToolbar(), 0, 0) {
                    @Override
                    public void onDrawerClosed(View drawerView) {
                        super.onDrawerClosed(drawerView);
                    }

                    @Override
                    public void onDrawerOpened(View drawerView) {
                        super.onDrawerOpened(drawerView);
                    }

                    @Override
                    public void onDrawerSlide(View drawerView, float slideOffset) {
                        super.onDrawerSlide(drawerView, slideOffset);
                        if(activity()!=null && activity(BaseDrawerActivity.class).getSublimeNavigationView()!=null){
                            float moveFactor = (activity(BaseDrawerActivity.class).getSublimeNavigationView().getWidth() * slideOffset);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                                view.setTranslationX(moveFactor);
                            } else {
                                TranslateAnimation anim = new TranslateAnimation(lastTranslate, moveFactor, 0.0f, 0.0f);
                                anim.setDuration(0);
                                anim.setFillAfter(true);
                                view.startAnimation(anim);
                                lastTranslate = moveFactor;
                            }
                        }

                    }
                };
                activity(BaseDrawerActivity.class).getDrawerLayout().post(() -> drawerToggle.syncState());
                activity(BaseDrawerActivity.class).getDrawerLayout().setDrawerListener(drawerToggle);
                drawerToggle.syncState();

            }catch (Exception e){
                e.printStackTrace();
            }
        }else if(WindowFlow.canGoBack(getActivity())){
            if(getToolbar()!=null) {
                getToolbar().setNavigationOnClickListener(v -> getActivity().onBackPressed());
            }
        }

    }



    public void switchMenu(boolean on){
        if(getMenuId()!=0 && activity(BaseDrawerActivity.class).getSublimeNavigationView().getMenu().getMenuItem(getMenuId())!=null){
            activity(BaseDrawerActivity.class).getSublimeNavigationView().getMenu().getMenuItem(getMenuId()).setChecked(on);
        }
    }

    public boolean keepSideMenu(){
        return false ;
    }
}
