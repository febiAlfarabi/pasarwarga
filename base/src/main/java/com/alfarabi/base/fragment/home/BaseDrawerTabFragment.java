package com.alfarabi.base.fragment.home;

import android.support.v4.view.ViewPager;

import com.alfarabi.alfalibs.adapters.viewpager.BaseTabPagerAdapter;
import com.alfarabi.base.view.TabLayout;

/**
 * Created by Alfarabi on 6/16/17.
 */

public abstract class BaseDrawerTabFragment<O> extends BaseDrawerFragment<O>{
    
    public static final String TAG = BaseDrawerTabFragment.class.getName();

    public abstract BaseTabPagerAdapter<?> baseTabPagerAdapter() ;

    public abstract TabLayout getTabLayout() ;
    public abstract ViewPager getViewPager() ;

    @Override
    public String getTAG() {
        return TAG ;
    }

    public void initTabLayout(ViewPager.OnPageChangeListener onPageChangeListener) {
        if(onPageChangeListener!=null) {
            getViewPager().addOnPageChangeListener(onPageChangeListener);
        }
        getViewPager().setAdapter(null);
        getViewPager().setAdapter(baseTabPagerAdapter());
        getTabLayout().removeAllTabs();
        getTabLayout().setupWithViewPager(getViewPager());
    }

}
