package com.alfarabi.base.fragment.login;

import com.alfarabi.base.fragment.BasePublicFragment;

/**
 * Created by Alfarabi on 6/15/17.
 */

public abstract class BaseLoginFormFragment extends BasePublicFragment {

    @Override
    public String getTAG() {
        return TAG = getClass().getName();
    }
}
