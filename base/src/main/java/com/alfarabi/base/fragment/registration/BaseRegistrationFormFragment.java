package com.alfarabi.base.fragment.registration;


import android.support.v4.app.Fragment;

import com.alfarabi.base.fragment.BasePublicFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public abstract class BaseRegistrationFormFragment<O> extends BasePublicFragment<O> {


    @Override
    public String getTAG() {
        return TAG = getClass().getName();
    }

}
