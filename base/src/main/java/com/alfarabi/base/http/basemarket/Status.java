//package com.alfarabi.base.http.basemarket;
//
//import com.google.gson.annotations.SerializedName;
//
//import lombok.Getter;
//import lombok.Setter;
//
///**
// * @author Hendra Anggrian (hendra@ahloo.com)
// */
//public class Status {
//    @Getter@Setter@SerializedName("message") String message;
//    @Getter@Setter@SerializedName("status") boolean status;
//    @Getter@Setter@SerializedName("code") int code;
//}