package com.alfarabi.base.http.basemarket;


import com.alfarabi.base.http.basemarket.response.Response;

import lombok.ToString;


/**
 * All responses from backend has {@literal status} JSONObject.
 * Therefore, every other response classes inherit from this class.
 *
 * @author Hendra Anggrian (hendra@ahloo.com)
 */
@ToString
public class StatusResponse {
    public Response status;
}