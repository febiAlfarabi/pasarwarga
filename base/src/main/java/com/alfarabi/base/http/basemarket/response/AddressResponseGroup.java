package com.alfarabi.base.http.basemarket.response;

import com.alfarabi.base.model.location.Address;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 8/15/17.
 */

public class AddressResponseGroup {

    public class GetAll extends Response{
        @Getter@Setter@SerializedName("data") List<Address> addresses ;
    }

    public class Get extends Response{
        @Getter@Setter@SerializedName("data") Address address ;
    }

    public class Delete extends Response{
//        @Getter@Setter@SerializedName("data") Address address ;
    }

}
