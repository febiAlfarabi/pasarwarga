package com.alfarabi.base.http.basemarket.response;

import com.alfarabi.base.model.User;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 8/15/17.
 */

public class AdministrationResponse {

    public class Signup extends Response{
        @SerializedName("data")@Getter@Setter User user ;
    }

    public class Verification extends Response{
        @SerializedName("data")@Getter@Setter User user ;
    }

    public class ForgetPassword extends Response{}

    public class ResetPassword extends Response{}

}
