package com.alfarabi.base.http.basemarket.response;

import com.alfarabi.base.model.Branch;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 8/1/17.
 */

public class BranchResponse extends Response {
    @SerializedName("data")@Getter@Setter List<Branch> branches ;

}
