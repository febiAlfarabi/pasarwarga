package com.alfarabi.base.http.basemarket.response;

import com.alfarabi.base.model.Payment;
import com.alfarabi.base.model.transaction.Cart;
import com.alfarabi.base.model.transaction.finance.kreditplus.KPCheckout;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 8/15/17.
 */

public class CartResponseGroup {

    public class AddToCart extends Response {
        @Getter @Setter @SerializedName("data") Cart cart;
    }

    public class ItemOnCart extends Response {
        @Getter @Setter @SerializedName("data") Cart cart;
    }

    public class DeleteFromCart extends Response {
    }

    public class PaymentMethod extends Response {
        @Getter @Setter @SerializedName("data") List<Payment> payments = new ArrayList<>();
    }

    public class OrderCheckout extends Response {
        @Getter @Setter @SerializedName("data")
        KPCheckout KPCheckout;
    }

    public class AddCouponCode extends Response {
        @Getter @Setter @SerializedName("data") Cart cart;
    }

    public class RemoveCouponCode extends Response {
        @Getter @Setter @SerializedName("data") Cart cart;
    }
}