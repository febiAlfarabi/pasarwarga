package com.alfarabi.base.http.basemarket.response;

import com.alfarabi.base.model.location.City;
import com.alfarabi.base.model.location.District;
import com.alfarabi.base.model.location.Province;
import com.alfarabi.base.model.location.Subdistrict;
import com.alfarabi.base.model.location.ZipCode;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 8/23/17.
 */

public class CityResponseGroup {

    public class ProvinceResponse extends Response {
        @SerializedName("data")@Getter @Setter List<Province> provinces = new ArrayList<>();

    }

    public class CityResponse extends Response {
        @SerializedName("data")@Getter@Setter List<City> cities = new ArrayList<>();

    }

    public class DistrictResponse extends Response {
        @SerializedName("data")@Getter@Setter List<District> districts = new ArrayList<>();

    }

    public class SubdistrictResponse extends Response {
        @SerializedName("data")@Getter@Setter List<Subdistrict> subdistricts = new ArrayList<>();

    }

    public class ZipCodeResponse extends Response {
        @SerializedName("data")@Getter@Setter List<ZipCode> zipCodes = new ArrayList<>();

    }
}
