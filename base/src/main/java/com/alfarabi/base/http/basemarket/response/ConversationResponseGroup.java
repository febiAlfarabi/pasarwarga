package com.alfarabi.base.http.basemarket.response;

import com.alfarabi.base.model.Expedition;
import com.alfarabi.base.model.Shipping;
import com.alfarabi.base.model.chat.Conversation;
import com.alfarabi.base.model.chat.UserConversation;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 8/15/17.
 */

public class ConversationResponseGroup {

    public class UserConversationResponse extends Response{
        @Getter@Setter@SerializedName("data") List<UserConversation> userConversations = new ArrayList<>();
    }

    public class ConversationResponse extends Response{
        @Getter@Setter@SerializedName("data") List<Conversation> conversations = new ArrayList<>();
    }

    public class SendConversationResponse extends Response{
        @Getter@Setter@SerializedName("data") Conversation conversations ;
    }

}
