package com.alfarabi.base.http.basemarket.response;

import com.alfarabi.base.model.Product;
import com.alfarabi.base.model.ProductSearch;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 8/15/17.
 */

public class FavoriteResponseGroup {

    public class ChangeFavoriteResponse extends Response {
        @Getter @Setter @SerializedName("data") Product product;
    }

    public class FavoriteListResponse extends Response {
        @Getter @Setter @SerializedName("data") List<Product> products = new ArrayList<>();
    }
}
