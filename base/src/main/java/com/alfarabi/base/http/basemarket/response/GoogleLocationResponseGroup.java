package com.alfarabi.base.http.basemarket.response;

import com.alfarabi.base.model.geolocation.Prediction;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 8/25/17.
 */

public class GoogleLocationResponseGroup {

    public class SearchPlace {
        @SerializedName("predictions")@Getter @Setter List<Prediction> predictions ;
        @SerializedName("status")@Getter@Setter String status ;


        public boolean isOk(){
            return status!=null && status.equals("OK");
        }
    }
}
