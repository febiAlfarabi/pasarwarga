package com.alfarabi.base.http.basemarket.response;

import com.alfarabi.base.model.Insurance;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 8/15/17.
 */

public class InsuranceResponseGroup {

    public class GetInsurances extends Response{
        @Getter@Setter@SerializedName("data") List<Insurance> insurances = new ArrayList<>();
    }

}
