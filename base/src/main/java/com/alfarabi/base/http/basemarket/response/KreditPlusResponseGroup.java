package com.alfarabi.base.http.basemarket.response;

import com.alfarabi.base.model.transaction.Cart;
import com.alfarabi.base.model.transaction.finance.kreditplus.KPOrder;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 8/15/17.
 */

public class KreditPlusResponseGroup {

    public class Submit extends Response{
        @Getter@Setter@SerializedName("data") KPOrder kpOrder ;
    }



}
