package com.alfarabi.base.http.basemarket.response;

import com.alfarabi.base.model.Landing;
import com.alfarabi.base.model.Product;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 8/15/17.
 */

public class LandingResponseGroup extends Response{
    @Getter@Setter@SerializedName("data") Landing landing ;

    public class EventResponse extends Response{
        @Getter@Setter@SerializedName("data") List<Product> products = new ArrayList<>();

    }


}
