package com.alfarabi.base.http.basemarket.response;

import com.alfarabi.base.model.Landing;
import com.alfarabi.base.model.User;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 8/15/17.
 */

public class LoginResponse extends Response{
    @Getter@Setter@SerializedName("data") User user ;
}
