package com.alfarabi.base.http.basemarket.response;

import com.alfarabi.base.model.shop.Note;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 8/15/17.
 */

public class NotesResponseGroup {

    public class GetNotes extends Response{
        @Getter@Setter@SerializedName("data") List<Note> notes = new ArrayList<>();
    }

    public class CreateNote extends Response{
        @Getter@Setter@SerializedName("data") Note note ;
    }

    public class DeleteNote extends Response{
    }

}
