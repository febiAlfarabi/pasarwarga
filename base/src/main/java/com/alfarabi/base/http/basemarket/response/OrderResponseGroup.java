package com.alfarabi.base.http.basemarket.response;

import com.alfarabi.base.model.Expedition;
import com.alfarabi.base.model.Shipping;
import com.alfarabi.base.model.shop.Order;
import com.alfarabi.base.model.shop.OrderDetail;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 8/15/17.
 */

public class OrderResponseGroup {

    public class OrderListResponse extends Response{
        @Getter@Setter@SerializedName("data") List<Order> orders = new ArrayList<>();
    }

    public class OrderDetailResponse extends Response{
        @Getter@Setter@SerializedName("data") OrderDetail orderDetail ;
    }

    public class ChangeStatusResponse extends Response{
        @Getter@Setter@SerializedName("data") Order order ;
    }

}
