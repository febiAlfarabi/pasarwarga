package com.alfarabi.base.http.basemarket.response;

import com.alfarabi.base.model.Landing;
import com.alfarabi.base.model.Product;
import com.alfarabi.base.model.ProductSearch;
import com.alfarabi.base.model.Tenor;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 8/15/17.
 */

public class ProductResponseGroup{

    public class ProductResponse extends Response{
        @Getter@Setter@SerializedName("data") Product product ;
    }

    public class ProductsResponse extends Response{
        @Getter@Setter@SerializedName("data") List<Product> products = new ArrayList<>();
    }

    public class ProductSearchResponse extends Response{
        @Getter@Setter@SerializedName("data") ProductSearch productSearch ;
    }

    public class AddProductResponse extends Response{
        @Getter@Setter@SerializedName("data") Product product ;
    }

    public class TenorResponse extends Response{
        @Getter@Setter@SerializedName("data") HashMap<String, List<Tenor>> hashMap = new HashMap<>();
    }

}
