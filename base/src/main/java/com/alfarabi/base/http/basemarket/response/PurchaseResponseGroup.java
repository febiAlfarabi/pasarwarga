package com.alfarabi.base.http.basemarket.response;

import com.alfarabi.base.model.Payment;
import com.alfarabi.base.model.transaction.Cart;
import com.alfarabi.base.model.transaction.SummaryStatus;
import com.alfarabi.base.model.transaction.finance.kreditplus.KPCheckout;
import com.alfarabi.base.model.transaction.purchase.Purchase;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 8/15/17.
 */

public class PurchaseResponseGroup {

    public class PurchasesResponse extends Response{
        @Getter@Setter@SerializedName("data") List<Purchase> purchases = new ArrayList<>() ;
    }

    public class PurchaseDetailResponse extends Response {
        @Getter@Setter@SerializedName("data") Purchase purchase;
    }

    public class PurchaseStatusResponse extends Response {
        @Getter@Setter@SerializedName("data") SummaryStatus summaryStatus ;
    }
}
