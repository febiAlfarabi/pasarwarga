package com.alfarabi.base.http.basemarket.response;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by alfarabi on 7/27/17.
 */

public class Response {
    @SerializedName("status")@Getter@Setter boolean status ;
    @SerializedName("code")@Getter@Setter int code ;
    @SerializedName("message")@Getter@Setter String message ;

}
