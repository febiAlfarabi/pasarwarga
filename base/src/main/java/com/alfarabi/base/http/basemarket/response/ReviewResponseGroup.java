package com.alfarabi.base.http.basemarket.response;

import com.alfarabi.base.model.User;
import com.alfarabi.base.model.review.Feedback;
import com.alfarabi.base.model.review.Review;
import com.alfarabi.base.model.review.SummaryFeedback;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by yudapratama on 8/21/2017.
 */

public class ReviewResponseGroup extends Response {

    @SerializedName("data") @Getter @Setter List<Review> reviews;

    public class FeedbackableResponse extends Response {
        @Getter @Setter @SerializedName("data") List<Feedback> feedbacks;
    }
    public class Feedbacks extends Response {
        @Getter @Setter @SerializedName("data") List<Feedback> feedbacks;
    }
    public class FeedbackAsSeller extends Response {
        @Getter @Setter @SerializedName("data") List<Feedback> feedbacks;
    }

    public class AddFeedback extends Response {
        @Getter @Setter @SerializedName("data") Feedback feedback;
    }
    public class FeedbackCount extends Response {
        @Getter @Setter @SerializedName("data")
        HashMap<String, SummaryFeedback> hashMap;
    }

}
