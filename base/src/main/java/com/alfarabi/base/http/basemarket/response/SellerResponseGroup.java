package com.alfarabi.base.http.basemarket.response;
import com.alfarabi.base.model.FeaturedShop;
import com.alfarabi.base.model.Product;
import com.alfarabi.base.model.ProductSearch;
import com.alfarabi.base.model.ShopLocation;
import com.alfarabi.base.model.shop.SellerDashboard;
import com.alfarabi.base.model.shop.SellerInfo;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 8/15/17.
 */

public class SellerResponseGroup {

    public class RegisterResponse extends Response{
        @Getter@Setter@SerializedName("data") SellerInfo sellerInfo ;
    }

    public class DetailResponse extends Response{
        @Getter@Setter@SerializedName("data") SellerDashboard sellerDashboard;
    }

    public class AdminActivationResponse extends Response{
        @Getter@Setter@SerializedName("data") HashMap<String, Boolean> hashMap = new HashMap<>();
    }


    public class SearchResponse extends Response{
        @Getter@Setter@SerializedName("data") List<FeaturedShop> featuredShops = new ArrayList<>();
    }

    public class SellerFavoriteResponse extends Response{
//        @Getter@Setter@SerializedName("data") List<FeaturedShop> featuredShops = new ArrayList<>();
    }

    public class ListSellerFavoriteResponse extends Response{
        @Getter@Setter@SerializedName("data") List<SellerInfo> sellerInfos = new ArrayList<>();
    }


    public class UpdateShopLocationInfo extends Response{
        @Getter@Setter@SerializedName("data") ShopLocation shopLocation;
    }

    public class UpdateShopResponse extends Response{
        @Getter@Setter@SerializedName("data") SellerInfo sellerInfo ;
    }
}
