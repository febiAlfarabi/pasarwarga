package com.alfarabi.base.http.basemarket.response;

import com.alfarabi.base.model.Product;
import com.alfarabi.base.model.User;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by USER on 9/14/2017.
 */

public class SettingResponseGroup {

    public class ChangeEmailAddress extends Response {
        @Getter @Setter @SerializedName("data") User user;
    }

    public class ChangePhoneNumber extends Response {
        @Getter @Setter @SerializedName("data") User user;
    }

    public class ChangePassword extends Response {
        @Getter @Setter @SerializedName("data") User user;
    }

    public class DeactiveAccount extends Response {
    }

    public class ChangeProfilePicture extends Response {
        //public String photo;
        @Getter @Setter @SerializedName("data") User user;
    }

    public class ChangeProfileName extends Response {
        @Getter @Setter @SerializedName("data") User user;
    }

    public class ChangeProfileInfo extends Response {
        @Getter @Setter @SerializedName("data") User user;
    }
}
