package com.alfarabi.base.http.basemarket.response;

import com.alfarabi.base.model.Expedition;
import com.alfarabi.base.model.Shipping;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 8/15/17.
 */

public class ShippingResponseGroup {

    public class AvailableShipping extends Response{
        @Getter@Setter@SerializedName("data") List<Shipping> shippings ;
    }

    public class ShippingResponse extends Response{
        @Getter@Setter@SerializedName("data") List<Shipping> shippings ;
    }

    public class ExpeditionService extends Response{
        @Getter@Setter@SerializedName("data") List<Expedition> expeditions ;
    }

}
