package com.alfarabi.base.http.basemarket.response;

import com.alfarabi.base.model.shop.Order;
import com.alfarabi.base.model.shop.OrderDetail;
import com.alfarabi.base.model.transaction.TraceTraking;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 8/15/17.
 */

public class TrackingResponseGroup {

    public class TrackingListResponse extends Response{
        @Getter@Setter@SerializedName("data") TraceTraking traceTrakings ;
    }
}

