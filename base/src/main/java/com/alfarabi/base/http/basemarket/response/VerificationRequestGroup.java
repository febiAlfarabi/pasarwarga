package com.alfarabi.base.http.basemarket.response;

import com.alfarabi.base.model.User;
import com.alfarabi.base.model.Verification;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 8/21/17.
 */

public class VerificationRequestGroup {

    public class Request extends Response{

        @Getter@Setter@SerializedName("data") Verification verification;

    }

    public class Verify extends Response{

        @Getter@Setter@SerializedName("data") User user ;

    }
}
