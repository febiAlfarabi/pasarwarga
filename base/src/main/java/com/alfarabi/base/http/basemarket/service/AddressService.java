package com.alfarabi.base.http.basemarket.service;

import com.alfarabi.base.http.basemarket.response.AddressResponseGroup;
import com.alfarabi.base.model.location.Address;
import com.alfarabi.base.tools.Initial;

import java.util.HashMap;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by User on 02/07/2017.
 */

public interface AddressService {


    static final String PATH_1 = "/shipping_address";
    static final String PATH_2 = "/get_all";
    static final String PATH_3 = "/create";
    static final String PATH_4 = "/destroy";
    static final String PATH_5 = "/update";

    static final String LIST = "/carts";
    static final String ID = "id";
    static final String PARAM_ID = "/{"+ID+"}";



    @GET(Initial.API+PATH_1+PATH_2) Observable<AddressResponseGroup.GetAll> getAll();
    @POST(Initial.API+PATH_1+PATH_3) Observable<AddressResponseGroup.Get> create(@Body Address address);
    @POST(Initial.API+PATH_1+PATH_5) Observable<AddressResponseGroup.Get> update(@Body Address address);
    @POST(Initial.API+PATH_1+PATH_4) Observable<AddressResponseGroup.Delete> delete(@Body HashMap<String, String> hashMap);


}
