package com.alfarabi.base.http.basemarket.service;

import com.alfarabi.base.http.basemarket.response.AdministrationResponse;
import com.alfarabi.base.http.basemarket.response.LoginResponse;
import com.alfarabi.base.tools.Initial;

import java.util.HashMap;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by User on 02/07/2017.
 */

public interface AdministrationService {

    public static String EMAIL = "email";
    public static String PASSWORD = "password";
    public static String FIRST_NAME = "first_name";
    public static String LAST_NAME = "last_name";
    public static String GENDER = "gender";
    public static String PHONE = "phone";
    public static String USERNAME = "username";


    public static String DEVICE_TOKEN = "device_token";


    static final String PATH_1 = "/auth";
    static final String PATH_2 = "/signup";
    static final String PATH_3 = "/site/user/confirm_register/";
    static final String PATH_4 = "/forgot_password";
    static final String PATH_5 = "/requestForgot";
    static final String PATH_6 = "/resetPassword";

    static final String LIST = "/products";
    static final String ID = "id";
    static final String PARAM_ID = "/{"+ID+"}";

    @POST(Initial.API+PATH_1+PATH_2) Observable<AdministrationResponse.Signup> signup(@Body HashMap<String, String> hashMap);

    @POST(Initial.API+PATH_3) Observable<AdministrationResponse.Verification> verify(@Body HashMap<String, String> hashMap);

    @POST(Initial.API+PATH_4+PATH_5) Observable<AdministrationResponse.ForgetPassword> forgetPasssword(@Body HashMap<String, String> hashMap);

    @POST(Initial.API+PATH_4+PATH_6) Observable<AdministrationResponse.ForgetPassword> resetPasssword(@Body HashMap<String, String> hashMap);


}
