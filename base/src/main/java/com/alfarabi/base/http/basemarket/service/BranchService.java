package com.alfarabi.base.http.basemarket.service;

import com.alfarabi.base.http.basemarket.response.BranchResponse;
import com.alfarabi.base.tools.Initial;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by User on 02/07/2017.
 */

public interface BranchService {


    public static final int IND_COUNTRY_ID = 91 ;

    static final String PATH_1 = "/shipping_address";
    static final String PATH_3 = "/getBranch";



    static final String LIST = "/cities";
    static final String CITY_ID = "city_id";
    static final String DISTRICT_ID = "kecamatan_id";
    static final String SUBDISTRICT_ID = "kelurahan_id";
    static final String PARAM_CITY_ID = "/{"+CITY_ID+"}";
    static final String PARAM_DISTRICT_ID = "/{"+DISTRICT_ID+"}";
    static final String PARAM_SUBDISTRICT_ID = "/{"+SUBDISTRICT_ID+"}";

    @GET(Initial.PW_API+PATH_1+PATH_3+PARAM_CITY_ID+PARAM_DISTRICT_ID+PARAM_SUBDISTRICT_ID)
    Observable<BranchResponse> getBranches(@Path(CITY_ID) long cityId, @Path(DISTRICT_ID) long districtId, @Path(SUBDISTRICT_ID) long subdistrictId);



}
