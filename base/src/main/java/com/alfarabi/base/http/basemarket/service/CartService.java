package com.alfarabi.base.http.basemarket.service;

import com.alfarabi.base.http.basemarket.response.CartResponseGroup;
import com.alfarabi.base.model.transaction.Cart;
import com.alfarabi.base.model.transaction.finance.kreditplus.KPCheckoutParam;
import com.alfarabi.base.tools.Initial;

import java.util.ArrayList;
import java.util.HashMap;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by User on 02/07/2017.
 */

public interface CartService {


    static final String PATH_1 = "/carts";
    static final String PATH_2 = "/cart";
    static final String PATH_3 = "/add_to_cart";
    static final String PATH_4 = "/delete_cart_item";
    static final String PATH_5 = "/payment_methods";
    static final String PATH_6 = "/checkout";
    static final String PATH_7 = "/coupon_validation";
    static final String PATH_8 = "/remove_coupon";

    static final String LIST = "/carts";
    static final String ID = "id";
    static final String CART_ID = "cart_id";
    static final String PARAM_ID = "/{"+ID+"}";
    static final String PARAM_CART_ID = "/{"+CART_ID+"}";

    @GET(Initial.API+PATH_1+LIST) Observable<ArrayList<Cart>> getCarts();

    @GET(Initial.API+PATH_1+PARAM_ID+PATH_2) Observable<Cart> getCart(@Path(ID) int id);

    @GET(Initial.API+PATH_2) Observable<CartResponseGroup.ItemOnCart> itemOnCart();

    @POST(Initial.API+PATH_2+PATH_3) Observable<CartResponseGroup.AddToCart> addToCart(@Body HashMap<String, String> hashMap);

    @POST(Initial.API+PATH_2+PATH_4) Observable<CartResponseGroup.DeleteFromCart> deleteFromCart(@Body HashMap<String, String> hashMap);

    @GET(Initial.API+PATH_2+PATH_5) Observable<CartResponseGroup.PaymentMethod> getPaymentMethod();

    @POST(Initial.API+PATH_2+PATH_6) Observable<CartResponseGroup.OrderCheckout> checkout(@Body KPCheckoutParam KPCheckoutParam);

    @POST(Initial.API + PATH_2 + PATH_7) Observable<CartResponseGroup.AddCouponCode> addCouponCode(@Body HashMap<String, String> hashMap);

    @GET(Initial.API + PATH_2 + PATH_8 + PARAM_CART_ID) Observable<CartResponseGroup.RemoveCouponCode> removeCouponCode(@Path(CART_ID) String cartId);
}
