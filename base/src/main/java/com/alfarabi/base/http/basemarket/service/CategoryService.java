package com.alfarabi.base.http.basemarket.service;

import com.alfarabi.base.model.location.Category;
import com.alfarabi.base.tools.Initial;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by User on 02/07/2017.
 */

public interface CategoryService {


    static final String PATH_1 = "/categories";
    static final String PATH_2 = "/category";

    static final String LIST = "/categories";
    static final String ID = "id";
    static final String PARAM_ID = "/{"+ID+"}";

    @GET(Initial.API_VERSION+PATH_1+LIST)
    Observable<List<Category>> getCategories();

    @GET(Initial.API_VERSION+PATH_1+PARAM_ID+PATH_2)
    Observable<Category> getCategory(@Path(ID) int id);

}
