package com.alfarabi.base.http.basemarket.service;

import com.alfarabi.base.http.basemarket.response.CityResponseGroup;
import com.alfarabi.base.tools.Initial;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by User on 02/07/2017.
 */

public interface CityService {


    public static final int IND_COUNTRY_ID = 91 ;

    static final String PATH_1 = "/shipping_address";
    static final String PATH_2 = "/getCountries";
    static final String PATH_3 = "/getProvince";
    static final String PATH_4 = "/getCities";
    static final String PATH_5 = "/getKecamatan";
    static final String PATH_6 = "/getKelurahan";
    static final String PATH_7 = "/getZipCode";
    static final String PATH_8 = "/getAllCity";
    static final String PATH_9 = "/getZipCodeByKecamatan";



    static final String LIST = "/cities";
    static final String ID = "id";
    static final String KECAMATAN_ID = "kecamatan_id";
    static final String PARAM_ID = "/{"+ID+"}";
    static final String PARAM_KECAMATAN_ID = "/{"+KECAMATAN_ID+"}";

    @GET(Initial.API+PATH_1+PATH_3+PARAM_ID)
    Observable<CityResponseGroup.ProvinceResponse> getProvinces(@Path(ID) long countryId);

    @GET(Initial.API+PATH_1+PATH_4+PARAM_ID)
    Observable<CityResponseGroup.CityResponse> getCities(@Path(ID) long provinceId);

    @GET(Initial.API+PATH_1+PATH_8+PARAM_ID)
    Observable<CityResponseGroup.CityResponse> getAllCityByCountry(@Path(ID) long countryId);

    @GET(Initial.API+PATH_1+PATH_5+PARAM_ID)
    Observable<CityResponseGroup.DistrictResponse> getKecamatan(@Path(ID) long cityId);

    @GET(Initial.API+PATH_1+PATH_6+PARAM_ID)
    Observable<CityResponseGroup.SubdistrictResponse> getKelurahan(@Path(ID) long districtId);

    @GET(Initial.API+PATH_1+PATH_7+PARAM_ID)
    Observable<CityResponseGroup.ZipCodeResponse> getZipCode(@Path(ID) long subdistrictId);

    @GET(Initial.API+PATH_1+PATH_9+PARAM_KECAMATAN_ID)
    Observable<CityResponseGroup.ZipCodeResponse> getZipCodeByDistrict(@Path(KECAMATAN_ID) long districtId);


//    @GET(Initial.API_VERSION+PATH_1+PARAM_ID+PATH_2)
//    Observable<City> getCity(@Path(ID) int id);


}
