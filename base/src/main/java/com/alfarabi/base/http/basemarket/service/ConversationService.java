package com.alfarabi.base.http.basemarket.service;

import com.alfarabi.base.http.basemarket.response.ConversationResponseGroup;
import com.alfarabi.base.http.basemarket.response.ShippingResponseGroup;
import com.alfarabi.base.tools.Initial;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by User on 02/07/2017.
 */

public interface ConversationService {


    static final String PATH_1 = "/message";
    static final String PATH_2 = "/conversations";
    static final String PATH_3 = "/read";
    static final String PATH_4 = "/send";


    static final String PARTNER_ID = "partner_id";
    static final String PARAM_PARTNER_ID = "/{" + PARTNER_ID + "}";
//    static final String PARTNER_ID = "partner_id";
//    static final String PARAM_PARTNER_ID = "/{" + PARTNER_ID + "}";

//    static final String PARAM_CATEGORY = "/{" + CATEGORY + "}";

    @GET(Initial.API+PATH_1+PATH_2) Observable<ConversationResponseGroup.UserConversationResponse> getConversation();

    @GET(Initial.API+PATH_1+PATH_3) Observable<ConversationResponseGroup.ConversationResponse> getConversation(@Query(PARTNER_ID) String partnerId);

    @POST(Initial.API+PATH_1+PATH_4+PARAM_PARTNER_ID) Observable<ConversationResponseGroup.SendConversationResponse> send(@Path(PARTNER_ID) String partnerId, @Body Map hashMap);

}
