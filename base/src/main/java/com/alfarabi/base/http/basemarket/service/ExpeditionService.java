package com.alfarabi.base.http.basemarket.service;

import com.alfarabi.base.http.basemarket.response.AddressResponseGroup;
import com.alfarabi.base.http.basemarket.response.ShippingResponseGroup;
import com.alfarabi.base.tools.Initial;

import java.util.HashMap;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by User on 02/07/2017.
 */

public interface ExpeditionService {


    static final String PATH_1 = "/cart";
    static final String PATH_2 = "/get_expedition_services";


    @POST(Initial.API+PATH_1+PATH_2) Observable<ShippingResponseGroup.ExpeditionService> expeditionService(@Body HashMap<String, String> hashMap);

}
