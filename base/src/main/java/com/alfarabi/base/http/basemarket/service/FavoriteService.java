package com.alfarabi.base.http.basemarket.service;

import com.alfarabi.base.http.basemarket.response.FavoriteResponseGroup;
import com.alfarabi.base.http.basemarket.response.ProductResponseGroup;
import com.alfarabi.base.model.Product;
import com.alfarabi.base.tools.Initial;

import java.util.ArrayList;
import java.util.HashMap;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by User on 02/07/2017.
 */

public interface FavoriteService {

//    https://www.pasarwarga.com/api/product?category=0&page=1&min_price=10000&max_price=999999&sort_by=newest

    static final String PATH_1 = "/product";
    static final String PATH_2 = "/change_favorite";
    static final String PATH_3 = "/favorite_list";

    static final String ID = "id";

    static final String KEYWORD = "keyword";

    static final String PARAM_ID = "/{" + ID + "}";

    @POST(Initial.API + PATH_1 + PATH_2)
    Observable<FavoriteResponseGroup.ChangeFavoriteResponse> changeFavorite(@Body HashMap<String, String> hashMap);

    @GET(Initial.API + PATH_1 + PATH_3)
    Observable<FavoriteResponseGroup.FavoriteListResponse> listFavorite();
}
