package com.alfarabi.base.http.basemarket.service;

import com.alfarabi.base.http.basemarket.response.GoogleLocationResponseGroup;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by User on 02/07/2017.
 */

public interface GoogleLocationService {
    //https://maps.googleapis.com/maps/api/place/autocomplete/json?input=Pamul&types=geocode&language=en&key=AIzaSyCwnI7xeujeiZiGsiYFBxG2IqLhIoX_3oY
    //https://maps.googleapis.com/maps/api/place/autocomplete/json?input=test&types=en&key=AIzaSyCwnI7xeujeiZiGsiYFBxG2IqLhIoX_3oY

    static final String PATH_1 = "/maps";
    static final String PATH_2 = "/api";
    static final String PATH_3 = "/place";
    static final String PATH_4 = "/autocomplete";
    static final String PATH_5 = "/json";

    static final String LIST = "/carts";
    static final String ID = "id";
    static final String PARAM_ID = "/{"+ID+"}";



    @GET(PATH_1+PATH_2+PATH_3+PATH_4+PATH_5) Observable<GoogleLocationResponseGroup.SearchPlace> search(@Query("input") String input, @Query("types") String types,@Query("language") String language, @Query("key") String key);


}
