package com.alfarabi.base.http.basemarket.service;

import com.alfarabi.base.http.basemarket.response.InsuranceResponseGroup;
import com.alfarabi.base.tools.Initial;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by User on 02/07/2017.
 */

public interface InsuranceService {


    static final String PATH_1 = "/cart";
    static final String PATH_2 = "/insurance";

    static final String CART_ID = "cart_id";
    static final String PARAM_CART_ID = "/{"+CART_ID+"}";



    @GET(Initial.API+PATH_1+PATH_2+PARAM_CART_ID) Observable<InsuranceResponseGroup.GetInsurances> getInsurance(@Path(CART_ID) String cartId);


}
