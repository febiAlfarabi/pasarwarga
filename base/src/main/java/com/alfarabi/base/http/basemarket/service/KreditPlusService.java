package com.alfarabi.base.http.basemarket.service;

import com.alfarabi.base.http.basemarket.response.CartResponseGroup;
import com.alfarabi.base.http.basemarket.response.KreditPlusResponseGroup;
import com.alfarabi.base.tools.Initial;

import java.util.HashMap;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by User on 02/07/2017.
 */

public interface KreditPlusService {


    static final String PATH_1 = "/kreditplus";
    static final String PATH_2 = "/submit";


    static final String ID = "id";
    static final String PARAM_ID = "/{"+ID+"}";

    @POST(Initial.API+PATH_1+PATH_2) Observable<KreditPlusResponseGroup.Submit> submit(@Body HashMap<String, String> hashMap);


}
