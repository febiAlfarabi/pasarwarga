package com.alfarabi.base.http.basemarket.service;

import com.alfarabi.base.http.basemarket.response.LandingResponseGroup;
import com.alfarabi.base.tools.Initial;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Alfarabi on 8/15/17.
 */

public interface LandingService {

    static final String PATH_1 = "/landing";
    static final String PATH_2 = "/event";



    static final String EVENT_ID = "event_id";
    static final String PAGE = "page";
    static final String PARAM_EVENT_ID = "/{" + EVENT_ID + "}";
    static final String PARAM_PAGE = "/{" + PAGE + "}";


    @GET(Initial.API+PATH_1) Observable<LandingResponseGroup> dataLanding();

    @GET(Initial.API+PATH_2) Observable<LandingResponseGroup.EventResponse> getEventProducts(@Query(EVENT_ID) String eventId, @Query(PAGE) String page);


}
