package com.alfarabi.base.http.basemarket.service;

import com.alfarabi.base.http.basemarket.response.LoginResponse;
import com.alfarabi.base.http.basemarket.response.ProductResponseGroup;
import com.alfarabi.base.http.basemarket.response.Response;
import com.alfarabi.base.model.Product;
import com.alfarabi.base.tools.Initial;

import java.util.ArrayList;
import java.util.HashMap;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by User on 02/07/2017.
 */

public interface LoginService {

    public static String EMAIL_OR_USERNAME = "email_or_username";
    public static String PASSWORD = "password";
    public static String DEVICE_TOKEN = "device_token";


    static final String PATH_1 = "/auth";
    static final String PATH_2 = "/signin";
    static final String PATH_3 = "/logout";

    static final String LIST = "/products";
    static final String ID = "id";
    static final String PARAM_ID = "/{"+ID+"}";

    @POST(Initial.API+PATH_1+PATH_2) Observable<LoginResponse> login(@Body HashMap<String, String> hashMap);
    @GET(Initial.API+PATH_1+PATH_2) Observable<Response> logout();

}
