package com.alfarabi.base.http.basemarket.service;

import com.alfarabi.base.http.basemarket.response.NotesResponseGroup;
import com.alfarabi.base.tools.Initial;

import java.util.HashMap;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Alfarabi on 9/25/17.
 */

public interface NoteService {

    static final String PATH_1 = "/shop";
    static final String PATH_2 = "/notes";
    static final String PATH_3 = "/add_update_notes";
    static final String PATH_4 = "/notes_by_seller";
    static final String PATH_5 = "/delete_note";
    static final String NOTE_ID = "note_id";
    static final String PARAM_NOTE_ID = "/{"+NOTE_ID+"}";
    static final String SELLER_ID = "seller_id";
    static final String PARAM_SELLER_ID = "/{"+SELLER_ID+"}";



    @GET(Initial.API+PATH_1+PATH_2)
    Observable<NotesResponseGroup.GetNotes> getNotes();

    @GET(Initial.API+PATH_1+PATH_4+PARAM_SELLER_ID)
    Observable<NotesResponseGroup.GetNotes> getNotesBySellerId(@Path(SELLER_ID) String sellerId);

    @POST(Initial.API+PATH_1+PATH_3)
    Observable<NotesResponseGroup.CreateNote> create(@Body HashMap<String, String> hashMap);

    @POST(Initial.API+PATH_1+PATH_3+PARAM_NOTE_ID)
    Observable<NotesResponseGroup.CreateNote> update(@Path(NOTE_ID)long noteId, @Body HashMap<String, String> hashMap);


    @POST(Initial.API+PATH_1+PATH_5+PARAM_NOTE_ID)
    Observable<NotesResponseGroup.DeleteNote> delete(@Path(NOTE_ID) long noteId);


}
