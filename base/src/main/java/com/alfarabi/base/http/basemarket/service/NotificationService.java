package com.alfarabi.base.http.basemarket.service;

import com.alfarabi.base.model.notification.Notification;
import com.alfarabi.base.tools.Initial;

import java.util.ArrayList;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by User on 02/07/2017.
 */

public interface NotificationService {


    static final String PATH_1 = "/notifications";
    static final String PATH_2 = "/notification";

    static final String LIST = "/notifications";
    static final String ID = "id";
    static final String PARAM_ID = "/{"+ID+"}";

    @GET(Initial.API_VERSION+PATH_1+LIST)
    Observable<ArrayList<Notification>> getNotifications();

    @GET(Initial.API_VERSION+PATH_1+PARAM_ID+PATH_2)
    Observable<Notification> getNotification(@Path(ID) int id);

}
