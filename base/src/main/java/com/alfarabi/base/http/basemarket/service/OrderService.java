package com.alfarabi.base.http.basemarket.service;

import com.alfarabi.base.http.basemarket.response.OrderResponseGroup;
import com.alfarabi.base.http.basemarket.response.OrderStatusResponseGroup;
import com.alfarabi.base.http.basemarket.response.ShippingResponseGroup;
import com.alfarabi.base.tools.Initial;

import java.util.HashMap;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Alfarabi on 9/22/17.
 */

public interface OrderService {

    static final String PATH_1 = "/shop";
    static final String PATH_2 = "/order_list";
    static final String PATH_3 = "/order_detail";
    static final String PATH_4 = "/change_shipping_status";
    static final String PATH_5 = "/transaction_summary";
    static final String PATH_6 = "/download_invoice";

    static final String PAGE = "page";
    static final String LIST = "/carts";
    static final String PRODUCT_ID = "product_id";
    static final String SHIPPING_STATUS = "shipping_status";
    static final String PARAM_ID = "/{"+PRODUCT_ID+"}";

    static final String ORDER_ID = "order_id";
    static final String PARAM_ORDER_ID = "/{"+ORDER_ID+"}";

    static final String DEAL_CODE_NUMBER = "deal_code_number";
    static final String PARAM_DEAL_CODE_NUMBER = "/{"+DEAL_CODE_NUMBER+"}";


    @GET(Initial.API+PATH_1+PATH_2) Observable<OrderResponseGroup.OrderListResponse> getOrders(@Query(PAGE) int page);

    @GET(Initial.API+PATH_1+PATH_2) Observable<OrderResponseGroup.OrderListResponse> getOrders(@Query(PAGE) int page, @Query(SHIPPING_STATUS) String status);

    @GET(Initial.API+PATH_1+PATH_3+PARAM_ORDER_ID) Observable<OrderResponseGroup.OrderDetailResponse> getOrderDetail(@Path(ORDER_ID) String orderId);

    @POST(Initial.API+PATH_1+PATH_4+PARAM_ORDER_ID) Observable<OrderResponseGroup.ChangeStatusResponse> changeOrderStatus(@Path(ORDER_ID) String orderId, @Body HashMap<String, String> hashMap);


    @GET(Initial.API+PATH_1+PATH_5) Observable<OrderStatusResponseGroup.SummaryStatusResponse> getSummaryStatus();

    @GET(Initial.API+PATH_1+PATH_6+PARAM_DEAL_CODE_NUMBER) Observable<ResponseBody> downloadInvoice(@Path(DEAL_CODE_NUMBER) String dealCodeNumber);


//    @GET(Initial.API+PATH_1+PATH_6+PARAM_DEAL_CODE_NUMBER) Observable<ResponseBody> downloadIncoice(@Path(DEAL_CODE_NUMBER) String dealCodeNumber);


}
