package com.alfarabi.base.http.basemarket.service;

import com.alfarabi.base.http.basemarket.response.Response;
import com.alfarabi.base.tools.Initial;

import java.util.HashMap;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Alfarabi on 9/27/17.
 */

public interface PhoneVerificationService {

    static final String PATH_1 = "/microsite";
    static final String PATH_2 = "/request";
    static final String PATH_3 = "/verify";

    static final String LIST = "/products";
    static final String ID = "id";
    static final String PARAM_ID = "/{"+ID+"}";
    static final String OTP_CODE = "otp_code";
    static final String PARAM_OTP_CODE = "/{"+OTP_CODE+"}";

    @POST(Initial.PW_API+PATH_1+PATH_2)
    Observable<Response> requestSmsVerification(@Body HashMap<String, String> hashMap);

    @GET(Initial.PW_API+PATH_1+PATH_3+PARAM_OTP_CODE)
    Observable<Response> verifiyCode(@Path(OTP_CODE) String otpCode);

}
