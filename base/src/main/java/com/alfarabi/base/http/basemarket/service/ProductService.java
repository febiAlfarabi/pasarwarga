package com.alfarabi.base.http.basemarket.service;

import android.support.annotation.Nullable;

import com.alfarabi.base.http.basemarket.response.ProductResponseGroup;
import com.alfarabi.base.model.FormProduct;
import com.alfarabi.base.model.Product;
import com.alfarabi.base.tools.Initial;

import java.util.ArrayList;
import java.util.HashMap;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by User on 02/07/2017.
 */

public interface ProductService {

//    https://www.pasarwarga.com/api/product?category=0&page=1&min_price=10000&max_price=999999&sort_by=newest

//    categories=71%2C73%2C75&cities=BEKASI%2CJAKARTA&shipping_methods=jne%2Cinternal&page=1&min_price=10000&max_price=999999&sort_by=newest


    static final String PATH_1 = "/product";
    static final String PATH_2 = "/details";
    static final String PATH_3 = "/search";
    static final String PATH_4 = "/add";
    static final String PATH_5 = "/update";
    static final String PATH_6 = "/change_publish_status";
    static final String PATH_7 = "/kreditplus";
    static final String PATH_8 = "/tenor";

    static final String LIST = "/products";
    static final String ID = "id";
    static final String PRODUCT_ID = "product_id";
    static final String CATEGORY = "category";
    static final String PAGE = "page";
    static final String MIN_PRICE = "min_price";
    static final String MAX_PRICE = "max_price";
    static final String SORT_BY = "sort_by";
    static final String DEALDAY = "dealday";
    static final String KEYWORD = "keyword";

    static final String CATEGORIES = "categories";
    static final String CITIES = "cities";
    static final String SHIPPING_METHODS = "shipping_methods";


    static final String PARAM_ID = "/{" + ID + "}";
    static final String PARAM_PRODUCT_ID = "/{" + PRODUCT_ID + "}";
    static final String PARAM_CATEGORY = "/{" + CATEGORY + "}";
    static final String PARAM_PAGE = "/{" + PAGE + "}";
    static final String PARAM_MIN_PRICE = "/{" + MIN_PRICE + "}";
    static final String PARAM_MAX_PRICE = "/{" + MAX_PRICE + "}";
    static final String PARAM_SORT_BY = "/{" + SORT_BY + "}";
    static final String PARAM_CATEGORIES = "/{" + CATEGORIES + "}";
    static final String PARAM_CITIES = "/{" + CITIES + "}";
    static final String PARAM_SHIPPING_METHODS = "/{" + SHIPPING_METHODS+ "}";



    @GET(Initial.API + PATH_1 + LIST)
    Observable<ArrayList<Product>> getProducts();

    @GET(Initial.API + PATH_1 + PATH_2 + PARAM_ID)
    Observable<ProductResponseGroup.ProductResponse> getProduct(@Path(ID) long id);

    @GET(Initial.API + PATH_1)
    Observable<ProductResponseGroup.ProductsResponse> getProducts(@Query(CATEGORY) long categoryId, @Query(PAGE) int page);

    @GET(Initial.API + PATH_1)
    Observable<ProductResponseGroup.ProductsResponse> getProducts(@Query(CATEGORY) long categoryId, @Query(PAGE) int page, @Query(MIN_PRICE) float minPrice, @Query(MAX_PRICE) float maxPrice);

    @GET(Initial.API + PATH_1)
    Observable<ProductResponseGroup.ProductsResponse> getProducts(@Query(CATEGORY) long categoryId, @Query(PAGE) int page, @Query(MIN_PRICE) float minPrice, @Query(MAX_PRICE) float maxPrice, @Query(SORT_BY) String sortby);

    @GET(Initial.API + PATH_1)
    Observable<ProductResponseGroup.ProductsResponse> getProductsBy(@Query(CATEGORY) long categoryId, @Query(PAGE) int page, @Query(SORT_BY) String sortby);

    @GET(Initial.API + PATH_3)
    Observable<ProductResponseGroup.ProductSearchResponse> search(@Query(KEYWORD) String search);

    @GET(Initial.API + PATH_1)
    Observable<ProductResponseGroup.ProductsResponse> getProducts(@Query(DEALDAY) String dealDay, @Query(CATEGORY) long categoryId, @Query(PAGE) int page);

    @GET(Initial.API + PATH_1)
    Observable<ProductResponseGroup.ProductsResponse> getProducts(@Query(DEALDAY) String dealDay, @Query(CATEGORY) long categoryId, @Query(PAGE) int page, @Query(MIN_PRICE) float minPrice,
                                                                  @Query(MAX_PRICE) float maxPrice, @Query(SORT_BY) String sortby);


    @GET(Initial.API + PATH_1)
    Observable<ProductResponseGroup.ProductsResponse> getProducts(@Query(DEALDAY) String dealDay,@Query(CATEGORIES) String categories, @Query(CITIES) String cities, @Query(SHIPPING_METHODS) String shippingmethods,
                                                                  @Query(MIN_PRICE) String minPrice, @Query(MAX_PRICE) String maxPrice, @Query(SORT_BY) String sortby, @Query(PAGE) int page);

    @GET(Initial.API + PATH_1)
    Observable<ProductResponseGroup.ProductsResponse> getProducts(@Query(CATEGORIES) String categories, @Query(CITIES) String cities, @Query(SHIPPING_METHODS) String shippingmethods,
                                                                  @Query(MIN_PRICE) String minPrice, @Query(MAX_PRICE) String maxPrice, @Query(SORT_BY) String sortby, @Query(PAGE) int page);


    @GET(Initial.API + PATH_1)
    Observable<ProductResponseGroup.ProductsResponse> getProductsBy(@Query(DEALDAY) String dealDay, @Query(CATEGORY) long categoryId, @Query(PAGE) int page, @Query(SORT_BY) String sortby);

    @POST(Initial.API + PATH_1+PATH_4)
    @Multipart Observable<ProductResponseGroup.AddProductResponse> addProduct(@Part("data") RequestBody data, @Part @Nullable MultipartBody.Part... photo);


    @POST(Initial.API + PATH_1+PATH_5+PARAM_ID)
    @Multipart Observable<ProductResponseGroup.AddProductResponse> editProduct(@Path(ID) long productId, @Part("data") RequestBody data, @Part @Nullable MultipartBody.Part... photo);

    @GET(Initial.API + PATH_1+PATH_6+PARAM_ID)
    Observable<ProductResponseGroup.ProductResponse> changePublishStatus(@Path(ID) long productId);


    @POST(Initial.API + PATH_7+PATH_8+PARAM_PRODUCT_ID)
    Observable<ProductResponseGroup.TenorResponse> getTenors(@Path(PRODUCT_ID) long productId, @Body HashMap hashMap);


}
