package com.alfarabi.base.http.basemarket.service;

import com.alfarabi.base.http.basemarket.response.CartResponseGroup;
import com.alfarabi.base.http.basemarket.response.OrderStatusResponseGroup;
import com.alfarabi.base.http.basemarket.response.PurchaseResponseGroup;
import com.alfarabi.base.model.transaction.Cart;
import com.alfarabi.base.model.transaction.finance.kreditplus.KPCheckoutParam;
import com.alfarabi.base.tools.Initial;

import java.util.ArrayList;
import java.util.HashMap;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Alfarabi on 9/19/17.
 */

public interface PurchaseService {

    static final String PATH_1 = "/order";
    static final String PATH_2 = "/purchase_list";
    static final String PATH_3 = "/purchase_details";
    static final String PATH_4 = "/purchase_summary";
    static final String PATH_5 = "/download_invoice";

    static final String ID = "id";
    static final String PARAM_ID = "/{" + ID + "}";
    static final String PAGE = "page";
    static final String SHIPPING_STATUS = "shipping_status";
    static final String DEAL_CODE_NUMBER = "deal_code_number";

    static final String PARAM_DEAL_CODE_NUMBER = "/{" + DEAL_CODE_NUMBER + "}";

    @GET(Initial.API + PATH_1 + PATH_2)
    Observable<PurchaseResponseGroup.PurchasesResponse> getPurchases();

    @GET(Initial.API + PATH_1 + PATH_2)
    Observable<PurchaseResponseGroup.PurchasesResponse> getPurchases(@Query(PAGE) int page, @Query(SHIPPING_STATUS) String status);

    @GET(Initial.API + PATH_1 + PATH_3 + PARAM_DEAL_CODE_NUMBER)
    Observable<PurchaseResponseGroup.PurchaseDetailResponse> getPurchaseDetail(@Path(DEAL_CODE_NUMBER) String dealCodeNumber);

    @GET(Initial.API + PATH_1 + PATH_4)
    Observable<PurchaseResponseGroup.PurchaseStatusResponse> getPurchaseSummaryStatus();

    @GET(Initial.API + PATH_1 + PATH_5 + PARAM_DEAL_CODE_NUMBER)
    Observable<ResponseBody> downloadIncoice(@Path(DEAL_CODE_NUMBER) String dealCodeNumber);

}
