package com.alfarabi.base.http.basemarket.service;

import com.alfarabi.base.http.basemarket.response.ReviewResponseGroup;
import com.alfarabi.base.tools.Initial;

import java.util.HashMap;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by yudapratama on 8/21/2017.
 */

public interface ReviewService {

    public static final String PAGE = "page";
    public static final String ORDER = "order";

    static final String PATH_1 = "/product";
    static final String PATH_2 = "/reviews";
    static final String PATH_3 = "/feedback_able";
    static final String PATH_4 = "/feedback_list";
    static final String PATH_5 = "/feedback_seller_list";
    static final String PATH_6 = "/add_feedback";
    static final String PATH_7 = "/feedback_summary";

    static final String LIST = "/logins";
    static final String ID = "id";
    static final String DEAL_CODE_NUMBER = "deal_code_number";
    static final String PARAM_ID = "/{"+ID+"}";
    static final String PARAM_DEAL_CODE_NUMBER = "/{"+DEAL_CODE_NUMBER+"}";

    @GET(Initial.PW_API+PATH_1+PATH_2+PARAM_ID)
    public Observable<ReviewResponseGroup> getReviews(@Path(ID) long id , @Query(PAGE) int page);


    @GET(Initial.PW_API+PATH_2+PATH_3)
    public Observable<ReviewResponseGroup.FeedbackableResponse> feedbackables(@Query(PAGE) int page);

    @GET(Initial.PW_API+PATH_2+PATH_4)
    public Observable<ReviewResponseGroup.Feedbacks> feedbacks(@Query(PAGE) int page);

    @GET(Initial.PW_API+PATH_2+PATH_5)
    public Observable<ReviewResponseGroup.FeedbackAsSeller> feedbackAsSeller(@Query(PAGE) int page);

    @POST(Initial.PW_API+PATH_2+PATH_6+PARAM_DEAL_CODE_NUMBER)
    public Observable<ReviewResponseGroup.AddFeedback> addFeedback(@Path(DEAL_CODE_NUMBER) String dealCodeNumber, @Body HashMap<String, String> hashMap);

    @GET(Initial.PW_API+PATH_2+PATH_7)
    public Observable<ReviewResponseGroup.FeedbackCount> feedbackableCount();


}
