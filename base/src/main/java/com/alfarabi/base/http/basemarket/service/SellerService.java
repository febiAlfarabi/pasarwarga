package com.alfarabi.base.http.basemarket.service;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.alfarabi.base.http.basemarket.response.Response;
import com.alfarabi.base.http.basemarket.response.SellerResponseGroup;
import com.alfarabi.base.tools.Initial;

import java.util.HashMap;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by User on 02/07/2017.
 */

public interface SellerService {


    static final String PATH_1 = "/shop";
    static final String PATH_2 = "/register";
    static final String PATH_3 = "/details";
    static final String PATH_4 = "/update_shop_info";
    static final String PATH_5 = "/check_admin_activation";
    static final String PATH_6 = "/search";
    static final String PATH_7 = "/shop";
    static final String PATH_8 = "/user";
    static final String PATH_9 = "/change_favorite_shop";
    static final String PATH_10 = "/favorite_shop_list";
    static final String PATH_11 = "/update_shop_location";
    static final String PATH_12 = "/send_complaint";




    static final String KEYWORD = "keyword";

    static final String LIST = "/categories";
    static final String ID = "id";
    static final String SELLER_ID = "seller_id";
    static final String PARAM_ID = "/{"+ID+"}";
    static final String PARAM_SELLER_ID = "/{"+SELLER_ID+"}";

    @POST(Initial.API+PATH_1+PATH_2) Observable<SellerResponseGroup.RegisterResponse> register(@Body HashMap<String, String> hashMap);

    @GET(Initial.API+PATH_1+PATH_5+PARAM_SELLER_ID) Observable<SellerResponseGroup.AdminActivationResponse> checkAdminActivation(@Path(SELLER_ID) String sellerId);


    @POST(Initial.API+PATH_1+PATH_4)
    @Multipart Observable<SellerResponseGroup.UpdateShopResponse> updateShopInfo(@Part("data") RequestBody data, @Part @Nullable MultipartBody.Part... photo);

    @GET(Initial.API+PATH_1+PATH_3+PARAM_SELLER_ID)
    Observable<SellerResponseGroup.DetailResponse> sellerDetail(@NonNull@Path(SELLER_ID) long sellerId);

    @GET(Initial.API+PATH_1+PATH_3+PARAM_SELLER_ID)
    Observable<SellerResponseGroup.DetailResponse> sellerDetail(@NonNull@Path(SELLER_ID) long sellerId, @NonNull@Query("page") int page);

    @GET(Initial.API+PATH_1+PATH_3+PARAM_SELLER_ID)
    Observable<SellerResponseGroup.DetailResponse> sellerDetail(@NonNull@Path(SELLER_ID) long sellerId, @NonNull@Query("page") int page, @Query("sort_by") String sortBy);

    @GET(Initial.API+PATH_1+PATH_3+PARAM_SELLER_ID)
    Observable<SellerResponseGroup.DetailResponse> sellerDetail(@NonNull@Path(SELLER_ID) long sellerId, @NonNull@Query("page") int page, @Query("sort_by") String sortBy, @Query("keyword") String keyword);

    @GET(Initial.API+PATH_1+PATH_3+PARAM_SELLER_ID)
    Observable<SellerResponseGroup.DetailResponse> sellerDetail(@NonNull@Path(SELLER_ID) long sellerId, @NonNull@Query("page") int page, @Query("category") long category, @Query("sort_by") String sortBy, @Query("keyword") String keyword);


    @GET(Initial.API+PATH_1+PATH_3+PARAM_SELLER_ID)
    Observable<SellerResponseGroup.DetailResponse> sellerDetail(@NonNull@Path(SELLER_ID) long sellerId, @NonNull@Query("page") int page, @NonNull@Query("category") long category,
                                                                @Query("min_price") float minPrice, @Query("max_price") float maxPrice, @Query("sort_by") String sortBy);

    @GET(Initial.API + PATH_6 + PATH_7)
    Observable<SellerResponseGroup.SearchResponse> search(@Query(KEYWORD) String search);

    @GET(Initial.API + PATH_8 + PATH_9+PARAM_SELLER_ID)
    Observable<SellerResponseGroup.SellerFavoriteResponse> favorite(@NonNull@Path(SELLER_ID) long sellerId);

    @GET(Initial.API + PATH_8 + PATH_10)
    Observable<SellerResponseGroup.ListSellerFavoriteResponse> listfavorite();


    @POST(Initial.API + PATH_1 + PATH_11)
    Observable<SellerResponseGroup.UpdateShopLocationInfo> updateShopLocation(@Body HashMap<String, String > hashMap);

    @POST(Initial.API + PATH_8 + PATH_12) Observable<Response> report(@Body HashMap<String, String > hashMap);

}
