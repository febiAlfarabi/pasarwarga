package com.alfarabi.base.http.basemarket.service;

import android.support.annotation.Nullable;

import com.alfarabi.base.http.basemarket.response.SettingResponseGroup;
import com.alfarabi.base.model.User;
import com.alfarabi.base.tools.Initial;

import java.util.HashMap;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by USER on 9/14/2017.
 */

public interface SettingService {

    static final String PATH_1 = "/settings";
    static final String PATH_2 = "/change_email";
    static final String PATH_3 = "/change_password";
    static final String PATH_4 = "/deactive_account";

    static final String PATH_5 = "/user";
    static final String PATH_6 = "/update_name";
    static final String PATH_7 = "/upload_profile_picture";
    static final String PATH_8 = "/profile_picture";
    static final String PATH_9 = "/change_phone";
    static final String PATH_10 = "/update_profile_info";

    //change email & phone
    static final String PASSWORD = "password";
    static final String EMAIL = "email";
    static final String NEW_EMAIL = "new_email";
    static final String PHONE = "phone";
    static final String NEW_PHONE = "new_phone";

    //change password
    static final String EMAIL_OR_USERNAME = "email_or_username";
    static final String NEW_PASSWORD = "new_password";

    //update user's name
    static final String FIRST_NAME = "first_name";
    static final String LAST_NAME = "last_name";

    //update profile picture
    static final String PHOTO = "photo";

    //update profile info
    static final String COUNTRY_ID = "country_id";
    static final String STATE_ID = "state_id";
    static final String CITY_ID = "city_id";
    static final String KECAMATAN_ID = "kecamatan_id";
    static final String KELURAHAN_ID = "kelurahan_id";
    static final String ZIPCODE = "zip_code";
    static final String BIRTHDAY = "birthday";
    static final String GENDER = "gender";
    static final String ABOUT = "about";

    @POST(Initial.API + PATH_1 + PATH_2)
    Observable<SettingResponseGroup.ChangeEmailAddress> changeEmail(@Body HashMap<String, String> hashMap);

    @POST(Initial.API + PATH_1 + PATH_9)
    Observable<SettingResponseGroup.ChangePhoneNumber> changePhone(@Body HashMap<String, String> hashMap);

    @POST(Initial.API + PATH_1 + PATH_3)
    Observable<SettingResponseGroup.ChangePassword> changePassword(@Body HashMap<String, String> hashMap);

    @GET(Initial.API + PATH_1 + PATH_4)
    Observable<SettingResponseGroup.DeactiveAccount> deactiveAccount();

    @POST(Initial.API + PATH_5 + PATH_7)
    @Multipart
    Observable<SettingResponseGroup.ChangeProfilePicture> changeProfilePicture(@Part @Nullable MultipartBody.Part photo);

    @GET(Initial.API + PATH_5 + PATH_8)
    Observable<SettingResponseGroup.ChangeProfilePicture> getCurrentProfilePicture();

    @POST(Initial.API + PATH_5 + PATH_6)
    Observable<SettingResponseGroup.ChangeProfileName> changeProfileName(@Body HashMap<String, String> hashMap);

    @POST(Initial.API + PATH_5 + PATH_10)
    Observable<SettingResponseGroup.ChangeProfileInfo> changeProfileInfo(@Body HashMap<String, String> hashMap);
}
