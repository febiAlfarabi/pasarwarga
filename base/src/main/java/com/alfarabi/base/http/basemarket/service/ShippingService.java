package com.alfarabi.base.http.basemarket.service;

import com.alfarabi.base.http.basemarket.response.ShippingResponseGroup;
import com.alfarabi.base.tools.Initial;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by User on 02/07/2017.
 */

public interface ShippingService {


    static final String PATH_1 = "/cart";
    static final String PATH_2 = "/get_shipping_services";
    static final String PATH_3 = "/shipping_services";
    static final String PATH_4 = "/all";

    static final String LIST = "/carts";
    static final String PRODUCT_ID = "product_id";
    static final String PARAM_ID = "/{"+PRODUCT_ID+"}";


    static final String ADDRESS_ID = "address_id";
    static final String PARAM_ADDRESS_ID = "/{"+ADDRESS_ID+"}";


    @GET(Initial.API+PATH_1+PATH_2+PARAM_ID) Observable<ShippingResponseGroup.AvailableShipping> availableShipping(@Path(PRODUCT_ID) String productId);
    @GET(Initial.API+PATH_1+PATH_2+PARAM_ID+PARAM_ADDRESS_ID) Observable<ShippingResponseGroup.AvailableShipping> availableShipping(@Path(PRODUCT_ID) String productId, @Path(ADDRESS_ID) String addressId);

    @GET(Initial.API+PATH_3+PATH_4) Observable<ShippingResponseGroup.ShippingResponse> shippingSevices();
//    @GET(Initial.API+PATH_3+PATH_4+PARAM_ADDRESS_ID) Observable<ShippingResponseGroup.ShippingResponse> shippingSevices(@Path(ADDRESS_ID) long addressId);


}
