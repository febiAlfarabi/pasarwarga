package com.alfarabi.base.http.basemarket.service;

import com.alfarabi.base.http.basemarket.response.OrderResponseGroup;
import com.alfarabi.base.http.basemarket.response.TrackingResponseGroup;
import com.alfarabi.base.tools.Initial;

import java.util.HashMap;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Alfarabi on 9/22/17.
 */

public interface TrackingService {

    static final String PATH_1 = "/order";
    static final String PATH_2 = "/trace_tracking";

    static final String PAGE = "page";
    static final String LIST = "/carts";
    static final String PRODUCT_ID = "product_id";
    static final String SHIPPING_STATUS = "shipping_status";
    static final String PARAM_ID = "/{"+PRODUCT_ID+"}";

    @POST(Initial.API+PATH_1+PATH_2) Observable<TrackingResponseGroup.TrackingListResponse> tracking(@Body HashMap<String, String> hashMap);


}
