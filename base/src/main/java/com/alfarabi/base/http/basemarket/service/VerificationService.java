package com.alfarabi.base.http.basemarket.service;

import com.alfarabi.base.http.basemarket.response.VerificationRequestGroup;
import com.alfarabi.base.tools.Initial;

import java.util.HashMap;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by User on 02/07/2017.
 */

public interface VerificationService {


    static final String PATH_1 = "/sms_otp";
    static final String PATH_2 = "/request";
    static final String PATH_3 = "/verify";

    static final String LIST = "/carts";
    static final String ID = "id";
    static final String PARAM_ID = "/{"+ID+"}";

    @POST(Initial.API+PATH_1+PATH_2)
    Observable<VerificationRequestGroup.Request> request(@Body HashMap<String, String> hashMap);

    @POST(Initial.API+PATH_1+PATH_3)
    Observable<VerificationRequestGroup.Verify> verify(@Body HashMap<String, String> hashMap);

}
