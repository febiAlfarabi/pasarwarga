package com.alfarabi.base.http.test;

import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * Created by Alfarabi on 6/16/17.
 */

public interface GoogleDotCom {

    @GET("/") Observable<String> request();
}
