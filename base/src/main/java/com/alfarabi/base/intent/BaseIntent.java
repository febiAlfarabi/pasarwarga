package com.alfarabi.base.intent;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;

public abstract class BaseIntent extends Intent {

    BaseIntent(String action) {
        super(action);
    }

    BaseIntent(String action, Uri uri) {
        super(action, uri);
    }

    public boolean isAvailable(@NonNull Context context) {
        return resolveActivity(context.getPackageManager()) != null;
    }
}