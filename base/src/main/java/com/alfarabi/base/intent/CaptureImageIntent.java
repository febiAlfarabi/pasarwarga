package com.alfarabi.base.intent;

import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.util.Log;

import com.alfarabi.base.R;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

public final class CaptureImageIntent extends BaseIntent implements Serializable {

    private static final String TAG = "Intent";

    private Uri result;
    private File tempFile ;
    private static CaptureImageIntent instance ;
    private Context context ;

    public static CaptureImageIntent getInstance(Context context){
        if(instance==null){
            instance = new CaptureImageIntent(context);
        }
        return instance ;
    }

    private CaptureImageIntent(@NonNull Context context) {
        super(MediaStore.ACTION_IMAGE_CAPTURE);
        this.context = context ;
        this.tempFile = createTempFile(context);
        this.result = null;
        if (this.tempFile == null) {
            throw new RuntimeException(context.getString(R.string.invalid_configuration));
        } else {
            this.result = Uri.fromFile(tempFile);
            try{
                putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(context, context.getPackageName() + ".fileprovider", tempFile));
                List<ResolveInfo> resInfoList = context.getPackageManager().queryIntentActivities(this, PackageManager.MATCH_DEFAULT_ONLY);
                for (ResolveInfo resolveInfo : resInfoList) {
                    String packageName = resolveInfo.activityInfo.packageName;
                    context.grantUriPermission(packageName, result, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }


    @NonNull
    public static Uri[] extract(@NonNull Intent data) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN && data.getClipData() != null) {
            final ClipData clipData = data.getClipData();
            final Uri[] results = new Uri[clipData.getItemCount()];
            for (int i = 0; i < clipData.getItemCount(); i++)
                results[i] = clipData.getItemAt(i).getUri();
            return results;
        } else {
            return new Uri[]{data.getData()};
        }
    }

    public Uri getResult(Intent... intents) {
//        if(result!=null){
//            return result ;
//        }
//        // result == null ;

//        if(intents!=null){
//            result = intents[0].getData();
//        }else if(intents==null){
//            if(result==null){
//                RuntimeException re = new RuntimeException("Result Or Passed Intent null");
//                re.printStackTrace();
//                throw re;
//            }
//
//        }
        return Uri.fromFile(tempFile);
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private static File createTempFile(@NonNull Context context) {
        try {
            final File parent = new File(Environment.getExternalStorageDirectory(), context.getString(R.string.fileprovider_parent));
            if (parent.exists()) {
                Log.d(TAG + " [1/3]", "parent folder already loadOrCreate, skipping...");
            } else {
                Log.d(TAG + " [1/3]", "parent folder not found, creating...");
                parent.mkdirs();
            }

            final File file = new File(parent, context.getString(R.string.fileprovider_child));
            if (file.exists()) {
                file.delete();
                Log.d(TAG + " [2/3]", "temp file found, deleting...");
            } else {
                Log.d(TAG + " [2/3]", "temp file does not exist, skipping...");
            }

            final boolean isFileCreated = file.createNewFile();
            Log.d(TAG + " [3/3]", isFileCreated ? "temp file created!" : "unable to create file!");
            return file;
        } catch (IOException ignored) {
            ignored.printStackTrace();
            return null;
        } catch (Exception ignored) {
            ignored.printStackTrace();
            return null;
        }
    }
}