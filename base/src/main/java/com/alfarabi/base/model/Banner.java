package com.alfarabi.base.model;

import com.alfarabi.alfalibs.helper.model.ObjectAdapterInterface;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import io.realm.BannerRealmProxy;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 6/30/17.
 */

@Parcel(value = Parcel.Serialization.BEAN, implementations = {BannerRealmProxy.class}, analyze = {Banner.class})
public class Banner extends RealmObject implements ObjectAdapterInterface{

    @PrimaryKey@SerializedName("id")@Getter@Setter long id ;
    @SerializedName("title")@Getter@Setter String title ;
    @SerializedName("image_url")@Getter@Setter String imageUrl ;
    @SerializedName("url")@Getter@Setter String url ;


    @Override
    public long getHeaderId() {
        return id;
    }

    @Override
    public boolean isSearchable() {
        return false;
    }

    @Override
    public String canSearchByField() {
        return "name";
    }
}
