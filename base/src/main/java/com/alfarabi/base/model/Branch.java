package com.alfarabi.base.model;

import com.alfarabi.base.view.interfaze.DataSpinnerInterface;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.Date;

import io.realm.BranchRealmProxy;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 7/20/17.
 */

@Parcel(implementations = {BranchRealmProxy.class}, value = Parcel.Serialization.BEAN, analyze = {Branch.class})
public class Branch extends RealmObject implements DataSpinnerInterface<Branch> {

    @PrimaryKey
    @SerializedName("id")@Getter @Setter String id ;
    @SerializedName("createdby")@Getter@Setter long createdby ;
    @SerializedName("created")@Getter@Setter Date created  = new Date();
    @SerializedName("updatedby")@Getter@Setter long updatedy ;
    @SerializedName("updated")@Getter@Setter Date updated = new Date();
    @SerializedName("name")@Getter@Setter String name ;

    @Override
    public String spinnerLabel() {
        return name;
    }

    @Override
    public boolean equal(Branch city) {
        if(id==city.getId()){
            return true ;
        }
        return false;
    }
}
