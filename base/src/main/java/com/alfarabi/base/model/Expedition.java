package com.alfarabi.base.model;

import com.alfarabi.alfalibs.helper.model.ObjectAdapterInterface;
import com.alfarabi.base.view.interfaze.DataSpinnerInterface;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;
import io.realm.ExpeditionRealmProxy;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 8/15/17.
 */

@Parcel(value = Parcel.Serialization.BEAN, implementations = {ExpeditionRealmProxy.class}, analyze = {Expedition.class})
public class Expedition extends RealmObject implements ObjectAdapterInterface, DataSpinnerInterface<Expedition> {

    @PrimaryKey @SerializedName("id")@Getter @Setter long id ;
    @SerializedName("name")@Getter@Setter String name ;
    @SerializedName("price")@Getter@Setter float price ;


    @Override
    public long getHeaderId() {
        return 0;
    }

    @Override
    public boolean isSearchable() {
        return false;
    }

    @Override
    public String canSearchByField() {
        return null;
    }


    @Override
    public String spinnerLabel() {
        return name;
    }

    @Override
    public boolean equal(Expedition expedition) {
        return id!=0 && id==expedition.getId();
    }
}
