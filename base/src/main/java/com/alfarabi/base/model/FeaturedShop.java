package com.alfarabi.base.model;

import com.alfarabi.alfalibs.helper.model.ObjectAdapterInterface;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import io.realm.FeaturedShopRealmProxy;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 8/15/17.
 */

@Parcel(value = Parcel.Serialization.BEAN, implementations = {FeaturedShopRealmProxy.class}, analyze = {FeaturedShop.class})
public class FeaturedShop extends RealmObject implements ObjectAdapterInterface {


    @PrimaryKey @SerializedName("id")@Getter @Setter long id ;
    @SerializedName("shop_name")@Getter@Setter String shopName ;
    @SerializedName("address")@Getter@Setter String address ;
    @SerializedName("location")@Getter@Setter Location location ;
    @SerializedName("shop_location")@Getter@Setter ShopLocation shopLocation ;
    @SerializedName("shop_ratting")@Getter@Setter String shopRatting ;
    @SerializedName("review_count")@Getter@Setter String reviewCount ;
    @SerializedName("store_image")@Getter@Setter String storeImage ;
    @SerializedName("banner_image")@Getter@Setter String bannerImage ;
    @SerializedName("seller_email")@Getter@Setter String sellerEmail ;
    @SerializedName("seller_name")@Getter@Setter String sellerName ;


    @Override
    public long getHeaderId() {
        return 0;
    }

    @Override
    public boolean isSearchable() {
        return false;
    }

    @Override
    public String canSearchByField() {
        return null;
    }
}
