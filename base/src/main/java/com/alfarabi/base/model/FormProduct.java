package com.alfarabi.base.model;

import com.alfarabi.alfalibs.helper.model.ObjectAdapterInterface;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 9/28/17.
 */

@Parcel
public class FormProduct {


    @SerializedName("id")@Getter@Setter long id ;
    @SerializedName("product_images")@Getter@Setter String productImage ;
    @SerializedName("name")@Getter@Setter String name ;
    @SerializedName("category_id")@Getter@Setter long categoryId ;
    @SerializedName("base_price")@Getter@Setter float basePrice = 0 ;
    @SerializedName("discount")@Getter@Setter int discount ;
    @SerializedName("start_discount")@Getter@Setter String startDiscount ;
    @SerializedName("end_discount")@Getter@Setter String endDiscount ;
    @SerializedName("description")@Getter@Setter String description ;
    @SerializedName("weight")@Getter@Setter int weight ;
    @SerializedName("quantity")@Getter@Setter int quantity ;
    @SerializedName("shipping_method_ids")@Getter@Setter String shippingMethodIds = "";
    @SerializedName("tags")@Getter@Setter String tags ;
    @SerializedName("insurance")@Getter@Setter boolean insurance ;
    @SerializedName("is_publish")@Getter@Setter boolean publish ;


}
