package com.alfarabi.base.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.Date;

import io.realm.ImageRealmProxy;
import io.realm.RealmObject;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 7/11/17.
 */

@Parcel(implementations = {ImageRealmProxy.class}, value = Parcel.Serialization.BEAN, analyze = {Image.class})
public class Image extends RealmObject {

    @SerializedName("id")@Getter @Setter long id ;
    @SerializedName("created")@Getter@Setter
    Date created  = new Date();
    @SerializedName("updated")@Getter@Setter
    Date updated = new Date();
    @SerializedName("url")@Getter@Setter
    String url ;
}
