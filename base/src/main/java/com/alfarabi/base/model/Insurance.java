package com.alfarabi.base.model;

import com.alfarabi.alfalibs.helper.model.ObjectAdapterInterface;
import com.alfarabi.base.BaseApplication;
import com.alfarabi.base.R;
import com.alfarabi.base.view.interfaze.DataSpinnerInterface;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.text.DecimalFormat;
import java.util.Date;

import io.realm.InsuranceRealmProxy;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 8/28/17.
 */

@Parcel(value = Parcel.Serialization.BEAN, implementations = {InsuranceRealmProxy.class}, analyze = {Insurance.class})
public class Insurance extends RealmObject implements ObjectAdapterInterface, DataSpinnerInterface<Insurance>{
//    {
//        "id": "1",
//            "name": "Tidak",
//            "value": 0
//    }

    @PrimaryKey @SerializedName("id")@Getter @Setter long id ;
    @SerializedName("createdby")@Getter@Setter long createdby ;
    @SerializedName("created")@Getter@Setter Date created  = new Date();
    @SerializedName("updatedby")@Getter@Setter long updatedy ;
    @SerializedName("updated")@Getter@Setter Date updated = new Date();

    @SerializedName("name")@Getter@Setter String name ;
    @SerializedName("value")@Getter@Setter float value ;

    @Override
    public long getHeaderId() {
        return 0;
    }

    @Override
    public boolean isSearchable() {
        return false;
    }

    @Override
    public String canSearchByField() {
        return null;
    }

    @Override
    public String spinnerLabel() {
        return name;
    }

    @Override
    public boolean equal(Insurance insurance) {
        return false;
    }
}
