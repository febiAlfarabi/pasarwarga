package com.alfarabi.base.model;

import com.alfarabi.base.model.location.Category;
import com.alfarabi.base.realm.GeneralRealmListParcelConverter;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import io.realm.LandingRealmProxy;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 8/15/17.
 */


@Parcel(value = Parcel.Serialization.BEAN, implementations = {LandingRealmProxy.class}, analyze = {Landing.class})
public class Landing extends RealmObject {

    @PrimaryKey@Getter@Setter private long id = 1;

    @Getter@SerializedName("categories") RealmList<Category> categories = new RealmList<>();
    @ParcelPropertyConverter(GeneralRealmListParcelConverter.class)
    public void setCategories(RealmList<Category> categories) {
        this.categories = categories;
    }

    @Getter@SerializedName("banner_sliders") RealmList<Banner> banners = new RealmList<>();
    @ParcelPropertyConverter(GeneralRealmListParcelConverter.class)
    public void setBanners(RealmList<Banner> banners) {
        this.banners = banners;
    }

    @Getter@SerializedName("featured_products") RealmList<Product> featuredProducts = new RealmList<>();
    @ParcelPropertyConverter(GeneralRealmListParcelConverter.class)
    public void setFeaturedProducts(RealmList<Product> featuredProducts) {
        this.featuredProducts = featuredProducts;
    }

    @Getter@SerializedName("today_deals") RealmList<Product> todayDeals = new RealmList<>();
    @ParcelPropertyConverter(GeneralRealmListParcelConverter.class)
    public void setTodayDeals(RealmList<Product> todayDeals) {
        this.todayDeals = todayDeals;
    }

    @Getter@SerializedName("latest_products") RealmList<Product> latestProducts = new RealmList<>();
    @ParcelPropertyConverter(GeneralRealmListParcelConverter.class)
    public void setLatestProducts(RealmList<Product> latestProducts) {
        this.latestProducts = latestProducts;
    }

    @Getter@SerializedName("featured_shops") RealmList<FeaturedShop> featuredShops = new RealmList<>();
    @ParcelPropertyConverter(GeneralRealmListParcelConverter.class)
    public void setFeaturedShops(RealmList<FeaturedShop> featuredShops) {
        this.featuredShops = featuredShops;
    }

    @Getter@SerializedName("medium_banners") RealmList<MediumBanner> mediumBanners = new RealmList<>();
    @ParcelPropertyConverter(GeneralRealmListParcelConverter.class)
    public void setMediumBanners(RealmList<MediumBanner> mediumBanners) {
        this.mediumBanners = mediumBanners;
    }
}
