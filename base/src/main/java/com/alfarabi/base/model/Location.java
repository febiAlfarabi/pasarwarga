package com.alfarabi.base.model;

import com.alfarabi.alfalibs.helper.model.ObjectAdapterInterface;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.Date;

import io.realm.LocationRealmProxy;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 8/15/17.
 */

@Parcel(value = Parcel.Serialization.BEAN, implementations = {LocationRealmProxy.class}, analyze = {Location.class})
public class Location extends RealmObject implements ObjectAdapterInterface {

    @PrimaryKey @SerializedName("id")@Getter @Setter long id ;
    @Getter@Setter@SerializedName("created") Date created ;
    @Getter@Setter@SerializedName("updated") Date updated ;

    @Getter@Setter float latitude ;
    @Getter@Setter float longitude ;



    @Override
    public long getHeaderId() {
        return 0;
    }

    @Override
    public boolean isSearchable() {
        return false;
    }

    @Override
    public String canSearchByField() {
        return null;
    }
}
