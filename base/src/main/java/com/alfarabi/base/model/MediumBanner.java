package com.alfarabi.base.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import io.realm.MediumBannerRealmProxy;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 10/12/17.
 */

@Parcel(value = Parcel.Serialization.BEAN, implementations = {MediumBannerRealmProxy.class}, analyze = {MediumBanner.class})
public class MediumBanner extends RealmObject {

    @PrimaryKey@SerializedName("id")@Getter@Setter long id ;
    @SerializedName("title")@Getter@Setter String title ;
    @SerializedName("image_url")@Getter@Setter String imageUrl ;
    @SerializedName("url")@Getter@Setter String url ;
    @SerializedName("type")@Getter@Setter String type ;
    @SerializedName("event_id")@Getter@Setter String eventId ;

    @SerializedName("created_at")@Getter@Setter String createdAt ;

}
