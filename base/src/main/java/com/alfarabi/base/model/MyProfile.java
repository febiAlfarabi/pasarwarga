package com.alfarabi.base.model;

import com.alfarabi.alfalibs.helper.model.ObjectAdapterInterface;

import org.parceler.Parcel;

import io.realm.RealmObject;
import io.realm.MyProfileRealmProxy;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 7/26/17.
 */

@Parcel(value = Parcel.Serialization.BEAN, implementations = {MyProfileRealmProxy.class}, analyze = {MyProfile.class})
public class MyProfile extends RealmObject implements ObjectAdapterInterface {

    @Getter@Setter User user ;

    @Override
    public long getHeaderId() {
        return 0;
    }

    @Override
    public boolean isSearchable() {
        return false;
    }

    @Override
    public String canSearchByField() {
        return null;
    }
}
