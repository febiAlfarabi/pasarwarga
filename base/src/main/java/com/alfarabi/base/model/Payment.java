package com.alfarabi.base.model;

import com.alfarabi.alfalibs.helper.model.ObjectAdapterInterface;
import com.alfarabi.base.view.interfaze.DataSpinnerInterface;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.Date;

import io.realm.PaymentRealmProxy;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 7/20/17.
 */

@Parcel(implementations = {PaymentRealmProxy.class}, value = Parcel.Serialization.BEAN, analyze = {Payment.class})
public class Payment extends RealmObject implements ObjectAdapterInterface, DataSpinnerInterface<Payment>{

    @PrimaryKey
    @SerializedName("id")@Getter @Setter long id ;
    @SerializedName("createdby")@Getter@Setter long createdby ;
    @SerializedName("created")@Getter@Setter Date created  = new Date();
    @SerializedName("updatedby")@Getter@Setter long updatedy ;
    @SerializedName("updated")@Getter@Setter Date updated = new Date();
    @SerializedName("gateway_name")@Getter@Setter String gatewayName ;
    @SerializedName("is_enable")@Getter@Setter boolean enable ;

    @Override
    public long getHeaderId() {
        return 0;
    }

    @Override
    public boolean isSearchable() {
        return false;
    }

    @Override
    public String canSearchByField() {
        return null;
    }

    @Override
    public String spinnerLabel() {
        return gatewayName;
    }

    @Override
    public boolean equal(Payment payment) {
        return payment.getId()==id;
    }
}
