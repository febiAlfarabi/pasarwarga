package com.alfarabi.base.model;

import com.alfarabi.alfalibs.helper.model.ObjectAdapterInterface;
import com.alfarabi.base.model.location.Category;
import com.alfarabi.base.model.review.Review;
import com.alfarabi.base.realm.GeneralRealmListParcelConverter;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import java.util.Date;

import io.realm.ProductRealmProxy;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 6/22/17.
 */

@Parcel(value = Parcel.Serialization.BEAN, implementations = {ProductRealmProxy.class}, analyze = {Product.class})
public class Product extends RealmObject implements ObjectAdapterInterface {



    @PrimaryKey @SerializedName("id")@Getter@Setter long id ;
    @SerializedName("createdby")@Getter@Setter long createdby ;
    @SerializedName("created")@Getter@Setter Date created  = new Date();
    @SerializedName("updatedby")@Getter@Setter long updatedy ;
    @SerializedName("updated")@Getter@Setter Date updated = new Date();

    @SerializedName("name")@Getter@Setter String name ;
    @SerializedName("image")@Getter@Setter String image = "";
    @SerializedName("images")@Getter@Setter String images ;
//    @SerializedName("big_images")@Getter@Setter String bigImages ;
    @SerializedName("base_price")@Getter@Setter float basePrice ;
    @SerializedName("price_after_discount")@Getter@Setter float priceAfterDiscount ;
    @SerializedName("discount")@Getter@Setter int discount ;
    @SerializedName("start_discount")@Getter@Setter String startDiscount ;
    @SerializedName("end_discount")@Getter@Setter String endDiscount ;
    @SerializedName("weight")@Getter@Setter int weight ;
    @SerializedName("quantity")@Getter@Setter int quantity ;
    @SerializedName("ratings")@Getter@Setter float ratings ;
    @SerializedName("is_favorite")@Getter@Setter boolean favourite ;
    @SerializedName("review_counts")@Getter@Setter double reviewCount ;
    @SerializedName("seo_url")@Getter@Setter String seoUrl  ;
    @SerializedName("description")@Getter@Setter String description  ;
    @SerializedName("tenors")@Getter RealmList<Tenor> tenors = new RealmList<>() ;
    @ParcelPropertyConverter(GeneralRealmListParcelConverter.class)
    public void setTenors(RealmList<Tenor> tenors) {
        this.tenors = tenors;
    }

//    @SerializedName("category_id")@Getter@Setter String categoryId ;
    @SerializedName("category")@Getter@Setter Category category ;
    @SerializedName("shop_name")@Getter@Setter String shopName ;

    @SerializedName("reviews")@Getter RealmList<Review> reviews = new RealmList<>();
    @ParcelPropertyConverter(GeneralRealmListParcelConverter.class)
    public void setReviews(RealmList<Review> reviews) {
        this.reviews = reviews;
    }

    @SerializedName("seller")@Getter@Setter Seller seller ;
    @SerializedName("insurance")@Getter@Setter boolean insurance ;

    @SerializedName("shipping_methods")@Getter RealmList<Shipping> shippings = new RealmList<>();
    @ParcelPropertyConverter(GeneralRealmListParcelConverter.class)
    public void setShipping(RealmList<Shipping> shippings) {
        this.shippings = shippings;
    }

    @SerializedName("is_publish")@Getter@Setter boolean publish ;

    @SerializedName("seourl")@Getter@Setter String completeSeourl;
    @SerializedName("tags")@Getter@Setter String tags ;
    @SerializedName("city")@Getter@Setter String city;


    @Override
    public long getHeaderId() {
        return 0;
    }

    @Override
    public boolean isSearchable() {
        return false;
    }

    @Override
    public String canSearchByField() {
        return "name";
    }
}
