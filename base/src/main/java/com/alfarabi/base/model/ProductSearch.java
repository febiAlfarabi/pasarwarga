package com.alfarabi.base.model;

import com.alfarabi.base.model.location.Category;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 8/21/17.
 */

public class ProductSearch {

    @Getter@Setter@SerializedName("specified") List<Specified> specifieds ;
    @Getter@Setter@SerializedName("categories") List<Category> categories ;



}
