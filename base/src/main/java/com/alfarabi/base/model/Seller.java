package com.alfarabi.base.model;

import com.alfarabi.alfalibs.helper.model.ObjectAdapterInterface;
import com.alfarabi.base.realm.GeneralRealmListParcelConverter;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import io.realm.RealmList;
import io.realm.SellerRealmProxy;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 8/15/17.
 */

@Parcel(value = Parcel.Serialization.BEAN, implementations = {SellerRealmProxy.class}, analyze = {Seller.class})
public class Seller extends RealmObject implements ObjectAdapterInterface {

    @PrimaryKey @SerializedName("id")@Getter @Setter long id ;
    @SerializedName("user_id")@Getter @Setter String userId ;
    @SerializedName("username")@Getter@Setter String username ;
    @SerializedName("name")@Getter@Setter String name ;
    @SerializedName("seller_name")@Getter@Setter String sellerName ;
    @SerializedName("business_name")@Getter@Setter String businessName ;
    @SerializedName("seo_url_shop")@Getter@Setter String seoUrlShop ;
    @SerializedName("location")@Getter@Setter Location location ;
    @SerializedName("address")@Getter@Setter String address ;
    @SerializedName("email")@Getter@Setter String email ;
    @SerializedName("phone_number")@Getter@Setter String phone_number ;
    @SerializedName("background_image_url")@Getter@Setter String backgroundImageUrl ;
    @SerializedName("avatar")@Getter@Setter String avatar ;
    @SerializedName("description")@Getter@Setter String description ;
    @SerializedName("images")@Getter RealmList<Expedition> expeditions = new RealmList<>();

    @SerializedName("shop_ratting")@Getter@Setter float shopRatting ;
    @SerializedName("review_count")@Getter@Setter int reviewCount ;
    @SerializedName("is_favorite")@Getter@Setter boolean favorite ;

    @ParcelPropertyConverter(GeneralRealmListParcelConverter.class)
    public void setExpeditions(RealmList<Expedition> expeditions) {
        this.expeditions = expeditions;
    }

    @Override
    public long getHeaderId() {
        return 0;
    }

    @Override
    public boolean isSearchable() {
        return false;
    }

    @Override
    public String canSearchByField() {
        return null;
    }
}
