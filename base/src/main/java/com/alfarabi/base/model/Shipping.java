package com.alfarabi.base.model;

import com.alfarabi.alfalibs.helper.model.ObjectAdapterInterface;
import com.alfarabi.base.view.interfaze.DataSpinnerInterface;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.Date;

import io.realm.ShippingRealmProxy;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 8/24/17.
 */
@Parcel(value = Parcel.Serialization.BEAN, implementations = {ShippingRealmProxy.class}, analyze = {Shipping.class})
public class Shipping extends RealmObject implements ObjectAdapterInterface, DataSpinnerInterface<Shipping> {

    @SerializedName("id")@Getter @Setter String id ;
    @SerializedName("createdby")@Getter@Setter long createdby ;
    @SerializedName("created")@Getter@Setter Date created  = new Date();
    @SerializedName("updatedby")@Getter@Setter long updatedy ;
    @SerializedName("updated")@Getter@Setter Date updated = new Date();

    @SerializedName("code")@Getter @Setter String code ;
    @SerializedName("shipp_name")@Getter @Setter String shippingName ;

    @Override
    public long getHeaderId() {
        return 0;
    }

    @Override
    public boolean isSearchable() {
        return false;
    }

    @Override
    public String canSearchByField() {
        return null;
    }

    @Override
    public String spinnerLabel() {
        return code;
    }

    @Override
    public boolean equal(Shipping shipping) {
        return id!=null && id.equals(shipping.getId());
    }
}
