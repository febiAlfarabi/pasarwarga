package com.alfarabi.base.model;

import com.alfarabi.alfalibs.helper.model.ObjectAdapterInterface;
import com.alfarabi.base.model.location.City;
import com.alfarabi.base.model.location.District;
import com.alfarabi.base.model.location.Province;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.HashMap;

import io.realm.ShopLocationRealmProxy;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 9/25/17.
 */


@Parcel(value = Parcel.Serialization.BEAN, implementations = {ShopLocationRealmProxy.class}, analyze = {ShopLocation.class})
public class ShopLocation extends RealmObject implements ObjectAdapterInterface {

//    "address": "Jl. Percontohan Kelurahan Begalan",
//            "province_id": "1",
//            "city_id": "4",
//            "kecamatan_id": "104",
//            "postal_code": "97231",

    @SerializedName("seller_id")@Getter@Setter String sellerId ;
    @SerializedName("address")@Getter@Setter String address ;
    @SerializedName("province")@Getter@Setter Province province ;
    @SerializedName("city")@Getter@Setter City city ;
    @SerializedName("kecamatan")@Getter@Setter District district ;
    @SerializedName("postal_code")@Getter@Setter String postalCode ;

    @Override
    public long getHeaderId() {
        return 0;
    }

    @Override
    public boolean isSearchable() {
        return false;
    }

    @Override
    public String canSearchByField() {
        return null;
    }

    public HashMap<String, String> toHashMap(){
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("address", address);
        hashMap.put("province_id", String.valueOf(province.getId()));
        hashMap.put("city_id", String.valueOf(city.getId()));
        hashMap.put("kecamatan_id", String.valueOf(district.getId()));
        hashMap.put("postal_code", String.valueOf(postalCode));

        return hashMap ;
    }
}
