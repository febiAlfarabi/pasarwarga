package com.alfarabi.base.model;

import com.alfarabi.alfalibs.helper.model.ObjectAdapterInterface;
import com.alfarabi.base.realm.GeneralRealmListParcelConverter;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.SpecifiedRealmProxy;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 8/21/17.
 */

@Parcel(value = Parcel.Serialization.BEAN, implementations = {SpecifiedRealmProxy.class}, analyze = {Specified.class})
public class Specified extends RealmObject implements ObjectAdapterInterface {
    @Getter @Setter @SerializedName("id") String id;
    @Getter @Setter @SerializedName("name") String name;
    @Getter @Setter @SerializedName("category_id") String categoryId;
    @Getter @Setter @SerializedName("shop_name") String shopName;
    @SerializedName("products") @Getter RealmList<Product> products = new RealmList<>();

    @ParcelPropertyConverter(GeneralRealmListParcelConverter.class)
    public void setProducts(RealmList<Product> products) {
        this.products = products;
    }

    @Override
    public long getHeaderId() {
        return 0;
    }

    @Override
    public boolean isSearchable() {
        return false;
    }

    @Override
    public String canSearchByField() {
        return null;
    }
}
