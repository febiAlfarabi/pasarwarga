package com.alfarabi.base.model;

import com.alfarabi.base.BaseApplication;
import com.alfarabi.base.R;
import com.alfarabi.base.view.interfaze.DataSpinnerInterface;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.text.DecimalFormat;
import java.util.Date;

import io.realm.RealmObject;
import io.realm.TenorRealmProxy;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 7/11/17.
 */

@Parcel(implementations = {TenorRealmProxy.class}, value = Parcel.Serialization.BEAN, analyze = {Tenor.class})
public class Tenor extends RealmObject implements DataSpinnerInterface<String> {

    @SerializedName("id")@Getter @Setter long id ;
    @SerializedName("createdby")@Getter@Setter long createdby ;
    @SerializedName("created")@Getter@Setter Date created  = new Date();
    @SerializedName("updatedby")@Getter@Setter long updatedy ;
    @SerializedName("updated")@Getter@Setter Date updated = new Date();
    @SerializedName("tenor")@Getter@Setter int tenor ;
    @SerializedName("amount")@Getter@Setter float amount ;

    @Override
    public String spinnerLabel() {
        return String.valueOf(tenor)+"x - "+ BaseApplication.context.getString(R.string.rp)+" "+ DecimalFormat.getInstance().format(Double.valueOf(amount));
    }

    @Override
    public boolean equal(String s) {
        return false;
    }
}
