
package com.alfarabi.base.model;


import com.alfarabi.alfalibs.helper.model.ObjectAdapterInterface;
import com.alfarabi.base.model.shop.SellerInfo;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import io.realm.UserRealmProxy;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;


@Parcel(value = Parcel.Serialization.BEAN, implementations = {UserRealmProxy.class}, analyze = {User.class})
public class User extends RealmObject implements ObjectAdapterInterface {

//    "id":"164",
//            "email": "babaho@example.com",
//            "first_name": "babaho",
//            "last_name": "shop",
//            "gender": "man",
//            "phone": "+62123123123",
//            "username": "babahoshop"
//            "photo": "https://dummyimage.com/600x400/eee/fff"
//            "token": "ajhsdkja7sdbasduiwyee_ashkdhaskjdiiadiasydasd"


    @PrimaryKey@SerializedName("id") @Getter@Setter String id;
    @SerializedName("email") @Getter@Setter String email;
    @SerializedName("first_name") @Getter@Setter String firstName;
    @SerializedName("last_name") @Getter@Setter String lastName;
    @SerializedName("phone") @Getter@Setter String phone;
    @SerializedName("thumbnail") @Getter@Setter String thumbnail;
    @SerializedName("username") @Getter@Setter String username;
    @SerializedName("gender") @Getter@Setter String gender;
    @SerializedName("photo") @Getter@Setter String photo;
    @SerializedName("city_id") @Getter@Setter String cityId;
    @SerializedName("city_name") @Getter@Setter String cityName;
    @SerializedName("about") @Getter@Setter String about;
    @SerializedName("birthday") @Getter@Setter String birthday;
    @SerializedName("token") @Getter@Setter String token;
    @SerializedName("zip_code") @Getter@Setter String zipCode;
    @SerializedName("status")@Getter@Setter String status ;
    @SerializedName("is_verified")@Getter@Setter boolean verified ;
    @SerializedName("is_seller")@Getter@Setter boolean isSeller ;
    @SerializedName("seller_id")@Getter@Setter String sellerId ;
    @SerializedName("seller_status")@Getter@Setter String sellerStatus ;
    @SerializedName("join_date")@Getter@Setter String joinDate ;
    @SerializedName("country_id")@Getter@Setter String countryId ;
    @SerializedName("country_name")@Getter@Setter String countryName ;
    @SerializedName("state_id")@Getter@Setter String stateId ;
    @SerializedName("state_name")@Getter@Setter String stateName ;
    @SerializedName("kecamatan_id")@Getter@Setter String districtId ;
    @SerializedName("kecamatan_name")@Getter@Setter String districtName ;
    @SerializedName("kelurahan_id")@Getter@Setter String subdistrictId ;
    @SerializedName("kelurahan_name")@Getter@Setter String subdistrictName ;
    @SerializedName("is_phone_verified")@Getter@Setter boolean isPhoneVerified ;


    @SerializedName("seller_info")@Getter@Setter SellerInfo sellerInfo ;


    @Override
    public long getHeaderId() {
        return 0;
    }

    @Override
    public boolean isSearchable() {
        return false;
    }

    @Override
    public String canSearchByField() {
        return null;
    }
}
