package com.alfarabi.base.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 8/21/17.
 */

public class Verification {
//    "user_id": "164",
//            "email": "azis@ahloo.com",
//            "phone": "+628579431325",
//            "otp_code": "VZhBMf",
//            "last_send": "2017-08-18 17:47:38",
//            "limit_send": "1"

    @Getter@Setter@SerializedName("user_id") String userId ;
    @Getter@Setter@SerializedName("email") String email ;
    @Getter@Setter@SerializedName("phone") String phone ;
    @Getter@Setter@SerializedName("otp_code") String otpCode ;
    @Getter@Setter@SerializedName("last_send") String lastSend ;
    @Getter@Setter@SerializedName("limit_send") String limitSend ;
}
