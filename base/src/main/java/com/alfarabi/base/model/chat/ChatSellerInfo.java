package com.alfarabi.base.model.chat;

import com.alfarabi.alfalibs.helper.model.ObjectAdapterInterface;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;
import io.realm.ChatSellerInfoRealmProxy;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 10/6/17.
 */


@Parcel(value = Parcel.Serialization.BEAN, implementations = {ChatSellerInfoRealmProxy.class}, analyze = {ChatSellerInfo.class})
public class ChatSellerInfo extends RealmObject implements ObjectAdapterInterface {

    @PrimaryKey@SerializedName("id")@Getter @Setter long id ;
    @SerializedName("shop_name")@Getter@Setter String name ;
    @SerializedName("store_image")@Getter@Setter String image ;

    @Override
    public long getHeaderId() {
        return 0;
    }

    @Override
    public boolean isSearchable() {
        return false;
    }

    @Override
    public String canSearchByField() {
        return null;
    }
}
