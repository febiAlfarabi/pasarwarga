package com.alfarabi.base.model.chat;

import com.alfarabi.alfalibs.helper.model.ObjectAdapterInterface;
import com.alfarabi.base.model.Product;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import io.realm.ChatUserRealmProxy;
import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 10/6/17.
 */

@Parcel(value = Parcel.Serialization.BEAN, implementations = {ChatUserRealmProxy.class}, analyze = {ChatUser.class})
public class ChatUser extends RealmObject implements ObjectAdapterInterface {


    @PrimaryKey@SerializedName("id")@Getter @Setter long id ;
    @SerializedName("first_name")@Getter@Setter String firstname ;
    @SerializedName("last_name")@Getter@Setter String lastname ;
    @SerializedName("photo")@Getter@Setter String image ;


    @Override
    public long getHeaderId() {
        return 0;
    }

    @Override
    public boolean isSearchable() {
        return false;
    }

    @Override
    public String canSearchByField() {
        return null;
    }
}
