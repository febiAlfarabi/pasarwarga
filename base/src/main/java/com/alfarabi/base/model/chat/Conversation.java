package com.alfarabi.base.model.chat;

import com.alfarabi.alfalibs.helper.model.ObjectAdapterInterface;
import com.alfarabi.base.realm.RealmDao;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.Date;
import io.realm.ConversationRealmProxy;
import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 10/6/17.
 */

@Parcel(value = Parcel.Serialization.BEAN, implementations = {ConversationRealmProxy.class}, analyze = {Conversation.class})
public class Conversation extends RealmObject implements ObjectAdapterInterface {

    @PrimaryKey@SerializedName("id")@Getter@Setter long id ;
    @SerializedName("chat")@Getter@Setter String chat ;
    @SerializedName("message")@Getter@Setter String message ;
    @SerializedName("link_attachment")@Getter@Setter String linkAttachment ;
    @SerializedName("product_attachment")@Getter@Setter ProductAttachment productAttachment ;
    @SerializedName("partner")@Getter@Setter Partner partner ;
    @SerializedName("created_at")@Getter@Setter String created ;
    @SerializedName("archive")@Getter@Setter boolean archive ;
    @SerializedName("has_been_read")@Getter@Setter boolean hasBeenRead ;

    @Override
    public long getHeaderId() {
        return 0;
    }

    @Override
    public boolean isSearchable() {
        return false;
    }

    @Override
    public String canSearchByField() {
        return null;
    }

    public UserConversation toUserConversation(boolean sender){
        UserConversation userConversation = null ;
        if(RealmDao.getRealm().where(UserConversation.class).equalTo("partner.id", partner.getId()).findFirst()!=null){
            userConversation = RealmDao.getRealm().where(UserConversation.class).equalTo("partner.id", partner.getId()).findFirst();
            userConversation = RealmDao.unmanage(userConversation);
            partner = userConversation.getPartner();
            partner.setSender(sender);
        }else{
            userConversation = new UserConversation();
            userConversation.setId(id);
        }
        userConversation.setChat(chat);
        userConversation.setMessage(message);
        userConversation.setLinkAttachment(linkAttachment);
        userConversation.setProductAttachment(productAttachment);
        userConversation.setPartner(partner);
        userConversation.setCreated(created);
        userConversation.setArchive(archive);
        return userConversation ;
    }

    public UserConversation toUserConversation(String parentId, boolean sender){
        UserConversation userConversation = null ;
        if(RealmDao.getRealm().where(UserConversation.class).equalTo("partner.id", parentId).findFirst()!=null){
            userConversation = RealmDao.getRealm().where(UserConversation.class).equalTo("partner.id", parentId).findFirst();
            userConversation = RealmDao.unmanage(userConversation);
            partner = userConversation.getPartner();
            partner.setSender(sender);
        }else{
            userConversation = new UserConversation();
        }
        userConversation.setId(Long.valueOf(parentId));
        userConversation.setChat(chat);
        userConversation.setMessage(message);
        userConversation.setLinkAttachment(linkAttachment);
        userConversation.setProductAttachment(productAttachment);
        userConversation.setPartner(partner);
        userConversation.setCreated(created);
        userConversation.setArchive(archive);
        return userConversation ;
    }
}
