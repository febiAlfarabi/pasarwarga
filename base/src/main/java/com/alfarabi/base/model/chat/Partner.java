package com.alfarabi.base.model.chat;

import com.alfarabi.alfalibs.helper.model.ObjectAdapterInterface;
import com.alfarabi.base.model.Product;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import io.realm.PartnerRealmProxy;
import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 10/6/17.
 */


@Parcel(value = Parcel.Serialization.BEAN, implementations = {PartnerRealmProxy.class}, analyze = {Partner.class})
public class Partner extends RealmObject implements ObjectAdapterInterface {

    @SerializedName("user_id")@Getter@Setter String id ;
    @SerializedName("user")@Getter @Setter ChatUser chatUser;
    @SerializedName("seller")@Getter @Setter ChatSellerInfo chatSellerInfo;
    @SerializedName("is_sender")@Getter @Setter boolean isSender ;
//    @SerializedName("as_seller")@Getter @Setter boolean asSeller ;


    @Override
    public long getHeaderId() {
        return 0;
    }

    @Override
    public boolean isSearchable() {
        return false;
    }

    @Override
    public String canSearchByField() {
        return null;
    }
}
