package com.alfarabi.base.model.chat;

import com.alfarabi.alfalibs.helper.model.ObjectAdapterInterface;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;
import io.realm.ProductAttachmentRealmProxy;
import io.realm.RealmObject;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 10/6/17.
 */
@Parcel(value = Parcel.Serialization.BEAN, implementations = {ProductAttachmentRealmProxy.class}, analyze = {ProductAttachment.class})
public class ProductAttachment extends RealmObject implements ObjectAdapterInterface {
    @SerializedName("id")@Getter @Setter long id ;
    @SerializedName("name")@Getter@Setter String name ;
//    @SerializedName("description")@Getter@Setter String description ;
    @SerializedName("image")@Getter@Setter String image ;

    @Override
    public long getHeaderId() {
        return 0;
    }

    @Override
    public boolean isSearchable() {
        return false;
    }

    @Override
    public String canSearchByField() {
        return null;
    }
}
