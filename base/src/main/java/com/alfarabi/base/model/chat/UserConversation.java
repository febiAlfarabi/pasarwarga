package com.alfarabi.base.model.chat;

import com.alfarabi.alfalibs.helper.model.ObjectAdapterInterface;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.Date;

import io.realm.UserConversationRealmProxy;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 10/6/17.
 */

@Parcel(value = Parcel.Serialization.BEAN, implementations = {UserConversationRealmProxy.class}, analyze = {UserConversation.class})
public class UserConversation extends RealmObject implements ObjectAdapterInterface {

    @PrimaryKey@SerializedName("id")@Getter@Setter long id ;
    @SerializedName("chat")@Getter@Setter String chat ;
    @SerializedName("message")@Getter@Setter String message ;
    @SerializedName("link_attachment")@Getter@Setter String linkAttachment ;
    @SerializedName("product_attachment")@Getter@Setter ProductAttachment productAttachment ;
    @SerializedName("partner")@Getter@Setter Partner partner ;
    @SerializedName("created_at")@Getter@Setter String created ;
    @SerializedName("archive")@Getter@Setter boolean archive ;

    @Override
    public long getHeaderId() {
        return 0;
    }

    @Override
    public boolean isSearchable() {
        return false;
    }

    @Override
    public String canSearchByField() {
        return null;
    }

    public Conversation toConversation(){
        Conversation conversation = new Conversation();
        conversation.setId(id);
        conversation.setChat(chat);
        conversation.setMessage(message);
        conversation.setLinkAttachment(linkAttachment);
        conversation.setProductAttachment(productAttachment);
        conversation.setPartner(partner);
        conversation.setCreated(created);
        conversation.setArchive(archive);
        return conversation ;
    }
}
