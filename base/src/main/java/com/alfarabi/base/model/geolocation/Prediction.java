package com.alfarabi.base.model.geolocation;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 8/25/17.
 */

public class Prediction {

    @SerializedName("id")@Getter@Setter String id ;
    @SerializedName("description")@Getter@Setter String description ;
    @SerializedName("place_id")@Getter@Setter String placeId ;
    @SerializedName("reference")@Getter@Setter String reference ;
    @SerializedName("structured_formatting")@Getter@Setter StructuredFormatting structuredFormatting ;
    @SerializedName("terms")@Getter@Setter List<Term> terms = new ArrayList<>();
    @SerializedName("types")@Getter@Setter String[] types ;



}
