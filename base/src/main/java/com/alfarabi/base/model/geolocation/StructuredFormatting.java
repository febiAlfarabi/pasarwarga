package com.alfarabi.base.model.geolocation;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 8/25/17.
 */

public class StructuredFormatting {

    @SerializedName("main_text")@Getter @Setter String mainText ;
    @SerializedName("secondary_text")@Getter @Setter String secondaryText ;
}
