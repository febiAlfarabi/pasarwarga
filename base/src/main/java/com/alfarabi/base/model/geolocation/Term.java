package com.alfarabi.base.model.geolocation;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 8/25/17.
 */

public class Term {

    @SerializedName("offset")@Getter@Setter int offset ;
    @SerializedName("value")@Getter@Setter String value ;

}
