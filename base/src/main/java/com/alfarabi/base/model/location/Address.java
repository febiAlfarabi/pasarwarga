package com.alfarabi.base.model.location;

import com.alfarabi.alfalibs.helper.model.ObjectAdapterInterface;
import com.alfarabi.base.view.interfaze.DataSpinnerInterface;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.Date;

import io.realm.AddressRealmProxy;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 8/23/17.
 */

@Parcel(value = Parcel.Serialization.BEAN, implementations = {AddressRealmProxy.class}, analyze = {Address.class})
public class Address extends RealmObject implements ObjectAdapterInterface, DataSpinnerInterface<Address> {

    @PrimaryKey @SerializedName("id")@Getter @Setter String id ;
    @SerializedName("createdby")@Getter@Setter long createdby ;
    @SerializedName("created")@Getter@Setter Date created  = new Date();
    @SerializedName("updatedby")@Getter@Setter long updatedy ;
    @SerializedName("updated")@Getter@Setter Date updated = new Date();

    @SerializedName("user_id")@Getter@Setter String userId ;
    @SerializedName("name")@Getter@Setter String name ;
    @SerializedName("address1")@Getter@Setter String address1 ;
    @SerializedName("address2")@Getter@Setter String address2 ;
    @SerializedName("city")@Getter@Setter String city ;
    @SerializedName("state")@Getter@Setter String state ;
    @SerializedName("country")@Getter@Setter String country ;
    @SerializedName("postal_code")@Getter@Setter String postalCode ;
    @SerializedName("phone")@Getter@Setter String phone ;


    @Override
    public long getHeaderId() {
        return 0;
    }

    @Override
    public boolean isSearchable() {
        return false;
    }

    @Override
    public String canSearchByField() {
        return null;
    }

    @Override
    public String spinnerLabel() {
        return name+" - "+address1;
    }

    @Override
    public boolean equal(Address address) {
        return id==address.getId();
    }
}
