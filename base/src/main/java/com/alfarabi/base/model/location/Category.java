package com.alfarabi.base.model.location;

import com.alfarabi.alfalibs.helper.model.ObjectAdapterInterface;
import com.alfarabi.base.model.Product;
import com.alfarabi.base.realm.GeneralRealmListParcelConverter;
import com.alfarabi.base.view.interfaze.DataSpinnerInterface;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import io.realm.CategoryRealmProxy;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 6/30/17.
 */

@Parcel(value = Parcel.Serialization.BEAN, implementations = {CategoryRealmProxy.class}, analyze = {Category.class})
public class Category extends RealmObject implements ObjectAdapterInterface, DataSpinnerInterface<Category>{

    @PrimaryKey@SerializedName("id")@Getter@Setter long id ;
    @SerializedName(value = "name", alternate = "cat_name")@Getter@Setter String name ;
    @SerializedName("description")@Getter@Setter String description ;
    @SerializedName("image")@Getter@Setter String image ;
    @SerializedName("products")@Getter RealmList<Product> products = new RealmList<>() ;
    @ParcelPropertyConverter(GeneralRealmListParcelConverter.class)
    public void setProducts(RealmList<Product> products) {
        this.products = products;
    }

    @Override
    public long getHeaderId() {
        return id;
    }

    @Override
    public boolean isSearchable() {
        return false;
    }

    @Override
    public String canSearchByField() {
        return "name";
    }

    @Override
    public String spinnerLabel() {
        return name;
    }

    @Override
    public boolean equal(Category category) {
        return category.getId()==id;
    }
}
