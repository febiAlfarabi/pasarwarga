package com.alfarabi.base.model.location;

import com.alfarabi.alfalibs.helper.model.ObjectAdapterInterface;
import com.alfarabi.base.view.interfaze.DataSpinnerInterface;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.Date;

import io.realm.CityRealmProxy;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 7/20/17.
 */

@Parcel(implementations = {CityRealmProxy.class}, value = Parcel.Serialization.BEAN, analyze = {City.class})
public class City extends RealmObject implements ObjectAdapterInterface, DataSpinnerInterface<City> {

    @PrimaryKey @SerializedName("id")@Getter @Setter String id ;
    @SerializedName("createdby")@Getter@Setter long createdby ;
    @SerializedName("created")@Getter@Setter Date created  = new Date();
    @SerializedName("updatedby")@Getter@Setter long updatedy ;
    @SerializedName("updated")@Getter@Setter Date updated = new Date();
    @SerializedName("name")@Getter@Setter String name ;

    @Override
    public String spinnerLabel() {
        return name;
    }

    @Override
    public boolean equal(City city) {
        if(id==city.getId()){
            return true ;
        }
        return false;
    }

    @Override
    public long getHeaderId() {
        return 0;
    }

    @Override
    public boolean isSearchable() {
        return true;
    }

    @Override
    public String canSearchByField() {
        return "name";
    }
}
