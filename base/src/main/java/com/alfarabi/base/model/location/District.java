package com.alfarabi.base.model.location;

import com.alfarabi.base.view.interfaze.DataSpinnerInterface;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.Date;

import io.realm.DistrictRealmProxy;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 7/20/17.
 */

@Parcel(implementations = {DistrictRealmProxy.class}, value = Parcel.Serialization.BEAN, analyze = {District.class})
public class District extends RealmObject implements DataSpinnerInterface<District> {

    @PrimaryKey
    @SerializedName("id")@Getter @Setter String id ;
    @SerializedName("createdby")@Getter@Setter long createdby ;
    @SerializedName("created")@Getter@Setter Date created  = new Date();
    @SerializedName("updatedby")@Getter@Setter long updatedy ;
    @SerializedName("updated")@Getter@Setter Date updated = new Date();
    @SerializedName("name")@Getter@Setter String name ;

    @Override
    public String spinnerLabel() {
        return name;
    }

    @Override
    public boolean equal(District city) {
        if(id==city.getId()){
            return true ;
        }
        return false;
    }
}
