package com.alfarabi.base.model.location;

import com.alfarabi.base.view.interfaze.DataSpinnerInterface;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.Date;

import io.realm.ProvinceRealmProxy;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 7/20/17.
 */

@Parcel(implementations = {ProvinceRealmProxy.class}, value = Parcel.Serialization.BEAN, analyze = {Province.class})
public class Province extends RealmObject implements DataSpinnerInterface<Province> {

    @PrimaryKey @SerializedName("id")@Getter @Setter String id ;
    @SerializedName("createdby")@Getter@Setter long createdby ;
    @SerializedName("created")@Getter@Setter Date created  = new Date();
    @SerializedName("updatedby")@Getter@Setter long updatedy ;
    @SerializedName("updated")@Getter@Setter Date updated = new Date();
    @SerializedName("name")@Getter@Setter String name ;

    @Override
    public String spinnerLabel() {
        return name;
    }

    @Override
    public boolean equal(Province city) {
        if(id==city.getId()){
            return true ;
        }
        return false;
    }
}
