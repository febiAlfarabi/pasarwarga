package com.alfarabi.base.model.location;

import com.alfarabi.base.view.interfaze.DataSpinnerInterface;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.ZipCodeRealmProxy;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 7/20/17.
 */

@Parcel(implementations = {ZipCodeRealmProxy.class}, value = Parcel.Serialization.BEAN, analyze = {ZipCode.class})
public class ZipCode extends RealmObject implements DataSpinnerInterface<ZipCode> {

    @SerializedName("id")@Getter @Setter long id ;
    @SerializedName("createdby")@Getter@Setter long createdby ;
    @SerializedName("created")@Getter@Setter Date created  = new Date();
    @SerializedName("updatedby")@Getter@Setter long updatedy ;
    @SerializedName("updated")@Getter@Setter Date updated = new Date();
    @SerializedName("zipcode")@PrimaryKey
    @Getter@Setter String zipcode ;

    @Override
    public String spinnerLabel() {
        return zipcode;
    }

    @Override
    public boolean equal(ZipCode zipCode) {
        if(zipcode==zipCode.getZipcode()){
            return true ;
        }
        return false;
    }
}
