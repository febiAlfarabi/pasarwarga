package com.alfarabi.base.model.notification;

import com.alfarabi.alfalibs.helper.model.ObjectAdapterInterface;
import com.google.gson.annotations.SerializedName;
import io.realm.NotificationRealmProxy;
import org.parceler.Parcel;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 6/30/17.
 */
@Parcel(value = Parcel.Serialization.BEAN, implementations = {NotificationRealmProxy.class}, analyze = {Notification.class})
public class Notification extends RealmObject implements ObjectAdapterInterface{

    public static int INFORMATION = 1 ;

    @PrimaryKey@SerializedName("id")@Getter @Setter long id ;
    @SerializedName("createdby")@Getter@Setter long createdby ;
    @SerializedName("created")@Getter@Setter Date created  = new Date();
    @SerializedName("updatedby")@Getter@Setter long updatedy ;
    @SerializedName("updated")@Getter@Setter Date updated = new Date();

    @SerializedName("type")@Getter@Setter String type ;
    @SerializedName("object_id")@Getter@Setter String objectId ;
    @SerializedName("detail")@Getter@Setter String detail ;


//    @SerializedName("title")@Getter@Setter String title ;
//    @SerializedName("body")@Getter@Setter String body ;
//    @SerializedName("sound")@Getter@Setter String sound ;
//    @SerializedName("badge")@Getter@Setter String badge ;
//    @SerializedName("data")@Getter@Setter NotificationDetail notificationDetail ;
//    @SerializedName("subtitle")@Getter@Setter String subtitle ;
//    @SerializedName("image")@Getter@Setter String image ;
//    @SerializedName("type")@Getter@Setter int type = INFORMATION ;



    @Override
    public long getHeaderId() {
        return 0;
    }

    @Override
    public boolean isSearchable() {
        return false;
    }

    @Override
    public String canSearchByField() {
        return null;
    }
}
