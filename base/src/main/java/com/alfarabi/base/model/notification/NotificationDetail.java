package com.alfarabi.base.model.notification;

import com.alfarabi.alfalibs.helper.model.ObjectAdapterInterface;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.NotificationDetailRealmProxy;
import org.parceler.Parcel;

import java.util.HashMap;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 10/9/17.
 */
@Parcel(value = Parcel.Serialization.BEAN, implementations = {NotificationDetailRealmProxy.class}, analyze = {NotificationDetail.class})
public class NotificationDetail extends RealmObject implements ObjectAdapterInterface {

    @Ignore@Expose public static final String CHAT = "chat";

    @SerializedName("type")@Getter@Setter String type ;
    @SerializedName("object_id")@Getter@Setter private long objectId ;
//    @SerializedName("detail") HashMap object ;

    @Override
    public long getHeaderId() {
        return 0;
    }

    @Override
    public boolean isSearchable() {
        return false;
    }

    @Override
    public String canSearchByField() {
        return null;
    }
}
