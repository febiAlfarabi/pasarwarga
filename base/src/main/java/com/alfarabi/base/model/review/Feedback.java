package com.alfarabi.base.model.review;

import com.alfarabi.alfalibs.helper.model.ObjectAdapterInterface;
import com.alfarabi.base.model.Seller;
import com.alfarabi.base.model.User;
import com.alfarabi.base.model.shop.SellerInfo;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import io.realm.RealmObject;
import io.realm.FeedbackRealmProxy;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 10/4/17.
 */



@Parcel(implementations = {FeedbackRealmProxy.class}, value = Parcel.Serialization.BEAN, analyze = {Feedback.class})
public class Feedback extends RealmObject implements ObjectAdapterInterface {

    @SerializedName("id")@Getter@Setter long id ;
    @SerializedName("deal_code_number")@Getter@Setter String dealCodeNumber ;
    @SerializedName("title")@Getter@Setter String title ;
    @SerializedName("description")@Getter@Setter String description ;
    @SerializedName("rating")@Getter@Setter int rating ;
    @SerializedName("created_at")@Getter@Setter String createdAt ;
    @SerializedName("product_name")@Getter@Setter String productName ;
    @SerializedName("product_image")@Getter@Setter String productImage ;
    @SerializedName("seller")@Getter@Setter SellerInfo seller ;
    @SerializedName("buyer")@Getter@Setter User buyer ;

    @Override
    public long getHeaderId() {
        return 0;
    }

    @Override
    public boolean isSearchable() {
        return false;
    }

    @Override
    public String canSearchByField() {
        return null;
    }
}
