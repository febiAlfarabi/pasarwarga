package com.alfarabi.base.model.review;

import com.alfarabi.alfalibs.helper.model.ObjectAdapterInterface;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.ReviewRealmProxy;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 6/30/17.
 */

@Parcel(implementations = {ReviewRealmProxy.class}, value = Parcel.Serialization.BEAN, analyze = {Review.class})
public class Review extends RealmObject implements ObjectAdapterInterface {

    @PrimaryKey @SerializedName("id")@Getter @Setter long id ;
    @SerializedName("createdby")@Getter@Setter long createdby ;
    @SerializedName("created")@Getter@Setter Date created  = new Date();
    @SerializedName("updatedby")@Getter@Setter long updatedy ;
    @SerializedName("updated")@Getter@Setter Date updated = new Date();
    @SerializedName("product_id")@Getter@Setter String productId ;
    @SerializedName("product_name")@Getter@Setter String productName ;
    @SerializedName("seller_id")@Getter@Setter String sellerId ;
    @SerializedName("user_id")@Getter@Setter String userId ;
    @SerializedName("user_name")@Getter@Setter String username ;
    @SerializedName("user_first_name")@Getter@Setter String userFirstname ;
    @SerializedName("user_last_name")@Getter@Setter String userLastname ;
    @SerializedName("deal_code")@Getter@Setter String dealCode ;
    @SerializedName("thumbnail")@Getter@Setter String thumbnail ;
    @SerializedName("title")@Getter@Setter String title ;
    @SerializedName("description")@Getter@Setter String description ;
    @SerializedName("rating")@Getter@Setter String rating ;
    @SerializedName("date_added")@Getter@Setter String dateAdded ;

    @Override
    public long getHeaderId() {
        return 0;
    }

    @Override
    public boolean isSearchable() {
        return false;
    }

    @Override
    public String canSearchByField() {
        return null;
    }
}
