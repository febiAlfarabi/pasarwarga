package com.alfarabi.base.model.review;

import com.alfarabi.alfalibs.helper.model.ObjectAdapterInterface;
import com.alfarabi.base.model.transaction.OrderStatus;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import io.realm.RealmObject;
import io.realm.SummaryFeedbackRealmProxy;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 9/25/17.
 */

@Parcel(value = Parcel.Serialization.BEAN, implementations = {SummaryFeedbackRealmProxy.class}, analyze = {SummaryFeedback.class})
public class SummaryFeedback extends RealmObject implements ObjectAdapterInterface {


    @SerializedName("id")@Getter@Setter long id ;

    @SerializedName("feedback_able")@Getter@Setter int feedbackable ;


    @Override
    public long getHeaderId() {
        return 0;
    }

    @Override
    public boolean isSearchable() {
        return false;
    }

    @Override
    public String canSearchByField() {
        return null;
    }


}
