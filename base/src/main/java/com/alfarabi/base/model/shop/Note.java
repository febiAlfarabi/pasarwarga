package com.alfarabi.base.model.shop;

import com.alfarabi.alfalibs.helper.model.ObjectAdapterInterface;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.HashMap;

import io.realm.NoteRealmProxy;
import io.realm.RealmObject;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 9/25/17.
 */

@Parcel(value = Parcel.Serialization.BEAN, implementations = {NoteRealmProxy.class}, analyze = {Note.class})
public class Note extends RealmObject implements ObjectAdapterInterface {

    @SerializedName("id")@Getter@Setter long id ;
    @SerializedName("seller_id")@Getter@Setter String sellerId ;
    @SerializedName("title") @Getter@Setter String title ;
    @SerializedName("content") @Getter@Setter String content ;
    @SerializedName("is_active")@Getter@Setter boolean active ;
    @SerializedName("created_at")@Getter@Setter String createdAt ;
    @SerializedName("updated_at")@Getter@Setter String updatedAt ;


    @Override
    public long getHeaderId() {
        return 0;
    }

    @Override
    public boolean isSearchable() {
        return false;
    }

    @Override
    public String canSearchByField() {
        return null;
    }


//    {
//        "title": "Judul catatan",
//            "content": "Isi catatan toko pasarwarga.",
//            "is_active": true
//    }

    public HashMap<String, String> toHashMap(){
        HashMap<String, String > hashMap = new HashMap<>();
        hashMap.put("title", title);
        hashMap.put("content", content);
        hashMap.put("is_active", String.valueOf(true));
        return hashMap ;
    }
}
