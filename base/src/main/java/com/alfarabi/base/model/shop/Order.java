package com.alfarabi.base.model.shop;

import com.alfarabi.alfalibs.helper.model.ObjectAdapterInterface;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import io.realm.OrderRealmProxy;
import io.realm.RealmObject;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 9/22/17.
 */

@Parcel(value = Parcel.Serialization.BEAN, implementations = {OrderRealmProxy.class}, analyze = {Order.class})
public class Order extends RealmObject implements ObjectAdapterInterface {
//    "id": "3088",
//            "deal_code_number": "1499958905",
//            "user_email": "dedicaisar88@gmail.com",
//            "product_price": 2400000,
//            "payment_type": "kreditplus",
//            "insurance": false,
//            "amount_insurance": 0,
//            "tracking_id": "1005609468",
//            "shipping_status": "Delivered"

    @SerializedName("id")@Getter@Setter String id ;
    @SerializedName("deal_code_number")@Getter@Setter String dealCodeNumber ;
    @SerializedName("user_email")@Getter@Setter String userEmail ;
    @SerializedName("product_price")@Getter@Setter float productPrice ;
    @SerializedName("payment_type")@Getter@Setter String paymentType ;
    @SerializedName("insurance")@Getter@Setter boolean insurance ;
    @SerializedName("amount_insurance")@Getter@Setter float amountInsurance ;
    @SerializedName("tracking_id")@Getter@Setter String trackingId ;
    @SerializedName("shipping_status")@Getter@Setter String shippingStatus ;

    @SerializedName("product_name")@Getter@Setter String productName ;
    @SerializedName("product_image")@Getter@Setter String productImage ;
    @SerializedName("total_price")@Getter@Setter float totalPrice ;
    @SerializedName("transaction_date")@Getter@Setter String transactionDate ;


    @Override
    public long getHeaderId() {
        return 0;
    }

    @Override
    public boolean isSearchable() {
        return false;
    }

    @Override
    public String canSearchByField() {
        return null;
    }
}
