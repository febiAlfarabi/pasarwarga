package com.alfarabi.base.model.shop;

import com.alfarabi.alfalibs.helper.model.ObjectAdapterInterface;
import com.alfarabi.base.model.Payment;
import com.alfarabi.base.model.Product;
import com.alfarabi.base.model.Tenor;
import com.alfarabi.base.model.User;
import com.alfarabi.base.model.location.Address;
import com.alfarabi.base.realm.GeneralRealmListParcelConverter;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import io.realm.OrderDetailRealmProxy;
import io.realm.RealmList;
import io.realm.RealmObject;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 9/22/17.
 */

@Parcel(value = Parcel.Serialization.BEAN, implementations = {OrderDetailRealmProxy.class}, analyze = {OrderDetail.class})
public class OrderDetail extends RealmObject implements ObjectAdapterInterface {

    @SerializedName("id")@Getter@Setter String id ;
    @SerializedName("deal_code_number")@Getter@Setter String dealCodeNumber ;
    @SerializedName("quantity")@Getter@Setter int quantity ;
    @SerializedName("shipping_cost")@Getter@Setter float shippingCost ;
    @SerializedName("discount_amount")@Getter@Setter float discountAmount ;
    @SerializedName("total")@Getter@Setter float total ;
    @SerializedName("status")@Getter@Setter String status ;
    @SerializedName("shipping_status")@Getter@Setter String shippingStatus ;
    @SerializedName("shipp_code")@Getter@Setter String shippCode ;
    @SerializedName("tracking_id")@Getter@Setter String trackingId ;
    @SerializedName("note")@Getter@Setter String note ;
    @SerializedName("expired_date")@Getter@Setter String expiredDate ;
    @SerializedName("created_at")@Getter@Setter String createdAt ;

    @SerializedName("tenor")@Getter RealmList<Tenor> tenors ;
    @ParcelPropertyConverter(GeneralRealmListParcelConverter.class)
    public void setTenors(RealmList<Tenor> tenors) {
        this.tenors = tenors;
    }
    @SerializedName("payment_method")@Getter@Setter Payment paymentMethod ;
    @SerializedName("product")@Getter@Setter Product product ;
    @SerializedName("buyer")@Getter@Setter User buyer ;
    @SerializedName("shipping_address")@Getter@Setter
    Address shippingAddress ;


    @Override
    public long getHeaderId() {
        return 0;
    }

    @Override
    public boolean isSearchable() {
        return false;
    }

    @Override
    public String canSearchByField() {
        return null;
    }
}
