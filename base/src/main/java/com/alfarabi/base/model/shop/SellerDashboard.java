package com.alfarabi.base.model.shop;

import com.alfarabi.alfalibs.helper.model.ObjectAdapterInterface;
import com.alfarabi.base.model.Product;
import com.alfarabi.base.model.Tenor;
import com.alfarabi.base.realm.GeneralRealmListParcelConverter;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.SellerDashboardRealmProxy;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 8/29/17.
 */

@Parcel(value = Parcel.Serialization.BEAN, implementations = {SellerDashboardRealmProxy.class}, analyze = {SellerDashboard.class})
public class SellerDashboard extends RealmObject implements ObjectAdapterInterface {

//    @Ignore@Expose public static final int INITIAL_ID = 1;

    @PrimaryKey@SerializedName("id")@Getter@Setter long id  ;
    @SerializedName("seller_info")@Getter@Setter private SellerInfo sellerInfo ;
    @SerializedName("products")@Getter private RealmList<Product> products ;
    @ParcelPropertyConverter(GeneralRealmListParcelConverter.class)
    public void setTenors(RealmList<Product> products) {
        this.products = products;
    }


    @Override
    public long getHeaderId() {
        return 0;
    }

    @Override
    public boolean isSearchable() {
        return false;
    }

    @Override
    public String canSearchByField() {
        return null;
    }
}
