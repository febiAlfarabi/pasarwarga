package com.alfarabi.base.model.shop;

import com.alfarabi.alfalibs.helper.model.ObjectAdapterInterface;
import com.alfarabi.base.model.Location;
import com.alfarabi.base.model.ShopLocation;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.Date;

import io.realm.SellerInfoRealmProxy;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 8/28/17.
 */

@Parcel(value = Parcel.Serialization.BEAN, implementations = {SellerInfoRealmProxy.class}, analyze = {SellerInfo.class})
public class SellerInfo extends RealmObject implements ObjectAdapterInterface{

    public static final String ADMIN_ACTIVATION = "admin_activation";

    @PrimaryKey @SerializedName("id")@Getter @Setter String id ;
    @SerializedName("user_id")@Getter @Setter String userId ;
    @SerializedName("createdby")@Getter@Setter long createdby ;
    @SerializedName("created")@Getter@Setter Date created  = new Date();
    @SerializedName("updatedby")@Getter@Setter long updatedy ;
    @SerializedName("updated")@Getter@Setter Date updated = new Date();

    @SerializedName("shop_name")@Getter@Setter String shopName;
    @SerializedName("address")@Getter@Setter String address;
    @SerializedName("location")@Getter@Setter Location location;
    @SerializedName("shop_location")@Getter@Setter ShopLocation shopLocation ;
    @SerializedName("shop_ratting")@Getter@Setter float shoprating;
    @SerializedName("review_count")@Getter@Setter int reviewCount;
    @SerializedName("sales_count")@Getter@Setter int salesCount;
    @SerializedName("favorite_count")@Getter@Setter int favoriteCount;
    @SerializedName("product_count")@Getter@Setter int productCount;
    @SerializedName("following_number")@Getter@Setter int followingNumber;
    @SerializedName("followers_number")@Getter@Setter int followersNumber;
    @SerializedName("store_image")@Getter@Setter String storeImage;
    @SerializedName("banner_image")@Getter@Setter String bannerImage;
    @SerializedName("seller_email")@Getter@Setter String sellerEmail;
    @SerializedName("seller_name")@Getter@Setter String sellerName;
    @SerializedName("status")@Getter@Setter String status;
    @SerializedName(ADMIN_ACTIVATION)@Getter@Setter boolean adminActivation;
    @SerializedName("shop_announcement")@Getter@Setter String shopAnnouncement;
    @SerializedName("message_to_buyers")@Getter@Setter String messageToBuyers;
    @SerializedName("seourl")@Getter@Setter String seourl;
    @SerializedName("is_favorite")@Getter@Setter boolean favorite ;
    @SerializedName("join_since")@Getter@Setter String joinSince;
    @SerializedName("last_login")@Getter@Setter String lastLogin;



    @Override
    public long getHeaderId() {
        return 0;
    }

    @Override
    public boolean isSearchable() {
        return false;
    }

    @Override
    public String canSearchByField() {
        return null;
    }

}
