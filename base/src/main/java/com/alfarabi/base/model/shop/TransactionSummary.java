package com.alfarabi.base.model.shop;

import com.alfarabi.alfalibs.helper.model.ObjectAdapterInterface;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import io.realm.OrderDetailRealmProxy;
import io.realm.RealmObject;
import io.realm.TransactionSummaryRealmProxy;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 9/22/17.
 */

@Parcel(value = Parcel.Serialization.BEAN, implementations = {TransactionSummaryRealmProxy.class}, analyze = {TransactionSummary.class})
public class TransactionSummary extends RealmObject implements ObjectAdapterInterface {

    @SerializedName("order_count")@Getter@Setter long orderCount ;
    @SerializedName("succeed_transaction_count")@Getter@Setter long succeedTransactionCount ;
    @SerializedName("succeed_transaction_price")@Getter@Setter float succeedTransactionPrice ;

    @Override
    public long getHeaderId() {
        return 0;
    }

    @Override
    public boolean isSearchable() {
        return false;
    }

    @Override
    public String canSearchByField() {
        return null;
    }
}
