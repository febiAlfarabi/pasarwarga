package com.alfarabi.base.model.transaction;

import com.alfarabi.alfalibs.helper.model.ObjectAdapterInterface;
import com.alfarabi.base.realm.GeneralRealmListParcelConverter;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;
import io.realm.CartRealmProxy;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 6/30/17.
 */

@Parcel(value = Parcel.Serialization.BEAN, implementations = {CartRealmProxy.class}, analyze = {Cart.class})
public class Cart extends RealmObject implements ObjectAdapterInterface{

    @PrimaryKey@SerializedName("id")@Getter@Setter long id ;
    @SerializedName("createdby")@Getter@Setter long createdby ;
    @SerializedName("created")@Getter@Setter Date created  = new Date();
    @SerializedName("updatedby")@Getter@Setter long updatedy ;
    @SerializedName("updated")@Getter@Setter Date updated = new Date();

    @SerializedName("count")@Getter@Setter int count ;
    @SerializedName("items")@Getter RealmList<CartItem> cartItems = new RealmList<>();
    @ParcelPropertyConverter(GeneralRealmListParcelConverter.class)
    public void setCartItems(RealmList<CartItem> cartItems) {
        this.cartItems = cartItems;
    }
    @SerializedName("admin_fee")@Getter@Setter float adminFee ;
    @SerializedName("coupon_discount_amount")@Getter@Setter float couponDiscountAmmount ;

    @Override
    public long getHeaderId() {
        return 0;
    }

    @Override
    public boolean isSearchable() {
        return false;
    }

    @Override
    public String canSearchByField() {
        return null;
    }
}
