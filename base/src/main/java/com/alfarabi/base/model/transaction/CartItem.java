package com.alfarabi.base.model.transaction;

import com.alfarabi.alfalibs.helper.model.ObjectAdapterInterface;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import io.realm.CartItemRealmProxy;

import java.util.Date;

import io.realm.RealmObject;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by User on 05/07/2017.
 */

@Parcel(value = Parcel.Serialization.BEAN, implementations = {CartItemRealmProxy.class}, analyze = {CartItem.class})
public class CartItem extends RealmObject implements ObjectAdapterInterface {

//        "id": "3974",
//        "product_id": "342",
//        "seller_id": "258",
//        "product_name": "Panasonic 22\" LED HD TV - Hitam (Model 22D305)",
//        "final_price": 1620000,
//        "discount": null,
//        "image": "https://s3-ap-southeast-1.amazonaws.com/pasarwarga/image




    @SerializedName("id")@Getter @Setter String id ;
    @SerializedName("createdby")@Getter@Setter long createdby ;
    @SerializedName("created")@Getter@Setter Date created  = new Date();
    @SerializedName("updatedby")@Getter@Setter long updatedy ;
    @SerializedName("updated")@Getter@Setter Date updated = new Date();

    @SerializedName("product_id")@Getter@Setter String productId ;
    @SerializedName("seller_id")@Getter@Setter String sellerId ;
    @SerializedName("product_name")@Getter@Setter String productName ;
    @SerializedName("product_price")@Getter@Setter float productPrice ;
    @SerializedName("final_price")@Getter@Setter float finalPrice ;
    @SerializedName("discount")@Getter@Setter int discount ;
    @SerializedName("image")@Getter@Setter String image ;
    @SerializedName("insurance")@Getter@Setter boolean insurance ;
    @SerializedName("amount_insurance")@Getter@Setter float amountInsurance ;
    @SerializedName("coupon_discount_amount")@Getter@Setter double couponDiscountAmmount ;

    @Override
    public long getHeaderId() {
        return 0;
    }

    @Override
    public boolean isSearchable() {
        return false;
    }

    @Override
    public String canSearchByField() {
        return null;
    }
}
