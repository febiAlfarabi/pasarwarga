package com.alfarabi.base.model.transaction;

import com.alfarabi.alfalibs.helper.model.ObjectAdapterInterface;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import io.realm.HistoryTrackingRealmProxy;
import io.realm.RealmObject;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 9/22/17.
 */


@Parcel(value = Parcel.Serialization.BEAN, implementations = {HistoryTrackingRealmProxy.class}, analyze = {HistoryTracking.class})
public class HistoryTracking extends RealmObject implements ObjectAdapterInterface {

    @SerializedName("manifest_date")@Getter@Setter String manifesDate;
    @SerializedName("manifest_time")@Getter@Setter String manifestTime;
    @SerializedName("manifest_description")@Getter@Setter String manifestDescription;

    @Override
    public long getHeaderId() {
        return 0;
    }

    @Override
    public boolean isSearchable() {
        return false;
    }

    @Override
    public String canSearchByField() {
        return null;
    }
}
