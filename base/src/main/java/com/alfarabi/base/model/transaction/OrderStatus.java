package com.alfarabi.base.model.transaction;

import com.alfarabi.alfalibs.helper.model.ObjectAdapterInterface;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import io.realm.OrderStatusRealmProxy;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 9/25/17.
 */

@Parcel(value = Parcel.Serialization.BEAN, implementations = {OrderStatusRealmProxy.class}, analyze = {OrderStatus.class})
public class OrderStatus extends RealmObject implements ObjectAdapterInterface {

    @Expose@Ignore public static final String TYPE_PENDING = "pending";
    @Expose@Ignore public static final String TYPE_PROCESSED = "processed";
    @Expose@Ignore public static final String TYPE_SHIPPED = "shipped";
    @Expose@Ignore public static final String TYPE_DELIVERED = "delivered";

    @SerializedName("count")@Getter@Setter int count = 0;
    @SerializedName("total")@Getter@Setter float total = 0 ;

    @Override
    public long getHeaderId() {
        return 0;
    }

    @Override
    public boolean isSearchable() {
        return false;
    }

    @Override
    public String canSearchByField() {
        return null;
    }
}
