package com.alfarabi.base.model.transaction;

import com.alfarabi.alfalibs.helper.model.ObjectAdapterInterface;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import io.realm.RealmObject;
import io.realm.SummaryStatusRealmProxy;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 9/25/17.
 */

@Parcel(value = Parcel.Serialization.BEAN, implementations = {SummaryStatusRealmProxy.class}, analyze = {SummaryStatus.class})
public class SummaryStatus extends RealmObject implements ObjectAdapterInterface {


    @SerializedName("id")@Getter@Setter long id ;

    @SerializedName("pending")@Getter@Setter OrderStatus pending ;
    @SerializedName("processed")@Getter@Setter OrderStatus processed ;
    @SerializedName("shipped")@Getter@Setter OrderStatus shipped ;
    @SerializedName("delivered")@Getter@Setter OrderStatus delivered ;


    @Override
    public long getHeaderId() {
        return 0;
    }

    @Override
    public boolean isSearchable() {
        return false;
    }

    @Override
    public String canSearchByField() {
        return null;
    }


    public float getTotalTransactionPrice(){
        float total = pending.getTotal()+processed.getTotal()+shipped.getTotal()+delivered.getTotal();
        return total ;
    }
    public float getTotalTransaction(){
        float count = pending.getCount()+processed.getCount()+shipped.getCount()+delivered.getCount();
        return count ;
    }

}
