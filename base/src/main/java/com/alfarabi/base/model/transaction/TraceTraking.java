package com.alfarabi.base.model.transaction;

import com.alfarabi.alfalibs.helper.model.ObjectAdapterInterface;
import com.alfarabi.base.realm.GeneralRealmListParcelConverter;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import io.realm.RealmList;
import io.realm.TraceTrakingRealmProxy;
import io.realm.RealmObject;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 9/22/17.
 */


@Parcel(value = Parcel.Serialization.BEAN, implementations = {TraceTrakingRealmProxy.class}, analyze = {TraceTraking.class})
public class TraceTraking extends RealmObject implements ObjectAdapterInterface {

    @SerializedName("waybill_number")@Getter@Setter String waybillNumber;
    @SerializedName("courier_name")@Getter@Setter String courierName;
    @SerializedName("waybill_date")@Getter@Setter String waybillDate;
    @SerializedName("shipper_name")@Getter@Setter String shipperName;
    @SerializedName("shipping_status")@Getter@Setter String shippingStatus;

    @SerializedName("history") @Getter RealmList<HistoryTracking> historyTrackings = new RealmList<>();
    @ParcelPropertyConverter(GeneralRealmListParcelConverter.class)
    public void setHistoryTrackings(RealmList<HistoryTracking> historyTrackings) {
        this.historyTrackings = historyTrackings;
    }

    @Override
    public long getHeaderId() {
        return 0;
    }

    @Override
    public boolean isSearchable() {
        return false;
    }

    @Override
    public String canSearchByField() {
        return null;
    }
}
