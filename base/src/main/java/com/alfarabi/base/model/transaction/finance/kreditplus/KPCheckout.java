package com.alfarabi.base.model.transaction.finance.kreditplus;

import com.alfarabi.base.model.Tenor;
import com.alfarabi.base.realm.GeneralRealmListParcelConverter;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import io.realm.KPCheckoutRealmProxy;
import io.realm.RealmList;
import io.realm.RealmObject;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 9/13/17.
 */

@Parcel(value = Parcel.Serialization.BEAN, implementations = {KPCheckoutRealmProxy.class}, analyze = {KPCheckout.class})
public class KPCheckout extends RealmObject {

    @Getter@Setter@SerializedName("id") private String id ;
    @Getter@Setter@SerializedName("deal_code_number") private String dealCodeNumber ;
    @Getter@Setter@SerializedName("user_id") private String userId ;
    @Getter@Setter@SerializedName("product_id") private String productId ;
    @Getter@Setter@SerializedName("seller_id") private String sellerId ;
    @Getter@Setter@SerializedName("quantity") private String quantity ;
    @Getter@Setter@SerializedName("indtotal") private float indtotal ;
    @Getter@Setter@SerializedName("shipping_address_id") private String shippingAddressId ;
    @Getter@Setter@SerializedName("shipping_cost") private float shippingCost ;
    @Getter@Setter@SerializedName("admin_fee") private float adminFee ;
    @Getter@Setter@SerializedName("amount_insurance") private float amountInsurance ;
    @Getter@SerializedName("tenors") private RealmList<Tenor> tenors = new RealmList<>();
    @ParcelPropertyConverter(GeneralRealmListParcelConverter.class)
    public void setTenors(RealmList<Tenor> tenors) {
        this.tenors = tenors;
    }
    @Getter@Setter@SerializedName("coupon_discount_amount") private float couponDiscountAmount ;
    @Getter@Setter@SerializedName("sum_total") private float sumTotal ;
    @Getter@Setter@SerializedName("status") private String status ;
    @Getter@Setter@SerializedName("payment_type") private String paymentType ;
    @Getter@Setter@SerializedName("shipping_status") private String shippingStatus ;
    @Getter@Setter@SerializedName("total") private String total ;
    @Getter@Setter@SerializedName("note") private String note ;



}
