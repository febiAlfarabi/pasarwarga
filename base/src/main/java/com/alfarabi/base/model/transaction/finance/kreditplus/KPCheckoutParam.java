package com.alfarabi.base.model.transaction.finance.kreditplus;

import com.alfarabi.base.model.Product;
import com.alfarabi.base.model.transaction.Cart;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import io.realm.KPCheckoutParamRealmProxy;
import io.realm.RealmObject;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 9/13/17.
 */

@Parcel(value = Parcel.Serialization.BEAN, implementations = {KPCheckoutParamRealmProxy.class}, analyze = {KPCheckoutParam.class})
public class KPCheckoutParam extends RealmObject{

    @Getter@Setter@SerializedName("cart") private Cart cart ;
    @Getter@Setter@SerializedName("product") private Product product ;
    @Getter@Setter@SerializedName("cart_id") private String cartId ;
    @Getter@Setter@SerializedName("note") private String note ;
    @Getter@Setter@SerializedName("shipp_code") private String shippCode;
    @Getter@Setter@SerializedName("shipping_service_id") private String shippingServiceId ;
    @Getter@Setter@SerializedName("shipping_service_type") private String shippingServiceType ;
    @Getter@Setter@SerializedName("address_id") private String addressId ;
    @Getter@Setter@SerializedName("payment_method_id") private String paymentMethodId ;
    @Getter@Setter@SerializedName("insurance_id") private String insuranceId ;
    @Getter@Setter@SerializedName("coupon_code") private String couponCode ;


    @Setter private float totalPrice = 0 ;

    public float getTotalPrice() {
        return totalPrice = productPrice+shippingCost+insurance+administrationFee-voucher;
    }

    @Getter@Setter private float productPrice = 0 ;
    @Getter@Setter@SerializedName("shipping_cost") private float shippingCost = 0 ;
    @Getter@Setter private float insurance = 0 ;
    @Getter@Setter private float administrationFee = 0 ;
    @Getter@Setter private float voucher = 0 ;

}
