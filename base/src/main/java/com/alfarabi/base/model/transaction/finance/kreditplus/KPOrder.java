package com.alfarabi.base.model.transaction.finance.kreditplus;

import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.base.model.Product;
import com.alfarabi.base.model.Tenor;
import com.alfarabi.base.model.location.City;
import com.alfarabi.base.model.location.District;
import com.alfarabi.base.model.location.Province;
import com.alfarabi.base.model.location.Subdistrict;
import com.alfarabi.base.model.location.ZipCode;
import com.alfarabi.base.realm.RealmDao;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang.time.DateFormatUtils;
import org.parceler.Parcel;

import java.util.Date;
import java.util.HashMap;

import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 7/31/17.
 */

@Parcel
public class KPOrder {

    @PrimaryKey@SerializedName("billing_id")@Getter@Setter long billingId ;
    @PrimaryKey@SerializedName("deal_code_number")@Getter@Setter String deal_code_number ;
    @PrimaryKey@SerializedName("shipping_status")@Getter@Setter String shipping_status ;
    @PrimaryKey@SerializedName("expired_date")@Getter@Setter  String expired_date ;
    @PrimaryKey@SerializedName("amount_finance")@Getter@Setter long amount_finance ;
    @PrimaryKey@SerializedName("shipping_cost")@Getter@Setter long shipping_cost ;
    @PrimaryKey@SerializedName("grand_total")@Getter@Setter long grand_total ;
    @PrimaryKey@SerializedName("user_id")@Getter@Setter String user_id ;
    @PrimaryKey@SerializedName("product_id")@Getter@Setter String product_id ;
    @PrimaryKey@SerializedName("seller_id")@Getter@Setter String seller_id ;
    @PrimaryKey@SerializedName("amount_insurance")@Getter@Setter long amount_insurance ;

}
