package com.alfarabi.base.model.transaction.finance.kreditplus;

import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.base.model.location.City;
import com.alfarabi.base.model.location.District;
import com.alfarabi.base.model.Product;
import com.alfarabi.base.model.location.Province;
import com.alfarabi.base.model.location.Subdistrict;
import com.alfarabi.base.model.Tenor;
import com.alfarabi.base.model.location.ZipCode;
import com.alfarabi.base.realm.RealmDao;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang.time.DateFormatUtils;
import org.parceler.Parcel;

import java.util.Date;
import java.util.HashMap;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 7/31/17.
 */

@Parcel
public class KPOrderParam {

    @SerializedName("id")@Getter @Setter long id ;
    @SerializedName("deal_code_number")@Getter @Setter private String dealCodeNumber ;
    @SerializedName("tenor")@Getter@Setter Tenor tenor ;
    @SerializedName("identity_code")@Getter@Setter private String identityCode ;
    @SerializedName("full_name")@Getter@Setter private String fullname ;
    @SerializedName("email")@Getter@Setter private String email ;
    @SerializedName("mobile_phone_number")@Getter@Setter String mobilePhoneNumber ;
    @SerializedName("home_phone_prefix")@Getter@Setter String homePhonePrefix ;
    @SerializedName("home_phone_number")@Getter@Setter String homePhoneNumber ;
    @SerializedName("office_phone_prefix")@Getter@Setter String officePhonePrefix ;
    @SerializedName("office_phone_number")@Getter@Setter String officePhoneNumber ;

    @SerializedName("gender")@Getter@Setter private String gender ;
    @SerializedName("birthcity")@Getter@Setter private String birthcity ;
    @SerializedName("birthday") @Getter@Setter Date birthday ;
    @SerializedName("mothername")@Getter@Setter private String mothername ;
    @SerializedName("graduation")@Getter@Setter private String graduation ;
    @SerializedName("marital_status")@Getter@Setter private String maritalStatus ;
    @SerializedName("dependent")@Getter@Setter private String dependent ;
    @SerializedName("residential_status")@Getter@Setter private String residentialStatus ;
    @SerializedName("have_stayed_for")@Getter@Setter private String haveStayedfor ;


    @SerializedName("employment_type")@Getter@Setter private String employmentType ;
    @SerializedName("employment_status")@Getter@Setter private String employmentStatus ;
    @SerializedName("working_from")@Getter@Setter private String workingFrom ;
    @SerializedName("working_until")@Getter@Setter private String workingUntil ;
    @SerializedName("occupation")@Getter@Setter private String occupation ;
    @SerializedName("working_position")@Getter@Setter private String workingPosition ;
    @SerializedName("salary")@Getter@Setter private String salary ;

    @SerializedName("address")@Getter@Setter private String address ;
    @SerializedName("address2")@Getter@Setter private String address2 ;
    @SerializedName("province")@Getter@Setter private Province province ;
    @SerializedName("branch")@Getter@Setter private String branch ;

    @SerializedName("city")@Getter@Setter private City city ;
    @SerializedName("kecamatan")@Getter@Setter private District district ;
    @SerializedName("kelurahan")@Getter@Setter private Subdistrict subdistrict ;
    @SerializedName("zipcode")@Getter@Setter private ZipCode zipCode ;
    @SerializedName("product")@Getter@Setter Product product;
    @SerializedName("checkout")@Getter@Setter
    KPCheckout KPCheckout;
    @SerializedName("checkout_param")@Getter@Setter
    KPCheckoutParam KPCheckoutParam;
    @SerializedName("insurance_id")@Getter@Setter private String insuranceId ;


    public String toGson(){
        this.province = RealmDao.unmanage(province);
        this.city = RealmDao.unmanage(city);
        this.district = RealmDao.unmanage(district);
        this.subdistrict = RealmDao.unmanage(subdistrict);
        this.zipCode = RealmDao.unmanage(zipCode);
        this.tenor = RealmDao.unmanage(tenor);
        return HttpInstance.getGson().toJson(this);
    }

//    {
//        "id_number": "1234567890123456",
//            "deal_code_number": "1501208566",
//            "full_name": "Azis Novian",
//            "birth_date": "1992-11-22",
//            "birth_place": "Sumedang",
//            "email": "azisnovian.kom48@gmail.com",
//            "address": "Jl. Cikadu Situraja",
//            "address2": "-",
//            "country": "Indonesia",
//            "region": "BEKASI",
//            "city": "BEKASI",
//            "kecamatan": "BANTARGEBANG",
//            "kelurahan": "CIKETING UDIK",
//            "zip_code": "17153",
//            "mother_name": "Nama Ibu",
//            "home_phone_area": "021",
//            "home_phone_number": "845794399",
//            "mobile_phone_number": "085794313256",
//            "job_type": "P",
//            "job_status": "Teks",
//            "job_position": "Teks",
//            "tenor": 6,
//            "office_area": "021",
//            "office_phone": "798798775",
//            "education": "S1",
//            "marital": "Kawin",
//            "tanggungan": "4",
//            "length_of_stay": "15",
//            "domisili_status": "Milik Sendiri",
//            "profession": "Kodingerz",
//            "work_from": "1995",
//            "work_to": "2011",
//            "salary": 15000000,
//            "insurance_id": "2"
//    }

    public HashMap toHasmap(){
        HashMap hashMap = new HashMap<>();
        hashMap.put("id_number", identityCode);
        hashMap.put("full_name", fullname);
        hashMap.put("deal_code_number", dealCodeNumber);
        hashMap.put("birth_date", DateFormatUtils.format(birthday, "yyyy-MM-dd"));
        hashMap.put("birth_place", birthcity);
        hashMap.put("email", email);
        hashMap.put("address", address);
        hashMap.put("address2", address2);
        hashMap.put("country", "Indonesia");
        hashMap.put("region", branch);
        hashMap.put("city", city.getName());
        hashMap.put("kecamatan", district.getName());
        hashMap.put("kelurahan", subdistrict.getName());
        hashMap.put("zip_code", zipCode.getZipcode());
        hashMap.put("mother_name", mothername);
        hashMap.put("home_phone_area", homePhonePrefix);
        hashMap.put("home_phone_number", homePhoneNumber);
        hashMap.put("mobile_phone_number", mobilePhoneNumber);// Mobile Phone
        hashMap.put("job_type", employmentType);
        hashMap.put("job_status", employmentStatus);
        hashMap.put("job_position", workingPosition);
        hashMap.put("tenor", tenor.getTenor());
        hashMap.put("office_area", officePhonePrefix);
        hashMap.put("office_phone", officePhoneNumber);
        hashMap.put("education", graduation);
        hashMap.put("marital", maritalStatus);
        hashMap.put("tanggungan", dependent);
        hashMap.put("length_of_stay", haveStayedfor);
        hashMap.put("domisili_status", residentialStatus);
        hashMap.put("profession",occupation);
        hashMap.put("work_from", workingFrom);
        hashMap.put("work_to", workingUntil);
        hashMap.put("salary", salary);
        hashMap.put("insurance_id", salary);

//        hashMap.put("product_id", String.valueOf(product.getId()));
//        hashMap.put("gender", gender);

//        hashMap.put("payment_method", "kreditplus");

        return hashMap ;
    }

    public String toPostGson(){
        return HttpInstance.getGson().toJson(toHasmap());
    }

}
