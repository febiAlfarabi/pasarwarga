package com.alfarabi.base.model.transaction.purchase;

import com.alfarabi.alfalibs.helper.model.ObjectAdapterInterface;
import com.alfarabi.base.model.Payment;
import com.alfarabi.base.model.Product;
import com.alfarabi.base.model.Seller;
import com.alfarabi.base.model.Tenor;
import com.alfarabi.base.model.location.Address;
import com.alfarabi.base.model.shop.SellerInfo;
import com.alfarabi.base.model.transaction.Cart;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.Date;

import io.realm.PurchaseRealmProxy;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 9/19/17.
 */

@Parcel(value = Parcel.Serialization.BEAN, implementations = {PurchaseRealmProxy.class}, analyze = {Purchase.class})
public class Purchase extends RealmObject implements ObjectAdapterInterface {

    @PrimaryKey@SerializedName("id")@Getter@Setter private String id ;
    @SerializedName("user_id")@Getter@Setter String userId ;
    @SerializedName("seller")@Getter@Setter
    SellerInfo sellerInfo ;
    @SerializedName("product")@Getter@Setter Product product ;
    @SerializedName("deal_code_number")@Getter@Setter String dealCodeNumber ;
    @SerializedName("quantity")@Getter@Setter String quantity ;
    @SerializedName("shipping_cost")@Getter@Setter float shippingCost ;
    @SerializedName("admin_fee")@Getter@Setter float adminFee ;
    @SerializedName("ind_total")@Getter@Setter float indTotal ;
    @SerializedName("total")@Getter@Setter float total ;
    @SerializedName("address_id")@Getter@Setter String addressId ;
    @SerializedName("status")@Getter@Setter String status ;
    @SerializedName("created")@Getter@Setter String created ;
    @SerializedName("shipp_code")@Getter@Setter String shippCode ;
    @SerializedName("shipping_status")@Getter@Setter String shippingStatus ;
    @SerializedName("tenor")@Getter@Setter Tenor tenor ;
    @SerializedName("payment_method")@Getter@Setter Payment paymentMethod ;
    @SerializedName("note")@Getter@Setter String note ;
    @SerializedName("tracking_id")@Getter@Setter String trackingId ;
    @SerializedName("expired_date")@Getter@Setter String expiredDate ;
    @SerializedName("insurance")@Getter@Setter boolean insurance ;
    @SerializedName("insurance_value")@Getter@Setter double insuranceValue ;
    @SerializedName("enable_feedback")@Getter@Setter boolean enableFeedback ;
    @SerializedName("rating")@Getter@Setter float rating ;
    @SerializedName("shipping_address")@Getter@Setter Address shippingAddress ;

    @Override
    public long getHeaderId() {
        return 0;
    }

    @Override
    public boolean isSearchable() {
        return false;
    }

    @Override
    public String canSearchByField() {
        return null;
    }

//    "id": "5744",
//            "user_id": "164",
//            "seller": {
//        "id": "171",
//                "business_name": "KING CELL"
//    },
//            "product": {
//        "id": "20331",
//                "name": "LENOVO K6 NOTE (SILVER/GREY/GOLD) NEW ARRIVAL",
//                "image": "https://s3-ap-southeast-1.amazonaws.com/pasarwarga/images/product/1500449491-lenovok62.jpg",
//                "base_price": 2899000,
//                "discount": 0,
//                "price_after_discount": 2899000
//    },
//            "deal_code_number": "1501474193",
//            "quantity": "1",
//            "shipping_cost": 8000,
//            "admin_fee": 50000,
//            "indtotal": 2899000,
//            "total": 2949000,
//            "address_id": "52",
//            "status": "Approve",
//            "created": "2017-07-31 11:09:53",
//            "shipp_code": "jne",
//            "shipping_status": "Shipped",
//            "tenor": {
//        "months": 6,
//                "amount": 611000
//    },
//            "payment_method": {
//        "id": "11",
//                "gateway_name": "kreditplus"
//    },
//            "note": "",
//            "tracking_id": "015720004402117",
//            "expired_date": "2017-08-03 23:59:00",
//            "enable_feedback": true,
//            "rating": 0
//}
}
