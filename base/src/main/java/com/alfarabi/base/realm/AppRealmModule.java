package com.alfarabi.base.realm;

import io.realm.annotations.RealmModule;

/**
 * Created by User on 26/03/2017.
 */

@RealmModule(allClasses = true, library = true)
public class AppRealmModule {}
