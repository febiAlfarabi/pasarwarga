package com.alfarabi.base.realm;

import android.content.Intent;

import com.alfarabi.alfalibs.tools.WLog;
import com.alfarabi.base.exception.LoginException;
import com.alfarabi.base.model.MyProfile;
import com.google.gson.Gson;

import java.lang.reflect.InvocationTargetException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.realm.DynamicRealm;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by Alfarabi on 7/11/17.
 */

public class RealmDao<T extends RealmObject> {



    public static final Realm getRealm(){
        return realmEngine ;
    }

    public static <T extends RealmObject> T unmanage(T object){
        if(object==null){
            return null;
        }
        if(!(object instanceof RealmObject)){
            throw  new RuntimeException("Your passed object is not instance of Realm Object");
        }
        if(object.isManaged() && object.isValid()){
            return object = realmEngine.copyFromRealm(object);
        }
        return object ;
    }

    public static <Y extends RealmObject> List<Y> unmanage(RealmList<Y> objects){
        if(objects==null){
            return null;
        }
        if(!(objects instanceof RealmList)){
            throw  new RuntimeException("Your passed object is not instance of Realm Object");
        }
        List<Y> arrayList = new ArrayList<Y>();
        for (int i = 0; i < objects.size(); i++) {
            arrayList.add(unmanage(objects.get(i)));
        }
        return arrayList ;
    }

    public static <Y extends RealmObject> List<Y> unmanage(List<Y> objects){
        if(objects==null){
            return null;
        }
        List<Y> arrayList = new ArrayList<Y>();
        for (int i = 0; i < objects.size(); i++) {
            arrayList.add(unmanage(objects.get(i)));
        }
        return arrayList ;
    }

    public static <T extends RealmObject> RealmDao<T> instance(){
        RealmDao<T> realmDao = new RealmDao<T>();
        return realmDao ;
    }


    public <T extends RealmObject> T insertOrUpdate(T object){

        if(object==null){
            throw new RuntimeException("Param Realm Object is exactly null, Cannot insert it to realm database");
        }
        if (object!=null && object.isManaged()){
            T copyFromRealm = realmEngine.copyFromRealm(object);
            realmEngine.executeTransaction(realm -> {
                realm.insertOrUpdate(copyFromRealm);
            });
        }else if(object!=null){
            realmEngine.executeTransaction(realm -> {
                realm.insertOrUpdate(object);
            });
        }
        return object ;
    }


    public <T extends RealmObject> void insertOrUpdateCollection(List<T> objects){
        if(objects!=null){
            WLog.i(TAG, String.valueOf(objects.size()));
            WLog.i(TAG, new Gson().toJson(objects));

            for (T t : objects) {
                insertOrUpdate(t);
            }
        }
    }

    public static RealmList listToRealmList(List objects){
        RealmList realmList = new RealmList();
        realmList.addAll(objects);
        return realmList ;
    }

    public <T extends RealmObject> void renew(List<T> objects, Class<T> clazz){
        deleteAll(clazz);
        insertOrUpdateCollection(objects);
    }

    public <T extends RealmObject> void delete(long id, Class<T> clazz){
        realmEngine.executeTransaction(realm -> {
            realm.where(clazz).equalTo("id", id).findAll().deleteAllFromRealm();
        });
    }

    public <T extends RealmObject> void deleteAll(Class<T> clazz){
        realmEngine.executeTransaction(realm -> {
            realm.where(clazz).findAll().deleteAllFromRealm();
        });
    }

    public <T extends RealmObject> T get(long id, Class<T> clazz){
        realmEngine.beginTransaction();
        T object =  realmEngine.where(clazz).equalTo("id", id).findFirst();
        realmEngine.commitTransaction();
        return unmanage(object) ;
    }

    public <T extends RealmObject> T get(Class<T> clazz){
        realmEngine.beginTransaction();
        T object =  realmEngine.where(clazz).findFirst();
        realmEngine.commitTransaction();
        return unmanage(object) ;
    }

    public <T extends RealmObject> boolean rowExist(Class<T> clazz){
        if(get(clazz)!=null){
            return true ;
        }else{
            return false ;
        }
    }

    public <T extends RealmObject> boolean checkLogin(Class<MyProfile> clazz, Intent observable) throws LoginException{
        if(get(clazz)!=null){
            return true ;
        }else{
            throw new LoginException();
        }
    }

//    public <T extends RealmObject> List<T> getList(Class<T> clazz){
//        realmEngine.beginTransaction();
//        List<T> object =  realmEngine.where(clazz).findAll();
//        realmEngine.commitTransaction();
//        return object ;
//    }

    public <T extends RealmObject> List<T> getList(Class<T> clazz, boolean manage){
        realmEngine.beginTransaction();
        List<T> objects = realmEngine.where(clazz).findAll();
        realmEngine.commitTransaction();
        if(!manage){
            objects = unmanage(objects);
        }
        return objects ;
    }
    public <T extends RealmObject> List<T> getList(Class<T> clazz, String sortBy){
        realmEngine.beginTransaction();
        List<T> object =  realmEngine.where(clazz).findAllSorted(sortBy);
        realmEngine.commitTransaction();
        return object ;
    }

    public <T extends RealmObject> List<T> getList(Class<T> clazz, String sortBy, boolean manage){
        realmEngine.beginTransaction();
        List<T> objects =  realmEngine.where(clazz).findAllSorted(sortBy);
        realmEngine.commitTransaction();
        if(!manage){
            objects = unmanage(objects);
        }
        return objects ;
    }

    public <T extends RealmObject> List<T> getList(Class<T> clazz, String sortBy, int limit, boolean manage){
        realmEngine.beginTransaction();
        List<T> objects =  realmEngine.where(clazz).findAllSorted(sortBy);
//        objects =  realmEngine.where(clazz).between("id", objects.size()-limit, objects.size()).findAllSorted(sortBy);
        objects =  objects.subList(0, limit);
        realmEngine.commitTransaction();
        if(!manage){
            objects = unmanage(objects);
        }
        return objects ;
    }

    public static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public static SecureRandom rnd = new SecureRandom();

    public static final String TAG = RealmDao.class.getName();

    static Realm realmEngine = Realm.getDefaultInstance();



    public static final String uniq(int len, Class object, String field){
        StringBuilder sb = new StringBuilder( len );
        for( int i = 0; i < len; i++ ) {
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        }
        if(RealmDao.getRealm().where(object).equalTo(field, sb.toString()).findFirst()!=null){
            return uniq(len, object, field);
        }
        return sb.toString();
    }

    public static Number currentId(Class clazz){
        return RealmDao.getRealm().where(clazz).max("id")==null?1:RealmDao.getRealm().where(clazz).max("id");
    }
    public static Number currentId(Class clazz, String key){
        return RealmDao.getRealm().where(clazz).max(key)==null?1:RealmDao.getRealm().where(clazz).max(key);
    }

    public static Long nextId(Class clazz){
        Number number = currentId(clazz);
        return number.longValue()+1;
    }
    public static Long nextId(Class clazz, String key){
        Number number = currentId(clazz, key);
        return number.longValue()+1;
    }

    public static Number currentId(Realm realm, Class clazz){
        return realm.where(clazz).max("id")==null?1:realm.where(clazz).max("id");
    }
    public static Number currentId(DynamicRealm realm, Class clazz){
        return realm.where(clazz.getName()).max("id")==null?1:realm.where(clazz.getName()).max("id");
    }

    public static Number currentId(Realm realm, Class clazz, String key){
        return realm.where(clazz).max(key)==null?1:realm.where(clazz).max(key);
    }

    public static Long nextId(Realm realm, Class clazz){
        Number number = currentId(realm, clazz);
        return number.longValue()+1;
    }
    public static Long nextId(DynamicRealm realm, Class clazz){
        Number number = currentId(realm, clazz);
        return number.longValue()+1;
    }
    public static Long nextId(Realm realm, Class clazz, String key){
        Number number = currentId(realm, clazz, key);
        return number.longValue()+1;
    }





    public static <T extends RealmObject & RealmHelper> T create(Class<T> clazz){
        T t = null ;
        RealmDao.getRealm().beginTransaction();
        try {
            t = (T) clazz.getConstructor().newInstance().create();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        RealmDao.getRealm().commitTransaction();
        return t;
    }



    public <T extends RealmObject> T renew(long id, T object){
        if(object==null){
            throw new RuntimeException("Param Realm Object is exactly null, Cannot insert it to realm database");
        }
        delete(id, object.getClass());
        return insertOrUpdate(object);
    }



    public <T extends RealmObject> T get(Class<T> clazz, boolean manage){
        realmEngine.beginTransaction();
        T object =  realmEngine.where(clazz).findFirst();
        realmEngine.commitTransaction();
        if(!manage){
            return unmanage(object);
        }
        return object ;
    }


    public <T extends RealmObject> List<T> getList(Class<T> clazz){
        realmEngine.beginTransaction();
        List<T> object =  realmEngine.where(clazz).findAll();
        realmEngine.commitTransaction();
        return object ;
    }


    public <T extends RealmObject> List<T> getList(Class<T> clazz, String sortBy, Sort sort){
        realmEngine.beginTransaction();
        List<T> object =  realmEngine.where(clazz).findAllSorted(sortBy, sort);
        realmEngine.commitTransaction();
        return object ;
    }

    public <T extends RealmObject> RealmResults<T> getRealmList(Class<T> clazz){
        realmEngine.beginTransaction();
        RealmResults<T> object =  realmEngine.where(clazz).findAll();
        realmEngine.commitTransaction();
        return object ;
    }


    public <T extends RealmObject> RealmResults<T> getRealmList(Class<T> clazz, String sortBy){
        realmEngine.beginTransaction();
        RealmResults<T> object =  realmEngine.where(clazz).findAllSorted(sortBy);
        realmEngine.commitTransaction();
        return object ;
    }



    public <T extends RealmObject> List<T> getList(Class<T> clazz, String where, long value){
        realmEngine.beginTransaction();
        List<T> object =  realmEngine.where(clazz).equalTo(where, value).findAll();
        realmEngine.commitTransaction();
        return object ;
    }
    public <T extends RealmObject> List<T> getList(Class<T> clazz, String where, String value){
        realmEngine.beginTransaction();
        List<T> object =  realmEngine.where(clazz).equalTo(where, value).findAll();
        realmEngine.commitTransaction();
        return object ;
    }

    public <T extends RealmObject> List<T> getList(Class<T> clazz, String where, String value, String whereNotEqual, String valueNotEqual){
        realmEngine.beginTransaction();
        List<T> object =  realmEngine.where(clazz).equalTo(where, value).notEqualTo(whereNotEqual, valueNotEqual).findAll();
        realmEngine.commitTransaction();
        return object ;
    }

    public <T extends RealmObject> List<T> getList(Class<T> clazz, String where, String value, String whereNotEqual, long valueNotEqual){
        realmEngine.beginTransaction();
        List<T> object =  realmEngine.where(clazz).equalTo(where, value).notEqualTo(whereNotEqual, valueNotEqual).findAll();
        realmEngine.commitTransaction();
        return object ;
    }






}
