package com.alfarabi.base.realm;


import io.realm.RealmObject;

/**
 * Created by User on 10/05/2017.
 */

public interface RealmHelper<T extends RealmObject> {

    public T create();



}