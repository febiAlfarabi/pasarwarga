package com.alfarabi.base.service;

import android.app.Notification;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.WLog;
import com.alfarabi.base.R;
import com.alfarabi.base.model.notification.NotificationDetail;
import com.alfarabi.base.realm.RealmDao;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

/**
 * Created by alfarabi on 7/23/17.
 */

public class BaseFirebaseMessagingService extends FirebaseMessagingService {

    public static final String TAG = BaseFirebaseMessagingService.class.getName();
    protected Uri notifSound ;

    @Override
    public void onCreate() {
        super.onCreate();
        notifSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
    }



    @Override
    public void onMessageSent(String s) {
        super.onMessageSent(s);
        WLog.i(TAG, "onMessageSent "+s);
    }
}
