package com.alfarabi.base.service;

import com.alfarabi.alfalibs.tools.WLog;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.hendraanggrian.preferencer.Preferencer;
import com.hendraanggrian.preferencer.Saver;
import com.hendraanggrian.preferencer.annotations.BindPreference;

/**
 * Created by alfarabi on 7/23/17.
 */

public class MyFirebaseInstanceIdService extends FirebaseInstanceIdService {

    public static final String TAG = MyFirebaseInstanceIdService.class.getName();

    private Saver saver ;
    @BindPreference String fcmToken ;


    @Override
    public void onCreate() {
        super.onCreate();
        saver = Preferencer.bind(this);
    }

    @Override
    public void onTokenRefresh() {
        fcmToken = FirebaseInstanceId.getInstance().getToken();
        WLog.d(TAG, "Refreshed token: " + fcmToken);
        saver.saveAll();
        sendRegistrationToServer();
    }

    public void sendRegistrationToServer(){
        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.

    }

}
