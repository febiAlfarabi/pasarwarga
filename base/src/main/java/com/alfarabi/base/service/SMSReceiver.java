/*
 Copyright 2008 Wissen Systems. All rights reserved.
 Author: Prashant Kalkar on 7:45:37 PM
 */
package com.alfarabi.base.service;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.gsm.SmsMessage;
import android.widget.Toast;

import com.alfarabi.alfalibs.tools.WLog;
import com.alfarabi.base.tools.Initial;

/**
 * The class is called when SMS is received.
 * 
 * @author Prashant Kalkar
 * 
 * Create Date : Nov 23, 2008
 */
public class SMSReceiver extends BroadcastReceiver {
    
    public static final String TAG = SMSReceiver.class.getName();

    private static SmsListener smsListener ;

    /**
     * @see android.content.BroadcastReceiver#onReceive(android.content.Context, android.content.Intent)
     */
    @Override
    public void onReceive(Context context, Intent intent) {
//        Bundle bundle = intent.getExtras();
//
//        Object messages[] = (Object[]) bundle.get("pdus");
//        SmsMessage smsMessage[] = new SmsMessage[messages.length];
//        for (int n = 0; n < messages.length; n++) {
//            smsMessage[n] = SmsMessage.createFromPdu((byte[]) messages[n]);
//        }
//
//        // show first message
//        Toast toast = Toast.makeText(context, "Received SMS: " + smsMessage[0].getMessageBody(), Toast.LENGTH_LONG);
//        toast.show();
//        WLog.i(TAG, smsMessage[0].getMessageBody());
        Bundle data  = intent.getExtras();

        Object[] pdus = (Object[]) data.get("pdus");

        for(int i=0;i<pdus.length;i++){
            SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) pdus[i]);

            String sender = smsMessage.getDisplayOriginatingAddress();
            //Check the sender to filter messages which we require to read
            WLog.i(TAG, smsMessage.getDisplayOriginatingAddress());
            WLog.i(TAG, smsMessage.getMessageBody());
            String message = smsMessage.getMessageBody();
            if(message.contains("pasarwarga.com")){
                if(smsListener!=null){
                    smsListener.messageReceived(message);
                }
            }

            if (sender.equals(Initial.SMS_SENDER)) {
                String messageBody = smsMessage.getMessageBody();
                smsListener.messageReceived(messageBody);
            }
        }
    }

    public static void bindListener(SmsListener listener) {
        smsListener = listener;
    }
}