package com.alfarabi.base.service;


public interface SmsListener {
    public void messageReceived(String messageText);
}