package com.alfarabi.base.tools;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.vansuita.library.CheckNewAppVersion;

/**
 * Created by Alfarabi on 10/23/17.
 */

public class GooglePlayUtil {


    public static void openUpdateLink(Context context) {
        context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(GooglePlayUtil.getExternalAppLink(context))));
    }

    public static String getExternalAppLink(Context context) {
        return String.format("http://play.google.com/store/apps/details?id=%s&hl=en", new Object[]{context.getPackageName()});
    }

}
