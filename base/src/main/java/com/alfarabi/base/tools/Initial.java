package com.alfarabi.base.tools;

import android.app.Activity;
import android.widget.Toast;

import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.base.http.basemarket.service.CityService;
import com.alfarabi.base.realm.RealmDao;
import com.tuyenmonkey.mkloader.MKLoader;

import io.realm.Realm;

/**
 * Created by User on 02/07/2017.
 */

public class Initial {


    public static final String LANGUAGE = "language";

    public static final String SMS_SENDER = "PASARWARGA";
    public static final String ABOUT_PASARWARGA_WEB_URL = "https://www.pasarwarga.com/mobile/pages/about-us";
    public static final String INVOICE_ENDPOINT = "api/v2/order/download_invoice/"; //userid/deal_code_number
    public static final String DOMAIN_MOCK = "https://www.mock.com";
    public static final String API_VERSION = "/api/v1";
    public static final String PW = "/pw" ;
    public static final String API = "/api/v2" ;
    public static final String PW_API = API ;

    public static final String TOKEN = "Token";
    public static final String X_CLIENT_CODE = "X-Client-Code";


    public static final String CONTENT_TYPE = "Content-Type";
    public static final String DEVICE_TYPE = "Device-Type";
    public static final String ANDROID = "Android";

    public static final String getApi(){
        return API_VERSION;
    }

    public static final String GOOGLE_PLACE_KEY = "AIzaSyCwnI7xeujeiZiGsiYFBxG2IqLhIoX_3oY";
    public static final String EN = "en";

    static {
        getApi();
    }

    public static final String CATEGORY = "CATEGORY";
    public static final String NOTES = "NOTES.";
    public static final String ADDRESS = "ADDRESS";

    public static final String ACTIVE = "active";
    public static final String WAITING = "waiting";
    public static final String TODAY = "today";



    public static final String MALE = "male";
    public static final String FEMALE = "female";



    /*
        Status Order
     */


    public static final String PENDING = "pending";
    public static final String PROCESSED = "processed";
    public static final String SHIPPED = "shipped";
    public static final String DELIVERED = "delivered";

    public static final double MIN_PRODUCT_PRICE = 1250000;

    public static final int CHAT_FRAGMENT = 12;
    public static final String FLAG_CHAT_FRAGMENT = "FLAG_CHAT_FRAGMENT";


    public static final String FLAG_PENDING_INTENT = "FLAG_PENDING_INTENT";


    public static void loadCities(Activity activity){
        HttpInstance.call(HttpInstance.create(CityService.class).getAllCityByCountry(CityService.IND_COUNTRY_ID), cityResponse -> {
            ResponseTools.validate(activity, cityResponse);
            if(cityResponse.isStatus()){
                RealmDao.getRealm().executeTransaction(realm -> {
                    realm.insertOrUpdate(cityResponse.getCities());
                });
            }
        }, throwable -> {
            Toast.makeText(activity, throwable.getMessage(), Toast.LENGTH_SHORT).show();
        }, false);
    }




}
