package com.alfarabi.base.tools;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;

import java.util.List;

/**
 * Created by Alfarabi on 10/19/17.
 */

public class IntentTools {


    public static void goToPdfApps(Activity activity, String path) throws Exception{
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(path));
        intent.setType("application/pdf");
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PackageManager pm = activity.getPackageManager();
        List<ResolveInfo> activities = pm.queryIntentActivities(intent, 0);
        if (activities.size() > 0) {
            activity.startActivity(intent);
        }else{
            throw new Exception("Application Not Found");
        }
    }
}
