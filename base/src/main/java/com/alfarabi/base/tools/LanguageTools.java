package com.alfarabi.base.tools;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.preference.PreferenceManager;

import com.hendraanggrian.preferencer.Preferencer;
import com.hendraanggrian.preferencer.Saver;

import java.util.Locale;

/**
 * Created by Alfarabi on 10/18/17.
 */

public class LanguageTools {


    public static void initLanguage(Activity context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String language = prefs.getString(Initial.LANGUAGE, "in");
//        String language  = "in"; // your language
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
    }
}
