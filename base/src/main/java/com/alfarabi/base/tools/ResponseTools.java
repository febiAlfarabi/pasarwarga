package com.alfarabi.base.tools;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.Fragment;

import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.activity.UnderMaintenanceActivity;
import com.alfarabi.base.http.basemarket.response.Response;
import com.alfarabi.base.model.MyProfile;
import com.alfarabi.base.model.chat.Conversation;
import com.alfarabi.base.model.chat.UserConversation;
import com.alfarabi.base.realm.RealmDao;

/**
 * Created by Alfarabi on 8/3/17.
 */

public class ResponseTools {

    public static void validate(Activity activity, Response response) throws Exception{
        if(!response.isStatus() && response.getCode()==203){
            PackageManager pm = activity.getPackageManager();
            Intent intent = pm.getLaunchIntentForPackage(activity.getPackageName());
            intent.putExtra("message", response.getMessage());
            activity.getWindow().getDecorView().postDelayed(() -> {
                RealmDao.instance().deleteAll(MyProfile.class);
                RealmDao.instance().deleteAll(UserConversation.class);
                RealmDao.instance().deleteAll(Conversation.class);
                HttpInstance.getHeaderMap().remove(Initial.TOKEN);
                WindowFlow.restart(activity, intent);
            }, 2000);
            throw new Exception(response.getMessage());
        }else if(!response.isStatus() && response.getCode()==404){
            PackageManager pm = activity.getPackageManager();
            Intent intent = new Intent(activity, UnderMaintenanceActivity.class);
            intent.putExtra("error_code", String.valueOf(response.getCode()));
            activity.getWindow().getDecorView().postDelayed(() -> {
                activity.startActivity(intent);
            }, 2000);
            throw new Exception(response.getMessage());
        }else if(!response.isStatus() && response.getCode()==503){
            PackageManager pm = activity.getPackageManager();
            Intent intent = new Intent(activity, UnderMaintenanceActivity.class);
            intent.putExtra("error_code", String.valueOf(response.getCode()));
            activity.getWindow().getDecorView().postDelayed(() -> {
                activity.finish();
                activity.startActivity(intent);
            }, 2000);
            throw new Exception(response.getMessage());
        }
    }
    public static void validate(Fragment fragment, Response response) throws Exception{
        if(!response.isStatus() && response.getCode()==203){
            PackageManager pm = fragment.getActivity().getPackageManager();
            Intent intent = pm.getLaunchIntentForPackage(fragment.getActivity().getPackageName());
            intent.putExtra("message", response.getMessage());
            fragment.getActivity().getWindow().getDecorView().postDelayed(() -> {
                RealmDao.instance().deleteAll(MyProfile.class);
                RealmDao.instance().deleteAll(UserConversation.class);
                RealmDao.instance().deleteAll(Conversation.class);
                HttpInstance.getHeaderMap().remove(Initial.TOKEN);
                WindowFlow.restart(fragment.getActivity(), intent);
            }, 2000);
            throw new Exception(response.getMessage());
        }else if(!response.isStatus() && response.getCode()==404){
            PackageManager pm = fragment.getActivity().getPackageManager();
            Intent intent = new Intent(fragment.getActivity(), UnderMaintenanceActivity.class);
            intent.putExtra("error_code", String.valueOf(response.getCode()));
            fragment.getActivity().getWindow().getDecorView().postDelayed(() -> {
                fragment.getActivity().startActivity(intent);
            }, 2000);
            throw new Exception(response.getMessage());
        }else if(!response.isStatus() && response.getCode()==503){
            PackageManager pm = fragment.getActivity().getPackageManager();
            Intent intent = new Intent(fragment.getActivity(), UnderMaintenanceActivity.class);
            intent.putExtra("error_code", String.valueOf(response.getCode()));
            fragment.getActivity().getWindow().getDecorView().postDelayed(() -> {
                fragment.getActivity().finish();
                fragment.getActivity().startActivity(intent);
            }, 2000);
            throw new Exception(response.getMessage());
        }
    }
    public static void validate(android.app.Fragment fragment, Response response) throws Exception{
        if(!response.isStatus() && response.getCode()==203){
            PackageManager pm = fragment.getActivity().getPackageManager();
            Intent intent = pm.getLaunchIntentForPackage(fragment.getActivity().getPackageName());
            intent.putExtra("message", response.getMessage());
            fragment.getActivity().getWindow().getDecorView().postDelayed(() -> {
                RealmDao.instance().deleteAll(MyProfile.class);
                RealmDao.instance().deleteAll(UserConversation.class);
                RealmDao.instance().deleteAll(Conversation.class);
                HttpInstance.getHeaderMap().remove(Initial.TOKEN);
                WindowFlow.restart(fragment.getActivity(), intent);
            }, 2000);
            throw new Exception(response.getMessage());
        }else if(!response.isStatus() && response.getCode()==404){
            PackageManager pm = fragment.getActivity().getPackageManager();
            Intent intent = new Intent(fragment.getActivity(), UnderMaintenanceActivity.class);
            intent.putExtra("error_code", String.valueOf(response.getCode()));
            fragment.getActivity().getWindow().getDecorView().postDelayed(() -> {
                fragment.getActivity().startActivity(intent);
            }, 2000);
            throw new Exception(response.getMessage());
        }else if(!response.isStatus() && response.getCode()==503){
            PackageManager pm = fragment.getActivity().getPackageManager();
            Intent intent = new Intent(fragment.getActivity(), UnderMaintenanceActivity.class);
            intent.putExtra("error_code", String.valueOf(response.getCode()));
            fragment.getActivity().getWindow().getDecorView().postDelayed(() -> {
                fragment.getActivity().finish();
                fragment.getActivity().startActivity(intent);
            }, 2000);
            throw new Exception(response.getMessage());
        }
    }

    public static void validate(Activity activity, Response response, boolean checkStatus) throws Exception{
        if(!response.isStatus() && response.getCode()==203){
            PackageManager pm = activity.getPackageManager();
            Intent intent = pm.getLaunchIntentForPackage(activity.getPackageName());
            intent.putExtra("message", response.getMessage());
            activity.getWindow().getDecorView().postDelayed(() -> {
                RealmDao.instance().deleteAll(MyProfile.class);
                RealmDao.instance().deleteAll(UserConversation.class);
                RealmDao.instance().deleteAll(Conversation.class);
                HttpInstance.getHeaderMap().remove(Initial.TOKEN);
                WindowFlow.restart(activity, intent);
            }, 2000);
            throw new Exception(response.getMessage());
        }else if(!response.isStatus() && response.getCode()==404){
            PackageManager pm = activity.getPackageManager();
            Intent intent = new Intent(activity, UnderMaintenanceActivity.class);
            intent.putExtra("error_code", String.valueOf(response.getCode()));
            activity.getWindow().getDecorView().postDelayed(() -> {
                activity.startActivity(intent);
            }, 2000);
            throw new Exception(response.getMessage());
        }else if(!response.isStatus() && response.getCode()==503){
            PackageManager pm = activity.getPackageManager();
            Intent intent = new Intent(activity, UnderMaintenanceActivity.class);
            intent.putExtra("error_code", String.valueOf(response.getCode()));
            activity.getWindow().getDecorView().postDelayed(() -> {
                activity.finish();
                activity.startActivity(intent);
            }, 2000);
            throw new Exception(response.getMessage());
        }else if(checkStatus && !response.isStatus()){
            throw new Exception(response.getMessage());
        }
    }
    public static void validate(Fragment fragment, Response response, boolean checkStatus) throws Exception{
        if(!response.isStatus() && response.getCode()==203){
            PackageManager pm = fragment.getActivity().getPackageManager();
            Intent intent = pm.getLaunchIntentForPackage(fragment.getActivity().getPackageName());
            intent.putExtra("message", response.getMessage());
            fragment.getActivity().getWindow().getDecorView().postDelayed(() -> {
                RealmDao.instance().deleteAll(MyProfile.class);
                RealmDao.instance().deleteAll(UserConversation.class);
                RealmDao.instance().deleteAll(Conversation.class);
                HttpInstance.getHeaderMap().remove(Initial.TOKEN);
                WindowFlow.restart(fragment.getActivity(), intent);
            }, 2000);
            throw new Exception(response.getMessage());
        }else if(!response.isStatus() && response.getCode()==404){
            PackageManager pm = fragment.getActivity().getPackageManager();
            Intent intent = new Intent(fragment.getActivity(), UnderMaintenanceActivity.class);
            intent.putExtra("error_code", String.valueOf(response.getCode()));
            fragment.getActivity().getWindow().getDecorView().postDelayed(() -> {
                fragment.getActivity().startActivity(intent);
            }, 2000);
            throw new Exception(response.getMessage());
        }else if(!response.isStatus() && response.getCode()==503){
            PackageManager pm = fragment.getActivity().getPackageManager();
            Intent intent = new Intent(fragment.getActivity(), UnderMaintenanceActivity.class);
            intent.putExtra("error_code", String.valueOf(response.getCode()));
            fragment.getActivity().getWindow().getDecorView().postDelayed(() -> {
                fragment.getActivity().finish();
                fragment.getActivity().startActivity(intent);
            }, 2000);
            throw new Exception(response.getMessage());
        }else if(checkStatus && !response.isStatus()){
            throw new Exception(response.getMessage());
        }
    }
    public static void validate(android.app.Fragment fragment, Response response, boolean checkStatus) throws Exception{
        if(!response.isStatus() && response.getCode()==203){
            PackageManager pm = fragment.getActivity().getPackageManager();
            Intent intent = pm.getLaunchIntentForPackage(fragment.getActivity().getPackageName());
            intent.putExtra("message", response.getMessage());
            fragment.getActivity().getWindow().getDecorView().postDelayed(() -> {
                RealmDao.instance().deleteAll(MyProfile.class);
                RealmDao.instance().deleteAll(UserConversation.class);
                RealmDao.instance().deleteAll(Conversation.class);
                HttpInstance.getHeaderMap().remove(Initial.TOKEN);
                WindowFlow.restart(fragment.getActivity(), intent);
            }, 2000);
            throw new Exception(response.getMessage());
        }else if(!response.isStatus() && response.getCode()==404){
            PackageManager pm = fragment.getActivity().getPackageManager();
            Intent intent = new Intent(fragment.getActivity(), UnderMaintenanceActivity.class);
            intent.putExtra("error_code", String.valueOf(response.getCode()));
            fragment.getActivity().getWindow().getDecorView().postDelayed(() -> {
                fragment.getActivity().startActivity(intent);
            }, 2000);
            throw new Exception(response.getMessage());
        }else if(!response.isStatus() && response.getCode()==503){
            PackageManager pm = fragment.getActivity().getPackageManager();
            Intent intent = new Intent(fragment.getActivity(), UnderMaintenanceActivity.class);
            intent.putExtra("error_code", String.valueOf(response.getCode()));
            fragment.getActivity().getWindow().getDecorView().postDelayed(() -> {
                fragment.getActivity().finish();
                fragment.getActivity().startActivity(intent);
            }, 2000);
            throw new Exception(response.getMessage());
        }else if(checkStatus && !response.isStatus()){
            throw new Exception(response.getMessage());
        }
    }
}
