package com.alfarabi.base.tools;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by Alfarabi on 10/23/17.
 */

public class WeightUtils {


    public static double gramToKg(int gram){
        //1 gram = 0.001 kilogram;
        double kilogram = gram * 0.001;
        return kilogram ;
    }
}
