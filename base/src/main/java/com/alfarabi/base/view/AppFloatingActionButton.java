package com.alfarabi.base.view;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;

/**
 * Created by Alfarabi on 9/5/17.
 */

public class AppFloatingActionButton extends FloatingActionButton {

    public AppFloatingActionButton(Context context) {
        super(context);
    }
}
