package com.alfarabi.base.view;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.alfarabi.alfalibs.tools.KeyboardUtils;
import com.alfarabi.base.R;
import com.alfarabi.base.view.interfaze.DataSpinnerInterface;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by User on 03/06/2017.
 */

public class DataMaterialSpinner<DSI extends DataSpinnerInterface> extends MaterialBetterSpinner {

    @Getter DSI selectedDsi;
    @Getter ThisAdapter<DSI> thisAdapter ;
    @Getter static boolean enableSpinner = true ;
    private List<DSI> dsis = new ArrayList<DSI>();


    public DataMaterialSpinner(Context context) {
        super(context);
        setFocusable(false);
    }
    public DataMaterialSpinner(Context arg0, AttributeSet arg1) {
        super(arg0, arg1);
        setFocusable(false);
    }

    public DataMaterialSpinner(Context arg0, AttributeSet arg1, int arg2) {
        super(arg0, arg1, arg2);
        setFocusable(false);
    }

    public void initDataAdapter(DSI[] dsis, AdapterView.OnItemClickListener onItemClickListener, DSI selectedDsi){
        List<DSI> list = new ArrayList<>();
        for (DSI dsi : dsis) {
            list.add(dsi);
        }
        initDataAdapter(list, onItemClickListener, selectedDsi);
    }

    public void initDataAdapter(List<DSI> dsis, AdapterView.OnItemClickListener onItemClickListener, DSI selectedDsi){
        this.dsis = dsis ;
        this.selectedDsi = selectedDsi;
        this.thisAdapter = new ThisAdapter<DSI>(getContext(), R.layout.simple_dropdown, dsis);
        setAdapter(this.thisAdapter);
        setOnClickListener(v -> {
            KeyboardUtils.hideKeyboard(getContext());
        });
        setOnItemClickListener((parent, view, position, id) -> {
            setSelectedDsi(thisAdapter.getDsis().get(position));
            if(onItemClickListener!=null){
                onItemClickListener.onItemClick(parent, view, position, id);
            }
        });
        if(this.selectedDsi!=null){
            setText(selectedDsi.spinnerLabel());
        }
        this.getThisAdapter().notifyDataSetChanged();

    }

//    public DSI getSelectedDsi() {
//        if(!getText().toString().equals("")){
//            if(dsis.size()>0){
//                this.selectedDsi = null ;
//                for (DSI dsi : dsis) {
//                    if(getText().toString().equals(dsi.spinnerLabel())){
//                        this.selectedDsi = dsi ;
//                        break;
//                    }
//                }
//            }
//        }else{
//            this.selectedDsi = null ;
//        }
//        return selectedDsi;
//    }

    public void setSelectedDsi(DSI selectedDsi) {
        this.selectedDsi = selectedDsi;
        if(this.selectedDsi!=null){
            setText(selectedDsi.spinnerLabel());
        }else{
            setText("");
        }
    }
    public void setEnableSpinner(boolean enableSpinner) {
        this.enableSpinner = enableSpinner;
        if(thisAdapter!=null){
            thisAdapter.notifyDataSetChanged();
        }
    }

    public void clear(){
        if(getThisAdapter()!=null){
            getThisAdapter().setDsis(null);
            getThisAdapter().notifyDataSetChanged();
            setSelectedDsi(null);
        }
    }

    public void reinit(List<DSI> datas, DSI defaulz){
        getThisAdapter().setDsis(datas);
        getThisAdapter().notifyDataSetChanged();
        if(defaulz!=null){
            this.selectedDsi = defaulz;
            setText(selectedDsi.spinnerLabel());
        }else{
            this.selectedDsi = null ;
            setText("");
        }
    }


    public static class ThisAdapter<DSI extends DataSpinnerInterface> extends ArrayAdapter<String>{

        @Getter
        @Setter
        List<DSI> dsis = new ArrayList<DSI>();

        public ThisAdapter(@NonNull Context context, @LayoutRes int resource, List<DSI> dsis) {
            super(context, resource);
            this.dsis = dsis;
        }

        @Override
        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if (convertView != null && convertView instanceof TextView) {
                ((TextView) convertView).setText(dsis.get(position).spinnerLabel());
            } else {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.simple_dropdown, parent, false);
                ((TextView) convertView).setText(dsis.get(position).spinnerLabel());
            }
            convertView.setTag(dsis.get(position));
            return convertView;
        }



        @Nullable
        @Override
        public String getItem(int position) {
            return dsis.get(position).spinnerLabel();
        }

        @Override
        public int getCount() {
            if(!enableSpinner){
                return 0;
            }
            return dsis!=null?dsis.size():0;
        }
    }
}
