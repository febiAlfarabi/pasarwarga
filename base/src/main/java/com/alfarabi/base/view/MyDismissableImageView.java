package com.alfarabi.base.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

import com.dmallcott.dismissibleimageview.FullScreenImageFragment;

/**
 * Created by Alfarabi on 7/25/17.
 */

public class MyDismissableImageView extends ImageView {

    private static final String FRAGMENT_TAG = "FS_TAG";

    public MyDismissableImageView(Context context) {
        super(context);
    }

    public MyDismissableImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public MyDismissableImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

//    public MyDismissibleImageView(Context context) {
//        this(context, (AttributeSet)null);
//    }
//
//    public MyDismissibleImageView(Context context, @Nullable AttributeSet attrs) {
//        this(context, attrs, 0);
//    }

//    public MyDismissibleImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
//        super(context, attrs, defStyleAttr);
//        this.setOnClickListener(this);
//        this.setDrawingCacheEnabled(true);
//        this.setAdjustViewBounds(true);
//    }

//    public void onClick(View v) {
//        if(this.getContext() instanceof AppCompatActivity) {
//            FullScreenImageFragment fragment = FullScreenImageFragment.newInstance(this.getDrawingCache());
//            fragment.show(((AppCompatActivity)this.getContext()).getSupportFragmentManager(), "FS_TAG");
//        }
//    }
}
