package com.alfarabi.base.view;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.alfarabi.base.R;
import com.alfarabi.alfalibs.tools.KeyboardUtils;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by User on 03/06/2017.
 */

public class StringMaterialSpinner extends MaterialBetterSpinner {

    @Getter String selectedDsi;

    @Getter
    @Setter
    ThisAdapter thisAdapter ;
    @Getter
    boolean enableSpinner = true ;

    public StringMaterialSpinner(Context context) {
        super(context);
        setFocusable(false);
    }

    public StringMaterialSpinner(Context arg0, AttributeSet arg1) {
        super(arg0, arg1);
        setFocusable(false);
    }

    public StringMaterialSpinner(Context arg0, AttributeSet arg1, int arg2) {
        super(arg0, arg1, arg2);
        setFocusable(false);
    }

    public void initDataAdapter(String[] dsis, @Nullable AdapterView.OnItemClickListener onItemClickListener, String selectedDsi){
        List<String> list = new ArrayList<>();
        for (String dsi : dsis) {
            list.add(dsi);
        }
        initDataAdapter(list, onItemClickListener, selectedDsi);
    }

    public void initDataAdapter(List<String> dsis, @Nullable AdapterView.OnItemClickListener onItemClickListener, String selectedDsi){
        this.selectedDsi = selectedDsi;
        this.thisAdapter = new ThisAdapter(getContext(), R.layout.simple_dropdown, dsis);
        setAdapter(this.thisAdapter);
        setOnClickListener(v -> {
            KeyboardUtils.hideKeyboard(getContext());
        });
        setOnItemClickListener((parent, view, position, id) -> {
            setSelectedDsi(thisAdapter.getItem(position));
            if(onItemClickListener!=null){
                onItemClickListener.onItemClick(parent, view, position, id);
            }
        });
        if(this.selectedDsi!=null){
            setText(selectedDsi);
        }
        this.getThisAdapter().notifyDataSetChanged();
    }

    public void setSelectedDsi(String selectedDsi) {
        this.selectedDsi = selectedDsi;
        if(this.selectedDsi!=null){
            setText(selectedDsi);
        }else{
            setText("");
        }
    }

    public void reinit(List<String> datas, String defaulz){
        getThisAdapter().setDsis(datas);
        getThisAdapter().notifyDataSetChanged();
        if(defaulz!=null){
            this.selectedDsi = defaulz;
            setText(this.selectedDsi);
        }else{
            this.selectedDsi = null ;
            setText("");
        }
    }

    public void clear(){
        if(getThisAdapter()!=null){
            getThisAdapter().setDsis(null);
            getThisAdapter().notifyDataSetChanged();
            setSelectedDsi(null);
        }
    }
    public void reinit(List<String> strings){
        getThisAdapter().setDsis(strings);
        getThisAdapter().notifyDataSetChanged();
    }

    public void setEnableSpinner(boolean enableSpinner) {
        this.enableSpinner = enableSpinner;
        if(thisAdapter!=null){
            thisAdapter.notifyDataSetChanged();
        }
    }

    public class ThisAdapter extends ArrayAdapter<String>{

        @Getter@Setter private List<String> dsis = new ArrayList<>();

        public ThisAdapter(@NonNull Context context, @LayoutRes int resource, List<String> dsis) {
            super(context, resource, dsis);
            this.dsis = dsis;
        }

        @Override
        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if(convertView!=null && convertView instanceof TextView){
                ((TextView) convertView).setText(dsis.get(position));
            }else{
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.simple_dropdown, parent, false);
                ((TextView) convertView).setText(dsis.get(position));
            }
            convertView.setTag(dsis.get(position));
            return convertView;
        }

        @Nullable
        @Override
        public String getItem(int position) {
            return dsis.get(position);
        }

        @Override
        public int getCount() {
            if(!enableSpinner){
                return 0;
            }
            return dsis!=null?dsis.size():0;
        }
    }

}
