package com.alfarabi.base.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

/**
 * Modified version of TabLayout from support design library with null check when putting an icon and color state change.
 *
 * @author Hendra Anggrian (hendra@ahloo.com)
 * @see android.support.design.widget.TabLayout
 */
public final class TabLayout extends android.support.design.widget.TabLayout {

    public TabLayout(Context context) {
        super(context);
    }

    public TabLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TabLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setWeightAt(int position, int weight) {
        ViewGroup slidingTabStrip = (ViewGroup) getChildAt(0);
        View tab = slidingTabStrip.getChildAt(position);
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) tab.getLayoutParams();
        layoutParams.weight = weight;
        tab.setLayoutParams(layoutParams);
    }

    public void setIconAt(int position, @DrawableRes int drawable) {
        Tab tab = getTabAt(position);
        if (tab != null)
            tab.setIcon(drawable);
    }

    public void setTitleAt(int position, String title) {
        Tab tab = getTabAt(position);
        if (tab != null)
            tab.setText(title);
    }

    public void applyColorState(Context context, @ColorRes int color) {
        for (int i = 0; i < getTabCount(); i++) {
            Tab tab = getTabAt(i);
            if (tab != null && tab.getIcon() != null) {
                Drawable icon = DrawableCompat.wrap(tab.getIcon());
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M)
                    DrawableCompat.setTintList(icon, context.getResources().getColorStateList(color, context.getTheme()));
                else
                    DrawableCompat.setTintList(icon, ContextCompat.getColorStateList(context, color));
            }
        }
    }
}