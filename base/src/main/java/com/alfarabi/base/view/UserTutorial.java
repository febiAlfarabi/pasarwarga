package com.alfarabi.base.view;

import android.app.Activity;
import android.view.View;

import com.alfarabi.base.R;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 10/16/17.
 */

public class UserTutorial {

    @Getter@Setter View view;
    @Getter@Setter String title ;
    @Getter@Setter String subtitle ;
    @Getter@Setter int outerCircleColor ;

    public static UserTutorial build(Activity activity, int id, String title, String subtitle, int outerCircleColor){
        UserTutorial userTutorial = new UserTutorial();
        userTutorial.setView(activity.findViewById(id));
        userTutorial.setTitle(title);
        userTutorial.setSubtitle(subtitle);
        userTutorial.setOuterCircleColor(outerCircleColor);
        return userTutorial ;
    }

    public static UserTutorial build(View view, String title, String subtitle, int outerCircleColor){
        UserTutorial userTutorial = new UserTutorial();
        userTutorial.setView(view);
        userTutorial.setTitle(title);
        userTutorial.setSubtitle(subtitle);
        userTutorial.setOuterCircleColor(outerCircleColor);
        return userTutorial ;
    }


    public static void show(Activity activity, TapTargetSequence.Listener listener, UserTutorial... userTutorials){
        if(userTutorials!=null && userTutorials.length>0){
//            TapTarget[] tapTargets = new TapTarget[userTutorials.length];
            List<TapTarget> tapTargets = new ArrayList<>();
            for (UserTutorial userTutorial :userTutorials) {
                tapTargets.add(TapTarget.forView(userTutorial.getView(), userTutorial.getTitle(), userTutorial.getSubtitle()).dimColor(R.color.black_tran_50));
            }
            new TapTargetSequence(activity).targets(tapTargets).listener(listener);
        }
    }


}
