package com.alfarabi.base.view.interfaze;

/**
 * Created by User on 03/06/2017.
 */

public interface DataSpinnerInterface<RO> {

    String spinnerLabel();

//    RO getItem();

    boolean equal(RO ro);

}
