//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.alfarabi.base.view.interfaze;

import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.View.DragShadowBuilder;
import android.widget.ImageView;

public class DismissibleDragShadowBuilder extends DragShadowBuilder {
    private final Drawable shadow;
    private final Point offset;

    public DismissibleDragShadowBuilder(ImageView imageView, Point offset) {
        super(imageView);
        this.offset = offset;
        this.shadow = new ColorDrawable(-3355444);
    }

    public void onProvideShadowMetrics(Point size, Point touch) {
        int width = this.getView().getWidth();
        int height = this.getView().getHeight();
        this.shadow.setBounds(0, 0, width, height);
        size.set(width, height);
        touch.set(this.offset.x, this.offset.y);
    }

    public void onDrawShadow(Canvas canvas) {
        this.shadow.draw(canvas);
        this.getView().draw(canvas);
    }
}
