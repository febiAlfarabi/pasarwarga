//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.alfarabi.base.view.interfaze;

import android.support.annotation.NonNull;
import android.view.DragEvent;
import android.view.View;
import android.view.View.OnDragListener;

public class DismissibleOnDragListener implements OnDragListener {
    @NonNull
    private final DismissibleOnDragListener.OnDropListener onDropListener;

    public DismissibleOnDragListener(@NonNull DismissibleOnDragListener.OnDropListener onDropListener) {
        this.onDropListener = onDropListener;
    }

    public boolean onDrag(View v, DragEvent event) {
        switch(event.getAction()) {
        case 1:
            this.onDropListener.onDragStarted();
            return true;
        case 2:
            this.onDropListener.onDragLocation(event.getX(), event.getY());
            return true;
        case 3:
            this.onDropListener.onDrop();
            return true;
        case 4:
            this.onDropListener.onDragEnded();
            return true;
        case 5:
            return true;
        case 6:
            return true;
        default:
            return false;
        }
    }

    public abstract static class OnDropListener {
        public OnDropListener() {
        }

        public abstract void onDragStarted();

        public abstract void onDrop();

        public abstract void onDragEnded();

        public abstract void onDragLocation(float var1, float var2);
    }
}
