# c libraries to hold all constant values
# http://www3.ntu.edu.sg/home/ehchua/programming/android/android_ndk.html
# https://developer.android.com/ndk/samples/sample_hellojni.html#an

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_SRC_FILES := batman.c
LOCAL_MODULE    := batman
include $(BUILD_SHARED_LIBRARY)

#include $(CLEAR_VARS)
#LOCAL_SRC_FILES := http.c
#LOCAL_MODULE    := http
#include $(BUILD_SHARED_LIBRARY)