#include <jni.h>

#define BATMAN "https://pasarwarga.com/"
#define GATOTKACA "https://dev.pasarwarga.com/"
#define BIMASAKTI "http://192.168.21.86/"
#define ROBIN "it@pasarwarga.com"

JNIEXPORT jobjectArray JNICALL Java_com_alfarabi_base_BaseApplication_batman(JNIEnv *env, jobject jobj) {
    return (*env)->NewStringUTF(env, BATMAN);
}
JNIEXPORT jobjectArray JNICALL Java_com_alfarabi_base_BaseApplication_gatotkaca(JNIEnv *env, jobject jobj) {
    return (*env)->NewStringUTF(env, GATOTKACA);
}
JNIEXPORT jobjectArray JNICALL Java_com_alfarabi_base_BaseApplication_bimasakti(JNIEnv *env, jobject jobj) {
    return (*env)->NewStringUTF(env, BIMASAKTI);
}
JNIEXPORT jobjectArray JNICALL Java_com_alfarabi_base_BaseApplication_robin(JNIEnv *env, jobject jobj) {
    return (*env)->NewStringUTF(env, ROBIN);
}
