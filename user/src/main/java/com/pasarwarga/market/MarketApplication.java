package com.pasarwarga.market;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatDelegate;

import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.DeviceTools;
import com.alfarabi.base.BaseApplication;
import com.alfarabi.base.tools.Initial;
import com.alfarabi.base.tools.LanguageTools;
import com.appeaser.sublimenavigationviewlibrary.SublimeMenu;
import com.google.gson.GsonBuilder;

/**
 * Created by Alfarabi on 6/15/17.
 */

public class MarketApplication extends BaseApplication {



    @Override
    public void onCreate() {
        super.onCreate();
        Thread.setDefaultUncaughtExceptionHandler((thread, e) -> {
            e.printStackTrace();
        });
        /** open 3rd party lib */
//        Realm.init(this);

        /** set default realm instance to include model from base module */
//        Realm.setDefaultConfiguration(new RealmConfiguration.Builder()
//                .name(getString(R.string.realm_config_name)).deleteRealmIfMigrationNeeded()
//                .modules(Realm.getDefaultModule(), getLibraryRealmModule())
//                .build());

        /** enable vector drawable support */

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
//        if(language==null) {
//            language = getString(com.alfarabi.base.R.string.language_in);
//            saver.saveAll();
//        }
        BaseApplication.baseDomain = batman();
        HttpInstance.init(BaseApplication.baseDomain, 30, 90, 60);
        HttpInstance.setGson((new GsonBuilder()).setLenient().setDateFormat("yyyy-MM-dd hh:mm:ss").create());
        HttpInstance.putHeaderMap(Initial.DEVICE_TYPE, Initial.ANDROID);
        HttpInstance.putHeaderMap(Initial.X_CLIENT_CODE, DeviceTools.id(this));
        HttpInstance.putHeaderMap(Initial.CONTENT_TYPE, "application/json");
    }

    @Override
    protected boolean getBuildConfigDebug() {
        return false;
    }

    @Override
    protected String getBuildConfigVersionName() {
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            return version;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return "0";
    }
}
