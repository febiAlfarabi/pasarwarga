package com.pasarwarga.market.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.LinearLayout;

import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.alfalibs.views.AlfaBannerView;
import com.alfarabi.base.activity.BasePublicActivity;
import com.alfarabi.base.adapter.DotRecyclerAdapter;
import com.alfarabi.base.adapter.IntroductionScollViewAdapter;
import com.alfarabi.alfalibs.tools.KeyboardUtils;
import com.hendraanggrian.preferencer.Preferencer;
import com.hendraanggrian.preferencer.Saver;
import com.hendraanggrian.preferencer.annotations.BindPreference;
import com.pasarwarga.market.R;
import com.yarolegovich.discretescrollview.DiscreteScrollView;

import butterknife.BindView;

public class IntroductionActivity extends BasePublicActivity {

    public static final String TAG = IntroductionActivity.class.getName();
    
    @BindPreference("com.pasarwarga.market.activity.IntroductionActivity") boolean secondTimeOpen ;

    @BindView(R.id.coordinator_layout) CoordinatorLayout coordinatorLayout ;
    @BindView(R.id.footer_layout) LinearLayout foterLayout ;
    @BindView(R.id.alfabannerview) AlfaBannerView alfaBannerView ;
    @BindView(R.id.skip_button) Button skipButton ;
    private int[] drawables ;


    @BindView(R.id.dot_recyclerview) RecyclerView dotRecyclerView ;
    DotRecyclerAdapter dotRecyclerAdapter ;

    @Override
    public CoordinatorLayout getCoordinatorLayout() {
        return coordinatorLayout;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.activity_introduction;
    }

    @Override
    protected void onStart() {
        super.onStart();
        KeyboardUtils.hideKeyboard(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Saver saver = Preferencer.bind(this);
        if(!secondTimeOpen){
            drawables = new int[]{R.drawable.launch_screen_1, R.drawable.launch_screen_2, R.drawable.launch_screen_3, R.drawable.launch_screen_4};
            alfaBannerView.setAdapter(new IntroductionScollViewAdapter(this, drawables));
            alfaBannerView.getAdapter().notifyDataSetChanged();
            dotRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            dotRecyclerAdapter = new DotRecyclerAdapter(this, drawables.length, 0);
            dotRecyclerView.setAdapter(dotRecyclerAdapter);
            dotRecyclerAdapter.notifyDataSetChanged();

            alfaBannerView.addScrollStateChangeListener(new DiscreteScrollView.ScrollStateChangeListener<RecyclerView.ViewHolder>() {
                @Override
                public void onScrollStart(@NonNull RecyclerView.ViewHolder currentItemHolder, int adapterPosition) {

                }

                @Override
                public void onScrollEnd(@NonNull RecyclerView.ViewHolder currentItemHolder, int adapterPosition) {
                    dotRecyclerAdapter.setFocus(alfaBannerView.getCurrentItem());
                }

                @Override
                public void onScroll(float scrollPosition, @NonNull RecyclerView.ViewHolder currentHolder, @NonNull RecyclerView.ViewHolder newCurrent) {

                }
            });
            skipButton.setOnClickListener(v -> {
                if(alfaBannerView.getCurrentItem()<drawables.length-1){
                    alfaBannerView.smoothScrollToPosition(alfaBannerView.getCurrentItem()+1);
                }else{
                    secondTimeOpen = true ;
                    saver.saveAll();
                    WindowFlow.startActivity(this, SplashActivity.class, true);
                }
            });
//            firsTimeOpen = true ;
//            saver.saveAll();
        }else{
            WindowFlow.startActivity(this, SplashActivity.class, true);
        }


    }
}
