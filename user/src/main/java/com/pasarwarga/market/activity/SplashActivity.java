package com.pasarwarga.market.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.GoogleApiValidator;
import com.alfarabi.alfalibs.tools.WLog;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.http.basemarket.service.LandingService;
import com.alfarabi.base.model.Landing;
import com.alfarabi.base.realm.RealmDao;
import com.alfarabi.base.tools.GooglePlayUtil;
import com.alfarabi.base.tools.Initial;
import com.alfarabi.base.tools.ResponseTools;
import com.kishan.askpermission.AskPermission;
import com.kishan.askpermission.ErrorCallback;
import com.kishan.askpermission.PermissionCallback;
import com.kishan.askpermission.PermissionInterface;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.alfarabi.base.activity.publix.BaseSplashActivity;
import com.pasarwarga.market.R;
import com.alfarabi.base.activity.UnderMaintenanceActivity;
import com.pasarwarga.market.tools.SystemApp;
import com.robohorse.gpversionchecker.GPVersionChecker;
import com.robohorse.gpversionchecker.domain.Version;
import com.robohorse.gpversionchecker.domain.VersionInfoListener;

import butterknife.BindView;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import lombok.Getter;


public class SplashActivity extends BaseSplashActivity {

    private static final String[] PERMISSIONS = {
            Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.INTERNET, Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.RECEIVE_SMS,
            Manifest.permission.READ_SMS
    };

    GPVersionChecker.Builder versionCheckerBuilder ;


    @BindView(R.id.coordinator_layout) CoordinatorLayout coordinatorLayout;
    @BindView(R.id.version_tv) TextView versionTextView;

    @Getter MaterialDialog materialDialog;
    TextView versionNameTextView;
    TextView versionInfoTextView;
    Button laterButton;
    Button updateButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        versionTextView.setText(getString(R.string.version) + " " + getVersionName());
    }

    @Override
    protected void onResume() {
        super.onResume();
        permissionAction();


    }

    public void checkingForUpdate(){

        versionCheckerBuilder = new GPVersionChecker.Builder(this)
                .setVersionInfoListener(new VersionInfoListener() {

                    @Override
                    public void onResulted(Version version) {
                        if(version.isNeedToUpdate()){
                            materialDialog = new MaterialDialog.Builder(SplashActivity.this).title(getString(R.string.application_update)).customView(R.layout.update_dialog, false).build();
                            View dialogView = materialDialog.getView();
                            versionNameTextView = (TextView) dialogView.findViewById(R.id.version_name_tv);
                            versionInfoTextView = (TextView) dialogView.findViewById(R.id.version_info_tv);
                            laterButton = (Button) dialogView.findViewById(R.id.later_button);
                            updateButton = (Button) dialogView.findViewById(R.id.update_button);
                            materialDialog.setCanceledOnTouchOutside(false);
                            materialDialog.setCancelable(false);
                            versionNameTextView.setText(getString(R.string.a_new_version_is)+" "+version.getNewVersionCode());
                            String versionChanges = version.getChanges();
                            versionInfoTextView.setText(versionChanges.replaceAll(" - ", "\n- "));
                            materialDialog.show();
                            updateButton.setOnClickListener(v -> {
                                materialDialog.dismiss();
                                GooglePlayUtil.openUpdateLink(SplashActivity.this);
                            });
                            laterButton.setOnClickListener(v -> {
                                materialDialog.dismiss();
                                checkingGoogleService();
                            });
                        }else{
                            checkingGoogleService();
                        }
                    }

                    @Override
                    public void onErrorHandled(Throwable throwable) {
                        checkingGoogleService();
                        showSnackbar(throwable, v -> {});
                    }
                }).setLoggingEnable(true)
                .create();
    }

    void permissionAction() {

        new AskPermission.Builder(this)
                .setPermissions(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.INTERNET, Manifest.permission.ACCESS_NETWORK_STATE,
                        Manifest.permission.RECEIVE_SMS,
                        Manifest.permission.READ_SMS)
                .setCallback(new PermissionCallback() {
                    @Override
                    public void onPermissionsGranted(int requestCode) {
                        checkingForUpdate();

                    }
                    @Override
                    public void onPermissionsDenied(int requestCode) {
                        showDialog(getString(R.string.permission_deniedd), getString(R.string.permission_required_or_close), (dialog, which) -> {
                            dialog.dismiss();
                            finish();
                        });
                    }
                })
                .setErrorCallback(new ErrorCallback() {
                    @Override
                    public void onShowRationalDialog(PermissionInterface permissionInterface, int requestCode) {
                        permissionInterface.onDialogShown();
                    }

                    @Override
                    public void onShowSettings(PermissionInterface permissionInterface, int requestCode) {
                        permissionInterface.onSettingsShown();
                    }
                }).request(123456);
    }

    public void checkingGoogleService() {
        GoogleApiValidator.getInstance(this).onAvailable(() -> {
            HttpInstance.call(HttpInstance.create(LandingService.class).dataLanding(), landingResponses -> {
                ResponseTools.validate(this, landingResponses);
                if (landingResponses.isStatus()) {

                    RealmDao.getRealm().executeTransactionAsync(realm -> {
                        realm.delete(Landing.class);
                        realm.insertOrUpdate(landingResponses.getLanding());

                    }, () -> {
                        Initial.loadCities(this);
                        nextActivity();
                    });
                } else {
                    if (RealmDao.instance().rowExist(Landing.class)) {
                        Initial.loadCities(this);
                        nextActivity();
                    } else {
                        showDialog(landingResponses.getMessage(), (dialog1, which) -> finish());
                    }
                }
            }, throwable -> {
                if (RealmDao.instance().rowExist(Landing.class)) {
                    showSnackbar(throwable, v -> {
                        if (RealmDao.instance().rowExist(Landing.class)) {
                            Initial.loadCities(this);
                            WindowFlow.startActivity(this, HomeActivity.class, true);
                        }
                    });
                    coordinatorLayout.postDelayed(() -> {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                            if (!isDestroyed()) {
                                nextActivity();
                            }
                        }
                    }, 3000);
                } else {
                    showDialog(throwable, (dialog1, which) -> finish());
                }

            });
        }).onUnavailable(new GoogleApiValidator.OnUnavailable() {
            @Override
            public void onUnavailableResolvable(@NonNull Dialog dialog) {
                showDialog(getString(R.string.google_unavailable), (dialog1, which) -> finish());
            }

            @Override
            public void onUnavailableUnresolvable() {
                showDialog(getString(R.string.google_unavailable), (dialog1, which) -> finish());
            }
        }).validate();
    }


    public void nextActivity(){
        if (SystemApp.checkLogin(this)) {
            if(getIntent().getExtras()!=null && getIntent().getExtras().containsKey(Initial.FLAG_CHAT_FRAGMENT)) {
                WLog.d(TAG, "On Click Chat");
            }
        }
        Observable.fromCallable(() -> {
            WindowFlow.startActivity(this, HomeActivity.class, true);
            return true;
        }).subscribeOn(Schedulers.newThread()).subscribe();
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.activity_splash;
    }

    @Override
    public CoordinatorLayout getCoordinatorLayout() {
        return coordinatorLayout;
    }

    public String getVersionName() {
        try {
            return getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode==KeyEvent.KEYCODE_BACK){
            if(versionCheckerBuilder!=null) {
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }
}
