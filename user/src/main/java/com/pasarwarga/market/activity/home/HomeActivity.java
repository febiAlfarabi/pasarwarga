package com.pasarwarga.market.activity.home;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.Gilimanuk;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.alfalibs.tools.WLog;
import com.alfarabi.alfalibs.tools.Gilimanuk;
import com.alfarabi.base.http.basemarket.service.LoginService;
import com.alfarabi.base.http.basemarket.service.OrderService;
import com.alfarabi.base.http.basemarket.service.ReviewService;
import com.alfarabi.base.http.basemarket.service.PurchaseService;
import com.alfarabi.base.model.chat.Conversation;
import com.alfarabi.base.model.transaction.SummaryStatus;
import com.alfarabi.base.realm.RealmDao;
import com.alfarabi.base.tools.Initial;
import com.alfarabi.base.tools.ResponseTools;
import com.appeaser.sublimenavigationviewlibrary.SublimeMenu;
import com.appeaser.sublimenavigationviewlibrary.SublimeTextWithBadgeMenuItem;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.github.clans.fab.FloatingActionButton;
import com.pasarwarga.market.MarketApplication;
import com.pasarwarga.market.activity.SplashActivity;
import com.pasarwarga.market.activity.publix.LoginActivity;
import com.pasarwarga.market.activity.publix.RegistrationActivity;
import com.pasarwarga.market.fragment.AboutFragment;
import com.pasarwarga.market.fragment.SettingsFragment;
import com.pasarwarga.market.fragment.asseller.SellerDashboardFragment;
import com.pasarwarga.market.fragment.asseller.transactiontab.SellerTransactionTabFragment;
import com.pasarwarga.market.fragment.chat.ConversationUserFragment;
import com.pasarwarga.market.fragment.profile.FragmentUserProfile;
import com.pasarwarga.market.fragment.registration.StoreRegistrationApprovalFragment;
import com.pasarwarga.market.fragment.registration.StoreRegistrationLandingFragment;
import com.pasarwarga.market.fragment.review.ReviewTabFragment;
import com.pasarwarga.market.fragment.store.CategoryListFragment;
import com.pasarwarga.market.fragment.store.StoreFavoriteFragment;
import com.pasarwarga.market.fragment.store.product.ProductWishlistFragment;
import com.pasarwarga.market.fragment.store.product.ProductListFragment;
import com.pasarwarga.market.fragment.store.product.ProductPromoListFragment;
import com.appeaser.sublimenavigationviewlibrary.SublimeBaseMenuItem;
import com.appeaser.sublimenavigationviewlibrary.SublimeNavigationView;
import com.alfarabi.base.activity.home.BaseDrawerActivity;
import com.pasarwarga.market.R;
import com.pasarwarga.market.fragment.home.HomeFragment;
import com.pasarwarga.market.fragment.store.transaction.purchase.PurchaseParentTabFragment;
import com.pasarwarga.market.tools.SystemApp;

import org.apache.commons.lang.StringUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import lombok.Getter;
import lombok.Setter;

public class HomeActivity extends BaseDrawerActivity {

    @Setter @BindView(R.id.navigation_view) SublimeNavigationView sublimeNavigationView;
    @Setter @BindView(R.id.drawer_layout) DrawerLayout drawerLayout;
    @Setter @BindView(R.id.coordinator_layout) CoordinatorLayout coordinatorLayout;
    @Getter @Setter @BindView(R.id.fab_button) FloatingActionButton fabButton;
    @BindView(R.id.fragment_frame) FrameLayout fragmentFrameLayout;

    @Getter @Setter private int favoriteCounter = 0;
    @Getter @Setter SummaryStatus summaryStatus;
    @Getter @Setter SummaryStatus purchaseSummaryStatus;

    @Override
    public DrawerLayout getDrawerLayout() {
        return drawerLayout;
    }

    @Override
    public SublimeNavigationView getSublimeNavigationView() {
        return sublimeNavigationView;
    }

    @Override
    public CoordinatorLayout getCoordinatorLayout() {
        return coordinatorLayout;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (SystemApp.checkLogin(this)) {
            if(getIntent()!=null && getIntent().getAction()!=null && getIntent().getAction().equals(Initial.FLAG_CHAT_FRAGMENT)){
                Gilimanuk.goFirst(this, HomeFragment.TAG, R.id.fragment_frame);
                Gilimanuk.withAnim(R.anim.enter_from_right, R.anim.exit_to_left).goTo(this, ConversationUserFragment.TAG, R.id.fragment_frame, true, false);
            }else{
                Gilimanuk.goFirst(this, HomeFragment.TAG, R.id.fragment_frame);
            }
        }else{
            Gilimanuk.goFirst(this, HomeFragment.TAG, R.id.fragment_frame);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        initMenu();
        if(SystemApp.checkLogin(this) && SystemApp.myProfile().getUser().isSeller()) {
            summaryOrderRequest();
            summaryReviewRequest();
        }

        if (SystemApp.checkLogin(this)) {
            purchaseSummaryRequest();
            RealmDao.getRealm().addChangeListener(realm -> {
                renderUnreadChatCount();
            });
            renderUnreadChatCount();
        }
    }

    public void initMenu() {
        if (myProfile != null) {
            if (myProfile.getUser().isSeller() && myProfile.getUser().getSellerStatus() != null && myProfile.getUser().getSellerStatus().equalsIgnoreCase(Initial.ACTIVE)) {
                sublimeNavigationView.switchMenuTo(R.menu.left_navigation_seller_menu);
            } else {
                sublimeNavigationView.switchMenuTo(R.menu.left_navigation_user_menu);
            }
        } else {
            sublimeNavigationView.switchMenuTo(R.menu.left_navigation_public_menu);
        }

        ImageView pwHeaderImageView = ButterKnife.findById(sublimeNavigationView.getHeaderView(), R.id.pasarwarga_header_iv);
        RelativeLayout profileHeaderLayout = ButterKnife.findById(sublimeNavigationView.getHeaderView(), R.id.profile_header_layout);//(RelativeLayout) findViewById(R.id.profile_header_layout);
        CircleImageView profileImageView = ButterKnife.findById(sublimeNavigationView.getHeaderView(), R.id.profile_user_iv);//(RelativeLayout) findViewById(R.id.profile_header_layout);
        TextView usernameTextView = ButterKnife.findById(sublimeNavigationView.getHeaderView(), R.id.username_tv);//(RelativeLayout) findViewById(R.id.profile_header_layout);
        TextView accountStatusTextView = ButterKnife.findById(sublimeNavigationView.getHeaderView(), R.id.user_status_tv);

        if (myProfile != null) {
            pwHeaderImageView.setVisibility(ImageView.GONE);




            profileHeaderLayout.setVisibility(RelativeLayout.VISIBLE);
            GlideApp.with(this).load(StringUtils.split(myProfile.getUser().getPhoto(), ",")[0])
                    .placeholder(R.drawable.ic_pasarwarga)
                    .error(R.drawable.ic_pasarwarga)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            e.printStackTrace();
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }
                    }).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL).into(profileImageView);
            usernameTextView.setText(myProfile.getUser().getFirstName() + " " + myProfile.getUser().getLastName());

            if (myProfile.getUser().isPhoneVerified()) {
                accountStatusTextView.setText(R.string.verified_account);
            }else {
                accountStatusTextView.setText(R.string.not_verified_account);
            }
            profileHeaderLayout.setOnClickListener(v -> {
                drawerLayout.closeDrawers();
                Gilimanuk.goTo(this, FragmentUserProfile.TAG, R.id.fragment_frame, true, false);
            });

        } else {
            pwHeaderImageView.setVisibility(ImageView.VISIBLE);
            profileHeaderLayout.setVisibility(RelativeLayout.GONE);
        }
        sublimeNavigationView.setNavigationMenuEventListener(this);

    }


    public void summaryOrderRequest(){

        HttpInstance.call(HttpInstance.create(OrderService.class).getSummaryStatus(), summaryStatusResponse -> {
            ResponseTools.validate(this, summaryStatusResponse);
            if (summaryStatusResponse.isStatus()) {
                summaryStatus = summaryStatusResponse.getSummaryStatus();
                summaryStatus.setId(1);
                summaryStatus = RealmDao.instance().renew(summaryStatus.getId(), summaryStatus);
                renderSummary(summaryStatus);
            }
        }, throwable -> showSnackbar(throwable, v -> {}), false);
    }
    public void summaryReviewRequest(){
        HttpInstance.call(HttpInstance.create(ReviewService.class).feedbackableCount(), feedbackableResponse -> {
            ResponseTools.validate(this, feedbackableResponse);
            if(feedbackableResponse.isStatus()) {
                if(feedbackableResponse.getHashMap().get("summary").getFeedbackable()==0){
                    ((SublimeTextWithBadgeMenuItem) sublimeNavigationView.getMenu().getMenuItem(R.id.sub_notification_menu_item_2))
                            .setBadgeText(" ").setValueProvidedAsync(false);
                }else{
                    ((SublimeTextWithBadgeMenuItem) sublimeNavigationView.getMenu().getMenuItem(R.id.sub_notification_menu_item_2))
                            .setBadgeText(String.valueOf(feedbackableResponse.getHashMap().get("summary").getFeedbackable())).setValueProvidedAsync(false);
                }
            }else{
                ((SublimeTextWithBadgeMenuItem) sublimeNavigationView.getMenu().getMenuItem(R.id.sub_notification_menu_item_2))
                        .setBadgeText(" ").setValueProvidedAsync(false);
            }
        }, throwable -> {
            ((SublimeTextWithBadgeMenuItem) sublimeNavigationView.getMenu().getMenuItem(R.id.sub_notification_menu_item_2))
                    .setBadgeText(" ").setValueProvidedAsync(false);
            showSnackbar(throwable, v -> {});
        }, false);
    }

    public void purchaseSummaryRequest() {
        HttpInstance.call(HttpInstance.create(PurchaseService.class).getPurchaseSummaryStatus(), purchaseStatusResponse -> {
            ResponseTools.validate(this, purchaseStatusResponse);
            if (purchaseStatusResponse.isStatus()) {
                purchaseSummaryStatus = purchaseStatusResponse.getSummaryStatus();
                purchaseSummaryStatus.setId(1);
                purchaseSummaryStatus = RealmDao.instance().renew(purchaseSummaryStatus.getId(), purchaseSummaryStatus);
                renderPurchaseSummary(purchaseSummaryStatus);
            }
        }, throwable -> showSnackbar(throwable, v -> {}), false);
    }

    public void renderSummary(SummaryStatus summaryStatus) {
        String pendingCount = String.valueOf(summaryStatus.getPending().getCount()==0?" ":summaryStatus.getPending().getCount());
        ((SublimeTextWithBadgeMenuItem) sublimeNavigationView.getMenu().getMenuItem(R.id.sub_mystore_menu_item_2)).setBadgeText(pendingCount).setValueProvidedAsync(false);

        String processeedCount = String.valueOf(summaryStatus.getProcessed().getCount()==0?" ":summaryStatus.getProcessed().getCount());
        ((SublimeTextWithBadgeMenuItem) sublimeNavigationView.getMenu().getMenuItem(R.id.sub_mystore_menu_item_3)).setBadgeText(processeedCount).setValueProvidedAsync(false);

        String shippedCount = String.valueOf(summaryStatus.getShipped().getCount()==0?" ":summaryStatus.getShipped().getCount());
        ((SublimeTextWithBadgeMenuItem) sublimeNavigationView.getMenu().getMenuItem(R.id.sub_mystore_menu_item_4)).setBadgeText(shippedCount).setValueProvidedAsync(false);

        String deliveredCount = String.valueOf(summaryStatus.getDelivered().getCount()==0?" ":summaryStatus.getDelivered().getCount());
        ((SublimeTextWithBadgeMenuItem) sublimeNavigationView.getMenu().getMenuItem(R.id.sub_mystore_menu_item_5)).setBadgeText(deliveredCount).setValueProvidedAsync(false);

    }

    public void renderPurchaseSummary(SummaryStatus purchaseSummaryStatus) {

        String pendingCount = String.valueOf(purchaseSummaryStatus.getPending().getCount()==0?" ":purchaseSummaryStatus.getPending().getCount());
        ((SublimeTextWithBadgeMenuItem) sublimeNavigationView.getMenu().getMenuItem(R.id.sub_purchase_menu_item_1)).setBadgeText(pendingCount).setValueProvidedAsync(false);

        String processeedCount = String.valueOf(purchaseSummaryStatus.getProcessed().getCount()==0?" ":purchaseSummaryStatus.getProcessed().getCount());
        ((SublimeTextWithBadgeMenuItem) sublimeNavigationView.getMenu().getMenuItem(R.id.sub_purchase_menu_item_2)).setBadgeText(processeedCount).setValueProvidedAsync(false);

        String shippedCount = String.valueOf(purchaseSummaryStatus.getShipped().getCount()==0?" ":purchaseSummaryStatus.getShipped().getCount());
        ((SublimeTextWithBadgeMenuItem) sublimeNavigationView.getMenu().getMenuItem(R.id.sub_purchase_menu_item_3)).setBadgeText(shippedCount).setValueProvidedAsync(false);

        String deliveredCount = String.valueOf(purchaseSummaryStatus.getDelivered().getCount()==0?" ":purchaseSummaryStatus.getDelivered().getCount());
        ((SublimeTextWithBadgeMenuItem) sublimeNavigationView.getMenu().getMenuItem(R.id.sub_purchase_menu_item_4)).setBadgeText(deliveredCount).setValueProvidedAsync(false);
    }

    public void renderUnreadChatCount() {
        int unReadCount = RealmDao.getRealm()
                .where(Conversation.class).equalTo("hasBeenRead", false).findAll().size();
        if(unReadCount!=0) {
            ((SublimeTextWithBadgeMenuItem) sublimeNavigationView.getMenu().getMenuItem(R.id.sub_notification_menu_item_1))
                    .setBadgeText(String.valueOf(unReadCount)).setValueProvidedAsync(false);
        }else{
            ((SublimeTextWithBadgeMenuItem) sublimeNavigationView.getMenu().getMenuItem(R.id.sub_notification_menu_item_1))
                    .setBadgeText(" ").setValueProvidedAsync(false);
        }
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.activity_home;
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(Gravity.START)) {
            drawerLayout.closeDrawers();
            return;
        }
        super.onBackPressed();
    }


    @Override
    public boolean onNavigationMenuEvent(Event event, SublimeBaseMenuItem menuItem) {
        super.onNavigationMenuEvent(event, menuItem);
        if (menuItem.getItemId() == R.id.home_menu_item) {
            drawerLayout.closeDrawer(Gravity.LEFT, true);

            drawerLayout.postDelayed(() -> {
                Gilimanuk.withAnim(slideAnims).goFirst(HomeActivity.this, HomeFragment.TAG, R.id.fragment_frame);
            }, 250);
        }
        if (menuItem.getItemId() == R.id.login_menu_item) {
            drawerLayout.closeDrawer(Gravity.LEFT, true);

            drawerLayout.postDelayed(() -> {
                Gilimanuk.startActivity(HomeActivity.this, LoginActivity.class, false);
            }, 250);
        }
        if (menuItem.getItemId() == R.id.register_menu_item) {
            drawerLayout.closeDrawer(Gravity.LEFT, true);

            drawerLayout.postDelayed(() -> {
                Gilimanuk.startActivity(HomeActivity.this, RegistrationActivity.class, false);
            }, 250);
        }
        if (menuItem.getItemId() == R.id.register_store_menu_item) {
            drawerLayout.closeDrawer(Gravity.LEFT, true);

            if (myProfile.getUser().isSeller()) {
                if (myProfile.getUser().getSellerStatus() != null && !myProfile.getUser().getSellerStatus().equalsIgnoreCase(Initial.ACTIVE)) {
                    drawerLayout.postDelayed(() -> {
                        Gilimanuk.withAnim(slideAnims).goTo(HomeActivity.this, StoreRegistrationApprovalFragment.TAG, R.id.fragment_frame, true, false);
                    }, 250);
                }
            } else {
                drawerLayout.postDelayed(() -> {
                    Gilimanuk.withAnim(slideAnims).goTo(HomeActivity.this, StoreRegistrationLandingFragment.TAG, R.id.fragment_frame, true, false);
                }, 250);
            }
        }
        if (menuItem.getItemId() == R.id.sub_mystore_menu_item_1) { // Store Profile
            drawerLayout.closeDrawer(Gravity.LEFT, true);

        }

        if (menuItem.getItemId() == R.id.sub_mystore_menu_item_2) { // Store Profile
            drawerLayout.closeDrawer(Gravity.LEFT, true);

            drawerLayout.postDelayed(() -> {
                Bundle bundle = new Bundle();
                bundle.putInt("tab_number", 0);
                Gilimanuk.force().goTo(HomeActivity.this, SellerTransactionTabFragment.TAG, bundle, R.id.fragment_frame, true, false);
            }, 250);

        }
        if (menuItem.getItemId() == R.id.sub_mystore_menu_item_3) { // Order List
            drawerLayout.closeDrawer(Gravity.LEFT, true);

            drawerLayout.postDelayed(() -> {
                Bundle bundle = new Bundle();
                bundle.putInt("tab_number", 1);
                Gilimanuk.force().goTo(HomeActivity.this, SellerTransactionTabFragment.TAG, bundle, R.id.fragment_frame, true, false);
            }, 250);
        }
        if (menuItem.getItemId() == R.id.sub_mystore_menu_item_4) { // Product List
            drawerLayout.closeDrawer(Gravity.LEFT, true);

            drawerLayout.postDelayed(() -> {
                Bundle bundle = new Bundle();
                bundle.putInt("tab_number", 2);
                Gilimanuk.force().goTo(HomeActivity.this, SellerTransactionTabFragment.TAG, bundle, R.id.fragment_frame, true, false);
            }, 250);
        }
        if (menuItem.getItemId() == R.id.sub_mystore_menu_item_5) { // Setting
            drawerLayout.closeDrawer(Gravity.LEFT, true);

            drawerLayout.postDelayed(() -> {
                Bundle bundle = new Bundle();
                bundle.putInt("tab_number", 3);
                Gilimanuk.force().goTo(HomeActivity.this, SellerTransactionTabFragment.TAG, bundle, R.id.fragment_frame, true, false);
            }, 250);
        }

        if (menuItem.getItemId() == R.id.sub_notification_menu_item_1) { // Setting
            drawerLayout.closeDrawer(Gravity.LEFT, true);

            drawerLayout.postDelayed(() -> {
                Gilimanuk.withAnim(slideAnims).goTo(HomeActivity.this, ConversationUserFragment.TAG, R.id.fragment_frame, true, false);
            }, 250);
        }
        if (menuItem.getItemId() == R.id.sub_notification_menu_item_2) { // Setting
            drawerLayout.closeDrawer(Gravity.LEFT, true);

            drawerLayout.postDelayed(() -> {
                Gilimanuk.withAnim(slideAnims).goTo(HomeActivity.this, ReviewTabFragment.TAG, R.id.fragment_frame, true, false);
            }, 250);
        }

        if (menuItem.getItemId() == R.id.promo_menu_item) {
            drawerLayout.closeDrawer(Gravity.LEFT, true);

            drawerLayout.postDelayed(() -> {
                Gilimanuk.withAnim(slideAnims).goTo(HomeActivity.this, ProductPromoListFragment.TAG, R.id.fragment_frame, true, false);
            }, 250);
        }

//        if (menuItem.getItemId() == R.id.product_menu_item) {
//            drawerLayout.closeDrawers();
//            Gilimanuk.withAnim(R.anim.enter_from_right, R.anim.exit_to_left).goTo(HomeActivity.this, ProductListFragment.TAG, R.id.fragment_frame, true, false);
//        }

        if (menuItem.getItemId() == R.id.category_menu_item) {
            drawerLayout.closeDrawer(Gravity.LEFT, true);

            drawerLayout.postDelayed(() -> {
                Gilimanuk.withAnim(slideAnims).goTo(HomeActivity.this, CategoryListFragment.TAG, R.id.fragment_frame, true, false);
            }, 250);
        }

        if (menuItem.getItemId() == R.id.wishlist_menu_item) { // Setting
            drawerLayout.closeDrawer(Gravity.LEFT, true);

            drawerLayout.postDelayed(() -> {
                Gilimanuk.withAnim(slideAnims).goTo(HomeActivity.this, ProductWishlistFragment.TAG, R.id.fragment_frame, true, false);
            }, 250);
        }

        if (menuItem.getItemId() == R.id.store_favorite_menu_item) { // Setting
            drawerLayout.closeDrawer(Gravity.LEFT, true);

            drawerLayout.postDelayed(() -> {
                Gilimanuk.withAnim(slideAnims).goTo(HomeActivity.this, StoreFavoriteFragment.TAG, R.id.fragment_frame, true, false);
            }, 250);
        }

//        if (menuItem.getItemId() == R.id.purchase_menu_item) { // Setting
//            drawerLayout.closeDrawers();
//            Gilimanuk.withAnim(R.anim.enter_from_right, R.anim.exit_to_left).goTo(HomeActivity.this, PurchaseListFragment.TAG, R.id.fragment_frame, true, false);
//        }

        if (menuItem.getItemId() == R.id.sub_purchase_menu_item_1) { //Payment
            drawerLayout.closeDrawer(Gravity.LEFT, true);
            drawerLayout.postDelayed(() -> {
                Bundle bundle = new Bundle();
                bundle.putInt("tab_number", 0);
                Gilimanuk.force().goTo(HomeActivity.this, PurchaseParentTabFragment.TAG, bundle, R.id.fragment_frame, true, false);
            }, 250);
        }
        if (menuItem.getItemId() == R.id.sub_purchase_menu_item_2) { //Order
            drawerLayout.closeDrawer(Gravity.LEFT, true);

            drawerLayout.postDelayed(() -> {
                Bundle bundle = new Bundle();
                bundle.putInt("tab_number", 1);
                Gilimanuk.force().goTo(HomeActivity.this, PurchaseParentTabFragment.TAG, bundle, R.id.fragment_frame, true, false);
            }, 250);
        }
        if (menuItem.getItemId() == R.id.sub_purchase_menu_item_3) { //Received
            drawerLayout.closeDrawer(Gravity.LEFT, true);

            drawerLayout.postDelayed(() -> {
                Bundle bundle = new Bundle();
                bundle.putInt("tab_number", 2);
                Gilimanuk.force().goTo(HomeActivity.this, PurchaseParentTabFragment.TAG, bundle, R.id.fragment_frame, true, false);
            }, 250);
        }
        if (menuItem.getItemId() == R.id.sub_purchase_menu_item_4) { //Transaction
            drawerLayout.closeDrawer(Gravity.LEFT, true);

            drawerLayout.postDelayed(() -> {
                Bundle bundle = new Bundle();
                bundle.putInt("tab_number", 3);
                Gilimanuk.force().goTo(HomeActivity.this, PurchaseParentTabFragment.TAG, bundle, R.id.fragment_frame, true, false);
            }, 250);
        }

//        if (menuItem.getItemId() == R.id.setting_menu_item) {
//            drawerLayout.closeDrawers();
//            Gilimanuk.withAnim(R.anim.enter_from_right, R.anim.exit_to_left).goTo(HomeActivity.this, SettingsFragment.TAG, R.id.fragment_frame, true, false);
//        }
//        if (menuItem.getItemId() == R.id.about_menu_item) {
//            drawerLayout.closeDrawers();
//            Gilimanuk.withAnim(R.anim.enter_from_right, R.anim.exit_to_left).goTo(HomeActivity.this, AboutFragment.TAG, R.id.fragment_frame, true, false);
//        }

        if (menuItem.getItemId() == R.id.logout_menu_item) {
            drawerLayout.closeDrawer(Gravity.LEFT, true);

            showDialog(getString(R.string.logout_confirmation), getString(R.string.logout_confirmation_message), (dialog, which) -> {
                HttpInstance.withProgress(HomeActivity.this).observe(HttpInstance.create(LoginService.class).logout(), response -> {
                    HttpInstance.getHeaderMap().remove(Initial.TOKEN);
                    SystemApp.deleteProfile();
                    SystemApp.deleteChat();
                    SystemApp.deleteGlide(HomeActivity.this);
                    Observable.fromCallable(() -> {
                        Gilimanuk.restart(HomeActivity.this, SplashActivity.class);
                        return true;
                    }).subscribeOn(Schedulers.newThread()).subscribe();
                }, throwable -> {
                    showSnackbar(throwable, v -> {});
                }, false);
            }, (dialog, which) -> dialog.dismiss());
        }
        return false;
    }

    public void focusMenu(int menuId) {
        for (SublimeBaseMenuItem sublimeBaseMenuItem : sublimeNavigationView.getMenu().getVisibleItems()) {
            sublimeBaseMenuItem.setChecked(false);
        }
        sublimeNavigationView.getMenu().getMenuItem(menuId).setChecked(true);

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        WLog.i(TAG, "On New Intent");
    }



}
