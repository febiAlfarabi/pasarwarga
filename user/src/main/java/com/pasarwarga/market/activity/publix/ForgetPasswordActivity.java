package com.pasarwarga.market.activity.publix;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.WLog;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.http.basemarket.service.AdministrationService;
import com.alfarabi.base.tools.InputTools;
import com.alfarabi.base.tools.ResponseTools;
import com.pasarwarga.market.R;
import com.alfarabi.base.activity.BasePublicActivity;

import java.util.HashMap;

import butterknife.BindView;
import cn.nekocode.rxactivityresult.RxActivityResult;

public class ForgetPasswordActivity extends BasePublicActivity {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.email_et) EditText emailEditText;
    @BindView(R.id.submit_btn) Button submitButton;
    @BindView(R.id.register_tv) TextView registerTextView;

    @Override
    public CoordinatorLayout getCoordinatorLayout() {
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(v -> {
            onBackPressed();
        });

        registerTextView.setOnClickListener(v -> {
            RxActivityResult.startActivityForResult(this, new Intent(this, RegistrationActivity.class), BasePublicActivity.REGISTER)
                    .subscribe(result -> {
                        if (result.getRequestCode() == BasePublicActivity.REGISTER && result.isOk()) {
                            WLog.i(TAG, "ON Register Result ");
                            setResult(Activity.RESULT_OK);
                            finish();
                        }
                    });


        });
        submitButton.setOnClickListener(v -> {
            if(InputTools.isValidEmail(emailEditText)){
                HashMap<String, String> hash = new HashMap<String, String>();
                hash.put("email", emailEditText.getText().toString());
                HttpInstance.withProgress(this).observe(HttpInstance.create(AdministrationService.class).forgetPasssword(hash), forgetPassword -> {
                    ResponseTools.validate(this, forgetPassword);
                    if(forgetPassword.isStatus()){
                        setResult(Activity.RESULT_OK);
                        WindowFlow.startActivity(this, ResetPasswordActivity.class, true);
                    }else{
                        showDialog(forgetPassword.getMessage(), (dialog, which) -> {
                            dialog.dismiss();
                        });
                    }
                }, throwable -> {
                    showSnackbar(throwable, v1 -> {});
                });
            }
        });
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.activity_forget_password;
    }
}
