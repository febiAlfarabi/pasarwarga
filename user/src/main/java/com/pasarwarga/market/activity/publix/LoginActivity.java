package com.pasarwarga.market.activity.publix;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;

import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.activity.publix.BaseLoginActivity;
import com.pasarwarga.market.R;
import com.pasarwarga.market.fragment.publix.LoginFormFragment;

public class LoginActivity extends BaseLoginActivity {

    @Override
    public CoordinatorLayout getCoordinatorLayout() {
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WindowFlow.goFirst(this, LoginFormFragment.TAG, R.id.fragment_frame);
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.activity_login;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

    }
}
