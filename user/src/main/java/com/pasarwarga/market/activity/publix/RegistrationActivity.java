package com.pasarwarga.market.activity.publix;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;

import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.activity.publix.BaseRegistrationActivity;
import com.pasarwarga.market.R;
import com.pasarwarga.market.fragment.RegistrationForm1Fragment;

public class RegistrationActivity extends BaseRegistrationActivity {

    @Override
    public CoordinatorLayout getCoordinatorLayout() {
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WindowFlow.goFirst(this, RegistrationForm1Fragment.TAG, R.id.fragment_frame);
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.activity_registration;
    }

    @Override
    public void onBackPressed() {
        if(WindowFlow.currentIs(this, RegistrationForm1Fragment.TAG)){
            finish();
        }else{
            super.onBackPressed();
        }
    }

}
