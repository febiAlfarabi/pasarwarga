package com.pasarwarga.market.activity.publix;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.EditText;

import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.activity.BasePublicActivity;
import com.alfarabi.base.http.basemarket.service.AdministrationService;
import com.alfarabi.base.tools.InputTools;
import com.alfarabi.base.tools.ResponseTools;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.SplashActivity;

import java.util.HashMap;

import butterknife.BindView;

public class ResetPasswordActivity extends BasePublicActivity {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.reset_code_et) EditText resetCodeEditText;
    @BindView(R.id.new_password_et) EditText newPasswordEditText;
    @BindView(R.id.confirm_new_password_et) EditText confirmNewPasswordEditText;
    @BindView(R.id.submit_btn) Button submitButton;

    @Override
    public CoordinatorLayout getCoordinatorLayout() {
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(v -> {
            onBackPressed();
        });

        submitButton.setOnClickListener(v -> {
            if(InputTools.isComplete(resetCodeEditText, newPasswordEditText, confirmNewPasswordEditText) && InputTools.equals(newPasswordEditText, confirmNewPasswordEditText)){
                HashMap<String, String> hash = new HashMap<String, String>();
                hash.put("reset_code", resetCodeEditText.getText().toString());
                hash.put("new_password", newPasswordEditText.getText().toString());
                HttpInstance.withProgress(this).observe(HttpInstance.create(AdministrationService.class).resetPasssword(hash), resetPassword -> {
                    ResponseTools.validate(this, resetPassword);
                    if(resetPassword.isStatus()){
                        WindowFlow.restart(this, LoginActivity.class);
//                        setResult(Activity.RESULT_OK);
//                        finish();
                    }else{
                        showDialog(resetPassword.getMessage(), (dialog, which) -> {
                            dialog.dismiss();
                        });
                    }
                }, throwable -> {
                    showSnackbar(throwable, v1 -> {});
                });
            }
        });
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.activity_reset_password;
    }

    @Override
    public void onBackPressed() {
        WindowFlow.restart(this, SplashActivity.class);
    }
}
