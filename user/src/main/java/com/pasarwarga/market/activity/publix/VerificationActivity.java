package com.pasarwarga.market.activity.publix;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.base.activity.publix.BaseLoginActivity;
import com.alfarabi.base.http.basemarket.service.VerificationService;
import com.alfarabi.base.model.MyProfile;
import com.alfarabi.base.realm.RealmDao;
import com.alfarabi.base.service.SMSReceiver;
import com.alfarabi.base.tools.Initial;
import com.alfarabi.base.tools.InputTools;
import com.alfarabi.base.tools.ResponseTools;
import com.pasarwarga.market.R;
import com.pasarwarga.market.tools.SystemApp;

import net.soroushjavdan.customfontwidgets.CTextView;

import java.util.HashMap;

import butterknife.BindView;

public class VerificationActivity extends BaseLoginActivity {

    public static final String TAG = VerificationActivity.class.getName();


    @BindView(R.id.coordinator_layout) CoordinatorLayout coordinatorLayout;
    @BindView(R.id.phonenumber_et) EditText phoneNumberEditText;
    @BindView(R.id.change_number_tv) TextView changeNumberTextView;
    @BindView(R.id.verification_code_et) EditText verificationEditText;
    @BindView(R.id.submit_btn) Button submitButton;
    @BindView(R.id.resend_btn) Button resendButton;
    @BindView(R.id.skip_tv) CTextView skipTextView;

    private MyProfile myProfile;
    private String defaultPhoneNumber = "";

    boolean verifying = false ;

    //@BindExtra User user;


    @Override
    public CoordinatorLayout getCoordinatorLayout() {
        return coordinatorLayout;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myProfile = RealmDao.instance().get(MyProfile.class);
        this.defaultPhoneNumber = myProfile.getUser().getPhone();
        phoneNumberEditText.setText(this.defaultPhoneNumber);

        changeNumberTextView.setOnTouchListener((v, event) -> {
            phoneNumberEditText.setFocusableInTouchMode(true);
            phoneNumberEditText.requestFocus();
            return false ;
        });
        resendButton.setOnClickListener(v -> {
            if (skipTextView.getText().toString().equalsIgnoreCase(getString(R.string.verify_later))) {
                if (InputTools.isComplete(phoneNumberEditText) && !phoneNumberEditText.getText().toString().equals(this.defaultPhoneNumber)) {
                    this.defaultPhoneNumber = phoneNumberEditText.getText().toString();
                    request(this.defaultPhoneNumber);
                }else{
                    showSnackbar(getString(R.string.same_phone_number_verification_please_choose_another), v1 -> {});
                }
            }else{
                showSnackbar(getString(R.string.please_wait_until_countdown_finished), v1 -> {});
            }
        });

        submitButton.setOnClickListener(v -> {
            if (InputTools.isComplete(phoneNumberEditText, verificationEditText)) {
                verify(phoneNumberEditText.getText().toString(), verificationEditText.getText().toString());
            }
        });

        skipTextView.setOnClickListener(v -> {
            if (skipTextView.getText().toString().equalsIgnoreCase(getString(R.string.verify_later))) {
                setResult(Activity.RESULT_OK);
                finish();
            }
        });
        request(this.defaultPhoneNumber);

    }

    public void countdownWaitingSms(final int second) {
        skipTextView.postDelayed(() -> {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                if(isDestroyed()){
                    return;
                }
            }
            if (second > 0) {
                skipTextView.setText("00:" + (second > 9 ? String.valueOf(second) : "0" + String.valueOf(second)));
                int remain = second - 1;
                countdownWaitingSms(remain);
                changeNumberTextView.setVisibility(View.GONE);
            } else if (second == 0){
                skipTextView.setText(R.string.verify_later);
                changeNumberTextView.setVisibility(View.VISIBLE);
            }
        }, 1000);
    }


    public void request(String phoneNumber) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("phone_number", phoneNumber);
        phoneNumberEditText.setFocusableInTouchMode(false);
        phoneNumberEditText.clearFocus();

        HttpInstance.withProgress(this).observe(HttpInstance.create(VerificationService.class).request(hashMap), request -> {
            ResponseTools.validate(this, request);
            verifying = false ;
            if(request.isStatus()){
                SMSReceiver.bindListener(messageText -> {
                    String code  = messageText.replaceAll("[^0-9]", "");
                    verificationEditText.setText(code);
                    verify(phoneNumberEditText.getText().toString(), verificationEditText.getText().toString());
                });
                countdownWaitingSms(29);
                showSnackbar(getString(R.string.sms_will_be_sent_as_verification), v -> {});
            }else{
                showDialog(getString(R.string.rejection), request.getMessage(), (dialog, which) -> {
                    dialog.dismiss();
                });
            }
        }, throwable -> showSnackbar(throwable,v -> {}), false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        phoneNumberEditText.setFocusableInTouchMode(false);
        phoneNumberEditText.clearFocus();

    }

    public void verify(String phoneNumber, String verificationCode) {
        if(verifying){
            return;
        }
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("phone_number", phoneNumber);
        hashMap.put("otp_code", verificationCode);
        submitButton.setEnabled(false);
        submitButton.setBackgroundColor(getResources().getColor(R.color.grey_300));
        verifying = true ;
        HttpInstance.withProgress(this).observe(HttpInstance.create(VerificationService.class).verify(hashMap), request -> {
            ResponseTools.validate(this, request);
            submitButton.setEnabled(true);
            submitButton.setBackground(getResources().getDrawable(R.drawable.white_button));
            verifying = false ;
            if (request.isStatus() && request.getUser() != null) {
                MyProfile myProfile = new MyProfile();
                myProfile.setUser(request.getUser());
                HttpInstance.putHeaderMap(Initial.TOKEN, myProfile.getUser().getToken());
                SystemApp.setProfile(myProfile);
                setResult(Activity.RESULT_OK);
                finish();
            } else {
                showDialog(getString(R.string.rejection), request.getMessage(), (dialog, which) -> {
                    dialog.dismiss();
                });
            }

        }, throwable -> showDialog(throwable, (dialog, which) -> {}), false);
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.activity_verification_form;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(Activity.RESULT_OK);
        finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }


}
