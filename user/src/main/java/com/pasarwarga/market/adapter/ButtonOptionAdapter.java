package com.pasarwarga.market.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.alfarabi.base.view.interfaze.DataSpinnerInterface;
import com.pasarwarga.market.R;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class ButtonOptionAdapter<T extends DataSpinnerInterface> extends RecyclerView.Adapter<ButtonOptionAdapter.ViewHolder> {
    @Getter@Setter int mSelectedItem = -1;
    @Getter public List<T> mItems;
    private Context mContext;

    public ButtonOptionAdapter(Context context, List<T> items) {
        mContext = context;
        mItems = items;
    }

    public void setmItems(List<T> mItems) {
        this.mItems = mItems;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(ButtonOptionAdapter.ViewHolder viewHolder, final int position) {
        viewHolder.showData(mItems.get(position), position);
    }

    @Override
    public int getItemCount() {
        return mItems!=null?mItems.size():0;
    }


    @Override
    public ButtonOptionAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        final View view = inflater.inflate(R.layout.item_button_option, viewGroup, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

//        public RadioButton mRadio;
//        public TextView mText;

        private Button button ;

        public ViewHolder(final View inflate) {
            super(inflate);

        }
        public void showData(T object, int position){
            button = (Button) itemView.findViewById(R.id.option_button);
            View.OnClickListener clickListener = v -> {
                mSelectedItem = getAdapterPosition();
                notifyItemRangeChanged(0, mItems.size());
            };
            button.setOnClickListener(clickListener);
            button.setBackgroundDrawable(position==mSelectedItem?
                    mContext.getResources().getDrawable(R.drawable.green_shape_border_button)
                    :
                    mContext.getResources().getDrawable(R.drawable.white_shape_border_button));
            button.setText(object.spinnerLabel());
            button.setTextColor(position==mSelectedItem?Color.WHITE:Color.BLACK);
//            mRadio.setOnClickListener(clickListener);
//            mText.setText(object.spinnerLabel());
//            mRadio.setChecked(position == mSelectedItem);
        }

    }

}