package com.pasarwarga.market.adapter;

import android.content.Context;
import android.support.v4.app.FragmentManager;

import com.alfarabi.alfalibs.adapters.viewpager.BaseTabPagerAdapter;
import com.alfarabi.base.fragment.BaseUserFragment;
import com.alfarabi.base.model.MyProfile;
import com.alfarabi.base.realm.RealmDao;
import com.pasarwarga.market.R;
import com.pasarwarga.market.fragment.review.BuyerFeedbackFragment;
import com.pasarwarga.market.fragment.review.NewFeedbackFragment;
import com.pasarwarga.market.fragment.review.MyFeedbackFragment;

import java.util.Arrays;

import lombok.Getter;

/**
 * Created by Alfarabi on 6/20/17.
 */

public class FeedbackTabPagerAdapter<CF extends BaseUserFragment> extends BaseTabPagerAdapter<CF> {

    private Context context ;
    private FragmentManager fm ;
    private MyProfile myProfile ;
    String[] titles ;
    @Getter CF[] fragments ;

    public FeedbackTabPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.fm = fm ;
        this.context = context ;
        myProfile = RealmDao.instance().get(MyProfile.class);
        if(myProfile.getUser().isSeller()){
            titles = new String[]{
                    context.getResources().getString(R.string.new_review),
                    context.getResources().getString(R.string.my_review),
                    context.getResources().getString(R.string.buyer_review)
            };
            fragments = (CF[]) Arrays.asList((CF) new NewFeedbackFragment(), (CF) new MyFeedbackFragment(), (CF) new BuyerFeedbackFragment()).toArray();
        }else{
            titles = new String[]{
                    context.getResources().getString(R.string.new_review),
                    context.getResources().getString(R.string.my_review)
            };
            fragments = (CF[]) Arrays.asList((CF) new NewFeedbackFragment(), (CF) new MyFeedbackFragment()).toArray();

        }
    }

    @Override
    public String[] titles() {
        return titles;
    }

    @Override
    public CF[] fragmentClasses() {
        return fragments;
    }
}
