package com.pasarwarga.market.adapter;

import android.content.Context;
import android.support.v4.app.FragmentManager;

import com.alfarabi.alfalibs.adapters.viewpager.BaseTabPagerAdapter;
import com.alfarabi.base.fragment.BaseUserFragment;
import com.pasarwarga.market.R;
import com.pasarwarga.market.fragment.transaction.status.NewOrderListFragment;
import com.pasarwarga.market.fragment.transaction.status.OnDeliveryOrderListFragment;
import com.pasarwarga.market.fragment.transaction.status.OnProgressOrderListFragment;
import com.pasarwarga.market.fragment.transaction.status.PaidOrderListFragment;
import com.pasarwarga.market.fragment.transaction.status.ReceivedOrderListFragment;
import com.pasarwarga.market.fragment.transaction.status.RejectedOrderListFragment;
import com.pasarwarga.market.fragment.transaction.status.SucceedOrderListFragment;

import java.util.Arrays;

/**
 * Created by Alfarabi on 6/20/17.
 */

public class FilterTabPagerAdapter<CF extends BaseUserFragment> extends BaseTabPagerAdapter<CF> {

    private Context context ;
    private FragmentManager fm ;

    public FilterTabPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.fm = fm ;
        this.context = context ;
    }

    @Override
    public String[] titles() {
        return new String[]{
                context.getResources().getString(R.string.new_order),
                context.getResources().getString(R.string.paid_order),
                context.getResources().getString(R.string.on_progress_order),
                context.getResources().getString(R.string.on_delivery_order),
                context.getResources().getString(R.string.received_order),
                context.getResources().getString(R.string.succeed_order),
                context.getResources().getString(R.string.rejected_order)
        }
        ;
    }

    @Override
    public CF[] fragmentClasses() {
        return (CF[]) Arrays.asList(
                (CF) new NewOrderListFragment(),
                (CF) new PaidOrderListFragment(),
                (CF) new OnProgressOrderListFragment(),
                (CF) new OnDeliveryOrderListFragment(),
                (CF) new ReceivedOrderListFragment(),
                (CF) new SucceedOrderListFragment(),
                (CF) new RejectedOrderListFragment()
                ).toArray();
    }
}
