package com.pasarwarga.market.adapter;

import com.alfarabi.alfalibs.adapters.viewpager.BaseTabPagerAdapter;
import com.alfarabi.alfalibs.fragments.SimpleBaseFragment;
import com.pasarwarga.market.R;
import com.pasarwarga.market.fragment.store.PaymentFragment;
import com.pasarwarga.market.fragment.store.product.ProductDescriptionFragment;
import com.pasarwarga.market.fragment.store.product.ProductDetailFragment;
import com.pasarwarga.market.fragment.store.ReviewFragment;
import com.alfarabi.base.fragment.SimpleBaseFragmentInitiator;
import com.alfarabi.base.model.Product;

/**
 * Created by Alfarabi on 7/11/17.
 */

public class ProductDetailTabPagerAdapter extends BaseTabPagerAdapter {

    public static final String TAG = ProductDetailTabPagerAdapter.class.getName();

    private ProductDetailFragment productDetailFragment ;
    private  Product product ;

    private SimpleBaseFragmentInitiator[] simpleBaseFragmentInitiator ;

    public ProductDetailTabPagerAdapter(ProductDetailFragment productDetailFragment, Product product) {
        super(productDetailFragment.getChildFragmentManager());
        this.productDetailFragment = productDetailFragment ;
        this.product = product ;
        simpleBaseFragmentInitiator = new SimpleBaseFragmentInitiator[3];
        simpleBaseFragmentInitiator[0] = new ProductDescriptionFragment();
        simpleBaseFragmentInitiator[0].setObject(product);

        simpleBaseFragmentInitiator[1] = new ReviewFragment();
        simpleBaseFragmentInitiator[1].setObject(product);

        simpleBaseFragmentInitiator[2] = new PaymentFragment();
        simpleBaseFragmentInitiator[2].setObject(product);
    }

    public void setProduct(Product product) {
        this.product = product;
        simpleBaseFragmentInitiator = new SimpleBaseFragmentInitiator[3];
        simpleBaseFragmentInitiator[0] = new ProductDescriptionFragment();
        simpleBaseFragmentInitiator[0].setObject(product);

        simpleBaseFragmentInitiator[1] = new ReviewFragment();
        simpleBaseFragmentInitiator[1].setObject(product);

        simpleBaseFragmentInitiator[2] = new PaymentFragment();
        simpleBaseFragmentInitiator[2].setObject(product);
    }

    @Override
    public String[] titles() {
        return new String[]{productDetailFragment.getString(R.string.detail), productDetailFragment.getString(R.string.review), productDetailFragment.getString(R.string.payment)};
    }

    @Override
    public SimpleBaseFragment[] fragmentClasses() {
        return simpleBaseFragmentInitiator ;
//        return new SimpleBaseFragment[]{
//                SimpleBaseFragmentInitiator.instance(ProductDescriptionFragment.class, product),
//                SimpleBaseFragmentInitiator.instance(ReviewFragment.class, product),
//                SimpleBaseFragmentInitiator.instance(PaymentFragment.class, product)
//        };
    }
}
