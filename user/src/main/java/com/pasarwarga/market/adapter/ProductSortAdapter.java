package com.pasarwarga.market.adapter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pasarwarga.market.R;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by USER on 8/23/2017.
 */

public class ProductSortAdapter extends RecyclerView.Adapter<ProductSortAdapter.ViewHolder> {

    @Nullable private OnSortClickListener onSortClickListener;
    @NonNull private Sort selectedSort;

    public List<Sort> listItems = new ArrayList<>();

    public ProductSortAdapter() {
        listItems.clear();
        Sort sort;

        sort = new Sort();
        sort.setSortValue("newest");
        sort.setSortText("Newest");
        listItems.add(sort);
        selectedSort = sort;

        sort = new Sort();
        sort.setSortValue("most_expensive");
        sort.setSortText("Most Expensive");
        listItems.add(sort);

        sort = new Sort();
        sort.setSortValue("cheapest");
        sort.setSortText("Cheapest");
        listItems.add(sort);

        sort = new Sort();
        sort.setSortValue("relevance");
        sort.setSortText("Relevance");
        listItems.add(sort);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.viewholder_product_sort, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bindView();
    }

    @Override
    public int getItemCount() {
        return listItems != null ? listItems.size() : 0;
    }

    @NonNull
    public Sort getSelectedSort() {
        return selectedSort;
    }

    public Sort getItem(int position) {
        return listItems.get(position);
    }

    @Nullable
    public OnSortClickListener getOnSortClickListener() {
        return onSortClickListener;
    }

    public void setOnSortClickListener(@Nullable OnSortClickListener onSortClickListener) {
        this.onSortClickListener = onSortClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView sortTextView;
        public ImageView checkImageView;

        public ViewHolder(View itemView) {
            super(itemView);
            sortTextView = (TextView) itemView.findViewById(R.id.sort_tv);
            checkImageView = (ImageView) itemView.findViewById(R.id.checklist_iv);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v == itemView && onSortClickListener != null) {
                selectedSort = getItem(getAdapterPosition());
                onSortClickListener.onSortClicked(selectedSort);
                notifyDataSetChanged();
            }
        }

        public void bindView() {
            final Sort sort = getItem(getAdapterPosition());

            sortTextView.setText(sort.getSortText());
            if (selectedSort == sort) {
                checkImageView.setVisibility(View.VISIBLE);
            } else {
                checkImageView.setVisibility(View.INVISIBLE);
            }
        }
    }

    public interface OnSortClickListener {

        void onSortClicked(Sort selectedSort);
    }

    public class Sort {

        @Nullable private @Getter @Setter String sortValue;
        @Nullable private @Getter @Setter String sortText;

        public Sort() {
        }
    }
}
