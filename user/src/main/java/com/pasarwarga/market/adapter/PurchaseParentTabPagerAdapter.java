package com.pasarwarga.market.adapter;

import android.content.Context;
import android.support.v4.app.FragmentManager;

import com.alfarabi.alfalibs.adapters.viewpager.BaseTabPagerAdapter;
import com.alfarabi.base.fragment.BaseUserFragment;
import com.alfarabi.base.view.TabLayout;
import com.pasarwarga.market.R;
import com.pasarwarga.market.fragment.store.transaction.purchase.PurchaseOrderListFragment;
import com.pasarwarga.market.fragment.store.transaction.purchase.PurchasePaymentListFragment;
import com.pasarwarga.market.fragment.store.transaction.purchase.PurchaseReceiptListFragment;
import com.pasarwarga.market.fragment.store.transaction.purchase.PurchaseTransactionListFragment;

import java.util.Arrays;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by USER on 9/29/2017.
 */

public class PurchaseParentTabPagerAdapter<CF extends BaseUserFragment> extends BaseTabPagerAdapter<CF> {

    private Context context;
    private FragmentManager fm;
    private @Getter @Setter CF[] fragmentClasses;

    public PurchaseParentTabPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.fm = fm;
        this.context = context;

        fragmentClasses = (CF[]) Arrays.asList(
                (CF) new PurchasePaymentListFragment(),
                (CF) new PurchaseOrderListFragment(),
                (CF) new PurchaseReceiptListFragment(),
                (CF) new PurchaseTransactionListFragment()
        ).toArray();
    }

    @Override
    public CF[] fragmentClasses() {
        return fragmentClasses;
    }

    @Override
    public String[] titles() {
        return new String[]{
                context.getResources().getString(R.string.payment_status),
                context.getResources().getString(R.string.order_status),
                context.getResources().getString(R.string.delivery_confirm),
                context.getResources().getString(R.string.purchase_list)
        };
    }

    public int[] unfocusedIcons() {
        return null;
    }

    public int[] focusedIcons() {
        return null;
    }

    public void initIcon(TabLayout tabLayout, int focusPos) {
        if (focusedIcons() != null) {
            for (int i = 0; i < focusedIcons().length; i++) {
                if (unfocusedIcons() != null && unfocusedIcons().length > i) {
                    tabLayout.setIconAt(i, unfocusedIcons()[i]);
                }
                if (focusPos == i && focusedIcons().length > i) {
                    tabLayout.setIconAt(i, focusedIcons()[i]);
                }
            }
        }

    }
}
