package com.pasarwarga.market.adapter;

import com.alfarabi.alfalibs.adapters.viewpager.BaseTabPagerAdapter;
import com.alfarabi.alfalibs.fragments.SimpleBaseFragment;
import com.pasarwarga.market.R;
import com.pasarwarga.market.fragment.store.product.ProductSearchFragment;
import com.pasarwarga.market.fragment.store.SearchFragment;
import com.pasarwarga.market.fragment.store.StoreSearchFragment;

import lombok.Getter;

/**
 * Created by Alfarabi on 7/11/17.
 */

public class SearchTabPagerAdapter<T extends SimpleBaseFragment> extends BaseTabPagerAdapter<T> {

    public static final String TAG = SearchTabPagerAdapter.class.getName();

    private SearchFragment searchFragment ;
    @Getter String query ;
    private T[] fragments;

    public SearchTabPagerAdapter(SearchFragment searchFragment, String query) {
        super(searchFragment.getFragmentManager());
        this.searchFragment = searchFragment ;
        this.query = query ;
        ProductSearchFragment productSearchFragment = new ProductSearchFragment();
        productSearchFragment.setInput(query);

        StoreSearchFragment storeSearchFragment = new StoreSearchFragment();
        storeSearchFragment.setInput(query);
        fragments = (T[]) new SimpleBaseFragment[]{productSearchFragment, storeSearchFragment};
    }

    @Override
    public String[] titles() {
        return new String[]{searchFragment.getString(R.string.product), searchFragment.getString(R.string.store)};
    }

    @Override
    public T[] fragmentClasses() {
        return fragments ;
//        return new SimpleBaseFragmentInitiator[][]{
//                SimpleBaseFragmentInitiator.instance(ProductSearchFragment.class, query),
//                SimpleBaseFragmentInitiator.instance(StoreSearchFragment.class, query)
//        };
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public void setQuery(String query, int position) {
        this.query = query;
        if(fragments[position] instanceof ProductSearchFragment){
            ((ProductSearchFragment)fragments[position]).setInput(query);
        }else if(fragments[position] instanceof StoreSearchFragment){
            ((StoreSearchFragment)fragments[position]).setInput(query);
        }
    }

}
