package com.pasarwarga.market.adapter;

import android.content.Context;
import android.support.v4.app.FragmentManager;

import com.alfarabi.alfalibs.adapters.viewpager.BaseTabPagerAdapter;
import com.alfarabi.base.fragment.BaseUserFragment;
import com.alfarabi.base.model.shop.SellerDashboard;
import com.pasarwarga.market.R;
import com.pasarwarga.market.fragment.asseller.SellerInformationFragment;
import com.pasarwarga.market.fragment.asseller.SellerNotesFragment;
import com.pasarwarga.market.fragment.asseller.SellerProductFragment;

import java.util.Arrays;

import lombok.Getter;

/**
 * Created by Alfarabi on 6/20/17.
 */

public class SellerDashboardTabPagerAdapter<CF extends BaseUserFragment> extends BaseTabPagerAdapter<CF> {

    private Context context ;
    private FragmentManager fm ;
    @Getter CF[] fragments ;
    @Getter private SellerDashboard sellerDashboard ;

    private SellerProductFragment sellerProductFragment ;
    private SellerInformationFragment sellerInformationFragment ;
    private SellerNotesFragment sellerNotesFragment ;


    public SellerDashboardTabPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.fm = fm ;
        this.context = context ;
        sellerProductFragment = new SellerProductFragment();
        sellerInformationFragment = new SellerInformationFragment();
        sellerNotesFragment = new SellerNotesFragment();
    }

    public void init(SellerDashboard sellerDashboard) {
        this.sellerDashboard = sellerDashboard;
        if(sellerProductFragment.getArguments()==null || !sellerProductFragment.getArguments().containsKey("sellerDashboard")){
//            sellerProductFragment.setArguments(Bundler.wrap(SellerProductFragment.class, sellerDashboard));
            sellerProductFragment.setObject(sellerDashboard);
        }
        if(sellerInformationFragment.getArguments()==null || !sellerInformationFragment.getArguments().containsKey("sellerDashboard")){
//            sellerInformationFragment.setArguments(Bundler.wrap(SellerInformationFragment.class, sellerDashboard));
            sellerInformationFragment.setObject(sellerDashboard);
        }
        if(sellerNotesFragment.getArguments()==null || !sellerNotesFragment.getArguments().containsKey("sellerDashboard")){
//            sellerNotesFragment.setArguments(Bundler.wrap(SellerNotesFragment.class, sellerDashboard));
            sellerNotesFragment.setObject(sellerDashboard);

        }

        fragments = (CF[]) Arrays.asList((CF) sellerProductFragment, (CF) sellerInformationFragment, (CF) sellerNotesFragment).toArray();
    }

    @Override
    public String[] titles() {
        return new String[]{
                context.getResources().getString(R.string.product),
                context.getResources().getString(R.string.information),
                context.getResources().getString(R.string.notes)
        };
    }

    @Override
    public CF[] fragmentClasses() {
        return fragments;
    }
}
