package com.pasarwarga.market.adapter;

import android.content.Context;
import android.support.v4.app.FragmentManager;

import com.alfarabi.alfalibs.adapters.viewpager.BaseTabPagerAdapter;
import com.alfarabi.base.fragment.BaseUserFragment;
import com.alfarabi.base.fragment.SimpleBaseFragmentInitiator;
import com.alfarabi.base.model.shop.SellerDashboard;
import com.alfarabi.base.realm.RealmDao;
import com.alfarabi.base.view.TabLayout;
import com.pasarwarga.market.R;
import com.pasarwarga.market.fragment.asseller.SellerProductFragment;
import com.pasarwarga.market.fragment.asseller.SellerProfileFragment;
import com.pasarwarga.market.fragment.asseller.SellerPurchaseFragment;
import com.pasarwarga.market.fragment.asseller.SellerSettingFragment;

import java.util.Arrays;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 6/20/17.
 */

public class SellerParentTabPagerAdapter<CF extends BaseUserFragment> extends BaseTabPagerAdapter<CF> {

    private Context context ;
    private FragmentManager fm ;
    private @Getter@Setter CF[] fragmentClasses ;

    public SellerParentTabPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.fm = fm ;
        this.context = context ;
        fragmentClasses = (CF[]) Arrays.asList(
                (CF) SimpleBaseFragmentInitiator.instance(SellerProfileFragment.class, RealmDao.instance().get(SellerDashboard.class)),
                (CF) SimpleBaseFragmentInitiator.instance(SellerPurchaseFragment.class, RealmDao.instance().get(SellerDashboard.class)),
                (CF) SimpleBaseFragmentInitiator.instance(SellerProductFragment.class, RealmDao.instance().get(SellerDashboard.class)),
                (CF) SimpleBaseFragmentInitiator.instance(SellerSettingFragment.class, RealmDao.instance().get(SellerDashboard.class))
        ).toArray();
    }

    @Override
    public String[] titles() {
        return new String[]{
                context.getResources().getString(R.string.shop_name),
                context.getResources().getString(R.string.purchase),
                context.getResources().getString(R.string.product),
                context.getResources().getString(R.string.setting)
        }
        ;
    }

    public int[] unfocusedIcons() {
        return new int[]{
                R.drawable.ic_store_black_24dp, R.drawable.ic_purchase_black_24dp, R.drawable.ic_product_black_24dp, R.drawable.ic_setting_black_24dp
        };
    }
    public int[] focusedIcons() {
        return new int[]{
                R.drawable.ic_store_red_24dp, R.drawable.ic_purchase_red_24dp, R.drawable.ic_product_red_24dp, R.drawable.ic_setting_red_24dp
        };
    }

    @Override
    public CF[] fragmentClasses() {
        return fragmentClasses;
    }


    public void initIcon(TabLayout tabLayout, int focusPos){
        for (int i = 0; i < focusedIcons().length; i++) {
            if(unfocusedIcons()!=null && unfocusedIcons().length>i){
                tabLayout.setIconAt(i, unfocusedIcons()[i]);
            }
            if(focusPos==i && focusedIcons()!=null && focusedIcons().length>i){
                tabLayout.setIconAt(i, focusedIcons()[i]);
            }
        }
    }



}
