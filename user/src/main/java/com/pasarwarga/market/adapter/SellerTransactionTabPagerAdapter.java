package com.pasarwarga.market.adapter;

import android.content.Context;
import android.support.v4.app.FragmentManager;

import com.alfarabi.alfalibs.adapters.viewpager.BaseTabPagerAdapter;
import com.alfarabi.base.fragment.BaseUserFragment;
import com.alfarabi.base.fragment.SimpleBaseFragmentInitiator;
import com.alfarabi.base.model.shop.SellerDashboard;
import com.alfarabi.base.realm.RealmDao;
import com.alfarabi.base.view.TabLayout;
import com.pasarwarga.market.R;
import com.pasarwarga.market.fragment.asseller.transactiontab.OrderPackagingListFragment;
import com.pasarwarga.market.fragment.asseller.transactiontab.OrderRequestListFragment;
import com.pasarwarga.market.fragment.asseller.transactiontab.OrderStatusListFragment;
import com.pasarwarga.market.fragment.asseller.transactiontab.TransactionListFragment;

import java.util.Arrays;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 6/20/17.
 */

public class SellerTransactionTabPagerAdapter<CF extends BaseUserFragment> extends BaseTabPagerAdapter<CF> {

    private Context context ;
    private FragmentManager fm ;
    private @Getter@Setter CF[] fragmentClasses ;

    public SellerTransactionTabPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.fm = fm ;
        this.context = context ;
        fragmentClasses = (CF[]) Arrays.asList(
                (CF) SimpleBaseFragmentInitiator.instance(OrderRequestListFragment.class, RealmDao.instance().get(SellerDashboard.class)),
                (CF) SimpleBaseFragmentInitiator.instance(OrderPackagingListFragment.class, RealmDao.instance().get(SellerDashboard.class)),
                (CF) SimpleBaseFragmentInitiator.instance(OrderStatusListFragment.class, RealmDao.instance().get(SellerDashboard.class)),
                (CF) SimpleBaseFragmentInitiator.instance(TransactionListFragment.class, RealmDao.instance().get(SellerDashboard.class))
        ).toArray();
    }

    @Override
    public String[] titles() {
        return new String[]{
                context.getResources().getString(R.string.new_order),
                context.getResources().getString(R.string.shipping_confirmation),
                context.getResources().getString(R.string.shipping_status),
                context.getResources().getString(R.string.transaction_list)
        }
        ;
    }

    public int[] unfocusedIcons() {
        return null ;
    }
    public int[] focusedIcons() {
        return null ;
    }

    @Override
    public CF[] fragmentClasses() {
        return fragmentClasses;
    }


    public void initIcon(TabLayout tabLayout, int focusPos){
        if(focusedIcons()!=null){
            for (int i = 0; i < focusedIcons().length; i++) {
                if(unfocusedIcons()!=null && unfocusedIcons().length>i){
                    tabLayout.setIconAt(i, unfocusedIcons()[i]);
                }
                if(focusPos==i && focusedIcons().length>i){
                    tabLayout.setIconAt(i, focusedIcons()[i]);
                }
            }
        }

    }



}
