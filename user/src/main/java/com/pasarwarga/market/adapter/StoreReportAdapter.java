package com.pasarwarga.market.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pasarwarga.market.R;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by USER on 8/23/2017.
 */

public class StoreReportAdapter extends RecyclerView.Adapter<StoreReportAdapter.ViewHolder> {

    @Nullable private OnSortClickListener onSortClickListener;
    @NonNull private Report selectedReport;

    public List<Report> listItems = new ArrayList<>();

    public StoreReportAdapter(Context context, String[] titles, String[] values) {
        listItems.clear();
        int i = 0;
        for (String title : titles) {
            Report sort = new Report();
            sort.setSortText(title);
            sort.setSortValue(values[i]);
            i++;
            listItems.add(sort);
            selectedReport = sort ;
        }
//        Report sort;
//
//        sort = new Report();
//        sort.setSortValue(context.getString(R.string.lorem_ipsum));
//        sort.setSortText(context.getString(R.string.lorem_ipsum2));
//        listItems.add(sort);
//        selectedReport = sort;
//
//        sort = new Report();
//        sort.setSortValue(context.getString(R.string.lorem_ipsum));
//        sort.setSortText(context.getString(R.string.lorem_ipsum2));
//        listItems.add(sort);
//        selectedReport = sort;
//
//        sort = new Report();
//        sort.setSortValue(context.getString(R.string.lorem_ipsum));
//        sort.setSortText(context.getString(R.string.lorem_ipsum2));
//        listItems.add(sort);
//        selectedReport = sort;
//
//        sort = new Report();
//        sort.setSortValue(context.getString(R.string.lorem_ipsum));
//        sort.setSortText(context.getString(R.string.lorem_ipsum2));
//        listItems.add(sort);
//        selectedReport = sort;


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.viewholder_report, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bindView();
    }

    @Override
    public int getItemCount() {
        return listItems != null ? listItems.size() : 0;
    }

    @NonNull
    public Report getselectedReport() {
        return selectedReport;
    }

    public Report getItem(int position) {
        return listItems.get(position);
    }

    @Nullable
    public OnSortClickListener getOnSortClickListener() {
        return onSortClickListener;
    }

    public void setOnSortClickListener(@Nullable OnSortClickListener onSortClickListener) {
        this.onSortClickListener = onSortClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView sortTextView;
        public TextView contentTextView;
        public ImageView checkImageView;

        public ViewHolder(View itemView) {
            super(itemView);
            sortTextView = (TextView) itemView.findViewById(R.id.sort_tv);
            contentTextView = (TextView) itemView.findViewById(R.id.content_tv);
            checkImageView = (ImageView) itemView.findViewById(R.id.checklist_iv);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v == itemView && onSortClickListener != null) {
                selectedReport = getItem(getAdapterPosition());
                onSortClickListener.onSortClicked(selectedReport);
                notifyDataSetChanged();
            }
        }

        public void bindView() {
            final Report report = getItem(getAdapterPosition());

            sortTextView.setText(report.getSortText());
            contentTextView.setText(report.getSortValue());
            if (selectedReport == report) {
                checkImageView.setVisibility(View.VISIBLE);
            } else {
                checkImageView.setVisibility(View.INVISIBLE);
            }
        }
    }

    public interface OnSortClickListener {

        void onSortClicked(Report selectedReport);
    }

    public class Report {

        @Nullable private @Getter @Setter String sortValue;
        @Nullable private @Getter @Setter String sortText;

        public Report() {
        }
    }
}
