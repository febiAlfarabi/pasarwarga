package com.pasarwarga.market.adapter;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.SimpleViewHolder;
import com.alfarabi.base.fragment.BaseUserFragment;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

/**
 * Created by Alfarabi on 9/27/17.
 */

public class StringRecyclerAdapter<BUF extends BaseUserFragment, S extends SimpleViewHolder, O extends Object> extends RecyclerView.Adapter<S>{

    private BUF buf ;
    @Getter List<O> objects = new ArrayList<>();
    private Class<S> vhClass ;

    public StringRecyclerAdapter(BUF buf, Class<S> vhClass, List<O> objects){
        this.buf = buf ;
        this.vhClass = vhClass ;
        this.objects = objects ;
    }

    public StringRecyclerAdapter initRecyclerView(RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(buf.getActivity(), 1, false));
        recyclerView.setAdapter(this);
        this.notifyDataSetChanged();
        return this;
    }

    public StringRecyclerAdapter initRecyclerView(RecyclerView recyclerView, RecyclerView.LayoutManager layoutManager) {
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(this);
        this.notifyDataSetChanged();
        return this;
    }

    public void setObjects(List<O> objects) {
        this.objects = objects;
        this.notifyDataSetChanged();
    }

    @Override
    public S onCreateViewHolder(ViewGroup parent, int viewType) {
        try {
            return (S)this.vhClass.getConstructor(new Class[]{buf.getClass(), ViewGroup.class}).newInstance(new Object[]{buf, parent});
        } catch (InstantiationException var4) {
            var4.printStackTrace();
        } catch (IllegalAccessException var5) {
            var5.printStackTrace();
        } catch (InvocationTargetException var6) {
            var6.printStackTrace();
        } catch (NoSuchMethodException var7) {
            var7.printStackTrace();
        }

        return null;
    }

    @Override
    public void onBindViewHolder(S holder, int position) {
        holder.showData(objects.get(position));
    }

    @Override
    public int getItemCount() {
        return objects.size();
    }


}
