//package com.pasarwarga.market.adapter.realm;
//
//import android.support.annotation.Nullable;
//import android.view.ViewGroup;
//
//import com.alfarabi.base.fragment.BaseUserFragment;
//import com.alfarabi.base.model.chat.UserConversation;
//import com.pasarwarga.market.R;
//import com.pasarwarga.market.holder.recyclerview.chat.UserArchiveChatViewHolder;
//
//import io.realm.OrderedRealmCollection;
//import io.realm.RealmRecyclerViewAdapter;
//
///**
// * Created by Alfarabi on 10/6/17.
// */
//
//public class ChatArchiveListAdapter extends RealmRecyclerViewAdapter<UserConversation, UserArchiveChatViewHolder> {
//
//    BaseUserFragment baseUserFragment ;
//
//    public ChatArchiveListAdapter(BaseUserFragment baseUserFragment, @Nullable OrderedRealmCollection<UserConversation> data, boolean autoUpdate) {
//        super(data, autoUpdate);
//        this.baseUserFragment = baseUserFragment ;
//    }
//
//    @Override
//    public UserArchiveChatViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        UserArchiveChatViewHolder userChatViewHolder = new UserArchiveChatViewHolder(baseUserFragment, parent);
//        return userChatViewHolder;
//    }
//
//    @Override
//    public void onBindViewHolder(UserArchiveChatViewHolder holder, int position) {
//        holder.showData(getData().get(position));
//    }
//}
