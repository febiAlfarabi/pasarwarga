package com.pasarwarga.market.adapter.realm;

import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.ViewGroup;

import com.alfarabi.alfalibs.tools.Caster;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.alfarabi.base.fragment.BaseUserFragment;
import com.alfarabi.base.model.chat.Conversation;
import com.alfarabi.base.realm.RealmDao;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.holder.recyclerview.chat.ChatViewHolder;

import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;

/**
 * Created by Alfarabi on 10/6/17.
 */

public class ChatListAdapter extends RealmRecyclerViewAdapter<Conversation, ChatViewHolder> {
    
    public static final String TAG = ChatListAdapter.class.getName();

    private static final int MESSAGE = 0 ;
    private static final int MESSAGE_WITH_HEADER = 1 ;
    private int viewType ;
    private AlfaRecyclerView recyclerView ;
    BaseUserFragment baseUserFragment ;

    public ChatListAdapter(BaseUserFragment baseUserFragment, @Nullable OrderedRealmCollection<Conversation> data, boolean autoUpdate) {
        super(data, autoUpdate);
        this.baseUserFragment = baseUserFragment ;

    }

    public void init(AlfaRecyclerView recyclerView){
        this.recyclerView = recyclerView ;
        recyclerView.setLayoutManager(new LinearLayoutManager(baseUserFragment.activity()));
        recyclerView.setAdapter(this);
        if(getData().size()>0){
            recyclerView.postDelayed(() -> {
                recyclerView.smoothScrollToPosition(getData().size()-1);
                RealmDao.getRealm().executeTransaction(realm -> {
                    for (int i = 0; i < getData().size(); i++) {
                        getData().get(i).setHasBeenRead(true);
                    }
                });
                Caster.activity(baseUserFragment.activity(HomeActivity.class), HomeActivity.class).renderUnreadChatCount();
            }, 300);
        }
    }

    @Override
    public ChatViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ChatViewHolder chatViewHolder ;
        if(viewType==MESSAGE_WITH_HEADER){
            chatViewHolder = new ChatViewHolder(baseUserFragment, parent, true);
        }else{
            chatViewHolder = new ChatViewHolder(baseUserFragment, parent, false);
        }
        return chatViewHolder;
    }

    @Override
    public void onBindViewHolder(ChatViewHolder holder, int position) {
        holder.showData(getData().get(position));
    }

    @Override
    public int getItemViewType(int position) {
        if((position==0)){
            viewType = MESSAGE_WITH_HEADER ;
            return viewType ;
        }
        if(getItemCount()>0 && getItemCount()>position){
            if(!getData().get(position).getCreated().split(" ")[0].equals(getData().get(position-1).getCreated().split(" ")[0])){
                viewType = MESSAGE_WITH_HEADER ;
                return viewType ;
            }
        }
        viewType = MESSAGE ;
        return viewType ;
    }



}
