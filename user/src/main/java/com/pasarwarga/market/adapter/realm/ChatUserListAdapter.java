package com.pasarwarga.market.adapter.realm;

import android.support.annotation.Nullable;
import android.view.ViewGroup;

import com.alfarabi.base.fragment.BaseUserFragment;
import com.alfarabi.base.model.chat.UserConversation;
import com.pasarwarga.market.R;
import com.pasarwarga.market.holder.recyclerview.chat.UserChatViewHolder;

import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;

/**
 * Created by Alfarabi on 10/6/17.
 */

public class ChatUserListAdapter extends RealmRecyclerViewAdapter<UserConversation, UserChatViewHolder> {

    BaseUserFragment baseUserFragment ;

    public ChatUserListAdapter(BaseUserFragment baseUserFragment, @Nullable OrderedRealmCollection<UserConversation> data, boolean autoUpdate) {
        super(data, autoUpdate);
        this.baseUserFragment = baseUserFragment ;
    }

    @Override
    public UserChatViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        UserChatViewHolder userChatViewHolder = new UserChatViewHolder(baseUserFragment, parent);
        return userChatViewHolder;
    }

    @Override
    public void onBindViewHolder(UserChatViewHolder holder, int position) {
        holder.showData(getData().get(position));
    }
}
