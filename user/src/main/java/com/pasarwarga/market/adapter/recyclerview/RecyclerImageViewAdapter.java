package com.pasarwarga.market.adapter.recyclerview;

import android.support.v4.app.Fragment;

import com.alfarabi.alfalibs.adapters.recyclerview.SimpleRecyclerAdapter;
import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.SimpleViewHolder;
import com.alfarabi.alfalibs.fragments.interfaze.SimpleFragmentCallback;
import com.alfarabi.alfalibs.helper.model.ObjectAdapterInterface;

import java.util.List;

/**
 * Created by Alfarabi on 11/6/17.
 */

public abstract class RecyclerImageViewAdapter<OBJ extends Object & ObjectAdapterInterface, F extends Fragment & SimpleFragmentCallback, VH extends SimpleViewHolder> extends SimpleRecyclerAdapter<OBJ, F, VH> {

    public RecyclerImageViewAdapter(F fragment, Class<VH> vhClass, List<OBJ> objects) {
        super(fragment, vhClass, objects);
    }
}
