package com.pasarwarga.market.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.alfarabi.base.fragment.home.BaseDrawerFragment;
import com.alfarabi.base.tools.Initial;
import com.kazy.lx.LxWebView;
import com.pasarwarga.market.R;

import butterknife.BindView;

/**
 * Created by Alfarabi on 10/17/17.
 */

public class AboutFragment extends BaseDrawerFragment {
    
    public static final String TAG = AboutFragment.class.getName();

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout;
    @BindView(R.id.webview) LxWebView webView ;


    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public int getMenuId() {
        return R.id.about_menu_item;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    @Override
    public String initTitle() {
        title = getString(R.string.about);
        toolbar.setTitle(title);
        return title;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_about;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        webView.loadUrl(Initial.ABOUT_PASARWARGA_WEB_URL);
    }

    @Override
    public boolean keepSideMenu() {
        return true;
    }
}
