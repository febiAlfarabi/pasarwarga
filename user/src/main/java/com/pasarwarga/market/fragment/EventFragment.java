package com.pasarwarga.market.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.alfarabi.alfalibs.adapters.recyclerview.SimpleRecyclerAdapter;
import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.AppProgress;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.alfarabi.alfalibs.views.AlfaSwipeRefreshLayout;
import com.alfarabi.base.activity.SimpleBaseActivityInitiator;
import com.alfarabi.base.behaviour.PaginationScrollListener;
import com.alfarabi.base.fragment.home.BaseDrawerFragment;
import com.alfarabi.base.http.basemarket.service.LandingService;
import com.alfarabi.base.http.basemarket.service.SellerService;
import com.alfarabi.base.model.MediumBanner;
import com.alfarabi.base.model.Product;
import com.alfarabi.base.realm.RealmDao;
import com.alfarabi.base.tools.ResponseTools;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.hendraanggrian.bundler.annotations.BindExtra;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.holder.recyclerview.EventFragmentProductViewHolder;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class EventFragment extends BaseDrawerFragment {

    
    public static final String TAG = EventFragment.class.getName();

    @BindView(R.id.toolbar) Toolbar toolbar ;
    @BindView(R.id.collapsing_toolbar) CollapsingToolbarLayout collapsingToolbarLayout ;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout ;
    @BindView(R.id.swiperefreshlayout) SwipeRefreshLayout swipeRefreshLayout ;
    @BindView(R.id.recyclerview) AlfaRecyclerView recyclerView ;
    @BindView(R.id.banner_iv) ImageView bannerImageView ;
    private SimpleRecyclerAdapter<Product, EventFragment, EventFragmentProductViewHolder> simpleRecyclerAdapter ;

    @BindExtra MediumBanner mediumBanner ;

    int page = 1 ;

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public int getMenuId() {
        return 0;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    @Override
    public String initTitle() {
        return null;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_event;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        simpleRecyclerAdapter = new SimpleRecyclerAdapter<>(this, EventFragmentProductViewHolder.class, new ArrayList<>());
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        firstRequest();
        GlideApp.with(this).load(mediumBanner.getImageUrl()).placeholder(R.drawable.banner_pasarwarga).error(R.drawable.banner_pasarwarga)
                .skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL).into(bannerImageView);
        simpleRecyclerAdapter.initRecyclerView(recyclerView, new GridLayoutManager(activity(HomeActivity.class), 2));

        swipeRefreshLayout.setOnRefreshListener(() -> {
            firstRequest();
        });

        recyclerView.setOnScrollListener(new PaginationScrollListener((LinearLayoutManager) recyclerView.getLayoutManager()) {
            @Override
            protected void loadMoreItems() {
                nextRequest();
            }
        });
    }

    public void firstRequest(){
        swipeRefreshLayout.setRefreshing(true);
        HttpInstance.call(HttpInstance.create(LandingService.class).getEventProducts(
                mediumBanner.getEventId(), String.valueOf(page = 1))
                , eventResponse -> {
                    swipeRefreshLayout.setRefreshing(false);
                    ResponseTools.validate(activity(HomeActivity.class), eventResponse);
                    if(eventResponse.isStatus() && eventResponse.getProducts().size()>0){
                        simpleRecyclerAdapter.setObjects(eventResponse.getProducts());
                    }else{
                        activity(HomeActivity.class).showSnackbar(eventResponse.getMessage(), v -> {});
                    }
                }, throwable -> {
                    activity(HomeActivity.class).showSnackbar(throwable, v -> {});
                    simpleRecyclerAdapter.notifyDataSetChanged();
                    swipeRefreshLayout.setRefreshing(false);
                }, false);

    }

    public void nextRequest(){
        HttpInstance.call(HttpInstance.create(LandingService.class).getEventProducts(
                mediumBanner.getEventId(), String.valueOf(++page))
                , eventResponse -> {
                    swipeRefreshLayout.setRefreshing(false);
                    ResponseTools.validate(activity(HomeActivity.class), eventResponse);
                    if(eventResponse.isStatus() && eventResponse.getProducts().size()>0){
                        simpleRecyclerAdapter.appendObjects(eventResponse.getProducts());
                    }else{
                        activity(HomeActivity.class).showSnackbar(eventResponse.getMessage(), v -> {});
                    }
                }, throwable -> {
                    activity(HomeActivity.class).showSnackbar(throwable, v -> {});
                    simpleRecyclerAdapter.notifyDataSetChanged();
                    swipeRefreshLayout.setRefreshing(false);
                }, false);
    }

}
