//package com.pasarwarga.market.fragment;
//
//
//import android.net.Uri;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v4.app.Fragment;
//import android.support.v7.widget.Toolbar;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//
//import com.alfarabi.alfalibs.http.HttpInstance;
//import com.alfarabi.alfalibs.tools.WindowFlow;
//import com.alfarabi.base.fragment.BaseUserFragment;
//import com.alfarabi.base.http.basemarket.service.OrderService;
//import com.alfarabi.base.model.shop.OrderDetail;
//import com.alfarabi.base.model.shop.SellerDashboard;
//import com.alfarabi.base.tools.CommonUtil;
//import com.github.barteksc.pdfviewer.PDFView;
//import com.hendraanggrian.bundler.annotations.BindExtra;
//import com.pasarwarga.market.R;
//import com.pasarwarga.market.activity.home.HomeActivity;
//
//import java.io.File;
//
//import butterknife.BindView;
//
///**
// * A simple {@link Fragment} subclass.
// */
//public class PDFFragment extends BaseUserFragment<SellerDashboard> {
//
//    public static final String TAG = PDFFragment.class.getName();
//
//    @BindView(R.id.pdf_viewer) PDFView pdfView ;
//    @BindView(R.id.toolbar) Toolbar toolbar;
//
//    @BindExtra OrderDetail orderDetail ;
////    https://www.pasarwarga.com/api/shop/download_invoice/deal_code_number
//
//    @Override
//    public String initTitle() {
//        return null;
//    }
//
//    @Override
//    public int contentXmlLayout() {
//        return R.layout.fragment_pdf;
//    }
//
//    @Override
//    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//        toolbar.setTitle(getString(R.string.invoice));
//        toolbar.setNavigationOnClickListener(v -> WindowFlow.backstack(activity(HomeActivity.class)));
//        HttpInstance.withProgress(activity()).observe(HttpInstance.create(OrderService.class).downloadIncoice(orderDetail.getDealCodeNumber()),responseBody -> {
//            if(responseBody!=null){
//                File file = CommonUtil.writeResponseBodyToDisk(activity(), responseBody);
//
//                pdfView.fromUri(Uri.fromFile(file))
//                        .enableSwipe(true) // allows to block changing pages using swipe
//                        .swipeHorizontal(false)
//                        .enableDoubletap(true)
//                        .defaultPage(0)
////                        // allows to draw something on the current page, usually visible in the middle of the screen
////                        .onDraw(onDrawListener)
////                        // allows to draw something on all pages, separately for every page. Called only for visible pages
////                        .onDrawAll(onDrawListener)
////                        .onLoad(onLoadCompleteListener) // called after document is loaded and starts to be rendered
////                        .onPageChange(onPageChangeListener)
////                        .onPageScroll(onPageScrollListener)
////                        .onError(onErrorListener)
////                        .onRender(onRenderListener) // called after document is rendered for the first time
////                        // called on single tap, return true if handled, false to toggle scroll handle visibility
////                        .onTap(onTapListener)
//                        .enableAnnotationRendering(false) // render annotations (such as comments, colors or forms)
//                        .password(null)
//                        .scrollHandle(null)
//                        .enableAntialiasing(true) // improve rendering a little bit on low-res screens
//                        // spacing between pages in dp. To define spacing color, set view background
//                        .spacing(0)
//                        .load();
//            }else{
//                activity(HomeActivity.class).showDialog(getString(R.string.server_error), (dialog, which) -> {
//                    WindowFlow.backstack(activity(HomeActivity.class));
//                    dialog.dismiss();
//                });
//            }
//        }, throwable -> {
//            activity(HomeActivity.class).showSnackbar(throwable, v -> {});
//        });
//    }
//}
