package com.pasarwarga.market.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.method.PasswordTransformationMethod;
import android.text.method.SingleLineTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.activity.SimpleBaseActivityInitiator;
import com.alfarabi.base.callback.EditTextClickListener;
import com.alfarabi.base.http.basemarket.service.AdministrationService;
import com.alfarabi.base.model.MyProfile;
import com.alfarabi.base.tools.Initial;
import com.alfarabi.base.tools.InputTools;
import com.alfarabi.base.tools.ResponseTools;
import com.alfarabi.base.view.StringMaterialSpinner;
import com.pasarwarga.market.activity.publix.LoginActivity;
import com.pasarwarga.market.activity.publix.RegistrationActivity;
import com.alfarabi.base.fragment.registration.BaseRegistrationFormFragment;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.publix.VerificationActivity;
import com.pasarwarga.market.fragment.registration.UserTermAndConditonFragment;
import com.pasarwarga.market.tools.SystemApp;

import org.apache.commons.lang.StringUtils;

import java.util.HashMap;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegistrationForm1Fragment extends BaseRegistrationFormFragment {

    public static final String TAG = RegistrationForm1Fragment.class.getName();

    @BindView(R.id.firstname_et) EditText firstnameEditText;
    @BindView(R.id.code_area_sp) StringMaterialSpinner codeAreaSpinner ;
    @BindView(R.id.phonenumber_et) EditText phoneNumberEditText;
    @BindView(R.id.email_et) EditText emailEditText;
    @BindView(R.id.register_username_et) EditText usernameEditText;
    @BindView(R.id.register_password_et) EditText passwordEditText;
    @BindView(R.id.submit_btn) Button submitButton;
    @BindView(R.id.signin_tv) TextView signinTextView ;
    @BindView(R.id.agreement_layout) LinearLayout agreementLayout ;


    public static RegistrationForm1Fragment instance(){
        RegistrationForm1Fragment registrationForm1Fragment = new RegistrationForm1Fragment();
        return registrationForm1Fragment;
    }

    @Override
    public String getTAG() {
        return TAG ;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_registration_form;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        genderSpinner.initDataAdapter(new String[]{getString(R.string.male),getString(R.string.female)}, (parent, view1, position, id) -> {}, "");
        codeAreaSpinner.initDataAdapter(getResources().getStringArray(R.array.code_area_array), (parent, view1, position, id) -> {}, getResources().getStringArray(R.array.code_area_array)[0]);
//        usernameEditText.setFilters(new InputFilter[]{filter});
//        emailEditText.setFilters(new InputFilter[]{filter});
        passwordEditText.setOnTouchListener(new EditTextClickListener.RightDrawableClickListener(passwordEditText){
            boolean showMask = true ;
            @Override
            public boolean onDrawableClick() {
                if(showMask){
                    passwordEditText.setTransformationMethod(new SingleLineTransformationMethod());
                    showMask = false ;
                }else{
                    passwordEditText.setTransformationMethod(new PasswordTransformationMethod());
                    showMask = true ;
                }
                return false;
            }
        });

        submitButton.setOnClickListener(v -> {
            if(InputTools.isComplete(firstnameEditText,  phoneNumberEditText, emailEditText,
                    usernameEditText, passwordEditText) && InputTools.isValidEmail(emailEditText)){
                HashMap<String, String> hashMap = new HashMap<String, String>();
                hashMap.put(AdministrationService.EMAIL, emailEditText.getText().toString());
                hashMap.put(AdministrationService.PASSWORD, passwordEditText.getText().toString());
                String fullName[] = StringUtils.split(firstnameEditText.getText().toString(), " ", 2);
                if(fullName.length>1){
                    hashMap.put(AdministrationService.FIRST_NAME, fullName[0].trim());
                    hashMap.put(AdministrationService.LAST_NAME, fullName[1].trim());
                }else{
                    hashMap.put(AdministrationService.FIRST_NAME, firstnameEditText.getText().toString().trim());
                }
//                hashMap.put(AdministrationService.GENDER, genderSpinner.getSelectedDsi());
                hashMap.put(AdministrationService.GENDER, getString(R.string.male).toLowerCase());
                String phoneNumber = phoneNumberEditText.getText().toString();
                String codeArea = codeAreaSpinner.getSelectedDsi();
                if(codeArea.startsWith("+")){
                    codeArea = codeArea.replace("+", "");
                }
                if(phoneNumber.startsWith(codeArea)){
                    phoneNumber = phoneNumber.replace(codeArea, "0");
                }else{
                    if(!phoneNumber.startsWith("0")){
                        phoneNumber = "0"+phoneNumber;
                    }
                }
                hashMap.put(AdministrationService.PHONE, phoneNumber);
                hashMap.put(AdministrationService.USERNAME, usernameEditText.getText().toString());
//                WLog.i(TAG, "Password "+passwordEditText.getText().toString());
                HttpInstance.withProgress(getActivity()).observe(HttpInstance.create(AdministrationService.class).signup(hashMap),signup -> {

                    ResponseTools.validate(activity(), signup);
                    if(signup.isStatus() && signup.getUser()!=null){
                        MyProfile myProfile = new MyProfile();
                        myProfile.setUser(signup.getUser());
                        SystemApp.setProfile(myProfile);
                        HttpInstance.putHeaderMap(Initial.TOKEN, myProfile.getUser().getToken());
                        activity(RegistrationActivity.class).setResult(Activity.RESULT_OK);
                        WindowFlow.startActivity(getActivity(), VerificationActivity.class, true);
                    }else{
                        activity(SimpleBaseActivityInitiator.class).showDialog(signup.getMessage(), (dialog, which) -> {
                            dialog.dismiss();
                        });
                    }
                }, throwable -> {
                    activity(SimpleBaseActivityInitiator.class).showSnackbar(throwable,v1 -> {});
                });

                return;
            }

            invalid();
        });
        signinTextView.setOnClickListener(v -> {
            activity(RegistrationActivity.class).setResult(Activity.RESULT_OK);
            activity(RegistrationActivity.class).finish();
            WindowFlow.startActivity(activity(RegistrationActivity.class), LoginActivity.class, false);
        });

        agreementLayout.setOnClickListener(v -> WindowFlow.goTo(activity(RegistrationActivity.class), UserTermAndConditonFragment.TAG, R.id.fragment_frame, true, false));
    }

    public void invalid(){

    }

    @Override
    public Object getObject() {
        return null;
    }


//        InputFilter filter = new InputFilter() {
//        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
//            String filtered = "";
//            for (int i = start; i < end; i++) {
//                char character = source.charAt(i);
//                if (!Character.isWhitespace(character)) {
//                    filtered += character;
//                }
//            }
//
//            return filtered;
//        }
//
//    };





}
