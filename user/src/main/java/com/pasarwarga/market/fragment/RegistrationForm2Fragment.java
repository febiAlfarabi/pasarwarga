package com.pasarwarga.market.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import com.alfarabi.base.fragment.registration.BaseRegistrationFormFragment;
import com.pasarwarga.market.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegistrationForm2Fragment extends BaseRegistrationFormFragment {

    public static final String TAG = RegistrationForm2Fragment.class.getName();

//    @BindView(R2.id.next_btn) Button nextButton ;

    public static RegistrationForm2Fragment instance(){
        RegistrationForm2Fragment registrationForm1Fragment = new RegistrationForm2Fragment();
        return registrationForm1Fragment;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_registration_form;
    }

    @Override
    public String getTAG() {
        return TAG ;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        nextButton.setOnClickListener(v -> {
//            WindowFlow.goTo(getActivity(), RegistrationForm3Fragment.TAG, R.id.fragment_frame, true);
//        });
    }

    @Override
    public Object getObject() {
        return null;
    }
}
