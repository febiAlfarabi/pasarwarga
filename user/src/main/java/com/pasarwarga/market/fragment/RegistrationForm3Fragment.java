package com.pasarwarga.market.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import com.alfarabi.base.fragment.registration.BaseRegistrationFormFragment;
import com.pasarwarga.market.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegistrationForm3Fragment extends BaseRegistrationFormFragment {

    public static final String TAG = RegistrationForm3Fragment.class.getName();

//    @BindView(R2.id.next_btn) Button nextButton ;

    public static RegistrationForm3Fragment instance(){
        RegistrationForm3Fragment registrationForm3Fragment = new RegistrationForm3Fragment();
        return registrationForm3Fragment;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_registration_form;
    }

    @Override
    public String getTAG() {
        return TAG ;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        nextButton.setOnClickListener(v -> {
//            WindowFlow.startActivity(getActivity(), LoginActivity.class, true);
//        });
    }

    @Override
    public Object getObject() {
        return null;
    }
}
