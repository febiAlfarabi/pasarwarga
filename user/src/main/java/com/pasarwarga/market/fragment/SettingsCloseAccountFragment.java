package com.pasarwarga.market.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.fragment.home.BaseDrawerFragment;
import com.alfarabi.base.http.basemarket.service.SettingService;
import com.alfarabi.base.tools.Initial;
import com.alfarabi.base.tools.ResponseTools;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.SplashActivity;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.tools.SystemApp;

import butterknife.BindView;

/**
 * Created by yudapratama on 8/15/2017.
 */

public class SettingsCloseAccountFragment extends BaseDrawerFragment {

    public static final String TAG = SettingsCloseAccountFragment.class.getName();

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.appbar_layout_setting_close_account) AppBarLayout appBarLayout;
    @BindView(R.id.close_button) Button closeAccountButton;

    @Override
    public String initTitle() {
        title = getString(R.string.close_account_setting);
        toolbar.setTitle(title);
        return title;
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        closeAccountButton.setOnClickListener(v -> {
            activity(HomeActivity.class).showDialog(getString(R.string.confirmation_close_account), (dialog, which) -> {
                HttpInstance.withProgress(activity()).observe(HttpInstance.create(SettingService.class).deactiveAccount(), deactiveAccount -> {
                    ResponseTools.validate(activity(), deactiveAccount);
                    if(deactiveAccount.isStatus()) {
                        HttpInstance.getHeaderMap().remove(Initial.TOKEN);
                        SystemApp.deleteProfile();
                        WindowFlow.restart(activity(), SplashActivity.class);
                    }else{
                        activity(HomeActivity.class).showDialog(deactiveAccount.getMessage(), (dialog1, which1) -> {});
                    }
                }, throwable -> {
                    activity(HomeActivity.class).showDialog(throwable, (dialog1, which1) -> {});
                });
                dialog.dismiss();
            }, (dialog, which) -> {
                dialog.dismiss();
            });
        });
    }

    @Override
    public int getMenuId() {
        return R.id.setting_menu_item;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_settings_close_account;
    }
}
