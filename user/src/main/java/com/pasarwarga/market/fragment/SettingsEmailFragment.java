package com.pasarwarga.market.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.fragment.home.BaseDrawerFragment;
import com.alfarabi.base.http.basemarket.service.SettingService;
import com.alfarabi.base.tools.InputTools;
import com.alfarabi.base.tools.ResponseTools;
import com.pasarwarga.market.R;

import java.util.HashMap;

import butterknife.BindView;

/**
 * Created by yudapratama on 8/15/2017.
 */

public class SettingsEmailFragment extends BaseDrawerFragment {

    public static final String TAG = SettingsEmailFragment.class.getName();

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.appbar_layout_setting_email) AppBarLayout appBarLayout;
    @BindView(R.id.update_button) Button updateButton;
    @BindView(R.id.current_email_et) EditText currentEmailEditText;
    @BindView(R.id.password_et) EditText passwordEditText;
    @BindView(R.id.new_email_et) EditText newEmailEditText;

    @Override
    public String initTitle() {
        title = getString(R.string.email_setting);
        toolbar.setTitle(title);
        return title;
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (myProfile != null) {
            updateButton.setOnClickListener(v -> {
                if (InputTools.isComplete(currentEmailEditText, passwordEditText, newEmailEditText)) {
                    HashMap<String, String> hashMap = new HashMap<String, String>();
                    hashMap.put(SettingService.EMAIL, currentEmailEditText.getText().toString());
                    hashMap.put(SettingService.PASSWORD, passwordEditText.getText().toString());
                    hashMap.put(SettingService.NEW_EMAIL, newEmailEditText.getText().toString());

                    HttpInstance.withProgress(activity()).observe(HttpInstance.create(SettingService.class).changeEmail(hashMap), changeEmailResponse -> {
                        ResponseTools.validate(activity(), changeEmailResponse);
                        Toast.makeText(getContext(), R.string.email_changed, Toast.LENGTH_SHORT).show();
                        WindowFlow.goTo(getActivity(), SettingsProfileFragment.TAG, R.id.fragment_frame, true, false);
                    },throwable -> {
                    });
                }
            });
        }
    }

    @Override
    public int getMenuId() {
        return R.id.setting_menu_item;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_settings_email;
    }
}
