package com.pasarwarga.market.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.fragment.home.BaseDrawerFragment;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.fragment.store.AddressListFragment;

import butterknife.BindView;

/**
 * Created by yudapratama on 8/14/2017.
 */

public class SettingsFragment extends BaseDrawerFragment {

    public static final String TAG = SettingsFragment.class.getName();

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.appbar_layout_settings) AppBarLayout appBarLayout;
    @BindView(R.id.linearlayout_profile) LinearLayout linearLayoutProfile;
    @BindView(R.id.linearlayout_email) LinearLayout linearLayoutEmail;
    @BindView(R.id.linearlayout_password) LinearLayout linearLayoutPassword;
    //@BindView(R.id.linearlayout_close_account) LinearLayout linearLayoutCloseAccount;

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        toolbar.setOnMenuItemClickListener(this);
        linearLayoutProfile.setOnClickListener(v -> {
            WindowFlow.withAnim(R.anim.enter_from_right, R.anim.exit_to_left).goTo(activity(HomeActivity.class), SettingsProfileFragment.TAG, R.id.fragment_frame, true, false);
        });

        linearLayoutEmail.setOnClickListener(v -> {
            WindowFlow.withAnim(R.anim.enter_from_right, R.anim.exit_to_left).goTo(activity(HomeActivity.class), AddressListFragment.TAG, R.id.fragment_frame, true, false);
        });

        linearLayoutPassword.setOnClickListener(v -> {
            WindowFlow.withAnim(R.anim.enter_from_right, R.anim.exit_to_left).goTo(activity(HomeActivity.class), SettingsPasswordFragment.TAG, R.id.fragment_frame, true, false);
        });

//        linearLayoutCloseAccount.setOnClickListener(v -> {
//            WindowFlow.withAnim(R.anim.enter_from_right, R.anim.exit_to_left).goTo(activity(HomeActivity.class), SettingsCloseAccountFragment.TAG, R.id.fragment_frame, true, false);
//        });
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_settings;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public String initTitle() {
        title = getString(R.string.profile_setting);
        toolbar.setTitle(title);
        return title;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    @Override
    public int getMenuId() {
//        return R.id.setting_menu_item;
        return 0;
    }

    @Override
    public boolean keepSideMenu() {
        return false;
    }
}
