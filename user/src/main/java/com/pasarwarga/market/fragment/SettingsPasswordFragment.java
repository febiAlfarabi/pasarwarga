package com.pasarwarga.market.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.fragment.home.BaseDrawerFragment;
import com.alfarabi.base.http.basemarket.service.SettingService;
import com.alfarabi.base.tools.Initial;
import com.alfarabi.base.tools.InputTools;
import com.alfarabi.base.tools.ResponseTools;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.SplashActivity;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.tools.SystemApp;

import java.util.HashMap;

import butterknife.BindView;

/**
 * Created by yudapratama on 8/15/2017.
 */

public class SettingsPasswordFragment extends BaseDrawerFragment {

    public static final String TAG = SettingsPasswordFragment.class.getName();

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.appbar_layout_setting_password) AppBarLayout appBarLayout;
    @BindView(R.id.update_button) Button updateButton;
    @BindView(R.id.email_or_username_et) EditText emailOrUsernameEditText;
    @BindView(R.id.new_password_et) EditText newPasswordEditText;
    @BindView(R.id.confirm_new_password_et) EditText confirmNewPasswordEditText;

    @Override
    public String initTitle() {
        title = getString(R.string.password_setting);
        toolbar.setTitle(title);
        return title;
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (myProfile != null) {
            updateButton.setOnClickListener(v -> {
                if (InputTools.isComplete(emailOrUsernameEditText, newPasswordEditText, confirmNewPasswordEditText) && InputTools.equals(newPasswordEditText, confirmNewPasswordEditText)) {
                    HashMap<String, String> hashMap = new HashMap<String, String>();
                    hashMap.put(SettingService.EMAIL_OR_USERNAME, emailOrUsernameEditText.getText().toString());
                    hashMap.put(SettingService.NEW_PASSWORD, newPasswordEditText.getText().toString());

                    HttpInstance.withProgress(activity()).observe(HttpInstance.create(SettingService.class).changePassword(hashMap), changePassword -> {
                        ResponseTools.validate(activity(), changePassword);
                        if(changePassword.isStatus()) {
                            activity(HomeActivity.class).showDialog(getString(R.string.password_changed), (dialog, which) -> {
                                HttpInstance.getHeaderMap().remove(Initial.TOKEN);
                                SystemApp.deleteProfile();
                                WindowFlow.restart(activity(), SplashActivity.class);
                            });
                        }else{
                            activity(HomeActivity.class).showDialog(changePassword.getMessage(), (dialog, which) -> dialog.dismiss());
                        }
                    }, throwable -> activity(HomeActivity.class).showSnackbar(throwable, v2 -> {}));
                }
            });
        }
    }

    @Override
    public int getMenuId() {
        return R.id.setting_menu_item;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_settings_password;
    }
}
