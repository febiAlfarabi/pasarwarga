package com.pasarwarga.market.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.fragment.home.BaseDrawerFragment;
import com.alfarabi.base.http.basemarket.service.SettingService;
import com.alfarabi.base.model.MyProfile;
import com.alfarabi.base.tools.InputTools;
import com.alfarabi.base.tools.ResponseTools;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.activity.publix.VerificationActivity;
import com.pasarwarga.market.tools.SystemApp;

import java.util.HashMap;

import butterknife.BindView;

/**
 * Created by USER on 9/14/2017.
 */

public class SettingsPhoneNumberFragment extends BaseDrawerFragment {

    public static final String TAG = SettingsPhoneNumberFragment.class.getName();

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout;
    @BindView(R.id.next_button) Button nextButton;
    @BindView(R.id.current_phonenumber_et) EditText currentPhoneNumberEditText;
    @BindView(R.id.password_et) EditText passwordEditText;
    @BindView(R.id.new_phonenumber_et) EditText newPhoneNumberEditText;

    @Override
    public String initTitle() {
        title = getString(R.string.phone_setting);
        toolbar.setTitle(title);
        return title;
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (myProfile != null) {
            nextButton.setOnClickListener(v -> {
                if (InputTools.isComplete(currentPhoneNumberEditText, passwordEditText, newPhoneNumberEditText)) {
                    HashMap<String, String> hashMap = new HashMap<String, String>();
                    hashMap.put(SettingService.PHONE, currentPhoneNumberEditText.getText().toString());
                    hashMap.put(SettingService.PASSWORD, passwordEditText.getText().toString());
                    hashMap.put(SettingService.NEW_PHONE, newPhoneNumberEditText.getText().toString());

                    HttpInstance.withProgress(activity()).observe(HttpInstance.create(SettingService.class).changePhone(hashMap), changePhoneResponse -> {
                        ResponseTools.validate(activity(), changePhoneResponse);
                        if (changePhoneResponse.isStatus()) {
                            MyProfile myProfile2 = SystemApp.myProfile();
                            myProfile2.getUser().setPhone(changePhoneResponse.getUser().getPhone());
                            SystemApp.setProfile(myProfile2);
                            ResponseTools.validate(activity(), changePhoneResponse);
                            Toast.makeText(getContext(), R.string.phone_number_verification4, Toast.LENGTH_SHORT).show();
                            WindowFlow.startActivity(activity(HomeActivity.class), VerificationActivity.class, false);
//                            activity(HomeActivity.class).showDialog(getString(R.string.phone_changed), (dialog, which) -> {
//                                HttpInstance.getHeaderMap().remove(Initial.TOKEN);
//                                SystemApp.deleteProfile();
//                                WindowFlow.restart(activity(), SplashActivity.class);
//                            });
                        }
                    }, throwable -> {
                    });
                }
            });
        }
    }

    @Override
    public int getMenuId() {
        return 0;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_settings_phone_number;
    }
}
