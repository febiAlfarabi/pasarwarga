package com.pasarwarga.market.fragment;

import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;

import com.alfarabi.base.fragment.home.BaseDrawerFragment;
import com.pasarwarga.market.R;

import butterknife.BindView;

/**
 * Created by USER on 10/9/2017.
 */

public class SettingsPhoneNumberVerificationFragment extends BaseDrawerFragment{

    public static final String TAG = SettingsPhoneNumberVerificationFragment.class.getName();

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout;
    @BindView(R.id.verification_code_et) EditText verifyCodeEditText;
    @BindView(R.id.verify_btn) Button verifyButton;

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public int getMenuId() {
        return 0;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    @Override
    public String initTitle() {
        title = getString(R.string.phone_setting);
        toolbar.setTitle(title);
        return title;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_settings_phone_number_verification;
    }
}
