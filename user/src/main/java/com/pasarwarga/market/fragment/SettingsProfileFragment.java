package com.pasarwarga.market.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.ArrayCalendarSpinner;
import com.alfarabi.alfalibs.tools.CommonUtil;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.alfalibs.tools.WLog;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.fragment.home.BaseDrawerFragment;
import com.alfarabi.base.http.basemarket.service.SettingService;
import com.alfarabi.base.intent.CaptureImageIntent;
import com.alfarabi.base.intent.GetContentIntent;
import com.alfarabi.base.model.MyProfile;
import com.alfarabi.base.tools.ResponseTools;
import com.alfarabi.base.view.BottomSheetLayout;
import com.alfarabi.base.view.StringMaterialSpinner;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.kishan.askpermission.AskPermission;
import com.kishan.askpermission.PermissionCallback;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.activity.publix.VerificationActivity;
import com.pasarwarga.market.fragment.asseller.SellerDashboardFragment;
import com.pasarwarga.market.tools.SystemApp;
import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import butterknife.BindView;
import cn.nekocode.rxactivityresult.RxActivityResult;
import okhttp3.MultipartBody;

/**
 * Created by yudapratama on 8/15/2017.
 */

public class SettingsProfileFragment extends BaseDrawerFragment {

    public static final String TAG = SettingsProfileFragment.class.getName();

    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_IMAGE_DATA = 2;

    public MultipartBody.Part imagePartProfile;
    public String userGender;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout;
    @BindView(R.id.fragment_setting_profile_bottom_sheet_layout) BottomSheetLayout bottomSheetLayout;
    @BindView(R.id.profile_firstname_et) EditText profileFirstNameEditText;
    @BindView(R.id.profile_lastname_et) EditText profileLastNameEditText;
    @BindView(R.id.date_sp) StringMaterialSpinner dateSpinner;
    @BindView(R.id.month_sp) StringMaterialSpinner monthSpinner;
    @BindView(R.id.year_sp) StringMaterialSpinner yearSpinner;
    @BindView(R.id.profile_gender_radiogroup) RadioGroup genderRadioGroup;
    @BindView(R.id.profile_gender_male_rb) RadioButton maleRadioButton;
    @BindView(R.id.profile_gender_female_rb) RadioButton femaleRadioButton;
    @BindView(R.id.profile_email_et) EditText profileEmailEditText;
    @BindView(R.id.profile_phone_et) EditText profilePhoneEditText;
    @BindView(R.id.profile_change_email_bt) Button changeEmailButton;
    @BindView(R.id.profile_change_number_bt) Button changeNumberButton;
    @BindView(R.id.profile_user_iv) ImageView profileImageView;
    @BindView(R.id.profile_picker_ib) ImageButton imageButtonPicker;
    @BindView(R.id.save_button) Button saveButton;
    private Uri imageUri;

    @Override
    public String initTitle() {
        title = getString(R.string.my_profile);
        toolbar.setTitle(title);
        return title;
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Calendar mCalendar = Calendar.getInstance();
        initYearSpinner(mCalendar.get(Calendar.YEAR) - 110, mCalendar.get(Calendar.YEAR) - 21, String.valueOf(mCalendar.get(Calendar.YEAR) - 21));
        initMonthSpinner(0, mCalendar.get(Calendar.MONTH), String.valueOf(mCalendar.get(Calendar.MONTH)));
        initDateSpinner(0, mCalendar.get(Calendar.DAY_OF_MONTH), String.valueOf(mCalendar.get(Calendar.DAY_OF_MONTH)));

        showData();
        saveProfileInfo();

        changeEmailButton.setOnClickListener(v -> {
            WindowFlow.withAnim(R.anim.enter_from_right, R.anim.exit_to_left).goTo(getActivity(), SettingsEmailFragment.TAG, R.id.fragment_frame, true, false);
        });
        changeNumberButton.setOnClickListener(v -> {
            WindowFlow.withAnim(R.anim.enter_from_right, R.anim.exit_to_left).goTo(getActivity(), SettingsPhoneNumberFragment.TAG, R.id.fragment_frame, true, false);
        });
        imageButtonPicker.setOnClickListener(v -> {
            openImagePicker();
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        Dispatcher.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Crop.REQUEST_CROP && resultCode == Activity.RESULT_OK) {
            Uri uriProfile = imageUri;
            GlideApp.with(this).load(uriProfile).error(R.drawable.ic_pasarwarga).placeholder(R.drawable.ic_pasarwarga)
                    .skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL).into(profileImageView);
            imagePartProfile = CommonUtil.generateMultiPart(getContext(), "photo", imageUri);
        }

        if (SystemApp.checkLogin(activity(HomeActivity.class), true)) {
            HttpInstance.withProgress(activity()).observe(HttpInstance.create(SettingService.class).changeProfilePicture(imagePartProfile), changeProfilePictureResponse -> {
                ResponseTools.validate(activity(), changeProfilePictureResponse);
                if (changeProfilePictureResponse.isStatus()) {
                    MyProfile myProfile2 = SystemApp.myProfile();
                    myProfile2.getUser().setPhoto(changeProfilePictureResponse.getUser().getPhoto());
                    SystemApp.setProfile(myProfile2);
                    ResponseTools.validate(activity(), changeProfilePictureResponse);
                    activity(HomeActivity.class).showDialog(changeProfilePictureResponse.getMessage(), (dialog, which) -> dialog.dismiss());
                } else {
                    activity(HomeActivity.class).showDialog(changeProfilePictureResponse.getMessage(), (dialog, which) -> dialog.dismiss());
                }
            }, throwable -> activity(HomeActivity.class).showSnackbar(throwable, v -> {
            }));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        Dispatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public int getMenuId() {
        return R.id.setting_menu_item;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_settings_profile;
    }

    public void showData() {
        if (myProfile != null) {
            profileFirstNameEditText.setText(myProfile.getUser().getFirstName());
            profileLastNameEditText.setText(myProfile.getUser().getLastName());
            profileEmailEditText.setText(myProfile.getUser().getEmail());
            profilePhoneEditText.setText(myProfile.getUser().getPhone());
            GlideApp.with(this).load(myProfile.getUser().getPhoto()).error(R.drawable.ic_pasarwarga).placeholder(R.drawable.ic_pasarwarga)
                    .skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL).into(profileImageView);

            dateSpinner.setSelectedDsi(myProfile.getUser().getBirthday().substring(8, 10));
            monthSpinner.setSelectedDsi(myProfile.getUser().getBirthday().substring(5, 7));
            yearSpinner.setSelectedDsi(myProfile.getUser().getBirthday().substring(0, 4));
            WLog.i(TAG, "BIRTHDAY");
            WLog.i(TAG, new Gson().toJson(myProfile.getUser().getBirthday()));

            userGender = myProfile.getUser().getGender();

            if (userGender.equals(getString(R.string.male))) {
                maleRadioButton.setChecked(true);
                femaleRadioButton.setChecked(false);
            } else if (userGender.equals(getString(R.string.female))) {
                maleRadioButton.setChecked(false);
                femaleRadioButton.setChecked(true);
            } else {
                maleRadioButton.setChecked(false);
                femaleRadioButton.setChecked(false);
            }

            HttpInstance.withProgress(activity()).observe(HttpInstance.create(SettingService.class).getCurrentProfilePicture(), getProfilePictureResponse -> {
                ResponseTools.validate(activity(), getProfilePictureResponse);
                if (getProfilePictureResponse.isStatus()) {
                    profileFirstNameEditText.setText(getProfilePictureResponse.getUser().getFirstName());
                    profileLastNameEditText.setText(getProfilePictureResponse.getUser().getLastName());
                    GlideApp.with(this).load(getProfilePictureResponse.getUser().getThumbnail()).error(R.drawable.ic_pasarwarga).placeholder(R.drawable.ic_pasarwarga)
                            .skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL).into(profileImageView);
                }
            }, throwable -> activity(HomeActivity.class).showSnackbar(throwable, v -> {
            }));
        }
    }

    public void openImagePicker() {
        bottomSheetLayout.showImagePicker(this, () -> {
                    new AskPermission.Builder(this).setPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE).setCallback(new PermissionCallback() {
                        @Override
                        public void onPermissionsGranted(int requestCode) {
                            RxActivityResult.startActivityForResult(activity(), CaptureImageIntent.getInstance(getContext()), 1).subscribe(activityResult -> {
                                if(activityResult.isOk() && activityResult.getRequestCode()==1){
                                    try {
                                        imageUri = Uri.fromFile(new File(getContext().getCacheDir(), "image_" + System.currentTimeMillis() + ".jpg"));
                                        Crop.of(CaptureImageIntent.getInstance(activity()).getResult(activityResult.getData()), imageUri).asSquare().start(getContext(), SettingsProfileFragment.this, Crop.REQUEST_CROP);
                                    }catch (Exception e){
                                        e.printStackTrace();
                                        new MaterialDialog.Builder(getContext()).title(R.string.confirmation).content(e.getMessage())
                                                .positiveText(R.string.report).negativeText(R.string.cancel).show();
                                    }
                                }
                            }, throwable -> {
                                    throwable.printStackTrace();
                                    new MaterialDialog.Builder(getContext()).title(R.string.confirmation).content(throwable.getMessage())
                                            .positiveText(R.string.report).negativeText(R.string.cancel).show();
                            });

//                            RxActivityResult.getInstance(getActivity()).from(getActivity()).startActivityForResult(CaptureImageIntent.getInstance(getContext()), 1)
//                                    .subscribe(activityResult -> {
//                                        if(activityResult.isOk() && activityResult.getData()!=null && activityResult.getRequestCode()==1){
//                                            try {
//                                                imageUri = Uri.fromFile(new File(getContext().getCacheDir(), "image_" + System.currentTimeMillis() + ".jpg"));
//                                                Crop.of(CaptureImageIntent.getInstance(getContext()).getResult(activityResult.getData()), imageUri).asSquare().start(getContext(), SettingsProfileFragment.this, Crop.REQUEST_CROP);
//                                            }catch (Exception e){
//                                                e.printStackTrace();
//                                                new MaterialDialog.Builder(getContext()).title(R.string.confirmation).content(e.getMessage())
//                                                        .positiveText(R.string.report).negativeText(R.string.cancel).show();
//                                            }
//                                        }
//                                    }, throwable -> {
//                                        throwable.printStackTrace();
//                                        new MaterialDialog.Builder(getContext()).title(R.string.confirmation).content(throwable.getMessage())
//                                                .positiveText(R.string.report).negativeText(R.string.cancel).show();
//                                    });
                        }

                        @Override
                        public void onPermissionsDenied(int requestCode) {
                            new MaterialDialog.Builder(getContext())
                                    .title(R.string.permission_required)
                                    .content(R.string.camera_permission)
                                    .positiveText(android.R.string.ok)
                                    .onPositive((dialog, which) -> dialog.dismiss())
                                    .show();
                        }
                    }).request(123456);
                }, () -> {
                    new AskPermission.Builder(this).setPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE).setCallback(new PermissionCallback() {
                        @Override
                        public void onPermissionsGranted(int requestCode) {
                            RxActivityResult.startActivityForResult(activity(), new GetContentIntent("image/*"), 2).subscribe(activityResult -> {
                                imageUri = Uri.fromFile(new File(getContext().getCacheDir(), "image_" + System.currentTimeMillis() + ".jpg"));
                                if(activityResult.getData()!=null) {
                                    Crop.of(GetContentIntent.extract(activityResult.getData())[0], imageUri)
                                            .asSquare()
                                            .start(getContext(), SettingsProfileFragment.this, Crop.REQUEST_CROP);
                                }
                            }, throwable -> {
                                throwable.printStackTrace();
                                new MaterialDialog.Builder(getContext()).title(R.string.confirmation).content(throwable.getMessage())
                                        .positiveText(R.string.report).negativeText(R.string.cancel).show();
                            });
                        }

                        @Override
                        public void onPermissionsDenied(int requestCode) {
                            new MaterialDialog.Builder(getContext())
                                    .title(R.string.permission_required)
                                    .content(R.string.camera_permission)
                                    .positiveText(android.R.string.ok)
                                    .onPositive((dialog, which) -> dialog.dismiss())
                                    .show();
                        }
                    }).request(123456);
                },
                uri -> {
                    imageUri = Uri.fromFile(new File(getContext().getCacheDir(), "image_" + System.currentTimeMillis() + ".jpg"));
                    Crop.of(uri, imageUri).asSquare().start(getContext(), this, Crop.REQUEST_CROP);
                });
    }

//    public void openImagePicker() {
//        bottomSheetLayout.showImagePicker(this, () -> {
//                    new AskPermission.Builder(this)
//                            .setPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                            .setCallback(new PermissionCallback() {
//                                @Override
//                                public void onPermissionsGranted(int requestCode) {
//                                    RxActivityResult.startActivityForResult(getActivity(), CaptureImageIntent.getInstance(getContext()), REQUEST_IMAGE_CAPTURE).subscribe(activityResult -> {
//                                        if (requestCode == REQUEST_IMAGE_CAPTURE) {
//                                            imageUri = Uri.fromFile(new File(getContext().getCacheDir(), "image_" + System.currentTimeMillis() + ".jpg"));
//                                            Crop.of(CaptureImageIntent.getInstance(getContext()).getResult(), imageUri).asSquare().start(getContext(), SettingsProfileFragment.this, Crop.REQUEST_CROP);
//                                            Uri uriProfile = imageUri;
//                                            GlideApp.with(getContext()).load(uriProfile).error(R.drawable.ic_pasarwarga).placeholder(R.drawable.ic_pasarwarga).into(profileImageView);
//                                            imagePartProfile = CommonUtil.generateMultiPart(getContext(), "photo", imageUri);
//                                        }
//                                    });
//                                }
//
//                                @Override
//                                public void onPermissionsDenied(int requestCode) {
//                                }
//                            }).setErrorCallback(new ErrorCallback() {
//                        @Override
//                        public void onShowRationalDialog(PermissionInterface permissionInterface, int requestCode) {
//                        }
//
//                        @Override
//                        public void onShowSettings(PermissionInterface permissionInterface, int requestCode) {
//                        }
//                    }).request(REQUEST_IMAGE_CAPTURE);
//                }, () -> {
//                    RxActivityResult.startActivityForResult(getActivity(), new GetContentIntent("image/*"), REQUEST_IMAGE_DATA).subscribe(data -> {
//                        if (data.getRequestCode() == REQUEST_IMAGE_DATA && data.isOk()) {
//                            imageUri = Uri.fromFile(new File(getContext().getCacheDir(), "image_" + System.currentTimeMillis() + ".jpg"));
////                            Crop.of(GetContentIntent.extract(data)[0], imageUri)
////                                    .asSquare()
////                                    .start(getContext(), this, Crop.REQUEST_CROP);
//                            Uri uriProfile = imageUri;
//                            GlideApp.with(getContext()).load(uriProfile).error(R.drawable.ic_pasarwarga).placeholder(R.drawable.ic_pasarwarga).into(profileImageView);
//                            imagePartProfile = CommonUtil.generateMultiPart(getContext(), "photo", imageUri);
//                        }
//                    });
//                },
//                uri -> {
//                    imageUri = Uri.fromFile(new File(getContext().getCacheDir(), "image_" + System.currentTimeMillis() + ".jpg"));
//                    Crop.of(uri, imageUri).asSquare().start(getContext(), this, Crop.REQUEST_CROP);
//                    Uri uriProfile = imageUri;
//                    GlideApp.with(getContext()).load(uriProfile).error(R.drawable.ic_pasarwarga).placeholder(R.drawable.ic_pasarwarga).into(profileImageView);
//                    imagePartProfile = CommonUtil.generateMultiPart(getContext(), "photo", imageUri);
//                });
//    }

    public void saveProfileInfo() {
        saveButton.setOnClickListener(v -> {
//                HashMap<String, String> hashMap = new HashMap<String, String>();
//                hashMap.put(SettingService.FIRST_NAME, profileFirstNameEditText.getText().toString());
//                hashMap.put(SettingService.LAST_NAME, profileLastNameEditText.getText().toString());
//                HttpInstance.withProgress(activity()).observe(HttpInstance.create(SettingService.class).changeProfileName(hashMap), changeProfileName -> {
//                    ResponseTools.validate(activity(), changeProfileName);
//                    if(changeProfileName.isStatus()){
//                        MyProfile myProfile2 = SystemApp.myProfile();
//                        myProfile2.getUser().setFirstName(changeProfileName.getUser().getFirstName());
//                        myProfile2.getUser().setLastName(changeProfileName.getUser().getLastName());
//                        SystemApp.setProfile(myProfile2);
//                        ResponseTools.validate(activity(), changeProfileName);
//                        Toast.makeText(getContext(), R.string.profile_name_updated, Toast.LENGTH_SHORT).show();
//                    }else{
//                        activity(HomeActivity.class).showDialog(changeProfileName.getMessage(), (dialog, which) -> dialog.dismiss());
//                    }
//                }, throwable -> activity(HomeActivity.class).showDialog(throwable, (dialog, which) -> {}));
            if (myProfile != null) {
                HashMap<String, String> hashMap1 = new HashMap<String, String>();
                hashMap1.put(SettingService.COUNTRY_ID, myProfile.getUser().getCountryId());
                hashMap1.put(SettingService.STATE_ID, myProfile.getUser().getStateId());
                hashMap1.put(SettingService.CITY_ID, myProfile.getUser().getCityId());
                hashMap1.put(SettingService.KECAMATAN_ID, myProfile.getUser().getDistrictId());
                hashMap1.put(SettingService.KELURAHAN_ID, myProfile.getUser().getSubdistrictId());
                hashMap1.put(SettingService.ZIPCODE, myProfile.getUser().getZipCode());

                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, Integer.valueOf(yearSpinner.getSelectedDsi()));
                calendar.set(Calendar.MONTH, ArrayCalendarSpinner.getMonth(monthSpinner.getSelectedDsi()));
                calendar.set(Calendar.DAY_OF_MONTH, Integer.valueOf(dateSpinner.getSelectedDsi()));

                Date date = calendar.getTime();
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                String formatted = simpleDateFormat.format(date);
                hashMap1.put(SettingService.BIRTHDAY, formatted);
                WLog.i(TAG, new Gson().toJson(formatted));

                if (maleRadioButton.isChecked()) {
                    hashMap1.put(SettingService.GENDER, getString(R.string.male));
                } else if (femaleRadioButton.isChecked()) {
                    hashMap1.put(SettingService.GENDER, getString(R.string.female));
                }

                hashMap1.put(SettingService.ABOUT, myProfile.getUser().getAbout());
                HttpInstance.withProgress(activity()).observe(HttpInstance.create(SettingService.class).changeProfileInfo(hashMap1), changeProfileInfo -> {
                    ResponseTools.validate(activity(), changeProfileInfo);
                    if (changeProfileInfo.isStatus()) {
                        MyProfile myProfileInfo = SystemApp.myProfile();
                        myProfileInfo.getUser().setCountryId(changeProfileInfo.getUser().getCountryId());
                        myProfileInfo.getUser().setStateId(changeProfileInfo.getUser().getStateId());
                        myProfileInfo.getUser().setCityId(changeProfileInfo.getUser().getCityId());
                        myProfileInfo.getUser().setDistrictId(changeProfileInfo.getUser().getDistrictId());
                        myProfileInfo.getUser().setSubdistrictId(changeProfileInfo.getUser().getSubdistrictId());
                        myProfileInfo.getUser().setZipCode(changeProfileInfo.getUser().getZipCode());
                        myProfileInfo.getUser().setBirthday(changeProfileInfo.getUser().getBirthday());
                        myProfileInfo.getUser().setGender(changeProfileInfo.getUser().getGender());
                        myProfileInfo.getUser().setAbout(changeProfileInfo.getUser().getAbout());
                        SystemApp.setProfile(myProfileInfo);
                        ResponseTools.validate(activity(), changeProfileInfo);
                        Toast.makeText(getContext(), R.string.profile_updated, Toast.LENGTH_SHORT).show();
                        //WindowFlow.startActivity(activity(HomeActivity.class), SettingsFragment.class, false);
                    } else {
                        activity(HomeActivity.class).showDialog(changeProfileInfo.getMessage(), (dialog, which) -> dialog.dismiss());
                    }
                }, throwable -> activity(HomeActivity.class).showDialog(throwable, (dialog, which) -> {
                }));
            }
        });
    }

    public void initDateSpinner(int min, int max, String def) {
        ArrayCalendarSpinner.initDateSpinner(min, max, def);
        dateSpinner.initDataAdapter(ArrayCalendarSpinner.getAllDays(), (parent, view1, position, id) -> {

        }, def);

    }

    public void initMonthSpinner(int min, int max, String def) {
        ArrayCalendarSpinner.initMonthSpinner(min, max, def);
        monthSpinner.initDataAdapter(ArrayCalendarSpinner.getAllMonths(), (parent, view1, position, id) -> {
            if (!monthSpinner.getSelectedDsi().equalsIgnoreCase(DateFormatSymbols.getInstance().getMonths()[max])) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.MONTH, ArrayCalendarSpinner.getMonth(monthSpinner.getSelectedDsi()));
                initDateSpinner(0, calendar.getActualMaximum(Calendar.DAY_OF_MONTH), String.valueOf(calendar.getActualMaximum(Calendar.DAY_OF_MONTH)));
            } else {
                Calendar calendar = Calendar.getInstance();
                initDateSpinner(0, calendar.get(Calendar.DAY_OF_MONTH), String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
            }
        }, DateFormatSymbols.getInstance().getMonths()[Integer.valueOf(def)]);
    }

    public void initYearSpinner(int min, int max, String def) {
        ArrayCalendarSpinner.initYearSpinner(min, max, def);
        yearSpinner.initDataAdapter(ArrayCalendarSpinner.getAllYears(), (parent, view1, position, id) -> {
            if (!yearSpinner.getSelectedDsi().equals(String.valueOf(max))) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, Integer.valueOf(yearSpinner.getSelectedDsi()));
                initMonthSpinner(0, 11, String.valueOf(11));
                initDateSpinner(0, calendar.getActualMaximum(Calendar.DAY_OF_MONTH), String.valueOf(calendar.getActualMaximum(Calendar.DAY_OF_MONTH)));
            } else {
                Calendar calendar = Calendar.getInstance();
                initMonthSpinner(0, calendar.get(Calendar.MONTH), String.valueOf(calendar.get(Calendar.MONTH)));
                initDateSpinner(0, calendar.get(Calendar.DAY_OF_MONTH), String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
            }
        }, def);
    }
}
