package com.pasarwarga.market.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.WLog;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.fragment.home.BaseDrawerFragment;
import com.alfarabi.base.http.basemarket.service.OrderService;
import com.alfarabi.alfalibs.tools.CommonUtil;
import com.alfarabi.base.tools.IntentTools;
import com.hendraanggrian.bundler.annotations.BindExtra;
import com.kazy.lx.LxWebView;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;

import java.io.File;

import butterknife.BindView;

/**
 * Created by Alfarabi on 10/17/17.
 */

public class WebViewFragment extends BaseDrawerFragment {
    
    public static final String TAG = WebViewFragment.class.getName();

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout;
    @BindView(R.id.webview) LxWebView webView ;


    @BindExtra String urlAddress ;
    @BindExtra String  dealCodeNumber ;

    boolean isDownloading ;


    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public int getMenuId() {
        return 0 ;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        WLog.i(TAG, "Menu Clicked %%% "+item.getTitle());
        if(item.getItemId()==R.id.download_menu){
            if(isDownloading){
                return false ;
            }
            isDownloading = true ;
            HttpInstance.withProgress(activity()).observe(HttpInstance.create(OrderService.class).downloadInvoice(dealCodeNumber), responseBody -> {
            if(responseBody!=null){
                File file = CommonUtil.writeResponseBodyToDisk(activity(), responseBody);
                try {
                    IntentTools.goToPdfApps(activity(HomeActivity.class), file.getAbsolutePath());
                }catch (Exception e){
                    e.printStackTrace();
                    activity(HomeActivity.class).showDialog(getString(R.string.confirmation), getString(R.string.application_not_found), (dialog, which) -> {
                        WindowFlow.gotoAnotherApps(activity(HomeActivity.class), "com.adobe.reader");
                    }, (dialog, which) -> dialog.dismiss());

                }
            }else{
                activity(HomeActivity.class).showDialog(getString(R.string.server_error), (dialog, which) -> {
                    dialog.dismiss();
                });
            }
            isDownloading = false ;
            }, throwable -> {
                isDownloading = false ;
                activity(HomeActivity.class).showSnackbar(throwable, v -> {});
            });
        }
        return true;
    }

    @Override
    public String initTitle() {
        return "";
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_webview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        WLog.i(TAG, "Invoice :: "+urlAddress);
        toolbar.inflateMenu(R.menu.download_menu);
        toolbar.setOnMenuItemClickListener(this);
        webView.loadUrl(urlAddress);
    }

    @Override
    public boolean keepSideMenu() {
        return true;
    }
}
