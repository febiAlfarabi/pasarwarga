package com.pasarwarga.market.fragment.asseller;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.alfarabi.alfalibs.adapters.viewpager.BaseTabPagerAdapter;
import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.alfalibs.tools.GoogleSocial;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.adapter.StringBannerScollViewAdapter;
import com.alfarabi.base.fragment.home.BaseDrawerTabFragment;
import com.alfarabi.base.http.basemarket.service.ProductService;
import com.alfarabi.base.http.basemarket.service.SellerService;
import com.alfarabi.base.http.basemarket.service.SettingService;
import com.alfarabi.base.intent.CaptureImageIntent;
import com.alfarabi.base.intent.GetContentIntent;
import com.alfarabi.base.model.FormProduct;
import com.alfarabi.base.model.MyProfile;
import com.alfarabi.base.model.Product;
import com.alfarabi.base.model.Seller;
import com.alfarabi.base.model.Shipping;
import com.alfarabi.base.model.shop.SellerDashboard;
import com.alfarabi.alfalibs.tools.Caster;
import com.alfarabi.alfalibs.tools.CommonUtil;
import com.alfarabi.base.tools.MimeType;
import com.alfarabi.base.tools.PhoneTools;
import com.alfarabi.base.tools.ResponseTools;
import com.alfarabi.base.view.TabLayout;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.MessageDialog;
import com.facebook.share.widget.ShareDialog;
import com.flipboard.bottomsheet.commons.IntentPickerSheetView;
import com.github.ornolfr.ratingview.RatingView;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.plus.PlusShare;
import com.hendraanggrian.bundler.Bundler;
import com.kishan.askpermission.AskPermission;
import com.kishan.askpermission.PermissionCallback;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.SplashActivity;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.adapter.SellerDashboardTabPagerAdapter;
import com.pasarwarga.market.fragment.asseller.productform.ProductForm1Fragment;
import com.pasarwarga.market.fragment.asseller.setting.SellerSettingListFragment;
import com.pasarwarga.market.holder.recyclerview.SellerProductViewHolder;
import com.pasarwarga.market.tools.SystemApp;
import com.soundcloud.android.crop.Crop;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import cn.nekocode.rxactivityresult.RxActivityResult;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MultipartBody;

/**
 * Created by Alfarabi on 6/20/17.
 */

public class SellerDashboardFragment extends BaseDrawerTabFragment {

    public static String TAG = SellerDashboardFragment.class.getName();

    public static final int PHOTO_UPLOAD_MODE = 1;
    public static final int BANNER_UPLOAD_MODE = 2;
    public static final int NO_UPLOAD_MODE = 0;


    private int uploadMode = NO_UPLOAD_MODE;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout;
    @BindView(R.id.tablayout) TabLayout tabLayout ;
    @BindView(R.id.viewpager) ViewPager viewpager ;
    @BindView(R.id.sellername_tv) TextView sellernameTv ;
    @BindView(R.id.seller_address_tv) TextView sellerAddressTv ;
    @BindView(R.id.seller_profile_iv) ImageView sellerImageView ;
    @BindView(R.id.banner_iv) ImageView bannerImageView ;
    @BindView(R.id.bottomsheet) com.alfarabi.base.view.BottomSheetLayout bottomSheetLayout ;
    @BindView(R.id.favorite_button) CircleImageView favoriteButton ;
    @BindView(R.id.chat_button) CircleImageView chatButton ;
    @BindView(R.id.share_button) CircleImageView shareButton ;
//    @BindView(R.id.location_button) CircleImageView locationButton ;
    @BindView(R.id.rating_bar) RatingView ratingBar ;
    @BindView(R.id.profile_picture_button) LinearLayout profilePictureButton ;
    @BindView(R.id.profile_banner_button) FloatingActionButton profileBannerButton ;

    private SellerDashboardTabPagerAdapter sellerDashboardTabPagerAdapter ;
    private StringBannerScollViewAdapter stringBannerScollViewAdapter ;


    private CallbackManager callbackManager;
    private ShareDialog shareDialog;
    private ShareLinkContent linkContent ;


    private SellerDashboard sellerDashboard ;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        linkContent = new ShareLinkContent.Builder().setContentUrl(Uri.parse(getString(R.string.pasarwarga_com))).build();

        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                MessageDialog.show(getActivity(), linkContent);
            }

            @Override
            public void onCancel() {}

            @Override
            public void onError(FacebookException error) {}
        });
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);
        chatButton.setVisibility(View.GONE);
//        locationButton.setVisibility(View.GONE);
        favoriteButton.setVisibility(View.GONE);
//        profilePictureButton.setVisibility(View.VISIBLE);
//        profileBannerButton.setVisibility(View.VISIBLE);
        profilePictureButton.setOnClickListener(v -> {
            openPicturePicker();
        });
        profileBannerButton.setOnClickListener(v -> {
            openBannerPicker();
        });
        sellerImageView.setOnClickListener(v -> {
            openPicturePicker();
        });
        bannerImageView.setOnClickListener(v -> {
            openBannerPicker();
        });
        toolbar.inflateMenu(R.menu.seller_dashboard_menu);

        sellerDashboardTabPagerAdapter = new SellerDashboardTabPagerAdapter<>(getChildFragmentManager(), getContext());
        sellernameTv.setText(myProfile.getUser().getSellerInfo().getShopName());
        sellerAddressTv.setText(myProfile.getUser().getSellerInfo().getAddress());
        try{
            ratingBar.setRating(Float.valueOf(myProfile.getUser().getSellerInfo().getShoprating()));
        }catch (Exception e){
            ratingBar.setRating(0);
            e.printStackTrace();
        }
        stringBannerScollViewAdapter = new StringBannerScollViewAdapter(activity(), R.layout.fragment_banner, R.drawable.banner_pasarwarga, myProfile.getUser().getSellerInfo().getBannerImage());
//        alfaBannerView.setAdapter(stringBannerScollViewAdapter);
//        alfaBannerView.getAdapter().notifyDataSetChanged();

//        GlideApp.with(this).load(myProfile.getUser().getSellerInfo().getStoreImage()).placeholder(R.drawable.ic_pasarwarga).error(R.drawable.ic_pasarwarga).into(sellerImageView);
//        GlideApp.with(this).load(myProfile.getUser().getSellerInfo().getBannerImage()).error(R.drawable.banner_pasarwarga).placeholder(R.drawable.banner_pasarwarga).into(bannerImageView);

    }

    @Override
    public void onResume() {
        super.onResume();
        if(uploadMode==NO_UPLOAD_MODE){
            basicRequest();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Caster.activity(activity(HomeActivity.class), HomeActivity.class).getFabButton().setVisibility(FloatingActionButton.GONE);
    }

    public void initToolbar(){
        toolbar.setOnMenuItemClickListener(item -> {
            if(item.getItemId()==R.id.setting_menu){
                WindowFlow.goTo(activity(HomeActivity.class), SellerSettingListFragment.TAG, Bundler.wrap(SellerSettingListFragment.class, sellerDashboard), R.id.fragment_frame, true, false);
            }
            return true ;
        });
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_seller_dashboard;
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public BaseTabPagerAdapter<?> baseTabPagerAdapter() {
        return sellerDashboardTabPagerAdapter ;
    }

    @Override
    public TabLayout getTabLayout() {
        return tabLayout;
    }

    @Override
    public ViewPager getViewPager() {
        return viewpager ;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public String initTitle() {
        return null;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    @Override
    public int getMenuId() {
        return R.id.sub_mystore_menu_item_1;
    }

    @Override
    public SellerDashboard getObject() {
        return null;
    }

    @Override
    public boolean keepSideMenu() {
        return true;
    }

    public void basicRequest(){
        if(this.sellerDashboard==null) {
            sellerDashboard = new SellerDashboard();
            sellerDashboard.setSellerInfo(myProfile.getUser().getSellerInfo());
            GlideApp.with(this).load(sellerDashboard.getSellerInfo().getBannerImage()).error(R.drawable.banner_pasarwarga).placeholder(R.drawable.banner_pasarwarga)
                    .skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL).into(bannerImageView);
            GlideApp.with(this).load(myProfile.getUser().getPhoto()).placeholder(R.drawable.ic_pasarwarga).error(R.drawable.ic_pasarwarga)
                    .skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL).into(sellerImageView);

            HttpInstance.withProgress(activity()).observe(HttpInstance.create(SellerService.class).sellerDetail(Long.valueOf(myProfile.getUser().getSellerInfo().getId())), detailResponse -> {
                ResponseTools.validate(activity(HomeActivity.class), detailResponse, true);
                this.sellerDashboard = detailResponse.getSellerDashboard();
                GlideApp.with(this).load(sellerDashboard.getSellerInfo().getBannerImage()).error(R.drawable.banner_pasarwarga).placeholder(R.drawable.banner_pasarwarga)
                        .skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL).into(bannerImageView);
                GlideApp.with(this).load(sellerDashboard.getSellerInfo().getStoreImage()).placeholder(R.drawable.ic_pasarwarga).error(R.drawable.ic_pasarwarga)
                        .skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL).into(sellerImageView);
                sellerDashboardTabPagerAdapter.init(detailResponse.getSellerDashboard());
                initTabLayout(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}
                    @Override
                    public void onPageSelected(int position) {
                        if(position==0){
                            Caster.activity(activity(HomeActivity.class), HomeActivity.class).getFabButton().setVisibility(FloatingActionButton.VISIBLE);
                            Caster.activity(activity(HomeActivity.class), HomeActivity.class).getFabButton().setImageDrawable(getResources().getDrawable(R.drawable.ic_add_24dp));
                            ((SellerProductFragment)sellerDashboardTabPagerAdapter.fragmentClasses()[position]).setOnLongClickListener(onLongClickListener);
                        }else{
                            Caster.activity(activity(HomeActivity.class), HomeActivity.class).getFabButton().setVisibility(FloatingActionButton.GONE);
                        }
                    }
                    @Override
                    public void onPageScrollStateChanged(int state) {}
                });
                initShare(sellerDashboard);
                favoriteButton.setOnClickListener(v -> favoriteSeller());
                initToolbar();
                Caster.activity(activity(HomeActivity.class), HomeActivity.class).getFabButton().setVisibility(FloatingActionButton.VISIBLE);
                Caster.activity(activity(HomeActivity.class), HomeActivity.class).getFabButton().setImageDrawable(getResources().getDrawable(R.drawable.ic_add_24dp));
                ((SellerProductFragment)sellerDashboardTabPagerAdapter.fragmentClasses()[0]).setOnLongClickListener(onLongClickListener);
                Caster.activity(activity(HomeActivity.class), HomeActivity.class).getFabButton().setOnClickListener(v -> {
                    WindowFlow.goTo(activity(HomeActivity.class), ProductForm1Fragment.TAG,
                            Bundler.wrap(ProductForm1Fragment.class, null, null, ProductForm1Fragment.MODE_ADD), R.id.fragment_frame, true, false);
                });
            }, throwable -> {
                activity(HomeActivity.class).showSnackbar(throwable, v -> {});
            }, false);
        }else{
            sellerDashboardTabPagerAdapter.init(sellerDashboard);
            GlideApp.with(this).load(sellerDashboard.getSellerInfo().getBannerImage()).error(R.drawable.banner_pasarwarga).placeholder(R.drawable.banner_pasarwarga)
                    .skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL).into(bannerImageView);
            GlideApp.with(this).load(sellerDashboard.getSellerInfo().getStoreImage()).placeholder(R.drawable.ic_pasarwarga).error(R.drawable.ic_pasarwarga)
                    .skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL).into(sellerImageView);
            initTabLayout(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}
                @Override
                public void onPageSelected(int position) {
                    if(position==0){
                        Caster.activity(activity(HomeActivity.class), HomeActivity.class).getFabButton().setVisibility(FloatingActionButton.VISIBLE);
                        Caster.activity(activity(HomeActivity.class), HomeActivity.class).getFabButton().setImageDrawable(getResources().getDrawable(R.drawable.ic_shopping_cart_24dp));
                        ((SellerProductFragment)sellerDashboardTabPagerAdapter.fragmentClasses()[position]).setOnLongClickListener(onLongClickListener);
                    }else{
                        Caster.activity(activity(HomeActivity.class), HomeActivity.class).getFabButton().setVisibility(FloatingActionButton.GONE);
                    }
                }
                @Override
                public void onPageScrollStateChanged(int state) {}
            });
            initShare(sellerDashboard);
            favoriteButton.setOnClickListener(v -> favoriteSeller());
            initToolbar();
            Caster.activity(activity(HomeActivity.class), HomeActivity.class).getFabButton().setVisibility(FloatingActionButton.VISIBLE);
            Caster.activity(activity(HomeActivity.class), HomeActivity.class).getFabButton().setImageDrawable(getResources().getDrawable(R.drawable.ic_add_24dp));
            ((SellerProductFragment)sellerDashboardTabPagerAdapter.fragmentClasses()[0]).setOnLongClickListener(onLongClickListener);
            Caster.activity(activity(HomeActivity.class), HomeActivity.class).getFabButton().setOnClickListener(v -> {
                WindowFlow.goTo(activity(HomeActivity.class), ProductForm1Fragment.TAG,
                        Bundler.wrap(ProductForm1Fragment.class, null, null, ProductForm1Fragment.MODE_ADD),R.id.fragment_frame, true, false);
            });
        }
    }


    public void initShare(SellerDashboard sellerDashboard){
        final Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType(getString(R.string.text_plain));
        shareIntent.putExtra(Intent.EXTRA_TEXT, Uri.parse(sellerDashboard.getSellerInfo().getSeourl()).toString());
        IntentPickerSheetView intentPickerSheet = new IntentPickerSheetView(getActivity(), shareIntent, getString(R.string.share_with), activityInfo -> {
            bottomSheetLayout.dismissSheet();
            if(activityInfo.label.equals(getString(R.string.google_plus))){
                if(GoogleSocial.instance(getActivity()).getApiClient().isConnected()){
                    RxActivityResult.startActivityForResult(activity(), Auth.GoogleSignInApi.getSignInIntent(GoogleSocial.instance(getActivity()).getApiClient()), 1).subscribe(activityResult -> {
                        RxActivityResult.startActivityForResult(activity(HomeActivity.class),
                                new PlusShare.Builder(getActivity()).setType(getString(R.string.text_plain))
                                        .setText(sellerDashboard.getSellerInfo().getShopName()).setContentUrl(Uri.parse(sellerDashboard.getSellerInfo().getSeourl())).getIntent()
                                , 2).subscribe(activityResult1 -> {

                        });
                    }, throwable -> {
                        Toast.makeText(getActivity(), getString(R.string.error), Toast.LENGTH_SHORT).show();
                    });
                }else{
                    RxActivityResult.startActivityForResult(activity(HomeActivity.class),
                            new PlusShare.Builder(getActivity()).setType(getString(R.string.text_plain))
                                    .setText(sellerDashboard.getSellerInfo().getShopName()).setContentUrl(Uri.parse(sellerDashboard.getSellerInfo().getSeourl())).getIntent()
                            , 2).subscribe(activityResult1 -> {

                    });
                }
                return;
            }
            if(activityInfo.label.equals(getString(R.string.facebook))){
                showShareDialog();
                return;
            }

            startActivity(activityInfo.getConcreteIntent(shareIntent));

        });
        intentPickerSheet.setFilter(info -> {
//             return !info.componentName.getPackageName().startsWith("com.android");
            return  info.componentName.getPackageName().contains("com.whatsapp") ||
                    info.componentName.getPackageName().contains("com.instagram") ||
                    info.componentName.getPackageName().contains("org.telegram") ||
                    info.componentName.getPackageName().contains("jp.naver.line.android") ||
                    info.componentName.getPackageName().contains("com.twitter.android") ||
                    info.componentName.getPackageName().contains("com.google.android.apps.docs") ||
                    info.componentName.getPackageName().equals(PhoneTools.getDefaultSmsAppPackageName(getActivity()));
        });
        intentPickerSheet.setSortMethod((o1, o2) -> o1.label.compareTo(o2.label));
        Drawable googleDrawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_google_plus, null);
        IntentPickerSheetView.ActivityInfo googleInfo = new IntentPickerSheetView.ActivityInfo(googleDrawable, getString(R.string.google_plus), getActivity(), SplashActivity.class);
        Drawable facebookDrawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_facebook, null);
        IntentPickerSheetView.ActivityInfo facebookInfo = new IntentPickerSheetView.ActivityInfo(facebookDrawable, getString(R.string.facebook), getActivity(), SplashActivity.class);
        List<IntentPickerSheetView.ActivityInfo> activityInfos = new ArrayList<>();
        Collections.addAll(activityInfos,googleInfo, facebookInfo);
        intentPickerSheet.setMixins(activityInfos);

        shareButton.setOnClickListener(v -> bottomSheetLayout.showWithSheetView(intentPickerSheet));
    }

    private void showShareDialog(){
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            shareDialog.show(linkContent);
        }
    }

    public void favoriteSeller(){
        if(SystemApp.checkLogin(activity(HomeActivity.class), true)){
            HttpInstance.withProgress(activity()).observe(HttpInstance.create(SellerService.class).favorite(Long.valueOf(sellerDashboard.getSellerInfo().getId())), sellerFavoriteResponse -> {
                ResponseTools.validate(activity(), sellerFavoriteResponse);
                if(sellerFavoriteResponse.isStatus()){
                    if(sellerDashboard.getSellerInfo().isFavorite()){
                        sellerDashboard.getSellerInfo().setFavorite(false);
                        favoriteButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorite_white_24dp));
                    }else{
                        sellerDashboard.getSellerInfo().setFavorite(true);
                        favoriteButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorite_red_24dp));
                    }
                }else{
                    activity(HomeActivity.class).showDialog(sellerFavoriteResponse.getMessage(), (dialog, which) -> {});
                }
            }, throwable -> {
                activity(HomeActivity.class).showSnackbar(throwable, v -> {});
            }, false);
        }
    }
    String[] options = new String[2];
    View.OnLongClickListener onLongClickListener = view -> {
        SellerProductViewHolder sellerProductViewHolder = (SellerProductViewHolder) view.getTag();

        options[0] = getString(R.string.edit_product);
        options[1] = sellerProductViewHolder.getObject().isPublish()?getString(R.string.unpublish):getString(R.string.publish);
        new MaterialDialog.Builder(activity())
                .title(R.string.what_do_you_want_to_do)
                .items(options)
                .itemsCallbackSingleChoice(0, (dialog, itemView, which, text) -> {
                    if(which==0){
                        HttpInstance.withProgress(activity()).observe(HttpInstance.create(ProductService.class).getProduct(sellerProductViewHolder.getObject().getId()), productResponse -> {
                            ResponseTools.validate(activity(), productResponse);
                            if(productResponse.isStatus()){
                                Product product = productResponse.getProduct();
                                FormProduct formProduct = new FormProduct();
                                formProduct.setId(product.getId());
                                formProduct.setName(product.getName());
                                formProduct.setCategoryId(Long.valueOf(product.getCategory().getId()));
                                formProduct.setBasePrice(product.getBasePrice());
                                formProduct.setDiscount(product.getDiscount());
                                formProduct.setStartDiscount(product.getStartDiscount());
                                formProduct.setEndDiscount(product.getEndDiscount());
                                formProduct.setDescription(product.getDescription());
                                formProduct.setWeight(product.getWeight());
                                formProduct.setQuantity(product.getQuantity());
                                formProduct.setTags(product.getTags());
                                formProduct.setPublish(product.isPublish());
                                StringBuilder shippingMethodIds = new StringBuilder();
                                String shippingIds = "";
                                for (Shipping shipping : product.getShippings()) {
                                    shippingMethodIds.append(shipping.getId()).append(",");
                                }
                                if(shippingMethodIds.toString().endsWith(",")){
                                    shippingIds = shippingMethodIds.toString().substring(0, shippingMethodIds.toString().length()-1);
                                }else{
                                    shippingIds = shippingMethodIds.toString();
                                }
                                formProduct.setShippingMethodIds(shippingIds);
                                formProduct.setInsurance(product.isInsurance());
                                formProduct.setProductImage(product.getImages());
                                WindowFlow.goTo(activity(HomeActivity.class), ProductForm1Fragment.TAG, Bundler.wrap(ProductForm1Fragment.class, formProduct, product.getCategory(),
                                        ProductForm1Fragment.MODE_EDIT), R.id.fragment_frame, true, false);
                            }else{
                                activity(HomeActivity.class).showDialog(productResponse.getMessage(),(dialog1, which1) -> dialog.dismiss());
                            }

                        }, throwable -> {
                            activity(HomeActivity.class).showDialog(throwable, (dialog1, which1) -> {});
                        }, false);
                    }else if(which==1){
                        HttpInstance.withProgress(activity()).observe(HttpInstance.create(ProductService.class).changePublishStatus(sellerProductViewHolder.getObject().getId()), productResponse -> {
                           ResponseTools.validate(activity(), productResponse);
                            if(productResponse.isStatus()){
                                sellerProductViewHolder.getFragment().getSimpleRecyclerAdapter().getObjects().remove(sellerProductViewHolder.getAdapterPosition());
                                sellerProductViewHolder.getFragment().getSimpleRecyclerAdapter().getObjects().add(sellerProductViewHolder.getAdapterPosition(), productResponse.getProduct());
                                sellerProductViewHolder.getFragment().getSimpleRecyclerAdapter().notifyItemChanged(sellerProductViewHolder.getAdapterPosition());

                            }else{
                                activity(HomeActivity.class).showDialog(productResponse.getMessage(), (dialog1, which1) -> dialog.dismiss());
                            }
                        }, throwable -> {
                            activity(HomeActivity.class).showDialog(throwable, (dialog1, which1) -> {});
                        }, false);
                    }
                    return true;
                })
                .positiveText(R.string.ok).negativeText(R.string.cancel)
                .show();

        return true ;
    } ;


    private Uri imageUri;
    public void openPicturePicker() {
        uploadMode = PHOTO_UPLOAD_MODE;
        bottomSheetLayout.showImagePicker(this, () -> {
            new AskPermission.Builder(this).setPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE).setCallback(new PermissionCallback() {
                @Override
                public void onPermissionsGranted(int requestCode) {
                    RxActivityResult.startActivityForResult(activity(),CaptureImageIntent.getInstance(getContext()), 1)
                            .subscribe(activityResult -> {
                                if(activityResult.isOk()&& activityResult.getRequestCode()==1){
                                    try {
                                        imageUri = Uri.fromFile(new File(getContext().getCacheDir(), "image_" + System.currentTimeMillis() + ".jpg"));
                                        Crop.of(CaptureImageIntent.getInstance(getContext()).getResult(activityResult.getData()), imageUri).asSquare().start(getContext(), SellerDashboardFragment.this, Crop.REQUEST_CROP);
                                    }catch (Exception e){
                                        e.printStackTrace();
                                        new MaterialDialog.Builder(getContext()).title(R.string.confirmation).content(e.getMessage())
                                                .positiveText(R.string.report).negativeText(R.string.cancel).show();
                                    }
                                }
                            }, throwable -> {
                                throwable.printStackTrace();
                                new MaterialDialog.Builder(getContext()).title(R.string.confirmation).content(throwable.getMessage())
                                        .positiveText(R.string.report).negativeText(R.string.cancel).show();
                            });
                }

                @Override
                public void onPermissionsDenied(int requestCode) {
                        uploadMode = NO_UPLOAD_MODE;
                        new MaterialDialog.Builder(getContext())
                                .title(R.string.permission_required)
                                .content(R.string.camera_permission)
                                .positiveText(android.R.string.ok)
                                .onPositive((dialog, which) -> dialog.dismiss())
                                .show();
                }
            }).request(123456);


//            Dispatcher.with(this)
//                    .requestPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                    .dispatch(alreadyGranted -> {
//                        Dispatcher.with(this)
//                                .startActivityForResult(CaptureImageIntent.getInstance(getContext())).dispatch(data -> {
//                            imageUri = Uri.fromFile(new File(getContext().getCacheDir(), "image_" + System.currentTimeMillis() + ".jpg"));
//                            Crop.of(CaptureImageIntent.getInstance(getContext()).getResult(data), imageUri).asSquare().start(getContext(), this, Crop.REQUEST_CROP);
//                        });
//                    }, (granted, denied) -> {
//                        uploadMode = NO_UPLOAD_MODE;
//                        Snackbar.make(bottomSheetLayout, getString(R.string.camera_permission), Snackbar.LENGTH_SHORT).show();
//                    }, (permissions, dispatcher, onGranted, onDenied) -> {
//                        uploadMode = NO_UPLOAD_MODE;
//                        new MaterialDialog.Builder(getContext())
//                                .title(R.string.permission_required)
//                                .content(R.string.camera_permission)
//                                .positiveText(android.R.string.ok)
//                                .onPositive((dialog, which) -> dispatcher.dispatch(onGranted))
//                                .show();
//                    });
        }, () -> {
            RxActivityResult.startActivityForResult(getActivity(),new GetContentIntent("image/*"), 2).subscribe(activityResult -> {
                if(activityResult.getData()!=null) {
                    imageUri = Uri.fromFile(new File(getContext().getCacheDir(), "image_" + System.currentTimeMillis() + ".jpg"));
                    Crop.of(GetContentIntent.extract(activityResult.getData())[0], imageUri)
                            .asSquare()
                            .start(getContext(), this, Crop.REQUEST_CROP);
                }

            }, throwable -> {
                throwable.printStackTrace();
                new MaterialDialog.Builder(getContext()).title(R.string.confirmation).content(throwable.getMessage())
                        .positiveText(R.string.report).negativeText(R.string.cancel).show();
            });
//            Dispatcher.with(this)
//                    .startActivityForResult(new GetContentIntent("image/*"))
//                    .dispatch(data -> {
//                        imageUri = Uri.fromFile(new File(getContext().getCacheDir(), "image_" + System.currentTimeMillis() + ".jpg"));
//                        Crop.of(GetContentIntent.extract(data)[0], imageUri)
//                                .asSquare()
//                                .start(getContext(), this, Crop.REQUEST_CROP);
//                    });
        }, uri -> {
            imageUri = Uri.fromFile(new File(getContext().getCacheDir(), "image_" + System.currentTimeMillis() + ".jpg"));
            Crop.of(uri, imageUri).asSquare().start(getContext(), this, Crop.REQUEST_CROP);
        });
    }

    public void openBannerPicker() {
        uploadMode = BANNER_UPLOAD_MODE;
        bottomSheetLayout.showImagePicker(this, () -> {
            new AskPermission.Builder(this).setPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE).setCallback(new PermissionCallback() {
                @Override
                public void onPermissionsGranted(int requestCode) {
                    RxActivityResult.startActivityForResult(activity(),CaptureImageIntent.getInstance(getContext()), 1)
                            .subscribe(activityResult -> {
                                if(activityResult.isOk() && activityResult.getRequestCode()==1){
                                    try {
                                        imageUri = Uri.fromFile(new File(getContext().getCacheDir(), "image_" + System.currentTimeMillis() + ".jpg"));
                                        Crop.of(CaptureImageIntent.getInstance(getContext()).getResult(activityResult.getData()), imageUri).withAspect(16, 12).start(getContext(), SellerDashboardFragment.this, Crop.REQUEST_CROP);
                                    }catch (Exception e){
                                        e.printStackTrace();
                                        new MaterialDialog.Builder(getContext()).title(R.string.confirmation).content(e.getMessage())
                                                .positiveText(R.string.report).negativeText(R.string.cancel).show();
                                    }
                                }
                            }, throwable -> {
                                throwable.printStackTrace();
                                new MaterialDialog.Builder(getContext()).title(R.string.confirmation).content(throwable.getMessage())
                                        .positiveText(R.string.report).negativeText(R.string.cancel).show();
                            });
                }

                @Override
                public void onPermissionsDenied(int requestCode) {
                    uploadMode = NO_UPLOAD_MODE;
                    new MaterialDialog.Builder(getContext())
                            .title(R.string.permission_required)
                            .content(R.string.camera_permission)
                            .positiveText(android.R.string.ok)
                            .onPositive((dialog, which) -> dialog.dismiss())
                            .show();
                }
            }).request(123456);

//            Dispatcher.with(this)
//                    .requestPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                    .dispatch(alreadyGranted -> {
//                        Dispatcher.with(this)
//                                .startActivityForResult(CaptureImageIntent.getInstance(getContext())).dispatch(data -> {
//                            imageUri = Uri.fromFile(new File(getContext().getCacheDir(), "image_" + System.currentTimeMillis() + ".jpg"));
//                            Crop.of(CaptureImageIntent.getInstance(getContext()).getResult(data), imageUri).withAspect(16, 12).start(getContext(), this, Crop.REQUEST_CROP);
//                        });
//                    }, (granted, denied) -> {
//                        uploadMode = NO_UPLOAD_MODE;
//                        Snackbar.make(bottomSheetLayout, getString(R.string.camera_permission), Snackbar.LENGTH_SHORT).show();
//                    }, (permissions, dispatcher, onGranted, onDenied) -> {
//                        uploadMode = NO_UPLOAD_MODE;
//                        new MaterialDialog.Builder(getContext())
//                                .title(R.string.permission_required)
//                                .content(R.string.camera_permission)
//                                .positiveText(android.R.string.ok)
//                                .onPositive((dialog, which) -> dispatcher.dispatch(onGranted))
//                                .show();
//                    });
        }, () -> {
            RxActivityResult.startActivityForResult(activity(), new GetContentIntent("image/*"), 2).subscribe(activityResult -> {
                if(activityResult.getData()!=null) {
                    imageUri = Uri.fromFile(new File(getContext().getCacheDir(), "image_" + System.currentTimeMillis() + ".jpg"));
                    Crop.of(GetContentIntent.extract(activityResult.getData())[0], imageUri)
                            .withAspect(16, 12)
                            .start(getContext(), this, Crop.REQUEST_CROP);
                }
            }, throwable -> {
                throwable.printStackTrace();
                new MaterialDialog.Builder(getContext()).title(R.string.confirmation).content(throwable.getMessage())
                        .positiveText(R.string.report).negativeText(R.string.cancel).show();
            });
//            Dispatcher.with(this)
//                    .startActivityForResult(new GetContentIntent("image/*"))
//                    .dispatch(data -> {
//                        imageUri = Uri.fromFile(new File(getContext().getCacheDir(), "image_" + System.currentTimeMillis() + ".jpg"));
//                        Crop.of(GetContentIntent.extract(data)[0], imageUri)
//                                .withAspect(16, 12)
//                                .start(getContext(), this, Crop.REQUEST_CROP);
//                    });
        }, uri -> {
            imageUri = Uri.fromFile(new File(getContext().getCacheDir(), "image_" + System.currentTimeMillis() + ".jpg"));
            Crop.of(uri, imageUri).withAspect(16, 12).start(getContext(), this, Crop.REQUEST_CROP);
        });
    }


    public MultipartBody.Part imagePartProfile;
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        Dispatcher.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Crop.REQUEST_CROP && resultCode == Activity.RESULT_OK) {
            Uri uriProfile = imageUri;
            if(uploadMode==PHOTO_UPLOAD_MODE){
                GlideApp.with(getContext()).load(uriProfile).into(sellerImageView);
                imagePartProfile = CommonUtil.generateMultiPart(getContext(), "photo", imageUri);
                if (SystemApp.checkLogin(activity(HomeActivity.class), true)) {
                    HttpInstance.withProgress(activity()).observe(HttpInstance.create(SettingService.class).changeProfilePicture(imagePartProfile), changeProfilePictureResponse -> {
                        ResponseTools.validate(activity(), changeProfilePictureResponse);
                        if(changeProfilePictureResponse.isStatus()) {
//                            MyProfile myProfile2 = SystemApp.myProfile();
                            myProfile.getUser().setPhoto(changeProfilePictureResponse.getUser().getPhoto());
                            SystemApp.setProfile(myProfile);
                            Caster.activity(activity(HomeActivity.class), HomeActivity.class).initData();
                            Caster.activity(activity(HomeActivity.class), HomeActivity.class).initMenu();
                            ResponseTools.validate(activity(), changeProfilePictureResponse);
                        }else{
                            activity(HomeActivity.class).showSnackbar(changeProfilePictureResponse.getMessage(), v -> {});
                            GlideApp.with(this).load(myProfile.getUser().getSellerInfo().getStoreImage()).placeholder(R.drawable.ic_pasarwarga).error(R.drawable.ic_pasarwarga).into(sellerImageView);
                        }
                    }, throwable -> {
                        activity(HomeActivity.class).showSnackbar(throwable, v -> {});
                    }, false);
                }
            }else if(uploadMode==BANNER_UPLOAD_MODE){
                GlideApp.with(this).load(uriProfile).error(R.drawable.banner_pasarwarga).placeholder(R.drawable.ic_pasarwarga).into(bannerImageView);
                imagePartProfile = CommonUtil.generateMultiPart(getContext(), "banner_image", imageUri);
                if (SystemApp.checkLogin(activity(HomeActivity.class), true)) {
                    HashMap<String, String> hashMap = new HashMap<>();
                    hashMap.put("banner_image", uriProfile.toString());

                    HttpInstance.withProgress(activity()).observe(HttpInstance.create(SellerService.class).updateShopInfo(
                            MimeType.valueOf("text/plain").createRequestBody(HttpInstance.getGson().toJson(hashMap)), imagePartProfile), response -> {
                        ResponseTools.validate(activity(), response);
                        if(response.isStatus()) {
                            myProfile.getUser().setSellerInfo(response.getSellerInfo());
                            SystemApp.setProfile(myProfile);
                            Caster.activity(activity(HomeActivity.class), HomeActivity.class).initData();
                            Caster.activity(activity(HomeActivity.class), HomeActivity.class).initMenu();
                        }else{
                            activity(HomeActivity.class).showSnackbar(response.getMessage(), v -> {});
                            GlideApp.with(this).load(sellerDashboard.getSellerInfo().getBannerImage()).error(R.drawable.banner_pasarwarga).placeholder(R.drawable.banner_pasarwarga).into(bannerImageView);
                        }
                    }, throwable -> {
                        activity(HomeActivity.class).showSnackbar(throwable, v -> {});
                    }, false);
                }
            }

        }


    }


}
