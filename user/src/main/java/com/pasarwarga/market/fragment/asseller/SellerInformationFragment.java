package com.pasarwarga.market.fragment.asseller;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.TextView;

import com.alfarabi.base.fragment.BaseUserFragment;
import com.alfarabi.base.model.shop.SellerDashboard;
import com.flipboard.bottomsheet.BottomSheetLayout;
import com.flipboard.bottomsheet.commons.IntentPickerSheetView;
import com.pasarwarga.market.R;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class SellerInformationFragment extends BaseUserFragment<SellerDashboard> {

    SellerDashboard sellerDashboard ;

    @BindView(R.id.store_owner_tv) TextView storeOwnerTextView ;
    @BindView(R.id.join_since_tv) TextView joinSinceTextView ;
    @BindView(R.id.last_login_tv) TextView lastLoginTextView ;
    @BindView(R.id.total_product_tv) TextView totalProductTextView ;
    @BindView(R.id.sold_product_tv) TextView soldProductTextView ;
    @BindView(R.id.successfull_transaction_tv) TextView successfullTransactionTextView ;
    @BindView(R.id.location_tv) TextView locationTextView ;
    @BindView(R.id.location_info_cardview) CardView locationInfoCardView ;
    @BindView(R.id.bottomsheet) BottomSheetLayout bottomSheetLayout ;


    @Override
    public String initTitle() {
        title = getString(R.string.information);
        return title;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_seller_information;
    }

    @Override
    public void setObject(SellerDashboard object) {
        super.setObject(object);
        sellerDashboard = object ;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        storeOwnerTextView.setText(getString(R.string.pasarwarga_store_owner)+" : "+sellerDashboard.getSellerInfo().getSellerName());
        joinSinceTextView.setText(getString(R.string.join_since)+" : "+sellerDashboard.getSellerInfo().getJoinSince());
        lastLoginTextView.setText(getString(R.string.last_login)+" : "+sellerDashboard.getSellerInfo().getLastLogin());

        totalProductTextView.setText(getString(R.string.total_product)+" : "+String.valueOf(sellerDashboard.getSellerInfo().getProductCount()));
        soldProductTextView.setText(getString(R.string.sold_product)+" : "+String.valueOf(sellerDashboard.getSellerInfo().getSalesCount()));
        successfullTransactionTextView.setText(getString(R.string.successful_transaction)+" : "+String.valueOf(sellerDashboard.getSellerInfo().getSalesCount()));
        locationTextView.setText(sellerDashboard.getSellerInfo().getAddress());

        String uri = "http://maps.google.com/maps?q=loc:"+String.valueOf(sellerDashboard.getSellerInfo().getLocation().getLatitude())
                +","+String.valueOf(sellerDashboard.getSellerInfo().getLocation().getLongitude())+" ("+getActivity().getPackageName()+")";
        Intent shareIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        shareIntent.setFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
        shareIntent.setFlags(Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP);
        IntentPickerSheetView intentPickerSheet = new IntentPickerSheetView(getActivity(), shareIntent, getString(R.string.open_with), activityInfo -> {
            bottomSheetLayout.dismissSheet();
            startActivity(activityInfo.getConcreteIntent(shareIntent));
        });
        intentPickerSheet.setFilter(info -> {
            return  info.componentName.getPackageName().contains("waze") ||
                    info.componentName.getPackageName().contains("maps") ||
                    info.componentName.getPackageName().contains("com.google.android.street") ||
                    info.componentName.getPackageName().contains("com.google.earth") ||
                    info.componentName.getPackageName().contains("com.google.android.apps.docs");
        });
        intentPickerSheet.setSortMethod((o1, o2) -> o1.label.compareTo(o2.label));

        locationInfoCardView.setOnClickListener(v -> {
            bottomSheetLayout.showWithSheetView(intentPickerSheet);
        });
    }
}
