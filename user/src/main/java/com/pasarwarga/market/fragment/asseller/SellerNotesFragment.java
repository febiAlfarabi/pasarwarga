package com.pasarwarga.market.fragment.asseller;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.base.fragment.BaseUserFragment;
import com.alfarabi.base.http.basemarket.service.NoteService;
import com.alfarabi.base.model.shop.Note;
import com.alfarabi.base.model.shop.SellerDashboard;
import com.alfarabi.base.tools.ResponseTools;
import com.github.aakira.expandablelayout.ExpandableLinearLayout;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class SellerNotesFragment extends BaseUserFragment<SellerDashboard> {


    @BindView(R.id.recyclerview) RecyclerView recyclerView ;
    private List<Note> notes = new ArrayList<>();

    @Override
    public String initTitle() {
        title = getString(R.string.information);
        return title;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_seller_notes;
    }

    @Override
    public void onResume() {
        super.onResume();
        basicRequest();
    }

    public void basicRequest(){
        HttpInstance.withProgress(activity()).observe(HttpInstance.create(NoteService.class).getNotesBySellerId(getObject().getSellerInfo().getId()), noteResponse -> {
            ResponseTools.validate(activity(HomeActivity.class), noteResponse);
            if(noteResponse.isStatus()){
                notes.clear();
                notes.addAll(noteResponse.getNotes());
                recyclerView.getAdapter().notifyDataSetChanged();
            }
        }, throwable -> {
            activity(HomeActivity.class).showSnackbar(throwable, v -> {});
        }, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity()));
        recyclerView.setAdapter(new RecyclerView.Adapter<ViewHolder>() {
            @Override
            public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                return new ViewHolder(LayoutInflater.from(activity(HomeActivity.class)).inflate(R.layout.viewholder_note, parent, false));
            }

            @Override
            public void onBindViewHolder(ViewHolder holder, int position) {
                holder.showData(position);
            }

            @Override
            public int getItemCount() {
                return notes.size();
            }
        });
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.title_tv) TextView titleTextView ;
        @BindView(R.id.content_tv) TextView contentTextView ;
        @BindView(R.id.button) LinearLayout button ;
        @BindView(R.id.expandableLayout) ExpandableLinearLayout expandableLinearLayout ;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void showData(int position){
            titleTextView.setText(notes.get(position).getTitle());
            contentTextView.setText(notes.get(position).getContent());
            button.setOnClickListener(v -> {
                if(expandableLinearLayout.isExpanded()){
                    expandableLinearLayout.collapse();
                }else{
                    expandableLinearLayout.expand();
                }
            });
        }
    }

}
