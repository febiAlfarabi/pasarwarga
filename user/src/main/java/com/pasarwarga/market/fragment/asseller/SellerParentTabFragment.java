package com.pasarwarga.market.fragment.asseller;


import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.alfarabi.alfalibs.adapters.viewpager.BaseTabPagerAdapter;
import com.alfarabi.base.fragment.home.BaseDrawerTabFragment;
import com.alfarabi.base.model.shop.SellerDashboard;
import com.alfarabi.base.view.TabLayout;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.adapter.SellerParentTabPagerAdapter;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
@Deprecated
public class SellerParentTabFragment extends BaseDrawerTabFragment<SellerDashboard> {

    public static final String TAG = SellerParentTabFragment.class.getName();


    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout;

    @BindView(R.id.tab_layout) TabLayout tabLayout ;
    @BindView(R.id.viewpager) ViewPager viewPager ;

    int tabNumber = 0 ;

    private SellerParentTabPagerAdapter sellerParentTabPagerAdapter ;


    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public int getMenuId() {
        if(tabNumber==0){
            return R.id.sub_mystore_menu_item_2;
        }else if(tabNumber==1){
            return R.id.sub_mystore_menu_item_3;
        }else if(tabNumber==2){
            return R.id.sub_mystore_menu_item_4;
        }else if(tabNumber==3){
            return R.id.sub_mystore_menu_item_5;
        }
        return 0;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    @Override
    public String initTitle() {
        title = getString(R.string.my_store);
        toolbar.setTitle(title);
        return title;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_seller_parent_tab;
    }

    @Override
    public BaseTabPagerAdapter<?> baseTabPagerAdapter() {
        return sellerParentTabPagerAdapter ;
    }

    @Override
    public TabLayout getTabLayout() {
        return tabLayout;
    }

    @Override
    public ViewPager getViewPager() {
        return viewPager;
    }

    @Override
    public boolean keepSideMenu() {
        return true;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sellerParentTabPagerAdapter = new SellerParentTabPagerAdapter<>(getFragmentManager(), activity(HomeActivity.class));
        if(getArguments()!=null && getArguments().containsKey("tab_number")){
            tabNumber = getArguments().getInt("tab_number");
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initTabLayout(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                new Handler().postDelayed(() -> sellerParentTabPagerAdapter.initIcon(tabLayout, position), 100);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        sellerParentTabPagerAdapter.initIcon(tabLayout, tabNumber);
        new Handler().postDelayed(() -> tabLayout.getTabAt(tabNumber).select(), 100);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public boolean onBackPressed() {
        if(tabLayout.getSelectedTabPosition()!=0){
            new Handler().postDelayed(() -> tabLayout.getTabAt(0).select(), 100);
            return false ;
        }
        return super.onBackPressed();
    }
}
