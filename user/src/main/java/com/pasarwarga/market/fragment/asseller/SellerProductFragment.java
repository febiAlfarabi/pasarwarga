package com.pasarwarga.market.fragment.asseller;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.alfarabi.alfalibs.adapters.recyclerview.SimpleRecyclerAdapter;
import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.alfarabi.base.R2;
import com.alfarabi.base.behaviour.PaginationScrollListener;
import com.alfarabi.base.fragment.BaseUserFragment;
import com.alfarabi.base.http.basemarket.service.SellerService;
import com.alfarabi.base.model.Landing;
import com.alfarabi.base.model.Product;
import com.alfarabi.base.model.location.Category;
import com.alfarabi.base.model.shop.SellerDashboard;
import com.alfarabi.base.realm.RealmDao;
import com.alfarabi.base.tools.ResponseTools;
import com.alfarabi.base.view.DataMaterialSpinner;
import com.hendraanggrian.preferencer.annotations.BindPreferenceRes;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.adapter.ProductSortAdapter;
import com.pasarwarga.market.holder.recyclerview.SellerProductViewHolder;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 6/19/17.
 */

//@Deprecated
public class SellerProductFragment extends BaseUserFragment<SellerDashboard> {

    public static final String TAG = SellerProductFragment.class.getName();

    @BindView(R.id.recyclerview) AlfaRecyclerView recyclerView;
    @BindView(R.id.swiperefreshlayout) SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.sort_button) ImageView sortImageView ;
    @BindView(R.id.grid_mode_button) ImageView gridModeImageView ;
    @BindView(R.id.category_sp) DataMaterialSpinner<Category> dataMaterialSpinner ;
    private Landing landing ;
    @Getter SimpleRecyclerAdapter<Product, SellerProductFragment, SellerProductViewHolder> simpleRecyclerAdapter;
    @Getter @Setter @BindPreferenceRes(R2.string.store_profile_list_column) int storeProfileListColumn;

    @Nullable @BindView(R.id.search_et) SearchView searchView ;

    int page = 1 ;
    long selectedCategoryId ;



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        landing = RealmDao.instance().get(Landing.class);
        sortDialog = new SortDialog();
        simpleRecyclerAdapter = new SimpleRecyclerAdapter(this, SellerProductViewHolder.class, new ArrayList<Product>());
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        searchView = ButterKnife.findById(activity(HomeActivity.class), R.id.search_et);
        dataMaterialSpinner.initDataAdapter(landing.getCategories(), (parent, view1, position, id) -> {
            selectedCategoryId = dataMaterialSpinner.getSelectedDsi().getId();
            firstRequest(sortDialog.getSortAdapter().getSelectedSort().getSortValue());
        }, landing.getCategories().get(0));

        simpleRecyclerAdapter.initRecyclerView(recyclerView, new GridLayoutManager(getActivity(), storeProfileListColumn));
        recyclerView.setOnScrollListener(new PaginationScrollListener((LinearLayoutManager) recyclerView.getLayoutManager()) {
            @Override
            protected void loadMoreItems() {
                HttpInstance.call(HttpInstance.create(SellerService.class).sellerDetail(Long.valueOf(getObject().getSellerInfo().getId()), ++page, selectedCategoryId,
                        sortDialog.getSortAdapter().getSelectedSort().getSortValue(), searchView.getQuery().toString()), detailResponse -> {
                    ResponseTools.validate(activity(HomeActivity.class), detailResponse);
                    if(detailResponse.isStatus() && detailResponse.getSellerDashboard()!=null){
                        simpleRecyclerAdapter.appendObjects(detailResponse.getSellerDashboard().getProducts());
                    }else{
                        activity(HomeActivity.class).showSnackbar(detailResponse.getMessage(), v -> {});
                    }
                    simpleRecyclerAdapter.notifyDataSetChanged();

                }, throwable -> {
                    activity(HomeActivity.class).showSnackbar(throwable, v -> {});
                    simpleRecyclerAdapter.notifyDataSetChanged();
                }, false);
            }
        });
        swipeRefreshLayout.setOnRefreshListener(() -> {
            firstRequest(sortDialog.getSortAdapter().getSelectedSort().getSortValue());
        });
        sortImageView.setOnClickListener(v -> {
            sortDialog.show();
        });
        gridModeImageView.setOnClickListener(v -> {
            if (storeProfileListColumn == 1) {
                setStoreProfileListColumn(2);
            } else {
                setStoreProfileListColumn(1);
            }
            if (saver != null) {
                saver.saveAll();
            }

            simpleRecyclerAdapter.initRecyclerView(recyclerView, new GridLayoutManager(getActivity(), storeProfileListColumn));
            firstRequest(sortDialog.getSortAdapter().getSelectedSort().getSortValue());
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                firstRequest(sortDialog.getSortAdapter().getSelectedSort().getSortValue());
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                firstRequest(sortDialog.getSortAdapter().getSelectedSort().getSortValue());
                return true;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if(simpleRecyclerAdapter!=null && simpleRecyclerAdapter.getObjects()!=null && simpleRecyclerAdapter.getObjects().size()>0){
            return;
        }
        firstRequest(sortDialog.getSortAdapter().getSelectedSort().getSortValue());
    }


    public void firstRequest(String sort){
        swipeRefreshLayout.setRefreshing(true);
        HttpInstance.call(HttpInstance.create(SellerService.class).sellerDetail(Long.valueOf(getObject().getSellerInfo().getId()), page = 1, selectedCategoryId, sort,
                searchView.getQuery().toString()), detailResponse -> {
            ResponseTools.validate(activity(HomeActivity.class), detailResponse);
            if(detailResponse.isStatus() && detailResponse.getSellerDashboard()!=null){
                simpleRecyclerAdapter.setObjects(detailResponse.getSellerDashboard().getProducts());
            }else{
                activity(HomeActivity.class).showSnackbar(detailResponse.getMessage(), v -> {});
            }
            swipeRefreshLayout.setRefreshing(false);

        }, throwable -> {
            activity(HomeActivity.class).showSnackbar(throwable, v -> {});
            simpleRecyclerAdapter.notifyDataSetChanged();
            swipeRefreshLayout.setRefreshing(false);

        }, false);
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_seller_product;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public String initTitle() {
        title = getString(R.string.store_profile);
        return title;
    }

    SortDialog sortDialog;

    @Getter View.OnLongClickListener onLongClickListener ;

    public void setOnLongClickListener(View.OnLongClickListener onLongClickListener) {
        this.onLongClickListener = onLongClickListener;
        if(simpleRecyclerAdapter!=null){
            simpleRecyclerAdapter.notifyDataSetChanged();
        }
    }

    public class SortDialog {

        @BindView(R.id.recycler_sort)
        RecyclerView recyclerView;

        BottomSheetDialog dialog;
        @Getter ProductSortAdapter sortAdapter;

        SortDialog() {
            View view = View.inflate(activity(HomeActivity.class), R.layout.dialog_sort_list, null);
            dialog = new BottomSheetDialog(activity(HomeActivity.class));
            dialog.setContentView(view, new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

            BottomSheetBehavior.from(((View) view.getParent())).setHideable(false);
            sortAdapter = new ProductSortAdapter();

            ButterKnife.bind(this, view);
            recyclerView.setLayoutManager(new LinearLayoutManager(activity(HomeActivity.class)));
            recyclerView.setAdapter(sortAdapter);
            sortAdapter.notifyDataSetChanged();

            sortAdapter.setOnSortClickListener(selectedSort -> {
                if (sortDialog != null && sortDialog.dialog != null && sortDialog.dialog.isShowing()) {
                    sortDialog.dismiss();
                }
                sortedData();
            });
        }

        public void sortedData() {
            firstRequest(sortAdapter.getSelectedSort().getSortValue());
        }

        public void show() {
            dialog.show();
        }

        public void dismiss() {
            dialog.dismiss();
        }
    }

}
