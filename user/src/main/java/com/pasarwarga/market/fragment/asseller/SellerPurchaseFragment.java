package com.pasarwarga.market.fragment.asseller;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.alfarabi.base.fragment.BaseUserFragment;
import com.alfarabi.base.model.shop.SellerDashboard;
import com.pasarwarga.market.R;

/**
 * Created by Alfarabi on 6/19/17.
 */

@Deprecated
public class SellerPurchaseFragment extends BaseUserFragment<SellerDashboard> {

    public static final String TAG = SellerPurchaseFragment.class.getName();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_seller_purchase;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public String initTitle() {
        title = getString(R.string.store_profile);
        return title;
    }
}
