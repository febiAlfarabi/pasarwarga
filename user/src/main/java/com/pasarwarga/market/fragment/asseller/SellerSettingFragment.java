package com.pasarwarga.market.fragment.asseller;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;

import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.base.fragment.BaseUserFragment;
import com.alfarabi.base.http.basemarket.service.SellerService;
import com.alfarabi.base.model.shop.SellerDashboard;
import com.alfarabi.base.realm.RealmDao;
import com.alfarabi.base.tools.ResponseTools;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;

import org.apache.commons.lang.StringUtils;

import butterknife.BindView;

/**
 * Created by Alfarabi on 6/19/17.
 */

@Deprecated
public class SellerSettingFragment extends BaseUserFragment<SellerDashboard> {

    public static final String TAG = SellerSettingFragment.class.getName();

    @BindView(R.id.seller_imageview) ImageView sellerImageView ;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        renderHeader();
    }

    @Override
    public void onResume() {
        super.onResume();
        HttpInstance.call(HttpInstance.create(SellerService.class).sellerDetail(Long.valueOf(myProfile.getUser().getSellerId())), detailResponse -> {
            ResponseTools.validate(activity(HomeActivity.class), detailResponse);
            if(detailResponse.isStatus() && detailResponse.getSellerDashboard()!=null){
//                RealmDao.instance().renew(SellerDashboard.INITIAL_ID, detailResponse.getSellerDashboard());
                RealmDao.instance().insertOrUpdate(detailResponse.getSellerDashboard());
                setObject(RealmDao.instance().get(SellerDashboard.class));
                renderHeader();
            }else{
                activity(HomeActivity.class).showSnackbar(detailResponse.getMessage(), v -> {});
            }
        }, throwable -> {
            activity(HomeActivity.class).showSnackbar(throwable, v -> {});
        });
    }

    public void renderHeader(){
        if(getObject()!=null){
            GlideApp.with(this).load(StringUtils.split(getObject().getSellerInfo().getStoreImage(), ",")[0])
                    .placeholder(R.drawable.ic_pasarwarga)
                    .error(R.drawable.ic_pasarwarga)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            e.printStackTrace();
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }
                    }).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL).into(sellerImageView);

//            sellernameTextView.setText(getObject().getSellerInfo().getShopName());
//            sellerLocationTextView.setText(getObject().getSellerInfo().getSellerName());
//            sellerRatingBar.setRating(getObject().getSellerInfo().getShoprating());

//            stringBannerScollViewAdapter.setBannerUrls(StringUtils.split(getObject().getSellerInfo().getBannerImage(), ","));
//            stringBannerScollViewAdapter.notifyDataSetChanged();
        }
    }


    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_seller_setting;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public String initTitle() {
        title = getString(R.string.store_profile);
        return title;
    }
}
