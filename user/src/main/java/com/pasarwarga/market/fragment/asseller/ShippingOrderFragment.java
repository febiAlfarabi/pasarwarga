package com.pasarwarga.market.fragment.asseller;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.fragment.BaseUserFragment;
import com.alfarabi.base.model.shop.SellerDashboard;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ShippingOrderFragment extends BaseUserFragment<SellerDashboard> {


    @BindView(R.id.toolbar) Toolbar toolbar;

    @Override
    public String initTitle() {
        return null;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_shipping_order;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        toolbar.setNavigationOnClickListener(v -> WindowFlow.backstack(activity(HomeActivity.class)));
    }
}
