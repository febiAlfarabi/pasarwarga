package com.pasarwarga.market.fragment.asseller.productform;


import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.afollestad.materialdialogs.MaterialDialog;
import com.alfarabi.alfalibs.tools.WLog;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.alfarabi.base.fragment.BaseUserFragment;
import com.alfarabi.base.intent.CaptureImageIntent;
import com.alfarabi.base.intent.GetContentIntent;
import com.alfarabi.base.model.FormProduct;
import com.alfarabi.base.model.Landing;
import com.alfarabi.base.model.location.Category;
import com.alfarabi.base.model.shop.SellerDashboard;
import com.alfarabi.base.realm.RealmDao;
import com.alfarabi.alfalibs.tools.CommonUtil;
import com.alfarabi.base.tools.InputTools;
import com.alfarabi.alfalibs.tools.KeyboardUtils;
import com.alfarabi.base.view.BottomSheetLayout;
import com.alfarabi.base.view.DataMaterialSpinner;
import com.hendraanggrian.bundler.Bundler;
import com.hendraanggrian.bundler.annotations.BindExtra;
import com.kishan.askpermission.AskPermission;
import com.kishan.askpermission.PermissionCallback;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.adapter.StringRecyclerAdapter;
import com.pasarwarga.market.fragment.SettingsProfileFragment;
import com.pasarwarga.market.fragment.asseller.SellerDashboardFragment;
import com.pasarwarga.market.holder.recyclerview.asseller.AddProductImageViewHolder;
import com.soundcloud.android.crop.Crop;

import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import cn.nekocode.rxactivityresult.RxActivityResult;
import it.sephiroth.android.library.tooltip.Tooltip;
import lombok.Getter;
import lombok.Setter;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductForm1Fragment extends BaseUserFragment<SellerDashboard> {


    // Add product dengan mengupload images dalam parameter product_images[]
    // sedangkan untuk datanya dijadikan json ke dalam parameter data

    public static final boolean MODE_ADD = false ;
    public static final boolean MODE_EDIT = true ;


    public static final String TAG = ProductForm1Fragment.class.getName();
    @Getter @BindView(R.id.bottomsheet) BottomSheetLayout bottomSheetLayout;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.recyclerview) AlfaRecyclerView recyclerView ;
    @BindView(R.id.product_name_et) EditText productNameEditText ;
    @BindView(R.id.category_sp) DataMaterialSpinner<Category> categorySpinner ;
    @BindView(R.id.next_button) Button nextButton ;

    @Getter StringRecyclerAdapter<ProductForm1Fragment, AddProductImageViewHolder, String> stringRecyclerAdapter;
    private List<Category> categories ;
    @Nullable @BindExtra FormProduct formProduct ;
    @Nullable @BindExtra Category category ;
    @Nullable @BindExtra boolean formMode ; /// MODE ADD = false, MODE EDIT

//    public static HashMap<Integer,File> uriHashSet = new HashMap<>();



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            CommonUtil.deleteImageFile(activity());
        } catch (IOException e) {
            e.printStackTrace();
        }
        stringRecyclerAdapter = new StringRecyclerAdapter<>(this, AddProductImageViewHolder.class, new ArrayList<>());
        stringRecyclerAdapter.getObjects().add("");
        if(formMode==MODE_EDIT){
            try{
                stringRecyclerAdapter.getObjects().addAll(Arrays.asList(StringUtils.split(formProduct.getProductImage(), ",")));
            }catch (Exception e){
                WindowFlow.backstack(activity(HomeActivity.class));
                activity(HomeActivity.class).showSnackbar(e, v -> {});
            }
        }
        List<Category> defaultCategories = RealmDao.instance().get(Landing.class).getCategories();
        categories = new ArrayList<>();
        categories.addAll(defaultCategories);
        int indexToRemove = 0;
        for (int i = 0; i < categories.size(); i++) {
            if(categories.get(i).getId()==0){
                indexToRemove = i;
                break;
            }
        }
        categories.remove(indexToRemove);
    }

    @Override
    public String initTitle() {
        return null;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_add_product_form_1;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        stringRecyclerAdapter.initRecyclerView(recyclerView, new LinearLayoutManager(activity(), LinearLayoutManager.HORIZONTAL, false));
        toolbar.setTitle(formMode==ProductForm1Fragment.MODE_ADD?getString(R.string.add_product):getString(R.string.edit_product));
        toolbar.setNavigationOnClickListener(v -> WindowFlow.backstack(activity(HomeActivity.class)));
        categorySpinner.initDataAdapter(categories, (parent, view1, position, id) -> {}, categories.get(0));
        categorySpinner.setOnClickListener(v -> KeyboardUtils.hideKeyboard(activity()));
        if(formMode==MODE_EDIT){
            productNameEditText.setText(formProduct.getName());
            categorySpinner.setSelectedDsi(category);
        }

        nextButton.setOnClickListener(v -> {
            if(stringRecyclerAdapter.getItemCount()<2){
                Tooltip.TooltipView tooltipView = Tooltip.make(getActivity(), new Tooltip.Builder(101).anchor(recyclerView, Tooltip.Gravity.BOTTOM)
                        .closePolicy(new Tooltip.ClosePolicy().insidePolicy(true, false).outsidePolicy(true, false), 3000)
                        .activateDelay(800).showDelay(300).text(getString(R.string.please_upload_image_min_one)).withStyleId(R.style.AppTheme_TooltipLayout)
                        .maxWidth(500).withArrow(true).withOverlay(true).floatingAnimation(Tooltip.AnimationBuilder.DEFAULT)
                        .build()
                );
                tooltipView.setTextColor(Color.WHITE);
                tooltipView.show();
                return;
            }
            if(InputTools.isComplete(productNameEditText, categorySpinner) && stringRecyclerAdapter.getItemCount()>=2){
                if(formMode==MODE_ADD){
                    formProduct = new FormProduct();
                }
                String[] imageArray = new String[stringRecyclerAdapter.getItemCount() - 1];
                formProduct.setName(productNameEditText.getText().toString());
                formProduct.setCategoryId(categorySpinner.getSelectedDsi().getId());
                int i = 0;
                for (String image : stringRecyclerAdapter.getObjects()) {
                    if (!image.isEmpty()) {
                        imageArray[i] = image;
                        i++;
                    }
                }
                formProduct.setProductImage(Arrays.toString(imageArray));
                WindowFlow.goTo(activity(HomeActivity.class), ProductForm2Fragment.TAG, Bundler.wrap(ProductForm2Fragment.class, formProduct, formMode), R.id.fragment_frame, true);
            }
        });
    }

    Uri imageUri = null ;
    @Getter@Setter AddProductImageViewHolder currentEditImageHolder ;
    @Getter View.OnClickListener imageClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            currentEditImageHolder = (AddProductImageViewHolder) v.getTag();
            bottomSheetLayout.showImagePicker(ProductForm1Fragment.this, () -> {
                new AskPermission.Builder(activity()).setPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE).setCallback(new PermissionCallback() {
                    @Override
                    public void onPermissionsGranted(int requestCode) {
                        RxActivityResult.startActivityForResult(activity(), CaptureImageIntent.getInstance(getContext()), 1).subscribe(activityResult -> {
                            if(activityResult.isOk() && activityResult.getRequestCode()==1){
                                try {
                                    imageUri = Uri.fromFile(new File(getContext().getCacheDir(), "image_" + System.currentTimeMillis() + ".jpg"));
                                    Crop.of(CaptureImageIntent.getInstance(activity(HomeActivity.class)).getResult(activityResult.getData()), imageUri).asSquare()
                                            .start(activity(HomeActivity.class), ProductForm1Fragment.this, Crop.REQUEST_CROP);
                                }catch (Exception e){
                                    e.printStackTrace();
                                    new MaterialDialog.Builder(getContext()).title(R.string.confirmation).content(e.getMessage())
                                            .positiveText(R.string.report).negativeText(R.string.cancel).show();
                                }
                            }
                        }, throwable -> {
                            throwable.printStackTrace();
                            new MaterialDialog.Builder(getContext()).title(R.string.confirmation).content(throwable.getMessage())
                                    .positiveText(R.string.report).negativeText(R.string.cancel).show();
                        });
                    }

                    @Override
                    public void onPermissionsDenied(int requestCode) {
                        new MaterialDialog.Builder(getContext())
                                .title(R.string.permission_required)
                                Uri idCardUri = FileProvider.getUriForFile(getActivity(), "com.pasarwarga.microsite.fileprovider", idCardFile);
                                intent.putExtra(MediaStore.EXTRA_OUTPUT, idCardUri);
                                List<ResolveInfo> resInfoList = getActivity(MainActivity.class).getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
                                for (ResolveInfo resolveInfo : resInfoList) {
                                    String packageName = resolveInfo.activityInfo.packageName;
                                    getActivity(MainActivity.class).grantUriPermission(packageName, idCardUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                }
                                RxActivityResult.startActivityForResult(getActivity(), intent, REQUEST_IDCARD_CAPTURE).subscribe(activityResult -> {
                                    if(requestCode == REQUEST_IDCARD_CAPTURE &&activityResult.isOk()){
                                        WLog.i(TAG, "REQUEST ID CARD CAPTURE SUCCESS");
                                        Bitmap imageBitmap = BitmapFactory.decodeFile(idCardFile.getAbsolutePath());
                                        int nh = (int) ( imageBitmap.getHeight() * (512.0 / imageBitmap.getWidth()) );
                                        Bitmap scaled = Bitmap.createScaledBitmap(imageBitmap, 512, nh, true);
                                        idCardImageView.setImageBitmap(scaled);

//                                        idCardImageView.setImageBitmap(imageBitmap);
                                        idCardSuccess = true ;
                                        idCardCamera.setImageDrawable(getActivity(MainActivity.class).getResources().getDrawable(R.drawable.ic_retake));
                                    }
                                });
                            }
                        }

                        @Override
                        public void onPermissionsDenied(int requestCode) {}
                    })
                    .setErrorCallback(new ErrorCallback() {
                        @Override
                        public void onShowRationalDialog(PermissionInterface permissionInterface, int requestCode) {}

                        @Override
                        public void onShowSettings(PermissionInterface permissionInterface, int requestCode) {}
                                .content(R.string.camera_permission)
                                .positiveText(android.R.string.ok)
                                .onPositive((dialog, which) -> dialog.dismiss())
                                .show();
                    }
                }).request(123456);


//                new AskPermission.Builder(ProductForm1Fragment.this).setPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE).setCallback(new PermissionCallback() {
//                    @Override
//                    public void onPermissionsGranted(int requestCode) {
//                        RxActivityResult.getInstance(getActivity()).from(getActivity()).startActivityForResult(CaptureImageIntent.getInstance(getContext()), 1)
//                                .subscribe(activityResult -> {
//                                    if(activityResult.isOk() && activityResult.getData()!=null && activityResult.getRequestCode()==1){
//                                        try {
//                                            imageUri = Uri.fromFile(new File(getContext().getCacheDir(), "image_" + System.currentTimeMillis() + ".jpg"));
//                                            Crop.of(CaptureImageIntent.getInstance(activity(HomeActivity.class)).getResult(activityResult.getData()), imageUri).asSquare()
//                                                    .start(activity(HomeActivity.class), ProductForm1Fragment.this, Crop.REQUEST_CROP);
//                                        }catch (Exception e){
//                                            e.printStackTrace();
//                                            new MaterialDialog.Builder(getContext()).title(R.string.confirmation).content(e.getMessage())
//                                                    .positiveText(R.string.report).negativeText(R.string.cancel).show();
//                                        }
//                                    }
//                                }, throwable -> {
//                                    throwable.printStackTrace();
//                                    new MaterialDialog.Builder(getContext()).title(R.string.confirmation).content(throwable.getMessage())
//                                            .positiveText(R.string.report).negativeText(R.string.cancel).show();
//                                });
//                    }
//
//                    @Override
//                    public void onPermissionsDenied(int requestCode) {
//                        Snackbar.make(bottomSheetLayout, getString(R.string.camera_permission), Snackbar.LENGTH_SHORT).show();
//                    }
//                }).request(123456);

//                Dispatcher.with(ProductForm1Fragment.this)
//                        .requestPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                        .dispatch(alreadyGranted -> {
//                            Dispatcher.with(ProductForm1Fragment.this).startActivityForResult(CaptureImageIntent.getInstance(activity(HomeActivity.class))).dispatch(data -> {
//                                imageUri = Uri.fromFile(new File(activity(HomeActivity.class).getCacheDir(), "image_" + System.currentTimeMillis() + ".jpg"));
//                                Crop.of(CaptureImageIntent.getInstance(activity(HomeActivity.class)).getResult(data), imageUri).asSquare()
//                                        .start(activity(HomeActivity.class), ProductForm1Fragment.this, Crop.REQUEST_CROP);
//                            });
//                        }, (granted, denied) -> {
//                            Snackbar.make(bottomSheetLayout, getString(R.string.camera_permission), Snackbar.LENGTH_SHORT).show();
//                        }, (permissions, dispatcher, onGranted, onDenied) -> {
//                            new MaterialDialog.Builder(activity(HomeActivity.class))
//                                    .title(R.string.permission_required)
//                                    .content(R.string.camera_permission)
//                                    .positiveText(android.R.string.ok)
//                                    .onPositive((dialog, which) -> dispatcher.dispatch(onGranted))
//                                    .show();
//                        });
            }, () -> {
                new AskPermission.Builder(activity()).setPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE).setCallback(new PermissionCallback() {
                    @Override
                    public void onPermissionsGranted(int requestCode) {
                        RxActivityResult.startActivityForResult(activity(), new GetContentIntent("image/*"), 2).subscribe(activityResult -> {
                            if(activityResult.getData()!=null) {
                                imageUri = Uri.fromFile(new File(getContext().getCacheDir(), "image_" + System.currentTimeMillis() + ".jpg"));
                                Crop.of(GetContentIntent.extract(activityResult.getData())[0], imageUri).asSquare()
                                .start(activity(HomeActivity.class), ProductForm1Fragment.this, Crop.REQUEST_CROP);
                            }
                        }, throwable -> {
                            throwable.printStackTrace();
                            new MaterialDialog.Builder(getContext()).title(R.string.confirmation).content(throwable.getMessage())
                                    .positiveText(R.string.report).negativeText(R.string.cancel).show();
                        });
                    }

                    @Override
                    public void onPermissionsDenied(int requestCode) {
                        new MaterialDialog.Builder(getContext())
                                .title(R.string.permission_required)
                                .content(R.string.camera_permission)
                                .positiveText(android.R.string.ok)
                                .onPositive((dialog, which) -> dialog.dismiss())
                                .show();
                    }
                }).request(123456);


//                RxActivityResult.getInstance(getActivity()).from(getActivity()).startActivityForResult(new GetContentIntent("image/*"), 2).subscribe(activityResult -> {
//                    if(activityResult.getData()!=null) {
//                        imageUri = Uri.fromFile(new File(getContext().getCacheDir(), "image_" + System.currentTimeMillis() + ".jpg"));
//                        Crop.of(GetContentIntent.extract(activityResult.getData())[0], imageUri).asSquare()
//                                .start(activity(HomeActivity.class), ProductForm1Fragment.this, Crop.REQUEST_CROP);
//                    }
//                }, throwable -> {
//                    throwable.printStackTrace();
//                    new MaterialDialog.Builder(getContext()).title(R.string.confirmation).content(throwable.getMessage())
//                            .positiveText(R.string.report).negativeText(R.string.cancel).show();
//                });
//                Dispatcher.with(ProductForm1Fragment.this)
//                        .startActivityForResult(new GetContentIntent("image/*"))
//                        .dispatch(data -> {
//                            imageUri = Uri.fromFile(new File(activity(HomeActivity.class).getCacheDir(), "image_" + System.currentTimeMillis() + ".jpg"));
//                            Crop.of(GetContentIntent.extract(data)[0], imageUri).asSquare()
//                                    .start(activity(HomeActivity.class), ProductForm1Fragment.this, Crop.REQUEST_CROP);
//                        });
            }, uri -> {
                imageUri = Uri.fromFile(new File(activity(HomeActivity.class).getCacheDir(), "image_" + System.currentTimeMillis() + ".jpg"));
                Crop.of(uri, imageUri).asSquare().start(activity(HomeActivity.class), ProductForm1Fragment.this, Crop.REQUEST_CROP);
            });
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        Dispatcher.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Crop.REQUEST_CROP && resultCode == Activity.RESULT_OK) {
            WLog.i(TAG, "Activity.RESULT_OK");
            WLog.i(TAG, "String IMAGE URI "+imageUri.toString());
            if(currentEditImageHolder.getObject()==null || currentEditImageHolder.getObject().isEmpty()){
                stringRecyclerAdapter.getObjects().remove(currentEditImageHolder.getAdapterPosition());
                stringRecyclerAdapter.getObjects().add(currentEditImageHolder.getAdapterPosition(), imageUri.toString());
                stringRecyclerAdapter.notifyItemChanged(currentEditImageHolder.getAdapterPosition());
                stringRecyclerAdapter.getObjects().add(0, "");
                stringRecyclerAdapter.notifyItemInserted(0);
            }else{
                stringRecyclerAdapter.getObjects().remove(currentEditImageHolder.getAdapterPosition());
                stringRecyclerAdapter.getObjects().add(currentEditImageHolder.getAdapterPosition(), imageUri.toString());
                stringRecyclerAdapter.notifyItemChanged(currentEditImageHolder.getAdapterPosition());
            }
//            Uri uriProfile = imageUri;
//            GlideApp.with(getContext()).load(uriProfile).into(profileImageView);
//            imagePartProfile = CommonUtil.generateMultiPart(getContext(), "photo", imageUri);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        Dispatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

}
