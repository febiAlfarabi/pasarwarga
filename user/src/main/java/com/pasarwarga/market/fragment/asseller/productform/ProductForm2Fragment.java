package com.pasarwarga.market.fragment.asseller.productform;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;

import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.WLog;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.fragment.BaseUserFragment;
import com.alfarabi.base.model.FormProduct;
import com.alfarabi.base.model.shop.SellerDashboard;
import com.alfarabi.base.tools.Initial;
import com.alfarabi.base.tools.InputTools;
import com.alfarabi.alfalibs.tools.KeyboardUtils;
import com.alfarabi.base.tools.MoneyTextWatcher;
import com.alfarabi.base.tools.NumberTextWatcherForNumber;
import com.alfarabi.base.view.BottomSheetLayout;
import com.alfarabi.base.view.StringMaterialSpinner;
import com.cunoraz.tagview.Tag;
import com.cunoraz.tagview.TagView;
import com.hendraanggrian.bundler.Bundler;
import com.hendraanggrian.bundler.annotations.BindExtra;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;

import org.apache.commons.lang.StringUtils;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import lombok.Getter;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductForm2Fragment extends BaseUserFragment<SellerDashboard> {



    public static final String TAG = ProductForm2Fragment.class.getName();
    @Getter @BindView(R.id.bottomsheet) BottomSheetLayout bottomSheetLayout;

    @BindView(R.id.toolbar) Toolbar toolbar;

    @BindView(R.id.product_price_et) EditText productPriceEditText ;
    @BindView(R.id.stock_et) EditText stockEditText ;
    @BindView(R.id.weight_sp) StringMaterialSpinner weightSpinner ;
    @BindView(R.id.weight_et) EditText weightEditText ;
    @BindView(R.id.description_et) EditText descriptionEditText ;
    @BindView(R.id.tag_et) EditText tagEditText ;
    @BindView(R.id.hashapp) TagView hashtagView ;

    List<Tag> hashtags = new ArrayList<>();
    @BindView(R.id.next_button) Button nextButton ;

    @BindExtra FormProduct formProduct ;
    @Nullable@BindExtra boolean formMode ; /// MODE ADD = false, MODE EDIT

    private String[] weights = null ;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        weights = new String[]{getString(R.string.gram)};
        WLog.i(TAG, "### PRODUCT ### "+HttpInstance.getGson().toJson(formProduct));
    }

    @Override
    public String initTitle() {
        return null;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_add_product_form_2;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        toolbar.setTitle(formMode==ProductForm1Fragment.MODE_ADD?getString(R.string.add_product):getString(R.string.edit_product));
        toolbar.setNavigationOnClickListener(v -> WindowFlow.backstack(activity(HomeActivity.class)));
        productPriceEditText.addTextChangedListener(new MoneyTextWatcher(productPriceEditText, "%,.2f"));
        stockEditText.addTextChangedListener(new NumberTextWatcherForNumber(stockEditText));
        weightEditText.addTextChangedListener(new NumberTextWatcherForNumber(weightEditText));
        if(formMode==ProductForm1Fragment.MODE_EDIT){
            productPriceEditText.setText(MoneyTextWatcher.moneyNormalize(formProduct.getBasePrice()));
            stockEditText.setText(String.valueOf(formProduct.getQuantity()));
            weightEditText.setText(String.valueOf(formProduct.getWeight()));

            descriptionEditText.setText(String.valueOf(formProduct.getDescription()));
            if(formProduct.getTags()!=null && !formProduct.getTags().isEmpty()){
                for (String hashtag :StringUtils.split(formProduct.getTags(), ",")) {
                    Tag tag = new Tag(hashtag);
                    tag.isDeletable = true ;
                    tag.layoutColor = activity().getResources().getColor(R.color.red_800);
                    hashtags.add(tag);
                }
                hashtagView.addTags(hashtags);
            }
        }else{
            stockEditText.setText(getString(R.string.nil));
            weightEditText.setText(getString(R.string.nil));
        }
        hashtagView.setOnTagDeleteListener((tagView, tag, i) -> {
            tagView.remove(i);
            hashtags.remove(i);
        });
        tagEditText.setOnEditorActionListener((v, actionId, event) -> {
           if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_DONE || event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
               Tag tag = new Tag(tagEditText.getText().toString());
               tag.isDeletable = true ;
               tag.layoutColor = activity().getResources().getColor(R.color.red_800);
               hashtagView.addTag(tag);
               tagEditText.setText("");
               KeyboardUtils.hideKeyboard(activity());
               return true;
           }
           return false;
       });
        weightSpinner.initDataAdapter(weights, (parent, view1, position, id) -> {}, weights[0]);
        nextButton.setOnClickListener(v -> {
            if (InputTools.isComplete(productPriceEditText, stockEditText, weightEditText, descriptionEditText)) {
                if(weightEditText.getText().toString().equalsIgnoreCase("0")){
                    weightEditText.setError(getString(R.string.weight_should_more_than_0));
                    return;
                }
                if(stockEditText.getText().toString().equalsIgnoreCase("0")){
                    stockEditText.setError(getString(R.string.weight_should_more_than_0));
                    return;
                }
                if(MoneyTextWatcher.floatOf(productPriceEditText.getText().toString())>= Initial.MIN_PRODUCT_PRICE) {
                    formProduct.setBasePrice(MoneyTextWatcher.floatOf(productPriceEditText.getText().toString()));
                    formProduct.setQuantity(Integer.valueOf(stockEditText.getText().toString()));
                    formProduct.setWeight(Integer.valueOf(weightEditText.getText().toString()));
                    formProduct.setDescription(descriptionEditText.getText().toString());
                    String[] hashtagArray = new String[hashtagView.getTags().size()];
                    int i = 0;
                    for (Tag tag :hashtagView.getTags()) {
                        hashtagArray[i++] = tag.text;
                    }
                    formProduct.setTags(Arrays.toString(hashtagArray).replace("[","").replace("]",""));
                    WindowFlow.goTo(activity(HomeActivity.class), ProductForm3Fragment.TAG, Bundler.wrap(ProductForm3Fragment.class, formProduct, formMode), R.id.fragment_frame, true);
                }else{
                    activity(HomeActivity.class).showDialog(getString(R.string.invalid_price),
                            getString(R.string.invalid_price_notice)+" "+getString(R.string.curr_symb)+" "+ DecimalFormat.getInstance().format(Initial.MIN_PRODUCT_PRICE).toString(), (dialog, which) -> {
                                productPriceEditText.setText("");
                                dialog.dismiss();
                            });
                }
            }
        });
    }
}
