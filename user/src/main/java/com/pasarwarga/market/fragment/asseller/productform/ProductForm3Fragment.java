package com.pasarwarga.market.fragment.asseller.productform;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.alfarabi.alfalibs.adapters.recyclerview.VerticalRecyclerAdapter;
import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.WLog;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.alfarabi.base.fragment.BaseUserFragment;
import com.alfarabi.base.http.basemarket.service.ShippingService;
import com.alfarabi.base.model.FormProduct;
import com.alfarabi.base.model.Shipping;
import com.alfarabi.base.model.shop.SellerDashboard;
import com.alfarabi.base.view.BottomSheetLayout;
import com.alfarabi.base.view.StringMaterialSpinner;
import com.hendraanggrian.bundler.Bundler;
import com.hendraanggrian.bundler.annotations.BindExtra;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.holder.recyclerview.asseller.ShippingRadioSelectViewHolder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import butterknife.BindView;
import it.sephiroth.android.library.tooltip.Tooltip;
import lombok.Getter;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductForm3Fragment extends BaseUserFragment<SellerDashboard> {

    public static final String TAG = ProductForm3Fragment.class.getName();
    @Getter @BindView(R.id.bottomsheet) BottomSheetLayout bottomSheetLayout;

    @BindView(R.id.toolbar) Toolbar toolbar;

    @BindView(R.id.insurance_sp) StringMaterialSpinner insuranceSpinner ;
    @BindView(R.id.recyclerview) AlfaRecyclerView<Shipping> recyclerView ;
    @BindView(R.id.next_button) Button nextButton ;

    @Getter@BindExtra FormProduct formProduct ;
    @Nullable@BindExtra boolean formMode ; /// MODE ADD = false, MODE EDIT
    String[] insuranceOptions = new String[2];

    List<Shipping> shippings = new ArrayList<>();
    @Getter private VerticalRecyclerAdapter<ProductForm3Fragment, ShippingRadioSelectViewHolder, Shipping> optionAdapter ;
    @Getter HashMap<String, Boolean> hashMap = new HashMap<>();

    StringBuilder selectedShipping = new StringBuilder() ;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WLog.i(TAG, "### PRODUCT ### "+ HttpInstance.getGson().toJson(formProduct));
        insuranceOptions = getResources().getStringArray(R.array.yes_no_option);
        optionAdapter = new VerticalRecyclerAdapter(this, ShippingRadioSelectViewHolder.class, shippings);

    }

    @Override
    public String initTitle() {
        return null;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_add_product_form_3;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(formMode==ProductForm1Fragment.MODE_ADD) {
            toolbar.setTitle(getString(R.string.add_product));
        }else{
            toolbar.setTitle(getString(R.string.edit_product));
        }
        toolbar.setNavigationOnClickListener(v -> WindowFlow.backstack(activity(HomeActivity.class)));
        hashMap.clear();

        String shippingMethodCollection = formProduct.getShippingMethodIds();
        if(shippingMethodCollection.split(",").length>0){
            String[] shippingMethodIds = shippingMethodCollection.split(",");
            for (int i = 0; i < shippingMethodIds.length ; i++) {
                hashMap.put(shippingMethodIds[i], true);
            }
        }

        insuranceSpinner.initDataAdapter(insuranceOptions, (parent, view1, position, id) -> {}, insuranceOptions[1]);
        if(formMode==ProductForm1Fragment.MODE_EDIT) {
            if(formProduct.isInsurance()){
                insuranceSpinner.setSelectedDsi(insuranceOptions[0]);
            }else{
                insuranceSpinner.setSelectedDsi(insuranceOptions[1]);
            }
        }else{

        }
        optionAdapter.initRecyclerView(recyclerView, new LinearLayoutManager(activity()));

        nextButton.setOnClickListener(v -> {
            if(hashMap.size()>0){
                selectedShipping = new StringBuilder();
                Set<Map.Entry<String, Boolean>> entrySet = hashMap.entrySet();
                Iterator<Map.Entry<String, Boolean>> iterator = entrySet.iterator();
                while (iterator.hasNext()){
                    Map.Entry<String, Boolean> entry = iterator.next();
                    if(entry.getValue()){
                        selectedShipping.append(entry.getKey()).append(",");
                    }
                }
                if(!selectedShipping.toString().isEmpty() && !selectedShipping.toString().equals(",")){
                    if(selectedShipping.toString().endsWith(",")){
                        selectedShipping.deleteCharAt(selectedShipping.length()-1);
                    }
                    formProduct.setShippingMethodIds(selectedShipping.toString());
                    formProduct.setInsurance(insuranceSpinner.getSelectedDsi().equals(getString(R.string.yes)));
                    WindowFlow.goTo(activity(HomeActivity.class), ProductForm4Fragment.TAG, Bundler.wrap(ProductForm4Fragment.class, formProduct, formMode), R.id.fragment_frame, true);
                }else{
                    Tooltip.TooltipView tooltipView = Tooltip.make(getActivity(), new Tooltip.Builder(101).anchor(recyclerView, Tooltip.Gravity.BOTTOM)
                            .closePolicy(new Tooltip.ClosePolicy().insidePolicy(true, false).outsidePolicy(true, false), 3000)
                            .activateDelay(800).showDelay(300).text(getString(R.string.choose_at_least_one_shipping_method)).withStyleId(R.style.AppTheme_TooltipLayout)
                            .maxWidth(500).withArrow(true).withOverlay(true).floatingAnimation(Tooltip.AnimationBuilder.DEFAULT)
                            .build()
                    );
                    tooltipView.show();
                }
            }

            return;
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        HttpInstance.withProgress(activity()).observe(HttpInstance.create(ShippingService.class).shippingSevices(), shippingResponse -> {
            if(shippingResponse.isStatus()){
                shippings.clear();
                shippings.addAll(shippingResponse.getShippings());
                optionAdapter.setObjects(shippings);
            }else{
                activity(HomeActivity.class).showDialog(shippingResponse.getMessage(), (dialog, which) -> dialog.dismiss());
            }
        }, throwable -> {
            activity(HomeActivity.class).showSnackbar(throwable, v -> {});
        });
    }
}
