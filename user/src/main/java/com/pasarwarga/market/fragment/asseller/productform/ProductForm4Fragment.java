package com.pasarwarga.market.fragment.asseller.productform;


import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.WLog;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.fragment.BaseUserFragment;
import com.alfarabi.base.http.basemarket.service.ProductService;
import com.alfarabi.base.model.FormProduct;
import com.alfarabi.base.model.shop.SellerDashboard;
import com.alfarabi.alfalibs.tools.CommonUtil;
import com.alfarabi.base.tools.Initial;
import com.alfarabi.base.tools.MimeType;
import com.alfarabi.base.tools.NumberTextWatcherForLimitPercent;
import com.alfarabi.base.tools.ResponseTools;
import com.alfarabi.base.view.BottomSheetLayout;
import com.alfarabi.base.view.StringMaterialSpinner;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.hendraanggrian.bundler.annotations.BindExtra;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.fragment.asseller.SellerDashboardFragment;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;

import butterknife.BindView;
import it.sephiroth.android.library.tooltip.Tooltip;
import lombok.Getter;
import okhttp3.MultipartBody;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductForm4Fragment extends BaseUserFragment<SellerDashboard> {

    public static final String TAG = ProductForm4Fragment.class.getName();
    @Getter @BindView(R.id.bottomsheet) BottomSheetLayout bottomSheetLayout;

    private static DateFormat dateFormat = new SimpleDateFormat("");

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.discount_et) EditText discountEditText;
    @BindView(R.id.draft_button) Button draftButton;
    @BindView(R.id.next_button) Button nextButton;
    @BindView(R.id.start_et) EditText startEditText;
    @BindView(R.id.end_et) EditText endEditText;
    @BindView(R.id.discount_option_spinner) StringMaterialSpinner discountSpinner ;
    @BindView(R.id.date_layout) LinearLayout dateLayout ;
    @BindView(R.id.expandable_layout)
    ExpandableRelativeLayout expandableRelativeLayout ;

    @BindExtra FormProduct formProduct;
    @Nullable@BindExtra boolean formMode ; /// MODE ADD = false, MODE EDIT
    private Calendar startCalendar;
    private Calendar endCalendar;
    private String[] noYes ;

    private String[] publishOptions = new String[2];


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dateFormat = new SimpleDateFormat(getString(R.string.yyyy_MM_dd));
        startCalendar = Calendar.getInstance();
        endCalendar = Calendar.getInstance();
        publishOptions[0] = getString(R.string.publish);
        publishOptions[1] = getString(R.string.unpublish);
        noYes = getResources().getStringArray(R.array.no_yes_option);
        if(formMode==ProductForm1Fragment.MODE_EDIT) {
            if(formProduct.getDiscount()>0){
                try {
                    startCalendar.setTime(dateFormat.parse(formProduct.getStartDiscount()));
                    endCalendar.setTime(dateFormat.parse(formProduct.getEndDiscount()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }


    }

    @Override
    public String initTitle() {
        return null;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_add_product_form_4;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        toolbar.setTitle(formMode==ProductForm1Fragment.MODE_ADD?getString(R.string.add_product):getString(R.string.edit_product));
        toolbar.setNavigationOnClickListener(v -> WindowFlow.backstack(activity(HomeActivity.class)));
        discountEditText.addTextChangedListener(new NumberTextWatcherForLimitPercent(discountEditText));
        if(formMode==ProductForm1Fragment.MODE_EDIT) {
            if(formProduct.getDiscount()>0){
                expandableRelativeLayout.postDelayed(() -> {
                    expandableRelativeLayout.expand();
                }, 500);
                discountSpinner.initDataAdapter(noYes, (parent, view1, position, id) -> {
                    if(discountSpinner.getSelectedDsi().equals(getString(R.string.yes))){
                        expandableRelativeLayout.expand();
                    }else{
                        expandableRelativeLayout.collapse();
                    }
                }, noYes[1]);
                discountEditText.setText(String.valueOf(formProduct.getDiscount()));
            }else{
                expandableRelativeLayout.postDelayed(() -> {
                    expandableRelativeLayout.collapse();
                }, 500);
                discountSpinner.initDataAdapter(noYes, (parent, view1, position, id) -> {
                    if(discountSpinner.getSelectedDsi().equals(getString(R.string.yes))){
                        expandableRelativeLayout.expand();
                    }else{
                        expandableRelativeLayout.collapse();
                    }
                }, noYes[0]);
            }
        }else{
            expandableRelativeLayout.collapse();
            discountSpinner.initDataAdapter(noYes, (parent, view1, position, id) -> {
                if(discountSpinner.getSelectedDsi().equals(getString(R.string.yes))){
                    expandableRelativeLayout.expand();
                }else{
                    expandableRelativeLayout.collapse();
                }
            }, noYes[0]);
        }


        startEditText.setText(DateFormatUtils.format(startCalendar.getTime(), getString(R.string.E_dd_MM_yyyy)));
        endEditText.setText(DateFormatUtils.format(endCalendar.getTime(), getString(R.string.E_dd_MM_yyyy)));

        startEditText.setOnClickListener(v -> {
            DatePickerDialog dpd = DatePickerDialog.newInstance((view1, year, monthOfYear, dayOfMonth) -> {
                    startCalendar.set(Calendar.YEAR, year);
                    startCalendar.set(Calendar.MONTH, monthOfYear);
                    startCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    startEditText.setText(DateFormatUtils.format(startCalendar.getTime(), getString(R.string.E_dd_MM_yyyy)));
                }, startCalendar.get(Calendar.YEAR), startCalendar.get(Calendar.MONTH), startCalendar.get(Calendar.DAY_OF_MONTH)
            );
            dpd.show(activity(HomeActivity.class).getFragmentManager(), "Datepickerdialog");
        });
        endEditText.setOnClickListener(v -> {
            DatePickerDialog dpd = DatePickerDialog.newInstance((view1, year, monthOfYear, dayOfMonth) -> {
                    endCalendar.set(Calendar.YEAR, year);
                    endCalendar.set(Calendar.MONTH, monthOfYear);
                    endCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    endEditText.setText(DateFormatUtils.format(endCalendar.getTime(), getString(R.string.E_dd_MM_yyyy)));
                },  endCalendar.get(Calendar.YEAR), endCalendar.get(Calendar.MONTH), endCalendar.get(Calendar.DAY_OF_MONTH)
            );
            dpd.show(activity(HomeActivity.class).getFragmentManager(), "Datepickerdialog");
        });
        nextButton.setOnClickListener(v -> save(true));
        draftButton.setOnClickListener(v -> save(false));
    }


    public void save(boolean statusPublish){
        if(formMode==ProductForm1Fragment.MODE_ADD) {
            if(expandableRelativeLayout.isExpanded()){
                if (!discountEditText.getText().toString().equals(getString(R.string.nil)) && !discountEditText.getText().toString().equals("")) {
                    if(startCalendar.getTime().getTime()>=endCalendar.getTime().getTime()){
                        Tooltip.TooltipView tooltipView = Tooltip.make(getActivity(), new Tooltip.Builder(101).anchor(dateLayout, Tooltip.Gravity.BOTTOM)
                                .closePolicy(new Tooltip.ClosePolicy().insidePolicy(true, false).outsidePolicy(true, false), 3000)
                                .activateDelay(800).showDelay(300).text(getString(R.string.startdate_should_be_earlier_than_enddate)).withStyleId(R.style.AppTheme_TooltipLayout)
                                .maxWidth(500).withArrow(true).withOverlay(true).floatingAnimation(Tooltip.AnimationBuilder.DEFAULT)
                                .build()
                        );
                        tooltipView.show();
                        return;
                    }
                }
                if (!discountEditText.getText().toString().equals(getString(R.string.nil)) && !discountEditText.getText().toString().equals("")) {
                    boolean priceTooLow = (formProduct.getBasePrice()-((formProduct.getBasePrice()*Integer.valueOf(discountEditText.getText().toString()))/100))< Initial.MIN_PRODUCT_PRICE;
                    if(priceTooLow){
                        activity(HomeActivity.class).showDialog(getString(R.string.rejection), getString(R.string.price_after_discount_below_limitation_validator), (dialog, which) -> dialog.dismiss());
                        return;
                    }
                }

                if (discountEditText.getText().toString().equals(getString(R.string.nil)) || discountEditText.getText().toString().equals("")) {
                    Tooltip.TooltipView tooltipView = Tooltip.make(getActivity(), new Tooltip.Builder(101).anchor(discountEditText, Tooltip.Gravity.BOTTOM)
                            .closePolicy(new Tooltip.ClosePolicy().insidePolicy(true, false).outsidePolicy(true, false), 3000)
                            .activateDelay(800).showDelay(300).text(getString(R.string.discount_should_not_zero)).withStyleId(R.style.AppTheme_TooltipLayout)
                            .maxWidth(500).withArrow(true).withOverlay(true).floatingAnimation(Tooltip.AnimationBuilder.DEFAULT)
                            .build()
                    );
                    tooltipView.show();
                    return;
                }
            }


            activity(HomeActivity.class).showDialog(getString(R.string.confirmation), getString(R.string.add_product_confirmation), (dialog, which) -> {
                dialog.dismiss();
                if (expandableRelativeLayout.isExpanded() && !discountEditText.getText().toString().isEmpty()) {
                    formProduct.setDiscount(Integer.valueOf(discountEditText.getText().toString()));
                    formProduct.setStartDiscount(DateFormatUtils.format(startCalendar.getTime(), getString(R.string.yyyy_MM_dd_hh_mm_ss)));
                    formProduct.setEndDiscount(DateFormatUtils.format(endCalendar.getTime(), getString(R.string.yyyy_MM_dd_hh_mm_ss)));
                }else{
                    formProduct.setDiscount(0);
                    formProduct.setStartDiscount(null);
                    formProduct.setEndDiscount(null);
                }
                formProduct.setPublish(statusPublish);
                String imageFromProducts = formProduct.getProductImage();
                imageFromProducts = StringUtils.remove(imageFromProducts, "]");
                imageFromProducts = StringUtils.remove(imageFromProducts, "[");
                String[] imageFromProductArray = StringUtils.split(imageFromProducts, ",");

                MultipartBody.Part[] parts = new MultipartBody.Part[imageFromProductArray.length];
                for (int i = 0; i < parts.length; i++) {
                    String filename = imageFromProductArray[i].substring(imageFromProductArray[i].lastIndexOf("/"));
                    File file = CommonUtil.openFile(activity(), filename);
                    parts[i] = CommonUtil.generateMultiPart(activity(), "product_images[]", Uri.fromFile(file));
                }
                HttpInstance.withProgress(activity()).observe(HttpInstance.create(ProductService.class)
                        .addProduct(MimeType.valueOf("text/plain").createRequestBody(HttpInstance.getGson().toJson(formProduct)), parts), addProductResponse -> {
                    ResponseTools.validate(activity(), addProductResponse);
                    if (addProductResponse.isStatus()) {
                        WindowFlow.backstack(activity(HomeActivity.class), SellerDashboardFragment.TAG, R.id.fragment_frame);
                    }else{
                        activity(HomeActivity.class).showDialog(getString(R.string.rejection), addProductResponse.getMessage(), (dialog1, which1) -> dialog.dismiss());
                    }
                }, throwable -> activity(HomeActivity.class).showDialog(throwable, (dialog1, which1) -> {}), false);
            }, (dialog, which) -> dialog.dismiss());

        }else{
            if(expandableRelativeLayout.isExpanded()) {
                if (!discountEditText.getText().toString().equals(getString(R.string.nil)) && !discountEditText.getText().toString().equals("")) {
                    if (startCalendar.getTime().getTime() >= endCalendar.getTime().getTime()) {
                        Tooltip.TooltipView tooltipView = Tooltip.make(getActivity(), new Tooltip.Builder(101).anchor(dateLayout, Tooltip.Gravity.BOTTOM)
                                .closePolicy(new Tooltip.ClosePolicy().insidePolicy(true, false).outsidePolicy(true, false), 3000)
                                .activateDelay(800).showDelay(300).text(getString(R.string.startdate_should_be_earlier_than_enddate)).withStyleId(R.style.AppTheme_TooltipLayout)
                                .maxWidth(500).withArrow(true).withOverlay(true).floatingAnimation(Tooltip.AnimationBuilder.DEFAULT)
                                .build()
                        );
                        tooltipView.show();
                        return;
                    }
                }

                if (!discountEditText.getText().toString().equals(getString(R.string.nil)) && !discountEditText.getText().toString().equals("")) {
                    boolean priceTooLow = (formProduct.getBasePrice() - ((formProduct.getBasePrice() * Integer.valueOf(discountEditText.getText().toString())) / 100)) < Initial.MIN_PRODUCT_PRICE;
                    if (priceTooLow) {
                        activity(HomeActivity.class).showDialog(getString(R.string.rejection), getString(R.string.price_after_discount_below_limitation_validator), (dialog, which) -> dialog.dismiss());
                        return;
                    }
                }

                if (discountEditText.getText().toString().equals(getString(R.string.nil)) || discountEditText.getText().toString().equals("")) {
                    Tooltip.TooltipView tooltipView = Tooltip.make(getActivity(), new Tooltip.Builder(101).anchor(discountEditText, Tooltip.Gravity.BOTTOM)
                            .closePolicy(new Tooltip.ClosePolicy().insidePolicy(true, false).outsidePolicy(true, false), 3000)
                            .activateDelay(800).showDelay(300).text(getString(R.string.discount_should_not_zero)).withStyleId(R.style.AppTheme_TooltipLayout)
                            .maxWidth(500).withArrow(true).withOverlay(true).floatingAnimation(Tooltip.AnimationBuilder.DEFAULT)
                            .build()
                    );
                    tooltipView.show();
                    return;
                }
            }

            activity(HomeActivity.class).showDialog(getString(R.string.confirmation), getString(R.string.edit_product_confirmation), (dialog, which) -> {
                if (expandableRelativeLayout.isExpanded() && !discountEditText.getText().toString().isEmpty()) {
                    formProduct.setDiscount(Integer.valueOf(discountEditText.getText().toString()));
                    formProduct.setStartDiscount(DateFormatUtils.format(startCalendar.getTime(), getString(R.string.yyyy_MM_dd_hh_mm_ss)));
                    formProduct.setEndDiscount(DateFormatUtils.format(endCalendar.getTime(), getString(R.string.yyyy_MM_dd_hh_mm_ss)));
                }else{
                    formProduct.setDiscount(0);
                    formProduct.setStartDiscount(null);
                    formProduct.setEndDiscount(null);
                }
                formProduct.setPublish(statusPublish);
                String imageFromProducts = formProduct.getProductImage();
                imageFromProducts = StringUtils.remove(imageFromProducts, "]");
                imageFromProducts = StringUtils.remove(imageFromProducts, "[");
                String[] imageFromProductArray = StringUtils.split(imageFromProducts, ",");

                MultipartBody.Part[] parts = new MultipartBody.Part[imageFromProductArray.length];
                for (int i = 0; i < parts.length; i++) {
                    String filename = imageFromProductArray[i].substring(imageFromProductArray[i].lastIndexOf("/"));
                    File file = CommonUtil.openFile(activity(), filename);
                    parts[i] = CommonUtil.generateMultiPart(activity(), "product_images[]", Uri.fromFile(file));
                }
                HttpInstance.withProgress(activity()).observe(HttpInstance.create(ProductService.class)
                        .editProduct(formProduct.getId(), MimeType.valueOf("text/plain").createRequestBody(HttpInstance.getGson().toJson(formProduct)), parts), addProductResponse -> {
                    ResponseTools.validate(activity(), addProductResponse);
                    if (addProductResponse.isStatus()) {
                        WindowFlow.backstack(activity(HomeActivity.class), SellerDashboardFragment.TAG, R.id.fragment_frame);
                    }else{
                        activity(HomeActivity.class).showDialog(getString(R.string.rejection), addProductResponse.getMessage(), (dialog1, which1) -> dialog.dismiss());
                    }
                }, throwable -> activity(HomeActivity.class).showDialog(throwable, (dialog1, which1) -> {}), false);
            }, (dialog, which) -> dialog.dismiss());

        }
    }

}
