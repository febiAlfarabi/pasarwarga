package com.pasarwarga.market.fragment.asseller.setting;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.alfarabi.alfalibs.adapters.recyclerview.SimpleRecyclerAdapter;
import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.alfarabi.base.fragment.home.BaseDrawerFragment;
import com.alfarabi.base.http.basemarket.service.NoteService;
import com.alfarabi.base.model.shop.Note;
import com.alfarabi.base.tools.ResponseTools;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.holder.recyclerview.asseller.NotesViewHolder;

import java.util.ArrayList;

import butterknife.BindView;
import lombok.Getter;

/**
 * Created by Alfarabi on 6/19/17.
 */

public class NoteSellerSettingFragment extends BaseDrawerFragment {

    public static final String TAG = NoteSellerSettingFragment.class.getName();

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout;
    @BindView(R.id.recyclerview) AlfaRecyclerView recyclerView ;

    @Getter SimpleRecyclerAdapter<Note, NoteSellerSettingFragment, NotesViewHolder> simpleRecyclerAdapter ;

    private String searchInput = "" ;
    private int column = 2 ;

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        simpleRecyclerAdapter = new SimpleRecyclerAdapter<>(this, NotesViewHolder.class, new ArrayList<>());

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        toolbar.inflateMenu(R.menu.seller_notes_menu);
        toolbar.setOnMenuItemClickListener(this);
        simpleRecyclerAdapter.initRecyclerView(recyclerView, new LinearLayoutManager(activity()));


    }

    @Override
    public void onResume() {
        super.onResume();
        basicRequest();
    }

    public void basicRequest(){
        HttpInstance.call(HttpInstance.create(NoteService.class).getNotes(), noteResponse -> {
            ResponseTools.validate(activity(HomeActivity.class), noteResponse);
            if(noteResponse.isStatus()){
                simpleRecyclerAdapter.setObjects(noteResponse.getNotes());
                simpleRecyclerAdapter.notifyItemRangeChanged(0, simpleRecyclerAdapter.getItemCount());
            }
        }, throwable -> {
            activity(HomeActivity.class).showSnackbar(throwable, v -> {});
        }, false);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_seller_notes_list;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public Object getObject() {
        return null;
    }

    @Override
    public String initTitle() {
        title = getString(R.string.notes);
        toolbar.setTitle(title);
        return title;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        if(item.getItemId()==R.id.add_notes){
            WindowFlow.goTo(activity(HomeActivity.class), NotesDetailFragment.TAG, R.id.fragment_frame, true, false);
        }
        return false;
    }

    @Override
    public int getMenuId() {
        return 0;
    }

    @Override
    public boolean keepSideMenu() {
        return false;
    }
}
