package com.pasarwarga.market.fragment.asseller.setting;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.fragment.home.BaseDrawerFragment;
import com.alfarabi.base.http.basemarket.service.NoteService;
import com.alfarabi.base.model.shop.Note;
import com.alfarabi.base.tools.Initial;
import com.alfarabi.base.tools.InputTools;
import com.alfarabi.base.tools.ResponseTools;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;

import org.parceler.Parcels;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotesDetailFragment extends BaseDrawerFragment {

    public static final String TAG = NotesDetailFragment.class.getName();

    @BindView(R.id.toolbar) Toolbar toolbar ;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout;
    @BindView(R.id.title_et) EditText titleEditText ;
    @BindView(R.id.content_et) EditText contentEditText ;
    @BindView(R.id.submit_btn) Button submitButton ;

    Note note;

    public static NotesDetailFragment instance(){
        NotesDetailFragment registrationForm1Fragment = new NotesDetailFragment();
        return registrationForm1Fragment;
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public int getMenuId() {
        return 0;
    }

    @Override
    public String getTAG() {
        return TAG ;
    }

    @Override
    public String initTitle() {
        title = getString(R.string.write_notes);
        toolbar.setTitle(title);
        return title;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_notes_detail_form;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if(getArguments()!=null && getArguments().containsKey(Initial.NOTES)){
            note = Parcels.unwrap(getArguments().getParcelable(Initial.NOTES));
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(note !=null){
            titleEditText.setText(note.getTitle());
            contentEditText.setText(note.getContent());
        }

        submitButton.setOnClickListener(v -> {
            if(InputTools.isComplete(titleEditText, contentEditText)){
                activity(HomeActivity.class).showDialog(getString(R.string.confirmation), getString(R.string.seller_notes_confirmation), (dialog1, which1) -> {
                    if(note ==null){
                        // Create new Address
                        note = new Note();
                        note.setTitle(titleEditText.getText().toString());
                        note.setContent(contentEditText.getText().toString());
                        HttpInstance.withProgress(activity()).observe(HttpInstance.create(NoteService.class).create(note.toHashMap()), get -> {
                            ResponseTools.validate(activity(), get);
                            if(get.isStatus()){
                                WindowFlow.backstack(activity(HomeActivity.class));
                            }else{
                                activity(HomeActivity.class).showDialog(get.getMessage(), (dialog, which) -> dialog.dismiss());
                            }
                        }, throwable -> activity(HomeActivity.class).showSnackbar(throwable, v1 -> {}), false);
                    }else{
                        note.setTitle(titleEditText.getText().toString());
                        note.setContent(contentEditText.getText().toString());
                        HttpInstance.withProgress(activity()).observe(HttpInstance.create(NoteService.class).update(note.getId(), note.toHashMap()), get -> {
                            ResponseTools.validate(activity(), get);
                            if(get.isStatus()){
                                WindowFlow.backstack(activity(HomeActivity.class));
                            }else{
                                activity(HomeActivity.class).showDialog(get.getMessage(), (dialog, which) -> dialog.dismiss());
                            }
                        }, throwable -> activity(HomeActivity.class).showSnackbar(throwable, v1 -> {}), false);
                    }
                    dialog1.dismiss();
                }, (dialog1, which1) -> dialog1.dismiss());

            }
        });


    }

    @Override
    public Object getObject() {
        return note;
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

}
