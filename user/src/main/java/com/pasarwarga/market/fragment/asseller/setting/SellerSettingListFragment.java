package com.pasarwarga.market.fragment.asseller.setting;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.alfarabi.alfalibs.adapters.recyclerview.VerticalRecyclerAdapter;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.alfarabi.base.fragment.home.BaseDrawerFragment;
import com.alfarabi.base.model.shop.SellerDashboard;
import com.hendraanggrian.bundler.annotations.BindExtra;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.holder.recyclerview.asseller.SettingViewHolder;

import java.util.Arrays;

import butterknife.BindView;
import lombok.Getter;

public class SellerSettingListFragment extends BaseDrawerFragment {
    
    public static final String TAG = SellerSettingListFragment.class.getName();


    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout;
    @BindView(R.id.recyclerview) AlfaRecyclerView recyclerView ;
    @Getter VerticalRecyclerAdapter verticalRecyclerAdapter ;

    @BindExtra SellerDashboard sellerDashboard ;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setObject(sellerDashboard);
        verticalRecyclerAdapter = new VerticalRecyclerAdapter(this, SettingViewHolder.class, Arrays.asList(activity(HomeActivity.class).getResources().getStringArray(R.array.seller_settings_array)));
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        verticalRecyclerAdapter.initRecyclerView(recyclerView, new LinearLayoutManager(activity()));
    }

    @Override
    public String initTitle() {
        title = getString(R.string.settings);
        toolbar.setTitle(title);
        return title;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_seller_setting_list;
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public int getMenuId() {
        return 0;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }
}
