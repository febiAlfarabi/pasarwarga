package com.pasarwarga.market.fragment.asseller.setting;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;


import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.WLog;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.fragment.BaseUserFragment;
import com.alfarabi.base.http.basemarket.service.CityService;
import com.alfarabi.base.http.basemarket.service.SellerService;
import com.alfarabi.base.model.ShopLocation;
import com.alfarabi.base.model.location.City;
import com.alfarabi.base.model.location.District;
import com.alfarabi.base.model.location.Province;
import com.alfarabi.base.model.location.ZipCode;
import com.alfarabi.base.model.shop.SellerDashboard;
import com.alfarabi.base.tools.InputTools;
import com.alfarabi.alfalibs.tools.KeyboardUtils;
import com.alfarabi.base.tools.ResponseTools;
import com.alfarabi.base.view.DataMaterialSpinner;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.google.gson.Gson;
import com.hendraanggrian.bundler.annotations.BindExtra;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;

import java.util.List;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ShippingSellerSettingFragment extends BaseUserFragment<SellerDashboard> {
    
    public static final String TAG = ShippingSellerSettingFragment.class.getName();

    @BindView(R.id.toolbar) Toolbar toolbar;

    @BindView(R.id.address_et) EditText addressEditText ;
    @BindView(R.id.province_sp) DataMaterialSpinner<Province> provinceSpinner ;
    @BindView(R.id.city_sp) DataMaterialSpinner<City> citySpinner ;
    @BindView(R.id.district_sp) DataMaterialSpinner<District> districtSpinner ;
    @BindView(R.id.postalcode_et) AutoCompleteTextView postalCodeEditText ;

    @BindView(R.id.store_location_expander) LinearLayout storeLocationExpanderButton ;
    @BindView(R.id.pickup_location_expander) LinearLayout pickupLocationExpanderButton ;
    @BindView(R.id.shipping_method_expander) LinearLayout shippingMethodExpanderButton ;

    @BindView(R.id.expandable_layout) ExpandableRelativeLayout expandableRelativeLayout;
    @BindView(R.id.pickup_expandable_layout) ExpandableRelativeLayout pickupExpandableRelativeLayout;
    @BindView(R.id.shipping_expandable_layout) ExpandableRelativeLayout shippingExpandableRelativeLayout;

    @BindView(R.id.submit_button) Button submitButton ;
    @BindExtra SellerDashboard sellerDashboard ;

    boolean firstOpen = true ;

    @Override
    public String initTitle() {
        return null;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_shipping_seller_setting;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pickupExpandableRelativeLayout.collapse();
        shippingExpandableRelativeLayout.collapse();
        toolbar.setTitle(getString(R.string.shipping_configuration));
        toolbar.setNavigationOnClickListener(v -> WindowFlow.backstack(activity(HomeActivity.class)));

//        messageButton.setOnClickListener(v -> {
//            WindowFlow.goTo(activity(HomeActivity.class), SendMessageFragment.TAG, R.id.fragment_frame, true, false);
//        });

        storeLocationExpanderButton.setOnClickListener(v -> {
            if(expandableRelativeLayout.isExpanded()){
                expandableRelativeLayout.collapse();
            }else{
                expandableRelativeLayout.expand();
            }
        });
        pickupLocationExpanderButton.setOnClickListener(v -> {
            if(pickupExpandableRelativeLayout.isExpanded()){
                pickupExpandableRelativeLayout.collapse();
            }else{
                pickupExpandableRelativeLayout.expand();
            }
        });

        shippingMethodExpanderButton.setOnClickListener(v -> {
            if(shippingExpandableRelativeLayout.isExpanded()){
                shippingExpandableRelativeLayout.collapse();
            }else{
                shippingExpandableRelativeLayout.expand();
            }
        });

    }

    public void renderData(){
        if(sellerDashboard!=null){
            addressEditText.setText(sellerDashboard.getSellerInfo().getShopLocation().getAddress());
            provinceSpinner.setSelectedDsi(sellerDashboard.getSellerInfo().getShopLocation().getProvince());
            citySpinner.setSelectedDsi(sellerDashboard.getSellerInfo().getShopLocation().getCity());
            districtSpinner.setSelectedDsi(sellerDashboard.getSellerInfo().getShopLocation().getDistrict());
            postalCodeEditText.setText(sellerDashboard.getSellerInfo().getShopLocation().getPostalCode());
        }

        submitButton.setOnClickListener(v -> {
            if(!InputTools.isComplete(addressEditText, provinceSpinner, citySpinner, districtSpinner, postalCodeEditText)){

                return;
            }
            activity(HomeActivity.class).showDialog(getString(R.string.confirmation), getString(R.string.update_address_confirmation), (dialog1, which1) -> {
                if(sellerDashboard.getSellerInfo().getShopLocation()==null){
                    // Create new Address
                    ShopLocation shopLocation = new ShopLocation();
                    shopLocation.setAddress(addressEditText.getText().toString());
                    shopLocation.setProvince(provinceSpinner.getSelectedDsi());
                    shopLocation.setCity(citySpinner.getSelectedDsi());
                    shopLocation.setDistrict(districtSpinner.getSelectedDsi());
                    shopLocation.setPostalCode(postalCodeEditText.getText().toString());

                    HttpInstance.withProgress(activity()).observe(HttpInstance.create(SellerService.class).updateShopLocation(shopLocation.toHashMap()), response -> {
                        ResponseTools.validate(activity(), response);
                        if(response.isStatus()){
                            sellerDashboard.getSellerInfo().setShopLocation(response.getShopLocation());
//                            RealmDao.instance().insertOrUpdate(sellerDashboard);
                            activity(HomeActivity.class).showDialog(getString(R.string.successfully_updated), getString(R.string.location_successfully_updated), (dialog, which) -> {
                                WindowFlow.backstack(activity(HomeActivity.class));
                            });
                        }else{
                            activity(HomeActivity.class).showDialog(response.getMessage(), (dialog, which) -> dialog.dismiss());
                        }
                    }, throwable -> {
                        activity(HomeActivity.class).showSnackbar(throwable, v1 -> {});
                    });
                }else{
                    ShopLocation shopLocation = sellerDashboard.getSellerInfo().getShopLocation();
                    shopLocation.setAddress(addressEditText.getText().toString());
                    shopLocation.setProvince(provinceSpinner.getSelectedDsi());
                    shopLocation.setCity(citySpinner.getSelectedDsi());
                    shopLocation.setDistrict(districtSpinner.getSelectedDsi());
                    shopLocation.setPostalCode(postalCodeEditText.getText().toString());
                    HttpInstance.withProgress(activity()).observe(HttpInstance.create(SellerService.class).updateShopLocation(shopLocation.toHashMap()), response -> {
                        ResponseTools.validate(activity(), response);
                        if(response.isStatus()){
                            sellerDashboard.getSellerInfo().setShopLocation(response.getShopLocation());
//                            RealmDao.instance().insertOrUpdate(sellerDashboard);

                            activity(HomeActivity.class).showDialog(getString(R.string.successfully_updated), getString(R.string.location_successfully_updated), (dialog, which) -> {
                                WindowFlow.backstack(activity(HomeActivity.class));
                            });
                        }else{
                            activity(HomeActivity.class).showDialog(response.getMessage(), (dialog, which) -> dialog.dismiss());
                        }
                    }, throwable -> {
                        activity(HomeActivity.class).showSnackbar(throwable, v1 -> {});
                    });
                }
            }, (dialog1, which1) -> dialog1.dismiss());

        });

    }

    @Override
    public void onResume() {
        super.onResume();
        toolbar.postDelayed(() -> {
            expandableRelativeLayout.expand();
        }, 500);
        basicRequest();
    }

    public void basicRequest(){
        HttpInstance.withProgress(activity()).observe(HttpInstance.create(SellerService.class).sellerDetail(Long.valueOf(myProfile.getUser().getSellerInfo().getId())), detailResponse -> {
            ResponseTools.validate(activity(HomeActivity.class), detailResponse, true);
            this.sellerDashboard = detailResponse.getSellerDashboard();
            requestProvince();
        }, throwable -> {
            activity(HomeActivity.class).showSnackbar(throwable, v -> {});
        }, false);
    }

    public void requestProvince(){
        HttpInstance.call(HttpInstance.create(CityService.class).getProvinces(91), provinceResponse -> {
            ResponseTools.validate(getActivity(), provinceResponse);
            if(provinceResponse.isStatus() && provinceResponse.getProvinces().size()>0) {
                provinceSpinner.initDataAdapter(provinceResponse.getProvinces(), (parent, view1, position, id) -> {
//                    WLog.i(TAG, new Gson().toJson(provinceSpinner.getSelectedDsi()));
                    requestCity(Integer.valueOf(provinceSpinner.getSelectedDsi().getId()));
                }, provinceResponse.getProvinces().get(0));
                requestCity(Integer.valueOf(provinceSpinner.getSelectedDsi().getId()));
            }
        }, throwable -> {
            throwable.printStackTrace();
        }, false);
    }

    public void requestCity(long provinceId){
        HttpInstance.call(HttpInstance.create(CityService.class).getCities(provinceId), cityResponse -> {
            ResponseTools.validate(getActivity(), cityResponse);
            if(cityResponse.isStatus() && cityResponse.getCities().size()>0){
                citySpinner.initDataAdapter(cityResponse.getCities(), (parent, view, position, id) -> {
                    requestDistrict(Integer.valueOf(citySpinner.getSelectedDsi().getId()));
                }, cityResponse.getCities().get(0));
                requestDistrict(Integer.valueOf(citySpinner.getSelectedDsi().getId()));
            }
        }, throwable -> {
            throwable.printStackTrace();
            activity(HomeActivity.class).showSnackbar(throwable.getMessage(), v -> {});
        }, false);
    }

    public void requestDistrict(long cityId){
        HttpInstance.call(HttpInstance.create(CityService.class).getKecamatan(cityId), districtResponse -> {
            ResponseTools.validate(getActivity(), districtResponse);
            if(districtResponse.isStatus() && districtResponse.getDistricts().size()>0) {
                districtSpinner.initDataAdapter(districtResponse.getDistricts(), (parent, view1, position, id) -> {
                    if (districtSpinner.getSelectedDsi() != null) {
//                    requestSubdistrict(Long.valueOf(districtSpinner.getSelectedDsi().getId()));
                        requestZipcode(Long.valueOf(districtSpinner.getSelectedDsi().getId()));
                    }
                }, districtResponse.getDistricts().get(0));
                requestZipcode(Long.valueOf(districtSpinner.getSelectedDsi().getId()));
            }
        }, throwable -> {
            throwable.printStackTrace();
            activity(HomeActivity.class).showSnackbar(throwable.getMessage(), v -> {});
        }, false);
    }

    private ArrayAdapter<String> postCodeAdapter ;
    private String[] postCodes ;
    private ZipCode zipCode ;
    List<ZipCode> zipCodes ;
    public void requestZipcode(long districtId){
        HttpInstance.call(HttpInstance.create(CityService.class).getZipCodeByDistrict(districtId), cityResponse -> {
            ResponseTools.validate(getActivity(), cityResponse);
            zipCodes = cityResponse.getZipCodes();
            postCodes = new String[zipCodes.size()];
            for (int i = 0; i<zipCodes.size(); i++) {
               postCodes[i] = zipCodes.get(i).getZipcode();
            }
            postCodeAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.select_dialog_item, postCodes);
            postalCodeEditText.setThreshold(1);
            postalCodeEditText.setAdapter(postCodeAdapter);
            postalCodeEditText.setOnItemClickListener((parent, view1, position, id) -> {
                if(zipCode!=null && zipCode.getZipcode().equalsIgnoreCase(postalCodeEditText.getText().toString())){
                    return;
                }
                for (int i = 0; i < zipCodes.size(); i++) {
                    if(zipCodes.get(i).getZipcode().equalsIgnoreCase(postalCodeEditText.getText().toString())){
                        zipCode = zipCodes.get(i);
                    }
                }
                if(zipCode!=null){
                    WLog.i(TAG, "CITY :::: "+new Gson().toJson(zipCode));
                    KeyboardUtils.hideKeyboard(getActivity());
                }
            });
            zipCode = zipCodes.get(0);
            postalCodeEditText.setText(zipCode.getZipcode());
            if(firstOpen){
                firstOpen = false ;
                renderData();
            }
        }, throwable -> {
            throwable.printStackTrace();
            activity(HomeActivity.class).showSnackbar(throwable.getMessage(), v -> {});
        }, false);
    }
}
