package com.pasarwarga.market.fragment.asseller.transactiontab;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.SimpleRecyclerAdapter;
import com.alfarabi.alfalibs.helper.SwipeCallback;
import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.alfarabi.alfalibs.views.AlfaSwipeRefreshLayout;
import com.alfarabi.base.fragment.BaseUserFragment;
import com.alfarabi.base.http.basemarket.service.OrderService;
import com.alfarabi.base.model.shop.Order;
import com.alfarabi.base.model.shop.SellerDashboard;
import com.alfarabi.base.model.transaction.SummaryStatus;
import com.alfarabi.base.realm.RealmDao;
import com.alfarabi.base.tools.Initial;
import com.alfarabi.base.tools.ResponseTools;
import com.hendraanggrian.bundler.Bundler;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.fragment.asseller.transactiontab.detail.OrderStatusDetailFragment;
import com.pasarwarga.market.holder.recyclerview.asseller.SellerOrderViewHolder;
import com.tuyenmonkey.mkloader.MKLoader;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class TransactionListFragment extends BaseUserFragment<SellerDashboard> implements TabFragmentCallback {


    @BindView(R.id.swiperefreshlayout) AlfaSwipeRefreshLayout alfaSwipeRefreshLayout ;
    @BindView(R.id.recyclerview) AlfaRecyclerView<Order> recyclerView ;
    @BindView(R.id.loader) MKLoader loader;

    @BindView(R.id.new_transaction_tv) TextView newTransactionTextView ;
    @BindView(R.id.total_transaction_qount_tv) TextView totalTransactionTextView ;
    @BindView(R.id.total_transaction_price_tv) TextView totalTransactionPriceTextView ;

    private List<Order> orders ;
    private SimpleRecyclerAdapter<Order, TransactionListFragment, SellerOrderViewHolder> simpleRecyclerAdapter ;
    private SummaryStatus summaryStatus ;

    @Override
    public String initTitle() {
        return null;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_transaction_list;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        orders = new ArrayList<>();
        summaryStatus = RealmDao.instance().get(SummaryStatus.class);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        simpleRecyclerAdapter = new SimpleRecyclerAdapter<Order, TransactionListFragment, SellerOrderViewHolder>(this, SellerOrderViewHolder.class, orders);
        simpleRecyclerAdapter.initRecyclerView(recyclerView, new LinearLayoutManager(activity()));
        alfaSwipeRefreshLayout.setOnSwipeListener(new SwipeCallback() {
            @Override
            public void onSwipeFromTop() {
                basicRequest(alfaSwipeRefreshLayout.startPage());
            }

            @Override
            public void onSwipeFromBottom() {
                basicRequest(alfaSwipeRefreshLayout.increasePage());
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
//        loader.setVisibility(MKLoader.VISIBLE);
//        basicRequest(alfaSwipeRefreshLayout.startPage());
        renderSummary();
    }

    public void basicRequest(int page){
        HttpInstance.call(HttpInstance.create(OrderService.class).getOrders(page, Initial.DELIVERED), orderListResponse -> {
            ResponseTools.validate(activity(), orderListResponse);
            loader.setVisibility(View.GONE);
            alfaSwipeRefreshLayout.finishRefresh();
            alfaSwipeRefreshLayout.finishRefreshLoadMore();
            if(orderListResponse.isStatus()){
                if(page==1){
                    orders.clear();
                }
                orders.addAll(orderListResponse.getOrders());
                simpleRecyclerAdapter.setObjects(orders);
            }else{
                activity(HomeActivity.class).showSnackbar(orderListResponse.getMessage(), v -> {});
            }
            summaryRequest();
        }, throwable -> {
            activity(HomeActivity.class).showSnackbar(throwable, v -> {});
            loader.setVisibility(View.GONE);
            alfaSwipeRefreshLayout.finishRefresh();
            alfaSwipeRefreshLayout.finishRefreshLoadMore();
        }, false);

    }

    public void summaryRequest(){
        HttpInstance.call(HttpInstance.create(OrderService.class).getSummaryStatus(),  summaryStatusResponse -> {
            ResponseTools.validate(activity(), summaryStatusResponse);
            if(summaryStatusResponse.isStatus()) {
                summaryStatus = summaryStatusResponse.getSummaryStatus();
                summaryStatus.setId(1);
                summaryStatus = RealmDao.instance().renew(summaryStatus.getId(), summaryStatus);
            }
//            }else{
//                activity(HomeActivity.class).showSnackbar(summaryStatusResponse.getMessage(), v -> {});
//            }
            renderSummary();
        }, throwable -> {
            activity(HomeActivity.class).showSnackbar(throwable, v -> {});
            loader.setVisibility(View.GONE);
            alfaSwipeRefreshLayout.finishRefresh();
            alfaSwipeRefreshLayout.finishRefreshLoadMore();
        }, false);

    }

    public void renderSummary(){
        if(summaryStatus!=null){
            newTransactionTextView.setText(String.valueOf(summaryStatus.getDelivered().getCount()));
            totalTransactionTextView.setText(String.valueOf(summaryStatus.getTotalTransaction()));
            totalTransactionPriceTextView.setText(getString(R.string.curr_symb)+NumberFormat.getInstance().format(summaryStatus.getTotalTransactionPrice()));

        }
    }

    @Override
    public void onTabShowed() {
        if(alfaSwipeRefreshLayout.getPage()==1 && simpleRecyclerAdapter.getObjects().size()>0){
            return;
        }
        loader.setVisibility(MKLoader.VISIBLE);
        basicRequest(alfaSwipeRefreshLayout.startPage());
    }

    public View.OnClickListener onClickListener = v -> {
        final Order order = (Order) v.getTag();
        HttpInstance.withProgress(activity()).observe(HttpInstance.create(OrderService.class).getOrderDetail(order.getId()), orderDetailResponse -> {
            ResponseTools.validate(activity(HomeActivity.class), orderDetailResponse);
            if(orderDetailResponse.isStatus()){
                WindowFlow.goTo(activity(HomeActivity.class), OrderStatusDetailFragment.TAG, Bundler.wrap(OrderStatusDetailFragment.class, order, orderDetailResponse.getOrderDetail()), R.id.fragment_frame, true);
            }else{
                activity(HomeActivity.class).showSnackbar(orderDetailResponse.getMessage(), v1 -> {});
            }
        },throwable -> {
            activity(HomeActivity.class).showSnackbar(throwable, v1 -> {});
        }, false);
    };

}
