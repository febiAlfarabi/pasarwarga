package com.pasarwarga.market.fragment.asseller.transactiontab.detail;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.alfarabi.alfalibs.adapters.recyclerview.VerticalRecyclerAdapter;
import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.alfarabi.base.fragment.BaseUserFragment;
import com.alfarabi.base.http.basemarket.service.OrderService;
import com.alfarabi.base.http.basemarket.service.ShippingService;
import com.alfarabi.base.model.Shipping;
import com.alfarabi.base.model.shop.Order;
import com.alfarabi.base.model.shop.OrderDetail;
import com.alfarabi.base.model.shop.SellerDashboard;
import com.alfarabi.base.tools.Initial;
import com.alfarabi.base.tools.InputTools;
import com.alfarabi.base.tools.ResponseTools;
import com.github.florent37.viewtooltip.ViewTooltip;
import com.hendraanggrian.bundler.annotations.BindExtra;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.holder.recyclerview.asseller.ShippingOrderRadioViewHolder;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import lombok.Getter;
import lombok.Setter;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrderShippingFragment extends BaseUserFragment<SellerDashboard> {
    
    
    public static final String TAG = OrderShippingFragment.class.getName();
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.tracking_code_et) EditText trackingCodeEditText ;
    @BindView(R.id.submit_button) Button submitButton ;
    @BindView(R.id.recyclerview) AlfaRecyclerView<Shipping> recyclerView ;
    @Getter@Setter int selectedIndex = -1 ;

//    private ButtonOptionAdapter<Shipping> buttonOptionAdapter ;

    VerticalRecyclerAdapter<OrderShippingFragment, ShippingOrderRadioViewHolder, Shipping> recyclerAdapter ;

    @BindExtra
    Order order ;
    @BindExtra OrderDetail orderDetail ;

    @Override
    public String initTitle() {
        title = getString(R.string.shipping_option);
        return title;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_order_shipping;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        recyclerAdapter = new VerticalRecyclerAdapter<>(this, ShippingOrderRadioViewHolder.class, new ArrayList<>());

//        buttonOptionAdapter = new ButtonOptionAdapter<>(activity(), new ArrayList<>());
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        toolbar.setNavigationOnClickListener(v -> WindowFlow.backstack(activity(HomeActivity.class)));
        recyclerAdapter.initRecyclerView(recyclerView, new LinearLayoutManager(activity()));

//        recyclerView.setLayoutManager(new LinearLayoutManager(activity()));
//        recyclerView.setAdapter(buttonOptionAdapter);

        submitButton.setOnClickListener(v -> {
            if(selectedIndex==-1){
                ViewTooltip.on(submitButton).autoHide(true, 3000)
                        .corner(30).textColor(submitButton.getResources().getColor(R.color.white)).color(submitButton.getResources().getColor(R.color.red_700)).position(ViewTooltip.Position.BOTTOM)
                        .text(submitButton.getResources().getString(R.string.error_field_required))
                        .show();
                return;
            }
            if(InputTools.isComplete(trackingCodeEditText)){
                activity(HomeActivity.class).showDialog(getString(R.string.confirmation), getString(R.string.confirmation_ready_to_shipping), (dialog, which) -> {
                dialog.dismiss();
//                new MaterialDialog.Builder(activity()).title(R.string.input_receipt_number)
//                        .content(R.string.receipt_number).inputType(InputType.TYPE_CLASS_TEXT)
//                        .input("", "", false, (dialog1, input) -> {
//                            if(input.toString().isEmpty()){
//                                return;
//                            }
//
//                        }).show();
                    HashMap<String, String> hashMap = new HashMap<String, String>();
                    hashMap.put("shipping_status", Initial.SHIPPED);
                    hashMap.put("shipp_code", orderDetail.getShippCode());
                    hashMap.put("tracking_id", recyclerAdapter.getObjects().get(selectedIndex).getCode());
                    hashMap.put("message", "");
                    HttpInstance.withProgress(activity()).observe(HttpInstance.create(OrderService.class).changeOrderStatus(order.getId() , hashMap), changeStatusResponse -> {
                        ResponseTools.validate(activity(), changeStatusResponse);
                        if(changeStatusResponse.isStatus()){
                            activity(HomeActivity.class).showDialog(changeStatusResponse.getMessage(), (dialog2, which1) -> {
                                dialog.dismiss();
                                WindowFlow.backstack(activity(HomeActivity.class));
                            });
                        }else{
                            activity(HomeActivity.class).showDialog(changeStatusResponse.getMessage(), (dialog2, which1) -> dialog2.show());
                        }
                    }, throwable -> {
                        activity(HomeActivity.class).showSnackbar(throwable, v1 -> {});
                    }, false);
            }, (dialog, which) -> {
                dialog.dismiss();
            });
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        getShipping(String.valueOf(orderDetail.getProduct().getId()), orderDetail.getShippingAddress().getId());
//        HttpInstance.withProgress(activity()).observe(HttpInstance.create(ShippingService.class).shippingSevices(), shippingResponse -> {
//            ResponseTools.validate(activity(), shippingResponse);
//            if(shippingResponse.isStatus()){
//                buttonOptionAdapter.setmItems(shippingResponse.getShippings());
//            }else{
//                activity(HomeActivity.class).showDialog(shippingResponse.getMessage(), (dialog, which) -> dialog.dismiss());
//            }
//        }, throwable -> {
//            activity(HomeActivity.class).showSnackbar(throwable, v -> {});
//        }, false);

    }

//    public void getShipping(String productId){
//        HttpInstance.withProgress(activity()).observe(HttpInstance.create(ShippingService.class).availableShipping(productId), shippingResponse -> {
//            ResponseTools.validate(activity(), shippingResponse);
//            if(shippingResponse.isStatus()){
//                recyclerAdapter.setObjects(shippingResponse.getShippings());
////                buttonOptionAdapter.setmItems(shippingResponse.getShippings());
//            }else{
//                activity(HomeActivity.class).showDialog(shippingResponse.getMessage(), (dialog, which) -> dialog.dismiss());
//            }
//        }, throwable -> {
//            activity(HomeActivity.class).showSnackbar(throwable, v -> {});
//        }, false);
//    }

    public void getShipping(String productId, String addressId){
        HttpInstance.withProgress(activity()).observe(HttpInstance.create(ShippingService.class).availableShipping(productId, addressId), shippingResponse -> {
            ResponseTools.validate(activity(), shippingResponse);
            if(shippingResponse.isStatus()){
                recyclerAdapter.setObjects(shippingResponse.getShippings());
//                buttonOptionAdapter.setmItems(shippingResponse.getShippings());
            }else{
                activity(HomeActivity.class).showDialog(shippingResponse.getMessage(), (dialog, which) -> dialog.dismiss());
            }
        }, throwable -> {
            activity(HomeActivity.class).showSnackbar(throwable, v -> {});
        }, false);
    }

}
