package com.pasarwarga.market.fragment.asseller.transactiontab.detail;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.SimpleRecyclerAdapter;
import com.alfarabi.alfalibs.helper.SwipeCallback;
import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.alfalibs.tools.WLog;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.alfarabi.alfalibs.views.AlfaSwipeRefreshLayout;
import com.alfarabi.base.BaseApplication;
import com.alfarabi.base.fragment.BaseUserFragment;
import com.alfarabi.base.http.basemarket.service.TrackingService;
import com.alfarabi.base.model.User;
import com.alfarabi.base.model.shop.Order;
import com.alfarabi.base.model.shop.OrderDetail;
import com.alfarabi.base.model.shop.SellerDashboard;
import com.alfarabi.base.model.transaction.HistoryTracking;
import com.alfarabi.base.realm.RealmDao;
import com.alfarabi.base.tools.Initial;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.hendraanggrian.bundler.Bundler;
import com.hendraanggrian.bundler.annotations.BindExtra;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
//import com.pasarwarga.market.fragment.PDFFragment;
import com.pasarwarga.market.fragment.WebViewFragment;
import com.pasarwarga.market.fragment.chat.ConversationListFragment;
import com.pasarwarga.market.fragment.store.profile.SendMessageFragment;
import com.pasarwarga.market.holder.recyclerview.TrackingViewHolder;
import com.pasarwarga.market.tools.SystemApp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrderStatusDetailFragment extends BaseUserFragment<SellerDashboard> {
    
    public static final String TAG = OrderStatusDetailFragment.class.getName();

    @BindView(R.id.toolbar) Toolbar toolbar;

    @BindView(R.id.transaction_date_tv) TextView transactionDateTextView ;
    @BindView(R.id.username_tv) TextView usernameTextView ;
    @BindView(R.id.invoice_code_tv) TextView invoiceCodeTextView ;
    @BindView(R.id.invoice_layout) CardView invoiceCardView ;
    @BindView(R.id.product_iv) ImageView productImageView ;
    @BindView(R.id.product_name_tv) TextView productNameTextView ;
    @BindView(R.id.product_price_tv) TextView productPriceTextView ;
    @BindView(R.id.quantity_tv) TextView quantityTextView ;
    @BindView(R.id.shipping_cost_tv) TextView shippingCostTextView ;
    @BindView(R.id.insurance_cost_tv) TextView insuranceCostTextView ;
    @BindView(R.id.total_price_tv) TextView totalPriceTextView ;
    @BindView(R.id.destination_address_tv) TextView destinationAddressTextView ;
    @BindView(R.id.shipping_media_tv) TextView shippingMediaTextView ;
    @BindView(R.id.detail_button) LinearLayout detailButton ;
    @BindView(R.id.expandable_layout) ExpandableRelativeLayout expandableRelativeLayout;

//    @BindView(R.id.tracking_button) LinearLayout trackingButton ;
//    @BindView(R.id.tracker_expandable_layout) ExpandableRelativeLayout trackerExpandableRelativeLayout;

    @BindView(R.id.swiperefreshlayout) AlfaSwipeRefreshLayout swipeRefreshLayout ;
    @BindView(R.id.recyclerview) AlfaRecyclerView<HistoryTracking> recyclerView ;


    @BindView(R.id.message_button) Button messageButton ;
    @BindView(R.id.submit_button) Button submitButton ;

    @Nullable@BindExtra Order order;
    @Nullable@BindExtra OrderDetail orderDetail;

    private SimpleRecyclerAdapter<HistoryTracking, OrderStatusDetailFragment, TrackingViewHolder> simpleRecyclerAdapter ;
    private List<HistoryTracking> historyTrackings = new ArrayList<>();


    @Override
    public String initTitle() {
        return null;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_order_status_detail;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        simpleRecyclerAdapter = new SimpleRecyclerAdapter<>(this, TrackingViewHolder.class, historyTrackings);
        simpleRecyclerAdapter.initRecyclerView(recyclerView, new LinearLayoutManager(activity()));

        expandableRelativeLayout.expand();
//        trackerExpandableRelativeLayout.collapse();
        toolbar.setTitle(getString(R.string.order_detail));
        toolbar.setNavigationOnClickListener(v -> WindowFlow.backstack(activity(HomeActivity.class)));
        transactionDateTextView.setText(order.getTransactionDate());
        usernameTextView.setText(orderDetail.getBuyer().getFirstName());
        invoiceCodeTextView.setText("#"+getString(R.string.invoice)+" "+order.getDealCodeNumber());
        GlideApp.with(this).load(order.getProductImage()).placeholder(R.drawable.ic_product_default).error(R.drawable.ic_product_default).diskCacheStrategy(DiskCacheStrategy.ALL)
                .skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL).into(productImageView);
        productNameTextView.setText(orderDetail.getProduct().getName());
        productPriceTextView.setText(getString(R.string.curr_symb)+""+java.text.DecimalFormat.getInstance().format(orderDetail.getProduct().getBasePrice()));
        quantityTextView.setText(getString(R.string.quantity)+" : "+String.valueOf(orderDetail.getQuantity()));
        shippingCostTextView.setText(getString(R.string.curr_symb)+""+java.text.DecimalFormat.getInstance().format(orderDetail.getShippingCost()));
        if(order.isInsurance()){
            insuranceCostTextView.setText(getString(R.string.curr_symb)+""+java.text.DecimalFormat.getInstance().format(order.getAmountInsurance()));
        }
        totalPriceTextView.setText(getString(R.string.curr_symb)+""+java.text.DecimalFormat.getInstance().format(orderDetail.getTotal()));
//        destinationAddressTextView.setText(order);
        shippingMediaTextView.setText(orderDetail.getShippCode());
        messageButton.setOnClickListener(v -> {
            User user = RealmDao.unmanage(orderDetail.getBuyer());
            if(SystemApp.checkLogin(activity(HomeActivity.class), true)){
                WindowFlow.goTo(activity(HomeActivity.class), ConversationListFragment.TAG,
                        Bundler.wrap(ConversationListFragment.class,
                                ConversationListFragment.buildConversation(user)),R.id.fragment_frame, true, false);
            }
//            WindowFlow.goTo(activity(HomeActivity.class), SendMessageFragment.TAG, R.id.fragment_frame, true, false);
        });

        if(orderDetail.getShippingAddress()!=null){
            destinationAddressTextView.setText(orderDetail.getShippingAddress().getAddress1()
                    + "\n" + orderDetail.getShippingAddress().getAddress2()
                    + "\n" + orderDetail.getShippingAddress().getCountry()
                    + "\n" + orderDetail.getShippingAddress().getCity()
                    + "\n" + orderDetail.getShippingAddress().getPostalCode()
                    + "\n" + orderDetail.getShippingAddress().getPhone());
        }
        invoiceCardView.setOnClickListener(v -> {
            WLog.i(TAG, HttpInstance.getGson().toJson(orderDetail));
            WindowFlow.goTo(activity(HomeActivity.class), WebViewFragment.TAG,
                    Bundler.wrap(WebViewFragment.class, BaseApplication.baseDomain+Initial.INVOICE_ENDPOINT+String.valueOf(order.getDealCodeNumber()), orderDetail.getDealCodeNumber()), R.id.fragment_frame, true, false);
//            WindowFlow.goTo(activity(HomeActivity.class), PDFFragment.TAG, Bundler.wrap(PDFFragment.class, orderDetail), R.id.fragment_frame, true, false);
        });

        detailButton.setOnClickListener(v -> {
            if(expandableRelativeLayout.isExpanded()){
                expandableRelativeLayout.collapse();
            }else{
                expandableRelativeLayout.expand();
            }
//            if(trackerExpandableRelativeLayout.isExpanded()){
//                trackerExpandableRelativeLayout.collapse();
//            }else{
//                trackerExpandableRelativeLayout.expand();
//            }
        });
//        trackingButton.setOnClickListener(v -> {
//            if(expandableRelativeLayout.isExpanded()){
//                expandableRelativeLayout.collapse();
//            }else{
//                expandableRelativeLayout.expand();
//            }
//            if(trackerExpandableRelativeLayout.isExpanded()){
//                trackerExpandableRelativeLayout.collapse();
//            }else{
//                trackerExpandableRelativeLayout.expand();
//            }
//        });

        swipeRefreshLayout.setOnSwipeListener(new SwipeCallback() {
            @Override
            public void onSwipeFromTop() {
                swipeRefreshLayout.finishRefresh();
            }

            @Override
            public void onSwipeFromBottom() {
                swipeRefreshLayout.finishRefreshLoadMore();

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
//        {
//            "deal_code_number": "1502072618",
//                "tracking_id": "014860025965817"
//            "shipp_code" : "jne"
//        }
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("deal_code_number", order.getDealCodeNumber());
        hashMap.put("tracking_id", order.getTrackingId());
        hashMap.put("shipp_code", orderDetail.getShippCode());

        HttpInstance.call(HttpInstance.create(TrackingService.class).tracking(hashMap), trackingListResponse -> {
            if(trackingListResponse.isStatus()){
                simpleRecyclerAdapter.setObjects(trackingListResponse.getTraceTrakings().getHistoryTrackings());
            }else{
                activity(HomeActivity.class).showSnackbar(trackingListResponse.getMessage(), v -> {});
            }
        }, throwable -> {
            activity(HomeActivity.class).showSnackbar(throwable, v -> {});
        }, false);
    }
}
