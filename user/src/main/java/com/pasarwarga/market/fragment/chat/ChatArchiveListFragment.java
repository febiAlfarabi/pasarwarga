//package com.pasarwarga.market.fragment.chat;
//
//
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v4.app.Fragment;
//import android.support.v7.widget.LinearLayoutManager;
//import android.view.View;
//
//import com.alfarabi.alfalibs.helper.SwipeCallback;
//import com.alfarabi.alfalibs.views.AlfaRecyclerView;
//import com.alfarabi.alfalibs.views.AlfaSwipeRefreshLayout;
//import com.alfarabi.base.fragment.BaseUserFragment;
//import com.alfarabi.base.model.chat.UserConversation;
//import com.alfarabi.base.realm.RealmDao;
//import com.pasarwarga.market.R;
//import com.pasarwarga.market.adapter.realm.ChatArchiveListAdapter;
//
//import butterknife.BindView;
//
///**
// * A simple {@link Fragment} subclass.
// */
//public class ChatArchiveListFragment extends BaseUserFragment {
//
//    @BindView(R.id.recyclerview) AlfaRecyclerView recyclerView;
//    @BindView(R.id.swiperefreshlayout) AlfaSwipeRefreshLayout swipeRefreshLayout;
//
//    private ChatArchiveListAdapter chatArchiveListAdapter ;
//
//
//    @Override
//    public String initTitle() {
//        return null;
//    }
//
//    @Override
//    public int contentXmlLayout() {
//        return R.layout.fragment_chat_archive_list;
//    }
//
//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        chatArchiveListAdapter = new ChatArchiveListAdapter(this, RealmDao.getRealm().where(UserConversation.class).equalTo("archive", true).findAll(), true);
//    }
//
//    @Override
//    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//        recyclerView.setLayoutManager(new LinearLayoutManager(activity(), LinearLayoutManager.VERTICAL, false));
//        recyclerView.setAdapter(chatArchiveListAdapter);
//
//        swipeRefreshLayout.setOnSwipeListener(new SwipeCallback() {
//            @Override
//            public void onSwipeFromTop() {
//                swipeRefreshLayout.finishRefresh();
//                swipeRefreshLayout.finishRefreshLoadMore();
//            }
//
//            @Override
//            public void onSwipeFromBottom() {
//                swipeRefreshLayout.finishRefresh();
//                swipeRefreshLayout.finishRefreshLoadMore();
//            }
//        });
//    }
//}
