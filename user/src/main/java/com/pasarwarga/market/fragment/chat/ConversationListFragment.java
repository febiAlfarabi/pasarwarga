package com.pasarwarga.market.fragment.chat;


import android.app.NotificationManager;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.alfarabi.alfalibs.helper.SwipeCallback;
import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.alfalibs.tools.WLog;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.alfarabi.alfalibs.views.AlfaSwipeRefreshLayout;
import com.alfarabi.base.fragment.BaseUserFragment;
import com.alfarabi.base.fragment.SimpleBaseDialogFragmentInitiator;
import com.alfarabi.base.http.basemarket.service.ConversationService;
import com.alfarabi.base.model.Seller;
import com.alfarabi.base.model.User;
import com.alfarabi.base.model.chat.ChatSellerInfo;
import com.alfarabi.base.model.chat.ChatUser;
import com.alfarabi.base.model.chat.Conversation;
import com.alfarabi.base.model.chat.Partner;
import com.alfarabi.base.model.chat.ProductAttachment;
import com.alfarabi.base.model.chat.UserConversation;
import com.alfarabi.base.model.shop.SellerInfo;
import com.alfarabi.base.realm.RealmDao;
import com.alfarabi.alfalibs.tools.KeyboardUtils;
import com.alfarabi.base.tools.ResponseTools;
import com.alfarabi.base.view.interfaze.ShowCaseEventListener;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.hendraanggrian.bundler.annotations.BindExtra;
import com.hendraanggrian.preferencer.annotations.BindPreference;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.adapter.realm.ChatListAdapter;
import com.pasarwarga.market.fragment.dialog.DialogCallback;
import com.pasarwarga.market.fragment.dialog.ProductChooserDialogFragment;
import com.pasarwarga.market.service.MyFirebaseMessagingService;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConversationListFragment extends BaseUserFragment implements DialogCallback<ProductAttachment> {

    @BindPreference("com.pasarwarga.market.fragment.chat.ConversationListFragment") boolean secondTimeOpen ;

    public static final String TAG = ConversationListFragment.class.getName();

    @BindView(R.id.toolbar) Toolbar toolbar ;
    @BindView(R.id.recyclerview) AlfaRecyclerView recyclerView;
    @BindView(R.id.swiperefreshlayout) AlfaSwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.message_et) EmojiconEditText messageEditText ;
    @BindView(R.id.emoji_button) ImageButton emojiButton ;
    @BindView(R.id.send_button) ImageButton sendButton ;
    @BindView(R.id.progress_bar) SmoothProgressBar progressBar ;
    @BindView(R.id.product_expandaple_chat_layout) ExpandableRelativeLayout productExpandableRelativeLayout ;

    @BindView(R.id.item_iv) ImageView productAttachmentImageView ;
    @BindView(R.id.item_name_tv) TextView productNameTextView ;
    @BindView(R.id.delete_iv) ImageView productAttachmentDeleteView ;

    @NonNull@BindExtra UserConversation userConversation ;
    private ChatListAdapter chatListAdapter ;
    private Map hashMap ;
    private ProductAttachment productAttachment ;

    private ProductChooserDialogFragment<ProductAttachment> productChooserDialogFragment ;

    NotificationManager mNotifyMgr ;

    @Override
    public String initTitle() {
        return null;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_conversation_list;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userConversation = RealmDao.unmanage(userConversation);
        chatListAdapter = new ChatListAdapter(this, RealmDao.getRealm()
                .where(Conversation.class).equalTo("partner.id", userConversation.getPartner().getId()).findAll(), true);
        mNotifyMgr = (NotificationManager) activity(HomeActivity.class).getSystemService(Context.NOTIFICATION_SERVICE);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        if(userConversation.getPartner().getChatSellerInfo()!=null && myProfile.getUser().isSeller() ){
            toolbar.inflateMenu(R.menu.chat_menu);
        }else if(userConversation.getPartner().getChatSellerInfo()!=null || myProfile.getUser().isSeller()){
            toolbar.inflateMenu(R.menu.chat_single_menu);
        }
        toolbar.setNavigationOnClickListener(v -> WindowFlow.backstack(activity(HomeActivity.class)));
        toolbar.setTitle(userConversation.getPartner().getChatUser().getFirstname()+" "+userConversation.getPartner().getChatUser().getLastname());
        chatListAdapter.init(recyclerView);
        EmojIconActions emojIcon = new EmojIconActions(activity(),activity().getWindow().getDecorView(), messageEditText , emojiButton);
        emojIcon.ShowEmojIcon();
        sendButton.setOnClickListener(v -> send());
        messageEditText.setOnEditorActionListener((v, actionId, event) -> {
            int result = actionId & EditorInfo.IME_MASK_ACTION;
            switch(result) {
                case EditorInfo.IME_ACTION_DONE:
                    send();
                    break;
                case EditorInfo.IME_ACTION_NEXT:
                    // next stuff
                    send();
                    break;
            }
            return true ;
        });

        swipeRefreshLayout.setOnSwipeListener(new SwipeCallback() {
            @Override
            public void onSwipeFromTop() {
                swipeRefreshLayout.finishRefresh();
                swipeRefreshLayout.finishRefreshLoadMore();
            }

            @Override
            public void onSwipeFromBottom() {
                swipeRefreshLayout.finishRefresh();
                swipeRefreshLayout.finishRefreshLoadMore();
            }
        });
        productExpandableRelativeLayout.collapse();
        productAttachment = null ;

        toolbar.setOnMenuItemClickListener(item -> {
            productChooserDialogFragment = SimpleBaseDialogFragmentInitiator.instance(ProductChooserDialogFragment.class, null, DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Holo_Dialog);

            SellerInfo sellerInfo = new SellerInfo();
            if(item.getItemId()==R.id.attach_my_product_menu){
                sellerInfo = myProfile.getUser().getSellerInfo();
            }else if(item.getItemId()==R.id.attach_partner_product_menu){
                sellerInfo.setId(String.valueOf(userConversation.getPartner().getChatSellerInfo().getId()));
                sellerInfo.setShopName(userConversation.getPartner().getChatSellerInfo().getName());
            }else if(item.getItemId()==R.id.attach_product_menu){
                if(userConversation.getPartner().getChatSellerInfo()!=null){
                    sellerInfo.setId(String.valueOf(userConversation.getPartner().getChatSellerInfo().getId()));
                    sellerInfo.setShopName(userConversation.getPartner().getChatSellerInfo().getName());
                }else if(myProfile.getUser().isSeller()){
                    sellerInfo = myProfile.getUser().getSellerInfo();
                }
            }
            productChooserDialogFragment.setSelectedProduct(-1);
            productChooserDialogFragment.showDialog(getChildFragmentManager(), productAttachment -> {
                this.productAttachment = productAttachment ;
                productNameTextView.setText(productAttachment.getName());
                GlideApp.with(this).load(StringUtils.split(productAttachment.getImage(), ",")[0])
                        .placeholder(R.drawable.ic_product_default)
                        .error(R.drawable.ic_product_default)
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                e.printStackTrace();
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                return false;
                            }
                        }).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL).into(productAttachmentImageView);
                productExpandableRelativeLayout.expand();

                productAttachmentDeleteView.setOnClickListener(v -> resetProductAttachment());
            }, sellerInfo);
            return true ;
        });

    }

    public void resetProductAttachment(){
        productExpandableRelativeLayout.collapse();
        this.productAttachment = null;
        productNameTextView.setText("");
        productAttachmentImageView.setImageBitmap(null);
    }

    public void send(){
        if(!messageEditText.getText().toString().equals("") || productAttachment!=null){
            hashMap = new HashMap();
            if(productAttachment!=null){
                Map productAttachmentMap = new HashMap<>();
                productAttachmentMap.put("id", productAttachment.getId());
                productAttachmentMap.put("name", productAttachment.getName());
                productAttachmentMap.put("image", productAttachment.getImage());
                hashMap.put("product_attachment", productAttachmentMap);
                if(!messageEditText.getText().toString().equals("")){
                    hashMap.put("message", StringEscapeUtils.escapeJava(messageEditText.getText().toString())+" ");
                }
            }else{
                hashMap.put("message", StringEscapeUtils.escapeJava(messageEditText.getText().toString())+" ");
            }
            Map partnerHashMap = new HashMap<>();
            partnerHashMap.put("user_id", String.valueOf(userConversation.getPartner().getId()));
//            partnerHashMap.put("as_seller", userConversation.getPartner().isAsSeller());
            partnerHashMap.put("as_seller", false);

            resetProductAttachment();

            Partner partner = userConversation.getPartner();
            partner.setSender(false);
            hashMap.put("partner", partnerHashMap);
            progressBar.setVisibility(View.VISIBLE);
            HttpInstance.call(HttpInstance.create(ConversationService.class).send(userConversation.getPartner().getId(), hashMap), sendConversationResponse -> {
                progressBar.setVisibility(View.GONE);

                ResponseTools.validate(activity(), sendConversationResponse);
                if(sendConversationResponse.isStatus()){
                    sendConversationResponse.getConversations().setHasBeenRead(true);
                    RealmDao.instance().insertOrUpdate(sendConversationResponse.getConversations());
                    RealmDao.instance().renew(userConversation.getId(), sendConversationResponse.getConversations().toUserConversation(String.valueOf(userConversation.getId()), false));
                }else{
                    activity(HomeActivity.class).showSnackbar(sendConversationResponse.getMessage(), v1 -> {});
                }
            }, throwable -> {
                progressBar.setVisibility(View.GONE);
                activity(HomeActivity.class).showSnackbar(throwable, v1 -> {});
            }, false);
            messageEditText.setText("");
            KeyboardUtils.hideKeyboard(activity());

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        progressBar.setVisibility(View.GONE);
//        HttpInstance.call(HttpInstance.create(ConversationService.class).getConversation(userConversation.getPartner().getId()), conversationResponse -> {
//            ResponseTools.validate(activity(), conversationResponse);
//            if(conversationResponse.isStatus() && conversationResponse.getConversations().size()>0){
//                for (Conversation conversation : conversationResponse.getConversations()) {
//                    conversation.setHasBeenRead(true);
//                    RealmDao.instance().renew(conversation.getId(), conversation);
//                }
//            }
//        }, throwable -> {
//            activity(HomeActivity.class).showSnackbar(throwable, v -> {});
//        }, false);

        if(chatListAdapter.getData()==null || chatListAdapter.getItemCount()==0){
            HttpInstance.call(HttpInstance.create(ConversationService.class).getConversation(userConversation.getPartner().getId()), conversationResponse -> {
                ResponseTools.validate(activity(), conversationResponse);
                if(conversationResponse.isStatus() && conversationResponse.getConversations().size()>0){
                    for (Conversation conversation : conversationResponse.getConversations()) {
                        conversation.setHasBeenRead(true);
                        RealmDao.instance().renew(conversation.getId(), conversation);
                    }
                }
            }, throwable -> {
                activity(HomeActivity.class).showSnackbar(throwable, v -> {});
            });
        }else{

        }
        showShowcase();
        chatListAdapter.registerAdapterDataObserver(adapterDataObserver);
        emojiButton.requestFocus();
        mNotifyMgr.cancel(Integer.valueOf(userConversation.getPartner().getId()));
    }

    @Override
    public void onStop() {
        super.onStop();
        chatListAdapter.unregisterAdapterDataObserver(adapterDataObserver);
    }

    @Override
    public boolean onBackPressed() {
        KeyboardUtils.hideKeyboard(activity(HomeActivity.class));
        return super.onBackPressed();
    }

    public static UserConversation buildConversation(Seller seller){
        UserConversation userConversation ;
        boolean previousChatExist = RealmDao.getRealm().where(UserConversation.class).equalTo("partner.id",seller.getUserId()).findFirst()!=null ;
        if(previousChatExist){
            userConversation = RealmDao.getRealm().where(UserConversation.class).equalTo("partner.id",seller.getUserId()).findFirst();
            return userConversation;
        }else{
            userConversation = new UserConversation();
            userConversation.setId(Long.valueOf(seller.getUserId()));
            userConversation.setArchive(false);
            Partner partner = new Partner();
            partner.setSender(false);
            partner.setId(seller.getUserId());
            ChatSellerInfo chatSellerInfo = new ChatSellerInfo();
            chatSellerInfo.setId(seller.getId());
            chatSellerInfo.setImage(seller.getAvatar());
            chatSellerInfo.setName(seller.getBusinessName());
            partner.setChatSellerInfo(chatSellerInfo);

            ChatUser chatUser = new ChatUser();
            chatUser.setId(Long.valueOf(seller.getUserId()));
            chatUser.setFirstname(seller.getName());
            chatUser.setImage(seller.getAvatar());
            chatUser.setLastname("");
            partner.setChatUser(chatUser);

            userConversation.setPartner(partner);
        }
        return RealmDao.unmanage(userConversation);
    }

    public static UserConversation buildConversation(SellerInfo seller){
        UserConversation userConversation ;
        boolean previousChatExist = RealmDao.getRealm().where(UserConversation.class).equalTo("partner.id",seller.getUserId()).findFirst()!=null ;
        if(previousChatExist){
            userConversation = RealmDao.getRealm().where(UserConversation.class).equalTo("partner.id",seller.getUserId()).findFirst();
            return userConversation;
        }else{
            userConversation = new UserConversation();
            userConversation.setId(Long.valueOf(seller.getUserId()));
            userConversation.setArchive(false);
            Partner partner = new Partner();
            partner.setSender(false);
            partner.setId(seller.getUserId());
            ChatSellerInfo chatSellerInfo = new ChatSellerInfo();
            chatSellerInfo.setId(Long.valueOf(seller.getId()));
            chatSellerInfo.setImage(seller.getStoreImage());
            chatSellerInfo.setName(seller.getShopName());
            partner.setChatSellerInfo(chatSellerInfo);

            ChatUser chatUser = new ChatUser();
            chatUser.setId(Long.valueOf(seller.getUserId()));
            chatUser.setFirstname(chatSellerInfo.getName());
            chatUser.setImage(chatSellerInfo.getImage());
            chatUser.setLastname("");
            partner.setChatUser(chatUser);

            userConversation.setPartner(partner);
        }
        return RealmDao.unmanage(userConversation);
    }


    public static UserConversation buildConversation(User user){
        UserConversation userConversation ;
        boolean previousChatExist = RealmDao.getRealm().where(UserConversation.class).equalTo("partner.id",user.getId()).findFirst()!=null ;
        if(previousChatExist){
            userConversation = RealmDao.getRealm().where(UserConversation.class).equalTo("partner.id",user.getId()).findFirst();
            return userConversation;
        }else{
            userConversation = new UserConversation();
            userConversation.setId(Long.valueOf(user.getId()));
            userConversation.setArchive(false);
            Partner partner = new Partner();
            partner.setSender(false);
            partner.setId(user.getId());
            if(user.isSeller() && user.isPhoneVerified() && user.getSellerInfo()!=null){
                ChatSellerInfo chatSellerInfo = new ChatSellerInfo();
                chatSellerInfo.setId(Long.valueOf(user.getSellerInfo().getId()));
                chatSellerInfo.setImage(user.getSellerInfo().getStoreImage());
                chatSellerInfo.setName(user.getSellerInfo().getShopName());
                partner.setChatSellerInfo(chatSellerInfo);
            }

            ChatUser chatUser = new ChatUser();
            chatUser.setId(Long.valueOf(user.getId()));
            chatUser.setFirstname(user.getFirstName());
            chatUser.setImage(user.getPhoto());
            chatUser.setLastname("");
            partner.setChatUser(chatUser);
            userConversation.setPartner(partner);
        }
        return RealmDao.unmanage(userConversation);
    }

    void showShowcase(){
        if(!secondTimeOpen){
            secondTimeOpen = true ;
            saver.saveAll();
            Observable.fromCallable(() -> {
                showcaseViewBuilder = new ShowcaseView.Builder(activity());
                showcaseViewBuilder.withHoloShowcase().hideOnTouchOutside().setStyle(R.style.CustomShowcaseTheme4)
                        .setTarget(new ViewTarget(messageEditText))
                        .setContentTitle(R.string.fill_chat_et_guide1)
                        .setContentText(R.string.fill_chat_et_guide2);

                showcaseViewBuilder.setShowcaseEventListener(new ShowCaseEventListener() {
                    @Override
                    public void onShowcaseViewHide(ShowcaseView showcaseView) {
                        super.onShowcaseViewHide(showcaseView);
                        showcaseViewBuilder = new ShowcaseView.Builder(activity());
                        showcaseViewBuilder.withHoloShowcase().hideOnTouchOutside().setStyle(R.style.CustomShowcaseTheme6)
                                .setTarget(new ViewTarget(emojiButton))
                                .setContentTitle(R.string.emoticon_button_guide1)
                                .setContentText(R.string.emoticon_button_guide2);
                        showcaseViewBuilder.setShowcaseEventListener(new ShowCaseEventListener() {
                            @Override
                            public void onShowcaseViewHide(ShowcaseView showcaseView) {
                                super.onShowcaseViewDidHide(showcaseView);
                                emojiButton.postDelayed(() -> {
                                    sendButton.postDelayed(() -> {
                                        showcaseViewBuilder = new ShowcaseView.Builder(activity());
                                        showcaseViewBuilder.withHoloShowcase().hideOnTouchOutside().setStyle(R.style.CustomShowcaseTheme4)
                                                .setTarget(new ViewTarget(sendButton))
                                                .setContentTitle(R.string.send_button_guide1)
                                                .setContentText(R.string.send_button_guide2);
                                        showcaseView.setOnShowcaseEventListener(new ShowCaseEventListener() {
                                            @Override
                                            public void onShowcaseViewHide(ShowcaseView showcaseView) {
                                                super.onShowcaseViewHide(showcaseView);
                                                emojiButton.requestFocus();
                                            }
                                        });
                                        showcaseViewBuilder.build();
                                    }, 700);
                                }, 700);

                            }

                        });
                        showcaseViewBuilder.build();
                    }
                }).build();
                return true ;
            }).subscribeOn(AndroidSchedulers.mainThread()).subscribe();

        }
    }

    RecyclerView.AdapterDataObserver adapterDataObserver = new RecyclerView.AdapterDataObserver() {


        @Override
        public void onItemRangeInserted(int positionStart, int itemCount) {
            super.onItemRangeInserted(positionStart, itemCount);
            WLog.i(TAG, "onItemRangeInserted :: "+String.valueOf(positionStart)+ " :::  "+String.valueOf(itemCount));
            recyclerView.smoothScrollToPosition(positionStart);
            mNotifyMgr.cancel(Integer.valueOf(userConversation.getPartner().getId()));
            RealmDao.getRealm().executeTransaction(realm -> chatListAdapter.getData().get(positionStart).setHasBeenRead(true));
        }

    };


    @Override
    public void onChoosed(ProductAttachment productAttachment) {

    }
}
