//package com.pasarwarga.market.fragment.chat;
//
//
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.design.widget.AppBarLayout;
//import android.support.v4.app.Fragment;
//import android.support.v4.view.ViewPager;
//import android.support.v4.widget.DrawerLayout;
//import android.support.v7.widget.Toolbar;
//import android.view.MenuItem;
//import android.view.View;
//
//import com.alfarabi.alfalibs.adapters.viewpager.BaseTabPagerAdapter;
//import com.alfarabi.alfalibs.fragments.SimpleBaseFragment;
//import com.alfarabi.alfalibs.fragments.interfaze.SimpleFragmentCallback;
//import com.alfarabi.alfalibs.http.HttpInstance;
//import com.alfarabi.base.fragment.home.BaseDrawerTabFragment;
//import com.alfarabi.base.http.basemarket.service.ConversationService;
//import com.alfarabi.base.model.chat.UserConversation;
//import com.alfarabi.base.realm.RealmDao;
//import com.alfarabi.base.view.TabLayout;
//import com.pasarwarga.market.R;
//import com.pasarwarga.market.activity.home.HomeActivity;
//
//import butterknife.BindView;
//
///**
// * A simple {@link Fragment} subclass.
// */
//public class ConversationTabFragment extends BaseDrawerTabFragment implements SimpleFragmentCallback {
//
//
//    public static final String TAG = ConversationTabFragment.class.getName();
//
//    @BindView(R.id.toolbar) Toolbar toolbar;
//    @BindView(R.id.drawer_layout) DrawerLayout drawerLayout;
//    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout;
//    @BindView(R.id.tab_layout) TabLayout tabLayout ;
//    @BindView(R.id.viewpager) ViewPager viewPager ;
//
//    @Override
//    public Toolbar getToolbar() {
//        return toolbar;
//    }
//
//    @Override
//    public AppBarLayout getAppbarLayout() {
//        return appBarLayout;
//    }
//
//    @Override
//    public int getMenuId() {
//        return R.id.sub_notification_menu_item_1;
//    }
//
//    @Override
//    public boolean onMenuItemClick(MenuItem item) {
//        return false;
//    }
//
//    @Override
//    public String initTitle() {
//        title = getString(R.string.conversation);
//        toolbar.setTitle(title);
//        return title;
//    }
//
//    @Override
//    public int contentXmlLayout() {
//        return R.layout.fragment_chat_tab;
//    }
//
//    @Override
//    public BaseTabPagerAdapter<?> baseTabPagerAdapter() {
//        return new BaseTabPagerAdapter<SimpleBaseFragment>(getChildFragmentManager()) {
//            @Override
//            public String[] titles() {
//                return new String[]{getString(R.string.chat), getString(R.string.archive)};
//            }
//
//            @Override
//            public SimpleBaseFragment[] fragmentClasses() {
//                return new SimpleBaseFragment[]{new ChatUserListFragment(), new ChatArchiveListFragment()};
//            }
//        };
//    }
//
//    @Override
//    public TabLayout getTabLayout() {
//        return tabLayout;
//    }
//
//    @Override
//    public ViewPager getViewPager() {
//        return viewPager;
//    }
//
//    @Override
//    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
////        toolbar.inflateMenu(R.menu.search_menu);
//        initTabLayout(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}
//
//            @Override
//            public void onPageSelected(int position) {}
//
//            @Override
//            public void onPageScrollStateChanged(int state) {}
//        });
////        injectSearchView(toolbar.getMenu(), new SearchView.OnQueryTextListener() {
////            @Override
////            public boolean onQueryTextSubmit(String query) {
////                return false;
////            }
////
////            @Override
////            public boolean onQueryTextChange(String newText) {
////                return false;
////            }
////        });
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        HttpInstance.call(HttpInstance.create(ConversationService.class).getConversation(), userConversationResponse -> {
//            if(userConversationResponse.isStatus()){
//                RealmDao.instance().renew(userConversationResponse.getUserConversations(), UserConversation.class);
//            }
//        }, throwable -> {
//            activity(HomeActivity.class).showDialog(throwable, (dialog, which) -> {});
//        }, false);
//    }
//}
