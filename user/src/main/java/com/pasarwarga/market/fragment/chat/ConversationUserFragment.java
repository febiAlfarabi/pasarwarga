package com.pasarwarga.market.fragment.chat;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.alfarabi.alfalibs.helper.SwipeCallback;
import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.WLog;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.alfarabi.alfalibs.views.AlfaSwipeRefreshLayout;
import com.alfarabi.base.fragment.home.BaseDrawerFragment;
import com.alfarabi.base.http.basemarket.service.ConversationService;
import com.alfarabi.base.model.chat.UserConversation;
import com.alfarabi.base.realm.RealmDao;
import com.alfarabi.alfalibs.tools.Caster;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.adapter.realm.ChatUserListAdapter;

import butterknife.BindView;
import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;
import io.realm.Sort;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConversationUserFragment extends BaseDrawerFragment {

    public static final String TAG = ConversationUserFragment.class.getName();

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout ;
    @BindView(R.id.recyclerview) AlfaRecyclerView recyclerView;
    @BindView(R.id.swiperefreshlayout) AlfaSwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.progress_bar) SmoothProgressBar progressBar ;

    private ChatUserListAdapter chatUserListAdapter ;


    @Override
    public String initTitle() {
        title = getString(R.string.conversation);
        toolbar.setTitle(title);
        return title;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_conversation_user;
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public int getMenuId() {
        return 0;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        chatUserListAdapter = new ChatUserListAdapter(this, RealmDao.getRealm().where(UserConversation.class).equalTo("archive", false).findAllSorted("created", Sort.DESCENDING), true);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity()));
        recyclerView.setAdapter(chatUserListAdapter);
        swipeRefreshLayout.setOnSwipeListener(new SwipeCallback() {
            @Override
            public void onSwipeFromTop() {
                swipeRefreshLayout.finishRefresh();
                swipeRefreshLayout.finishRefreshLoadMore();
            }

            @Override
            public void onSwipeFromBottom() {
                swipeRefreshLayout.finishRefresh();
                swipeRefreshLayout.finishRefreshLoadMore();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        progressBar.setVisibility(View.VISIBLE);
        HttpInstance.call(HttpInstance.create(ConversationService.class).getConversation(), userConversationResponse -> {
            if(userConversationResponse.isStatus()){
                for (UserConversation userConversation : userConversationResponse.getUserConversations()) {
                    WLog.d(TAG, "UserConversation class $$$ "+HttpInstance.getGson().toJson(userConversation));
                }
                RealmDao.instance().deleteAll(UserConversation.class);
                chatUserListAdapter.notifyDataSetChanged();
                RealmDao.instance().insertOrUpdateCollection(userConversationResponse.getUserConversations());
                chatUserListAdapter.notifyDataSetChanged();
            }
            progressBar.setVisibility(View.GONE);
        }, throwable -> {
            activity(HomeActivity.class).showDialog(throwable, (dialog, which) -> {});
            progressBar.setVisibility(View.GONE);
        }, false);
        Caster.activity(activity(HomeActivity.class), HomeActivity.class).renderUnreadChatCount();
    }



    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    @Override
    public boolean keepSideMenu() {
        return true;
    }
}
