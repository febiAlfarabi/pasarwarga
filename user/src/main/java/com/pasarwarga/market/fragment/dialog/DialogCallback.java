package com.pasarwarga.market.fragment.dialog;

/**
 * Created by Alfarabi on 10/24/17.
 */

public interface DialogCallback<O extends Object> {

    public void onChoosed(O o);
}
