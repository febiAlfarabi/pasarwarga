package com.pasarwarga.market.fragment.dialog;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.alfarabi.alfalibs.adapters.recyclerview.SimpleRecyclerAdapter;
import com.alfarabi.alfalibs.helper.SwipeCallback;
import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.alfarabi.alfalibs.views.AlfaSwipeRefreshLayout;
import com.alfarabi.base.fragment.SimpleBaseDialogFragmentInitiator;
import com.alfarabi.base.http.basemarket.service.SellerService;
import com.alfarabi.base.model.Product;
import com.alfarabi.base.model.chat.ProductAttachment;
import com.alfarabi.base.model.shop.SellerInfo;
import com.alfarabi.base.tools.ResponseTools;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.holder.recyclerview.dialog.ProductChooserDialogViewHolder;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;

import butterknife.BindView;
import lombok.Getter;
import lombok.Setter;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductChooserDialogFragment<O> extends SimpleBaseDialogFragmentInitiator {


    public static final String TAG = ProductChooserDialogFragment.class.getName();
    private SellerInfo sellerInfo ;


    @BindView(R.id.toolbar) Toolbar toolbar ;
    @BindView(R.id.swiperefreshlayout) AlfaSwipeRefreshLayout swipeRefreshLayout ;
    @BindView(R.id.recyclerview) AlfaRecyclerView recyclerView ;
    @BindView(R.id.cancel_button) Button cancelButton ;
    @BindView(R.id.select_button) Button selectButton ;
    @BindView(R.id.loader) MKLoader loader ;
    @Getter private SimpleRecyclerAdapter<Product, ProductChooserDialogFragment, ProductChooserDialogViewHolder> simpleRecyclerAdapter ;

    int page = 1 ;
    @Getter@Setter int selectedProduct = -1 ;

    private DialogCallback<ProductAttachment> dialogCallback ;

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_product_chooser_dialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        simpleRecyclerAdapter = new SimpleRecyclerAdapter<>(this,  ProductChooserDialogViewHolder.class, new ArrayList<>());
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        toolbar.setTitle(getString(R.string.available_product));
        simpleRecyclerAdapter.initRecyclerView(recyclerView, new GridLayoutManager(activity(), 2));
        swipeRefreshLayout.setOnSwipeListener(new SwipeCallback() {
            @Override
            public void onSwipeFromTop() {
                HttpInstance.call(HttpInstance.create(SellerService.class).sellerDetail(Long.valueOf(sellerInfo.getId()), page = 1 , 0, "newest", ""), detailResponse -> {
                    ResponseTools.validate(activity(HomeActivity.class), detailResponse);
                    if(detailResponse.isStatus() && detailResponse.getSellerDashboard()!=null){
                        simpleRecyclerAdapter.setObjects(detailResponse.getSellerDashboard().getProducts());
                    }else{
                        activity(HomeActivity.class).showSnackbar(detailResponse.getMessage(), v -> {});
                    }
                    simpleRecyclerAdapter.notifyDataSetChanged();
                    swipeRefreshLayout.finishRefreshing();
                    swipeRefreshLayout.finishRefreshLoadMore();

                }, throwable -> {
                    activity(HomeActivity.class).showSnackbar(throwable, v -> {});
                    simpleRecyclerAdapter.notifyDataSetChanged();
                    swipeRefreshLayout.finishRefreshing();
                    swipeRefreshLayout.finishRefreshLoadMore();
                }, false);
            }

            @Override
            public void onSwipeFromBottom() {
                HttpInstance.call(HttpInstance.create(SellerService.class).sellerDetail(Long.valueOf(sellerInfo.getId()), ++page, 0, "newest", ""), detailResponse -> {
                    ResponseTools.validate(activity(HomeActivity.class), detailResponse);
                    if(detailResponse.isStatus() && detailResponse.getSellerDashboard()!=null){
                        simpleRecyclerAdapter.appendObjects(detailResponse.getSellerDashboard().getProducts());
                    }else{
                        activity(HomeActivity.class).showSnackbar(detailResponse.getMessage(), v -> {});
                    }
                    simpleRecyclerAdapter.notifyDataSetChanged();
                    swipeRefreshLayout.finishRefreshing();
                    swipeRefreshLayout.finishRefreshLoadMore();

                }, throwable -> {
                    activity(HomeActivity.class).showSnackbar(throwable, v -> {});
                    simpleRecyclerAdapter.notifyDataSetChanged();
                    swipeRefreshLayout.finishRefreshing();
                    swipeRefreshLayout.finishRefreshLoadMore();
                }, false);
            }
        });
        firstRequest(sellerInfo);

        cancelButton.setOnClickListener(v -> {
            dismiss();
        });
        selectButton.setOnClickListener(v -> {
            dismiss();
            if(dialogCallback!=null && getSelectedProduct()!=-1){
                Product product = getSimpleRecyclerAdapter().getObjects().get(getSelectedProduct());
                ProductAttachment productAttachment = new ProductAttachment();
                productAttachment.setId(product.getId());
                productAttachment.setName(product.getName());
//                productAttachment.setDescription(product.getDescription());
                productAttachment.setImage(product.getImage());
                dialogCallback.onChoosed(productAttachment);
            }
        });

    }


    public void showDialog(FragmentManager fragmentManager, DialogCallback<ProductAttachment> dialogCallback, SellerInfo sellerInfo) {
        this.dialogCallback = dialogCallback ;
        this.sellerInfo = sellerInfo ;
        FragmentTransaction ft = fragmentManager.beginTransaction();
        Fragment prev = fragmentManager.findFragmentByTag(TAG);
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        show(fragmentManager, TAG);
    }


    public void firstRequest(SellerInfo sellerInfo) {
        loader.setVisibility(View.VISIBLE);
        HttpInstance.call(HttpInstance.create(SellerService.class).sellerDetail(Long.valueOf(sellerInfo.getId()),  page = 1, 0, "newest", ""), detailResponse -> {
            ResponseTools.validate(activity(HomeActivity.class), detailResponse);
            if(detailResponse.isStatus() && detailResponse.getSellerDashboard()!=null){
                simpleRecyclerAdapter.setObjects(detailResponse.getSellerDashboard().getProducts());
            }else{
                activity(HomeActivity.class).showSnackbar(detailResponse.getMessage(), v -> {});
            }
            swipeRefreshLayout.finishRefreshing();
            swipeRefreshLayout.finishRefreshLoadMore();
            loader.setVisibility(View.GONE);

        }, throwable -> {
            activity(HomeActivity.class).showSnackbar(throwable, v -> {});
            loader.setVisibility(View.GONE);
            simpleRecyclerAdapter.notifyDataSetChanged();
            swipeRefreshLayout.finishRefreshing();
            swipeRefreshLayout.finishRefreshLoadMore();

        }, false);
    }

}
