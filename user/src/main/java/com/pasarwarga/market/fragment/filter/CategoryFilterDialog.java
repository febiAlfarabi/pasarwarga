package com.pasarwarga.market.fragment.filter;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.alfarabi.alfalibs.adapters.recyclerview.VerticalRecyclerAdapter;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.alfarabi.base.fragment.BaseUserFragment;
import com.alfarabi.base.model.location.Category;
import com.pasarwarga.market.R;
import com.pasarwarga.market.holder.recyclerview.filter.CategoryFilterViewHolder;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import butterknife.BindView;
import lombok.Getter;
import lombok.Setter;

/**
 * A simple {@link Fragment} subclass.
 */
public class CategoryFilterDialog extends BaseUserFragment implements FilterDialogCallback {
    
    public static final String TAG = CategoryFilterDialog.class.getName();

    @BindView(R.id.recyclerview) AlfaRecyclerView recyclerView ;

    VerticalRecyclerAdapter<CategoryFilterDialog, CategoryFilterViewHolder, Category> verticalRecyclerAdapter ;
    @Getter@Setter private HashMap<Long, Boolean> selectedCategoryHashMap = new HashMap<Long, Boolean>();

    @Override
    public String initTitle() {
        return null;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_category_filter;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        verticalRecyclerAdapter = new VerticalRecyclerAdapter<>(this, CategoryFilterViewHolder.class, landing.getCategories());
        verticalRecyclerAdapter.initRecyclerView(recyclerView, new LinearLayoutManager(activity()));
    }


    @Override
    public void reset() {
        selectedCategoryHashMap.clear();
        if(verticalRecyclerAdapter!=null){
            verticalRecyclerAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public String getFilterString() {
        StringBuilder sb = new StringBuilder();
        Iterator<Map.Entry<Long , Boolean>> iterator = selectedCategoryHashMap.entrySet().iterator();
        while (iterator.hasNext()){
            Map.Entry<Long , Boolean> entry = iterator.next();
            if(entry.getValue()){
                sb.append(String.valueOf(entry.getKey())).append(",");
            }
        }

        if(sb.toString().endsWith(",")){
            sb.deleteCharAt(sb.toString().length()-1);
        }
        return sb.toString();
    }
}
