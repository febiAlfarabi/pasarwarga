package com.pasarwarga.market.fragment.filter;

/**
 * Created by Alfarabi on 9/20/17.
 */

interface FilterDialogCallback {

    public void reset();

    public String getFilterString();
}
