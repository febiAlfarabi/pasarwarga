package com.pasarwarga.market.fragment.filter;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alfarabi.alfalibs.tools.WLog;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.fragment.BaseUserFragment;
import com.alfarabi.base.tools.InputTools;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.fragment.store.product.ProductListFragment;
import com.pasarwarga.market.fragment.store.product.ProductPromoListFragment;

import java.util.HashMap;

import butterknife.BindView;
import lombok.Getter;
import lombok.Setter;

/**
 * A simple {@link Fragment} subclass.
 */
public class FilterFragment<FF extends BaseUserFragment & FilterDialogCallback> extends BaseUserFragment {

    public static final String TAG = FilterFragment.class.getName();
    

    @BindView(R.id.menu_recyclerview) RecyclerView menuRecyclerView ;
    @BindView(R.id.viewpager) ViewPager viewpager ;
    @BindView(R.id.reset_button) Button resetButton ;
    @BindView(R.id.submit_button) Button submitButton ;

    @Getter private HashMap<String, FF> fragmentHashMap = new HashMap<String, FF>();
    private FilterAdapter filterAdapter ;
    private String[] menus = {};


    @Override
    public String initTitle() {
        return null;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_filter;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        filterAdapter = new FilterAdapter();
        menus = new String[]{getString(R.string.category), getString(R.string.location), getString(R.string.price), getString(R.string.shipping)};

        fragmentHashMap.put(getString(R.string.category), (FF) new CategoryFilterDialog());
        fragmentHashMap.put(getString(R.string.location), (FF) new LocationFilterDialog());
        fragmentHashMap.put(getString(R.string.price), (FF) new PriceFilterDialog());
        fragmentHashMap.put(getString(R.string.shipping), (FF) new ShippingFilterDialog());


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        menuRecyclerView.setLayoutManager(new LinearLayoutManager(activity()));
        menuRecyclerView.setAdapter(filterAdapter);
        filterAdapter.notifyDataSetChanged();
        viewpager.setAdapter(new PagerAdapter(getFragmentManager()));
        viewpager.getAdapter().notifyDataSetChanged();
        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                ((FilterAdapter)menuRecyclerView.getAdapter()).setFocus(position);
                ((FilterAdapter)menuRecyclerView.getAdapter()).notifyDataSetChanged();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        resetButton.setOnClickListener(v -> {
            fragmentHashMap.get(getString(R.string.price)).reset();
            fragmentHashMap.get(getString(R.string.category)).reset();
            fragmentHashMap.get(getString(R.string.location)).reset();
            fragmentHashMap.get(getString(R.string.shipping)).reset();
            if(WindowFlow.getFragment(activity(HomeActivity.class), ProductListFragment.TAG)!=null){
                ((ProductListFragment)WindowFlow.getFragment(activity(HomeActivity.class), ProductListFragment.TAG)).setFilter(null, null, null, null, null);
            }
            if(WindowFlow.getFragment(activity(HomeActivity.class), ProductPromoListFragment.TAG)!=null){
                ((ProductPromoListFragment)WindowFlow.getFragment(activity(HomeActivity.class), ProductPromoListFragment.TAG)).setFilter(null, null, null, null, null);
            }
        });
        submitButton.setOnClickListener(v -> {
            String categories = fragmentHashMap.get(getString(R.string.category)).getFilterString();
            String locations = fragmentHashMap.get(getString(R.string.location)).getFilterString();
            String shippings = fragmentHashMap.get(getString(R.string.shipping)).getFilterString();
            String startPrice = String.valueOf((int)((PriceFilterDialog)fragmentHashMap.get(getString(R.string.price))).getStartPrice());
            String endPrice = String.valueOf((int)((PriceFilterDialog)fragmentHashMap.get(getString(R.string.price))).getEndPrice());
            if(InputTools.notEmpty(categories, locations, shippings, startPrice, endPrice)){
                if(WindowFlow.getFragment(activity(HomeActivity.class), ProductListFragment.TAG)!=null){
                    ((ProductListFragment)WindowFlow.getFragment(activity(HomeActivity.class), ProductListFragment.TAG)).setFilter(categories, locations, shippings, startPrice, endPrice);
                }
                if(WindowFlow.getFragment(activity(HomeActivity.class), ProductPromoListFragment.TAG)!=null){
                    ((ProductPromoListFragment)WindowFlow.getFragment(activity(HomeActivity.class), ProductPromoListFragment.TAG)).setFilter(categories, locations, shippings, startPrice, endPrice);
                }
            }else{
                if(WindowFlow.getFragment(activity(HomeActivity.class), ProductListFragment.TAG)!=null){
                    ((ProductListFragment)WindowFlow.getFragment(activity(HomeActivity.class), ProductListFragment.TAG)).setFilter(null, null, null, null, null);
                }
                if(WindowFlow.getFragment(activity(HomeActivity.class), ProductPromoListFragment.TAG)!=null){
                    ((ProductPromoListFragment)WindowFlow.getFragment(activity(HomeActivity.class), ProductPromoListFragment.TAG)).setFilter(null, null, null, null, null);
                }
            }
            WindowFlow.backstack(activity(HomeActivity.class));

        });
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        @Getter TextView textView ;
        @Getter LinearLayout containerLayout ;

        public ViewHolder(View itemView) {
            super(itemView);
            this.textView = (TextView) itemView.findViewById(R.id.row_1_tv);
            containerLayout = (LinearLayout) itemView.findViewById(R.id.container_layout);
        }

        void showData(String text, int focus){
            textView.setText(text);
            if(getAdapterPosition()==focus){
                containerLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.vertical_tab_shape_selected_bg));
                textView.setTextColor(Color.WHITE);
            }else{
                containerLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.vertical_tab_shape_bg));
                textView.setTextColor(Color.BLACK);
            }
            containerLayout.setOnClickListener(v -> {
                int position = 0;
                loop:
                for (int i = 0; i < menus.length; i++) {
                    if(text.equalsIgnoreCase(menus[i])){
                        position = i ;
                        break loop;
                    }
                }
                WLog.i(TAG, "Clicked Position :::: "+ String.valueOf(position));
                WLog.i(TAG, "Clicked Position :::: "+ String.valueOf(getAdapterPosition()));
                filterAdapter.setFocus(getAdapterPosition());
                filterAdapter.notifyItemRangeChanged(0, menus.length);
                viewpager.setCurrentItem(position);
            });
        }
    }

    class PagerAdapter extends FragmentStatePagerAdapter{

        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentHashMap.get(menus[position]);
        }

        @Override
        public int getCount() {
            return menus.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return menus[position];
        }

    }
    class FilterAdapter extends RecyclerView.Adapter<ViewHolder>{

        @Getter@Setter int focus = 0 ;
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(activity()).inflate(R.layout.viewholder_filter, parent, false));
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.showData(menus[position], focus);

        }

        @Override
        public int getItemCount() {
            return menus.length;
        }
    }
}
