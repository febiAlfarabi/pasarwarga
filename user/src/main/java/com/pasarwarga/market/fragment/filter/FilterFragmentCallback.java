package com.pasarwarga.market.fragment.filter;

/**
 * Created by Alfarabi on 9/20/17.
 */

public interface FilterFragmentCallback {

    public boolean isFiltered();

    public void resetFilter();

    public void setFilter(String categories, String cities, String shippingMethods, String minprice, String maxPrice);
}
