package com.pasarwarga.market.fragment.filter;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.alfarabi.alfalibs.adapters.recyclerview.VerticalRecyclerAdapter;
import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.alfarabi.base.fragment.BaseUserFragment;
import com.alfarabi.base.http.basemarket.service.CityService;
import com.alfarabi.base.model.location.City;
import com.alfarabi.base.realm.RealmDao;
import com.alfarabi.base.tools.ResponseTools;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.holder.recyclerview.filter.LocationFilterViewHolder;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import lombok.Getter;
import lombok.Setter;

/**
 * A simple {@link Fragment} subclass.
 */
public class LocationFilterDialog extends BaseUserFragment implements FilterDialogCallback {
    
    public static final String TAG = LocationFilterDialog.class.getName();

    @BindView(R.id.recyclerview) AlfaRecyclerView recyclerView ;
    @BindView(R.id.search_et) EditText searchEditText ;

    @Getter VerticalRecyclerAdapter<LocationFilterDialog, LocationFilterViewHolder, City> verticalRecyclerAdapter ;
    @Getter@Setter public HashMap<String, Boolean> selectedCityHashMap = new HashMap<>();
    @BindView(R.id.loader) MKLoader loader;

    private List<City> cities = new ArrayList<>();
    boolean onSearch = false ;

    @Override
    public String initTitle() {
        return null;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_location_filter;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        verticalRecyclerAdapter = new VerticalRecyclerAdapter<>(this, LocationFilterViewHolder.class, cities);
        verticalRecyclerAdapter.initRecyclerView(recyclerView, new LinearLayoutManager(activity()));
        if(RealmDao.instance().rowExist(City.class)){
            verticalRecyclerAdapter.setObjects(RealmDao.instance().getList(City.class, false));
        }else{
            requestCity();
        }
        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                if(onSearch){
                    return;
                }

                onSearch = true ;
                verticalRecyclerAdapter.filter(s.toString());
                onSearch = false ;
            }
        });
    }
    public void requestCity(){
        loader.setVisibility(MKLoader.VISIBLE);
        HttpInstance.call(HttpInstance.create(CityService.class).getAllCityByCountry(CityService.IND_COUNTRY_ID), cityResponse -> {
            ResponseTools.validate(getActivity(), cityResponse);
            loader.setVisibility(MKLoader.GONE);
            if(cityResponse.isStatus()){
                cities.clear();
                cities.addAll(cityResponse.getCities());
                verticalRecyclerAdapter.setObjects(cities);
            }else{
                activity(HomeActivity.class).showSnackbar(cityResponse.getMessage(), v -> {});
            }

        }, throwable -> {
            loader.setVisibility(MKLoader.GONE);
            activity(HomeActivity.class).showSnackbar(throwable.getMessage(), v -> {});
        });
    }

    @Override
    public void reset() {
        getSelectedCityHashMap().clear();
        if(verticalRecyclerAdapter!=null){
            verticalRecyclerAdapter.notifyDataSetChanged();
        }

    }

    @Override
    public String getFilterString() {
        StringBuilder sb = new StringBuilder();
        Iterator<Map.Entry<String , Boolean>> iterator = selectedCityHashMap.entrySet().iterator();
        while (iterator.hasNext()){
            Map.Entry<String , Boolean> entry = iterator.next();
            if(entry.getValue()){
                sb.append(entry.getKey()).append(",");
            }
        }

        if(sb.toString().endsWith(",")){
            sb.deleteCharAt(sb.toString().length()-1);
        }
        return sb.toString();
    }
}
