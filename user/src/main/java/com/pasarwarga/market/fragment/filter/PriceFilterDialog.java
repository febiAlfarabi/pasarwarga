package com.pasarwarga.market.fragment.filter;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.TextView;

import com.alfarabi.base.fragment.BaseUserFragment;
import com.pasarwarga.market.R;

import org.jetbrains.annotations.NotNull;

import java.text.NumberFormat;

import butterknife.BindView;
import lombok.Getter;
import lombok.Setter;
import me.bendik.simplerangeview.SimpleRangeView;

/**
 * A simple {@link Fragment} subclass.
 */
public class PriceFilterDialog extends BaseUserFragment implements FilterDialogCallback {

    private static final int MULTIPLY = 10000;

    public static final String TAG = PriceFilterDialog.class.getName();

    @BindView(R.id.rangeview) SimpleRangeView simpleRangeView ;
    @BindView(R.id.start_price_tv) TextView startPriceTextView ;
    @BindView(R.id.end_price_tv) TextView endPriceTextView ;
    @Getter@Setter float startPrice = 0 ;
    @Getter@Setter float endPrice = 990*MULTIPLY;

    @Override
    public String initTitle() {
        return "";
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_price_filter;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        startPriceTextView.setText(getString(R.string.curr_symb)+" "+ NumberFormat.getInstance().format(startPrice));
        endPriceTextView.setText(getString(R.string.curr_symb)+" "+ NumberFormat.getInstance().format(endPrice));


        simpleRangeView.setOnTrackRangeListener(new SimpleRangeView.OnTrackRangeListener() {
            @Override
            public void onStartRangeChanged(@NotNull SimpleRangeView simpleRangeView, int i) {
                startPrice = i*MULTIPLY;
                startPriceTextView.setText(getString(R.string.curr_symb)+" "+ NumberFormat.getInstance().format(startPrice));
            }

            @Override
            public void onEndRangeChanged(@NotNull SimpleRangeView simpleRangeView, int i) {
                endPrice = i*MULTIPLY;
                endPriceTextView.setText(getString(R.string.curr_symb)+" "+ NumberFormat.getInstance().format(endPrice));

            }
        });
        simpleRangeView.postDelayed(() -> {
            simpleRangeView.setStart((int) startPrice);
            simpleRangeView.setEnd((int) endPrice);
        }, 1000);
    }

    @Override
    public void reset(){
        startPrice = 0;
        endPrice = 990*MULTIPLY ;
        if(simpleRangeView!=null && startPriceTextView!=null && endPriceTextView!=null){
            simpleRangeView.setStart((int) startPrice);
            simpleRangeView.setEnd((int) endPrice);
            startPriceTextView.setText(getString(R.string.curr_symb)+" "+ NumberFormat.getInstance().format(startPrice));
            endPriceTextView.setText(getString(R.string.curr_symb)+" "+ NumberFormat.getInstance().format(endPrice));
        }
    }

    @Override
    public String getFilterString() {
        return null;
    }
}
