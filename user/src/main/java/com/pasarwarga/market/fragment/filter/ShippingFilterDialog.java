package com.pasarwarga.market.fragment.filter;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.alfarabi.alfalibs.adapters.recyclerview.VerticalRecyclerAdapter;
import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.alfarabi.base.fragment.BaseUserFragment;
import com.alfarabi.base.http.basemarket.service.ShippingService;
import com.alfarabi.base.model.Shipping;
import com.alfarabi.base.tools.ResponseTools;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.holder.recyclerview.filter.ShippingFilterViewHolder;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import lombok.Getter;
import lombok.Setter;

/**
 * A simple {@link Fragment} subclass.
 */
public class ShippingFilterDialog extends BaseUserFragment implements FilterDialogCallback {
    
    public static final String TAG = ShippingFilterDialog.class.getName();

    @BindView(R.id.recyclerview) AlfaRecyclerView recyclerView ;
    @BindView(R.id.loader) MKLoader loader;

    VerticalRecyclerAdapter<ShippingFilterDialog, ShippingFilterViewHolder, Shipping> verticalRecyclerAdapter ;
    @Getter@Setter HashMap<String, Boolean> selectedShippingHashMap = new HashMap<>();
    private List<Shipping> shippings = new ArrayList<>();

    @Override
    public String initTitle() {
        return null;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_shipping_filter;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        verticalRecyclerAdapter = new VerticalRecyclerAdapter<>(this, ShippingFilterViewHolder.class, shippings);
        verticalRecyclerAdapter.initRecyclerView(recyclerView, new LinearLayoutManager(activity()));
        getShippingServices();
    }

    public void getShippingServices(){
        loader.setVisibility(MKLoader.VISIBLE);
        HttpInstance.call(HttpInstance.create(ShippingService.class).shippingSevices(), shippingResponse -> {
            ResponseTools.validate(activity(), shippingResponse);
            if(shippingResponse.isStatus() && shippingResponse.getShippings()!=null && shippingResponse.getShippings().size()>0){
                shippings.clear();
                shippings.addAll(shippingResponse.getShippings());
                verticalRecyclerAdapter.setObjects(shippings);
            }else{
                activity(HomeActivity.class).showDialog(shippingResponse.getMessage(), (dialog, which) -> {
                    dialog.dismiss();
                });
            }
            loader.setVisibility(MKLoader.GONE);
        }, throwable -> {
            loader.setVisibility(MKLoader.GONE);
            activity(HomeActivity.class).showSnackbar(throwable, v -> {});
        }, false);
    }

    @Override
    public void reset() {
        selectedShippingHashMap.clear();
        if(verticalRecyclerAdapter!=null){
            verticalRecyclerAdapter.notifyDataSetChanged();
        }
    }
    @Override
    public String getFilterString() {
        StringBuilder sb = new StringBuilder();
        Iterator<Map.Entry<String , Boolean>> iterator = selectedShippingHashMap.entrySet().iterator();
        while (iterator.hasNext()){
            Map.Entry<String , Boolean> entry = iterator.next();
            if(entry.getValue()){
                sb.append(entry.getKey()).append(",");
            }
        }
//        selectedShippingHashMap.forEach((s, aBoolean) -> {
//            if(aBoolean){
//                sb.append(s).append(",");
//            }
//        });
        if(sb.toString().endsWith(",")){
            sb.deleteCharAt(sb.toString().length()-1);
        }
        return sb.toString();
    }
}
