package com.pasarwarga.market.fragment.home;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.ImageSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.StringStickyRecyclerAdapter;
import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.Gilimanuk;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.alfalibs.views.AlfaBannerView;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.alfarabi.base.activity.SimpleBaseActivityInitiator;
import com.alfarabi.base.adapter.DotRecyclerAdapter;
import com.alfarabi.base.http.basemarket.service.CartService;
import com.alfarabi.base.http.basemarket.service.LandingService;
import com.alfarabi.base.model.transaction.Cart;
import com.alfarabi.base.model.Product;
import com.alfarabi.base.realm.RealmDao;
import com.alfarabi.base.tools.ResponseTools;
import com.alfarabi.base.view.interfaze.ShowCaseEventListener;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.hendraanggrian.bundler.Bundler;
import com.hendraanggrian.preferencer.annotations.BindPreference;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.fragment.store.CartFragment;
import com.alfarabi.base.adapter.BannerScollViewAdapter;
import com.alfarabi.base.fragment.home.BaseDrawerFragment;
import com.pasarwarga.market.R;
import com.pasarwarga.market.fragment.store.CartListFragment;
import com.pasarwarga.market.fragment.store.SearchFragment;
import com.pasarwarga.market.holder.sticky.ProductBodyViewHolder;
import com.pasarwarga.market.holder.sticky.ProductHeaderViewHolder;
import com.pasarwarga.market.tools.SystemApp;
import com.yarolegovich.discretescrollview.DiscreteScrollView;

import java.util.Arrays;

import butterknife.BindView;
import ca.barrenechea.widget.recyclerview.decoration.StickyHeaderDecoration;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import lombok.Getter;

/**
 * Created by Alfarabi on 6/16/17.
 */

public class HomeFragment extends BaseDrawerFragment{

    //
    // this link explain speedup recyclerview when he has a lot of data's : https://thisbitofcode.com/speeding-up-recyclerviews/

    public static final String TAG = HomeFragment.class.getName();

    @BindPreference("com.pasarwarga.market.fragment.home.HomeFragment") boolean secondTimeOpen ;

    @BindView(R.id.toolbar) Toolbar toolbar ;
    @BindView(R.id.collapsing_toolbar) CollapsingToolbarLayout collapsingToolbarLayout ;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout ;
    @BindView(R.id.swiperefreshlayout) SwipeRefreshLayout swipeRefreshLayout ;
    @BindView(R.id.recyclerview) AlfaRecyclerView recyclerView ;
    @BindView(R.id.alfabannerview) AlfaBannerView alfaBannerView ;
    @BindView(R.id.search_et) EditText searchEditText ;
    @BindView(R.id.dot_recyclerview) RecyclerView dotRecyclerView ;
    DotRecyclerAdapter dotRecyclerAdapter ;


    @Getter private StringStickyRecyclerAdapter stringStickyRecyclerAdapter ;
    private StickyHeaderDecoration stickyHeaderDecoration ;
    private SearchView searchView ;
    private TextView cartBadgeCount ;

    private Cart cart ;


    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_home;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        stringStickyRecyclerAdapter = new StringStickyRecyclerAdapter(this, ProductHeaderViewHolder.class, ProductBodyViewHolder.class, Arrays.asList(getResources().getStringArray(R.array.landing_groups)));
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        toolbar.inflateMenu(R.menu.home_menu);
        toolbar.setOnMenuItemClickListener(this);

        MenuItemCompat.setActionView(toolbar.getMenu().findItem(R.id.cart_menu), R.layout.badge_count);
        View actionView = MenuItemCompat.getActionView(toolbar.getMenu().findItem(R.id.cart_menu));
        actionView.setOnClickListener(v -> {
            if(SystemApp.checkLogin(activity(), true)){
                if(!SystemApp.checkVerified(activity())){
                    return;
                }
                Gilimanuk.withAnim(appearAnims).goTo(activity(HomeActivity.class), CartListFragment.TAG, R.id.fragment_frame, true, false);
//                if(cart!=null && cart.getCount()>0){
//                    Gilimanuk.withAnim(appearAnims).goTo(activity(HomeActivity.class), CartFragment.TAG, Bundler.wrap(CartFragment.class, new Product()), R.id.fragment_frame, true, false);
//                }else{
//                    Gilimanuk.withAnim(appearAnims).goTo(activity(HomeActivity.class), CartListFragment.TAG, R.id.fragment_frame, true, false);
////                    activity(HomeActivity.class).showSnackbar(getString(R.string.you_dont_have_any_order), v1 -> {});
//                }
            }
        });
        cartBadgeCount = (TextView) actionView.findViewById(R.id.notif_count);
        renderBadge();
        alfaBannerView.setAdapter(new BannerScollViewAdapter(getActivity(), landing.getBanners(), R.layout.fragment_home_banner, R.drawable.banner_pasarwarga));
        alfaBannerView.getAdapter().notifyDataSetChanged();
        dotRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        dotRecyclerAdapter = new DotRecyclerAdapter(this, landing.getBanners().size(), 0);
        dotRecyclerView.setAdapter(dotRecyclerAdapter);
        dotRecyclerAdapter.notifyDataSetChanged();
        alfaBannerView.addScrollStateChangeListener(new DiscreteScrollView.ScrollStateChangeListener<RecyclerView.ViewHolder>() {
            @Override
            public void onScrollStart(@NonNull RecyclerView.ViewHolder currentItemHolder, int adapterPosition) {}

            @Override
            public void onScrollEnd(@NonNull RecyclerView.ViewHolder currentItemHolder, int adapterPosition) {
                dotRecyclerAdapter.setFocus(alfaBannerView.getCurrentItem());
            }

            @Override
            public void onScroll(float scrollPosition, @NonNull RecyclerView.ViewHolder currentHolder, @NonNull RecyclerView.ViewHolder newCurrent) {}
        });
        stickyHeaderDecoration = new StickyHeaderDecoration(stringStickyRecyclerAdapter);
        recyclerView.addItemDecoration(stickyHeaderDecoration);
        stringStickyRecyclerAdapter.initRecyclerView(recyclerView, new LinearLayoutManager(getContext()));

        swipeRefreshLayout.setOnRefreshListener(() -> {
            HttpInstance.call(HttpInstance.create(LandingService.class).dataLanding(), landingResponses -> {
                ResponseTools.validate(activity(), landingResponses);
                if(landingResponses.isStatus()){
                    landing = RealmDao.instance().renew(landing.getId(), landingResponses.getLanding());
                    stringStickyRecyclerAdapter.notifyDataSetChanged();
                }
                swipeRefreshLayout.setRefreshing(false);
            }, throwable -> {
                swipeRefreshLayout.setRefreshing(false);
                activity(SimpleBaseActivityInitiator.class).showSnackbar(throwable, v -> {});
            });
        });


        if(alfaBannerView.getAdapter().getItemCount()!=1) {
            autoMove(3000, true);
        }

        ImageSpan imageHint = new ImageSpan(activity(), R.drawable.ic_search_black_24dp);
        SpannableString spannableString = new SpannableString("_"+getString(R.string.search_product_or_store));
        spannableString.setSpan(imageHint, 0, 1, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        searchEditText.setHint(spannableString);
//        searchEditText.addTextChangedListener(new TextWatcher() {
//
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                searchEditText.setCompoundDrawables(null, null, null, null);
//            }
//
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//                if (searchEditText.getText().length() == 0)
//                    searchEditText.setCompoundDrawablesWithIntrinsicBounds(android.R.drawable.ic_search_category_default, 0, 0, 0);
//            }
//
//            public void afterTextChanged(Editable s) {
//                if (searchEditText.getText().length() == 0)
//                    searchEditText.setCompoundDrawablesWithIntrinsicBounds(android.R.drawable.ic_search_category_default, 0, 0, 0);
//            }
//        });
        searchEditText.setFocusable(false);
        searchEditText.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putString("input", "");
            Gilimanuk.withAnim(slideAnims).goTo(getActivity(), SearchFragment.TAG, bundle, R.id.fragment_frame, true, false);
        });

    }


    @Override
    public int getMenuId() {
        return R.id.home_menu_item;
    }

    @Override
    public void onResume() {
        super.onResume();
        renderBadge();
        if(!secondTimeOpen){
            secondTimeOpen = true ;
            saver.saveAll();
            Observable.fromCallable(() -> {
                showcaseViewBuilder = new  ShowcaseView.Builder(activity());
                showcaseViewBuilder.withHoloShowcase().hideOnTouchOutside().setStyle(R.style.CustomShowcaseTheme4)
                        .setTarget(new ViewTarget(toolbar.getChildAt(0)))
                        .setContentTitle(R.string.show_left_menu_guide1)
                        .setContentText(R.string.show_left_menu_guide2);

                showcaseViewBuilder.setShowcaseEventListener(new ShowCaseEventListener() {
                    @Override
                    public void onShowcaseViewHide(ShowcaseView showcaseView) {
                        super.onShowcaseViewHide(showcaseView);
                        showcaseViewBuilder = new ShowcaseView.Builder(activity());
                        showcaseViewBuilder.withHoloShowcase().hideOnTouchOutside().setStyle(R.style.CustomShowcaseTheme6)
                                .setTarget(new ViewTarget(cartBadgeCount))
                                .setContentTitle(R.string.bracket_menu_guide1)
                                .setContentText(R.string.bracket_menu_guide2);
                        showcaseViewBuilder.setShowcaseEventListener(new ShowCaseEventListener() {
                            @Override
                            public void onShowcaseViewHide(ShowcaseView showcaseView) {
                                super.onShowcaseViewDidHide(showcaseView);
                                showcaseViewBuilder = new ShowcaseView.Builder(activity());
                                showcaseViewBuilder.withHoloShowcase().hideOnTouchOutside().setStyle(R.style.CustomShowcaseTheme4)
                                        .setTarget(new ViewTarget(searchEditText))
                                        .setContentTitle(R.string.search_menu_guide1)
                                        .setContentText(R.string.search_menu_guide2);
                                showcaseViewBuilder.setShowcaseEventListener(new ShowCaseEventListener() {
                                    @Override
                                    public void onShowcaseViewHide(ShowcaseView showcaseView) {
                                        super.onShowcaseViewHide(showcaseView);
                                        showcaseViewBuilder = new ShowcaseView.Builder(activity());
                                        showcaseViewBuilder.withMaterialShowcase().hideOnTouchOutside().setStyle(R.style.CustomShowcaseTheme5)
                                                .setTarget(new ViewTarget(recyclerView))
                                                .setContentTitle(R.string.home_menu_guide1)
                                                .setContentText(R.string.home_menu_guide2);
                                        showcaseViewBuilder.setShowcaseEventListener(new ShowCaseEventListener() {
                                            @Override
                                            public void onShowcaseViewDidHide(ShowcaseView showcaseView) {
                                                super.onShowcaseViewHide(showcaseView);
                                                showcaseViewBuilder = null ;
                                            }
                                        });
                                        showcaseViewBuilder.build();
                                    }
                                });
                                showcaseViewBuilder.build();
                            }

                        });
                        showcaseViewBuilder.build();
                    }
                }).build();
                return true ;
            }).subscribeOn(AndroidSchedulers.mainThread()).subscribe();

        }
    }
    public void renderBadge(){
        if(SystemApp.checkLogin(activity())){
            HttpInstance.call(HttpInstance.create(CartService.class).itemOnCart(), itemOnCart -> {
                ResponseTools.validate(activity(), itemOnCart);
                cart = itemOnCart.getCart() ;
                if(itemOnCart.getCart().getCount()==0){
                    cartBadgeCount.setText(String.valueOf(itemOnCart.getCart().getCount()));
                    cartBadgeCount.setVisibility(View.GONE);
                }else{
                    cartBadgeCount.setText(String.valueOf(itemOnCart.getCart().getCount()));
                    cartBadgeCount.setVisibility(View.VISIBLE);

                }
            }, throwable -> {
//                activity(HomeActivity.class).showSnackbar(throwable, v -> {});
            }, false);
        }else{
            cartBadgeCount.setVisibility(View.GONE);
        }
    }

    @Override
    public void onPause() {
        swipeRefreshLayout.setRefreshing(false);
        super.onPause();
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public String initTitle() {
        title = getString(R.string.home);
        return title;
    }

    @Override
    public boolean onBackPressed() {
        if(showcaseViewBuilder!=null){
            return false ;
        }
        freeMemory();
        getActivity().finish();
        return false ;
    }

    @Override
    public Object getObject() {
        return null;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return true;
    }


    private void autoMove(int timemilis, boolean forward){
        alfaBannerView.postDelayed(() -> {
            if(!(activity() instanceof HomeActivity)){
                return;
            }
            if(activity(HomeActivity.class)!=null && activity(HomeActivity.class).getWindow()!=null && !isDetached() && isVisible()){
                if(forward){
                    if(alfaBannerView.getCurrentItem()<alfaBannerView.getAdapter().getItemCount()-1){
                        alfaBannerView.smoothScrollToPosition(alfaBannerView.getCurrentItem()+1);
                        autoMove(timemilis, true);
                    }else{
                        alfaBannerView.smoothScrollToPosition(alfaBannerView.getCurrentItem()-1);
                        autoMove(timemilis, false);
                    }
                    return;
                }else{
                    if(alfaBannerView.getCurrentItem()>0){
                        alfaBannerView.smoothScrollToPosition(alfaBannerView.getCurrentItem()-1);
                        autoMove(timemilis, false);
                    }else{
                        alfaBannerView.smoothScrollToPosition(1);
                        autoMove(timemilis, true);
                    }
                    return;
                }
            }
        }, timemilis);
    }

    @Override
    public boolean keepSideMenu() {
        return true;
    }
}
