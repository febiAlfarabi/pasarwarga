package com.pasarwarga.market.fragment.profile;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.fragment.home.BaseDrawerFragment;
import com.alfarabi.base.http.basemarket.service.SettingService;
import com.alfarabi.base.tools.ResponseTools;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.pasarwarga.market.R;
import com.pasarwarga.market.fragment.SettingsFragment;
import com.pasarwarga.market.fragment.asseller.SellerDashboardFragment;

import org.apache.commons.lang.StringUtils;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by USER on 9/13/2017.
 */

public class FragmentUserProfile extends BaseDrawerFragment {

    public static final String TAG = FragmentUserProfile.class.getName();

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout;
    @BindView(R.id.profile_user_iv) CircleImageView imageView;
    @BindView(R.id.profile_seller_iv) CircleImageView storeImageView;
    @BindView(R.id.profile_name_tv) TextView nameTextView;
    @BindView(R.id.seller_name_tv) TextView storeNameTextView;
    @BindView(R.id.profile_account_status_tv) TextView accountStatusTextView;
    @BindView(R.id.profile_email_tv) TextView emailTextView;
    @BindView(R.id.profile_phone_tv) TextView phoneTextView;
    @BindView(R.id.profile_address_tv) TextView addressTextView;
    @BindView(R.id.seller_location_tv) TextView storeLocationTextView;
    @BindView(R.id.seller_ratingbar) RatingBar storeRatingBar;
    @BindView(R.id.profile_birth_tv) TextView birthDateTextView;
    @BindView(R.id.mystore_linearlayout) LinearLayout myStoreLinearLayout;


    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public int getMenuId() {
        return 0;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        if (item.getItemId() == com.alfarabi.base.R.id.action_profile_settings) {
            WindowFlow.withAnim(R.anim.enter_from_right, R.anim.exit_to_left).goTo(getActivity(), SettingsFragment.TAG, R.id.fragment_frame, true, false);
        }
        return false;
    }

    @Override
    public String initTitle() {
        title = getString(R.string.profile);
        toolbar.setTitle(title);
        return title;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_user_profile;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public boolean keepSideMenu() {
        return true;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        toolbar.inflateMenu(com.alfarabi.base.R.menu.toolbar_menu);
        toolbar.setOnMenuItemClickListener(this);
        showData();
    }

    public void showData() {
        if (myProfile != null) {
            nameTextView.setText(myProfile.getUser().getFirstName() + " " + myProfile.getUser().getLastName());
            if (myProfile.getUser().isVerified()) {
                accountStatusTextView.setText(R.string.verified_account);
            } else {
                accountStatusTextView.setText(R.string.unverified_account);
            }

            HttpInstance.withProgress(activity()).observe(HttpInstance.create(SettingService.class).getCurrentProfilePicture(), getProfilePictureResponse -> {
                ResponseTools.validate(activity(), getProfilePictureResponse);
                nameTextView.setText(getProfilePictureResponse.getUser().getFirstName() + " " + getProfilePictureResponse.getUser().getLastName());
                GlideApp.with(getContext()).load(getProfilePictureResponse.getUser().getThumbnail())
                        .placeholder(R.drawable.ic_pasarwarga)
                        .error(R.drawable.ic_pasarwarga).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL).into(imageView);
                GlideApp.with(getContext()).load(getProfilePictureResponse.getUser().getThumbnail()).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL).into(storeImageView);
            }, throwable -> {
                GlideApp.with(this).load(StringUtils.split(myProfile.getUser().getPhoto(), ",")[0])
                        .placeholder(R.drawable.ic_pasarwarga)
                        .error(R.drawable.ic_pasarwarga)
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                e.printStackTrace();
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                return false;
                            }
                        }).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL).into(imageView);
            });


            emailTextView.setText(myProfile.getUser().getEmail());
            phoneTextView.setText(myProfile.getUser().getPhone());
            addressTextView.setText(myProfile.getUser().getStateName() + ", " + myProfile.getUser().getDistrictName()
                    + ", " + myProfile.getUser().getZipCode() + "\n" + myProfile.getUser().getCityName());

            birthDateTextView.setText(myProfile.getUser().getBirthday());

            if (myProfile.getUser().isSeller()) {
                myStoreLinearLayout.setVisibility(View.VISIBLE);
                GlideApp.with(this).load(StringUtils.split(myProfile.getUser().getPhoto(), ",")[0])
                        .placeholder(R.drawable.ic_pasarwarga)
                        .error(R.drawable.ic_pasarwarga)
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                e.printStackTrace();
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                return false;
                            }
                        }).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL).into(storeImageView);
                storeNameTextView.setText(myProfile.getUser().getSellerInfo().getShopName());
                storeLocationTextView.setText(myProfile.getUser().getSellerInfo().getAddress());
                storeRatingBar.setRating(myProfile.getUser().getSellerInfo().getShoprating());

                myStoreLinearLayout.setOnClickListener(v -> {
                    WindowFlow.withAnim(R.anim.enter_from_right, R.anim.exit_to_left).goTo(getActivity(), SellerDashboardFragment.TAG, R.id.fragment_frame, true, false);
                });
            } else {
                myStoreLinearLayout.setVisibility(View.GONE);
            }
        }
    }
}
