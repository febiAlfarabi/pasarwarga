package com.pasarwarga.market.fragment.publix;


import android.support.v4.app.Fragment;

import com.pasarwarga.market.R;
import com.alfarabi.base.fragment.BasePublicFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class ForgetPasswordFragment extends BasePublicFragment {

    public static final String TAG = ForgetPasswordFragment.class.getName();


    public ForgetPasswordFragment() {}

    @Override
    public Object getObject() {
        return null;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_forget_password;
    }

    @Override
    public String getTAG() {
        return TAG;
    }
}
