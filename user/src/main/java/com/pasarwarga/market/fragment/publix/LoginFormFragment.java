package com.pasarwarga.market.fragment.publix;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.WLog;
import com.alfarabi.base.activity.BasePublicActivity;
import com.alfarabi.base.activity.SimpleBaseActivityInitiator;
import com.alfarabi.base.http.basemarket.service.LoginService;
import com.alfarabi.base.model.MyProfile;
import com.alfarabi.base.tools.Initial;
import com.alfarabi.base.tools.InputTools;
import com.hendraanggrian.preferencer.Preferencer;
import com.hendraanggrian.preferencer.Saver;
import com.hendraanggrian.preferencer.annotations.BindPreference;
import com.pasarwarga.market.activity.publix.ForgetPasswordActivity;
import com.pasarwarga.market.activity.publix.LoginActivity;
import com.pasarwarga.market.activity.publix.RegistrationActivity;
import com.alfarabi.base.fragment.login.BaseLoginFormFragment;
import com.pasarwarga.market.R;
import com.pasarwarga.market.tools.SystemApp;

import java.util.HashMap;

import butterknife.BindView;
import cn.nekocode.rxactivityresult.RxActivityResult;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFormFragment extends BaseLoginFormFragment {

    public static final String TAG = LoginFormFragment.class.getName();

    @BindView(R.id.login_btn) Button submitButton;
    @BindView(R.id.register_tv) TextView registerTextView;
    @BindView(R.id.forget_password_tv) TextView forgetPasswordTextView;
    @BindView(R.id.username_et) EditText editTextUsername;
    @BindView(R.id.password_et) EditText editTextPassword;

    @BindPreference String fcmToken ;
    Saver saver ;

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_login_form;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        saver = Preferencer.bind(this);
        submitButton.setOnClickListener(v -> {
            if(InputTools.isComplete(editTextUsername, editTextPassword)){
                HashMap<String, String> hashMap = new HashMap<String, String>();
                hashMap.put(LoginService.EMAIL_OR_USERNAME, editTextUsername.getText().toString());
                hashMap.put(LoginService.PASSWORD, editTextPassword.getText().toString());
                hashMap.put(LoginService.DEVICE_TOKEN, fcmToken);
                HttpInstance.withProgress(getActivity()).observe(HttpInstance.create(LoginService.class).login(hashMap), loginResponse -> {
                    if(loginResponse.isStatus() && loginResponse.getUser()!=null){
                        MyProfile myProfile = new MyProfile();
                        myProfile.setUser(loginResponse.getUser());
                        SystemApp.setProfile(myProfile);
                        HttpInstance.putHeaderMap(Initial.TOKEN, myProfile.getUser().getToken());
                        activity(LoginActivity.class).onBackPressed();
                    }else{
                        activity(SimpleBaseActivityInitiator.class).showDialog(loginResponse.getMessage(), (dialog, which) -> {
                            dialog.dismiss();
                        });
                    }
                }, throwable -> {
                    activity(SimpleBaseActivityInitiator.class).showSnackbar(throwable, v1 -> {});
                });
            }
        });
        registerTextView.setOnClickListener(v -> {
            RxActivityResult.startActivityForResult(activity(), new Intent(activity(LoginActivity.class), RegistrationActivity.class), BasePublicActivity.REGISTER)
                    .subscribe(result -> {
                        if (result.getRequestCode() == BasePublicActivity.REGISTER && result.isOk()) {
                            WLog.i(TAG, "ON Register Result ");
                            activity(LoginActivity.class).setResult(Activity.RESULT_OK);
                            activity(LoginActivity.class).finish();
                        }
                    });


        });
        forgetPasswordTextView.setOnClickListener(v -> {
//            WindowFlow.startActivity(getActivity(), ForgetPasswordActivity.class, false);
            RxActivityResult.startActivityForResult(activity(), new Intent(activity(LoginActivity.class), ForgetPasswordActivity.class), BasePublicActivity.FORGET_PASSWORD)
                    .subscribe(result -> {
                        if (result.getRequestCode() == BasePublicActivity.FORGET_PASSWORD && result.isOk()) {
                            activity(LoginActivity.class).setResult(Activity.RESULT_OK);
                            activity(LoginActivity.class).finish();
                        }
                    });


        });
    }

    @Override
    public Object getObject() {
        return null;
    }


//    InputFilter filter = new InputFilter() {
//        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
//            String filtered = "";
//            for (int i = start; i < end; i++) {
//                char character = source.charAt(i);
//                if (!Character.isWhitespace(character)) {
//                    filtered += character;
//                }
//            }
//
//            return filtered;
//        }
//
//    };

//    @Override
//    public boolean onBackPressed() {
//        if(!WindowFlow.canGoBack(activity(LoginActivity.class))){
//            WindowFlow.restart(activity(LoginActivity.class), SplashActivity.class);
//            return false ;
//        }
//        return true;
//    }
}
