package com.pasarwarga.market.fragment.registration;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.alfarabi.base.fragment.home.BaseDrawerFragment;
import com.kazy.lx.LxWebView;
import com.pasarwarga.market.MarketApplication;
import com.pasarwarga.market.R;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class SellerTermAndConditonFragment extends BaseDrawerFragment {
    
    public static final String TAG = SellerTermAndConditonFragment.class.getName();

    @BindView(R.id.toolbar) Toolbar toolbar ;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout ;
    @BindView(R.id.webview) LxWebView lxWebView ;

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public int getMenuId() {
        return 0;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    @Override
    public String initTitle() {
        return null;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_term_and_conditon;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        lxWebView.loadUrl(MarketApplication.batman()+"/mobile/pages/syarat-ketentuan-pengoperasian-toko");
    }
}
