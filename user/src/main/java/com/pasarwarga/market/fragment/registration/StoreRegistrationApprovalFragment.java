package com.pasarwarga.market.fragment.registration;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.fragment.home.BaseDrawerFragment;
import com.alfarabi.base.http.basemarket.service.SellerService;
import com.alfarabi.base.model.MyProfile;
import com.alfarabi.base.model.shop.SellerInfo;
import com.alfarabi.base.tools.Initial;
import com.alfarabi.base.tools.ResponseTools;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.SplashActivity;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.tools.SystemApp;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class StoreRegistrationApprovalFragment extends BaseDrawerFragment {
    
    public static final String TAG = StoreRegistrationApprovalFragment.class.getName();

    @BindView(R.id.toolbar) Toolbar toolbar ;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout ;
    @BindView(R.id.update_btn) Button updateButton ;

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public int getMenuId() {
        return R.id.register_store_menu_item;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    @Override
    public String initTitle() {
        return null;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_store_registration_approval;
    }

    @Override
    public boolean keepSideMenu() {
        return true;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        updateButton.setOnClickListener(v -> {


            MyProfile myProfile = SystemApp.myProfile();
            if(myProfile.getUser().isSeller() && myProfile.getUser().getSellerStatus()!=null && myProfile.getUser().getSellerStatus().equalsIgnoreCase(Initial.WAITING)){
                SellerInfo sellerInfo = myProfile.getUser().getSellerInfo();
                HttpInstance.withProgress(activity(HomeActivity.class)).observe(HttpInstance.create(SellerService.class).checkAdminActivation(sellerInfo.getId()), registerResponse -> {
                    ResponseTools.validate(activity(), registerResponse);
                    if(registerResponse.isStatus() && registerResponse.getHashMap().containsKey(SellerInfo.ADMIN_ACTIVATION)){
                        if(registerResponse.getHashMap().get(SellerInfo.ADMIN_ACTIVATION)){
                            sellerInfo.setStatus(Initial.ACTIVE);
                            myProfile.getUser().setSellerInfo(sellerInfo);
                            myProfile.getUser().setSeller(true);
                            myProfile.getUser().setSellerStatus(Initial.ACTIVE);
                            SystemApp.setProfile(myProfile);
                            WindowFlow.restart(activity(HomeActivity.class), SplashActivity.class);
                        }else{
                            activity(HomeActivity.class).showDialog(HttpInstance.getGson().toJson(registerResponse), (dialog, which) -> dialog.dismiss());
                        }
                    }else{
                        activity(HomeActivity.class).showDialog(registerResponse.getMessage(), (dialog, which) -> dialog.dismiss());
                    }
                }, throwable -> {
                    activity(HomeActivity.class).showSnackbar(throwable, v1 -> {});
                });
            }


        });
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
