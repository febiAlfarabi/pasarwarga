package com.pasarwarga.market.fragment.registration;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.WLog;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.fragment.home.BaseDrawerFragment;
import com.alfarabi.base.http.basemarket.service.CityService;
import com.alfarabi.base.http.basemarket.service.GoogleLocationService;
import com.alfarabi.base.http.basemarket.service.SellerService;
import com.alfarabi.base.model.location.City;
import com.alfarabi.base.model.location.District;
import com.alfarabi.base.model.MyProfile;
import com.alfarabi.base.model.location.Province;
import com.alfarabi.base.model.location.Subdistrict;
import com.alfarabi.base.model.location.ZipCode;
import com.alfarabi.base.model.geolocation.Prediction;
import com.alfarabi.base.model.shop.SellerInfo;
import com.alfarabi.base.tools.Initial;
import com.alfarabi.base.tools.InputTools;
import com.alfarabi.base.tools.ResponseTools;
import com.alfarabi.base.view.DataMaterialSpinner;
import com.alfarabi.base.view.StringMaterialSpinner;
import com.google.gson.Gson;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.tools.SystemApp;

import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class StoreRegistrationFormFragment extends BaseDrawerFragment {

    public static final String TAG = StoreRegistrationFormFragment.class.getName();

    @BindView(R.id.toolbar) Toolbar toolbar ;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout ;
    @BindView(R.id.term_and_condition_tv) HtmlTextView termAndConditionTextView ;
    @BindView(R.id.store_name_et) EditText storeNameEditText ;
    @BindView(R.id.your_location_et) AutoCompleteTextView yourLocationTextView ;
    @BindView(R.id.reference_code_et) EditText referenceCodeEditText ;
    @BindView(R.id.country_sp) StringMaterialSpinner countrySpinner ;
    @BindView(R.id.province_sp) DataMaterialSpinner<Province>  provinceSpinner;

    @BindView(R.id.city_sp) DataMaterialSpinner<City> citySpinner ;
    @BindView(R.id.district_sp) DataMaterialSpinner<District> districtSpinner ;
    @BindView(R.id.subdistrict_sp) DataMaterialSpinner<Subdistrict> subdistrictSpinner ;
    @BindView(R.id.postalcode_sp) DataMaterialSpinner<ZipCode> postalcodeSpinner ;

    @BindView(R.id.checkbox) CheckBox checkBox;
    @BindView(R.id.submit_btn) Button submitButton;

    private ArrayAdapter<String> locationAdapter ;

    public static StoreRegistrationFormFragment instance(){
        StoreRegistrationFormFragment registrationForm1Fragment = new StoreRegistrationFormFragment();
        return registrationForm1Fragment;
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public int getMenuId() {
        return 0;
    }

    @Override
    public String getTAG() {
        return TAG ;
    }

    @Override
    public String initTitle() {
        return null;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_store_registration_form;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        termAndConditionTextView.setOnTouchListener((v, event) -> {
            WindowFlow.goTo(activity(HomeActivity.class), SellerTermAndConditonFragment.TAG, R.id.fragment_frame, true, false);
            return false ;
        });

        countrySpinner.initDataAdapter(getResources().getStringArray(R.array.country_names), (parent, view1, position, id) -> {
            requestProvince(getResources().getIntArray(R.array.country_codes)[position]);
        }, getResources().getStringArray(R.array.country_names)[0]);
        countrySpinner.setFocusable(false);
        countrySpinner.setClickable(false);
        requestProvince(getResources().getIntArray(R.array.country_codes)[0]);


        yourLocationTextView.setThreshold(0);

        yourLocationTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().equals("")){
                    return;
                }
                HttpInstance.call(HttpInstance.create(GoogleLocationService.class, "https://maps.googleapis.com")
                        .search(s.toString(), "geocode", Initial.EN, Initial.GOOGLE_PLACE_KEY), searchPlace -> {
                    if(searchPlace.isOk()){
                        List<Prediction> predictions = searchPlace.getPredictions();
                        String[] locations = new String[predictions.size()];
                        for (int i = 0; i < locations.length; i++) {
                            locations[i] = predictions.get(i).getStructuredFormatting().getMainText()+" "+predictions.get(i).getStructuredFormatting().getSecondaryText();
                        }
                        locationAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.select_dialog_item, locations);
                        yourLocationTextView.setThreshold(1);
                        yourLocationTextView.setAdapter(locationAdapter);
                        locationAdapter.notifyDataSetChanged();
                    }

                }, throwable -> {

                }, false);
            }
        });

        submitButton.setOnClickListener(v -> {
//            WindowFlow.goTo(activity(HomeActivity.class), StoreRegistrationApprovalFragment.TAG, R.id.fragment_frame, true);

            //, referenceCodeEditText not mandatory
            if(InputTools.isComplete(storeNameEditText, yourLocationTextView, countrySpinner, provinceSpinner, citySpinner, districtSpinner, subdistrictSpinner, postalcodeSpinner)
                    && checkBox.isChecked()){
                HashMap<String, String> hashMap = new HashMap<String, String>();
                hashMap.put("seller_businessname", storeNameEditText.getText().toString());
                hashMap.put("shop_location", yourLocationTextView.getText().toString());
                hashMap.put("country_id",countrySpinner.getSelectedDsi().toString());
                hashMap.put("province_id", provinceSpinner.getSelectedDsi().getName());
                hashMap.put("city_id", citySpinner.getSelectedDsi().getName());
                hashMap.put("kecamatan_id", districtSpinner.getSelectedDsi().getName());
                hashMap.put("kelurahan_id", subdistrictSpinner.getSelectedDsi().getName());
                hashMap.put("zip_code", postalcodeSpinner.getSelectedDsi().getZipcode());
                if(!referenceCodeEditText.getText().equals("")){
                    hashMap.put("reference", referenceCodeEditText.getText().toString());
                }

                HttpInstance.withProgress(activity(HomeActivity.class)).observe(HttpInstance.create(SellerService.class).register(hashMap), registerResponse -> {
                    ResponseTools.validate(activity(), registerResponse);
                    if(registerResponse.isStatus() && registerResponse.getSellerInfo()!=null){
                        SellerInfo sellerInfo = registerResponse.getSellerInfo();
                        MyProfile myProfile = SystemApp.myProfile();
                        myProfile.getUser().setSellerInfo(sellerInfo);
                        myProfile.getUser().setSeller(true);
                        myProfile.getUser().setSellerStatus(Initial.WAITING);
                        SystemApp.setProfile(myProfile);
                        WindowFlow.goTo(activity(HomeActivity.class), StoreRegistrationApprovalFragment.TAG, R.id.fragment_frame, true);
                    }else{
                        activity(HomeActivity.class).showDialog(registerResponse.getMessage(), (dialog, which) -> dialog.dismiss());
                    }
                }, throwable -> {
                    activity(HomeActivity.class).showSnackbar(throwable, v1 -> {});
                });
            }
        });
    }

    public void requestProvince(int countryCode){
        HttpInstance.withProgress(activity()).observe(HttpInstance.create(CityService.class).getProvinces(countryCode), provinceResponse -> {
            ResponseTools.validate(getActivity(), provinceResponse);
            if(provinceResponse.isStatus() && provinceResponse.getProvinces().size()>0) {
                provinceSpinner.initDataAdapter(provinceResponse.getProvinces(), (parent, view1, position, id) -> {
                    WLog.i(TAG, new Gson().toJson(provinceSpinner.getSelectedDsi()));
                    requestCity(Integer.valueOf(provinceSpinner.getSelectedDsi().getId()));
                }, provinceResponse.getProvinces().size() > 0 ? provinceResponse.getProvinces().get(0) : null);
                requestCity(Integer.valueOf(provinceSpinner.getSelectedDsi().getId()));
            }else{
                activity(HomeActivity.class).showSnackbar(provinceResponse.getMessage(), v -> {});
            }
        }, throwable -> {
            activity(HomeActivity.class).showSnackbar(throwable, v -> {});
            throwable.printStackTrace();
        });
    }

    public void requestCity(long provinceId){
        HttpInstance.withProgress(activity()).observe(HttpInstance.create(CityService.class).getCities(provinceId), cityResponse -> {
            ResponseTools.validate(getActivity(), cityResponse);
            if(cityResponse.isStatus() && cityResponse.getCities().size()>0) {
                citySpinner.initDataAdapter(cityResponse.getCities(), (parent, view1, position, id) -> {
                    WLog.i(TAG, new Gson().toJson(citySpinner.getSelectedDsi()));
                    requestDistrict(Long.valueOf(citySpinner.getSelectedDsi().getId()));
                }, cityResponse.getCities().size() > 0 ? cityResponse.getCities().get(0) : null);
                requestDistrict(Long.valueOf(citySpinner.getSelectedDsi().getId()));
            }else{
                activity(HomeActivity.class).showSnackbar(cityResponse.getMessage(), v -> {});
            }
        }, throwable -> {
            throwable.printStackTrace();
            activity(HomeActivity.class).showSnackbar(throwable.getMessage(), v -> {});
        }, false);
    }

    public void requestDistrict(long cityId){
        HttpInstance.withProgress(activity()).observe(HttpInstance.create(CityService.class).getKecamatan(cityId), districtResponse -> {
            ResponseTools.validate(getActivity(), districtResponse);
            if(districtResponse.isStatus() && districtResponse.getDistricts().size()>0) {
                districtSpinner.initDataAdapter(districtResponse.getDistricts(), (parent, view1, position, id) -> {
                    if (districtSpinner.getSelectedDsi() != null) {
                        requestSubdistrict(Long.valueOf(districtSpinner.getSelectedDsi().getId()));
                    }
                }, districtResponse.getDistricts().size() > 0 ? districtResponse.getDistricts().get(0) : null);
                requestSubdistrict(Long.valueOf(districtSpinner.getSelectedDsi().getId()));
            }else{
                activity(HomeActivity.class).showSnackbar(districtResponse.getMessage(), v -> {});
            }
        }, throwable -> {
            throwable.printStackTrace();
            activity(HomeActivity.class).showSnackbar(throwable.getMessage(), v -> {});
        }, false);
    }

    public void requestSubdistrict(long districtId){
        HttpInstance.withProgress(activity()).observe(HttpInstance.create(CityService.class).getKelurahan(districtId), subdistrictResponse -> {
            ResponseTools.validate(getActivity(), subdistrictResponse);
            if(subdistrictResponse.isStatus() && subdistrictResponse.getSubdistricts().size()>0) {
                subdistrictSpinner.initDataAdapter(subdistrictResponse.getSubdistricts(), (parent, view1, position, id) -> {
                    requestZipcode(Long.valueOf(subdistrictSpinner.getSelectedDsi().getId()));
                }, subdistrictResponse.getSubdistricts().size() > 0 ? subdistrictResponse.getSubdistricts().get(0) : null);
                requestZipcode(Long.valueOf(subdistrictSpinner.getSelectedDsi().getId()));
            }else{
                activity(HomeActivity.class).showSnackbar(subdistrictResponse.getMessage(), v -> {});
            }
        }, throwable -> {
            throwable.printStackTrace();
            activity(HomeActivity.class).showSnackbar(throwable.getMessage(), v -> {});
        }, false);
    }

    public void requestZipcode(long subdistrictId){
        HttpInstance.withProgress(activity()).observe(HttpInstance.create(CityService.class).getZipCode(subdistrictId), zipCodeResponse -> {
            ResponseTools.validate(getActivity(), zipCodeResponse);
            if(zipCodeResponse.isStatus() && zipCodeResponse.getZipCodes().size()>0) {
                postalcodeSpinner.initDataAdapter(zipCodeResponse.getZipCodes(), (parent, view1, position, id) -> {

                }, zipCodeResponse.getZipCodes().size() > 0 ? zipCodeResponse.getZipCodes().get(0) : null);
            }else{
                activity(HomeActivity.class).showSnackbar(zipCodeResponse.getMessage(), v -> {});
            }
        }, throwable -> {
            throwable.printStackTrace();
            activity(HomeActivity.class).showSnackbar(throwable.getMessage(), v -> {});
        }, false);
    }

    public void invalid(){

    }

    @Override
    public Object getObject() {
        return null;
    }


    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }
}
