package com.pasarwarga.market.fragment.registration;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.fragment.home.BaseDrawerFragment;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.tools.SystemApp;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class StoreRegistrationLandingFragment extends BaseDrawerFragment {
    
    public static final String TAG = StoreRegistrationLandingFragment.class.getName();

    @BindView(R.id.toolbar) Toolbar toolbar ;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout ;
    @BindView(R.id.register_btn) Button registerButton ;

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public int getMenuId() {
        return 0;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    @Override
    public String initTitle() {
        return null;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_store_registration_landing;
    }

    @Override
    public boolean keepSideMenu() {
        return true;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        registerButton.setOnClickListener(v -> {
            if(!SystemApp.checkVerified(activity())){
                return;
            }
            WindowFlow.goTo(activity(HomeActivity.class), StoreRegistrationFormFragment.TAG, R.id.fragment_frame, true, false);
        });
    }
}
