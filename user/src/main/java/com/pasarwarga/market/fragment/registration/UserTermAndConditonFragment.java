package com.pasarwarga.market.fragment.registration;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.alfarabi.alfalibs.tools.AssetUtils;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.fragment.BasePublicFragment;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.publix.RegistrationActivity;

import org.sufficientlysecure.htmltextview.HtmlTextView;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserTermAndConditonFragment extends BasePublicFragment {
    
    public static final String TAG = UserTermAndConditonFragment.class.getName();

    @BindView(R.id.toolbar) Toolbar toolbar ;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout ;
    @BindView(R.id.html_textview) HtmlTextView htmlTextView ;

    public Toolbar getToolbar() {
        return toolbar;
    }

    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    public int getMenuId() {
        return 0;
    }

    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    public String initTitle() {
        return null;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_user_term_and_conditon;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        toolbar.setNavigationOnClickListener(v -> {
            WindowFlow.backstack(activity(RegistrationActivity.class));
        });
        htmlTextView.setHtml(AssetUtils.readHtmlFile(activity(), "user_agreement.html"));
    }

    @Override
    public boolean onBackPressed() {
        WindowFlow.backstack(activity(RegistrationActivity.class));
        return false;
    }
}
