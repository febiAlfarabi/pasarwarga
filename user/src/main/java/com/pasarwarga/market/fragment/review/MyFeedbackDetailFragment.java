package com.pasarwarga.market.fragment.review;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.alfarabi.alfalibs.fragments.interfaze.SimpleFragmentCallback;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.fragment.BaseUserFragment;
import com.alfarabi.base.model.review.Feedback;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.ornolfr.ratingview.RatingView;
import com.hendraanggrian.bundler.annotations.BindExtra;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyFeedbackDetailFragment extends BaseUserFragment<Feedback> implements SimpleFragmentCallback<Feedback> {
    
    public static final String TAG = MyFeedbackDetailFragment.class.getName();


    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout;

    @BindView(R.id.user_iv) CircleImageView userImageView ;
    @BindView(R.id.name_tv) TextView nameTextView ;
    @BindView(R.id.transaction_id_tv) TextView transactionIdTextView ;
    @BindView(R.id.date_tv) TextView dateTextView ;

    @BindView(R.id.product_iv) ImageView productImageView ;
    @BindView(R.id.productname_tv) TextView productNameTextView ;
    @BindView(R.id.comment_tv) TextView commentTextView ;
    @BindView(R.id.ratingview) RatingView ratingView ;


    @BindExtra Feedback feedback ;


    @Override
    public String initTitle() {
        title = getString(R.string.review);
        toolbar.setTitle(title);
        return title;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_my_feedback_detail;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setObject(feedback);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        toolbar.setNavigationOnClickListener(v -> WindowFlow.backstack(activity(HomeActivity.class)));
        GlideApp.with(this).load(feedback.getSeller().getStoreImage()).error(R.drawable.thumbnail_profile).placeholder(R.drawable.thumbnail_profile)
                .skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL).into(userImageView);
        nameTextView.setText(feedback.getSeller().getShopName());
        transactionIdTextView.setText(getString(R.string.transaction_id)+" # "+feedback.getDealCodeNumber());
        dateTextView.setText(feedback.getCreatedAt());

        GlideApp.with(this).load(feedback.getProductImage()).error(R.drawable.ic_pasarwarga).placeholder(R.drawable.ic_pasarwarga)
                .skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL).into(productImageView);
        productNameTextView.setText(feedback.getProductName());
        ratingView.setRating((float)feedback.getRating());
        commentTextView.setText(feedback.getDescription());

    }
}
