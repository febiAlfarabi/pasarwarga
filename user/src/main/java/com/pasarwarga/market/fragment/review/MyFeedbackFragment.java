package com.pasarwarga.market.fragment.review;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.alfarabi.alfalibs.adapters.recyclerview.SimpleRecyclerAdapter;
import com.alfarabi.alfalibs.fragments.interfaze.SimpleFragmentCallback;
import com.alfarabi.alfalibs.helper.SwipeCallback;
import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.alfarabi.alfalibs.views.AlfaSwipeRefreshLayout;
import com.alfarabi.base.fragment.BaseUserFragment;
import com.alfarabi.base.http.basemarket.service.ReviewService;
import com.alfarabi.base.model.review.Feedback;
import com.alfarabi.base.model.review.Review;
import com.alfarabi.base.tools.ResponseTools;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.holder.recyclerview.review.MyFeedbackViewHolder;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * Created by Alfarabi on 6/19/17.
 */

public class MyFeedbackFragment extends BaseUserFragment<ArrayList<Feedback>> implements SimpleFragmentCallback<ArrayList<Feedback>>{

    public static final String TAG = MyFeedbackFragment.class.getName();

    @BindView(R.id.loader) MKLoader loader;
    @BindView(R.id.recyclerview) AlfaRecyclerView recyclerView ;
    @BindView(R.id.swiperefreshlayout) AlfaSwipeRefreshLayout swipeRefreshLayout ;
    SimpleRecyclerAdapter<Feedback, NewFeedbackFragment, MyFeedbackViewHolder> simpleRecyclerAdapter ;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        simpleRecyclerAdapter  = new SimpleRecyclerAdapter(this, MyFeedbackViewHolder.class, new ArrayList<Review>());

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        simpleRecyclerAdapter.initRecyclerView(recyclerView, new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        swipeRefreshLayout.setOnSwipeListener(new SwipeCallback() {
            @Override
            public void onSwipeFromTop() {
//                swipeRefreshLayout.finishRefresh();
                basicRequest(swipeRefreshLayout.startPage());
            }

            @Override
            public void onSwipeFromBottom() {
//                swipeRefreshLayout.finishRefreshLoadMore();
                basicRequest(swipeRefreshLayout.increasePage());
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        loader.setVisibility(View.VISIBLE);
        basicRequest(swipeRefreshLayout.startPage());
    }



    public void basicRequest(int page){

        HttpInstance.call(HttpInstance.create(ReviewService.class).feedbacks(page), feedbacks -> {
            ResponseTools.validate(activity(), feedbacks);
            loader.setVisibility(View.GONE);
            swipeRefreshLayout.finishRefresh();
            swipeRefreshLayout.finishRefreshLoadMore();
            if(feedbacks.isStatus()){
                if(page==1){
                    simpleRecyclerAdapter.setObjects(feedbacks.getFeedbacks());
                    return;
                }
                simpleRecyclerAdapter.getObjects().addAll(feedbacks.getFeedbacks());
                simpleRecyclerAdapter.notifyDataSetChanged();
            }else{
                activity(HomeActivity.class).showDialog(feedbacks.getMessage(), (dialog, which) -> {});
            }

        }, throwable -> {
            loader.setVisibility(View.GONE);
            activity(HomeActivity.class).showSnackbar(throwable, v -> {});
            swipeRefreshLayout.finishRefresh();
            swipeRefreshLayout.finishRefreshLoadMore();
        }, false);

    }


    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_my_feedback;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public String initTitle() {
        return title = getString(R.string.review);
    }

    @Override
    public ArrayList<Feedback> getObject() {
        if(simpleRecyclerAdapter!=null){
            return (ArrayList<Feedback>) simpleRecyclerAdapter.getObjects();
        }
        return null;
    }
}
