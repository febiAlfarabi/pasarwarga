package com.pasarwarga.market.fragment.review;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.alfarabi.alfalibs.adapters.viewpager.BaseTabPagerAdapter;
import com.alfarabi.base.fragment.BaseUserFragment;
import com.alfarabi.base.fragment.home.BaseDrawerTabFragment;
import com.alfarabi.base.model.User;
import com.alfarabi.base.view.TabLayout;
import com.pasarwarga.market.R;
import com.pasarwarga.market.adapter.FeedbackTabPagerAdapter;

import butterknife.BindView;

/**
 * Created by Alfarabi on 6/20/17.
 */

public class ReviewTabFragment extends BaseDrawerTabFragment<User> {

    public static String TAG = ReviewTabFragment.class.getName();

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout;
    @BindView(R.id.tablayout) TabLayout tabLayout ;
    @BindView(R.id.viewpager) ViewPager viewpager ;


    private FeedbackTabPagerAdapter<BaseUserFragment> feedbackTabPagerAdapter ;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        feedbackTabPagerAdapter = new FeedbackTabPagerAdapter(getChildFragmentManager(), getContext());
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();
        initTabLayout(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
//                feedbackTabPagerAdapter.getFragments()[position].onResume();
            }
            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_review_tab;
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public BaseTabPagerAdapter<?> baseTabPagerAdapter() {
        return feedbackTabPagerAdapter ;
    }

    @Override
    public TabLayout getTabLayout() {
        return tabLayout;
    }

    @Override
    public ViewPager getViewPager() {
        return viewpager ;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public String initTitle() {
        title = getString(R.string.review);
        toolbar.setTitle(title);
        return title;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    @Override
    public int getMenuId() {
//        return R.id.transaction_review_menu_item;
        return 0;
    }

    @Override
    public User getObject() {
        return null;
    }

    @Override
    public boolean keepSideMenu() {
        return true;
    }
}
