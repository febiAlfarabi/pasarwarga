package com.pasarwarga.market.fragment.seller;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.alfarabi.alfalibs.adapters.viewpager.BaseTabPagerAdapter;
import com.alfarabi.base.model.transaction.CartItem;
import com.pasarwarga.market.R;
import com.pasarwarga.market.adapter.TransactionStatusTabPagerAdapter;
import com.alfarabi.base.fragment.home.BaseDrawerTabFragment;
import com.alfarabi.base.view.TabLayout;

import java.util.List;

import butterknife.BindView;

/**
 * Created by Alfarabi on 6/20/17.
 */

public class OrderTabFragment extends BaseDrawerTabFragment<List<CartItem>> {

    public static String TAG = OrderTabFragment.class.getName();

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout;
    @BindView(R.id.tablayout) TabLayout tabLayout ;
    @BindView(R.id.viewpager) ViewPager viewpager ;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initTabLayout(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_transaction_tab;
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public BaseTabPagerAdapter<?> baseTabPagerAdapter() {
        return new TransactionStatusTabPagerAdapter<>(getFragmentManager(), getContext());
    }

    @Override
    public TabLayout getTabLayout() {
        return tabLayout;
    }

    @Override
    public ViewPager getViewPager() {
        return viewpager ;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public String initTitle() {
        title = getString(R.string.transaction);
        toolbar.setTitle(title);
        return title;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    @Override
    public int getMenuId() {
//        return R.id.sub_seller_menu_item_2;
        return 0;
    }

    @Override
    public List<CartItem> getObject() {
        return null;
    }

    @Override
    public boolean keepSideMenu() {
        return true;
    }
}
