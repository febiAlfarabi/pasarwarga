package com.pasarwarga.market.fragment.seller;

import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.alfarabi.base.fragment.home.BaseDrawerFragment;

/**
 * Created by Alfarabi on 7/7/17.
 */

public class ProfileSellerFragment extends BaseDrawerFragment {


    @Override
    public Toolbar getToolbar() {
        return null;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return null;
    }

    @Override
    public int getMenuId() {
        return 0;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    @Override
    public String initTitle() {
        return null;
    }

    @Override
    public int contentXmlLayout() {
        return 0;
    }

    @Override
    public Object getObject() {
        return null;
    }
}
