package com.pasarwarga.market.fragment.store;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.Gilimanuk;
import com.alfarabi.alfalibs.tools.WLog;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.fragment.home.BaseDrawerFragment;
import com.alfarabi.base.http.basemarket.service.CityService;
import com.alfarabi.base.http.basemarket.service.AddressService;
import com.alfarabi.base.model.location.Address;
import com.alfarabi.base.model.location.City;
import com.alfarabi.base.model.location.Province;
import com.alfarabi.alfalibs.tools.Caster;
import com.alfarabi.base.tools.Initial;
import com.alfarabi.base.tools.InputTools;
import com.alfarabi.alfalibs.tools.KeyboardUtils;
import com.alfarabi.base.tools.ResponseTools;
import com.alfarabi.base.view.DataMaterialSpinner;
import com.google.gson.Gson;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;

import org.parceler.Parcels;

import java.util.List;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddressDetailFragment extends BaseDrawerFragment {

    public static final String TAG = AddressDetailFragment.class.getName();

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout;
    @BindView(R.id.province_sp) DataMaterialSpinner<Province> provinceSpinner;
    @BindView(R.id.city_et) AutoCompleteTextView cityAutoCompleteTextView;
    @BindView(R.id.postalcode_et) EditText postalCodeEditText;
    @BindView(R.id.name_et) EditText nameEditText;
    @BindView(R.id.addreses1_et) EditText address1EditText;
    @BindView(R.id.addreses2_et) EditText address2EditText;
    @BindView(R.id.phonenumber_et) EditText phoneNumberEditText;
    @BindView(R.id.save_button) Button saveButton ;

    private Address address;

    private ArrayAdapter<String> cityAdapter;
    private String[] cities;
    private City city;
    List<City> cityList;

    public static AddressDetailFragment instance() {
        AddressDetailFragment registrationForm1Fragment = new AddressDetailFragment();
        return registrationForm1Fragment;
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public int getMenuId() {
        return 0;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public String initTitle() {
        title = getString(R.string.shipping_address);
        toolbar.setTitle(title);
        return title;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_address_form;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().containsKey(Initial.ADDRESS)) {
            WLog.i(TAG, "ADDRESS PARCELABLE");
            address = Parcels.unwrap(getArguments().getParcelable(Initial.ADDRESS));
        }
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        requestProvince();

        if (address != null) {
            nameEditText.setText(address.getName());
            address1EditText.setText(address.getAddress1());
            address2EditText.setText(address.getAddress2());
            provinceSpinner.setText(address.getState());
            cityAutoCompleteTextView.setText(address.getCity());
            postalCodeEditText.setText(address.getPostalCode());
            phoneNumberEditText.setText(address.getPhone());
        }

        saveButton.setOnClickListener(v -> {
            if (InputTools.isComplete(nameEditText, address1EditText, address2EditText, provinceSpinner,
                    cityAutoCompleteTextView, postalCodeEditText, phoneNumberEditText)) {
                if (address == null) {
                    // Create new Address
                    address = new Address();
                    address.setName(nameEditText.getText().toString());
                    address.setAddress1(address1EditText.getText().toString());
                    address.setAddress2(address2EditText.getText().toString());
                    address.setCountry(getString(R.string.local_country));
                    address.setState(provinceSpinner.getText().toString());
                    address.setCity(cityAutoCompleteTextView.getText().toString());
                    address.setPostalCode(postalCodeEditText.getText().toString());
                    address.setPhone(phoneNumberEditText.getText().toString());
                    HttpInstance.withProgress(activity()).observe(HttpInstance.create(AddressService.class).create(address), get -> {
                        ResponseTools.validate(activity(), get);
                        Gilimanuk.backstack(activity(HomeActivity.class));
                    }, throwable -> {
                        activity(HomeActivity.class).showSnackbar(throwable, v1 -> {});
                    });
                } else {
                    address.setName(nameEditText.getText().toString());
                    address.setAddress1(address1EditText.getText().toString());
                    address.setAddress2(address2EditText.getText().toString());
                    address.setCountry(getString(R.string.local_country));
                    address.setState(provinceSpinner.getText().toString());
                    address.setCity(cityAutoCompleteTextView.getText().toString());
                    address.setPostalCode(postalCodeEditText.getText().toString());
                    address.setPhone(phoneNumberEditText.getText().toString());
                    HttpInstance.withProgress(activity()).observe(HttpInstance.create(AddressService.class).update(address), get -> {
                        ResponseTools.validate(activity(), get);
                        Gilimanuk.backstack(activity(HomeActivity.class));
                    }, throwable -> {
                        activity(HomeActivity.class).showSnackbar(throwable, v1 -> {});
                    });
                }
            }
        });

    }

    @Override
    public Object getObject() {
        return address;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    public void requestProvince() {
        HttpInstance.call(HttpInstance.create(CityService.class).getProvinces(91), provinceResponse -> {
            ResponseTools.validate(getActivity(), provinceResponse);
            provinceSpinner.initDataAdapter(provinceResponse.getProvinces(), (parent, view1, position, id) -> {
                WLog.i(TAG, new Gson().toJson(provinceSpinner.getSelectedDsi()));
                requestCity(Integer.valueOf(provinceSpinner.getSelectedDsi().getId()));
            }, null);
        }, throwable -> {
            throwable.printStackTrace();
        });
    }

    public void requestCity(long provinceId) {
        HttpInstance.call(HttpInstance.create(CityService.class).getCities(provinceId), cityResponse -> {
            ResponseTools.validate(getActivity(), cityResponse);
            cityList = cityResponse.getCities();
            cities = new String[cityList.size()];
            for (int i = 0; i < cityList.size(); i++) {
                cities[i] = cityList.get(i).getName();
            }
            cityAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.select_dialog_item, cities);
            cityAutoCompleteTextView.setThreshold(1);
            cityAutoCompleteTextView.setAdapter(cityAdapter);
            if (cities.length > 0) {
                cityAutoCompleteTextView.setText(cities[0]);
            }
            cityAutoCompleteTextView.setOnItemClickListener((parent, view1, position, id) -> {
                if (city != null && city.getName().equalsIgnoreCase(cityAutoCompleteTextView.getText().toString())) {
                    return;
                }
                for (int i = 0; i < cityList.size(); i++) {
                    if (cityList.get(i).getName().equalsIgnoreCase(cityAutoCompleteTextView.getText().toString())) {
                        city = cityList.get(i);
                    }
                }
                if (city != null) {
                    WLog.i(TAG, "CITY :::: " + new Gson().toJson(city));
                    KeyboardUtils.hideKeyboard(getActivity());
//                    requestDistrict(Long.valueOf(city.getId()));
                }
            });
        }, throwable -> {
            throwable.printStackTrace();
            activity(HomeActivity.class).showSnackbar(throwable.getMessage(), v -> {
            });
        }, false);
    }

//    public void requestDistrict(long cityId){
//        HttpInstance.call(HttpInstance.create(CityService.class).getKecamatan(cityId), districtResponse -> {
//            ResponseTools.validate(getActivity(), districtResponse);
//            districtSpinner.initDataAdapter(districtResponse.getDistricts(), (parent, view1, position, id) -> {
//                if(districtSpinner.getSelectedDsi()!=null){
//                    requestSubdistrict(Long.valueOf(districtSpinner.getSelectedDsi().getId()));
//                }
//            }, null);
//        }, throwable -> {
//            throwable.printStackTrace();
//            activity(HomeActivity.class).showSnackbar(throwable.getMessage(), v -> {});
//        }, false);
//    }
//
//    public void requestSubdistrict(long districtId){
//        HttpInstance.call(HttpInstance.create(CityService.class).getKelurahan(districtId), subdistrictResponse -> {
//            ResponseTools.validate(getActivity(), subdistrictResponse);
//            subdistrictSpinner.initDataAdapter(subdistrictResponse.getSubdistricts(), (parent, view1, position, id) -> {
//                if(subdistrictSpinner.getSelectedDsi()!=null) {
//                    requestZipcode(Long.valueOf(subdistrictSpinner.getSelectedDsi().getId()));
//                }
//
//            }, null);
//        }, throwable -> {
//            throwable.printStackTrace();
//            activity(HomeActivity.class).showSnackbar(throwable.getMessage(), v -> {});
//        }, false);
//    }

//    public void requestZipcode(long subdistrictId){
//        HttpInstance.call(HttpInstance.create(CityService.class).getZipCode(subdistrictId), zipCodeResponse -> {
//            ResponseTools.validate(getActivity(), zipCodeResponse);
//            postalcodeSpinner.initDataAdapter(zipCodeResponse.getZipCodes(), (parent, view1, position, id) -> {
//
//            }, null);
//        }, throwable -> {
//            throwable.printStackTrace();
//            activity(HomeActivity.class).showSnackbar(throwable.getMessage(), v -> {});
//        }, false);
//    }
}
