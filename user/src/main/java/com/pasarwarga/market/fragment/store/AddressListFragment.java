package com.pasarwarga.market.fragment.store;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.alfarabi.alfalibs.adapters.recyclerview.SimpleRecyclerAdapter;
import com.alfarabi.alfalibs.helper.SwipeCallback;
import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.Gilimanuk;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.alfarabi.alfalibs.views.AlfaSwipeRefreshLayout;
import com.alfarabi.base.fragment.home.BaseDrawerFragment;
import com.alfarabi.base.http.basemarket.service.AddressService;
import com.alfarabi.base.model.Product;
import com.alfarabi.base.model.location.Address;
import com.alfarabi.alfalibs.tools.Caster;
import com.alfarabi.base.tools.ResponseTools;
import com.hendraanggrian.bundler.Bundler;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.holder.recyclerview.AddressViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import lombok.Getter;

/**
 * Created by Alfarabi on 6/19/17.
 */

public class AddressListFragment extends BaseDrawerFragment {

    public static final String TAG = AddressListFragment.class.getName();

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout;
    @BindView(R.id.recyclerview) AlfaRecyclerView recyclerView ;
    @BindView(R.id.swiperefreshlayout) AlfaSwipeRefreshLayout swipeRefreshLayout ;

    @Getter SimpleRecyclerAdapter<Address, AddressListFragment, AddressViewHolder> simpleRecyclerAdapter ;

    private String searchInput = "" ;
    private int column = 2 ;

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        List<Address> addresses = new ArrayList<>();
        simpleRecyclerAdapter = new SimpleRecyclerAdapter<>(this, AddressViewHolder.class, addresses);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        toolbar.inflateMenu(R.menu.add_menu);
        toolbar.setOnMenuItemClickListener(this);
        simpleRecyclerAdapter.initRecyclerView(recyclerView, new LinearLayoutManager(activity()));

        swipeRefreshLayout.setOnSwipeListener(new SwipeCallback() {
            @Override
            public void onSwipeFromTop() {
                swipeRefreshLayout.finishRefresh();
            }

            @Override
            public void onSwipeFromBottom() {
                swipeRefreshLayout.finishRefreshLoadMore();
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        init();

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    public void init(){
        swipeRefreshLayout.autoRefresh();
        HttpInstance.withProgress(activity(HomeActivity.class)).observe(HttpInstance.create(AddressService.class).getAll(), getAll -> {
            ResponseTools.validate(activity(), getAll);
            swipeRefreshLayout.finishRefresh();
            if(getAll.isStatus() && getAll.getAddresses()!=null && getAll.getAddresses().size()>0){
                simpleRecyclerAdapter.setObjects(getAll.getAddresses());
            }
        }, throwable -> {
            swipeRefreshLayout.finishRefresh();
            activity(HomeActivity.class).showSnackbar(throwable, v -> {});
        }, false);
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_address_list;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public Object getObject() {
        return null;
    }

    @Override
    public String initTitle() {
        title = getString(R.string.shipping_address);
        toolbar.setTitle(title);
        return title;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        Gilimanuk.withAnim(appearAnims).goTo(activity(HomeActivity.class), AddressDetailFragment.TAG, R.id.fragment_frame, true, false);
        return true;
    }

    @Override
    public int getMenuId() {
        return 0;
    }

    @Override
    public boolean keepSideMenu() {
        return false;
    }

}
