package com.pasarwarga.market.fragment.store;

import android.app.ProgressDialog;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.AppDialog;
import com.alfarabi.alfalibs.tools.AppProgress;
import com.alfarabi.alfalibs.tools.Gilimanuk;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.alfalibs.tools.WLog;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.fragment.home.BaseDrawerFragment;
import com.alfarabi.base.http.basemarket.service.CartService;
import com.alfarabi.base.http.basemarket.service.AddressService;
import com.alfarabi.base.http.basemarket.service.ExpeditionService;
import com.alfarabi.base.http.basemarket.service.InsuranceService;
import com.alfarabi.base.http.basemarket.service.ProductService;
import com.alfarabi.base.http.basemarket.service.ShippingService;
import com.alfarabi.base.model.location.Address;
import com.alfarabi.base.model.transaction.Cart;
import com.alfarabi.base.model.transaction.CartItem;
import com.alfarabi.base.model.Expedition;
import com.alfarabi.base.model.Insurance;
import com.alfarabi.base.model.Product;
import com.alfarabi.base.model.Shipping;
import com.alfarabi.base.model.transaction.finance.kreditplus.KPCheckoutParam;
import com.alfarabi.base.realm.RealmDao;
import com.alfarabi.base.tools.InputTools;
import com.alfarabi.base.tools.ResponseTools;
import com.alfarabi.base.view.DataMaterialSpinner;
import com.alfarabi.base.view.StringMaterialSpinner;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.hendraanggrian.bundler.Bundler;
import com.hendraanggrian.bundler.annotations.BindExtra;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
//import com.pasarwarga.market.activity.publix.VerificationActivity;
import com.tuyenmonkey.mkloader.MKLoader;

import java.text.NumberFormat;
import java.util.HashMap;

import butterknife.BindView;

/**
 * Created by Alfarabi on 6/19/17.
 */

public class CartFragment extends BaseDrawerFragment<Cart> {

    public static final String TAG = CartFragment.class.getName();

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout;
    @BindView(R.id.item_iv) ImageView itemImageView;
    @BindView(R.id.item_original_price_tv) TextView itemOriginalPriceTextView;
    @BindView(R.id.item_price_tv) TextView itemPriceTextView;
    @BindView(R.id.item_name_tv) TextView itemNameTextView;
    @BindView(R.id.discount_tv) TextView itemDiscountTextView;
    @BindView(R.id.quantity_tv) TextView quantityTextView ;

    @BindView(R.id.total_price_tv) TextView totalPriceTextView;
    @BindView(R.id.product_price_tv) TextView productPriceTextView;
    @BindView(R.id.shipping_cost_tv) TextView shippingCostTextView;
    @BindView(R.id.insurance_tv) TextView insuranceTextView;
    @BindView(R.id.admin_fee_tv) TextView adminFeeTextView;
    @BindView(R.id.voucher_tv) TextView voucherTextView;
    @BindView(R.id.add_coupon_button) Button couponCodeButton;


    @BindView(R.id.loader) MKLoader loader;
    @BindView(R.id.cart_item_layout) LinearLayout cartItemLayout;
    @BindView(R.id.cart_notes_et) EditText cartNoteEditText;
    @BindView(R.id.cart_coupon_et) EditText cartCouponEditText;
    @BindView(R.id.address_sp) DataMaterialSpinner<Address> addressSpinner;
    @BindView(R.id.service_sp) DataMaterialSpinner<Shipping> shippingSpinner;
    @BindView(R.id.expedition_sp) DataMaterialSpinner<Expedition> expeditionSpinner;
//    @BindView(R.id.payment_sp) StringMaterialSpinner paymentSpinner;
    @BindView(R.id.insurance_sp) DataMaterialSpinner<Insurance> insuranceSpinner;

    @BindView(R.id.choosed_address_tv) TextView choosedAddressTextView;
    @BindView(R.id.cart_address_button) Button cartAddressButton;
    @BindView(R.id.cart_buy_now_button) Button buyButton;

    @Nullable @BindExtra Product product;
    private CartItem cartItem;
    private Cart cart;
    KPCheckoutParam kpCheckoutParam;
    String couponAmount;
    String input = "";


    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        kpCheckoutParam = new KPCheckoutParam();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        toolbar.inflateMenu(com.alfarabi.base.R.menu.cart_menu);
        toolbar.setOnMenuItemClickListener(this);
        choosedAddressTextView.setText(R.string.loading);
        cartItemLayout.setVisibility(LinearLayout.INVISIBLE);
        loader.setVisibility(MKLoader.VISIBLE);

        cartAddressButton.setOnClickListener(v -> Gilimanuk.withAnim(appearAnims).goTo(activity(HomeActivity.class), AddressListFragment.TAG, R.id.fragment_frame, true, false));

//        paymentSpinner.initDataAdapter(activity().getResources().getStringArray(R.array.registered_payment_list), (parent, view1, position, id) -> {
//        }, activity().getResources().getStringArray(R.array.registered_payment_list)[0]);

        cartCouponEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                input = s.toString();

                if (input.length() > 0) {
                    addCouponCode();
                    calculateAndRender();
                } else {
                    cancelCouponCode();
                    calculateAndRender();
                }
            }
        });
        initChartItem();
        //addCouponCode();
    }

    @Override
    public int getMenuId() {
        return 0;
    }


    public void initChartItem(){
        AppProgress.showBasic(activity(), R.string.loading, true);
        HttpInstance.call(HttpInstance.create(CartService.class).itemOnCart(), itemOnCart -> {
            ResponseTools.validate(activity(), itemOnCart);
            AppProgress.dismissBasic();
            if (itemOnCart.isStatus() && itemOnCart.getCart().getCount() > 0 ) {
                cartItemLayout.setVisibility(LinearLayout.VISIBLE);
                loader.setVisibility(MKLoader.INVISIBLE);
                cartItem = itemOnCart.getCart().getCartItems().get(0);
                cart = itemOnCart.getCart();
                renderICartItem();
            }

//            if (itemOnCart.isStatus() && itemOnCart.getCart().getCount() <= 0 && product.getId() != 0) {
//                HashMap<String, String> hashMap = new HashMap();
//                hashMap.put("quantity", "1");
//                hashMap.put("product_id", String.valueOf(product.getId()));
//                HttpInstance.withProgress(activity(HomeActivity.class)).observe(HttpInstance.create(CartService.class).addToCart(hashMap), addToCart -> {
//                    ResponseTools.validate(activity(), addToCart);
//                    if (addToCart.isStatus()) {
//                        cartItemLayout.setVisibility(LinearLayout.VISIBLE);
//                        loader.setVisibility(MKLoader.INVISIBLE);
//                        cartItem = addToCart.getCart().getCartItems().get(0);
//                        cart = addToCart.getCart();
//                        renderICartItem();
//                    } else {
//                        activity(HomeActivity.class).showSnackbar(addToCart.getMessage(), v -> {});
//                        WindowFlow.backstack(activity(HomeActivity.class));
//                    }
//                }, throwable -> {
//                    activity(HomeActivity.class).showDialog(throwable, (dialog, which) -> {
//                        dialog.dismiss();
//                        WindowFlow.backstack(activity(HomeActivity.class));
//                    });
//                }, false);
//            } else if (itemOnCart.isStatus() && itemOnCart.getCart().getCount() > 0 && product.getId()==0) {
//                cartItemLayout.setVisibility(LinearLayout.VISIBLE);
//                loader.setVisibility(MKLoader.INVISIBLE);
//                cartItem = itemOnCart.getCart().getCartItems().get(0);
//                cart = itemOnCart.getCart();
//                renderICartItem();
//            } else if (itemOnCart.isStatus() && itemOnCart.getCart().getCount() > 0 && product.getId()!=0) {
//                cartItemLayout.setVisibility(LinearLayout.VISIBLE);
//                loader.setVisibility(MKLoader.INVISIBLE);
//                cartItem = itemOnCart.getCart().getCartItems().get(0);
//                cart = itemOnCart.getCart();
//                renderICartItem();
//                activity(HomeActivity.class).showDialog(getString(R.string.warning), getString(R.string.previous_cart_available_warning), (dialog, which) -> {
//                    dialog.dismiss();
//                });
//            }


        }, throwable -> {
            AppProgress.dismissBasic();
            activity(HomeActivity.class).showDialog(throwable, (dialog, which) -> {
                dialog.dismiss();
                WindowFlow.backstack(activity(HomeActivity.class));
            });
        }, false);
    }

    public void getUserAddress(){
        HttpInstance.call(HttpInstance.create(AddressService.class).getAll(), getAll -> {
            ResponseTools.validate(this, getAll);
            if (getAll.isStatus() && getAll.getAddresses() != null && getAll.getAddresses().size() > 0) {
                choosedAddressTextView.setText(R.string.select_address);
                addressSpinner.initDataAdapter(getAll.getAddresses(), (parent, view, position, id) -> {
                    choosedAddressTextView.setText(addressSpinner.getSelectedDsi().getAddress1()
                            + "\n" + addressSpinner.getSelectedDsi().getAddress2()
                            + "\n" + addressSpinner.getSelectedDsi().getCountry()
                            + "\n" + addressSpinner.getSelectedDsi().getCity()
                            + "\n" + addressSpinner.getSelectedDsi().getPostalCode()
                            + "\n" + addressSpinner.getSelectedDsi().getPhone()
                    );
                    if (shippingSpinner.getSelectedDsi() != null) {
                        getExpedition(shippingSpinner.getSelectedDsi().getCode());
                    }
                    getShipping(cartItem.getProductId(), addressSpinner.getSelectedDsi().getId());
                }, getAll.getAddresses().get(0));
                choosedAddressTextView.setText(addressSpinner.getSelectedDsi().getAddress1()
                        + "\n" + addressSpinner.getSelectedDsi().getAddress2()
                        + "\n" + addressSpinner.getSelectedDsi().getCountry()
                        + "\n" + addressSpinner.getSelectedDsi().getCity()
                        + "\n" + addressSpinner.getSelectedDsi().getPostalCode()
                        + "\n" + addressSpinner.getSelectedDsi().getPhone()
                );
                getShipping(cartItem.getProductId(), addressSpinner.getSelectedDsi().getId());
            } else {
                choosedAddressTextView.setText(R.string.no_saved_address);
            }
        }, throwable -> activity(HomeActivity.class).showSnackbar(throwable, v -> {}), false);
    }

    public void addCouponCode() {
        couponCodeButton.setOnClickListener(v -> {
            if(InputTools.isComplete(cartCouponEditText)){
                activity(HomeActivity.class).showDialog(getString(R.string.confirmation), getString(R.string.do_you_want_to_use_voucher), (dialog, which) -> {
                    HashMap<String, String> hashMap = new HashMap<String, String>();
                    hashMap.put("coupon_code", cartCouponEditText.getText().toString());
                    HttpInstance.withProgress(activity()).observe(HttpInstance.create(CartService.class).addCouponCode(hashMap), addCouponCode -> {
                        ResponseTools.validate(activity(), addCouponCode);
                        if(addCouponCode.isStatus()){
                            cart.setCouponDiscountAmmount(addCouponCode.getCart().getCouponDiscountAmmount());
                            cartCouponEditText.setText("");
                            couponCodeButton.setBackgroundColor(getContext().getResources().getColor(R.color.grey_300));
                            couponCodeButton.setText(R.string.cancel);
                            voucherTextView.setText(getString(R.string.curr_symb) +"-"+ NumberFormat.getInstance().format(addCouponCode.getCart().getCouponDiscountAmmount()));
                        }else{
                            activity(HomeActivity.class).showDialog(getString(R.string.warning), addCouponCode.getMessage(), (dialog1, which1) -> dialog.dismiss());
                        }
                    }, throwable -> activity(HomeActivity.class).showDialog(throwable, (dialog1, which1) -> dialog1.dismiss()), false);
                }, (dialog, which) -> dialog.dismiss());
            }
        });
    }

    public void cancelCouponCode() {
        couponCodeButton.setOnClickListener(v -> {
            HttpInstance.call(HttpInstance.create(CartService.class).removeCouponCode(String.valueOf(cartItem.getId())), removeCouponCode -> {
                ResponseTools.validate(activity(), removeCouponCode);
                if (removeCouponCode.isStatus()) {
                    cart.setCouponDiscountAmmount(removeCouponCode.getCart().getCouponDiscountAmmount());
                    couponCodeButton.setText(R.string.add);
                    cartCouponEditText.setText("");
                    //voucherTextView.setText("");
                    voucherTextView.setText(getString(R.string.curr_symb) + getString(R.string.nil));
                    couponCodeButton.setBackgroundColor(getContext().getResources().getColor(R.color.green_500));
                } else {
                    activity(HomeActivity.class).showDialog(getString(R.string.warning), removeCouponCode.getMessage(), (dialog1, which1) -> dialog1.dismiss());
                }
            }, throwable -> activity(HomeActivity.class).showDialog(throwable, (dialog1, which1) -> dialog1.dismiss()));
        });
    }

    public void cancelCouponCode2() {
        HttpInstance.call(HttpInstance.create(CartService.class).removeCouponCode(String.valueOf(cartItem.getId())), removeCouponCode -> {
            ResponseTools.validate(activity(), removeCouponCode);
            couponCodeButton.setText(R.string.add_coupon);
            cartCouponEditText.setText("");
            voucherTextView.setText("");
        }, throwable -> activity(HomeActivity.class).showDialog(throwable, (dialog1, which1) -> dialog1.dismiss()));
    }

    public void renderICartItem() {
        buyButton.setEnabled(false);
        buyButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.disable_button));
        if (cartItem != null) {
            GlideApp.with(this).load(cartItem.getImage()).placeholder(R.drawable.ic_product_default).error(R.drawable.ic_product_default)
                    .skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL).into(itemImageView);
            itemNameTextView.setText(cartItem.getProductName());
            quantityTextView.setText(getString(R.string.quantity)+" : "+String.valueOf(1));
            itemPriceTextView.setText(getString(R.string.curr_symb) + NumberFormat.getInstance().format(cartItem.getFinalPrice()));
            if (cartItem.getDiscount() == 0) {
                itemDiscountTextView.setVisibility(View.GONE);
                itemOriginalPriceTextView.setVisibility(View.GONE);
                itemPriceTextView.setText(getString(R.string.curr_symb) + NumberFormat.getInstance().format(cartItem.getFinalPrice()));
            } else {
                itemDiscountTextView.setVisibility(View.VISIBLE);
                itemDiscountTextView.setText(String.valueOf(cartItem.getDiscount()+ getString(R.string.percent_symb))+" "+getString(R.string.off));
                itemOriginalPriceTextView.setPaintFlags(itemOriginalPriceTextView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                itemOriginalPriceTextView.setText(getString(R.string.curr_symb) + NumberFormat.getInstance().format(cartItem.getProductPrice()));
                itemPriceTextView.setText(getString(R.string.curr_symb) + NumberFormat.getInstance().format(cartItem.getFinalPrice()));
            }
            HttpInstance.call(HttpInstance.create(InsuranceService.class).getInsurance(String.valueOf(cartItem.getId())), getInsurances -> {
                ResponseTools.validate(activity(), getInsurances);
                if (product == null || product.getId() == 0) {
                    HttpInstance.call(HttpInstance.create(ProductService.class).getProduct(Long.valueOf(cartItem.getProductId())), productResponse -> {

                        if (getInsurances.isStatus() && getInsurances.getInsurances().size() > 0) {
                            this.product = RealmDao.instance().insertOrUpdate(productResponse.getProduct());
                            insuranceSpinner.initDataAdapter(getInsurances.getInsurances(), (parent, view, position, id) -> {
                                calculateAndRender();
                            }, getInsurances.getInsurances().get(0));
                        } else {
                            activity(HomeActivity.class).showSnackbar(getInsurances.getMessage(), v -> {});
                        }
                    }, throwable -> activity(HomeActivity.class).showSnackbar(getInsurances.getMessage(), v -> {
                    }), false);
                } else {
                    if (getInsurances.isStatus() && getInsurances.getInsurances().size() > 0) {
                        insuranceSpinner.initDataAdapter(getInsurances.getInsurances(), (parent, view, position, id) -> {
                            calculateAndRender();
                        }, getInsurances.getInsurances().get(0));
                    } else {
                        activity(HomeActivity.class).showSnackbar(getInsurances.getMessage(), v -> {});
                    }
                }

            }, throwable -> activity(HomeActivity.class).showSnackbar(throwable, v -> {}), false);
            calculateAndRender();
            getUserAddress();
        }
    }

    public void calculateAndRender() {
        if (product != null) {
            kpCheckoutParam.setProduct(product);
        }
        if (cartItem != null) {
            kpCheckoutParam.setProductPrice(cartItem.getFinalPrice());
            kpCheckoutParam.setCartId(String.valueOf(cartItem.getId()));
        }

        if (cart != null) {
            kpCheckoutParam.setCart(cart);
            kpCheckoutParam.setAdministrationFee(cart.getAdminFee());
        }
        if (addressSpinner.getSelectedDsi() != null) {
            kpCheckoutParam.setAddressId(addressSpinner.getSelectedDsi().getId());
        }
        if (insuranceSpinner.getSelectedDsi() != null) {
            kpCheckoutParam.setInsuranceId(String.valueOf(insuranceSpinner.getSelectedDsi().getId()));
            kpCheckoutParam.setInsurance(insuranceSpinner.getSelectedDsi().getValue());
        }
        if (shippingSpinner.getSelectedDsi() != null) {
            kpCheckoutParam.setShippingServiceId(shippingSpinner.getSelectedDsi().getId());
            kpCheckoutParam.setShippCode(shippingSpinner.getSelectedDsi().getCode());
        }
        if (expeditionSpinner.getSelectedDsi() != null) {
            kpCheckoutParam.setShippingCost(expeditionSpinner.getSelectedDsi().getPrice());
            kpCheckoutParam.setShippingServiceType(expeditionSpinner.getSelectedDsi().getName());
        }
        if (voucherTextView.getText() != null) {
            kpCheckoutParam.setVoucher(cart.getCouponDiscountAmmount());
        }
        if (expeditionSpinner.getSelectedDsi() != null && cartItem != null) {

        }

        totalPriceTextView.setText(getString(R.string.curr_symb) + NumberFormat.getInstance().format(kpCheckoutParam.getTotalPrice()));
        productPriceTextView.setText(getString(R.string.curr_symb) + NumberFormat.getInstance().format(kpCheckoutParam.getProductPrice()));
        shippingCostTextView.setText(getString(R.string.curr_symb) + NumberFormat.getInstance().format(kpCheckoutParam.getShippingCost()));
        insuranceTextView.setText(getString(R.string.curr_symb) + NumberFormat.getInstance().format(kpCheckoutParam.getInsurance()));
        adminFeeTextView.setText(getString(R.string.curr_symb) + NumberFormat.getInstance().format(kpCheckoutParam.getAdministrationFee()));
        voucherTextView.setText(getString(R.string.curr_symb) + NumberFormat.getInstance().format(kpCheckoutParam.getVoucher()));

        buyButton.setEnabled(true);
        buyButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.red_shape_button));

        buyButton.setOnClickListener(v -> {
            if (InputTools.isComplete(shippingSpinner, expeditionSpinner, addressSpinner)) {
                if (cartItem.isInsurance()) {
                    if (!InputTools.isComplete(insuranceSpinner)) {
                        return;
                    }
                    kpCheckoutParam.setInsurance(insuranceSpinner.getSelectedDsi().getValue());
                    kpCheckoutParam.setInsuranceId(String.valueOf(insuranceSpinner.getSelectedDsi().getId()));
                }
                if (!cartNoteEditText.getText().toString().equalsIgnoreCase("")) {
                    kpCheckoutParam.setNote(cartNoteEditText.getText().toString());
                }
                if (addressSpinner.getSelectedDsi() != null) {
                    kpCheckoutParam.setAddressId(addressSpinner.getSelectedDsi().getId());
                }
                if (addressSpinner.getSelectedDsi() != null) {
                    kpCheckoutParam.setAddressId(addressSpinner.getSelectedDsi().getId());
                }
                if (insuranceSpinner.getSelectedDsi() != null) {
                    kpCheckoutParam.setInsuranceId(String.valueOf(insuranceSpinner.getSelectedDsi().getId()));
                    kpCheckoutParam.setInsurance(insuranceSpinner.getSelectedDsi().getValue());
                }
                if (shippingSpinner.getSelectedDsi() != null) {
                    kpCheckoutParam.setShippingServiceId(shippingSpinner.getSelectedDsi().getId());
                    kpCheckoutParam.setShippCode(shippingSpinner.getSelectedDsi().getCode());
                }
                if (expeditionSpinner.getSelectedDsi() != null) {
                    kpCheckoutParam.setShippingCost(expeditionSpinner.getSelectedDsi().getPrice());
                    kpCheckoutParam.setShippingServiceType(expeditionSpinner.getSelectedDsi().getName());
                }
                if (voucherTextView.getText() != null) {
                    kpCheckoutParam.setVoucher(cart.getCouponDiscountAmmount());
                }
                if (!cartCouponEditText.getText().toString().equalsIgnoreCase("")) {
                    kpCheckoutParam.setCouponCode(cartCouponEditText.getText().toString());
                }
                WLog.i(TAG, HttpInstance.getGson().toJson(product));
                WindowFlow.withAnim(R.anim.slide_in_from_right, R.anim.slide_out_to_right)
                        .goTo(activity(HomeActivity.class), CartPaymentMethodFragment.TAG, Bundler.wrap(CartPaymentMethodFragment.class, kpCheckoutParam, cart), R.id.fragment_frame, true);
            }

        });
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_cart;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public Cart getObject() {
        return cart;
    }

    @Override
    public String initTitle() {
        title = getString(R.string.your_cart);
        toolbar.setTitle(title);
        return title;
    }


    public void getShipping(String productId, String addressId){
        AppProgress.showBasic(activity(), R.string.loading, true);
        HttpInstance.call(HttpInstance.create(ShippingService.class).availableShipping(productId, addressId), availableShipping -> {
            ResponseTools.validate(activity(), availableShipping);
            AppProgress.dismissBasic();
            if (availableShipping.isStatus() && availableShipping.getShippings() != null && availableShipping.getShippings().size() > 0) {
                shippingSpinner.initDataAdapter(availableShipping.getShippings(), (parent, view, position, id) -> {
                    if(expeditionSpinner.getThisAdapter()!=null){
                        expeditionSpinner.getThisAdapter().getDsis().clear();
                        expeditionSpinner.getThisAdapter().notifyDataSetChanged();
                    }
                    getExpedition(shippingSpinner.getSelectedDsi().getCode());
                }, availableShipping.getShippings().size()>0?availableShipping.getShippings().get(0):null);
                getExpedition(shippingSpinner.getSelectedDsi().getCode());
            } else {
                activity(HomeActivity.class).showDialog(getString(R.string.no_shipping_found), (dialog, which) -> dialog.dismiss());
                shippingSpinner.setSelectedDsi(null);
                if(shippingSpinner.getThisAdapter()!=null){
                    shippingSpinner.getThisAdapter().getDsis().clear();
                    shippingSpinner.getThisAdapter().notifyDataSetChanged();
                    expeditionSpinner.setSelectedDsi(null);
                }

                if(expeditionSpinner.getThisAdapter()!=null){
                    expeditionSpinner.getThisAdapter().getDsis().clear();
                    expeditionSpinner.getThisAdapter().notifyDataSetChanged();
                }


            }
        }, throwable -> {
            activity(HomeActivity.class).showSnackbar(throwable, v -> {});
            AppProgress.dismissBasic();
        }, false);
    }

    public void getExpedition(String shippingCode) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("address_id", addressSpinner.getSelectedDsi().getId());
        hashMap.put("seller_id", cartItem.getSellerId());
        hashMap.put("shipp_code", shippingCode);
        AppProgress.showBasic(activity(), R.string.loading, true);
        HttpInstance.call(HttpInstance.create(ExpeditionService.class).expeditionService(hashMap), expeditionService -> {
            ResponseTools.validate(activity(), expeditionService);
            AppProgress.dismissBasic();
            if (expeditionService.isStatus() && expeditionService.getExpeditions() != null && expeditionService.getExpeditions().size() > 0) {
                expeditionSpinner.initDataAdapter(expeditionService.getExpeditions(), (parent, view, position, id) -> {
                    calculateAndRender();
                }, expeditionService.getExpeditions().size()>0?expeditionService.getExpeditions().get(0):null);
            } else {
                activity(HomeActivity.class).showDialog(getString(R.string.no_expedition_found), (dialog, which) -> dialog.dismiss());
                expeditionSpinner.setSelectedDsi(null);
                if(expeditionSpinner.getThisAdapter()!=null){
                    expeditionSpinner.getThisAdapter().getDsis().clear();
                    expeditionSpinner.getThisAdapter().notifyDataSetChanged();
                }
            }
        }, throwable -> {
            activity(HomeActivity.class).showSnackbar(throwable, v -> {});
            AppProgress.dismissBasic();
        }, false);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        if (item.getItemId() == com.alfarabi.base.R.id.delete_menu) {
            AppDialog.with(getContext()).dialog(com.alfarabi.base.R.string.remove_dialog_title
                    , com.alfarabi.base.R.string.remove_cart_confirm, (dialog, which) -> {
                        HashMap<String, String> hashMap = new HashMap<String, String>();
                        hashMap.put("cart_id", String.valueOf(cartItem.getId()));
                        dialog.dismiss();
                        HttpInstance.withProgress(getContext()).observe(HttpInstance.create(CartService.class).deleteFromCart(hashMap), deleteFromCart -> {
                            ResponseTools.validate(activity(), deleteFromCart);
                            if (deleteFromCart.isStatus()) {
                                WindowFlow.backstack(activity(HomeActivity.class));
                            } else {
                                activity(HomeActivity.class).showDialog(deleteFromCart.getMessage(), (dialog1, which1) -> dialog1.dismiss());
                            }
                        }, throwable -> activity(HomeActivity.class).showDialog(throwable, (dialog1, which1) -> dialog1.dismiss()));
                        dialog.dismiss();
                    }, (dialog, which) -> dialog.dismiss());
        }
        return false;
    }

    @Override
    public boolean keepSideMenu() {
        return false;
    }
}
