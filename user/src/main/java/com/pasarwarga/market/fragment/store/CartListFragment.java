package com.pasarwarga.market.fragment.store;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.alfarabi.alfalibs.adapters.recyclerview.SimpleRecyclerAdapter;
import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.AppDialog;
import com.alfarabi.alfalibs.tools.AppProgress;
import com.alfarabi.alfalibs.tools.Gilimanuk;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.alfarabi.alfalibs.views.AlfaSwipeRefreshLayout;
import com.alfarabi.base.fragment.home.BaseDrawerFragment;
import com.alfarabi.base.http.basemarket.service.CartService;
import com.alfarabi.base.model.Product;
import com.alfarabi.base.model.transaction.Cart;
import com.alfarabi.base.model.transaction.CartItem;
import com.alfarabi.base.tools.ResponseTools;
import com.hendraanggrian.bundler.Bundler;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.fragment.home.HomeFragment;
import com.pasarwarga.market.holder.recyclerview.CartViewHolder;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

/**
 * Created by Alfarabi on 6/19/17.
 */

public class CartListFragment extends BaseDrawerFragment<List<CartItem>> {

    public static final String TAG = CartListFragment.class.getName();

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout;
    @BindView(R.id.recyclerview) AlfaRecyclerView recyclerView ;
    @BindView(R.id.add_button) Button addButton ;
    @BindView(R.id.swiperefreshlayout) AlfaSwipeRefreshLayout swipeRefreshLayout;
    SimpleRecyclerAdapter<CartItem, CartListFragment, CartViewHolder> simpleRecyclerAdapter ;

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        toolbar.setOnMenuItemClickListener(this);
        simpleRecyclerAdapter  = new SimpleRecyclerAdapter(this, CartViewHolder.class, new ArrayList<CartItem>());

        simpleRecyclerAdapter.initRecyclerView(recyclerView, new LinearLayoutManager(activity()));
    }

    @Override
    public int getMenuId() {
        return 0;
    }

    @Override
    public void onResume() {
        super.onResume();
        AppProgress.showBasic(activity(), R.string.loading, true);
        HttpInstance.call(HttpInstance.create(CartService.class).itemOnCart(), itemOnCart -> {
            ResponseTools.validate(activity(), itemOnCart);
            AppProgress.dismissBasic();
            if(itemOnCart.isStatus() && itemOnCart.getCart().getCount()>0){
                simpleRecyclerAdapter.setObjects(itemOnCart.getCart().getCartItems());
//                addButton.setText(getString(R.string.apply_now));
                addButton.setText(getString(R.string.see_another_product));
                addButton.setOnClickListener(v -> {
                    Gilimanuk.backstack(activity(HomeActivity.class), HomeFragment.TAG, R.id.fragment_frame);
//                    Gilimanuk.withAnim(appearAnims).goTo(activity(HomeActivity.class), CartFragment.TAG, Bundler.wrap(CartFragment.class, new Product()), R.id.fragment_frame, true, false);
                });
            }else{
                addButton.setText(getString(R.string.start_shop));
                addButton.setOnClickListener(v -> {
                    Gilimanuk.backstack(activity(HomeActivity.class), HomeFragment.TAG, R.id.fragment_frame);
                });
            }
        }, throwable -> {
            AppProgress.dismissBasic();
            activity(HomeActivity.class).showDialog(throwable, (dialog, which) -> {
                dialog.dismiss();
                WindowFlow.backstack(activity(HomeActivity.class));
            });
        }, false);
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_cart_list;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public List<CartItem> getObject() {
        return simpleRecyclerAdapter.getObjects();
    }

    @Override
    public String initTitle() {
        title = getString(R.string.your_cart);
        toolbar.setTitle(title);
        return title;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        if(item.getItemId()== com.alfarabi.base.R.id.delete_menu){
            AppDialog.with(getContext()).dialog(com.alfarabi.base.R.string.remove_dialog_title
                    , com.alfarabi.base.R.string.remove_cart_confirm, (dialog, which) -> {
                        dialog.dismiss();
                    }, (dialog, which) -> dialog.dismiss());
        }
        return false;
    }

    @Override
    public boolean keepSideMenu() {
        return false;
    }
}
