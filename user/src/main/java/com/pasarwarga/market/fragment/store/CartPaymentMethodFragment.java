package com.pasarwarga.market.fragment.store;


import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.alfarabi.base.fragment.home.BaseDrawerFragment;
import com.alfarabi.base.http.basemarket.service.CartService;
import com.alfarabi.base.model.transaction.Cart;
import com.alfarabi.base.model.Payment;
import com.alfarabi.base.model.transaction.finance.kreditplus.KPCheckoutParam;
import com.alfarabi.base.tools.ResponseTools;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.hendraanggrian.bundler.Bundler;
import com.hendraanggrian.bundler.annotations.BindExtra;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.adapter.ButtonOptionAdapter;
import com.pasarwarga.market.fragment.store.transaction.finance.KPInstallmentFragment;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import it.sephiroth.android.library.tooltip.Tooltip;

/**
 * A simple {@link Fragment} subclass.
 */
public class CartPaymentMethodFragment extends BaseDrawerFragment<Cart> {
    
    public static final String TAG = CartPaymentMethodFragment.class.getName();

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout;
    @BindView(R.id.payment_rv) AlfaRecyclerView paymentRecyclerView ;

    @BindView(R.id.total_price_tv) TextView totalPriceTextView ;
    @BindView(R.id.product_price_tv) TextView productPriceTextView ;
    @BindView(R.id.shipping_cost_tv) TextView shippingCostTextView ;
    @BindView(R.id.insurance_tv) TextView insuranceTextView ;
    @BindView(R.id.admin_fee_tv) TextView adminFeeTextView ;
    @BindView(R.id.voucher_tv) TextView voucherTextView ;

    @BindView(R.id.item_iv) ImageView itemImageView ;
    @BindView(R.id.item_name_tv) TextView itemNameTextView ;
    @BindView(R.id.discount_tv) TextView itemDiscountTextView;
    @BindView(R.id.quantity_tv) TextView quantityTextView ;
    @BindView(R.id.item_original_price_tv) TextView itemOriginalPriceTextView ;
    @BindView(R.id.item_price_tv) TextView itemPriceTextView ;
    @BindView(R.id.description_tv) TextView descriptionTextView ;

    @BindView(R.id.process_button) Button processButton ;
    private ButtonOptionAdapter<Payment> paymentOptionAdapter ;

    private List<Payment> payments = new ArrayList<>();
    @BindExtra KPCheckoutParam kpCheckoutParam;


    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public int getMenuId() {
        return 0;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    @Override
    public String initTitle() {
        title = getString(R.string.payment_method);
        toolbar.setTitle(title);
        return title;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_cart_payment_method;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        paymentOptionAdapter = new ButtonOptionAdapter<>(activity(), payments);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        paymentRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        paymentRecyclerView.setAdapter(paymentOptionAdapter);
        HttpInstance.withProgress(activity()).observe(HttpInstance.create(CartService.class).getPaymentMethod(),paymentMethod -> {
            ResponseTools.validate(this, paymentMethod);
            paymentOptionAdapter.setmItems(paymentMethod.getPayments());

        }, throwable -> {
            activity(HomeActivity.class).showSnackbar(throwable, v -> WindowFlow.backstack(activity(HomeActivity.class)));
        });

        if(kpCheckoutParam.getNote()!=null){
            descriptionTextView.setText(kpCheckoutParam.getNote());
        }else{
            descriptionTextView.setText("-");
        }
        calculateAndRender();

    }

    public void calculateAndRender(){

        GlideApp.with(this).load(kpCheckoutParam.getCart().getCartItems().get(0).getImage()).placeholder(R.drawable.ic_product_default).error(R.drawable.ic_product_default)
                .skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL).into(itemImageView);
        itemNameTextView.setText(kpCheckoutParam.getCart().getCartItems().get(0).getProductName());
        quantityTextView.setText(getString(R.string.quantity)+" : "+String.valueOf(1));
        itemPriceTextView.setText(getString(R.string.curr_symb) + NumberFormat.getInstance().format(kpCheckoutParam.getCart().getCartItems().get(0).getFinalPrice()));
        if (kpCheckoutParam.getCart().getCartItems().get(0).getDiscount() == 0) {
            itemDiscountTextView.setVisibility(View.GONE);
            itemOriginalPriceTextView.setVisibility(View.GONE);
            itemPriceTextView.setText(getString(R.string.curr_symb) + NumberFormat.getInstance().format(kpCheckoutParam.getCart().getCartItems().get(0).getFinalPrice()));
        } else {
            itemDiscountTextView.setVisibility(View.VISIBLE);
            itemDiscountTextView.setText(String.valueOf(kpCheckoutParam.getCart().getCartItems().get(0).getDiscount()+ getString(R.string.percent_symb))+" "+getString(R.string.off));
            itemOriginalPriceTextView.setPaintFlags(itemOriginalPriceTextView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            itemOriginalPriceTextView.setText(getString(R.string.curr_symb) + NumberFormat.getInstance().format(kpCheckoutParam.getCart().getCartItems().get(0).getProductPrice()));
            itemPriceTextView.setText(getString(R.string.curr_symb) + NumberFormat.getInstance().format(kpCheckoutParam.getCart().getCartItems().get(0).getFinalPrice()));
        }


//        quantityTextView.setText(getString(R.string.quantity)+" : "+String.valueOf(1));
//        itemOriginalPriceTextView.setText(getString(R.string.curr_symb)+ NumberFormat.getInstance().format(kpCheckoutParam.getProduct().getBasePrice()));
//        itemPriceTextView.setText(getString(R.string.curr_symb)+ NumberFormat.getInstance().format(kpCheckoutParam.getProductPrice()));
//        if(kpCheckoutParam.getProduct().getDiscount()>0){
//            itemOriginalPriceTextView.setPaintFlags(itemOriginalPriceTextView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//        }
        totalPriceTextView.setText(getString(R.string.curr_symb)+ NumberFormat.getInstance().format(kpCheckoutParam.getTotalPrice()));
        productPriceTextView.setText(getString(R.string.curr_symb)+NumberFormat.getInstance().format(kpCheckoutParam.getProductPrice()));
        shippingCostTextView.setText(getString(R.string.curr_symb)+NumberFormat.getInstance().format(kpCheckoutParam.getShippingCost()));
        insuranceTextView.setText(getString(R.string.curr_symb)+NumberFormat.getInstance().format(kpCheckoutParam.getInsurance()));
        adminFeeTextView.setText(getString(R.string.curr_symb)+NumberFormat.getInstance().format(kpCheckoutParam.getAdministrationFee()));
        voucherTextView.setText(getString(R.string.curr_symb)+NumberFormat.getInstance().format(kpCheckoutParam.getVoucher()));
        processButton.setOnClickListener(v -> {
            if(paymentOptionAdapter.getMSelectedItem()!=-1){
                Payment payment = paymentOptionAdapter.getMItems().get(paymentOptionAdapter.getMSelectedItem());
                kpCheckoutParam.setPaymentMethodId(String.valueOf(payment.getId()));
                HttpInstance.withProgress(activity()).observe(HttpInstance.create(CartService.class).checkout(kpCheckoutParam), orderCheckout -> {
                    ResponseTools.validate(this, orderCheckout, true);
                    WindowFlow.withAnim(R.anim.slide_in_from_right, R.anim.slide_out_to_right)
                            .goTo(activity(HomeActivity.class), KPInstallmentFragment.TAG,
                                    Bundler.wrap(KPInstallmentFragment.class, kpCheckoutParam, orderCheckout.getKPCheckout()), R.id.fragment_frame, true);
                }, throwable -> {
                    activity(HomeActivity.class).showSnackbar(throwable, v1 -> {});
                });
            }else{
                activity(HomeActivity.class).showTooltip(paymentRecyclerView, getString(R.string.select_payment_validation), Tooltip.Gravity.BOTTOM);
            }
        });
    }
}
