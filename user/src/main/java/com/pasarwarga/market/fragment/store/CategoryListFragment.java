package com.pasarwarga.market.fragment.store;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.VerticalRecyclerAdapter;
import com.alfarabi.alfalibs.fragments.interfaze.SimpleFragmentCallback;
import com.alfarabi.alfalibs.helper.SwipeCallback;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.alfarabi.alfalibs.views.AlfaSwipeRefreshLayout;
import com.alfarabi.base.fragment.home.BaseDrawerFragment;
import com.alfarabi.base.model.location.Category;
import com.pasarwarga.market.R;
import com.pasarwarga.market.holder.recyclerview.CategoryViewHolder;

import butterknife.BindView;

/**
 * Created by Alfarabi on 6/19/17.
 */

public class CategoryListFragment extends BaseDrawerFragment implements SimpleFragmentCallback {

    public static final String TAG = CategoryListFragment.class.getName();

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout;
    @BindView(R.id.recyclerview) AlfaRecyclerView recyclerView ;
    @BindView(R.id.swiperefreshlayout) AlfaSwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.textview) TextView textView;

    VerticalRecyclerAdapter<CategoryListFragment, CategoryViewHolder, Category> verticalRecyclerAdapter ;

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        toolbar.inflateMenu(com.alfarabi.base.R.menu.empty_menu);
        toolbar.setOnMenuItemClickListener(this);
        verticalRecyclerAdapter = new VerticalRecyclerAdapter<>(this, CategoryViewHolder.class, landing.getCategories());
        verticalRecyclerAdapter.initRecyclerView(recyclerView);
//        swipeRefreshLayout.setOnRefreshListener(() -> swipeRefreshLayout.setRefreshing(false));
        swipeRefreshLayout.setOnSwipeListener(new SwipeCallback() {
            @Override
            public void onSwipeFromTop() {
//                swipeRefreshLayout.setRefreshing(false);
                swipeRefreshLayout.finishRefresh();
            }

            @Override
            public void onSwipeFromBottom() {

            }
        });
        swipeRefreshLayout.setRefreshable(false);
    }

    @Override
    public void onResume() {
        super.onResume();
//        swipeRefreshLayout.load(HttpInstance.mock(this, CategoryService.class).getCategories(), categories -> {
//            RealmDao.instance().insertOrUpdateCollection(categories);
//            verticalRecyclerAdapter.setObjects(categories);
//        }, Throwable::printStackTrace);
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_category_list;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public Object getObject() {
        return null;
    }

    @Override
    public String initTitle() {
        title = getString(R.string.category);
        toolbar.setTitle(title);
        return title;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    @Override
    public int getMenuId() {
        return R.id.category_menu_item;
    }

    @Override
    public boolean keepSideMenu() {
        return true;
    }
}
