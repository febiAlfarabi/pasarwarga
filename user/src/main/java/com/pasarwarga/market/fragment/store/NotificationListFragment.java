//package com.pasarwarga.market.fragment.store;
//
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.design.widget.AppBarLayout;
//import android.support.v7.widget.SearchView;
//import android.support.v7.widget.Toolbar;
//import android.view.MenuItem;
//import android.view.View;
//
//import com.alfarabi.alfalibs.adapters.recyclerview.SimpleRecyclerAdapter;
//import com.alfarabi.alfalibs.views.AlfaRecyclerView;
//import com.alfarabi.alfalibs.views.AlfaSwipeRefreshLayout;
//import com.alfarabi.base.fragment.home.BaseDrawerFragment;
//import com.alfarabi.base.model.notification.Notification;
//import com.pasarwarga.market.R;
//import com.pasarwarga.market.holder.recyclerview.NotificationViewHolder;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import butterknife.BindView;
//
///**
// * Created by Alfarabi on 6/19/17.
// */
//
//public class NotificationListFragment extends BaseDrawerFragment<List<Notification>>{
//
//    public static final String TAG = NotificationListFragment.class.getName();
//
//    @BindView(R.id.toolbar) Toolbar toolbar;
//    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout;
//    @BindView(R.id.recyclerview) AlfaRecyclerView recyclerView ;
//    @BindView(R.id.swiperefreshlayout) AlfaSwipeRefreshLayout swipeRefreshLayout;
//    SimpleRecyclerAdapter<Notification, NotificationListFragment, NotificationViewHolder> simpleRecyclerAdapter ;
//    private SearchView searchView ;
//
//    @Override
//    public Toolbar getToolbar() {
//        return toolbar;
//    }
//
//    @Override
//    public AppBarLayout getAppbarLayout() {
//        return appBarLayout;
//    }
//
//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//
//    }
//
//    @Override
//    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//        getToolbar().inflateMenu(R.menu.notification_menu);
//        getToolbar().setOnMenuItemClickListener(this);
//
////        searchView = injectSearchView(toolbar.getMenu(), new SearchView.OnQueryTextListener() {
////            @Override
////            public boolean onQueryTextSubmit(String query) {
////                WLog.i(TAG, query);
////                return false;
////            }
////
////            @Override
////            public boolean onQueryTextChange(String newText) {
////                WLog.i(TAG, newText);
////                return false;
////            }
////        });
//        simpleRecyclerAdapter  = new SimpleRecyclerAdapter(this, NotificationViewHolder.class, new ArrayList<Notification>());
//
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
////        swipeRefreshLayout.load(HttpInstance.mock(this, NotificationService.class).getNotifications(), notifications -> {
////            simpleRecyclerAdapter.setObjects(notifications);
////            simpleRecyclerAdapter.initRecyclerView(recyclerView, new LinearLayoutManager(getContext()));
////        }, Throwable::printStackTrace);
//    }
//
//
//    @Override
//    public int contentXmlLayout() {
//        return R.layout.fragment_notification_list;
//    }
//
//    @Override
//    public String getTAG() {
//        return TAG;
//    }
//
//    @Override
//    public List<Notification> getObject() {
//        return simpleRecyclerAdapter.getObjects();
//    }
//
//    @Override
//    public String initTitle() {
//        title = getString(R.string.notification);
//        toolbar.setTitle(title);
//        return title;
//    }
//
//    @Override
//    public boolean onMenuItemClick(MenuItem item) {
//        return false;
//    }
//
//
//    @Override
//    public int getMenuId() {
////        return R.id.notification_menu_item;
//        return 0;
//    }
//
//    @Override
//    public boolean keepSideMenu() {
//        return true;
//    }
//}
