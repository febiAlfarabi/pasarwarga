package com.pasarwarga.market.fragment.store;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.alfarabi.alfalibs.adapters.recyclerview.SimpleRecyclerAdapter;
import com.alfarabi.alfalibs.tools.AppDialog;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.alfarabi.alfalibs.views.AlfaSwipeRefreshLayout;
import com.alfarabi.base.model.transaction.CartItem;
import com.pasarwarga.market.R;
import com.pasarwarga.market.holder.recyclerview.OrderViewHolder;
import com.alfarabi.base.fragment.home.BaseDrawerFragment;
import com.alfarabi.base.model.transaction.Cart;
import com.hendraanggrian.bundler.annotations.BindExtra;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by Alfarabi on 6/19/17.
 */

public class OrderListFragment extends BaseDrawerFragment<List<CartItem>> {

    public static final String TAG = OrderListFragment.class.getName();

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout;
    @BindView(R.id.recyclerview) AlfaRecyclerView recyclerView ;
    @BindView(R.id.swiperefreshlayout) AlfaSwipeRefreshLayout swipeRefreshLayout;
    SimpleRecyclerAdapter<CartItem, OrderListFragment, OrderViewHolder> simpleRecyclerAdapter ;

    @BindExtra Cart cart ;

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public int getMenuId() {
        return 0;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        toolbar.inflateMenu(com.alfarabi.base.R.menu.cart_menu);
        toolbar.setOnMenuItemClickListener(this);
        simpleRecyclerAdapter  = new SimpleRecyclerAdapter(this, OrderViewHolder.class, new ArrayList<CartItem>());

    }

    @Override
    public void onResume() {
        super.onResume();
        simpleRecyclerAdapter.setObjects(cart.getCartItems());
        simpleRecyclerAdapter.initRecyclerView(recyclerView, new LinearLayoutManager(getContext()));
//        swipeRefreshLayout.load(HttpInstance.mock(getContext(), OrderService.class).getCartItems(), orders -> {
//            simpleRecyclerAdapter.setObjects(orders);
//            simpleRecyclerAdapter.initRecyclerView(recyclerView, new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
//        }, Throwable::printStackTrace);
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_order_list;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public List<CartItem> getObject() {
        return simpleRecyclerAdapter.getObjects();
    }

    @Override
    public String initTitle() {
        title = getString(R.string.order);
        toolbar.setTitle(title);
        return title;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        if(item.getItemId()== com.alfarabi.base.R.id.delete_menu){
            AppDialog.with(getContext()).dialog(com.alfarabi.base.R.string.remove_dialog_title
                    , com.alfarabi.base.R.string.remove_order_confirm, (dialog, which) -> {
                        dialog.dismiss();
                    }, (dialog, which) -> dialog.dismiss());
        }
        return false;
    }
}
