package com.pasarwarga.market.fragment.store;


import android.support.v4.app.Fragment;
import android.widget.ImageView;

import com.pasarwarga.market.R;
import com.alfarabi.base.fragment.SimpleBaseFragmentInitiator;
import com.alfarabi.base.model.Product;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class PaymentFragment extends SimpleBaseFragmentInitiator<Product> {

    @BindView(R.id.payment_service_iv) ImageView imageViewPayment;
//    @BindView(R.id.payment_service_tv) TextView textViewPayment;

//    @Setter @Getter Product product ;
//
//    public static PaymentFragment instance(Product product){
//        PaymentFragment fragment = new PaymentFragment();
//        fragment.setProduct(product);
//        return fragment ;
//    }

    @Override
    public String getTAG() {
        return TAG = getClass().getName();
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_payment;
    }

    @Override
    public Product getObject() {
        return null;
    }
}
