package com.pasarwarga.market.fragment.store;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.SimpleRecyclerAdapter;
import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.alfarabi.base.http.basemarket.service.ReviewService;
import com.alfarabi.base.realm.RealmDao;
import com.alfarabi.base.tools.ResponseTools;
import com.hendraanggrian.bundler.Bundler;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.fragment.store.product.ProductReviewListFragment;
import com.pasarwarga.market.holder.recyclerview.ReviewViewHolder;
import com.alfarabi.base.fragment.SimpleBaseFragmentInitiator;
import com.alfarabi.base.model.Product;
import com.alfarabi.base.model.review.Review;
import com.singh.daman.proprogressviews.DottedArcProgress;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import io.realm.RealmList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReviewFragment extends SimpleBaseFragmentInitiator<Product> {

    public static final String TAG = ReviewFragment.class.getName();
    
    @BindView(R.id.rating_review_count_tv) TextView ratingCountTextView;
    @BindView(R.id.rating_review_bar) RatingBar ratingBar;
    @BindView(R.id.review_review_count_tv) TextView reviewCoumtTextView;
    @BindView(R.id.loadmore_button) Button loadMoreButton;
    @BindView(R.id.dotted_arcprogress) DottedArcProgress dottedArcProgress ;
    @BindView(R.id.recyclerview) AlfaRecyclerView<Review> reviewAlfaRecyclerView;
    SimpleRecyclerAdapter<Review, ReviewFragment, ReviewViewHolder> simpleRecyclerAdapter;
    private int max = 3 ;
    int page = 1;

    @Override
    public String getTAG() {
        return TAG ;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_review;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setObject(RealmDao.unmanage(getObject()));
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setData();
        simpleRecyclerAdapter = new SimpleRecyclerAdapter<>(this, ReviewViewHolder.class, new ArrayList<Review>());
        simpleRecyclerAdapter.initRecyclerView(reviewAlfaRecyclerView, new LinearLayoutManager(getActivity()));
        loadMoreButton.setOnClickListener(v -> {
            WindowFlow.goTo(activity(HomeActivity.class), ProductReviewListFragment.TAG, Bundler.wrap(ProductReviewListFragment.class, RealmDao.unmanage(getObject())), R.id.fragment_frame, true, false);

        });
    }

    public void setData(){
        if(getObject()==null){
            setObject(RealmDao.unmanage(RealmDao.instance().get(getObject().getId(), Product.class)));
        }
        ratingCountTextView.setText(String.valueOf(getObject().getRatings()));
        ratingBar.setRating(getObject().getRatings());
        reviewCoumtTextView.setText("(" + String.valueOf(getObject().getReviewCount()) + " " + getActivity().getString(R.string.review) + ")");
        dottedArcProgress.setVisibility(View.GONE);
        if(getObject().getReviewCount()>max){
            loadMoreButton.setVisibility(View.VISIBLE);
        }else{
            loadMoreButton.setVisibility(View.GONE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        page = 1;
        dottedArcProgress.setVisibility(View.VISIBLE);
        HttpInstance.call(HttpInstance.create(ReviewService.class).getReviews(getObject().getId(), page), reviewResponse -> {
            ResponseTools.validate(getActivity(), reviewResponse);
            if(reviewResponse.getReviews()!=null && reviewResponse.getReviews().size()>0){
                if(page==1){
                    RealmList realmList = new RealmList();
                    realmList.addAll(reviewResponse.getReviews());
                    getObject().setReviews(realmList);
                }else{
                    getObject().getReviews().addAll(reviewResponse.getReviews());
                }
                RealmDao.instance().insertOrUpdate(getObject());
                setObject(RealmDao.unmanage(getObject()));
                List<Review> reviewList  = reviewResponse.getReviews();
                if(reviewList.size()>max){
                    reviewList = reviewList.subList(0, max);
                }
                simpleRecyclerAdapter.getObjects().addAll(reviewList);
                page++ ;
            }
            setData();
            simpleRecyclerAdapter.notifyDataSetChanged();
        }, throwable -> {
            setData();
            simpleRecyclerAdapter.getObjects().addAll(getObject().getReviews());
            simpleRecyclerAdapter.notifyDataSetChanged();
            dottedArcProgress.setVisibility(View.GONE);
        }, false);
    }
}
