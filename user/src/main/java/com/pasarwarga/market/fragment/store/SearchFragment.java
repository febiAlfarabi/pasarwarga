package com.pasarwarga.market.fragment.store;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.ImageSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.alfarabi.alfalibs.fragments.interfaze.SimpleFragmentCallback;
import com.alfarabi.alfalibs.tools.KeyboardUtils;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.fragment.home.BaseDrawerFragment;
import com.alfarabi.base.view.AlfaViewPager;
import com.alfarabi.base.view.TabLayout;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.adapter.SearchTabPagerAdapter;

import butterknife.BindView;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 6/19/17.
 */

public class SearchFragment extends BaseDrawerFragment implements SimpleFragmentCallback {

    public static final String TAG = SearchFragment.class.getName();

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout;

    @BindView(R.id.search_et) EditText searchEditText ;
    @BindView(R.id.tab_layout) TabLayout tabLayout ;
    @BindView(R.id.viewpager)
    AlfaViewPager viewPager ;

    private SearchTabPagerAdapter searchTabPagerAdapter ;

    @Getter@Setter String input = "" ;

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments()!=null){
            input = getArguments().getString("input");
        }

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        searchTabPagerAdapter = new SearchTabPagerAdapter(this, input);
        viewPager.setAdapter(searchTabPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        ImageSpan imageHint = new ImageSpan(activity(), R.drawable.ic_search_black_24dp);
        SpannableString spannableString = new SpannableString("_"+getString(R.string.search_product_or_store));
        spannableString.setSpan(imageHint, 0, 1, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        searchEditText.setHint(spannableString);

        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                input = s.toString();
                searchTabPagerAdapter.setQuery(input, viewPager.getCurrentItem());
//                if(input.length()>0){
//                    searchTabPagerAdapter.setQuery(input, viewPager.getCurrentItem());
//                }
//                else{
//                    WindowFlow.backstack(getActivity());
//                }
            }
        });
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                searchTabPagerAdapter.setQuery(input, viewPager.getCurrentItem());
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        searchEditText.setText(input);

    }

    @Override
    public void onStop() {
        super.onStop();
        KeyboardUtils.hideKeyboard(activity(HomeActivity.class));
    }

    @Override
    public void onResume() {
        super.onResume();
        searchEditText.requestFocus();
        KeyboardUtils.showKeyboard(activity(HomeActivity.class));
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_search;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public Object getObject() {
        return null;
    }

    @Override
    public String initTitle() {
        return null;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    @Override
    public int getMenuId() {
        return 0;
    }
}
