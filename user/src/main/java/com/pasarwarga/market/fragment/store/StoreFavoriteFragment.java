package com.pasarwarga.market.fragment.store;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.alfarabi.alfalibs.adapters.recyclerview.SimpleRecyclerAdapter;
import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.alfarabi.base.fragment.home.BaseDrawerFragment;
import com.alfarabi.base.http.basemarket.service.SellerService;
import com.alfarabi.base.model.shop.SellerInfo;
import com.alfarabi.base.tools.ResponseTools;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.holder.recyclerview.StorefavoritViewHolder;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import lombok.Getter;

/**
 * Created by USER on 9/11/2017.
 */

public class StoreFavoriteFragment extends BaseDrawerFragment {

    public static final String TAG = StoreFavoriteFragment.class.getName();

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.drawer_layout) DrawerLayout drawerLayout;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout;
    @BindView(R.id.recyclerview) AlfaRecyclerView recyclerView;
    @BindView(R.id.loader) MKLoader loader;

    @Getter SimpleRecyclerAdapter<SellerInfo, StoreFavoriteFragment, StorefavoritViewHolder> simpleRecyclerAdapter;


    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public int getMenuId() {
        return R.id.store_favorite_menu_item;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    @Override
    public boolean keepSideMenu() {
        return true;
    }

    @Override
    public String initTitle() {
        title = getString(R.string.store_favorite);
        toolbar.setTitle(title);
        return title;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public Object getObject() {
        return null;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_store_favorite;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        List<SellerInfo> sellerInfos = new ArrayList<>();
        simpleRecyclerAdapter = new SimpleRecyclerAdapter<>(this, StorefavoritViewHolder.class, sellerInfos);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        simpleRecyclerAdapter.initRecyclerView(recyclerView, new GridLayoutManager(activity(), 2));
    }

    @Override
    public void onResume() {
        super.onResume();
        loader.setVisibility(MKLoader.VISIBLE);
        HttpInstance.call(HttpInstance.create(SellerService.class).listfavorite(), favoriteListResponse -> {
            ResponseTools.validate(activity(), favoriteListResponse);

            if (favoriteListResponse.isStatus() && favoriteListResponse.getSellerInfos() != null && favoriteListResponse.getSellerInfos().size() > 0) {
                simpleRecyclerAdapter.setObjects(favoriteListResponse.getSellerInfos());
            }
            loader.setVisibility(MKLoader.GONE);
        }, throwable -> {
            activity(HomeActivity.class).showSnackbar(throwable, v -> {});
            loader.setVisibility(MKLoader.GONE);
        }, false);
    }
}
