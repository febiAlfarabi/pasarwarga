package com.pasarwarga.market.fragment.store;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.alfarabi.alfalibs.adapters.recyclerview.SimpleRecyclerAdapter;
import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.alfarabi.base.fragment.SimpleBaseFragmentInitiator;
import com.alfarabi.base.http.basemarket.service.SellerService;
import com.alfarabi.base.model.FeaturedShop;
import com.alfarabi.base.tools.ResponseTools;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.holder.recyclerview.StoreViewHolder;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;

import butterknife.BindView;
import lombok.Getter;

/**
 * Created by Alfarabi on 6/19/17.
 */

public class StoreSearchFragment extends SimpleBaseFragmentInitiator<String> {

    public static final String TAG = StoreSearchFragment.class.getName();

    @BindView(R.id.store_rv) AlfaRecyclerView storeRecyclerView ;
    @BindView(R.id.store_loader) MKLoader storeLoader ;


    SimpleRecyclerAdapter<FeaturedShop, StoreSearchFragment, StoreViewHolder> simpleRecyclerAdapter ;


    @Getter String input = "" ;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        simpleRecyclerAdapter = new SimpleRecyclerAdapter<>(this, StoreViewHolder.class, new ArrayList<>());
        if(getArguments()!=null){
            input = getArguments().getString("input");
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        simpleRecyclerAdapter.initRecyclerView(storeRecyclerView, new LinearLayoutManager(getContext()));

    }

    public void setInput(String input) {
        this.input = input;
        if(input.length()>0){
            if(storeLoader!=null){
                storeLoader.setVisibility(View.VISIBLE);
            }
            HttpInstance.call(HttpInstance.create(SellerService.class).search(input), searchResponse -> {
                ResponseTools.validate(activity(), searchResponse);
                if(searchResponse.isStatus()){
                    simpleRecyclerAdapter.setObjects(searchResponse.getFeaturedShops());
                }else{
                    simpleRecyclerAdapter.setObjects(new ArrayList<>());
                }
                if(storeLoader!=null){
                    storeLoader.setVisibility(View.GONE);
                }
            }, throwable -> {
                simpleRecyclerAdapter.setObjects(new ArrayList<>());
                activity(HomeActivity.class).showSnackbar(throwable, v -> {});
                if(storeLoader!=null){
                    storeLoader.setVisibility(View.GONE);
                }
            }, false);
        }
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_store_search;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public String getObject() {
        return null;
    }
}
