package com.pasarwarga.market.fragment.store;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.alfarabi.alfalibs.adapters.recyclerview.SimpleRecyclerAdapter;
import com.alfarabi.alfalibs.fragments.interfaze.SimpleFragmentCallback;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.alfarabi.base.fragment.home.BaseDrawerFragment;
import com.alfarabi.base.model.location.Category;
import com.pasarwarga.market.R;
import com.pasarwarga.market.holder.recyclerview.SubCategoryViewHolder2;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * Created by Alfarabi on 6/19/17.
 */

public class SubCategoryFragment extends BaseDrawerFragment implements SimpleFragmentCallback {

    public static final String TAG = SubCategoryFragment.class.getName();

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout;
    @BindView(R.id.recyclerview) AlfaRecyclerView recyclerView ;
    SimpleRecyclerAdapter<Category, SubCategoryFragment, SubCategoryViewHolder2> simpleRecyclerAdapter ;

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        simpleRecyclerAdapter  = new SimpleRecyclerAdapter(this, SubCategoryViewHolder2.class, new ArrayList<Category>());

    }

    @Override
    public void onResume() {
        super.onResume();
        simpleRecyclerAdapter.initRecyclerView(recyclerView, new StaggeredGridLayoutManager(2, LinearLayout.VERTICAL));
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_category_list;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public Object getObject() {
        return null;
    }

    @Override
    public String initTitle() {
        title = getString(R.string.subcategory);
        toolbar.setTitle(title);
        return title;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    @Override
    public int getMenuId() {
        return 0;
    }
}
