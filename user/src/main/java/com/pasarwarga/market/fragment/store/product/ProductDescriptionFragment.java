package com.pasarwarga.market.fragment.store.product;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.WLog;
import com.alfarabi.base.http.basemarket.service.ProductService;
import com.alfarabi.base.realm.RealmDao;
import com.alfarabi.base.tools.ResponseTools;
import com.pasarwarga.market.R;
import com.alfarabi.base.fragment.SimpleBaseFragmentInitiator;
import com.alfarabi.base.model.Product;
import com.pasarwarga.market.activity.home.HomeActivity;

import org.sufficientlysecure.htmltextview.HtmlHttpImageGetter;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductDescriptionFragment extends SimpleBaseFragmentInitiator<Product> {

    @BindView(R.id.product_description_tv) HtmlTextView textViewDescription;

//    @BindView(R.id.webview) LxWebView lxWebView ;

    @Override
    public String getTAG() {
        return TAG = getClass().getName();
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_product_description;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(getObject()==null){
            setObject(RealmDao.instance().get(getObject().getId(), Product.class));
        }
        if(getObject()!=null && getObject().getDescription()!=null && !getObject().getDescription().isEmpty()){
            try {
                textViewDescription.setHtml(getObject().getDescription()!=null?getObject().getDescription():getString(R.string.no_description), new HtmlHttpImageGetter(textViewDescription));
            }catch (Exception e){
                e.printStackTrace();
                WLog.i(TAG, "DESCRIPTION :::: "+getObject().getDescription()!=null?getObject().getDescription():getString(R.string.no_description));
            }
        }else{
            textViewDescription.setHtml((getObject()!=null && getObject().getDescription()!=null)?getObject().getDescription():getString(R.string.load_description), new HtmlHttpImageGetter(textViewDescription));
            if(getObject()!=null){
                HttpInstance.call(HttpInstance.create(ProductService.class).getProduct(getObject().getId()), productResponse -> {
                    ResponseTools.validate(activity(), productResponse);

                    if(productResponse.isStatus() && productResponse.getProduct().getDescription()!=null){
                        RealmDao.instance().insertOrUpdate(productResponse.getProduct());
                        try{
                            textViewDescription.setHtml(productResponse.getProduct().getDescription(), new HtmlHttpImageGetter(textViewDescription));
                        }catch (Exception e){e.printStackTrace();}

                    }else{
                        try{
                            textViewDescription.setHtml(getObject().getDescription()!=null?getObject().getDescription():getString(R.string.no_description), new HtmlHttpImageGetter(textViewDescription));
                        }catch (Exception e){e.printStackTrace();}
                    }
                }, throwable -> {
                    textViewDescription.setHtml(getObject().getDescription()!=null?getObject().getDescription():getString(R.string.no_description), new HtmlHttpImageGetter(textViewDescription));
                    activity(HomeActivity.class).showSnackbar(throwable, v -> {});
                }, false);
            }

        }
    }
}
