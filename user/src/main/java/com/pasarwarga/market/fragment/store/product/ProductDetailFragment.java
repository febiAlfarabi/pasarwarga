package com.pasarwarga.market.fragment.store.product;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.AppProgress;
import com.alfarabi.alfalibs.tools.Gilimanuk;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.alfalibs.tools.WLog;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.alfalibs.views.AlfaBannerView;
import com.alfarabi.base.activity.SimpleBaseActivityInitiator;
import com.alfarabi.base.adapter.StringBannerScollViewAdapter;
import com.alfarabi.base.http.basemarket.service.CartService;
import com.alfarabi.base.http.basemarket.service.FavoriteService;
import com.alfarabi.base.http.basemarket.service.ProductService;
import com.alfarabi.base.http.basemarket.service.SellerService;
import com.alfarabi.base.model.FeaturedShop;
import com.alfarabi.base.model.transaction.Cart;
import com.alfarabi.base.realm.RealmDao;
import com.alfarabi.alfalibs.tools.GoogleSocial;
import com.alfarabi.base.tools.PhoneTools;
import com.alfarabi.base.tools.ResponseTools;
import com.alfarabi.base.tools.WeightUtils;
import com.alfarabi.base.view.interfaze.ShowCaseEventListener;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.MessageDialog;
import com.facebook.share.widget.ShareDialog;
import com.flipboard.bottomsheet.BottomSheetLayout;
import com.flipboard.bottomsheet.commons.IntentPickerSheetView;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.github.ornolfr.ratingview.RatingView;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.plus.PlusShare;
import com.hendraanggrian.bundler.Bundler;
import com.hendraanggrian.bundler.annotations.BindExtra;
import com.hendraanggrian.preferencer.annotations.BindPreference;
import com.pasarwarga.market.activity.SplashActivity;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.adapter.ProductDetailTabPagerAdapter;
import com.alfarabi.base.fragment.home.BaseDrawerFragment;
import com.pasarwarga.market.R;
import com.alfarabi.base.model.Product;
import com.alfarabi.base.view.AlfaViewPager;
import com.pasarwarga.market.fragment.chat.ConversationListFragment;
import com.pasarwarga.market.fragment.store.CartFragment;
import com.pasarwarga.market.fragment.store.CartListFragment;
import com.pasarwarga.market.fragment.store.profile.StoreProfileFragment;
import com.pasarwarga.market.tools.SystemApp;
import com.tuyenmonkey.mkloader.MKLoader;
import com.yarolegovich.discretescrollview.transform.Pivot;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import net.soroushjavdan.customfontwidgets.CTextView;

import org.apache.commons.lang.StringUtils;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import cn.nekocode.rxactivityresult.RxActivityResult;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import lombok.Setter;

/**
 * Created by Alfarabi on 6/16/17.
 */

public class ProductDetailFragment extends BaseDrawerFragment<Product> {

    @BindPreference("com.pasarwarga.market.fragment.store.product.ProductDetailFragment") boolean secondTimeOpen ;

    public static final String TAG = ProductDetailFragment.class.getName();

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout;
    @BindView(R.id.tab_layout) TabLayout tabLayout;
    @BindView(R.id.viewpager) AlfaViewPager viewPager;
    @BindView(R.id.product_detail_fab) FloatingActionButton floatingActionButton;
    @BindView(R.id.product_share_fab) FloatingActionButton floatingShareButton;
    @BindView(R.id.product_name_tv) TextView productNameTextView;
    @BindView(R.id.rating_bar) RatingView ratingBar;
    @BindView(R.id.product_original_price_tv) TextView productOriginalPriceTextView;
    @BindView(R.id.product_price_tv) TextView productPriceTextView;
    @BindView(R.id.installment_simulation_tv) TextView installmentSimulationTextView;
    @BindView(R.id.weight_tv) TextView weightTextView;
    @BindView(R.id.stock_tv) TextView stockTextView;
    @BindView(R.id.alfabannerview) AlfaBannerView alfaBannerView;
    @BindView(R.id.review_count_tv) TextView reviewCountTextView;
    @BindView(R.id.discount_tv) CTextView discountTextView;
    @BindView(R.id.tenor_loader) MKLoader tenorLoader;
    @BindView(R.id.collapsing_toolbar) CollapsingToolbarLayout collapsingToolbarLayout ;
    @BindView(R.id.nested_scrollview) NestedScrollView nestedScrollView ;
    @BindView(R.id.store_info_layout) CardView storeInfoLayout ;
    @BindView(R.id.location_layout) LinearLayout locationLayout ;
    @BindView(R.id.location_tv) TextView locationTextView ;
    @BindView(R.id.seller_name_tv) TextView sellernameTextView ;
    @BindView(R.id.seller_ratingview) RatingView sellerRatingView ;
    @BindView(R.id.seller_iv) ImageView sellerImageView ;
    @BindView(R.id.seller_chat_button) ImageView sellerChatButton ;
    @BindView(R.id.seller_favorite_button) ImageView sellerFavoriteButton ;
//    @BindView(R.id.location_button) ImageView locationButton ;
    @BindView(R.id.order_button) Button orderButton ;

    @BindView(R.id.bottomsheet) BottomSheetLayout bottomSheetLayout ;

    private TextView cartBadgeCount;
    private StringBannerScollViewAdapter stringBannerScollViewAdapter;

    private ProductDetailTabPagerAdapter productDetailTabPagerAdapter;

    private boolean onOrder = false;
    boolean flag = true;

    private Cart cart ;

    @Setter@BindExtra Product product;

    private CallbackManager callbackManager;
    private ShareDialog shareDialog;
    private ShareLinkContent linkContent ;

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_product_detail;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(RealmDao.unmanage(RealmDao.instance().get(product.getId(), Product.class))!=null){
            product = RealmDao.unmanage(RealmDao.instance().get(product.getId(), Product.class));
        }
        productDetailTabPagerAdapter = new ProductDetailTabPagerAdapter(this, product);
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        linkContent = new ShareLinkContent.Builder().setContentUrl(Uri.parse(product.getCompleteSeourl())).build();

        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                MessageDialog.show(getActivity(), linkContent);
            }

            @Override
            public void onCancel() {}

            @Override
            public void onError(FacebookException error) {}
        });
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        toolbar.inflateMenu(R.menu.home_menu);
        toolbar.setOnMenuItemClickListener(this);
        MenuItemCompat.setActionView(toolbar.getMenu().findItem(R.id.cart_menu), R.layout.badge_count);
        View actionView = MenuItemCompat.getActionView(toolbar.getMenu().findItem(R.id.cart_menu));
        actionView.setOnClickListener(v -> {
            if(SystemApp.checkLogin(activity(), true)){
                Gilimanuk.withAnim(appearAnims).goTo(activity(HomeActivity.class), CartListFragment.TAG, R.id.fragment_frame, true, false);
//                if(cart!=null && cart.getCount()>0){
//                    Gilimanuk.withAnim(appearAnims).goTo(activity(HomeActivity.class), CartFragment.TAG, Bundler.wrap(CartFragment.class, new Product()), R.id.fragment_frame, true, false);
//                }else{
//                    Gilimanuk.withAnim(appearAnims).goTo(activity(HomeActivity.class), CartListFragment.TAG, R.id.fragment_frame, true, false);
//                }
            }
        });

        cartBadgeCount = (TextView) actionView.findViewById(R.id.notif_count);
        alfaBannerView.setMinimumHeight(getDimension()[0]);
        if (product != null) {
            stringBannerScollViewAdapter = new StringBannerScollViewAdapter(getActivity(), R.layout.fragment_home_banner, R.drawable.ic_product_default, StringUtils.split(product.getImages()!=null?product.getImages():product.getImage(), ","));
            alfaBannerView.setAdapter(stringBannerScollViewAdapter);
            alfaBannerView.setItemTransformer(new ScaleTransformer.Builder().setMaxScale(1.05f).setMinScale(0.7f).setPivotX(Pivot.X.CENTER).setPivotY(Pivot.Y.BOTTOM).build());

            productNameTextView.setText(product.getName());
            weightTextView.setText(getString(R.string.weight) + " : " + (product.getWeight()<1000?
                    String.valueOf(product.getWeight()) + " " + getString(R.string.gram):
                    String.valueOf((float)product.getWeight()/1000) + " " + getString(R.string.kg)));
            stockTextView.setText(getString(R.string.stock) + " : " + String.valueOf(product.getQuantity()));
            if (product.getDiscount() == 0) {
                productOriginalPriceTextView.setText(getString(R.string.curr_symb) + NumberFormat.getInstance().format(product.getBasePrice()));
                productPriceTextView.setVisibility(View.GONE);
                discountTextView.setVisibility(View.GONE);
            } else {
                discountTextView.setVisibility(View.VISIBLE);
                productOriginalPriceTextView.setPaintFlags(productOriginalPriceTextView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                productOriginalPriceTextView.setText(getString(R.string.curr_symb) + NumberFormat.getInstance().format(product.getBasePrice()));
                productPriceTextView.setText(getString(R.string.curr_symb) + NumberFormat.getInstance().format(product.getPriceAfterDiscount()));
                discountTextView.setText(String.valueOf(product.getDiscount()+ getString(R.string.percent_symb))+" "+getString(R.string.off));
            }

            if (product.getTenors() != null) {
                renderTenor();
            }
            reviewCountTextView.setText(String.valueOf(product.getReviewCount()));
            ratingBar.setRating(product.getRatings());


            renderSeller();
            if(product.isFavourite()){
                floatingActionButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorite_red_24dp));
            }else{
                floatingActionButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorite_black_24dp));
            }
            storeInfoLayout.setOnClickListener(v -> {
                FeaturedShop featuredShop = new FeaturedShop();
                featuredShop.setId(product.getSeller().getId());
                featuredShop.setShopName(product.getShopName());
                featuredShop.setAddress(product.getSeller().getAddress());
                featuredShop.setLocation(product.getSeller().getLocation());
                featuredShop.setShopRatting(String.valueOf(product.getSeller().getShopRatting()));
                featuredShop.setReviewCount(String.valueOf(product.getSeller().getReviewCount()));
                featuredShop.setStoreImage(product.getSeller().getAvatar());
                featuredShop.setBannerImage(product.getSeller().getBackgroundImageUrl());
                featuredShop.setSellerEmail(product.getSeller().getEmail());
                featuredShop.setSellerName(product.getSeller().getName());
                Gilimanuk.withAnim(appearAnims).goTo(activity(HomeActivity.class), StoreProfileFragment.TAG, Bundler.wrap(StoreProfileFragment.class, featuredShop), R.id.fragment_frame, true, false);
            });
        }
        floatingActionButton.setPadding(-12, -12, -12, -12);

    }


    public void initOrderButton(boolean init){
        if(init){
            orderButton.setClickable(true);
            orderButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.red_shape_button));
            orderButton.setOnClickListener(view1 -> {
                if (!installmentSimulationTextView.getText().toString().isEmpty()) {
                    if(SystemApp.checkLogin(activity(), true)){
                        onOrder = true;
                        if(!SystemApp.checkVerified(activity())){
                            return;
                        }
                        addChartItem();
                    }
                }else{
                    activity(HomeActivity.class).showSnackbar(getString(R.string.please_wait_until_installment_showing), v -> {});
                }
            });
        }else{
            orderButton.setClickable(false);
            orderButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.grey_shape_button));
        }
    }

    public void addChartItem(){
        AppProgress.showBasic(activity(), R.string.loading, true);
        HttpInstance.call(HttpInstance.create(CartService.class).itemOnCart(), itemOnCart -> {
            ResponseTools.validate(activity(), itemOnCart);
            AppProgress.dismissBasic();
            if (itemOnCart.isStatus() && itemOnCart.getCart().getCount() <= 0 && product.getId() != 0) {
                HashMap<String, String> hashMap = new HashMap();
                hashMap.put("quantity", "1");
                hashMap.put("product_id", String.valueOf(product.getId()));
                HttpInstance.withProgress(activity(HomeActivity.class)).observe(HttpInstance.create(CartService.class).addToCart(hashMap), addToCart -> {
                    ResponseTools.validate(activity(), addToCart);
                    if (addToCart.isStatus()) {
                        cart = addToCart.getCart();
                        Gilimanuk.withAnim(appearAnims).goTo(activity(HomeActivity.class), CartFragment.TAG, Bundler.wrap(CartFragment.class, product), R.id.fragment_frame, true, false);
                    } else {
                        activity(HomeActivity.class).showSnackbar(addToCart.getMessage(), v -> {});
                    }
                }, throwable -> {
                    activity(HomeActivity.class).showDialog(throwable, (dialog, which) -> {
                        dialog.dismiss();
                        WindowFlow.backstack(activity(HomeActivity.class));
                    });
                }, false);
            } else if (itemOnCart.isStatus() && itemOnCart.getCart().getCount() > 0 && product.getId()!=0) {
                cart = itemOnCart.getCart();
                activity(HomeActivity.class).showDialog(getString(R.string.warning), getString(R.string.previous_cart_available_warning), (dialog, which) -> {
                    dialog.dismiss();
                });
            }
        }, throwable -> {
            AppProgress.dismissBasic();
            activity(HomeActivity.class).showDialog(throwable, (dialog, which) -> {
                dialog.dismiss();
                WindowFlow.backstack(activity(HomeActivity.class));
            });
        }, false);
    }


    public void renderSeller(){
        if(product.getSeller()==null){
            return;
        }

        if (SystemApp.checkLogin(activity(), false) && SystemApp.myProfile().getUser().isSeller()){
            if(SystemApp.myProfile().getUser().getSellerId().trim().equalsIgnoreCase(String.valueOf(product.getSeller().getId()).trim())){
                initOrderButton(false);
                initChatButton(false);
            }else{
                initOrderButton(true);
                initChatButton(true);
            }
        }else{
            initOrderButton(true);
            initChatButton(true);
        }
        locationTextView.setText(StringUtils.split(product.getSeller().getAddress(), ",")[0]);
        sellernameTextView.setText(product.getSeller().getBusinessName());
        sellerRatingView.setRating(product.getSeller().getShopRatting());
        if(product.getSeller().getAvatar()!=null){
            GlideApp.with(this).load(StringUtils.split(product.getSeller().getAvatar(), ",")[0])
                    .placeholder(R.drawable.ic_pasarwarga)
                    .error(R.drawable.ic_pasarwarga)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            e.printStackTrace();
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }
                    }).diskCacheStrategy(DiskCacheStrategy.ALL).into(sellerImageView);
        }

        if(product.getSeller().isFavorite()){
            sellerFavoriteButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorite_red_24dp));
        }else{
            sellerFavoriteButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorite_black_24dp));
        }
        sellerFavoriteButton.setOnClickListener(v -> favoriteSeller());


        // It Will Show Waze or Maps Application
//        locationButton.setOnClickListener(v -> {
//            showMapApplication();
//        });


    }

    public void initChatButton(boolean init){
        if(init){
            sellerChatButton.setOnClickListener(v -> {
                if(SystemApp.checkLogin(activity(HomeActivity.class), true)){
                    Gilimanuk.withAnim(appearAnims).goTo(activity(HomeActivity.class), ConversationListFragment.TAG,
                            Bundler.wrap(ConversationListFragment.class,
                                    ConversationListFragment.buildConversation(RealmDao.unmanage(product.getSeller()))),R.id.fragment_frame, true, false);
                }
            });
        }else{
            sellerChatButton.setOnClickListener(v -> {
                activity(HomeActivity.class).showSnackbar(getString(R.string.cant_chat_urself), v1 -> {});
            });
        }

    }

    void showMapApplication(){
        String uri = "http://maps.google.com/maps?q=loc:"+String.valueOf(product.getSeller().getLocation().getLatitude())
                +","+String.valueOf(product.getSeller().getLocation().getLongitude())+" ("+getActivity().getPackageName()+")";
        Intent shareIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        shareIntent.setFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
        shareIntent.setFlags(Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP);
        IntentPickerSheetView intentPickerSheet = new IntentPickerSheetView(getActivity(), shareIntent, getString(R.string.open_with), activityInfo -> {
            bottomSheetLayout.dismissSheet();
            startActivity(activityInfo.getConcreteIntent(shareIntent));
        });
        intentPickerSheet.setFilter(info -> {
            return  info.componentName.getPackageName().contains("waze") ||
                    info.componentName.getPackageName().contains("maps") ||
                    info.componentName.getPackageName().contains("com.google.android.street") ||
                    info.componentName.getPackageName().contains("com.google.earth") ||
                    info.componentName.getPackageName().contains("com.google.android.apps.docs");
        });

        intentPickerSheet.setSortMethod((o1, o2) -> o1.label.compareTo(o2.label));
        bottomSheetLayout.showWithSheetView(intentPickerSheet);
    }

    public void favoriteSeller(){
        if(SystemApp.checkLogin(activity(HomeActivity.class), true)){
            HttpInstance.withProgress(activity()).observe(HttpInstance.create(SellerService.class).favorite(Long.valueOf(product.getSeller().getId())), sellerFavoriteResponse -> {
                if(product.getSeller().isFavorite()){
                    product.getSeller().setFavorite(false);
                    sellerFavoriteButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorite_black_24dp));
                }else{
                    product.getSeller().setFavorite(true);
                    sellerFavoriteButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorite_red_24dp));
                }
            }, throwable -> {
                activity(HomeActivity.class).showSnackbar(throwable, v -> {});
            }, false);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (product.getTenors() == null || product.getTenors().size() <= 0) {
            tenorLoader.setVisibility(MKLoader.VISIBLE);
        }
//        initOrderButton(false);
        HttpInstance.call(HttpInstance.create(ProductService.class).getProduct(product.getId()), productResponse -> {
            ResponseTools.validate(activity(), productResponse);
            if (productResponse.isStatus() && productResponse.getProduct() != null) {
                this.product = productResponse.getProduct();
                if (this.product.getTenors() != null && this.product.getTenors().size()>0) {
                    renderTenor();
                }else{
                    retrieveTenors();
                }
                renderSeller();
                if(product.isFavourite()){
                    floatingActionButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorite_red_24dp));
                }else{
                    floatingActionButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorite_black_24dp));
                }
                weightTextView.setText(getString(R.string.weight) + " : " + (product.getWeight()<1000?
                        String.valueOf(product.getWeight()) + " " + getString(R.string.gram):
                        String.valueOf((float)product.getWeight()/1000) + " " + getString(R.string.kg)));
                stockTextView.setText(getString(R.string.stock) + " : " + String.valueOf(product.getQuantity()));
                reviewCountTextView.setText(String.valueOf(product.getReviewCount()));
                ratingBar.setRating(product.getRatings());
                stringBannerScollViewAdapter.setBannerUrls(StringUtils.split(product.getImages(), ","));
                stringBannerScollViewAdapter.notifyDataSetChanged();
                autoMove(2000, true);
                productDetailTabPagerAdapter.setProduct(product);
                viewPager.setAdapter(null);
                viewPager.setAdapter(productDetailTabPagerAdapter);
                tabLayout.setupWithViewPager(viewPager);
                initShare(product);
                showShowcase();
//                initOrderButton(true);
            } else {
                tenorLoader.setVisibility(MKLoader.GONE);
                initOrderButton(false);
                activity(SimpleBaseActivityInitiator.class).showSnackbar(productResponse.getMessage(), v -> {});
            }
        }, throwable -> {
            tenorLoader.setVisibility(MKLoader.GONE);
            initOrderButton(false);
            activity(SimpleBaseActivityInitiator.class).showSnackbar(throwable, v -> {});
        }, false);
        renderBadge();
        floatingActionButton.setOnClickListener(v -> {
            if( SystemApp.checkLogin(activity(), true)){
                HashMap<String, String> hashMap = new HashMap<String, String >();
                hashMap.put("product_id", String.valueOf(product.getId()));
                HttpInstance.withProgress(activity()).call(HttpInstance.create(FavoriteService.class).changeFavorite(hashMap), changeFavoriteResponse ->{
                    ResponseTools.validate(activity(), changeFavoriteResponse);
                    product = changeFavoriteResponse.getProduct();
                    RealmDao.instance().insertOrUpdate(product);
                    if(product.isFavourite()){
                        floatingActionButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorite_red_24dp));
                    }else{
                        floatingActionButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorite_black_24dp));
                    }
                }, throwable -> {
                    product.setFavourite(!product.isFavourite());
                    RealmDao.instance().insertOrUpdate(product);
                    if(product.isFavourite()){
                        floatingActionButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorite_red_24dp));
                    }else{
                        floatingActionButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorite_black_24dp));
                    }
                    activity(HomeActivity.class).showSnackbar(throwable, v1 -> {});
                }, false);
            }
        });
        if (onOrder) {
            WLog.i(TAG, "On CartItem");
        }
    }

    public void renderBadge(){
        if(SystemApp.checkLogin(activity())){
            HttpInstance.call(HttpInstance.create(CartService.class).itemOnCart(), itemOnCart -> {
                ResponseTools.validate(activity(), itemOnCart);
                cart = itemOnCart.getCart() ;
                if(itemOnCart.getCart().getCount()==0){
                    cartBadgeCount.setText(String.valueOf(itemOnCart.getCart().getCount()));
                    cartBadgeCount.setVisibility(View.GONE);
                }else{
                    cartBadgeCount.setText(String.valueOf(itemOnCart.getCart().getCount()));
                    cartBadgeCount.setVisibility(View.VISIBLE);

                }
            }, throwable -> {
//                activity(HomeActivity.class).showSnackbar(throwable, v -> {});
            }, false);
        }else{
            cartBadgeCount.setVisibility(View.GONE);
        }
    }


    void showShowcase(){
        if(!secondTimeOpen){
            secondTimeOpen = true ;
            saver.saveAll();
            Observable.fromCallable(() -> {
                showcaseViewBuilder = new ShowcaseView.Builder(activity());
                showcaseViewBuilder.withHoloShowcase().hideOnTouchOutside().setStyle(R.style.CustomShowcaseTheme4)
                        .setTarget(new ViewTarget(floatingActionButton))
                        .setContentTitle(R.string.product_favorite_guide1)
                        .setContentText(R.string.product_favorite_guide2);

                showcaseViewBuilder.setShowcaseEventListener(new ShowCaseEventListener() {
                    @Override
                    public void onShowcaseViewHide(ShowcaseView showcaseView) {
                        super.onShowcaseViewHide(showcaseView);
                        showcaseViewBuilder = new ShowcaseView.Builder(activity());
                        showcaseViewBuilder.withHoloShowcase().hideOnTouchOutside().setStyle(R.style.CustomShowcaseTheme6)
                                .setTarget(new ViewTarget(floatingShareButton))
                                .setContentTitle(R.string.product_share_guide1)
                                .setContentText(R.string.product_share_guide2);
                        showcaseViewBuilder.setShowcaseEventListener(new ShowCaseEventListener() {
                            @Override
                            public void onShowcaseViewHide(ShowcaseView showcaseView) {
                                super.onShowcaseViewDidHide(showcaseView);
                                nestedScrollView.fullScroll(View.FOCUS_DOWN);

                                showcaseViewBuilder = new ShowcaseView.Builder(activity());
                                showcaseViewBuilder.withHoloShowcase().hideOnTouchOutside().setStyle(R.style.CustomShowcaseTheme4)
                                        .setTarget(new ViewTarget(sellerChatButton))
                                        .setContentTitle(R.string.chat_menu_guide1)
                                        .setContentText(R.string.chat_menu_guide2);
                                showcaseViewBuilder.setShowcaseEventListener(new ShowCaseEventListener() {
                                    @Override
                                    public void onShowcaseViewHide(ShowcaseView showcaseView) {
                                        super.onShowcaseViewHide(showcaseView);
                                        appBarLayout.setExpanded(false);
                                        showcaseViewBuilder = new ShowcaseView.Builder(activity());
                                        showcaseViewBuilder.withHoloShowcase().hideOnTouchOutside().setStyle(R.style.CustomShowcaseTheme5)
                                                .setTarget(new ViewTarget(orderButton))
                                                .setContentTitle(R.string.order_button_guide1)
                                                .setContentText(R.string.order_button_guide2);
                                        showcaseViewBuilder.setShowcaseEventListener(new ShowCaseEventListener() {
                                            @Override
                                            public void onShowcaseViewDidHide(ShowcaseView showcaseView) {
                                                super.onShowcaseViewDidHide(showcaseView);
                                                showcaseViewBuilder = null ;
                                            }
                                        });
                                        showcaseViewBuilder.build();
                                    }
                                });
                                showcaseViewBuilder.build();
                            }

                        });
                        showcaseViewBuilder.build();
                    }
                }).build();
                return true ;
            }).subscribeOn(AndroidSchedulers.mainThread()).subscribe();

        }
    }

    @Override
    public boolean onBackPressed() {
        if(showcaseViewBuilder!=null){
            return false ;
        }
        return true ;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public String initTitle() {
        title = getString(R.string.product_detail);
        collapsingToolbarLayout.setExpandedTitleColor(Color.TRANSPARENT);
        collapsingToolbarLayout.setCollapsedTitleTextColor(Color.WHITE);
        collapsingToolbarLayout.setTitle(title);
        collapsingToolbarLayout.setScrimsShown(true);
        collapsingToolbarLayout.setTitleEnabled(true);
        return title;
    }

    @Override
    public int getMenuId() {
        return 0;
    }

    @Override
    public Product getObject() {
        return product;
    }

    public void retrieveTenors(){
        HashMap hashMap = new HashMap();
        hashMap.put("shipping_cost",  0);
        hashMap.put("insurance_value",  0);
        HttpInstance.call(HttpInstance.create(ProductService.class).getTenors(product.getId(), hashMap), tenorResponse -> {
            ResponseTools.validate(activity(HomeActivity.class), tenorResponse);
            if(tenorResponse.isStatus() && tenorResponse.getHashMap().containsKey("tenors") && tenorResponse.getHashMap().get("tenors").size()>0){
                product.setTenors(RealmDao.listToRealmList(tenorResponse.getHashMap().get("tenors")));
                renderTenor();
            }else{
                activity(HomeActivity.class).showSnackbar(tenorResponse.getMessage(), v -> {});
            }
        }, throwable -> {
            activity(HomeActivity.class).showSnackbar(throwable.getMessage(), v -> {});
        }, false);
    }

    public void renderTenor() {
        String installments = "";
        for (int i = 0; i < product.getTenors().size(); i++) {
            installments += String.valueOf(product.getTenors().get(i).getTenor()) + "x - " + getString(R.string.rp)
                    + " " + DecimalFormat.getInstance().format(Double.valueOf(product.getTenors().get(i).getAmount())) + " / " + getString(R.string.month);
            if (i + 1 < product.getTenors().size()) {
                installments += "\n";
            }
        }
//        orderButton.setVisibility(View.VISIBLE);
//        initOrderButton(true);
        tenorLoader.setVisibility(MKLoader.GONE);
        installmentSimulationTextView.setText(installments);
    }

    private void autoMove(int timemilis, boolean forward){
        alfaBannerView.postDelayed(() -> {
            if(!(activity() instanceof HomeActivity) && isAdded() && !isDetached()){
                return;
            }
            if(activity(HomeActivity.class)!=null && activity(HomeActivity.class).getWindow()!=null && !isDetached() && isVisible()){
                if(forward){
                    if(alfaBannerView.getCurrentItem()<alfaBannerView.getAdapter().getItemCount()-1){
                        alfaBannerView.smoothScrollToPosition(alfaBannerView.getCurrentItem()+1);
                        autoMove(timemilis, true);
                    }else if(alfaBannerView.getAdapter().getItemCount()!=1){
                        alfaBannerView.smoothScrollToPosition(alfaBannerView.getCurrentItem()-1);
                        autoMove(timemilis, false);
                    }
                    return;
                }else{
                    if(alfaBannerView.getCurrentItem()>0){
                        alfaBannerView.smoothScrollToPosition(alfaBannerView.getCurrentItem()-1);
                        autoMove(timemilis, false);
                    }else if(alfaBannerView.getAdapter().getItemCount()!=1){
                        alfaBannerView.smoothScrollToPosition(1);
                        autoMove(timemilis, true);
                    }
                    return;
                }
            }
        }, timemilis);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }


    public void initShare(Product product){
        final Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType(getString(R.string.text_plain));
        shareIntent.putExtra(Intent.EXTRA_TEXT, Uri.parse(product.getCompleteSeourl()).toString());
        IntentPickerSheetView intentPickerSheet = new IntentPickerSheetView(getActivity(), shareIntent, getString(R.string.share_with), activityInfo -> {
            bottomSheetLayout.dismissSheet();
            if(activityInfo.label.equals(getString(R.string.google_plus))){
                if(GoogleSocial.instance(getActivity()).getApiClient().isConnected()){
                    RxActivityResult.startActivityForResult(activity(HomeActivity.class),Auth.GoogleSignInApi.getSignInIntent(GoogleSocial.instance(getActivity()).getApiClient()), 1).subscribe(activityResult -> {
                        RxActivityResult.startActivityForResult(activity(HomeActivity.class),
                                new PlusShare.Builder(getActivity()).setType(getString(R.string.text_plain))
                                        .setText(product.getName()).setContentUrl(Uri.parse(product.getCompleteSeourl())).getIntent()
                        , 2).subscribe(activityResult1 -> {

                        });
                    }, throwable -> {
                        activity(HomeActivity.class).showSnackbar(throwable, view -> {});
                    });
                }else{
                    RxActivityResult.startActivityForResult(activity(HomeActivity.class),
                            new PlusShare.Builder(getActivity()).setType(getString(R.string.text_plain))
                                    .setText(product.getName()).setContentUrl(Uri.parse(product.getCompleteSeourl())).getIntent()
                            , 2).subscribe(activityResult1 -> {
                    });
                }
                return;
            }
            if(activityInfo.label.equals(getString(R.string.facebook))){
                showShareDialog();
                return;
            }

            startActivity(activityInfo.getConcreteIntent(shareIntent));

        });
        intentPickerSheet.setFilter(info -> {
//             return !info.componentName.getPackageName().startsWith("com.android");
            return  info.componentName.getPackageName().contains("com.whatsapp") ||
                    info.componentName.getPackageName().contains("com.instagram") ||
                    info.componentName.getPackageName().contains("org.telegram") ||
                    info.componentName.getPackageName().contains("jp.naver.line.android") ||
                    info.componentName.getPackageName().contains("com.twitter.android") ||
                    info.componentName.getPackageName().contains("com.google.android.apps.docs") ||
                    info.componentName.getPackageName().equals(PhoneTools.getDefaultSmsAppPackageName(getActivity()));
        });
        intentPickerSheet.setSortMethod((o1, o2) -> o1.label.compareTo(o2.label));
        Drawable googleDrawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_google_plus, null);
        IntentPickerSheetView.ActivityInfo googleInfo = new IntentPickerSheetView.ActivityInfo(googleDrawable, getString(R.string.google_plus), getActivity(), SplashActivity.class);
        Drawable facebookDrawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_facebook, null);
        IntentPickerSheetView.ActivityInfo facebookInfo = new IntentPickerSheetView.ActivityInfo(facebookDrawable, getString(R.string.facebook), getActivity(), SplashActivity.class);
        List<IntentPickerSheetView.ActivityInfo> activityInfos = new ArrayList<>();
        Collections.addAll(activityInfos,googleInfo, facebookInfo);
        intentPickerSheet.setMixins(activityInfos);

        floatingShareButton.setOnClickListener(v -> bottomSheetLayout.showWithSheetView(intentPickerSheet));
    }

    private void showShareDialog(){
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            shareDialog.show(linkContent);
        }
    }



}

