package com.pasarwarga.market.fragment.store.product;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.view.animation.FastOutLinearInInterpolator;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.SimpleRecyclerAdapter;
import com.alfarabi.alfalibs.fragments.interfaze.SimpleFragmentCallback;
import com.alfarabi.alfalibs.helper.SwipeCallback;
import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.alfalibs.tools.WLog;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.alfarabi.alfalibs.views.AlfaSwipeRefreshLayout;
import com.alfarabi.base.R2;
import com.alfarabi.base.activity.SimpleBaseActivityInitiator;
import com.alfarabi.base.fragment.home.BaseDrawerFragment;
import com.alfarabi.base.http.basemarket.service.ProductService;
import com.alfarabi.base.model.location.Category;
import com.alfarabi.base.model.Product;
import com.alfarabi.base.realm.RealmDao;
import com.alfarabi.alfalibs.tools.Caster;
import com.alfarabi.base.tools.Initial;
import com.alfarabi.base.tools.ResponseTools;
import com.bumptech.glide.Glide;
import com.hendraanggrian.preferencer.annotations.BindPreferenceRes;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.adapter.ProductSortAdapter;
import com.pasarwarga.market.adapter.recyclerview.RecyclerImageViewAdapter;
import com.pasarwarga.market.fragment.filter.FilterFragment;
import com.pasarwarga.market.fragment.filter.FilterFragmentCallback;
import com.pasarwarga.market.fragment.store.CategoryListFragment;
import com.pasarwarga.market.holder.recyclerview.ProductPromoViewHolder;
import com.tuyenmonkey.mkloader.MKLoader;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;
import lombok.Getter;
import lombok.Setter;
import rjsv.floatingmenu.animation.enumerators.AnimationType;
import rjsv.floatingmenu.floatingmenubutton.FloatingMenuButton;

/**
 * Created by Alfarabi on 6/19/17.
 */

public class ProductPromoListFragment extends BaseDrawerFragment implements SimpleFragmentCallback, FilterFragmentCallback {

    public static final String TAG = ProductPromoListFragment.class.getName();


    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.drawer_layout) DrawerLayout drawerLayout;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout;
    @BindView(R.id.recyclerview) AlfaRecyclerView recyclerView;
    @BindView(R.id.swiperefreshlayout) AlfaSwipeRefreshLayout swipeRefreshLayout;
    @Getter @BindView(R.id.sort_menu_fab) FloatingMenuButton sortFloatingMenuButton;
    @BindView(R.id.loader) MKLoader loader;
    @BindView(R.id.product_count_tv) TextView productcountTextView;
    @Getter RecyclerImageViewAdapter<Product, ProductPromoListFragment, ProductPromoViewHolder> simpleRecyclerAdapter;

    SortDialog sortDialog;

    private Category category;
    private String searchInput = "";
    @Getter@Setter@BindPreferenceRes(R2.string.product_list_column) int productListColumn ;
    private int selectedFilter = 0;

    /*
    * To fill multiple value separate it by coma in every filter field
    * 0 = Categories
    * 1 = Cities
    * 2 = SHIPPING METHODS
    * 3 = MIN PRICE
    * 4 = MAX PRICE
    * */
    @Getter@Setter private String[] filters = {"","","","",""};

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sortDialog = new SortDialog();

        if (getArguments() != null && getArguments().containsKey(Initial.CATEGORY)) {
            category = Parcels.unwrap(getArguments().getParcelable(Initial.CATEGORY));
        }
        if (category == null) {
            category = new Category();
            category.setId(0);
        }

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            drawerToggle = new ActionBarDrawerToggle(activity(), Caster.activity(activity(), HomeActivity.class).getDrawerLayout(), getToolbar(), 0, 0) {
                @Override
                public void onDrawerSlide(View drawerView, float slideOffset) {
                    super.onDrawerSlide(drawerView, slideOffset);
                    if (sortFloatingMenuButton.isMenuOpen()) {
                        sortFloatingMenuButton.closeMenu();
                    }
                    if (activity() != null && Caster.activity(activity(), HomeActivity.class).getSublimeNavigationView() != null) {
                        float moveFactor = (Caster.activity(activity(), HomeActivity.class).getSublimeNavigationView().getWidth() * slideOffset);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                            view.setTranslationX(moveFactor);
                        } else {
                            TranslateAnimation anim = new TranslateAnimation(lastTranslate, moveFactor, 0.0f, 0.0f);
                            anim.setDuration(0);
                            anim.setFillAfter(true);
                            view.startAnimation(anim);
                            lastTranslate = moveFactor;
                        }
                    }
                }
            };
            Caster.activity(activity(), HomeActivity.class).getDrawerLayout().post(() -> drawerToggle.syncState());
            Caster.activity(activity(), HomeActivity.class).getDrawerLayout().setDrawerListener(drawerToggle);
            drawerToggle.syncState();
        } catch (Exception e) {
            e.printStackTrace();
        }

        toolbar.inflateMenu(R.menu.no_menu);
        toolbar.setOnMenuItemClickListener(this);

        sortFloatingMenuButton.setStartAngle(360).setEndAngle(360).setAnimationType(AnimationType.EXPAND).setAnchored(true);

        sortFloatingMenuButton.getAnimationHandler().setOpeningAnimationDuration(500)
                .setClosingAnimationDuration(200).setLagBetweenItems(0)
                .setOpeningInterpolator(new FastOutSlowInInterpolator())
                .setClosingInterpolator(new FastOutLinearInInterpolator())
                .shouldFade(true).shouldScale(true).shouldRotate(true);

        sortFloatingMenuButton.getSubMenuButtons().get(0).getView().setOnClickListener(v -> {
            sortFloatingMenuButton.closeMenu();
            if(productListColumn==1){
                setProductListColumn(2);
                sortFloatingMenuButton.getSubMenuButtons().get(0).getView().setBackgroundDrawable(getResources().getDrawable(R.drawable.ic_list_1));
            }else{
                setProductListColumn(1);
                sortFloatingMenuButton.getSubMenuButtons().get(0).getView().setBackgroundDrawable(getResources().getDrawable(R.drawable.ic_list_2));
            }
            if(saver!=null){
                saver.saveAll();
            }
            Bundle bundle = new Bundle();
            bundle.putParcelable(Initial.CATEGORY, Parcels.wrap(category));
            WindowFlow.force().goTo(activity(HomeActivity.class), ProductPromoListFragment.TAG, bundle, R.id.fragment_frame, false);

        });
        sortFloatingMenuButton.getSubMenuButtons().get(1).getView().setOnClickListener(v -> {
            sortFloatingMenuButton.closeMenu();
            sortFloatingMenuButton.postDelayed(() -> WindowFlow.force().withAnim(R.anim.enter_from_bottom, R.anim.exit_to_top).goTo(activity(HomeActivity.class), FilterFragment.TAG, R.id.framelayout, true), 50);
        });
        sortFloatingMenuButton.getSubMenuButtons().get(2).getView().setOnClickListener(v -> {
            sortFloatingMenuButton.closeMenu();
            sortFloatingMenuButton.postDelayed(() -> sortDialog.show(), 50);
        });
        sortFloatingMenuButton.getSubMenuButtons().get(3).getView().setOnClickListener(v -> {
            sortFloatingMenuButton.closeMenu();
            sortFloatingMenuButton.postDelayed(() -> WindowFlow.force().goTo(activity(HomeActivity.class), CategoryListFragment.TAG, R.id.fragment_frame, true, false), 50);
        });

        simpleRecyclerAdapter = new RecyclerImageViewAdapter<Product, ProductPromoListFragment, ProductPromoViewHolder>(this, ProductPromoViewHolder.class, category.getProducts()){
            @Override
            public void onViewRecycled(ProductPromoViewHolder holder) {
                super.onViewRecycled(holder);
                WLog.d(TAG, "Clear Glide On Scroll");
                GlideApp.with(ProductPromoListFragment.this).clear(holder.getImageView());

            }
        };
        simpleRecyclerAdapter.initRecyclerView(recyclerView, new GridLayoutManager(getActivity(), productListColumn));
        if (category.getId() == 0 && (category.getProducts() == null || category.getProducts().size() == 0)) {
            simpleRecyclerAdapter.setObjects(RealmDao.instance().getList(Product.class, "id", 12, false));
        }

        swipeRefreshLayout.setOnSwipeListener(new SwipeCallback() {
            @Override
            public void onSwipeFromTop() {
                if(isFiltered()){
                    filteredRequest(false, swipeRefreshLayout.startPage());
                }else{
                    basicRequest(false, swipeRefreshLayout.startPage());
                }

            }

            @Override
            public void onSwipeFromBottom() {
                if(isFiltered()){
                    filteredRequest(true, swipeRefreshLayout.increasePage());
                }else{
                    basicRequest(true, swipeRefreshLayout.increasePage());
                }
            }
        });
        productcountTextView.setText(getString(R.string.product_count) + " : " + String.valueOf(0));

    }

    @Override
    public void onResume() {
        super.onResume();
        if (simpleRecyclerAdapter.getObjects().size() == 0) {
            loader.setVisibility(MKLoader.VISIBLE);
            recyclerView.getEmptyView().setVisibility(LinearLayout.GONE);
        }
        basicRequest(false, swipeRefreshLayout.startPage());
    }

    @Override
    public void onDestroyView() {
        if (sortFloatingMenuButton.isMenuOpen())
            sortFloatingMenuButton.closeMenu();
        super.onDestroyView();
    }

    @Override
    public void onPause() {
        super.onPause();
        sortFloatingMenuButton.closeMenu();
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_product_promo_list;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public Object getObject() {
        return null;
    }

    @Override
    public String initTitle() {
        title = getString(R.string.promo);
        toolbar.setTitle(title);
        return title;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    @Override
    public int getMenuId() {
        return R.id.promo_menu_item;
    }

    @Override
    public boolean keepSideMenu() {
        return true;
    }

    @Override
    public boolean onBackPressed() {
        if (sortFloatingMenuButton.isMenuOpen()) {
            sortFloatingMenuButton.closeMenu();
        }
        return super.onBackPressed();
    }

    public void basicRequest(boolean modeAdd, int page){
        HttpInstance.call(HttpInstance.create(ProductService.class).getProductsBy(Initial.TODAY, category.getId(), page, sortDialog.getSortAdapter().getSelectedSort().getSortValue()), productsResponse -> {
            ResponseTools.validate(activity(), productsResponse);
            if(productsResponse.isStatus()){
                category.setProducts(RealmDao.listToRealmList(productsResponse.getProducts()));
                if(modeAdd){
                    simpleRecyclerAdapter.appendObjects(productsResponse.getProducts());
                }else{
                    simpleRecyclerAdapter.setObjects(productsResponse.getProducts());
                }
            }/*else{
                simpleRecyclerAdapter.setObjects(new ArrayList<>());
            }*/
            simpleRecyclerAdapter.notifyDataSetChanged();

            swipeRefreshLayout.finishRefresh();
            swipeRefreshLayout.finishRefreshLoadMore();
            loader.setVisibility(MKLoader.GONE);

            productcountTextView.setText(getString(R.string.product_count) + " : " + String.valueOf(simpleRecyclerAdapter.getItemCount()));
        }, throwable -> {
            swipeRefreshLayout.finishRefresh();
            swipeRefreshLayout.finishRefreshLoadMore();
            loader.setVisibility(MKLoader.GONE);
            productcountTextView.setText(getString(R.string.product_count) + " : " + String.valueOf(simpleRecyclerAdapter.getItemCount()));
            activity(SimpleBaseActivityInitiator.class).showSnackbar(throwable, v -> {});
        }, false);
    }

    public void filteredRequest(boolean modeAdd, int page){
        if(page==1){
            loader.setVisibility(MKLoader.VISIBLE);
        }

        HttpInstance.call(HttpInstance.create(ProductService.class).getProducts(Initial.TODAY, filters[0], filters[1], filters[2], filters[3], filters[4],
                sortDialog.getSortAdapter().getSelectedSort().getSortValue(), page), productsResponse -> {
            ResponseTools.validate(this, productsResponse);
            if(productsResponse.isStatus()){
                category.setProducts(RealmDao.listToRealmList(productsResponse.getProducts()));
                if(modeAdd){
                    simpleRecyclerAdapter.appendObjects(productsResponse.getProducts());
                }else{
                    simpleRecyclerAdapter.setObjects(productsResponse.getProducts());
                }
            }/*else{
                simpleRecyclerAdapter.setObjects(new ArrayList<>());
            }*/
            simpleRecyclerAdapter.notifyDataSetChanged();
            swipeRefreshLayout.finishRefresh();
            swipeRefreshLayout.finishRefreshLoadMore();
            loader.setVisibility(MKLoader.GONE);
            productcountTextView.setText(getString(R.string.product_count) + " : " + String.valueOf(simpleRecyclerAdapter.getItemCount()));
        }, throwable -> {
            swipeRefreshLayout.finishRefresh();
            swipeRefreshLayout.finishRefreshLoadMore();
            loader.setVisibility(MKLoader.GONE);
            simpleRecyclerAdapter.notifyDataSetChanged();
            activity(SimpleBaseActivityInitiator.class).showSnackbar(throwable, v -> {
            });
        }, false);
    }

    public class SortDialog {

        @BindView(R.id.recycler_sort) RecyclerView recyclerView;

        private BottomSheetDialog dialog;
        @Getter private ProductSortAdapter sortAdapter;

        SortDialog() {
            View view = View.inflate(activity(HomeActivity.class), R.layout.dialog_sort_list, null);
            dialog = new BottomSheetDialog(activity(HomeActivity.class));
            dialog.setContentView(view, new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

            BottomSheetBehavior.from(((View) view.getParent())).setHideable(false);
            sortAdapter = new ProductSortAdapter();

            ButterKnife.bind(this, view);
            recyclerView.setLayoutManager(new LinearLayoutManager(activity(HomeActivity.class)));
            recyclerView.setAdapter(sortAdapter);
            sortAdapter.notifyDataSetChanged();

            sortAdapter.setOnSortClickListener(selectedSort -> {
                if (sortDialog != null && sortDialog.dialog != null && sortDialog.dialog.isShowing()) {
                    sortDialog.dismiss();
                }
                ProductPromoListFragment.this.loader.setVisibility(MKLoader.VISIBLE);
                ProductPromoListFragment.this.recyclerView.getEmptyView().setVisibility(LinearLayout.GONE);
                if(isFiltered()){
                    filteredRequest(false, swipeRefreshLayout.startPage());
                }else{
                    basicRequest(false, swipeRefreshLayout.startPage());

                }
            });
        }

        public void show() {
            dialog.show();
        }

        public void dismiss() {
            dialog.dismiss();
        }
    }

    @Override
    public boolean isFiltered(){
        boolean filtered = false ;
        for (String s : filters) {
            if(!s.isEmpty()){
                filtered = true ;
                break;
            }
        }
        return filtered ;
    }

    @Override
    public void resetFilter(){
        filters[0] = "";
        filters[1] = "";
        filters[2] = "";
        filters[3] = "";
        filters[4] = "";
    }

    @Override
    public void setFilter(String categories, String cities, String shippingMethods, String minprice, String maxPrice){
        if(categories==null && cities==null && shippingMethods==null && minprice==null && maxPrice==null){
            basicRequest(true, swipeRefreshLayout.startPage());
            return;
        }
        filters[0] = categories;
        filters[1] = cities;
        filters[2] = shippingMethods;
        filters[3] = minprice;
        filters[4] = maxPrice;
        filteredRequest(false, swipeRefreshLayout.startPage());
    }
}

