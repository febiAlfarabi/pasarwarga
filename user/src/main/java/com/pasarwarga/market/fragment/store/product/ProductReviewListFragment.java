package com.pasarwarga.market.fragment.store.product;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.alfarabi.alfalibs.adapters.recyclerview.SimpleRecyclerAdapter;
import com.alfarabi.alfalibs.helper.SwipeCallback;
import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.alfarabi.alfalibs.views.AlfaSwipeRefreshLayout;
import com.alfarabi.base.fragment.home.BaseDrawerFragment;
import com.alfarabi.base.http.basemarket.service.ReviewService;
import com.alfarabi.base.model.Product;
import com.alfarabi.base.model.review.Review;
import com.alfarabi.base.realm.RealmDao;
import com.alfarabi.base.tools.ResponseTools;
import com.hendraanggrian.bundler.annotations.BindExtra;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.holder.recyclerview.ProductReviewViewHolder;

import java.util.ArrayList;

import butterknife.BindView;
import io.realm.RealmList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductReviewListFragment extends BaseDrawerFragment<Product> {

    public static final String TAG = ProductReviewListFragment.class.getName();

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout;
    @BindView(R.id.recyclerview) AlfaRecyclerView<Review> reviewAlfaRecyclerView;
    @BindView(R.id.swiperefreshlayout) AlfaSwipeRefreshLayout swipeRefreshLayout ;
    SimpleRecyclerAdapter<Review, ProductReviewListFragment, ProductReviewViewHolder> simpleRecyclerAdapter;

    @BindExtra Product product ;

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public int getMenuId() {
        return 0;
    }

    @Override
    public String getTAG() {
        return TAG ;
    }

    @Override
    public String initTitle() {
        title = getString(R.string.review);
        toolbar.setNavigationOnClickListener(v -> WindowFlow.backstack(activity(HomeActivity.class)));
        toolbar.setTitle(title);
        return title;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_product_review;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setObject(RealmDao.unmanage(product));
        simpleRecyclerAdapter = new SimpleRecyclerAdapter<>(this, ProductReviewViewHolder.class, new ArrayList<Review>());
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setData();
        swipeRefreshLayout.setOnSwipeListener(new SwipeCallback() {
            @Override
            public void onSwipeFromTop() {
                HttpInstance.call(HttpInstance.create(ReviewService.class).getReviews(getObject().getId(), swipeRefreshLayout.startPage()), reviewResponse -> {
                    ResponseTools.validate(getActivity(), reviewResponse);
                    if(reviewResponse.getReviews()!=null && reviewResponse.getReviews().size()>0){
                        RealmList realmList = new RealmList();
                        realmList.addAll(reviewResponse.getReviews());
                        getObject().setReviews(realmList);
                        RealmDao.instance().insertOrUpdate(getObject());
                        swipeRefreshLayout.finishRefresh();
                        setObject(RealmDao.unmanage(getObject()));
                        simpleRecyclerAdapter.setObjects(reviewResponse.getReviews());
                    }
                    setData();
                    simpleRecyclerAdapter.notifyDataSetChanged();
                }, throwable -> {
                    setData();
                    simpleRecyclerAdapter.getObjects().addAll(getObject().getReviews());
                    simpleRecyclerAdapter.notifyDataSetChanged();
                }, false);
            }

            @Override
            public void onSwipeFromBottom() {
                HttpInstance.call(HttpInstance.create(ReviewService.class).getReviews(getObject().getId(), swipeRefreshLayout.increasePage()), reviewResponse -> {
                    ResponseTools.validate(getActivity(), reviewResponse);
                    if(reviewResponse.getReviews()!=null && reviewResponse.getReviews().size()>0){
                        RealmList realmList = new RealmList();
                        realmList.addAll(reviewResponse.getReviews());
                        getObject().setReviews(realmList);
                        RealmDao.instance().insertOrUpdate(getObject());
                        setObject(RealmDao.unmanage(getObject()));
                        simpleRecyclerAdapter.getObjects().addAll(reviewResponse.getReviews());
                    }
                    swipeRefreshLayout.finishRefreshLoadMore();
                    setData();
                    simpleRecyclerAdapter.notifyDataSetChanged();
                }, throwable -> {
                    setData();
                    swipeRefreshLayout.finishRefreshLoadMore();
                    simpleRecyclerAdapter.getObjects().addAll(getObject().getReviews());
                    simpleRecyclerAdapter.notifyDataSetChanged();
                }, false);
            }
        });
        simpleRecyclerAdapter.initRecyclerView(reviewAlfaRecyclerView, new LinearLayoutManager(getActivity()));

    }

    public void setData(){
        setObject(RealmDao.unmanage(RealmDao.instance().get(getObject().getId(), Product.class)));

    }

    @Override
    public void onResume() {
        super.onResume();
        HttpInstance.call(HttpInstance.create(ReviewService.class).getReviews(getObject().getId(), swipeRefreshLayout.startPage()), reviewResponse -> {
            ResponseTools.validate(getActivity(), reviewResponse);
            if(reviewResponse.getReviews()!=null && reviewResponse.getReviews().size()>0){
                RealmList realmList = new RealmList();
                realmList.addAll(reviewResponse.getReviews());
                getObject().setReviews(realmList);
                RealmDao.instance().insertOrUpdate(getObject());

                setObject(RealmDao.unmanage(getObject()));
                simpleRecyclerAdapter.getObjects().addAll(reviewResponse.getReviews());
            }
            setData();
            simpleRecyclerAdapter.notifyDataSetChanged();
        }, throwable -> {
            setData();
            simpleRecyclerAdapter.getObjects().addAll(getObject().getReviews());
            simpleRecyclerAdapter.notifyDataSetChanged();
        }, false);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }
}
