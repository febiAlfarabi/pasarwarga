package com.pasarwarga.market.fragment.store.product;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.alfarabi.alfalibs.adapters.recyclerview.SimpleRecyclerAdapter;
import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.alfarabi.base.fragment.SimpleBaseFragmentInitiator;
import com.alfarabi.base.http.basemarket.service.ProductService;
import com.alfarabi.base.model.location.Category;
import com.alfarabi.base.model.Specified;
import com.alfarabi.base.tools.ResponseTools;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.holder.recyclerview.SearchCategoryViewHolder;
import com.pasarwarga.market.holder.recyclerview.SpecifiedViewHolder;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;

import butterknife.BindView;
import lombok.Getter;

/**
 * Created by Alfarabi on 6/19/17.
 */

public class ProductSearchFragment extends SimpleBaseFragmentInitiator<String> {

    public static final String TAG = ProductSearchFragment.class.getName();

    @BindView(R.id.specified_rv) AlfaRecyclerView specifiedRecyclerView ;
    @BindView(R.id.category_rv) AlfaRecyclerView categoryRecyclerView ;
    @BindView(R.id.product_loader) MKLoader productLoader ;

    SimpleRecyclerAdapter<Specified, ProductSearchFragment, SpecifiedViewHolder> simpleRecyclerAdapter ;
    SimpleRecyclerAdapter<Category, ProductSearchFragment, SearchCategoryViewHolder> categoryRecyclerAdapter ;


    @Getter String input = "" ;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        simpleRecyclerAdapter = new SimpleRecyclerAdapter<>(this, SpecifiedViewHolder.class, new ArrayList<>());
        categoryRecyclerAdapter = new SimpleRecyclerAdapter<>(this, SearchCategoryViewHolder.class, new ArrayList<>());
        if(getArguments()!=null){
            input = getArguments().getString("input");
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        simpleRecyclerAdapter.initRecyclerView(specifiedRecyclerView, new LinearLayoutManager(getContext()));
        categoryRecyclerAdapter.initRecyclerView(categoryRecyclerView, new LinearLayoutManager(getContext()));

}

    public void setInput(String input) {
        this.input = input;
        if(input.length()>0){
            if(productLoader!=null){
                productLoader.setVisibility(View.VISIBLE);
            }
            HttpInstance.call(HttpInstance.create(ProductService.class).search(input), productsResponse -> {
                ResponseTools.validate(activity(), productsResponse);
                if(productsResponse.isStatus()){
                    simpleRecyclerAdapter.setObjects(productsResponse.getProductSearch().getSpecifieds());
                    categoryRecyclerAdapter.setObjects(productsResponse.getProductSearch().getCategories());
                }else{
                    simpleRecyclerAdapter.setObjects(new ArrayList<>());
                    categoryRecyclerAdapter.setObjects(new ArrayList<>());
//                            activity(HomeActivity.class).showSnackbar(productsResponse.getMessage(), v -> {});
                }
                if(productLoader!=null){
                    productLoader.setVisibility(View.GONE);
                }
            }, throwable -> {
                simpleRecyclerAdapter.setObjects(new ArrayList<>());
                categoryRecyclerAdapter.setObjects(new ArrayList<>());
                activity(HomeActivity.class).showSnackbar(throwable, v -> {});
                if(productLoader!=null){
                    productLoader.setVisibility(View.GONE);
                }
            }, false);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_product_search;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public String getObject() {
        return null;
    }
}
