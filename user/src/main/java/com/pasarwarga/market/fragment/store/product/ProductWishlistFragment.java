package com.pasarwarga.market.fragment.store.product;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.alfarabi.alfalibs.adapters.recyclerview.SimpleRecyclerAdapter;
import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.WLog;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.alfarabi.base.fragment.home.BaseDrawerFragment;
import com.alfarabi.base.http.basemarket.service.FavoriteService;
import com.alfarabi.base.model.Product;
import com.alfarabi.base.tools.Initial;
import com.alfarabi.base.tools.ResponseTools;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.fragment.home.HomeFragment;
import com.pasarwarga.market.holder.recyclerview.ProductWishlistViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import lombok.Getter;

/**
 * Created by USER on 9/11/2017.
 */

public class ProductWishlistFragment extends BaseDrawerFragment {

    public static final String TAG = ProductWishlistFragment.class.getName();

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.drawer_layout) DrawerLayout drawerLayout;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout;
    @BindView(R.id.recyclerview) AlfaRecyclerView recyclerView;

    @Getter SimpleRecyclerAdapter<Product, ProductWishlistFragment, ProductWishlistViewHolder> simpleRecyclerAdapter;


    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public int getMenuId() {
        return R.id.wishlist_menu_item;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    @Override
    public boolean keepSideMenu() {
        return true;
    }

    @Override
    public String initTitle() {
        title = getString(R.string.favorite);
        toolbar.setTitle(title);
        return title;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public Object getObject() {
        return null;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_product_wishlist;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        List<Product> products = new ArrayList<>();
        simpleRecyclerAdapter = new SimpleRecyclerAdapter<>(this, ProductWishlistViewHolder.class, products);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        simpleRecyclerAdapter.initRecyclerView(recyclerView, new LinearLayoutManager(activity()));
        recyclerView.getEmptyView().findViewById(R.id.start_shop_button).setOnClickListener(v -> {
            WindowFlow.backstack(activity(HomeActivity.class), HomeFragment.TAG, R.id.fragment_frame);
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        WLog.i(TAG, "TOKEN "+HttpInstance.getHeaderMap().get(Initial.TOKEN));
        HttpInstance.withProgress(activity(HomeActivity.class)).observe(HttpInstance.create(FavoriteService.class).listFavorite(), favoriteListResponse -> {
            ResponseTools.validate(activity(), favoriteListResponse);

            if (favoriteListResponse.isStatus() && favoriteListResponse.getProducts() != null && favoriteListResponse.getProducts().size() > 0)
                simpleRecyclerAdapter.setObjects(favoriteListResponse.getProducts());

        }, throwable -> {
            activity(HomeActivity.class).showSnackbar(throwable, v -> {
            });
        }, false);
    }
}
