package com.pasarwarga.market.fragment.store.profile;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.alfarabi.base.fragment.home.BaseDrawerFragment;
import com.alfarabi.base.model.transaction.Cart;
import com.alfarabi.base.tools.InputTools;
import com.pasarwarga.market.R;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class SendMessageFragment extends BaseDrawerFragment<Cart> {
    
    public static final String TAG = SendMessageFragment.class.getName();

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout;

    @BindView(R.id.receiver_et) EditText receiverEditText ;
    @BindView(R.id.subject_et) EditText subjectEditText ;
    @BindView(R.id.message_et) EditText messageEditText ;
    @BindView(R.id.submit_button) Button button ;

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public int getMenuId() {
        return 0;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    @Override
    public String initTitle() {
        title = getString(R.string.send_message);
        toolbar.setTitle(title);
        return title;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_send_message;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        receiverEditText.setText(myProfile.getUser().getFirstName()+" "+(myProfile.getUser().getLastName()!=null?myProfile.getUser().getLastName():""));
//        receiverEditText.setFocusable(false);
//        int regionCode = PhoneNumberUtil.getInstance().getCountryCodeForRegion("ID");

        button.setOnClickListener(v -> {
            if(InputTools.isComplete(receiverEditText, subjectEditText, messageEditText)){
//                KPOrderParam.setIdentityCode(identityEditText.getText().toString());
//                KPOrderParam.setFullname(fullNameEditText.getText().toString());
//                KPOrderParam.setEmail(emailEditText.getText().toString());
//                WindowFlow.withAnim(R.anim.slide_in_from_right, R.anim.slide_out_to_right).goTo(activity(HomeActivity.class), KPForm2Fragment.TAG,
//                        Bundler.wrap(KPForm2Fragment.class, KPOrderParam), R.id.fragment_frame, true);
            }


        });
    }
}
