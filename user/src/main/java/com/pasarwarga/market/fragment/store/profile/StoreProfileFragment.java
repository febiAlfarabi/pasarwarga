package com.pasarwarga.market.fragment.store.profile;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alfarabi.alfalibs.adapters.viewpager.BaseTabPagerAdapter;
import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.Gilimanuk;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.adapter.StringBannerScollViewAdapter;
import com.alfarabi.base.fragment.home.BaseDrawerTabFragment;
import com.alfarabi.base.http.basemarket.service.SellerService;
import com.alfarabi.base.model.FeaturedShop;
import com.alfarabi.base.model.shop.SellerDashboard;
import com.alfarabi.base.realm.RealmDao;
import com.alfarabi.alfalibs.tools.GoogleSocial;
import com.alfarabi.base.tools.PhoneTools;
import com.alfarabi.base.tools.ResponseTools;
import com.alfarabi.base.view.TabLayout;
import com.alfarabi.base.view.interfaze.ShowCaseEventListener;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.MessageDialog;
import com.facebook.share.widget.ShareDialog;
import com.flipboard.bottomsheet.BottomSheetLayout;
import com.flipboard.bottomsheet.commons.IntentPickerSheetView;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.github.ornolfr.ratingview.RatingView;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.plus.PlusShare;
import com.hendraanggrian.bundler.Bundler;
import com.hendraanggrian.bundler.annotations.BindExtra;
import com.hendraanggrian.preferencer.annotations.BindPreference;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.SplashActivity;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.adapter.SellerDashboardTabPagerAdapter;
import com.pasarwarga.market.fragment.chat.ConversationListFragment;
import com.pasarwarga.market.tools.SystemApp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import cn.nekocode.rxactivityresult.RxActivityResult;
import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;

/**
 * Created by Alfarabi on 6/20/17.
 */

public class StoreProfileFragment extends BaseDrawerTabFragment<SellerDashboard> {

    @BindPreference("com.pasarwarga.market.fragment.store.profile.StoreProfileFragment") boolean secondTimeOpen ;

    public static String TAG = StoreProfileFragment.class.getName();

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout;
    @BindView(R.id.tablayout) TabLayout tabLayout ;
    @BindView(R.id.viewpager) ViewPager viewpager ;
    @BindView(R.id.sellername_tv) TextView sellernameTv ;
    @BindView(R.id.seller_address_tv) TextView sellerAddressTv ;
    @BindView(R.id.seller_profile_iv) ImageView sellerImageView ;
    @BindView(R.id.banner_iv) ImageView bannerImageView ;

    @BindView(R.id.bottomsheet) BottomSheetLayout bottomSheetLayout ;
    @BindView(R.id.favorite_button) CircleImageView favoriteButton ;
    @BindView(R.id.chat_button) CircleImageView chatButton ;
    @BindView(R.id.share_button) CircleImageView shareButton ;
//    @BindView(R.id.location_button) CircleImageView locationButton ;
    @BindView(R.id.rating_bar) RatingView ratingBar ;

    @BindView(R.id.search_et) SearchView searchView ;
    private CallbackManager callbackManager;
    private ShareDialog shareDialog;
    private ShareLinkContent linkContent ;

    @Nullable@BindExtra FeaturedShop featuredShop ;
    private SellerDashboard sellerDashboard ;

    private SellerDashboardTabPagerAdapter sellerDashboardTabPagerAdapter ;
    private StringBannerScollViewAdapter stringBannerScollViewAdapter ;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        linkContent = new ShareLinkContent.Builder().setContentUrl(Uri.parse(getString(R.string.pasarwarga_com))).build();

        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                MessageDialog.show(getActivity(), linkContent);
            }

            @Override
            public void onCancel() {}

            @Override
            public void onError(FacebookException error) {}
        });
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(SystemApp.checkLogin(activity(), false)){
            if(myProfile.getUser().isSeller() && myProfile.getUser().getSellerId().equals(String.valueOf(featuredShop.getId()))){
                chatButton.setVisibility(View.GONE);
            }
        }
        setHasOptionsMenu(true);
        toolbar.inflateMenu(R.menu.store_profile_menu);

        sellerDashboardTabPagerAdapter = new SellerDashboardTabPagerAdapter<>(getChildFragmentManager(), getContext());

    }

    @Override
    public void onResume() {
        super.onResume();
        if(featuredShop!=null){
            stringBannerScollViewAdapter = new StringBannerScollViewAdapter(activity(), R.layout.fragment_banner, R.drawable.banner_pasarwarga, featuredShop.getBannerImage());
            String shopName = featuredShop.getShopName()!=null && !featuredShop.getSellerName().equals("")?featuredShop.getShopName():featuredShop.getSellerName();
            sellernameTv.setText(shopName);
            sellerAddressTv.setText(featuredShop.getAddress());
            try{
                ratingBar.setRating(Float.valueOf(featuredShop.getShopRatting()));
            }catch (Exception e){
                ratingBar.setRating(0);
                e.printStackTrace();
            }
            GlideApp.with(this).load(featuredShop.getStoreImage()).placeholder(R.drawable.ic_pasarwarga).error(R.drawable.ic_pasarwarga)
                    .skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL).into(sellerImageView);
            if(this.sellerDashboard==null) {
                HttpInstance.withProgress(activity()).observe(HttpInstance.create(SellerService.class).sellerDetail(Long.valueOf(featuredShop.getId())), detailResponse -> {
                    ResponseTools.validate(activity(HomeActivity.class), detailResponse, true);
                    this.sellerDashboard = detailResponse.getSellerDashboard();
                    GlideApp.with(this).load(sellerDashboard.getSellerInfo().getBannerImage()).error(R.drawable.banner_pasarwarga).placeholder(R.drawable.banner_pasarwarga)
                            .skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL).into(bannerImageView);
                    sellerDashboardTabPagerAdapter.init(detailResponse.getSellerDashboard());
                    initTabLayout(null);
                    initShare(sellerDashboard);
                    renderDashboard();

                    favoriteButton.setOnClickListener(v -> favoriteSeller());
                    chatButton.setOnClickListener(v -> {
                        if(SystemApp.checkLogin(activity(HomeActivity.class), true)){
                            Gilimanuk.withAnim(appearAnims).goTo(activity(HomeActivity.class), ConversationListFragment.TAG,
                                    Bundler.wrap(ConversationListFragment.class,
                                            ConversationListFragment.buildConversation(RealmDao.unmanage(sellerDashboard.getSellerInfo()))),R.id.fragment_frame, true, false);
                        }
                    });
//                    locationButton.setOnClickListener(v -> {
//                        showMapApplication();
//                    });
                    showShowcase();
                }, throwable -> {
                    activity(HomeActivity.class).showSnackbar(throwable, v -> {});
                }, false);

            }else{
                sellerDashboardTabPagerAdapter.init(sellerDashboard);
                GlideApp.with(this).load(sellerDashboard.getSellerInfo().getBannerImage()).error(R.drawable.banner_pasarwarga).placeholder(R.drawable.ic_pasarwarga)
                        .skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL).into(bannerImageView);
                initTabLayout(null);
                initShare(sellerDashboard);
                renderDashboard();
//                locationButton.setOnClickListener(v -> {product.getCompleteSeourl()
//                    showMapApplication();
//                });

                favoriteButton.setOnClickListener(v -> favoriteSeller());
                chatButton.setOnClickListener(v -> {
                    if(SystemApp.checkLogin(activity(HomeActivity.class), true)){
                        Gilimanuk.withAnim(appearAnims).goTo(activity(HomeActivity.class), ConversationListFragment.TAG,
                                Bundler.wrap(ConversationListFragment.class,
                                        ConversationListFragment.buildConversation(RealmDao.unmanage(sellerDashboard.getSellerInfo()))),R.id.fragment_frame, true, false);
                    }
                });
                showShowcase();
            }



        }
    }

    @Override
    public boolean onBackPressed() {
        if(showcaseViewBuilder!=null){
            return false ;
        }
        return super.onBackPressed();
    }

    void showShowcase(){
        if(!secondTimeOpen){
            secondTimeOpen = true ;
            saver.saveAll();
            Observable.fromCallable(() -> {
                showcaseViewBuilder = new ShowcaseView.Builder(activity());
                showcaseViewBuilder.withHoloShowcase().hideOnTouchOutside().setStyle(R.style.CustomShowcaseTheme4)
                        .setTarget(new ViewTarget(searchView))
                        .setContentTitle(R.string.search_product_guide1)
                        .setContentText(R.string.search_product_guide2);

                showcaseViewBuilder.setShowcaseEventListener(new ShowCaseEventListener() {
                    @Override
                    public void onShowcaseViewHide(ShowcaseView showcaseView) {
                        super.onShowcaseViewHide(showcaseView);
                        showcaseViewBuilder = new ShowcaseView.Builder(activity());
                        showcaseViewBuilder.withHoloShowcase().hideOnTouchOutside().setStyle(R.style.CustomShowcaseTheme6)
                                .setTarget(new ViewTarget(favoriteButton))
                                .setContentTitle(R.string.store_favorite_guide1)
                                .setContentText(R.string.store_favorite_guide2);
                        showcaseViewBuilder.setShowcaseEventListener(new ShowCaseEventListener() {
                            @Override
                            public void onShowcaseViewHide(ShowcaseView showcaseView) {
                                super.onShowcaseViewDidHide(showcaseView);

                                showcaseViewBuilder = new ShowcaseView.Builder(activity());
                                showcaseViewBuilder.withHoloShowcase().hideOnTouchOutside().setStyle(R.style.CustomShowcaseTheme4)
                                        .setTarget(new ViewTarget(chatButton))
                                        .setContentTitle(R.string.chat_menu_guide1)
                                        .setContentText(R.string.chat_menu_guide2);
                                showcaseViewBuilder.setShowcaseEventListener(new ShowCaseEventListener() {
                                    @Override
                                    public void onShowcaseViewDidHide(ShowcaseView showcaseView) {
                                        super.onShowcaseViewDidHide(showcaseView);
                                        showcaseViewBuilder = null ;
                                    }
                                });
                                showcaseViewBuilder.build();
                            }

                        });
                        showcaseViewBuilder.build();
                    }
                }).build();
                return true ;
            }).subscribeOn(AndroidSchedulers.mainThread()).subscribe();
        }
    }
    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_seller_dashboard;
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public BaseTabPagerAdapter<?> baseTabPagerAdapter() {
        return sellerDashboardTabPagerAdapter ;
    }

    @Override
    public TabLayout getTabLayout() {
        return tabLayout;
    }

    @Override
    public ViewPager getViewPager() {
        return viewpager ;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public String initTitle() {
        return null;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    @Override
    public int getMenuId() {
        return 0;
    }

    @Override
    public SellerDashboard getObject() {
        return null;
    }

    @Override
    public boolean keepSideMenu() {
        return true;
    }


    public void renderDashboard(){
        if(sellerDashboard.getSellerInfo().isFavorite()){
            favoriteButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorite_red_24dp));
        }else{
            favoriteButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorite_white_24dp));
        }
        toolbar.setOnMenuItemClickListener(item -> {
            if(item.getItemId()==R.id.report_menu){
                Gilimanuk.withAnim(slideAnims).goTo(activity(HomeActivity.class), StoreReportListFragment.TAG, Bundler.wrap(StoreReportListFragment.class, sellerDashboard), R.id.fragment_frame, true, false);
            }
            return true ;
        });
    }

    public void initShare(SellerDashboard sellerDashboard){
        final Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType(getString(R.string.text_plain));
        shareIntent.putExtra(Intent.EXTRA_TEXT, Uri.parse(sellerDashboard.getSellerInfo().getSeourl()).toString());
        IntentPickerSheetView intentPickerSheet = new IntentPickerSheetView(getActivity(), shareIntent, getString(R.string.share_with), activityInfo -> {
            bottomSheetLayout.dismissSheet();
            if(activityInfo.label.equals(getString(R.string.google_plus))){
                if(GoogleSocial.instance(getActivity()).getApiClient().isConnected()){
                    RxActivityResult.startActivityForResult(activity(), Auth.GoogleSignInApi.getSignInIntent(GoogleSocial.instance(getActivity()).getApiClient()), 1).subscribe(activityResult -> {
                        RxActivityResult.startActivityForResult(activity(),
                                new PlusShare.Builder(getActivity()).setType(getString(R.string.text_plain))
                                        .setText(sellerDashboard.getSellerInfo().getShopName()).setContentUrl(Uri.parse(sellerDashboard.getSellerInfo().getSeourl())).getIntent()
                                , 2).subscribe(activityResult1 -> {

                        });
                    }, throwable -> {
                        Toast.makeText(getActivity(), getString(R.string.error), Toast.LENGTH_SHORT).show();
                    });

//                    Dispatcher.with(this).startActivityForResult(Auth.GoogleSignInApi.getSignInIntent(GoogleSocial.instance(getActivity()).getApiClient())).dispatch(data -> {
//                        Dispatcher.with(this).startActivityForResult(
//                                new PlusShare.Builder(getActivity()).setType(getString(R.string.text_plain))
//                                        .setText(sellerDashboard.getSellerInfo().getShopName()).setContentUrl(Uri.parse(sellerDashboard.getSellerInfo().getSeourl())).getIntent())
//                                .dispatch(data1 -> {});
//                    }, data -> {
//                        Toast.makeText(getActivity(), getString(R.string.error), Toast.LENGTH_SHORT).show();
//                    });
                }else{
                    RxActivityResult.startActivityForResult(activity(),
                            new PlusShare.Builder(getActivity()).setType(getString(R.string.text_plain))
                                    .setText(sellerDashboard.getSellerInfo().getShopName()).setContentUrl(Uri.parse(sellerDashboard.getSellerInfo().getSeourl())).getIntent()
                            , 2).subscribe(activityResult1 -> {

                    });
//                    Dispatcher.with(this).startActivityForResult(
//                            new PlusShare.Builder(getActivity()).setType(getString(R.string.text_plain))
//                                    .setText(sellerDashboard.getSellerInfo().getShopName()).setContentUrl(Uri.parse(sellerDashboard.getSellerInfo().getSeourl())).getIntent())
//                            .dispatch(data1 -> {});
                }
                return;
            }
            if(activityInfo.label.equals(getString(R.string.facebook))){
                showShareDialog();
                return;
            }

            startActivity(activityInfo.getConcreteIntent(shareIntent));

        });
        intentPickerSheet.setFilter(info -> {
//             return !info.componentName.getPackageName().startsWith("com.android");
            return  info.componentName.getPackageName().contains("com.whatsapp") ||
                    info.componentName.getPackageName().contains("com.instagram") ||
                    info.componentName.getPackageName().contains("org.telegram") ||
                    info.componentName.getPackageName().contains("jp.naver.line.android") ||
                    info.componentName.getPackageName().contains("com.twitter.android") ||
                    info.componentName.getPackageName().contains("com.google.android.apps.docs") ||
                    info.componentName.getPackageName().equals(PhoneTools.getDefaultSmsAppPackageName(getActivity()));
        });
        intentPickerSheet.setSortMethod((o1, o2) -> o1.label.compareTo(o2.label));
        Drawable googleDrawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_google_plus, null);
        IntentPickerSheetView.ActivityInfo googleInfo = new IntentPickerSheetView.ActivityInfo(googleDrawable, getString(R.string.google_plus), getActivity(), SplashActivity.class);
        Drawable facebookDrawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_facebook, null);
        IntentPickerSheetView.ActivityInfo facebookInfo = new IntentPickerSheetView.ActivityInfo(facebookDrawable, getString(R.string.facebook), getActivity(), SplashActivity.class);
        List<IntentPickerSheetView.ActivityInfo> activityInfos = new ArrayList<>();
        Collections.addAll(activityInfos,googleInfo, facebookInfo);
        intentPickerSheet.setMixins(activityInfos);

        shareButton.setOnClickListener(v -> bottomSheetLayout.showWithSheetView(intentPickerSheet));

    }

    void showMapApplication(){
        String uri = "http://maps.google.com/maps?q=loc:"+String.valueOf(sellerDashboard.getSellerInfo().getLocation().getLatitude())
                +","+String.valueOf(sellerDashboard.getSellerInfo().getLocation().getLongitude())+" ("+getActivity().getPackageName()+")";
        Intent shareIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        shareIntent.setFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
        shareIntent.setFlags(Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP);
        IntentPickerSheetView intentPickerSheet = new IntentPickerSheetView(getActivity(), shareIntent, getString(R.string.open_with), activityInfo -> {
            bottomSheetLayout.dismissSheet();
            startActivity(activityInfo.getConcreteIntent(shareIntent));
        });
        intentPickerSheet.setFilter(info -> {
            return  info.componentName.getPackageName().contains("waze") ||
                    info.componentName.getPackageName().contains("maps") ||
                    info.componentName.getPackageName().contains("com.google.android.street") ||
                    info.componentName.getPackageName().contains("com.google.earth") ||
                    info.componentName.getPackageName().contains("com.google.android.apps.docs");
        });

        intentPickerSheet.setSortMethod((o1, o2) -> o1.label.compareTo(o2.label));
        bottomSheetLayout.showWithSheetView(intentPickerSheet);
    }

    private void showShareDialog(){
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            shareDialog.show(linkContent);
        }
    }


    public void favoriteSeller(){
        if(SystemApp.checkLogin(activity(HomeActivity.class), true)){
            HttpInstance.withProgress(activity()).observe(HttpInstance.create(SellerService.class).favorite(Long.valueOf(sellerDashboard.getSellerInfo().getId())), sellerFavoriteResponse -> {
                if(sellerDashboard.getSellerInfo().isFavorite()){
                    sellerDashboard.getSellerInfo().setFavorite(false);
                    favoriteButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorite_white_24dp));
                }else{
                    sellerDashboard.getSellerInfo().setFavorite(true);
                    favoriteButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorite_red_24dp));
                }
            }, throwable -> {

            }, false);
        }
    }




}
