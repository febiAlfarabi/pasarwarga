package com.pasarwarga.market.fragment.store.profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.alfarabi.base.fragment.home.BaseDrawerFragment;
import com.alfarabi.base.http.basemarket.service.SellerService;
import com.alfarabi.base.model.shop.SellerDashboard;
import com.alfarabi.alfalibs.tools.Caster;
import com.hendraanggrian.bundler.annotations.BindExtra;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.adapter.StoreReportAdapter;
import com.pasarwarga.market.fragment.home.HomeFragment;

import java.util.HashMap;

import butterknife.BindView;

/**
 * Created by Alfarabi on 6/19/17.
 */

public class StoreReportListFragment extends BaseDrawerFragment {

    public static final String TAG = StoreReportListFragment.class.getName();

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout;
    @BindView(R.id.recyclerview) AlfaRecyclerView recyclerView ;
    @BindView(R.id.report_button) Button reportButton ;

//    @Getter SimpleRecyclerAdapter<Address, StoreReportListFragment, AddressViewHolder> simpleRecyclerAdapter ;

    private StoreReportAdapter storeReportAdapter ;

    @BindExtra SellerDashboard sellerDashboard ;

    private String searchInput = "" ;
    private int column = 2 ;

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        storeReportAdapter = new StoreReportAdapter(activity(), activity(HomeActivity.class).getResources().getStringArray(R.array.report_titles),
                activity(HomeActivity.class).getResources().getStringArray(R.array.report_contents));


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        toolbar.setOnMenuItemClickListener(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity()));
        recyclerView.setAdapter(storeReportAdapter);
        storeReportAdapter.setOnSortClickListener(selectedReport -> {
//            sortedData();
        });
        reportButton.setOnClickListener(v -> {
            activity(HomeActivity.class).showDialog(getString(R.string.confirmation), getString(R.string.seller_report_confirmation), (dialog, which) -> {
                dialog.dismiss();
                HashMap<String, String> hashMap = new HashMap<String, String>();
                hashMap.put("seller_id", sellerDashboard.getSellerInfo().getId());
                hashMap.put("spam_title", storeReportAdapter.getselectedReport().getSortText());
                hashMap.put("complaint", storeReportAdapter.getselectedReport().getSortValue());

                HttpInstance.withProgress(activity()).observe(HttpInstance.create(SellerService.class).report(hashMap), response -> {
                    if(response.isStatus()){
                        activity(HomeActivity.class).showDialog(getString(R.string.successfully_report_seller), (dialog1, which1) -> {
                            dialog1.dismiss();
                            WindowFlow.backstack(activity(HomeActivity.class), HomeFragment.TAG, R.id.fragment_frame);
                        });
                    }else{
                        activity(HomeActivity.class).showDialog(response.getMessage(), (dialog1, which1) -> {
                            dialog1.dismiss();
                        });
                    }
                }, throwable -> {
                    activity(HomeActivity.class).showSnackbar(throwable, v1 -> {});
                });
            }, (dialog, which) -> dialog.dismiss());
        });
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        Caster.activity(activity(), HomeActivity.class).getFabButton().setVisibility(FloatingActionButton.GONE);
        super.onPause();
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_store_report_list;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public Object getObject() {
        return null;
    }

    @Override
    public String initTitle() {
        title = getString(R.string.report_this_store);
        toolbar.setTitle(title);
        return title;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    @Override
    public int getMenuId() {
        return 0;
    }

    @Override
    public boolean keepSideMenu() {
        return false;
    }
}
