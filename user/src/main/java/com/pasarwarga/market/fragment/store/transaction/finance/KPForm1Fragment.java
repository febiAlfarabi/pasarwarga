package com.pasarwarga.market.fragment.store.transaction.finance;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.alfarabi.alfalibs.tools.WLog;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.fragment.home.BaseDrawerFragment;
import com.alfarabi.base.model.transaction.Cart;
import com.alfarabi.base.model.transaction.finance.kreditplus.KPOrderParam;
import com.alfarabi.base.tools.InputTools;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.hendraanggrian.bundler.Bundler;
import com.hendraanggrian.bundler.annotations.BindExtra;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;

import butterknife.BindView;
import lombok.Getter;

/**
 * A simple {@link Fragment} subclass.
 */
public class KPForm1Fragment extends BaseDrawerFragment<Cart> {

    public static final String TAG = KPForm1Fragment.class.getName();

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout;

    @BindView(R.id.identity_code_et) EditText identityEditText;
    @BindView(R.id.fullname_et) EditText fullNameEditText;
    @BindView(R.id.email_et) EditText emailEditText;

    @BindView(R.id.mobile_phone_number_et) EditText mobilePhoneEditText;
    @BindView(R.id.house_prefix_phone_et) EditText housePrefixEditText;
    @BindView(R.id.house_phone_number_et) EditText housePhoneEditText;
    @BindView(R.id.office_prefix_phone_et) EditText officePrefixEditText;
    @BindView(R.id.office_phone_number_et) EditText officePhoneEditText;

    @BindView(R.id.next_button) Button button;


    public static final int DECREASE_COUNT = 120;
    @Getter MaterialDialog materialDialog;
    EditText tokenEditText;
    TextView dialogMobilePhoneNumberEditText;
    Button dialogCancelButton;
    Button dialogSubmitButton;
    @Getter int timeCount = 0;


    @BindExtra
    KPOrderParam KPOrderParam;

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public int getMenuId() {
        return 0;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    @Override
    public String initTitle() {
        title = getString(R.string.payment_method);
        toolbar.setTitle(title);
        return title;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_kpform1;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fullNameEditText.setText(myProfile.getUser().getFirstName() + " " + (myProfile.getUser().getLastName() != null ? myProfile.getUser().getLastName() : ""));
        //fullNameEditText.setFocusable(false);
        mobilePhoneEditText.setText(myProfile.getUser().getPhone());
        mobilePhoneEditText.setFocusable(false);
        int regionCode = PhoneNumberUtil.getInstance().getCountryCodeForRegion("ID");
        WLog.i(TAG, String.valueOf(regionCode));
        officePrefixEditText.setText(String.valueOf("+" + String.valueOf(regionCode) + ""));
        officePrefixEditText.setFocusable(false);
        housePrefixEditText.setText(String.valueOf("+" + String.valueOf(regionCode) + ""));
        housePrefixEditText.setFocusable(false);
        housePhoneEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
        officePhoneEditText.setInputType(InputType.TYPE_CLASS_NUMBER);

        button.setOnClickListener(v -> {
            if (InputTools.isComplete(identityEditText, fullNameEditText, emailEditText, mobilePhoneEditText, housePrefixEditText, housePhoneEditText, officePrefixEditText, officePhoneEditText)
                    && InputTools.isValidEmail(emailEditText)) {

                KPOrderParam.setIdentityCode(identityEditText.getText().toString());
                KPOrderParam.setFullname(fullNameEditText.getText().toString());
                KPOrderParam.setEmail(emailEditText.getText().toString());
                KPOrderParam.setHomePhonePrefix(housePrefixEditText.getText().toString());
                KPOrderParam.setHomePhoneNumber(housePhoneEditText.getText().toString());
                KPOrderParam.setMobilePhoneNumber(mobilePhoneEditText.getText().toString());
                KPOrderParam.setOfficePhonePrefix(officePrefixEditText.getText().toString());
                KPOrderParam.setOfficePhoneNumber(officePhoneEditText.getText().toString());
                WindowFlow.withAnim(R.anim.slide_in_from_right, R.anim.slide_out_to_right).goTo(activity(HomeActivity.class), KPForm2Fragment.TAG,
                        Bundler.wrap(KPForm2Fragment.class, KPOrderParam), R.id.fragment_frame, true);
//                dialogMobilePhoneNumberEditText.setText(getText(R.string.your_phone_number_is) + " : " + mobilePhoneEditText.getText().toString());
//                KeyboardUtils.hideKeyboard(getActivity());
//                HashMap<String, String> hashMap = new HashMap<String, String>();
//                hashMap.put("phone_number", mobilePhoneEditText.getText().toString());
//                HttpInstance.withProgress(getActivity()).observe(HttpInstance.create(PhoneVerificationService.class).requestSmsVerification(hashMap), response -> {
//                    if (response.isStatus()) {
//                        tokenEditText.setText("");
//                        materialDialog.setCancelable(false);
//                        materialDialog.show();
//                        timeCount = DECREASE_COUNT;
//                        decreasecounter();
//                    } else {
//                        activity(HomeActivity.class).showDialog(response.getMessage(), (dialog, which) -> {
//                            mobilePhoneEditText.setText("");
//                            dialog.dismiss();
//                        });
//                    }
//                }, throwable -> {
//                    activity(HomeActivity.class).showSnackbar(throwable, v1 -> {
//                    });
//                }, false);
            }
        });

        materialDialog = new MaterialDialog.Builder(getActivity()).title(getString(R.string.sms_token_confirmation)).customView(R.layout.dialog_sms_confirmation, false).build();
        View dialogView = materialDialog.getView();
        tokenEditText = (EditText) dialogView.findViewById(R.id.token_et);
        dialogMobilePhoneNumberEditText = (TextView) dialogView.findViewById(R.id.mobile_phone_number_tv);
        dialogCancelButton = (Button) dialogView.findViewById(R.id.cancel_button);
        dialogSubmitButton = (Button) dialogView.findViewById(R.id.submit_button);
        materialDialog.setCanceledOnTouchOutside(false);

//        dialogCancelButton.setOnClickListener(v -> {
//            KeyboardUtils.hideKeyboard(getActivity());
//            if (dialogCancelButton.getText().toString().equalsIgnoreCase(getString(R.string.cancel) + " (0)")) {
//                tokenEditText.setText("");
//                materialDialog.dismiss();
//                mobilePhoneEditText.setText("");
//                activity(HomeActivity.class).showDialog(getString(R.string.warning), getString(R.string.makesure_phone_number_active), (dialog, which) -> dialog.dismiss());
//            }
//        });

//        dialogSubmitButton.setOnClickListener(v -> {
//            if (InputTools.isComplete(tokenEditText)) {
//                materialDialog.dismiss();
//                HttpInstance.withProgress(getActivity()).observe(HttpInstance.create(PhoneVerificationService.class).verifiyCode(tokenEditText.getText().toString()), response -> {
//                    if (response.isStatus()) {
//                        KPOrderParam.setIdentityCode(identityEditText.getText().toString());
//                        KPOrderParam.setFullname(fullNameEditText.getText().toString());
//                        KPOrderParam.setEmail(emailEditText.getText().toString());
//                        KPOrderParam.setHomePhonePrefix(housePrefixEditText.getText().toString());
//                        KPOrderParam.setHomePhoneNumber(housePhoneEditText.getText().toString());
//                        KPOrderParam.setMobilePhoneNumber(mobilePhoneEditText.getText().toString());
//                        KPOrderParam.setOfficePhonePrefix(officePrefixEditText.getText().toString());
//                        KPOrderParam.setOfficePhoneNumber(officePhoneEditText.getText().toString());
//                        WindowFlow.withAnim(R.anim.slide_in_from_right, R.anim.slide_out_to_right).goTo(activity(HomeActivity.class), KPForm2Fragment.TAG,
//                                Bundler.wrap(KPForm2Fragment.class, KPOrderParam), R.id.fragment_frame, true);
//                    } else {
//                        activity(HomeActivity.class).showDialog(response.getMessage(), (dialog, which) -> {
//                            dialog.dismiss();
//                            materialDialog.show();
//                            tokenEditText.setText("");
//                        });
//                    }
//                }, throwable -> {
//                    materialDialog.show();
//                    tokenEditText.setText("");
//                    activity(HomeActivity.class).showSnackbar(throwable, v1 -> {
//                    });
//                }, false);
//            }
//        });
    }

    public void decreasecounter() {
        dialogCancelButton.postDelayed(() -> {
            timeCount = timeCount > 0 ? timeCount - 1 : 0;
            dialogCancelButton.setText(String.valueOf(getString(R.string.cancel) + " (" + String.valueOf(timeCount) + ")"));
            if (timeCount > 0) {
                decreasecounter();
            } else {
                materialDialog.setCancelable(true);
            }
        }, 1000);
    }
}
