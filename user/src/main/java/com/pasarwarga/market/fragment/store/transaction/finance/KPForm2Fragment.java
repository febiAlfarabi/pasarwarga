package com.pasarwarga.market.fragment.store.transaction.finance;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.ArrayCalendarSpinner;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.fragment.home.BaseDrawerFragment;
import com.alfarabi.base.http.basemarket.service.CityService;
import com.alfarabi.base.model.location.City;
import com.alfarabi.base.model.transaction.Cart;
import com.alfarabi.base.model.transaction.finance.kreditplus.KPOrderParam;
import com.alfarabi.base.tools.InputTools;
import com.alfarabi.alfalibs.tools.KeyboardUtils;
import com.alfarabi.base.tools.ResponseTools;
import com.alfarabi.base.view.StringMaterialSpinner;
import com.hendraanggrian.bundler.Bundler;
import com.hendraanggrian.bundler.annotations.BindExtra;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;

import org.apache.commons.lang.StringUtils;

import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class KPForm2Fragment extends BaseDrawerFragment<Cart> {
    
    public static final String TAG = KPForm2Fragment.class.getName();
    @BindView(R.id.next_button) Button button ;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout;

    @BindView(R.id.gender_sp) StringMaterialSpinner genderSpinner ;
    @BindView(R.id.birthcity_et) AutoCompleteTextView birthCityAutoCompleteTextView ;
    @BindView(R.id.date_sp) StringMaterialSpinner dateSpinner ;
    @BindView(R.id.month_sp) StringMaterialSpinner monthSpinner ;
    @BindView(R.id.year_sp) StringMaterialSpinner yearSpinner ;
    @BindView(R.id.mother_name_et) EditText motherNameEditText ;
    @BindView(R.id.graduation_sp) StringMaterialSpinner graduationSpinner ;
    @BindView(R.id.marital_status_sp) StringMaterialSpinner maritalStatusSpinner ;
    @BindView(R.id.dependent_sp) StringMaterialSpinner dependentSpinner ;
    @BindView(R.id.residential_status_sp) StringMaterialSpinner residentialStatusSpinner ;
    @BindView(R.id.have_stayed_et) EditText haveStayedEditText ;

    @BindExtra
    KPOrderParam KPOrderParam;

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public int getMenuId() {
        return 0;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    @Override
    public String initTitle() {
        title = getString(R.string.payment_method);
        toolbar.setTitle(title);
        return title;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_kpform2;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        genderSpinner.initDataAdapter(new String[]{getString(R.string.male), getString(R.string.female)}, (parent, view1, position, id) -> {}, "");

        Calendar mCalendar = Calendar.getInstance();
        initDateSpinner(0, mCalendar.get(Calendar.DAY_OF_MONTH), String.valueOf(mCalendar.get(Calendar.DAY_OF_MONTH)));
        initMonthSpinner(0, mCalendar.get(Calendar.MONTH), String.valueOf(mCalendar.get(Calendar.MONTH)));
        initYearSpinner(mCalendar.get(Calendar.YEAR)-110, mCalendar.get(Calendar.YEAR)-21, String.valueOf(mCalendar.get(Calendar.YEAR)-21));
        String[] graduationLevel = getResources().getStringArray(R.array.graduation_level_array);
        graduationSpinner.initDataAdapter(graduationLevel, (parent, view1, position, id) -> {}, "");
        String[] maritalStatuses = getResources().getStringArray(R.array.marital_statuses_array);
        maritalStatusSpinner.initDataAdapter(maritalStatuses, (parent, view1, position, id) -> {}, "");
        String[] dependents = StringUtils.split("0 1 2 3 4 5 6 7 8 9 10", " ");
        dependentSpinner.initDataAdapter(dependents, (parent, view1, position, id) -> {}, "");
        String[] residentials = getResources().getStringArray(R.array.residential_status_array);
        residentialStatusSpinner.initDataAdapter(residentials, (parent, view1, position, id) -> {}, "");

        button.setOnClickListener(v -> {
            if(InputTools.isComplete(genderSpinner, birthCityAutoCompleteTextView, motherNameEditText, graduationSpinner, maritalStatusSpinner, residentialStatusSpinner, haveStayedEditText )){
                KPOrderParam.setGender(genderSpinner.getSelectedDsi());
                KPOrderParam.setBirthcity(birthCityAutoCompleteTextView.getText().toString());
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.DAY_OF_MONTH, Integer.valueOf(dateSpinner.getSelectedDsi()));
                calendar.set(Calendar.MONTH, ArrayCalendarSpinner.getMonth(monthSpinner.getSelectedDsi()));
                calendar.set(Calendar.YEAR, Integer.valueOf(yearSpinner.getSelectedDsi()));
                KPOrderParam.setBirthday(calendar.getTime());
                KPOrderParam.setMothername(motherNameEditText.getText().toString());
                KPOrderParam.setGraduation(graduationSpinner.getSelectedDsi());
                KPOrderParam.setMaritalStatus(maritalStatusSpinner.getSelectedDsi());
                KPOrderParam.setDependent(dependentSpinner.getText().toString());
                KPOrderParam.setResidentialStatus(residentialStatusSpinner.getSelectedDsi());
                KPOrderParam.setHaveStayedfor(haveStayedEditText.getText().toString());

                WindowFlow.withAnim(R.anim.slide_in_from_right, R.anim.slide_out_to_right).goTo(activity(HomeActivity.class), KPForm3Fragment.TAG,
                        Bundler.wrap(KPForm3Fragment.class, KPOrderParam), R.id.fragment_frame, true);
            }


        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if(cities==null){
            requestCity();
        }
    }


    private String[] cities ;
    private ArrayAdapter<String> birthAdapter ;

    public void requestCity(){
        HttpInstance.call(HttpInstance.create(CityService.class).getAllCityByCountry(CityService.IND_COUNTRY_ID), cityResponse -> {
            ResponseTools.validate(getActivity(), cityResponse);

            cities = new String[cityResponse.getCities().size()];
            List<City> cityList = cityResponse.getCities();
            cities = new String[cityList.size()];
            for (int i = 0; i<cityList.size(); i++) {
                cities[i] = cityList.get(i).getName();
            }
            birthAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.select_dialog_item, cities);
            birthCityAutoCompleteTextView.setThreshold(1);
            birthCityAutoCompleteTextView.setAdapter(birthAdapter);
            birthAdapter.notifyDataSetChanged();
            birthCityAutoCompleteTextView.setOnItemClickListener((parent, view1, position, id) -> KeyboardUtils.hideKeyboard(getActivity()));
        }, throwable -> {
            activity(HomeActivity.class).showSnackbar(throwable.getMessage(), v -> {});
        });
    }


    public void initDateSpinner(int min, int max, String def){
        ArrayCalendarSpinner.initDateSpinner(min, max, def);
        dateSpinner.initDataAdapter(ArrayCalendarSpinner.getAllDays(), (parent, view1, position, id) -> {

        },def);

    }

    public void initMonthSpinner(int min, int max, String def){
        ArrayCalendarSpinner.initMonthSpinner(min, max, def);
        monthSpinner.initDataAdapter(ArrayCalendarSpinner.getAllMonths(),(parent, view1, position, id) -> {
            if(!monthSpinner.getSelectedDsi().equalsIgnoreCase(DateFormatSymbols.getInstance().getMonths()[max])){
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.MONTH, ArrayCalendarSpinner.getMonth(monthSpinner.getSelectedDsi()));
                initDateSpinner(0, calendar.getActualMaximum(Calendar.DAY_OF_MONTH), String.valueOf(calendar.getActualMaximum(Calendar.DAY_OF_MONTH)));
            }else{
                Calendar calendar = Calendar.getInstance();
                initDateSpinner(0, calendar.get(Calendar.DAY_OF_MONTH), String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
            }
        }, DateFormatSymbols.getInstance().getMonths()[Integer.valueOf(def)]);
    }

    public void initYearSpinner(int min, int max, String def){
        ArrayCalendarSpinner.initYearSpinner(min, max, def);
        yearSpinner.initDataAdapter(ArrayCalendarSpinner.getAllYears(),(parent, view1, position, id) -> {
            if(!yearSpinner.getSelectedDsi().equals(String.valueOf(max))){
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, Integer.valueOf(yearSpinner.getSelectedDsi()));
                initMonthSpinner(0, 11, String.valueOf(11));
                initDateSpinner(0, calendar.getActualMaximum(Calendar.DAY_OF_MONTH), String.valueOf(calendar.getActualMaximum(Calendar.DAY_OF_MONTH)));
            }else{
                Calendar calendar = Calendar.getInstance();
                initMonthSpinner(0, calendar.get(Calendar.MONTH), String.valueOf(calendar.get(Calendar.MONTH)));
                initDateSpinner(0, calendar.get(Calendar.DAY_OF_MONTH), String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
            }
        },def);
    }
}
