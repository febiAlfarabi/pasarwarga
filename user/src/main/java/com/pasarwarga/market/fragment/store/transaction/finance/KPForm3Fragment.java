package com.pasarwarga.market.fragment.store.transaction.finance;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.fragment.home.BaseDrawerFragment;
import com.alfarabi.base.model.transaction.Cart;
import com.alfarabi.base.model.transaction.finance.kreditplus.KPOrderParam;
import com.alfarabi.base.tools.InputTools;
import com.alfarabi.base.view.StringMaterialSpinner;
import com.github.florent37.viewtooltip.ViewTooltip;
import com.hendraanggrian.bundler.Bundler;
import com.hendraanggrian.bundler.annotations.BindExtra;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class KPForm3Fragment extends BaseDrawerFragment<Cart> {

    public static final String TAG = KPForm3Fragment.class.getName();


    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout;
    @BindView(R.id.employment_type_sp) StringMaterialSpinner employmentTypeSpinner;
    @BindView(R.id.employment_status_sp) StringMaterialSpinner employmentStatusSpinner;
    @BindView(R.id.from_year_sp) StringMaterialSpinner fromYearSpinner;
    @BindView(R.id.until_year_sp) StringMaterialSpinner untilYearSpinner;
    @BindView(R.id.occupation_et) EditText occupationEditText;
    @BindView(R.id.position_et) EditText positionEditText;
    @BindView(R.id.salary_et) EditText salaryEditText;

    @BindView(R.id.next_button) Button button;
    @BindExtra
    KPOrderParam KPOrderParam;

    private String employmentType;

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public int getMenuId() {
        return 0;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    @Override
    public String initTitle() {
        title = getString(R.string.payment_method);
        toolbar.setTitle(title);
        return title;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_kpform3;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        fromYearSpinner.initDataAdapter(allYears, (parent, view1, position, id) -> {
            if (Integer.valueOf(untilYearSpinner.getSelectedDsi().trim()) < Integer.valueOf(fromYearSpinner.getSelectedDsi().trim())) {
                ViewTooltip.on(fromYearSpinner).autoHide(true, 3000)
                        .corner(30).textColor(fromYearSpinner.getResources().getColor(R.color.white))
                        .color(fromYearSpinner.getResources().getColor(R.color.red_700))
                        .position(ViewTooltip.Position.BOTTOM)
                        .text(getString(R.string.from_year_shouldbe_smaller_than_untilyear))
                        .show();
                fromYearSpinner.setSelectedDsi(untilYearSpinner.getSelectedDsi());
            }
        }, allYears.get(0));

        untilYearSpinner.initDataAdapter(allYears, (parent, view1, position, id) -> {
            if (Integer.valueOf(untilYearSpinner.getSelectedDsi().trim()) < Integer.valueOf(fromYearSpinner.getSelectedDsi().trim())) {
                ViewTooltip.on(untilYearSpinner).autoHide(true, 3000)
                        .corner(30).textColor(untilYearSpinner.getResources().getColor(R.color.white))
                        .color(untilYearSpinner.getResources().getColor(R.color.red_700))
                        .position(ViewTooltip.Position.BOTTOM)
                        .text(getString(R.string.until_year_shouldbe_larger_than_fromyear))
                        .show();
                untilYearSpinner.setSelectedDsi(fromYearSpinner.getSelectedDsi());
            }
        }, allYears.get(0));

        employmentTypeSpinner.initDataAdapter(getResources().getStringArray(R.array.employment_type_array), (parent, view1, position, id) -> {
            employmentType = getResources().getStringArray(R.array.employment_type_array_value)[position];
        }, "");
        employmentStatusSpinner.initDataAdapter(getResources().getStringArray(R.array.employment_status_array), (parent, view1, position, id) -> {
        }, "");


        button.setOnClickListener(v -> {
            if (InputTools.isComplete(employmentTypeSpinner, employmentStatusSpinner, fromYearSpinner, untilYearSpinner, occupationEditText, positionEditText, salaryEditText) && employmentType != null) {
                KPOrderParam.setEmploymentType(employmentType);
                KPOrderParam.setEmploymentStatus(employmentStatusSpinner.getSelectedDsi());
                KPOrderParam.setWorkingFrom(fromYearSpinner.getSelectedDsi());
                KPOrderParam.setWorkingUntil(untilYearSpinner.getSelectedDsi());
                KPOrderParam.setOccupation(occupationEditText.getText().toString());
                KPOrderParam.setWorkingPosition(positionEditText.getText().toString());
//                productOrder.setWorkingPosition(positionSpinner.getSelectedDsi());
                KPOrderParam.setSalary(salaryEditText.getText().toString());
                WindowFlow.withAnim(R.anim.slide_in_from_right, R.anim.slide_out_to_right).goTo(activity(HomeActivity.class), KPForm4Fragment.TAG,
                        Bundler.wrap(KPForm4Fragment.class, KPOrderParam), R.id.fragment_frame, true);
            }


        });
    }


    static ArrayList<String> allYears = new ArrayList<String>();

    public static void initYearSpinner(int min, int max, String def) {
        allYears.clear();
        for (int i = max; i > min; i--) {
            allYears.add(String.valueOf(i));
        }
    }

    static {
        Calendar mCalendar = Calendar.getInstance();
        initYearSpinner(mCalendar.get(Calendar.YEAR) - 50, mCalendar.get(Calendar.YEAR), String.valueOf(mCalendar.get(Calendar.YEAR)));
    }
}
