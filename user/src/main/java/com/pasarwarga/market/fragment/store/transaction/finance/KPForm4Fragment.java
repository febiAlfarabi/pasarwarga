package com.pasarwarga.market.fragment.store.transaction.finance;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.WLog;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.fragment.home.BaseDrawerFragment;
import com.alfarabi.base.http.basemarket.service.BranchService;
import com.alfarabi.base.http.basemarket.service.CityService;
import com.alfarabi.base.http.basemarket.service.KreditPlusService;
import com.alfarabi.base.model.Branch;
import com.alfarabi.base.model.location.City;
import com.alfarabi.base.model.location.District;
import com.alfarabi.base.model.location.Subdistrict;
import com.alfarabi.base.model.location.ZipCode;
import com.alfarabi.base.model.transaction.Cart;
import com.alfarabi.base.model.transaction.finance.kreditplus.KPOrderParam;
import com.alfarabi.base.tools.InputTools;
import com.alfarabi.alfalibs.tools.KeyboardUtils;
import com.alfarabi.base.tools.ResponseTools;
import com.alfarabi.base.view.DataMaterialSpinner;
import com.google.gson.Gson;
import com.hendraanggrian.bundler.Bundler;
import com.hendraanggrian.bundler.annotations.BindExtra;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class KPForm4Fragment extends BaseDrawerFragment<Cart> {
    
    public static final String TAG = KPForm4Fragment.class.getName();


    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout;


    @BindView(R.id.addreses_et) EditText addressesEditText ;
    @BindView(R.id.branch_layout) LinearLayout branchLayout ;
    @BindView(R.id.branch_sp) DataMaterialSpinner<Branch> branchSpinner ;
    @BindView(R.id.city_et) AutoCompleteTextView cityAutoCompleteTextView ;
    @BindView(R.id.district_sp) DataMaterialSpinner<District> districtSpinner ;
    @BindView(R.id.subdistrict_sp) DataMaterialSpinner<Subdistrict> subdistrictSpinner ;
    @BindView(R.id.postalcode_sp) DataMaterialSpinner<ZipCode> postalCodeSpinner ;

    @BindView(R.id.next_button) Button button ;

    @BindExtra
    KPOrderParam kpOrderParam;

    private Branch branch ;
    private String[] cities ;
    private ArrayAdapter<String> cityAdapter ;
    private City city ;
    List<City> cityList ;

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public int getMenuId() {
        return 0;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    @Override
    public String initTitle() {
        title = getString(R.string.payment_method);
        toolbar.setTitle(title);
        return title;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_kpform4;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        button.setOnClickListener(v -> {
            if(InputTools.isComplete(addressesEditText, cityAutoCompleteTextView, districtSpinner, subdistrictSpinner, postalCodeSpinner)){
                activity(HomeActivity.class).showDialog(getString(R.string.confirmation), getString(R.string.kp_submit_confirmation), (dialog, which) -> {
                    dialog.dismiss();
                    kpOrderParam.setAddress(addressesEditText.getText().toString());
                    if(branchSpinner.getSelectedDsi()!=null){
                        kpOrderParam.setBranch(branchSpinner.getSelectedDsi().getName());
                    }
                    kpOrderParam.setCity(city);
                    kpOrderParam.setDistrict(districtSpinner.getSelectedDsi());
                    kpOrderParam.setSubdistrict(subdistrictSpinner.getSelectedDsi());
                    kpOrderParam.setZipCode(postalCodeSpinner.getSelectedDsi());
                    HashMap<String, String> hashMap = kpOrderParam.toHasmap();
                    HttpInstance.withProgress(activity(HomeActivity.class)).observe(HttpInstance.create(KreditPlusService.class).submit(hashMap), submit -> {
                        ResponseTools.validate(activity(), submit, true);
                        if(submit.isStatus()){
                            WindowFlow.withAnim(R.anim.slide_in_from_right, R.anim.slide_out_to_right).goTo(activity(HomeActivity.class), OrderConfirmationFragment.TAG,
                                    Bundler.wrap(OrderConfirmationFragment.class, kpOrderParam), R.id.fragment_frame, true);

                        }else{
                            activity(HomeActivity.class).showDialog(submit.getMessage(),(dialog1, which1) -> dialog1.dismiss());
                        }
                    }, throwable -> {
                        activity(HomeActivity.class).showSnackbar(throwable, v1 -> {});
                    }, false);
                }, (dialog, which) -> dialog.dismiss());

            }
        });
        requestCity(0);

    }


    public void requestCity(long provinceId){
        HttpInstance.call(HttpInstance.create(CityService.class).getCities(provinceId), cityResponse -> {
            ResponseTools.validate(getActivity(), cityResponse);
            cityList = cityResponse.getCities();
            cities = new String[cityList.size()];
            for (int i = 0; i<cityList.size(); i++) {
                cities[i] = cityList.get(i).getName();
            }
            cityAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.select_dialog_item, cities);
            cityAutoCompleteTextView.setThreshold(1);
            cityAutoCompleteTextView.setAdapter(cityAdapter);
            districtSpinner.clear();
            cityAutoCompleteTextView.setOnItemClickListener((parent, view1, position, id) -> {
                if(city!=null && city.getName().equalsIgnoreCase(cityAutoCompleteTextView.getText().toString())){
                    return;
                }
                for (int i = 0; i < cityList.size(); i++) {
                    if(cityList.get(i).getName().equalsIgnoreCase(cityAutoCompleteTextView.getText().toString())){
                        city = cityList.get(i);
                    }
                }
                if(city!=null){
                    WLog.i(TAG, "CITY :::: "+new Gson().toJson(city));
                    KeyboardUtils.hideKeyboard(getActivity());
                    requestDistrict(Long.valueOf(city.getId()));
                }
                branchSpinner.clear();
                subdistrictSpinner.clear();
                postalCodeSpinner.clear();
            });

            branchSpinner.clear();
            subdistrictSpinner.clear();
            postalCodeSpinner.clear();
        }, throwable -> {
            throwable.printStackTrace();
            activity(HomeActivity.class).showSnackbar(throwable.getMessage(), v -> {});
        }, false);
    }


    public void requestDistrict(long cityId){
        HttpInstance.call(HttpInstance.create(CityService.class).getKecamatan(cityId), districtResponse -> {
            ResponseTools.validate(getActivity(), districtResponse);
            subdistrictSpinner.clear();
            if(districtResponse.isStatus() && districtResponse.getDistricts().size()>0){
                districtSpinner.initDataAdapter(districtResponse.getDistricts(), (parent, view1, position, id) -> {
                    requestSubdistrict(Long.valueOf(districtSpinner.getSelectedDsi().getId()));
                    branchSpinner.clear();
                    postalCodeSpinner.clear();
                }, districtResponse.getDistricts().get(0));
                requestSubdistrict(Long.valueOf(districtSpinner.getSelectedDsi().getId()));
            }
            branchSpinner.clear();
            postalCodeSpinner.clear();
        }, throwable -> {
            throwable.printStackTrace();
            activity(HomeActivity.class).showSnackbar(throwable.getMessage(), v -> {});
        }, false);
    }

    public void requestSubdistrict(long districtId){
        HttpInstance.call(HttpInstance.create(CityService.class).getKelurahan(districtId), subdistrictResponse -> {
            ResponseTools.validate(getActivity(), subdistrictResponse);
            branchSpinner.clear();
            postalCodeSpinner.clear();
            if(subdistrictResponse.isStatus() && subdistrictResponse.getSubdistricts().size()>0) {
                subdistrictSpinner.initDataAdapter(subdistrictResponse.getSubdistricts(), (parent, view1, position, id) -> {
                    requestZipcode(Long.valueOf(subdistrictSpinner.getSelectedDsi().getId()));
                    requestBranch(Long.valueOf(city.getId()), Long.valueOf(districtSpinner.getSelectedDsi().getId()), Long.valueOf(subdistrictSpinner.getSelectedDsi().getId()));
                }, subdistrictResponse.getSubdistricts().get(0));
                requestZipcode(Long.valueOf(subdistrictSpinner.getSelectedDsi().getId()));
                requestBranch(Long.valueOf(city.getId()), Long.valueOf(districtSpinner.getSelectedDsi().getId()), Long.valueOf(subdistrictSpinner.getSelectedDsi().getId()));
            }
        }, throwable -> {
            throwable.printStackTrace();
            activity(HomeActivity.class).showSnackbar(throwable.getMessage(), v -> {});
        }, false);
    }

    public void requestZipcode(long subdistrictId){
        HttpInstance.call(HttpInstance.create(CityService.class).getZipCode(subdistrictId), zipCodeResponse -> {
            ResponseTools.validate(getActivity(), zipCodeResponse);
            if(zipCodeResponse.isStatus() && zipCodeResponse.getZipCodes().size()>0) {
                postalCodeSpinner.initDataAdapter(zipCodeResponse.getZipCodes(), (parent, view1, position, id) -> {

                }, zipCodeResponse.getZipCodes().get(0));
            }
        }, throwable -> {
            throwable.printStackTrace();
            activity(HomeActivity.class).showSnackbar(throwable.getMessage(), v -> {});
        }, false);
    }

    public void requestBranch(long cityId, long districtId, long subdistrictId){
        HttpInstance.call(HttpInstance.create(BranchService.class).getBranches(cityId, districtId, subdistrictId), branchResponse -> {

            Branch branch = (branchResponse.getBranches()!=null && branchResponse.getBranches().size()>0)?branchResponse.getBranches().get(0):null;
            if(branchResponse.isStatus() && branch!=null){
                branchLayout.setVisibility(View.VISIBLE);
                branchSpinner.setVisibility(DataMaterialSpinner.VISIBLE);
                branchSpinner.initDataAdapter(branchResponse.getBranches(), (parent, view1, position, id) -> {

                }, branch);
            }else{
                activity(HomeActivity.class).showSnackbar(branchResponse.getMessage(), v -> {});
            }
        }, throwable -> {
            branchLayout.setVisibility(View.GONE);
            branchSpinner.setVisibility(DataMaterialSpinner.GONE);
            throwable.printStackTrace();
            activity(HomeActivity.class).showSnackbar(throwable.getMessage(), v -> {});
        }, false);
    }
}
