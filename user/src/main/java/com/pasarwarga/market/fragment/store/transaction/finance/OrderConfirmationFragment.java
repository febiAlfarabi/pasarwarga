package com.pasarwarga.market.fragment.store.transaction.finance;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.fragment.home.BaseDrawerFragment;
import com.alfarabi.base.model.Product;
import com.alfarabi.base.model.transaction.finance.kreditplus.KPCheckoutParam;
import com.alfarabi.base.model.transaction.finance.kreditplus.KPOrderParam;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.hendraanggrian.bundler.annotations.BindExtra;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.fragment.home.HomeFragment;
import com.pasarwarga.market.fragment.store.transaction.purchase.PurchaseParentTabFragment;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrderConfirmationFragment extends BaseDrawerFragment<Product> {

    public static final String TAG = OrderConfirmationFragment.class.getName();

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout;

    @BindView(R.id.check_status_button) Button checkStatusButton ;
    @BindView(R.id.home_button) Button homeButton ;
    @BindView(R.id.detail_button) LinearLayout detailButton ;
    @BindView(R.id.expandable_layout) ExpandableRelativeLayout expandableRelativeLayout;


    @BindView(R.id.total_price_tv) TextView totalPriceTextView ;
    @BindView(R.id.product_price_tv) TextView productPriceTextView ;
    @BindView(R.id.shipping_cost_tv) TextView shippingCostTextView ;
    @BindView(R.id.insurance_tv) TextView insuranceTextView ;
    @BindView(R.id.admin_fee_tv) TextView adminFeeTextView ;
    @BindView(R.id.voucher_tv) TextView voucherTextView ;
    @BindView(R.id.tenor_tv) TextView tenorTextView ;


    @BindExtra
    KPOrderParam KPOrderParam;

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public int getMenuId() {
        return 0;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public String initTitle() {
        title = getString(R.string.payment_method);
        toolbar.setTitle(title);
        return title;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_order_confirmation;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        expandableRelativeLayout.collapse();
        homeButton.setOnClickListener(v -> {
//            ((MainActivity)getActivity()).expandedAppBar(true);
//            WindowFlow.restart(getActivity(), MainActivity.class);
//            WindowFlow.goFirst(getActivity(), MainFragment.TAG, R.id.fragment_frame);
            WindowFlow.backstack(getActivity(), HomeFragment.TAG, R.id.fragment_frame);
        });
        detailButton.setOnClickListener(v -> {
            if(expandableRelativeLayout.isExpanded()){
                expandableRelativeLayout.collapse();
            }else{
                expandableRelativeLayout.expand();
            }
        });
        checkStatusButton.setOnClickListener(v -> {
            WindowFlow.backstack(getActivity(), HomeFragment.TAG, R.id.fragment_frame);
//            WindowFlow.goTo(activity(HomeActivity.class), CategoryListFragment.TAG, R.id.fragment_frame, true);
            Bundle bundle = new Bundle();
            bundle.putInt("tab_number", 0);
            WindowFlow.force().goTo(activity(HomeActivity.class), PurchaseParentTabFragment.TAG, bundle, R.id.fragment_frame, true, false);
        });
        render();
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    public void render(){
        KPCheckoutParam KPCheckoutParam = KPOrderParam.getKPCheckoutParam();

        totalPriceTextView.setText(getString(R.string.curr_symb)+ NumberFormat.getInstance().format(KPCheckoutParam.getTotalPrice()));
        productPriceTextView.setText(getString(R.string.curr_symb)+NumberFormat.getInstance().format(KPCheckoutParam.getProductPrice()));
        shippingCostTextView.setText(getString(R.string.curr_symb)+NumberFormat.getInstance().format(KPCheckoutParam.getShippingCost()));
        insuranceTextView.setText(getString(R.string.curr_symb)+NumberFormat.getInstance().format(KPCheckoutParam.getInsurance()));
        adminFeeTextView.setText(getString(R.string.curr_symb)+NumberFormat.getInstance().format(KPCheckoutParam.getAdministrationFee()));
        voucherTextView.setText(getString(R.string.curr_symb)+NumberFormat.getInstance().format(KPCheckoutParam.getVoucher()));
        String installment = String.valueOf(KPOrderParam.getTenor().getTenor()) + "x - " + getString(R.string.rp)
                + " " + DecimalFormat.getInstance().format(Double.valueOf(KPOrderParam.getTenor().getAmount())) + " / " + getString(R.string.month);
        tenorTextView.setText(getString(R.string.installment)+" "+installment);

    }
}
