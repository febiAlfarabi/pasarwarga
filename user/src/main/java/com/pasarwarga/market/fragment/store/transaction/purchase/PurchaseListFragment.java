package com.pasarwarga.market.fragment.store.transaction.purchase;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.alfarabi.alfalibs.adapters.recyclerview.VerticalRecyclerAdapter;
import com.alfarabi.alfalibs.fragments.interfaze.SimpleFragmentCallback;
import com.alfarabi.alfalibs.helper.SwipeCallback;
import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.alfarabi.alfalibs.views.AlfaSwipeRefreshLayout;
import com.alfarabi.base.fragment.home.BaseDrawerFragment;
import com.alfarabi.base.http.basemarket.service.PurchaseService;
import com.alfarabi.base.model.transaction.purchase.Purchase;
import com.alfarabi.base.tools.ResponseTools;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.fragment.home.HomeFragment;
import com.pasarwarga.market.holder.recyclerview.PurchaseViewHolder;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by Alfarabi on 6/19/17.
 */

public class PurchaseListFragment extends BaseDrawerFragment implements SimpleFragmentCallback {

    public static final String TAG = PurchaseListFragment.class.getName();

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout;
    @BindView(R.id.recyclerview) AlfaRecyclerView recyclerView ;
    @BindView(R.id.swiperefreshlayout) AlfaSwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.loader) MKLoader loader;

    private VerticalRecyclerAdapter<PurchaseListFragment, PurchaseViewHolder, Purchase> verticalRecyclerAdapter ;

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        toolbar.inflateMenu(com.alfarabi.base.R.menu.empty_menu);
        toolbar.setOnMenuItemClickListener(this);
        List<Purchase> purchases = new ArrayList<>();
        verticalRecyclerAdapter = new VerticalRecyclerAdapter<>(this, PurchaseViewHolder.class, purchases);
        verticalRecyclerAdapter.initRecyclerView(recyclerView);
        swipeRefreshLayout.setOnSwipeListener(new SwipeCallback() {
            @Override
            public void onSwipeFromTop() {
                swipeRefreshLayout.finishRefresh();
            }

            @Override
            public void onSwipeFromBottom() {

            }
        });
        swipeRefreshLayout.setRefreshable(false);
        recyclerView.getEmptyView().findViewById(R.id.start_shop_button).setOnClickListener(v -> {
            WindowFlow.backstack(activity(HomeActivity.class), HomeFragment.TAG, R.id.fragment_frame);
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        loader.setVisibility(MKLoader.VISIBLE);
        HttpInstance.call(HttpInstance.create(PurchaseService.class).getPurchases(), purchasesResponse -> {
            ResponseTools.validate(activity(), purchasesResponse);
            if(purchasesResponse.isStatus()){
                verticalRecyclerAdapter.setObjects(purchasesResponse.getPurchases());
            }
            verticalRecyclerAdapter.notifyDataSetChanged();
            loader.setVisibility(MKLoader.GONE);
        }, throwable -> {
            loader.setVisibility(MKLoader.GONE);
            activity(HomeActivity.class).showSnackbar(throwable, v -> {});
        });

    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_purchase_list;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public Object getObject() {
        return null;
    }

    @Override
    public String initTitle() {
        title = getString(R.string.all_purchases);
        toolbar.setTitle(title);
        return title;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    @Override
    public int getMenuId() {
        return R.id.purchase_menu_item;
    }

    @Override
    public boolean keepSideMenu() {
        return true;
    }
}
