//package com.pasarwarga.market.fragment.store.transaction.purchase;
//
//import android.net.Uri;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v7.widget.Toolbar;
//import android.view.View;
//
//import com.alfarabi.alfalibs.http.HttpInstance;
//import com.alfarabi.alfalibs.tools.WindowFlow;
//import com.alfarabi.base.fragment.BaseUserFragment;
//import com.alfarabi.base.http.basemarket.service.OrderService;
//import com.alfarabi.base.http.basemarket.service.PurchaseService;
//import com.alfarabi.base.model.transaction.purchase.Purchase;
//import com.alfarabi.base.tools.CommonUtil;
//import com.github.barteksc.pdfviewer.PDFView;
//import com.hendraanggrian.bundler.annotations.BindExtra;
//import com.pasarwarga.market.R;
//import com.pasarwarga.market.activity.home.HomeActivity;
//
//import java.io.File;
//
//import butterknife.BindView;
//
///**
// * Created by USER on 10/10/2017.
// */
//
//public class PurchasePDFFragment extends BaseUserFragment {
//
//    public static final String TAG = PurchasePDFFragment.class.getName();
//
//    @BindView(R.id.pdf_viewer) PDFView pdfView;
//    @BindView(R.id.toolbar) Toolbar toolbar;
//
//    @BindExtra Purchase purchase;
//
//    @Override
//    public String initTitle() {
//        return null;
//    }
//
//    @Override
//    public int contentXmlLayout() {
//        return R.layout.fragment_pdf_purchase;
//    }
//
//    @Override
//    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//        toolbar.setTitle(getString(R.string.invoice));
//        toolbar.setNavigationOnClickListener(v -> WindowFlow.backstack(activity(HomeActivity.class)));
//        HttpInstance.withProgress(activity()).observe(HttpInstance.create(OrderService.class).downloadIncoice(purchase.getDealCodeNumber()), responseBody -> {
//            if (responseBody != null) {
//                File file = CommonUtil.writeResponseBodyToDisk(activity(), responseBody);
//
//                pdfView.fromUri(Uri.fromFile(file))
//                        .enableSwipe(true)
//                        .swipeHorizontal(false)
//                        .enableDoubletap(true)
//                        .defaultPage(0)
//                        .enableAnnotationRendering(false)
//                        .password(null)
//                        .scrollHandle(null)
//                        .enableAntialiasing(true)
//                        .spacing(0)
//                        .load();
//            } else {
//                activity(HomeActivity.class).showDialog(getString(R.string.server_error), (dialog, which) -> {
//                    WindowFlow.backstack(activity(HomeActivity.class));
//                    dialog.dismiss();
//                });
//            }
//        }, throwable -> {
//            activity(HomeActivity.class).showSnackbar(throwable, v -> {});
//        });
//    }
//}
