package com.pasarwarga.market.fragment.store.transaction.purchase;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.alfarabi.alfalibs.adapters.viewpager.BaseTabPagerAdapter;
import com.alfarabi.base.fragment.home.BaseDrawerTabFragment;
import com.alfarabi.base.view.TabLayout;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.adapter.PurchaseParentTabPagerAdapter;

import butterknife.BindView;

/**
 * Created by USER on 9/29/2017.
 */

public class PurchaseParentTabFragment extends BaseDrawerTabFragment {

    public static final String TAG = PurchaseParentTabFragment.class.getName();

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.appbar_layout) AppBarLayout appBarLayout;

    @BindView(R.id.tab_layout) TabLayout tabLayout;
    @BindView(R.id.viewpager) ViewPager viewPager;

    int tabNumber = 0;

    private PurchaseParentTabPagerAdapter purchaseParentTabPagerAdapter;

    @Override
    public BaseTabPagerAdapter<?> baseTabPagerAdapter() {
        return purchaseParentTabPagerAdapter;
    }

    @Override
    public TabLayout getTabLayout() {
        return tabLayout;
    }

    @Override
    public ViewPager getViewPager() {
        return viewPager;
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public AppBarLayout getAppbarLayout() {
        return appBarLayout;
    }

    @Override
    public boolean keepSideMenu() {
        return true;
    }

    @Override
    public int getMenuId() {
        if(tabNumber==0){
            return R.id.sub_purchase_menu_item_1;
        }else if(tabNumber==1){
            return R.id.sub_purchase_menu_item_2;
        }else if(tabNumber==2){
            return R.id.sub_purchase_menu_item_3;
        }else if(tabNumber==3){
            return R.id.sub_purchase_menu_item_4;
        }
        return 0;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    @Override
    public String initTitle() {
        title = getString(R.string.purchase);
        toolbar.setTitle(title);
        return title;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_purchase_parent_tab;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        purchaseParentTabPagerAdapter = new PurchaseParentTabPagerAdapter<>(getChildFragmentManager(), activity(HomeActivity.class));
        if (getArguments() != null && getArguments().containsKey("tab_number")) {
            tabNumber = getArguments().getInt("tab_number");
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initTabLayout(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switchMenu(false);
                tabNumber = position;
                new Handler().postDelayed(() -> {
                    purchaseParentTabPagerAdapter.initIcon(tabLayout, position);
                    if (purchaseParentTabPagerAdapter.fragmentClasses()[position] instanceof TabFragmentCallback) {
                        ((TabFragmentCallback) purchaseParentTabPagerAdapter.fragmentClasses()[position]).onTabShowed();
                        switchMenu(true);
                    }
                }, 50);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        purchaseParentTabPagerAdapter.initIcon(tabLayout, tabNumber);
        new Handler().postDelayed(() -> {
            tabLayout.getTabAt(tabNumber).select();
            if (purchaseParentTabPagerAdapter.fragmentClasses()[tabNumber] instanceof TabFragmentCallback) {
                ((TabFragmentCallback) purchaseParentTabPagerAdapter.fragmentClasses()[tabNumber]).onTabShowed();
            }
        }, 50);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public boolean onBackPressed() {
        if (tabLayout.getSelectedTabPosition() != 0) {
            new Handler().postDelayed(() -> tabLayout.getTabAt(0).select(), 100);
            return false;
        }
        return super.onBackPressed();
    }
}
