package com.pasarwarga.market.fragment.store.transaction.purchase;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.alfarabi.alfalibs.adapters.recyclerview.SimpleRecyclerAdapter;
import com.alfarabi.alfalibs.helper.SwipeCallback;
import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.alfarabi.alfalibs.views.AlfaSwipeRefreshLayout;
import com.alfarabi.base.fragment.BaseUserFragment;
import com.alfarabi.base.http.basemarket.service.PurchaseService;
import com.alfarabi.base.model.transaction.purchase.Purchase;
import com.alfarabi.base.tools.Initial;
import com.alfarabi.base.tools.ResponseTools;
import com.hendraanggrian.bundler.Bundler;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.holder.recyclerview.PurchaseOrderViewHolder;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by USER on 9/28/2017.
 */

public class PurchasePaymentListFragment extends BaseUserFragment implements TabFragmentCallback{

    public static final String TAG = PurchasePaymentListFragment.class.getName();

    @BindView(R.id.swiperefreshlayout) AlfaSwipeRefreshLayout alfaSwipeRefreshLayout;
    @BindView(R.id.recyclerview) AlfaRecyclerView<Purchase> recyclerView;
    @BindView(R.id.loader) MKLoader loader;

    private List<Purchase> purchases;
    private SimpleRecyclerAdapter<Purchase, PurchasePaymentListFragment, PurchaseOrderViewHolder> simpleRecyclerAdapter;

    @Override
    public String initTitle() {
        return null;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_purchase_payment_list;
    }

    @Override
    public void onTabShowed() {
        if (alfaSwipeRefreshLayout.getPage() == 1 && simpleRecyclerAdapter.getObjects().size() > 0) {
            return;
        }
        loader.setVisibility(MKLoader.VISIBLE);
        basicRequest(alfaSwipeRefreshLayout.startPage());
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        purchases = new ArrayList<>();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        simpleRecyclerAdapter = new SimpleRecyclerAdapter<Purchase, PurchasePaymentListFragment, PurchaseOrderViewHolder>(this, PurchaseOrderViewHolder.class, purchases);
        simpleRecyclerAdapter.initRecyclerView(recyclerView, new LinearLayoutManager(activity()));
        alfaSwipeRefreshLayout.setOnSwipeListener(new SwipeCallback() {
            @Override
            public void onSwipeFromTop() {
                basicRequest(alfaSwipeRefreshLayout.startPage());
            }

            @Override
            public void onSwipeFromBottom() {
                basicRequest(alfaSwipeRefreshLayout.increasePage());
            }
        });
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void basicRequest(int page) {
        HttpInstance.call(HttpInstance.create(PurchaseService.class).getPurchases(page, Initial.PENDING), purchasesResponse -> {
            ResponseTools.validate(activity(), purchasesResponse);
            loader.setVisibility(View.GONE);
            alfaSwipeRefreshLayout.finishRefresh();
            alfaSwipeRefreshLayout.finishRefreshLoadMore();
            if (purchasesResponse.isStatus()) {
                if (page == 1) {
                    purchases.clear();
                }
                purchases.addAll(purchasesResponse.getPurchases());
                simpleRecyclerAdapter.setObjects(purchases);
            } else {
                activity(HomeActivity.class).showSnackbar(purchasesResponse.getMessage(), v -> {
                });
            }
        }, throwable -> {
            activity(HomeActivity.class).showSnackbar(throwable, v -> {
            });
            loader.setVisibility(View.GONE);
            alfaSwipeRefreshLayout.finishRefresh();
            alfaSwipeRefreshLayout.finishRefreshLoadMore();
        }, false);
    }

    public View.OnClickListener onClickListener = v -> {
        final Purchase purchase = (Purchase) v.getTag();
        HttpInstance.withProgress(activity()).observe(HttpInstance.create(PurchaseService.class).getPurchaseDetail(purchase.getDealCodeNumber()), purchaseDetailResponse -> {
            ResponseTools.validate(activity(HomeActivity.class), purchaseDetailResponse);
            if (purchaseDetailResponse.isStatus()) {
                WindowFlow.goTo(activity(HomeActivity.class), PurchaseDetailFragment.TAG, Bundler.wrap(PurchaseDetailFragment.class, purchaseDetailResponse.getPurchase()), R.id.fragment_frame, true);
            } else {
                activity(HomeActivity.class).showSnackbar(purchaseDetailResponse.getMessage(), v1 -> {});
            }
        }, throwable -> {
            activity(HomeActivity.class).showSnackbar(throwable, v1 -> {
            });
        }, false);
    };
}
