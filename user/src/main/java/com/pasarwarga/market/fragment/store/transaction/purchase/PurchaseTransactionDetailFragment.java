package com.pasarwarga.market.fragment.store.transaction.purchase;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.SimpleRecyclerAdapter;
import com.alfarabi.alfalibs.helper.SwipeCallback;
import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.alfarabi.alfalibs.views.AlfaSwipeRefreshLayout;
import com.alfarabi.base.BaseApplication;
import com.alfarabi.base.fragment.BaseUserFragment;
import com.alfarabi.base.http.basemarket.service.TrackingService;
import com.alfarabi.base.model.transaction.HistoryTracking;
import com.alfarabi.base.model.transaction.purchase.Purchase;
import com.alfarabi.base.tools.Initial;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.hendraanggrian.bundler.Bundler;
import com.hendraanggrian.bundler.annotations.BindExtra;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.fragment.WebViewFragment;
import com.pasarwarga.market.holder.recyclerview.PurchaseTrackingViewHolder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

/**
 * Created by USER on 10/5/2017.
 */

public class PurchaseTransactionDetailFragment extends BaseUserFragment {

    public static final String TAG = PurchaseTransactionDetailFragment.class.getName();

    @BindView(R.id.toolbar) Toolbar toolbar;

    @BindView(R.id.transaction_date_tv) TextView transactionDateTextView;
    @BindView(R.id.sellername_tv) TextView sellernameTextView;
    @BindView(R.id.invoice_code_tv) TextView invoiceCodeTextView;
    @BindView(R.id.product_iv) ImageView productImageView;
    @BindView(R.id.product_name_tv) TextView productNameTextView;
    @BindView(R.id.product_price_tv) TextView productPriceTextView;
    @BindView(R.id.notes_tv) TextView notesTextView;
    @BindView(R.id.quantity_tv) TextView quantityTextView;
    @BindView(R.id.shipping_cost_tv) TextView shippingCostTextView;
    @BindView(R.id.insurance_cost_tv) TextView insuranceCostTextView;
    @BindView(R.id.total_price_tv) TextView totalPriceTextView;
    @BindView(R.id.destination_address_tv) TextView destinationAddressTextView;
    @BindView(R.id.shipping_media_tv) TextView shippingMediaTextView;

    @BindView(R.id.swiperefreshlayout) AlfaSwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recyclerview) AlfaRecyclerView<HistoryTracking> recyclerView;

    @Nullable @BindExtra Purchase purchase;

    private SimpleRecyclerAdapter<HistoryTracking, PurchaseTransactionDetailFragment, PurchaseTrackingViewHolder> simpleRecyclerAdapter;
    private List<HistoryTracking> historyTrackings = new ArrayList<>();

    @Override
    public String initTitle() {
        return null;
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_purchase_transaction_detail;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        simpleRecyclerAdapter = new SimpleRecyclerAdapter<>(this, PurchaseTrackingViewHolder.class, historyTrackings);
        simpleRecyclerAdapter.initRecyclerView(recyclerView, new LinearLayoutManager(activity()));

        toolbar.setTitle(R.string.purchase_detail);
        toolbar.setNavigationOnClickListener(v -> WindowFlow.backstack(activity(HomeActivity.class)));
        transactionDateTextView.setText(purchase.getCreated());
        sellernameTextView.setText(purchase.getSellerInfo().getShopName());
        if(purchase.getNote()!=null && !purchase.getNote().equals("")) {
            notesTextView.setText(purchase.getNote());
        }
        invoiceCodeTextView.setText("#" + getString(R.string.invoice) + " " + purchase.getDealCodeNumber());
        GlideApp.with(this).load(purchase.getProduct().getImage()).placeholder(R.drawable.ic_product_default).error(R.drawable.ic_product_default)
                .skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL).into(productImageView);
        productNameTextView.setText(purchase.getProduct().getName());
        productPriceTextView.setText(getString(R.string.curr_symb) + "" + java.text.DecimalFormat.getInstance().format(purchase.getProduct().getBasePrice()));
        quantityTextView.setText(getString(R.string.quantity) + " : " + String.valueOf(purchase.getQuantity()));
        shippingCostTextView.setText(getString(R.string.curr_symb) + "" + java.text.DecimalFormat.getInstance().format(purchase.getShippingCost()));
        if (purchase.isInsurance()) {
            insuranceCostTextView.setText(getString(R.string.curr_symb) + "" + java.text.DecimalFormat.getInstance().format(purchase.getInsuranceValue()));
        }
        totalPriceTextView.setText(getString(R.string.curr_symb) + "" + java.text.DecimalFormat.getInstance().format(purchase.getTotal()));
        shippingMediaTextView.setText(purchase.getShippCode());
        if(purchase.getShippingAddress()!=null){
            destinationAddressTextView.setText(purchase.getShippingAddress().getAddress1()
                    + "\n" + purchase.getShippingAddress().getAddress2()
                    + "\n" + purchase.getShippingAddress().getCountry()
                    + "\n" + purchase.getShippingAddress().getCity()
                    + "\n" + purchase.getShippingAddress().getPostalCode()
                    + "\n" + purchase.getShippingAddress().getPhone());
        }
        invoiceCodeTextView.setOnClickListener(v -> {
            WindowFlow.goTo(activity(HomeActivity.class), WebViewFragment.TAG,
                    Bundler.wrap(WebViewFragment.class, BaseApplication.baseDomain+ Initial.INVOICE_ENDPOINT+String.valueOf(purchase.getDealCodeNumber()), purchase.getDealCodeNumber()), R.id.fragment_frame, true, false);
//            WindowFlow.goTo(activity(HomeActivity.class), PurchasePDFFragment.TAG, Bundler.wrap(PurchasePDFFragment.class, purchase), R.id.fragment_frame, true, false);
        });

        swipeRefreshLayout.setOnSwipeListener(new SwipeCallback() {
            @Override
            public void onSwipeFromTop() {
                swipeRefreshLayout.finishRefresh();
            }

            @Override
            public void onSwipeFromBottom() {
                swipeRefreshLayout.finishRefreshLoadMore();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("deal_code_number", purchase.getDealCodeNumber());
        hashMap.put("tracking_id", purchase.getTrackingId());
        hashMap.put("shipp_code", purchase.getShippCode());

        HttpInstance.call(HttpInstance.create(TrackingService.class).tracking(hashMap), trackingListResponse -> {
            if(trackingListResponse.isStatus()){
                simpleRecyclerAdapter.setObjects(trackingListResponse.getTraceTrakings().getHistoryTrackings());
            }else{
                activity(HomeActivity.class).showSnackbar(trackingListResponse.getMessage(), v -> {});
            }
        }, throwable -> {
            activity(HomeActivity.class).showSnackbar(throwable, v -> {});
        }, false);
    }
}
