package com.pasarwarga.market.fragment.store.transaction.purchase;

/**
 * Created by Alfarabi on 9/22/17.
 */

interface TabFragmentCallback {

    public void onTabShowed();
}
