package com.pasarwarga.market.fragment.transaction.status;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.alfarabi.alfalibs.adapters.recyclerview.SimpleRecyclerAdapter;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.alfarabi.alfalibs.views.AlfaSwipeRefreshLayout;
import com.alfarabi.base.model.transaction.CartItem;
import com.pasarwarga.market.R;
import com.pasarwarga.market.holder.recyclerview.OrderViewHolder;
import com.alfarabi.base.fragment.BaseUserFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by Alfarabi on 6/19/17.
 */

public class OnProgressOrderListFragment extends BaseUserFragment<List<CartItem>> {

    public static final String TAG = NewOrderListFragment.class.getName();

    @BindView(R.id.recyclerview) AlfaRecyclerView recyclerView ;
    @BindView(R.id.swiperefreshlayout) AlfaSwipeRefreshLayout swipeRefreshLayout;
    SimpleRecyclerAdapter<CartItem, NewOrderListFragment, OrderViewHolder> simpleRecyclerAdapter ;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        simpleRecyclerAdapter  = new SimpleRecyclerAdapter(this, OrderViewHolder.class, new ArrayList<CartItem>());

    }

    @Override
    public void onResume() {
        super.onResume();
//        simpleRecyclerAdapter.setObjects(cart.getCartItems());
        simpleRecyclerAdapter.initRecyclerView(recyclerView, new LinearLayoutManager(getContext()));
    }

    @Override
    public int contentXmlLayout() {
        return R.layout.fragment_status_on_progress_order_list;
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public List<CartItem> getObject() {
        return simpleRecyclerAdapter.getObjects();
    }

    @Override
    public String initTitle() {
        title = getString(R.string.order);
        return title;
    }
}