package com.pasarwarga.market.holder.recyclerview;

import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.SimpleViewHolder;
import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.Gilimanuk;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.http.basemarket.service.AddressService;
import com.alfarabi.base.model.location.Address;
import com.alfarabi.base.tools.Initial;
import com.alfarabi.base.tools.ResponseTools;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.fragment.store.AddressDetailFragment;
import com.pasarwarga.market.fragment.store.AddressListFragment;

import org.parceler.Parcels;

import java.util.HashMap;

import butterknife.BindView;

/**
 * Created by Alfarabi on 6/19/17.
 */

public class AddressViewHolder extends SimpleViewHolder<AddressListFragment, Address, String> {

    @BindView(R.id.row_1_tv) TextView row1TextView;
    @BindView(R.id.row_2_tv) TextView row2TextView;
    @BindView(R.id.row_3_tv) TextView row3TextView;
    @BindView(R.id.postalcode_tv) TextView postalCodeTextView;
    @BindView(R.id.phonenumber_tv) TextView phoneNumberTextView;
    @BindView(R.id.edit_btn) LinearLayout editButton;
    @BindView(R.id.delete_btn) LinearLayout deleteButton;


    public AddressViewHolder(AddressListFragment fragment, ViewGroup viewGroup) {
        super(fragment, R.layout.viewholder_address, viewGroup);
    }

    @Override
    public void showData(Address object) {
        super.showData(object);
        row1TextView.setText(object.getName());
        row2TextView.setText(object.getAddress1());
        row3TextView.setText(object.getAddress2());

        postalCodeTextView.setText(getString(R.string.postal_code)+" : "+object.getPostalCode());
        phoneNumberTextView.setText(getString(R.string.phone)+" : "+object.getPhone());

        editButton.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putParcelable(Initial.ADDRESS, Parcels.wrap(object));
            Gilimanuk.withAnim(getFragment().getAppearAnims()).goTo(getFragment().activity(HomeActivity.class), AddressDetailFragment.TAG, bundle, R.id.fragment_frame, true, false);
        });
        deleteButton.setOnClickListener(v -> {
            getFragment().activity(HomeActivity.class).showDialog(getString(R.string.delete_address_confirmation), (dialog, which) -> {
                HashMap<String, String> hashMap = new HashMap<String, String>();
                hashMap.put("shipping_id", object.getId());
                HttpInstance.call(HttpInstance.create(AddressService.class).delete(hashMap), delete -> {
                    ResponseTools.validate(getFragment().activity(), delete);
                    if(delete.isStatus()){
                        getFragment().getSimpleRecyclerAdapter().getObjects().remove(getAdapterPosition());
                        getFragment().getSimpleRecyclerAdapter().notifyItemRemoved(getAdapterPosition());
                    }else{
                        getFragment().activity(HomeActivity.class).showDialog(delete.getMessage(), (dialog1, which1) -> {});
                    }
                }, throwable -> {
                    getFragment().activity(HomeActivity.class).showDialog(throwable, (dialog1, which1) -> {});
                });
                dialog.dismiss();
            }, (dialog, which) -> {
                dialog.dismiss();
            });
        });


    }

    @Override
    public void find(String s) {

    }
}
