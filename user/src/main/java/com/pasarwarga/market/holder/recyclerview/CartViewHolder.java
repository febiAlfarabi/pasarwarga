package com.pasarwarga.market.holder.recyclerview;

import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.SimpleViewHolder;
import com.alfarabi.alfalibs.tools.Gilimanuk;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.model.Product;
import com.alfarabi.base.model.transaction.CartItem;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.fragment.store.CartFragment;
import com.pasarwarga.market.fragment.store.OrderListFragment;
import com.alfarabi.base.model.transaction.Cart;
import com.pasarwarga.market.fragment.store.CartListFragment;
import com.hendraanggrian.bundler.Bundler;

import java.text.NumberFormat;
import java.util.List;

import butterknife.BindView;

/**
 * Created by Alfarabi on 6/19/17.
 */

public class CartViewHolder extends SimpleViewHolder<CartListFragment, CartItem, String> {

//    @BindView(R.id.info_frame_layout) FrameLayout infoFrameLayout;
//    @BindView(R.id.reveal_layout) SwipeRevealLayout swipeRevealLayout;
//    @BindView(R.id.delete_btn) LinearLayout linearLayoutDelete;
//    @BindView(R.id.delete_tv) TextView textViewDelete;
//    @BindView(R.id.row_1_tv) TextView textViewCartName;
//    @BindView(R.id.row_2_tv) TextView textViewCategory;

    @BindView(R.id.item_iv) ImageView itemImageView;
    @BindView(R.id.item_original_price_tv) TextView itemOriginalPriceTextView;
    @BindView(R.id.item_price_tv) TextView itemPriceTextView;
    @BindView(R.id.item_name_tv) TextView itemNameTextView;
    @BindView(R.id.discount_tv) TextView itemDiscountTextView;
    @BindView(R.id.quantity_tv) TextView quantityTextView ;

    public CartViewHolder(CartListFragment fragment, ViewGroup viewGroup) {
        super(fragment, R.layout.viewholder_cart, viewGroup);
//        infoFrameLayout.setOnClickListener(v -> {
//            List<Cart> carts = fragment.getObject();
//            Cart cart = carts.get(getAdapterPosition());
//            Bundle bundle = Bundler.wrap(OrderListFragment.class, cart);
//            WindowFlow.goTo(fragment.activity(HomeActivity.class), OrderListFragment.TAG, bundle, R.id.fragment_frame, true);
//        });
    }

    @Override
    public void showData(CartItem cartItem) {
        super.showData(cartItem);
        GlideApp.with(getFragment()).load(cartItem.getImage()).placeholder(R.drawable.ic_product_default).error(R.drawable.ic_product_default)
                .skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL).into(itemImageView);
        itemNameTextView.setText(cartItem.getProductName());
        quantityTextView.setText(getString(R.string.quantity)+" : "+String.valueOf(1));
        itemPriceTextView.setText(getString(R.string.curr_symb) + NumberFormat.getInstance().format(cartItem.getFinalPrice()));
        if (cartItem.getDiscount() == 0) {
            itemDiscountTextView.setVisibility(View.GONE);
            itemOriginalPriceTextView.setVisibility(View.GONE);
            itemPriceTextView.setText(getString(R.string.curr_symb) + NumberFormat.getInstance().format(cartItem.getFinalPrice()));
        } else {
            itemDiscountTextView.setVisibility(View.VISIBLE);
            itemDiscountTextView.setText(String.valueOf(cartItem.getDiscount()+ getString(R.string.percent_symb))+" "+getString(R.string.off));
            itemOriginalPriceTextView.setPaintFlags(itemOriginalPriceTextView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            itemOriginalPriceTextView.setText(getString(R.string.curr_symb) + NumberFormat.getInstance().format(cartItem.getProductPrice()));
            itemPriceTextView.setText(getString(R.string.curr_symb) + NumberFormat.getInstance().format(cartItem.getFinalPrice()));
        }
        itemView.setOnClickListener(v -> {
            Gilimanuk.withAnim(getFragment().getAppearAnims())
                    .goTo(getFragment().activity(HomeActivity.class), CartFragment.TAG, Bundler.wrap(CartFragment.class, new Product()), R.id.fragment_frame, true, false);
        });
    }

    @Override
    public void find(String s) {

    }
}
