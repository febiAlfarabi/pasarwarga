package com.pasarwarga.market.holder.recyclerview;

import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.SimpleViewHolder;
import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.Gilimanuk;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.fragment.SimpleBaseFragmentInitiator;
import com.alfarabi.base.http.basemarket.service.FavoriteService;
import com.alfarabi.base.model.Product;
import com.alfarabi.base.realm.RealmDao;
import com.alfarabi.base.tools.ResponseTools;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.fragment.home.HomeFragment;
import com.pasarwarga.market.fragment.store.product.ProductDetailFragment;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.hendraanggrian.bundler.Bundler;
import com.pasarwarga.market.tools.SystemApp;
import com.tuyenmonkey.mkloader.MKLoader;

import net.soroushjavdan.customfontwidgets.CTextView;

import org.apache.commons.lang.StringUtils;

import java.text.NumberFormat;
import java.util.HashMap;

import butterknife.BindView;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Alfarabi on 6/19/17.
 */

public class HomeProductViewHolder extends SimpleViewHolder<HomeFragment, Product, String> {

    public static final String TAG = HomeProductViewHolder.class.getName();

    @BindView(R.id.viewholder_layout) LinearLayout viewHolderLayout;
    @BindView(R.id.imageview) ImageView imageView;
    @BindView(R.id.name_tv) TextView nameTextView;
    @BindView(R.id.discount_tv) CTextView discountTextView;
    @BindView(R.id.original_price_tv) TextView originalPriceTextView;
    @BindView(R.id.price_tv) TextView priceTextView;
    @BindView(R.id.whislist_iv) ImageView whislistImageView ;
    @BindView(R.id.loader) MKLoader loader;
    @BindView(R.id.seller_name_tv) TextView sellerNameTextView;
    @BindView(R.id.seller_location_tv) TextView sellerLocationTextView;

    RatingBar ratingBar;

    public HomeProductViewHolder(HomeFragment fragment, ViewGroup viewGroup) {
        super(fragment, R.layout.viewholder_home_product, viewGroup);
    }

    @Override
    public void showData(final Product product) {
        super.showData(product);
        nameTextView.setText(getObject().getName());
        if(getObject().isFavourite()){
            whislistImageView.setImageDrawable(getFragment().getResources().getDrawable(R.drawable.ic_wishlist_2));
        }else{
            whislistImageView.setImageDrawable(getFragment().getResources().getDrawable(R.drawable.ic_wishlist_1));
        }
        whislistImageView.setOnClickListener(v -> {
            if( SystemApp.checkLogin(getFragment().activity(), true)){
                loader.setVisibility(View.VISIBLE);
                HashMap<String, String> hashMap = new HashMap<String, String >();
                hashMap.put("product_id", String.valueOf(getObject().getId()));
                HttpInstance.call(HttpInstance.create(FavoriteService.class).changeFavorite(hashMap), changeFavoriteResponse ->{
                    ResponseTools.validate(getFragment().activity(), changeFavoriteResponse);
                    loader.setVisibility(View.INVISIBLE);
                    RealmDao.instance().insertOrUpdate(changeFavoriteResponse.getProduct());
                    setObject(changeFavoriteResponse.getProduct());
                    if(getObject().isFavourite()){
                        whislistImageView.setImageDrawable(getFragment().getResources().getDrawable(R.drawable.ic_wishlist_2));

                    }else{
                        whislistImageView.setImageDrawable(getFragment().getResources().getDrawable(R.drawable.ic_wishlist_1));

                    }
                }, throwable -> {
                    getFragment().activity(HomeActivity.class).showSnackbar(throwable, v1 -> {});
                    loader.setVisibility(View.INVISIBLE);
                }, false);
            }
        });

        if (getObject().getDiscount() == 0) {
            originalPriceTextView.setPaintFlags(Paint.LINEAR_TEXT_FLAG);
            originalPriceTextView.setText(getString(R.string.curr_symb) + NumberFormat.getInstance().format(getObject().getBasePrice()));
            priceTextView.setText(R.string.z);
//            circleViewDiscount.setVisibility(View.GONE);
            discountTextView.setVisibility(View.GONE);
        } else {
            discountTextView.setVisibility(View.VISIBLE);
            originalPriceTextView.setPaintFlags(originalPriceTextView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            originalPriceTextView.setText(getString(R.string.curr_symb) + NumberFormat.getInstance().format(getObject().getBasePrice()));
            priceTextView.setText(getString(R.string.curr_symb) + NumberFormat.getInstance().format(getObject().getPriceAfterDiscount()));
            discountTextView.setText(String.valueOf(getObject().getDiscount()+ getString(R.string.percent_symb))+" "+getString(R.string.off));
        }
        sellerNameTextView.setText(getObject().getSeller().getBusinessName());
        sellerLocationTextView.setText(getObject().getCity());
        drawingImage(getObject());
        itemView.setOnClickListener(v -> {
            Gilimanuk.withAnim(getFragment().getAppearAnims()).goTo(getFragment().getActivity(), ProductDetailFragment.TAG, Bundler.wrap(ProductDetailFragment.class, product), R.id.fragment_frame, true, false);
        });

    }

    private void drawingImage(Product product) {
        loader.setVisibility(View.VISIBLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            imageView.setClipToOutline(true);
        }
        GlideApp.with(getFragment()).load(StringUtils.split(product.getImage(), ",")[0])
                .placeholder(R.drawable.ic_product_default)
                .error(R.drawable.ic_product_default)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        e.printStackTrace();
                        loader.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        Log.i(TAG, "onResourceReady");
                        loader.setVisibility(View.GONE);
                        return false;
                    }
                }).diskCacheStrategy(DiskCacheStrategy.ALL).into(imageView);
    }

    public <T extends SimpleBaseFragmentInitiator> T getFragment2() {
        return (T) getFragment();
    }

    @Override
    public void find(String s) {

    }
}
