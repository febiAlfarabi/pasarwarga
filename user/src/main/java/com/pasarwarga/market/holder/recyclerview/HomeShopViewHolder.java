package com.pasarwarga.market.holder.recyclerview;

import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.SimpleViewHolder;
import com.alfarabi.alfalibs.tools.Gilimanuk;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.fragment.SimpleBaseFragmentInitiator;
import com.alfarabi.base.model.FeaturedShop;
import com.alfarabi.base.realm.RealmDao;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.hendraanggrian.bundler.Bundler;
import com.pasarwarga.market.R;
import com.pasarwarga.market.fragment.home.HomeFragment;
import com.pasarwarga.market.fragment.store.profile.StoreProfileFragment;
import com.singh.daman.proprogressviews.DottedArcProgress;

import org.apache.commons.lang.StringUtils;

import butterknife.BindView;

/**
 * Created by Alfarabi on 6/19/17.
 */

public class HomeShopViewHolder extends SimpleViewHolder<HomeFragment, FeaturedShop, String> {

    public static final String TAG = HomeShopViewHolder.class.getName();

    @BindView(R.id.viewholder_layout) LinearLayout viewHolderLayout;
    @BindView(R.id.imageview) ImageView imageView;
    @BindView(R.id.name_tv) TextView nameTextView;
    @BindView(R.id.dotted_arcprogress) DottedArcProgress dottedArcProgress;
    @BindView(R.id.seller_location_tv) TextView sellerLocationTextView;

    public HomeShopViewHolder(HomeFragment fragment, ViewGroup viewGroup) {
        super(fragment, R.layout.viewholder_home_shop, viewGroup);
    }

    @Override
    public void showData(FeaturedShop object) {
        super.showData(object);
        final FeaturedShop featuredShop = RealmDao.unmanage(object);
        if (featuredShop.getStoreImage() != null) {
            drawingImage(featuredShop);
        }
        nameTextView.setText(featuredShop.getShopName());
        if(object.getShopLocation()!=null) {
            sellerLocationTextView.setText(object.getShopLocation().getCity().getName());
        }
        itemView.setOnClickListener(v -> {
            Gilimanuk.withAnim(getFragment().getAppearAnims()).goTo(getFragment().getActivity(), StoreProfileFragment.TAG, Bundler.wrap(StoreProfileFragment.class, object), R.id.fragment_frame, true, false);
        });

    }

    private void drawingImage(FeaturedShop featuredShop) {
        dottedArcProgress.setVisibility(DottedArcProgress.VISIBLE);
        imageView.setVisibility(ImageView.INVISIBLE);
        GlideApp.with(getFragment()).load(StringUtils.split(featuredShop.getStoreImage(), ",")[0])
                .placeholder(R.drawable.ic_product_default)
                .error(R.drawable.ic_product_default)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        e.printStackTrace();
                        dottedArcProgress.setVisibility(DottedArcProgress.GONE);
                        imageView.setVisibility(ImageView.VISIBLE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        Log.i(TAG, "onResourceReady");
                        dottedArcProgress.setVisibility(DottedArcProgress.GONE);
                        imageView.setVisibility(ImageView.VISIBLE);
                        return false;
                    }
                }).into(imageView);
    }

    public <T extends SimpleBaseFragmentInitiator> T getFragment2() {
        return (T) getFragment();
    }

    @Override
    public void find(String s) {

    }
}
