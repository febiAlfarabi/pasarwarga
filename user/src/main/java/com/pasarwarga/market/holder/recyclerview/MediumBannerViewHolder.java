package com.pasarwarga.market.holder.recyclerview;

import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.SimpleViewHolder;
import com.alfarabi.alfalibs.tools.Gilimanuk;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.fragment.SimpleBaseFragmentInitiator;
import com.alfarabi.base.model.MediumBanner;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.hendraanggrian.bundler.Bundler;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.fragment.EventFragment;
import com.pasarwarga.market.fragment.home.HomeFragment;
import com.pasarwarga.market.fragment.store.profile.StoreProfileFragment;
import com.singh.daman.proprogressviews.DottedArcProgress;

import butterknife.BindView;

/**
 * Created by Alfarabi on 6/19/17.
 */

public class MediumBannerViewHolder extends SimpleViewHolder<HomeFragment, MediumBanner, String> {

    public static final String TAG = MediumBannerViewHolder.class.getName();

    @BindView(R.id.imageview) ImageView imageView;
    @BindView(R.id.dotted_arcprogress) DottedArcProgress dottedArcProgress;

    public MediumBannerViewHolder(HomeFragment fragment, ViewGroup viewGroup) {
        super(fragment, R.layout.viewholder_home_medium_banner, viewGroup);
    }

    @Override
    public void showData(MediumBanner object) {
        super.showData(object);
//        if(object.getEventId()==null || object.getEventId().equals("")){
//            itemView.setVisibility(View.GONE);
//            getFragment().getLanding().getMediumBanners().remove(getAdapterPosition());
//            getFragment().getStringStickyRecyclerAdapter().notifyItemRemoved(getAdapterPosition());
//            return;
//        }
        dottedArcProgress.setVisibility(DottedArcProgress.VISIBLE);
        imageView.setVisibility(ImageView.INVISIBLE);
                GlideApp.with(getFragment()).load(object.getImageUrl())
                .placeholder(R.drawable.banner_pasarwarga)
                .error(R.drawable.banner_pasarwarga)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        e.printStackTrace();
                        dottedArcProgress.setVisibility(DottedArcProgress.GONE);
                        imageView.setVisibility(ImageView.VISIBLE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        Log.i(TAG, "onResourceReady");
                        dottedArcProgress.setVisibility(DottedArcProgress.GONE);
                        imageView.setVisibility(ImageView.VISIBLE);
                        return false;
                    }
                }).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL).into(imageView);


        itemView.setOnClickListener(v -> {
            if(object.getEventId()==null){
                return;
            }
            Gilimanuk.withAnim(getFragment().getAppearAnims()).goTo(getFragment().getActivity(), EventFragment.TAG, Bundler.wrap(EventFragment.class, object), R.id.fragment_frame, true);
        });

    }

    private void drawingImage(MediumBanner mediumBanner) {

    }

    public <T extends SimpleBaseFragmentInitiator> T getFragment2() {
        return (T) getFragment();
    }

    @Override
    public void find(String s) {

    }
}
