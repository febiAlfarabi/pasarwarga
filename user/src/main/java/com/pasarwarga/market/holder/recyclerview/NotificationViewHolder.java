//package com.pasarwarga.market.holder.recyclerview;
//
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.SimpleViewHolder;
//import com.alfarabi.alfalibs.tools.GlideApp;
//import com.alfarabi.alfalibs.tools.WLog;
//import com.alfarabi.base.model.notification.Notification;
//import com.pasarwarga.market.R;
//import com.pasarwarga.market.fragment.store.NotificationListFragment;
//
//import org.apache.commons.lang3.time.DateFormatUtils;
//
//import butterknife.BindView;
//
///**
// * Created by Alfarabi on 6/19/17.
// */
//
//public class NotificationViewHolder extends SimpleViewHolder<NotificationListFragment, Notification, String> {
//
////    public static final String TAG = NotificationViewHolder.class.getName();
//
//    @BindView(R.id.imageview) ImageView imageView ;
//    @BindView(R.id.date_tv) TextView dateTextView;
//    @BindView(R.id.title_tv) TextView titleTextView;
//    @BindView(R.id.subtitle_tv) TextView subtitleTextView;
//
//    public NotificationViewHolder(NotificationListFragment fragment, ViewGroup viewGroup) {
//        super(fragment, R.layout.viewholder_notification, viewGroup);
////        itemView.setOnClickListener(v -> {
////            WindowFlow.goTo(fragment.activity(), ProductDetailFragment.TAG, R.id.fragment_frame, true);
////        });
//    }
//
//    @Override
//    public void showData(Notification object) {
//        super.showData(object);
//        if(object!=null && object.getImage()!=null && !object.getImage().equals("")){
//            WLog.i(TAG, object.getImage());
//            GlideApp.with(getFragment()).load(object.getImage()).placeholder(R.drawable.user).circleCrop().into(imageView);
//        }
//        dateTextView.setText(DateFormatUtils.format(object.getCreated(), getFragment().getString(R.string.E_dd_MM_yyyy)));
//        titleTextView.setText(object.getTitle());
//        subtitleTextView.setText(object.getSubtitle());
//    }
//
//    @Override
//    public void find(String s) {
//
//    }
//}
