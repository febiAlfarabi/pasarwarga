package com.pasarwarga.market.holder.recyclerview;

import android.view.ViewGroup;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.SimpleViewHolder;
import com.pasarwarga.market.R;
import com.pasarwarga.market.fragment.store.OrderListFragment;
import com.alfarabi.base.model.transaction.CartItem;

/**
 * Created by Alfarabi on 6/19/17.
 */

public class OrderViewHolder extends SimpleViewHolder<OrderListFragment, CartItem, String> {


    public OrderViewHolder(OrderListFragment fragment, ViewGroup viewGroup) {
        super(fragment, R.layout.viewholder_order, viewGroup);
    }

    @Override
    public void find(String s) {

    }
}
