package com.pasarwarga.market.holder.recyclerview;

import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.SimpleViewHolder;
import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.Gilimanuk;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.http.basemarket.service.FavoriteService;
import com.alfarabi.base.http.basemarket.service.ProductService;
import com.alfarabi.base.model.Product;
import com.alfarabi.base.tools.ResponseTools;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.hendraanggrian.bundler.Bundler;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.fragment.store.product.ProductWishlistFragment;
import com.pasarwarga.market.fragment.store.product.ProductDetailFragment;
import com.pasarwarga.market.tools.SystemApp;
import com.singh.daman.proprogressviews.DottedArcProgress;

import net.soroushjavdan.customfontwidgets.CTextView;

import org.apache.commons.lang.StringUtils;

import java.text.NumberFormat;
import java.util.HashMap;

import butterknife.BindView;

/**
 * Created by USER on 9/12/2017.
 */

public class ProductWishlistViewHolder extends SimpleViewHolder<ProductWishlistFragment, Product, String> {

    @BindView(R.id.imageview) ImageView imageView;
    @BindView(R.id.dotted_arcprogress) DottedArcProgress dottedArcProgress;
    @BindView(R.id.discount_tv) CTextView discountTextView;
    @BindView(R.id.product_name_tv) TextView productNameTextView;
    @BindView(R.id.product_delete_button) Button productDeleteButton;
    @BindView(R.id.product_normalprice_tv) TextView normalPriceTextView;
    @BindView(R.id.product_discountprice_tv) TextView discountPriceTextView;
    @BindView(R.id.product_seller_tv) TextView sellerNameTextView;
    @BindView(R.id.product_seller_location_tv) TextView sellerLocationTextView;
    @BindView(R.id.buy_button) Button buttonBuy;

    public ProductWishlistViewHolder(ProductWishlistFragment fragment, ViewGroup viewGroup) {
        super(fragment, R.layout.viewholder_product_wishlist, viewGroup);
    }

    @Override
    public void showData(Product object) {
        super.showData(object);
        productNameTextView.setText(object.getName());
        if (object.getDiscount() == 0) {
            normalPriceTextView.setPaintFlags(Paint.LINEAR_TEXT_FLAG);
            normalPriceTextView.setText(getString(R.string.curr_symb) + NumberFormat.getInstance().format(object.getBasePrice()));
            discountPriceTextView.setText(R.string.z);
            discountTextView.setVisibility(View.GONE);
        } else {
            discountTextView.setVisibility(View.VISIBLE);
            normalPriceTextView.setPaintFlags(normalPriceTextView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            normalPriceTextView.setText(getString(R.string.curr_symb) + NumberFormat.getInstance().format(object.getBasePrice()));
            discountPriceTextView.setText(getString(R.string.curr_symb) + NumberFormat.getInstance().format(object.getPriceAfterDiscount()));
            discountTextView.setText(String.valueOf(object.getDiscount() + getString(R.string.percent_symb)) + " " + getString(R.string.off));
        }
        sellerNameTextView.setText(object.getSeller().getBusinessName());
        String shortAddress = object.getSeller().getAddress()!=null?StringUtils.split(object.getSeller().getAddress(), ",")[0]:"";
        sellerLocationTextView.setText(shortAddress);
        drawingImage();

        productDeleteButton.setOnClickListener(v -> {
            if( SystemApp.checkLogin(getFragment().activity(), true)){
                getFragment().activity(HomeActivity.class).showDialog(getString(R.string.delete_confirmation), getString(R.string.delete_favorite_confirmation), (dialog, which) -> {
                    dialog.dismiss();
                    HashMap<String, String> hashMap = new HashMap<String, String >();
                    hashMap.put("product_id", String.valueOf(getObject().getId()));
                    HttpInstance.withProgress(getFragment().activity()).observe(HttpInstance.create(FavoriteService.class).changeFavorite(hashMap), changeFavoriteResponse ->{
                        ResponseTools.validate(getFragment().activity(), changeFavoriteResponse);
                        getFragment().getSimpleRecyclerAdapter().getObjects().remove(getAdapterPosition());
                        getFragment().getSimpleRecyclerAdapter().notifyItemRemoved(getAdapterPosition());
//                        Caster.activity(getFragment().activity(), HomeActivity.class).renderFavorite(getFragment().getSimpleRecyclerAdapter().getObjects().size());

                    }, throwable -> {
                        getFragment().activity(HomeActivity.class).showSnackbar(throwable, v1 -> {});
                    });
                }, (dialog, which) -> dialog.dismiss());
            }
        });
        buttonBuy.setOnClickListener(v -> {
            HttpInstance.withProgress(getFragment().activity()).observe(HttpInstance.create(ProductService.class).getProduct(getObject().getId()), productResponse -> {
                ResponseTools.validate(getFragment().activity(), productResponse);
                if(productResponse.isStatus()){
                    Gilimanuk.withAnim(getFragment().getAppearAnims()).goTo(getFragment().getActivity(), ProductDetailFragment.TAG, Bundler.wrap(ProductDetailFragment.class, productResponse.getProduct()), R.id.fragment_frame, true, false);
                }else{
                    getFragment().activity(HomeActivity.class).showDialog(productResponse.getMessage(), (dialog1, which1) -> dialog1.dismiss());
                }
            }, throwable -> {
                getFragment().activity(HomeActivity.class).showDialog(throwable, (dialog1, which1) -> {});
            }, false);


        });
    }

    @Override
    public void find(String s) {
    }

    private void drawingImage() {
        dottedArcProgress.setVisibility(DottedArcProgress.VISIBLE);
        imageView.setVisibility(ImageView.INVISIBLE);

        GlideApp.with(getFragment()).load(StringUtils.split(getObject().getImage(), ",")[0])
                .placeholder(R.drawable.ic_product_default)
                .error(R.drawable.ic_product_default)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        e.printStackTrace();
                        dottedArcProgress.setVisibility(DottedArcProgress.GONE);
                        imageView.setVisibility(ImageView.VISIBLE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        dottedArcProgress.setVisibility(DottedArcProgress.GONE);
                        imageView.setVisibility(ImageView.VISIBLE);
                        return false;
                    }
                }).into(imageView);
    }
}
