package com.pasarwarga.market.holder.recyclerview;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.SimpleViewHolder;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.base.fragment.BaseUserFragment;
import com.alfarabi.base.model.transaction.purchase.Purchase;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.pasarwarga.market.R;
import com.pasarwarga.market.fragment.store.transaction.purchase.PurchaseOrderListFragment;
import com.pasarwarga.market.fragment.store.transaction.purchase.PurchasePaymentListFragment;
import com.pasarwarga.market.fragment.store.transaction.purchase.PurchaseReceiptListFragment;
import com.pasarwarga.market.fragment.store.transaction.purchase.PurchaseTransactionListFragment;

import java.text.NumberFormat;

import butterknife.BindView;

/**
 * Created by USER on 9/28/2017.
 */

public class PurchaseOrderViewHolder extends SimpleViewHolder<BaseUserFragment, Purchase, String> {

    @BindView(R.id.product_image_iv) ImageView productImageView;
    @BindView(R.id.transaction_id_tv) TextView transactionIdTextView;
    @BindView(R.id.transaction_date_tv) TextView transactionDateTextView;
    @BindView(R.id.total_price_tv) TextView totalPriceTextView;
    @BindView(R.id.tracking_no_tv) TextView trackingNoTextView;
    private View.OnClickListener onClickListener;

    public PurchaseOrderViewHolder(PurchasePaymentListFragment fragment, ViewGroup viewGroup) {
        super(fragment, R.layout.viewholder_user_order, viewGroup);
        this.onClickListener = fragment.onClickListener;
    }

    public PurchaseOrderViewHolder(PurchaseOrderListFragment fragment, ViewGroup viewGroup) {
        super(fragment, R.layout.viewholder_user_order, viewGroup);
        this.onClickListener = fragment.onClickListener;
    }

    public PurchaseOrderViewHolder(PurchaseReceiptListFragment fragment, ViewGroup viewGroup) {
        super(fragment, R.layout.viewholder_user_order, viewGroup);
        this.onClickListener = fragment.onClickListener;
    }

    public PurchaseOrderViewHolder(PurchaseTransactionListFragment fragment, ViewGroup viewGroup) {
        super(fragment, R.layout.viewholder_user_order, viewGroup);
        this.onClickListener = fragment.onClickListener;
    }

    @Override
    public void showData(Purchase purchase) {
        super.showData(purchase);
        GlideApp.with(getFragment()).load(purchase.getProduct().getImage()).placeholder(R.drawable.ic_product_default).error(R.drawable.ic_product_default)
                .into(productImageView);
        transactionIdTextView.setText(getString(R.string.transaction_id) + " : " + "#" + purchase.getDealCodeNumber());
        transactionDateTextView.setText(getString(R.string.transaction_date) + " : " + purchase.getCreated());
        totalPriceTextView.setText(getString(R.string.total_price) + " : " + getString(R.string.curr_symb) + NumberFormat.getInstance().format(purchase.getTotal()));
        if (purchase.getTrackingId().isEmpty()) {
            trackingNoTextView.setVisibility(View.INVISIBLE);
        } else {
            trackingNoTextView.setVisibility(View.VISIBLE);
            trackingNoTextView.setText(getString(R.string.tracking_no) + " : " + purchase.getTrackingId());
        }

        if (onClickListener != null) {
            itemView.setTag(purchase);
            itemView.setOnClickListener(onClickListener);
        }
    }

    @Override
    public void find(String s) {

    }
}
