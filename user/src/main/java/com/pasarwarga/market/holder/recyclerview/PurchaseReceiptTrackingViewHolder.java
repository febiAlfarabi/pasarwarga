package com.pasarwarga.market.holder.recyclerview;

import android.view.ViewGroup;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.SimpleViewHolder;
import com.alfarabi.base.fragment.BaseUserFragment;
import com.alfarabi.base.model.transaction.HistoryTracking;
import com.pasarwarga.market.R;
import com.pasarwarga.market.fragment.store.transaction.purchase.PurchaseReceiptDetailFragment;

import butterknife.BindView;

/**
 * Created by USER on 10/12/2017.
 */

public class PurchaseReceiptTrackingViewHolder extends SimpleViewHolder<BaseUserFragment, HistoryTracking, String> {

    @BindView(R.id.row_1_tv) TextView row1TextView;
    @BindView(R.id.row_2_tv) TextView row2TextView;

    public PurchaseReceiptTrackingViewHolder(PurchaseReceiptDetailFragment fragment, ViewGroup viewGroup) {
        super(fragment, R.layout.viewholder_tracking, viewGroup);
    }

    @Override
    public void showData(HistoryTracking object) {
        super.showData(object);
        row1TextView.setText(object.getManifesDate() + " " + object.getManifestTime());
        row2TextView.setText(object.getManifestDescription());
    }

    @Override
    public void find(String s) {

    }
}
