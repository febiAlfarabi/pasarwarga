package com.pasarwarga.market.holder.recyclerview;

import android.view.ViewGroup;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.SimpleViewHolder;
import com.alfarabi.base.model.location.Category;
import com.pasarwarga.market.R;
import com.pasarwarga.market.fragment.store.CategoryListFragment;

/**
 * Created by Alfarabi on 6/19/17.
 */

public class PurchaseViewHolder extends SimpleViewHolder<CategoryListFragment, Category, String> {

//    @BindView(R.id.row_1_tv) TextView row1TextView;

    public PurchaseViewHolder(CategoryListFragment fragment, ViewGroup viewGroup) {
        super(fragment, R.layout.viewholder_purchase, viewGroup);
    }

    @Override
    public void showData(Category object) {
        super.showData(object);
//        row1TextView.setText(object.getName());
//        itemView.setOnClickListener(v -> {
//            Bundle bundle = new Bundle();
//            bundle.putParcelable(Initial.CATEGORY, Parcels.wrap(object));
//            WindowFlow.goTo(getFragment().activity(HomeActivity.class), ProductListFragment.TAG, bundle, R.id.fragment_frame, true);
//        });

    }

    @Override
    public void find(String s) {

    }
}
