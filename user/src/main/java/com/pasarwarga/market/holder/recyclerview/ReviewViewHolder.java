package com.pasarwarga.market.holder.recyclerview;

import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.SimpleViewHolder;
import com.pasarwarga.market.R;
import com.pasarwarga.market.fragment.store.ReviewFragment;
import com.alfarabi.base.model.review.Review;

import butterknife.BindView;

/**
 * Created by Alfarabi on 7/20/17.
 */

public class ReviewViewHolder extends SimpleViewHolder<ReviewFragment, Review, String> {

    @BindView(R.id.imageview) ImageView imageView;
    @BindView(R.id.name_tv) TextView nameTextView;
    @BindView(R.id.date_tv) TextView dateTextView;
    @BindView(R.id.review_tv) TextView reviewTextView;
    @BindView(R.id.rating_bar) RatingBar ratingBar;

    public ReviewViewHolder(ReviewFragment fragment, ViewGroup viewGroup) {
        super(fragment, R.layout.viewholder_review, viewGroup);
    }

    @Override
    public void find(String s) {
    }

    @Override
    public void showData(Review object) {
        super.showData(object);
        nameTextView.setText(object.getUserFirstname() + " " + object.getUserLastname());
        dateTextView.setText(object.getDateAdded());
        reviewTextView.setText(object.getDescription());
        ratingBar.setRating(Double.valueOf(object.getRating()).intValue());
    }
}
