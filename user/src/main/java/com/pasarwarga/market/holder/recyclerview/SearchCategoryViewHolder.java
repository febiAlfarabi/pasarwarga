package com.pasarwarga.market.holder.recyclerview;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.SimpleViewHolder;
import com.alfarabi.alfalibs.tools.Gilimanuk;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.model.location.Category;
import com.alfarabi.base.tools.Initial;
import com.alfarabi.alfalibs.tools.KeyboardUtils;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.fragment.store.product.ProductListFragment;
import com.pasarwarga.market.fragment.store.product.ProductSearchFragment;

import org.parceler.Parcels;

import butterknife.BindView;

/**
 * Created by Alfarabi on 6/19/17.
 */

public class SearchCategoryViewHolder extends SimpleViewHolder<ProductSearchFragment, Category, String> {

    @BindView(R.id.row_1_tv) TextView row1TextView ;
    @BindView(R.id.imageview) ImageView imageView ;

    public SearchCategoryViewHolder(ProductSearchFragment fragment, ViewGroup viewGroup) {
        super(fragment, R.layout.viewholder_category, viewGroup);
    }

    @Override
    public void showData(Category object) {
        super.showData(object);
        row1TextView.setText(object.getName());
        if(object.getImage()!=null){
            imageView.setVisibility(View.VISIBLE);
            GlideApp.with(getFragment()).load(object.getImage()).placeholder(R.drawable.ic_category_black_24dp).error(R.drawable.ic_category_black_24dp)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            GlideApp.with(getFragment()).load(getFragment().getResources().getDrawable(R.drawable.ic_category_black_24dp));
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }
                    }).into(imageView);
        }else{
            imageView.setVisibility(View.GONE);
        }
        itemView.setOnClickListener(v -> {
            KeyboardUtils.hideKeyboard(getFragment().activity());
            Bundle bundle = new Bundle();
            bundle.putParcelable(Initial.CATEGORY, Parcels.wrap(object));
            Gilimanuk.withAnim(getFragment().getAppearAnims()).goTo(getFragment().activity(HomeActivity.class), ProductListFragment.TAG, bundle, R.id.fragment_frame, false, false);
        });

    }

    @Override
    public void find(String s) {

    }
}
