
package com.pasarwarga.market.holder.recyclerview;

import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.SimpleViewHolder;
import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.Gilimanuk;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.http.basemarket.service.FavoriteService;
import com.alfarabi.base.http.basemarket.service.ProductService;
import com.alfarabi.base.model.FormProduct;
import com.alfarabi.base.model.Product;
import com.alfarabi.base.model.Shipping;
import com.alfarabi.base.realm.RealmDao;
import com.alfarabi.base.tools.ResponseTools;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.hendraanggrian.bundler.Bundler;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.fragment.asseller.SellerProductFragment;
import com.pasarwarga.market.fragment.asseller.productform.ProductForm1Fragment;
import com.pasarwarga.market.fragment.store.product.ProductDetailFragment;
import com.pasarwarga.market.tools.SystemApp;
import com.tuyenmonkey.mkloader.MKLoader;

import net.soroushjavdan.customfontwidgets.CTextView;

import org.apache.commons.lang.StringUtils;

import java.text.NumberFormat;
import java.util.HashMap;

import butterknife.BindView;

/**
 * Created by Alfarabi on 6/19/17.
 */

public class SellerProductViewHolder extends SimpleViewHolder<SellerProductFragment, Product, String> {

    final static HashMap<Integer, Integer> layoutMap = new HashMap<>();
    static {
        layoutMap.put(1, R.layout.viewholder_product_1);
        layoutMap.put(2, R.layout.viewholder_product_2);

    }

    @BindView(R.id.viewholder_layout) LinearLayout viewHolderLayout;
    @BindView(R.id.product_toolbar) Toolbar productToolbar ;

    @BindView(R.id.imageview) ImageView imageView;
    @BindView(R.id.name_tv) TextView nameTextView;
    @BindView(R.id.original_price_tv) TextView originalPriceTextView;
    @BindView(R.id.price_tv) TextView priceTextView;
    @BindView(R.id.discount_tv) CTextView discountTextView;
    @BindView(R.id.loader) MKLoader loader;
    @BindView(R.id.unpublished_masker)
    RelativeLayout unpublishedMaskerLayout ;

    @BindView(R.id.seller_name_tv) TextView sellerNameTextView;
    @BindView(R.id.seller_location_tv) TextView sellerLocationTextView;

    @BindView(R.id.whislist_iv) ImageView whislistImageView ;


    public SellerProductViewHolder(SellerProductFragment fragment, ViewGroup viewGroup) {
        super(fragment, layoutMap.get(fragment.getStoreProfileListColumn()), viewGroup);
    }

    @Override
    public void showData(Product object) {
        super.showData(object);
        nameTextView.setText(object.getName());

        productToolbar.setVisibility(View.GONE);
        whislistImageView.setVisibility(View.VISIBLE);
        if(productToolbar.getMenu()!=null){
            productToolbar.getMenu().clear();
        }
        sellerNameTextView.setText(getObject().getSeller().getBusinessName());
        sellerLocationTextView.setText(getObject().getCity());
        if(SystemApp.checkLogin(getFragment().activity())){
            if(SystemApp.myProfile().getUser().isSeller()){
                if(SystemApp.myProfile().getUser().getSellerId().equals(String.valueOf(object.getSeller().getId()))){
                    productToolbar.inflateMenu(R.menu.product_holder_menu);
                    productToolbar.setVisibility(View.VISIBLE);
                    whislistImageView.setVisibility(View.GONE);
                    productToolbar.getMenu().findItem(R.id.publish_menu).setTitle(getString(object.isPublish()?R.string.unpublish:R.string.publish));
                    productToolbar.setOnMenuItemClickListener(item -> {
                        if(item.getTitle().equals(getString(R.string.edit))){
                            prepareEdit();
                        }else if(item.getTitle().equals(getString(R.string.publish))){
                            publishUnpublish();
                        }else if(item.getTitle().equals(getString(R.string.unpublish))){
                            publishUnpublish();
                        }
                        return true ;
                    });
                }
            }
        }
        if (object.getDiscount() == 0) {
            originalPriceTextView.setPaintFlags(Paint.LINEAR_TEXT_FLAG);
            originalPriceTextView.setText(getString(R.string.curr_symb) + NumberFormat.getInstance().format(object.getBasePrice()));
            priceTextView.setText(R.string.z);
            discountTextView.setVisibility(View.GONE);
        } else {
            discountTextView.setVisibility(View.VISIBLE);
            originalPriceTextView.setPaintFlags(originalPriceTextView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            originalPriceTextView.setText(getString(R.string.curr_symb) + NumberFormat.getInstance().format(object.getBasePrice()));
            priceTextView.setText(getString(R.string.curr_symb) + NumberFormat.getInstance().format(object.getPriceAfterDiscount()));
            discountTextView.setText(String.valueOf(object.getDiscount() + getString(R.string.percent_symb)));
        }
        drawingImage();
        itemView.setTag(this);
        if(getFragment().getOnLongClickListener()!=null){
            itemView.setOnLongClickListener(getFragment().getOnLongClickListener());
        }
        if(object.isPublish()){
            unpublishedMaskerLayout.setVisibility(View.GONE);
            itemView.setOnClickListener(v -> {
                Gilimanuk.withAnim(getFragment().getAppearAnims()).goTo(getFragment().getActivity(), ProductDetailFragment.TAG, Bundler.wrap(ProductDetailFragment.class, object), R.id.fragment_frame, true, false);
            });
        }else{
            unpublishedMaskerLayout.setVisibility(View.VISIBLE);
            itemView.setOnClickListener(v -> {
//                WindowFlow.goTo(getFragment().getActivity(), ProductDetailFragment.TAG, Bundler.wrap(ProductDetailFragment.class, object), R.id.fragment_frame, true, false);
                getFragment().activity(HomeActivity.class).showSnackbar(getString(R.string.publish_before_show_detail), v1 -> {});
            });
        }
    }

    @Override
    public void find(String s) {}

    private void drawingImage() {
        if(getObject().isFavourite()){
            whislistImageView.setImageDrawable(getFragment().getResources().getDrawable(R.drawable.ic_wishlist_2));
        }else{
            whislistImageView.setImageDrawable(getFragment().getResources().getDrawable(R.drawable.ic_wishlist_1));
        }
        whislistImageView.setOnClickListener(v -> {
            if( SystemApp.checkLogin(getFragment().activity(), true)){
                loader.setVisibility(View.VISIBLE);
                HashMap<String, String> hashMap = new HashMap<String, String >();
                hashMap.put("product_id", String.valueOf(getObject().getId()));
                HttpInstance.call(HttpInstance.create(FavoriteService.class).changeFavorite(hashMap), changeFavoriteResponse ->{
                    ResponseTools.validate(getFragment().activity(), changeFavoriteResponse);
                    loader.setVisibility(View.INVISIBLE);
                    RealmDao.instance().insertOrUpdate(changeFavoriteResponse.getProduct());
                    setObject(changeFavoriteResponse.getProduct());
                    if(getObject().isFavourite()){
                        whislistImageView.setImageDrawable(getFragment().getResources().getDrawable(R.drawable.ic_wishlist_2));
                    }else{
                        whislistImageView.setImageDrawable(getFragment().getResources().getDrawable(R.drawable.ic_wishlist_1));
                    }

                }, throwable -> {
                    getFragment().activity(HomeActivity.class).showSnackbar(throwable, v1 -> {});
                    loader.setVisibility(View.INVISIBLE);
                },false);
            }
        });


        loader.setVisibility(View.VISIBLE);
        imageView.setVisibility(ImageView.INVISIBLE);

        GlideApp.with(getFragment()).load(StringUtils.split(getObject().getImage(), ",")[0])
                .placeholder(R.drawable.ic_product_default)
                .error(R.drawable.ic_product_default)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        e.printStackTrace();
                        loader.setVisibility(View.GONE);
                        imageView.setVisibility(ImageView.VISIBLE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        loader.setVisibility(View.GONE);
                        imageView.setVisibility(ImageView.VISIBLE);
                        return false;
                    }
                }).override(Target.SIZE_ORIGINAL).into(imageView);
    }


    public void prepareEdit(){
        HttpInstance.withProgress(getFragment().activity()).observe(HttpInstance.create(ProductService.class).getProduct(getObject().getId()), productResponse -> {
            ResponseTools.validate(getFragment().activity(), productResponse);
            if(productResponse.isStatus()){
                Product product = productResponse.getProduct();
                FormProduct formProduct = new FormProduct();
                formProduct.setId(product.getId());
                formProduct.setName(product.getName());
                formProduct.setCategoryId(Long.valueOf(product.getCategory().getId()));
                formProduct.setBasePrice(product.getBasePrice());
                formProduct.setDiscount(product.getDiscount());
                formProduct.setStartDiscount(product.getStartDiscount());
                formProduct.setEndDiscount(product.getEndDiscount());
                formProduct.setDescription(product.getDescription());
                formProduct.setWeight(product.getWeight());
                formProduct.setQuantity(product.getQuantity());
                formProduct.setTags(product.getTags());
                formProduct.setPublish(product.isPublish());
                StringBuilder shippingMethodIds = new StringBuilder();
                String shippingIds = "";
                for (Shipping shipping : product.getShippings()) {
                    shippingMethodIds.append(shipping.getId()).append(",");
                }
                if(shippingMethodIds.toString().endsWith(",")){
                    shippingIds = shippingMethodIds.toString().substring(0, shippingMethodIds.toString().length()-1);
                }else{
                    shippingIds = shippingMethodIds.toString();
                }
                formProduct.setShippingMethodIds(shippingIds);
                formProduct.setInsurance(product.isInsurance());
                formProduct.setProductImage(product.getImages());
                Gilimanuk.withAnim(getFragment().getAppearAnims()).goTo(getFragment().activity(HomeActivity.class), ProductForm1Fragment.TAG, Bundler.wrap(ProductForm1Fragment.class, formProduct, product.getCategory(), ProductForm1Fragment.MODE_EDIT), R.id.fragment_frame, true, false);
            }else{
                getFragment().activity(HomeActivity.class).showDialog(productResponse.getMessage(),(dialog1, which1) -> dialog1.dismiss());
            }

        }, throwable -> {
            getFragment().activity(HomeActivity.class).showDialog(throwable, (dialog1, which1) -> {});
        }, false);
    }

    public void publishUnpublish(){
        HttpInstance.withProgress(getFragment().activity()).observe(HttpInstance.create(ProductService.class).changePublishStatus(getObject().getId()), productResponse -> {
            ResponseTools.validate(getFragment().activity(), productResponse);
            if(productResponse.isStatus()){
                getFragment().getSimpleRecyclerAdapter().getObjects().remove(getAdapterPosition());
                getFragment().getSimpleRecyclerAdapter().getObjects().add(getAdapterPosition(), productResponse.getProduct());
                getFragment().getSimpleRecyclerAdapter().notifyItemChanged(getAdapterPosition());

            }else{
                getFragment().activity(HomeActivity.class).showDialog(productResponse.getMessage(), (dialog1, which1) -> dialog1.dismiss());
            }
        }, throwable -> {
            getFragment().activity(HomeActivity.class).showDialog(throwable, (dialog1, which1) -> {});
        }, false);
    }
}
