package com.pasarwarga.market.holder.recyclerview;

import android.view.ViewGroup;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.SimpleViewHolder;
import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.Gilimanuk;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.http.basemarket.service.ProductService;
import com.alfarabi.base.model.Product;
import com.alfarabi.base.model.Specified;
import com.alfarabi.base.realm.RealmDao;
import com.alfarabi.alfalibs.tools.KeyboardUtils;
import com.alfarabi.base.tools.ResponseTools;
import com.hendraanggrian.bundler.Bundler;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.fragment.store.product.ProductDetailFragment;
import com.pasarwarga.market.fragment.store.product.ProductSearchFragment;

import butterknife.BindView;

/**
 * Created by Alfarabi on 6/19/17.
 */

public class SpecifiedViewHolder extends SimpleViewHolder<ProductSearchFragment, Specified, String> {

    @BindView(R.id.row_1_tv) TextView row1TextView;
    //Specified specified = new Specified();

    public SpecifiedViewHolder(ProductSearchFragment fragment, ViewGroup viewGroup) {
        super(fragment, R.layout.viewholder_specified, viewGroup);
    }

    @Override
    public void showData(Specified object) {
        super.showData(object);
        row1TextView.setText(object.getName());
        itemView.setOnClickListener(v -> {
            KeyboardUtils.hideKeyboard(getFragment().activity());
            Product product = RealmDao.instance().get(Long.valueOf(object.getId()), Product.class);
            if ( product != null) {
                Gilimanuk.withAnim(getFragment().getAppearAnims()).goTo(getFragment().getActivity(), ProductDetailFragment.TAG, Bundler.wrap(ProductDetailFragment.class,product ), R.id.fragment_frame, false, false);
                return;
            }else{
                HttpInstance.withProgress(getFragment().activity(HomeActivity.class)).observe(HttpInstance.create(ProductService.class).getProduct(Long.valueOf(object.getId())), productResponse -> {
                    ResponseTools.validate(getFragment().activity(), productResponse);
                    if (productResponse.isStatus() && productResponse.getProduct() != null) {
                        RealmDao.instance().insertOrUpdate(productResponse.getProduct());
                        Gilimanuk.withAnim(getFragment().getAppearAnims()).goTo(getFragment().getActivity(), ProductDetailFragment.TAG, Bundler.wrap(ProductDetailFragment.class, productResponse.getProduct()), R.id.fragment_frame, false, false);
                        //getObject() = RealmDao.instance().get(productResponse.getProduct().getId(), Specified.class);
                    } else {
                        getFragment().activity(HomeActivity.class).showSnackbar(productResponse.getMessage(), v1 -> {});
                    }
                }, throwable -> {
                    getFragment().activity(HomeActivity.class).showSnackbar(throwable, v1 -> {});
                });
            }

        });
    }

    @Override
    public void find(String s) {

    }
}
