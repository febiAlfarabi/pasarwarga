package com.pasarwarga.market.holder.recyclerview;

import android.view.ViewGroup;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.SimpleViewHolder;
import com.alfarabi.alfalibs.tools.Gilimanuk;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.model.FeaturedShop;
import com.alfarabi.alfalibs.tools.KeyboardUtils;
import com.hendraanggrian.bundler.Bundler;
import com.pasarwarga.market.R;
import com.pasarwarga.market.fragment.store.StoreSearchFragment;
import com.pasarwarga.market.fragment.store.profile.StoreProfileFragment;

import butterknife.BindView;

/**
 * Created by Alfarabi on 6/19/17.
 */

public class StoreViewHolder extends SimpleViewHolder<StoreSearchFragment, FeaturedShop, String> {

    @BindView(R.id.row_1_tv) TextView row1TextView;

    public StoreViewHolder(StoreSearchFragment fragment, ViewGroup viewGroup) {
        super(fragment, R.layout.viewholder_store, viewGroup);
    }

    @Override
    public void showData(FeaturedShop object) {
        super.showData(object);
        row1TextView.setText(object.getShopName());
        itemView.setOnClickListener(v -> {
            KeyboardUtils.hideKeyboard(getFragment().activity());
            Gilimanuk.withAnim(getFragment().getAppearAnims()).goTo(getFragment().getActivity(), StoreProfileFragment.TAG, Bundler.wrap(StoreProfileFragment.class, object), R.id.fragment_frame, false, false);

        });
    }

    @Override
    public void find(String s) {

    }
}
