package com.pasarwarga.market.holder.recyclerview;

import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.SimpleViewHolder;
import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.Gilimanuk;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.http.basemarket.service.SellerService;
import com.alfarabi.base.model.FeaturedShop;
import com.alfarabi.base.model.shop.SellerInfo;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.hendraanggrian.bundler.Bundler;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.fragment.store.StoreFavoriteFragment;
import com.pasarwarga.market.fragment.store.profile.StoreProfileFragment;
import com.pasarwarga.market.tools.SystemApp;
import com.singh.daman.proprogressviews.DottedArcProgress;

import org.apache.commons.lang.StringUtils;

import butterknife.BindView;

/**
 * Created by USER on 9/12/2017.
 */

public class StorefavoritViewHolder extends SimpleViewHolder<StoreFavoriteFragment, SellerInfo, String> {
    
    public static final String TAG = StorefavoritViewHolder.class.getName();

    @BindView(R.id.viewholder_layout) LinearLayout viewHolderLayout;
    @BindView(R.id.imageview) ImageView imageView;
    @BindView(R.id.name_tv) TextView nameTextView;
    @BindView(R.id.dotted_arcprogress) DottedArcProgress dottedArcProgress;
    @BindView(R.id.whislist_iv) ImageView whislistImageView ;
    @BindView(R.id.seller_location_tv) TextView sellerLocationTextView;

    public StorefavoritViewHolder(StoreFavoriteFragment fragment, ViewGroup viewGroup) {
        super(fragment, R.layout.viewholder_store_favorite, viewGroup);
    }

    @Override
    public void showData(SellerInfo object) {
        super.showData(object);
        if (object.getStoreImage() != null) {
            drawingImage(object);
        }
        nameTextView.setText(object.getShopName());
        if(object.getShopLocation()!=null) {
            sellerLocationTextView.setText(object.getShopLocation().getCity().getName());
        }
        whislistImageView.setImageDrawable(getFragment().getResources().getDrawable(R.drawable.ic_wishlist_2));
        whislistImageView.setOnClickListener(v -> {
            favoriteSeller();
        });
        itemView.setOnClickListener(v -> {
            FeaturedShop featuredShop = new FeaturedShop();
            featuredShop.setId(Long.valueOf(object.getId()));
            featuredShop.setShopName(object.getShopName());
            featuredShop.setAddress(object.getAddress());
            featuredShop.setLocation(object.getLocation());
            featuredShop.setShopRatting(String.valueOf(object.getShoprating()));
            featuredShop.setReviewCount(String.valueOf(object.getReviewCount()));
            featuredShop.setStoreImage(object.getStoreImage());
            featuredShop.setBannerImage(object.getBannerImage());
            featuredShop.setSellerEmail(object.getSellerEmail());
            featuredShop.setSellerName(object.getSellerName());
            Gilimanuk.withAnim(getFragment().getAppearAnims()).goTo(getFragment().getActivity(), StoreProfileFragment.TAG, Bundler.wrap(StoreProfileFragment.class, featuredShop), R.id.fragment_frame, true, false);
        });
    }

    @Override
    public void find(String s) {
    }

    private void drawingImage(SellerInfo sellerInfo) {
        dottedArcProgress.setVisibility(DottedArcProgress.VISIBLE);
        imageView.setVisibility(ImageView.INVISIBLE);
        GlideApp.with(getFragment()).load(StringUtils.split(sellerInfo.getStoreImage(), ",")[0])
//                .override(getFragment().getDimension()[0]/3, getFragment().getDimension()[1]/3)
                .placeholder(R.drawable.ic_product_default)
                .error(R.drawable.ic_product_default)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        e.printStackTrace();
                        dottedArcProgress.setVisibility(DottedArcProgress.GONE);
                        imageView.setVisibility(ImageView.VISIBLE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        Log.i(TAG, "onResourceReady");
                        dottedArcProgress.setVisibility(DottedArcProgress.GONE);
                        imageView.setVisibility(ImageView.VISIBLE);
                        return false;
                    }
                }).into(imageView);
    }

    public void favoriteSeller(){
        if(SystemApp.checkLogin(getFragment().activity(HomeActivity.class), true)){
            HttpInstance.withProgress(getFragment().activity()).observe(HttpInstance.create(SellerService.class).favorite(Long.valueOf(getObject().getId())), sellerFavoriteResponse -> {
                if(getObject().isFavorite()){
                    getObject().setFavorite(false);
                    getFragment().getSimpleRecyclerAdapter().getObjects().remove(getAdapterPosition());
                    getFragment().getSimpleRecyclerAdapter().notifyItemRemoved(getAdapterPosition());
                    getFragment().getSimpleRecyclerAdapter().notifyDataSetChanged();
                }
//                else{
//                    getObject().setFavorite(true);
//                    favoriteButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorite_red_24dp));
//                }
            }, throwable -> {

            }, false);
        }
    }

}
