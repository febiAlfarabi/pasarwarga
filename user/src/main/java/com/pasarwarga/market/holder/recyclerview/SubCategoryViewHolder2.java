package com.pasarwarga.market.holder.recyclerview;

import android.view.ViewGroup;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.SimpleViewHolder;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.model.location.Category;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.fragment.store.product.ProductListFragment;
import com.pasarwarga.market.fragment.store.SubCategoryFragment;

/**
 * Created by Alfarabi on 6/19/17.
 */

public class SubCategoryViewHolder2 extends SimpleViewHolder<SubCategoryFragment, Category, String> {


    public SubCategoryViewHolder2(SubCategoryFragment fragment, ViewGroup viewGroup) {
        super(fragment, R.layout.viewholder_subcategory, viewGroup);
        itemView.setOnClickListener(v -> {
            WindowFlow.goTo(fragment.activity(HomeActivity.class), ProductListFragment.TAG, R.id.fragment_frame, true);
        });
    }

    @Override
    public void find(String s) {

    }
}
