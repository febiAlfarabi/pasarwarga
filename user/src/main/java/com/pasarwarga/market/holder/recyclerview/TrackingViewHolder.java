package com.pasarwarga.market.holder.recyclerview;

import android.view.ViewGroup;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.SimpleViewHolder;
import com.alfarabi.base.fragment.BaseUserFragment;
import com.alfarabi.base.model.transaction.HistoryTracking;
import com.pasarwarga.market.R;
import com.pasarwarga.market.fragment.asseller.transactiontab.detail.OrderStatusDetailFragment;

import butterknife.BindView;

/**
 * Created by Alfarabi on 6/19/17.
 */

public class TrackingViewHolder extends SimpleViewHolder<BaseUserFragment, HistoryTracking, String> {

    @BindView(R.id.row_1_tv) TextView row1TextView;
    @BindView(R.id.row_2_tv) TextView row2TextView;


    public TrackingViewHolder(OrderStatusDetailFragment fragment, ViewGroup viewGroup) {
        super(fragment, R.layout.viewholder_tracking, viewGroup);
    }

    @Override
    public void showData(HistoryTracking object) {
        super.showData(object);
        row1TextView.setText(object.getManifesDate()+" "+object.getManifestTime());
        row2TextView.setText(object.getManifestDescription());
//        itemView.setOnClickListener(v -> {
//            Bundle bundle = new Bundle();
//            bundle.putParcelable(Initial.CATEGORY, Parcels.wrap(object));
//            WindowFlow.goTo(getFragment().activity(HomeActivity.class), ProductListFragment.TAG, bundle, R.id.fragment_frame, true);
//        });

    }

    @Override
    public void find(String s) {

    }
}
