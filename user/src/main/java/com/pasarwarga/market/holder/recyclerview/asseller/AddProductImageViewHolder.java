package com.pasarwarga.market.holder.recyclerview.asseller;

import android.graphics.Bitmap;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.SimpleViewHolder;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.alfalibs.tools.CommonUtil;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.fragment.asseller.productform.ProductForm1Fragment;

import java.io.File;
import java.io.IOException;

import butterknife.BindView;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alfarabi on 9/27/17.
 */

public class AddProductImageViewHolder extends SimpleViewHolder<ProductForm1Fragment, String, String> {

    @BindView(R.id.imageview) ImageView imageView ;
    @BindView(R.id.delete_button) ImageView deleteButton ;
//    @Getter Uri imageUri ;
//    @Getter@Setter String filename ;
    @Getter@Setter File file ;

    public AddProductImageViewHolder(ProductForm1Fragment fragment, ViewGroup viewGroup) {
        super(fragment, R.layout.viewholder_add_product_image, viewGroup);
    }

    @Override
    public void showData(String object) {
        super.showData(object);
        if(!object.isEmpty()){
//            setFilename(getObject().substring(object.lastIndexOf("/")));
            deleteButton.setVisibility(View.VISIBLE);
            loadImage();
        }else{
            imageView.setImageDrawable(null);
            deleteButton.setVisibility(View.GONE);
            itemView.setTag(this);
            itemView.setOnClickListener(getFragment().getImageClickListener());
        }

    }

    public void loadImage() {
        GlideApp.with(getFragment()).asBitmap().load(getObject()).placeholder(R.drawable.ic_product_default).error(R.drawable.ic_product_default).listener(new RequestListener<Bitmap>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
//                imageView.postDelayed(() -> loadImage(), 500);
                try{
//                    imageView.postDelayed(() -> getFragment().isDetached(), 500);
                }catch (Exception ex){
                    ex.printStackTrace();
                }
                deleteButton.setOnClickListener(v -> {
                    getFragment().activity(HomeActivity.class).showDialog(getString(R.string.confirmation), getString(R.string.delete_image_confirmation), (dialog, which) -> {
                        dialog.dismiss();
                        getFragment().getStringRecyclerAdapter().getObjects().remove(getAdapterPosition());
                        getFragment().getStringRecyclerAdapter().notifyItemRemoved(getAdapterPosition());
                        getFragment().setCurrentEditImageHolder(null);
                    }, (dialog, which) -> dialog.dismiss());
                });
                return false;
            }

            @Override
            public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                try {
                    file = CommonUtil.storeImage(getFragment().activity(), resource, getObject().substring(getObject().lastIndexOf("/")));
                    setFile(file);
                    setObject(file.getAbsolutePath());
                    getFragment().getStringRecyclerAdapter().getObjects().remove(getAdapterPosition());
                    getFragment().getStringRecyclerAdapter().getObjects().add(getAdapterPosition(), getObject());

                    itemView.setTag(AddProductImageViewHolder.this);
                    itemView.setOnClickListener(v -> {
                        getFragment().getImageClickListener().onClick(v);
                    });
                    itemView.setOnLongClickListener(v -> {
                        try {
                            getFragment().activity(HomeActivity.class).showDialog(getFragment().getString(R.string.location),
                                    file.getAbsolutePath()+"\n"+file.getCanonicalPath()+"\n"+file.getPath(), (dialog, which) -> dialog.dismiss());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        return true ;
                    });
                    deleteButton.setOnClickListener(v -> {
                        getFragment().activity(HomeActivity.class).showDialog(getString(R.string.confirmation), getString(R.string.delete_image_confirmation), (dialog, which) -> {
                            dialog.dismiss();
                            getFragment().getStringRecyclerAdapter().getObjects().remove(getAdapterPosition());
                            getFragment().getStringRecyclerAdapter().notifyItemRemoved(getAdapterPosition());
                            getFragment().setCurrentEditImageHolder(null);
                        }, (dialog, which) -> dialog.dismiss());
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return false;
            }
        }).into(imageView);
    }
    @Override
    public void find(String s) {

    }
}
