package com.pasarwarga.market.holder.recyclerview.asseller;

import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.SimpleViewHolder;
import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.http.basemarket.service.NoteService;
import com.alfarabi.base.model.shop.Note;
import com.alfarabi.base.tools.Initial;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.fragment.asseller.setting.NoteSellerSettingFragment;
import com.pasarwarga.market.fragment.asseller.setting.NotesDetailFragment;

import org.parceler.Parcels;

import butterknife.BindView;

/**
 * Created by Alfarabi on 6/19/17.
 */

public class NotesViewHolder extends SimpleViewHolder<NoteSellerSettingFragment, Note, String> {

    @BindView(R.id.expandable_layout) ExpandableRelativeLayout expandableRelativeLayout ;
    @BindView(R.id.row_1_tv) TextView row1TextView;
    @BindView(R.id.content_tv) TextView contentTextView;
    @BindView(R.id.edit_iv) ImageView editImageView ;
    @BindView(R.id.delete_iv) ImageView deleteImageView;


    public NotesViewHolder(NoteSellerSettingFragment fragment, ViewGroup viewGroup) {
        super(fragment, R.layout.viewholder_seller_notes, viewGroup);
    }

    @Override
    public void showData(Note object) {
        super.showData(object);
        row1TextView.setText(object.getTitle());
        contentTextView.setText(object.getContent());
        itemView.setOnClickListener(v -> {
            if(expandableRelativeLayout.isExpanded()){
                expandableRelativeLayout.collapse();
            }else{
                expandableRelativeLayout.expand();
            }
        });
        editImageView.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putParcelable(Initial.NOTES, Parcels.wrap(object));
            WindowFlow.goTo(getFragment().activity(HomeActivity.class), NotesDetailFragment.TAG, bundle, R.id.fragment_frame, true ,false);
        });
        deleteImageView.setOnClickListener(v -> {
            getFragment().activity(HomeActivity.class).showDialog(getString(R.string.delete_confirmation), getString(R.string.delete_note_confirmation), (dialog, which) -> {
                dialog.dismiss();
                HttpInstance.withProgress(getFragment().activity()).observe(HttpInstance.create(NoteService.class).delete(object.getId()), deleteNote -> {
                    if (deleteNote.isStatus()){
                        getFragment().getSimpleRecyclerAdapter().getObjects().remove(getAdapterPosition());
                        getFragment().getSimpleRecyclerAdapter().notifyItemRemoved(getAdapterPosition());
                        getFragment().getSimpleRecyclerAdapter().notifyDataSetChanged();
                    }else{
                        getFragment().activity(HomeActivity.class).showDialog(deleteNote.getMessage(),(dialog1, which1) -> dialog.dismiss());
                    }
                }, throwable -> {
                    getFragment().activity(HomeActivity.class).showSnackbar(throwable, v1 -> {});
                }, false);
            }, (dialog, which) -> dialog.dismiss());
        });
        itemView.postDelayed(() -> {
            expandableRelativeLayout.collapse();
        }, 10);

    }

    @Override
    public void find(String s) {

    }
}
