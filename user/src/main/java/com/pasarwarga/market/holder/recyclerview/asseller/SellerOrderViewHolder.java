package com.pasarwarga.market.holder.recyclerview.asseller;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.SimpleViewHolder;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.base.fragment.BaseUserFragment;
import com.alfarabi.base.model.shop.Order;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.pasarwarga.market.R;
import com.pasarwarga.market.fragment.asseller.transactiontab.OrderPackagingListFragment;
import com.pasarwarga.market.fragment.asseller.transactiontab.OrderRequestListFragment;
import com.pasarwarga.market.fragment.asseller.transactiontab.OrderStatusListFragment;
import com.pasarwarga.market.fragment.asseller.transactiontab.TransactionListFragment;

import java.text.NumberFormat;

import butterknife.BindView;

/**
 * Created by Alfarabi on 6/19/17.
 */

public class SellerOrderViewHolder extends SimpleViewHolder<BaseUserFragment, Order, String> {

    @BindView(R.id.product_image_iv) ImageView productImageView ;
    @BindView(R.id.transaction_id_tv) TextView transactionIdTextView ;
    @BindView(R.id.transaction_date_tv) TextView transactionDateTextView ;
    @BindView(R.id.total_price_tv) TextView totalPriceTextView ;
    private View.OnClickListener onClickListener ;

    public SellerOrderViewHolder(OrderRequestListFragment fragment, ViewGroup viewGroup) {
        super(fragment, R.layout.viewholder_seller_order, viewGroup);
        this.onClickListener = fragment.onClickListener;
    }

    public SellerOrderViewHolder(OrderPackagingListFragment fragment, ViewGroup viewGroup) {
        super(fragment, R.layout.viewholder_seller_order, viewGroup);
        this.onClickListener = fragment.onClickListener;
    }

    public SellerOrderViewHolder(OrderStatusListFragment fragment, ViewGroup viewGroup) {
        super(fragment, R.layout.viewholder_seller_order, viewGroup);
        this.onClickListener = fragment.onClickListener;
    }

    public SellerOrderViewHolder(TransactionListFragment fragment, ViewGroup viewGroup) {
        super(fragment, R.layout.viewholder_seller_order, viewGroup);
        this.onClickListener = fragment.onClickListener;
    }

    @Override
    public void showData(Order object) {
        super.showData(object);
        GlideApp.with(getFragment()).load(object.getProductImage()).placeholder(R.drawable.ic_product_default).error(R.drawable.ic_product_default)
                .skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL).into(productImageView);
        transactionIdTextView.setText(getString(R.string.transaction_id)+" : "+"#"+object.getDealCodeNumber());
        transactionDateTextView.setText(getString(R.string.transaction_date)+" : "+ object.getTransactionDate());
        totalPriceTextView.setText(getString(R.string.total_price)+" : "+getString(R.string.curr_symb)+ NumberFormat.getInstance().format(object.getTotalPrice()));
        if(onClickListener!=null){
            itemView.setTag(object);
            itemView.setOnClickListener(onClickListener);
        }


    }

    @Override
    public void find(String s) {}
}
