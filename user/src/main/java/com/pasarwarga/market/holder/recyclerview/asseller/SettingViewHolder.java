package com.pasarwarga.market.holder.recyclerview.asseller;

import android.view.ViewGroup;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.SimpleViewHolder;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.hendraanggrian.bundler.Bundler;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.fragment.asseller.setting.NoteSellerSettingFragment;
import com.pasarwarga.market.fragment.asseller.setting.SellerSettingListFragment;
import com.pasarwarga.market.fragment.asseller.setting.ShippingSellerSettingFragment;

import butterknife.BindView;

/**
 * Created by Alfarabi on 6/19/17.
 */

public class SettingViewHolder extends SimpleViewHolder<SellerSettingListFragment, String, String> {

    @BindView(R.id.row_1_tv) TextView row1TextView;

    public SettingViewHolder(SellerSettingListFragment fragment, ViewGroup viewGroup) {
        super(fragment, R.layout.viewholder_setting, viewGroup);
    }

    @Override
    public void showData(String object) {
        super.showData(object);
        row1TextView.setText(object);
        itemView.setOnClickListener(v -> {
            if(object.equalsIgnoreCase(getString(R.string.shipping))){
                WindowFlow.goTo(getFragment().activity(HomeActivity.class), ShippingSellerSettingFragment.TAG, Bundler.wrap(ShippingSellerSettingFragment.class, getFragment().getObject()), R.id.fragment_frame, true);
            }else if(object.equalsIgnoreCase(getString(R.string.location))){

            }else if(object.equalsIgnoreCase(getString(R.string.notes))){
                WindowFlow.goTo(getFragment().activity(HomeActivity.class), NoteSellerSettingFragment.TAG,Bundler.wrap(NoteSellerSettingFragment.class, getFragment().getObject()),  R.id.fragment_frame, true);
            }
//            Bundle bundle = new Bundle();
//            bundle.putParcelable(Initial.CATEGORY, Parcels.wrap(object));
//            WindowFlow.goTo(getFragment().activity(HomeActivity.class), ProductListFragment.TAG, bundle, R.id.fragment_frame, true);

        });

    }

    @Override
    public void find(String s) {

    }
}
