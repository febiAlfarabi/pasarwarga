package com.pasarwarga.market.holder.recyclerview.asseller;

import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.SimpleViewHolder;
import com.alfarabi.base.R;
import com.alfarabi.base.model.Shipping;
import com.pasarwarga.market.fragment.asseller.transactiontab.detail.OrderShippingFragment;

import butterknife.BindView;

/**
 * Created by Alfarabi on 9/28/17.
 */

public class ShippingOrderRadioViewHolder extends SimpleViewHolder<OrderShippingFragment, Shipping, String> {

    @BindView(R.id.row_1_tv) TextView row1TextView ;
    @BindView(R.id.radio_button) RadioButton radioButton ;


    public ShippingOrderRadioViewHolder(OrderShippingFragment fragment, ViewGroup viewGroup) {
        super(fragment, R.layout.viewholder_option, viewGroup);
    }

    @Override
    public void showData(Shipping object) {
        super.showData(object);
        row1TextView.setText(object.getShippingName());
//        if(getFragment().getFormProduct().getShippingMethodIds()!=null && !getFragment().getFormProduct().getShippingMethodIds().isEmpty()){
//            if(getFragment().getFormProduct().getShippingMethodIds().contains(object.getId())){
//                radioButton.setChecked(true);
//                getFragment().getHashMap().put(object.getId(), true);
//            }
//        }
        radioButton.setChecked(getFragment().getSelectedIndex()==getAdapterPosition());
        itemView.setOnClickListener(v -> {
            radioButton.setChecked(!radioButton.isChecked());
            getFragment().setSelectedIndex(getAdapterPosition());
//            getFragment().getHashMap().put(object.getId(), radioButton.isChecked());
        });

    }

    @Override
    public void find(String s) {

    }
}
