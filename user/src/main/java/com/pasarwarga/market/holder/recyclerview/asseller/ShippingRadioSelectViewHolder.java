package com.pasarwarga.market.holder.recyclerview.asseller;

import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.SimpleViewHolder;
import com.alfarabi.base.R;
import com.alfarabi.base.model.Shipping;
import com.pasarwarga.market.fragment.asseller.productform.ProductForm3Fragment;

import butterknife.BindView;

/**
 * Created by Alfarabi on 9/28/17.
 */

public class ShippingRadioSelectViewHolder extends SimpleViewHolder<ProductForm3Fragment, Shipping, String> {

    @BindView(R.id.row_1_tv) TextView row1TextView ;
    @BindView(R.id.radio_button) RadioButton radioButton ;


    public ShippingRadioSelectViewHolder(ProductForm3Fragment fragment, ViewGroup viewGroup) {
        super(fragment, R.layout.viewholder_option, viewGroup);
    }

    @Override
    public void showData(Shipping object) {
        super.showData(object);
        row1TextView.setText(object.getShippingName());
        if(getFragment().getHashMap()!=null && getFragment().getHashMap().containsKey(object.getId())){
            radioButton.setChecked(getFragment().getHashMap().get(object.getId()));
//            getFragment().getHashMap().put(object.getId(), true);
        }
        itemView.setOnClickListener(v -> {
            radioButton.setChecked(!radioButton.isChecked());
            getFragment().getHashMap().put(object.getId(), radioButton.isChecked());
            getFragment().getOptionAdapter().notifyItemChanged(getAdapterPosition());
        });
        radioButton.setClickable(false);
//        radioButton.setOnClickListener(v -> {
//            getFragment().getHashMap().put(object.getId(), radioButton.isChecked());
//            getFragment().getOptionAdapter().notifyItemChanged(getAdapterPosition());
//        });

    }

    @Override
    public void find(String s) {

    }
}
