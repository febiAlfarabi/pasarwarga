package com.pasarwarga.market.holder.recyclerview.chat;

import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.alfalibs.tools.WLog;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.fragment.BaseUserFragment;
import com.alfarabi.base.http.basemarket.service.ProductService;
import com.alfarabi.base.model.chat.Conversation;
import com.alfarabi.base.model.chat.ProductAttachment;
import com.alfarabi.base.realm.RealmDao;
import com.alfarabi.base.tools.ResponseTools;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.hendraanggrian.bundler.Bundler;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.fragment.store.product.ProductDetailFragment;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

/**
 * Created by Alfarabi on 10/6/17.
 */

public class ChatViewHolder extends RecyclerView.ViewHolder {

    public static final String TAG = ChatViewHolder.class.getName();

    @BindView(R.id.sticky_layout) LinearLayout stickyLayout ;
    @BindView(R.id.date_tv) TextView datetTextView ;
    @BindView(R.id.chat_container_layout) LinearLayout  chatContainerLayout;
    @BindView(R.id.chat_content_layout) LinearLayout  chatContentLayout;
    @BindView(R.id.chat_time_layout) LinearLayout  chatTimeLayout;
    @BindView(R.id.product_layout) LinearLayout productLayout ;
    @BindView(R.id.line_view) View  lineView ;
    @BindView(R.id.message_layout) LinearLayout  messageLayout;
    @BindView(R.id.item_name_tv) TextView nameTextView;
    @BindView(R.id.item_iv) ImageView imageView;
    @BindView(R.id.message_tv) EmojiconTextView messageTextView ;
    @BindView(R.id.time_tv) TextView timeTextView;


    private BaseUserFragment baseUserFragment ;


    private boolean addHeader ;
    View view ;

    public ChatViewHolder(BaseUserFragment baseUserFragment, ViewGroup parent, boolean addHeader) {
        super(LayoutInflater.from(baseUserFragment.activity(HomeActivity.class)).inflate(R.layout.viewholder_chat_container, parent, false));
        ButterKnife.bind(this, itemView);
        this.baseUserFragment = baseUserFragment ;
        this.addHeader = addHeader ;
    }

    public void showData(Conversation conversation){

        if (conversation.getPartner().isSender()) {
            chatContainerLayout.setGravity(Gravity.LEFT);
            messageLayout.setGravity(Gravity.LEFT);
            chatTimeLayout.setGravity(Gravity.LEFT);
            productLayout.setGravity(Gravity.LEFT);
            messageTextView.setGravity(Gravity.LEFT);
            chatContentLayout.setGravity(Gravity.LEFT);
            chatContentLayout.setBackground(baseUserFragment.getResources().getDrawable(R.drawable.shape_chat_left));
        } else {
            chatContainerLayout.setGravity(Gravity.RIGHT);
            messageLayout.setGravity(Gravity.RIGHT);
            chatTimeLayout.setGravity(Gravity.RIGHT);
            productLayout.setGravity(Gravity.RIGHT);
            messageTextView.setGravity(Gravity.RIGHT);
            chatContentLayout.setGravity(Gravity.RIGHT);
            chatContentLayout.setBackground(baseUserFragment.getResources().getDrawable(R.drawable.shape_chat_right));
        }

        if(addHeader && conversation.getCreated()!=null && !conversation.getCreated().isEmpty()){
            stickyLayout.setVisibility(View.VISIBLE);
            datetTextView.setText(conversation.getCreated().split(" ")[0]);
        }else{
            stickyLayout.setVisibility(View.GONE);
        }
        if(conversation.getProductAttachment()!=null){
            ProductAttachment productAttachment = conversation.getProductAttachment();
            productLayout.setVisibility(View.VISIBLE);
            lineView.setVisibility(View.VISIBLE);
            nameTextView.setText(productAttachment.getName());
            renderImage(productAttachment);
            productLayout.setOnClickListener(v -> {
                gotoProductDetail(productAttachment.getId());
            });
        }else{
            productLayout.setVisibility(View.GONE);
            lineView.setVisibility(View.GONE);
        }

        if(conversation.getMessage()==null || conversation.getMessage().equals("")){
            messageLayout.setVisibility(View.GONE);
            lineView.setVisibility(View.GONE);
        }else{
            messageLayout.setVisibility(View.VISIBLE);
            messageTextView.setText(StringEscapeUtils.unescapeJava(conversation.getMessage()));
        }
        if(conversation.getCreated()!=null){
            timeTextView.setText(conversation.getCreated().split(" ")[1]);
        }
    }


    public void renderImage(ProductAttachment productAttachment){
        GlideApp.with(baseUserFragment).load(StringUtils.split(productAttachment.getImage(), ",")[0])
                .placeholder(R.drawable.ic_product_default)
                .error(R.drawable.ic_product_default)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        e.printStackTrace();
//                        renderImage(productAttachment);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        return false;
                    }
                }).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL).into(imageView);
    }


    void gotoProductDetail(Long productId){
        HttpInstance.withProgress(baseUserFragment.activity(HomeActivity.class)).observe(HttpInstance.create(ProductService.class).getProduct(productId), productResponse -> {
            ResponseTools.validate(baseUserFragment.activity(), productResponse);
            if (productResponse.isStatus() && productResponse.getProduct() != null) {
                RealmDao.instance().insertOrUpdate(productResponse.getProduct());
                WindowFlow.goTo(baseUserFragment.getActivity(), ProductDetailFragment.TAG, Bundler.wrap(ProductDetailFragment.class, productResponse.getProduct()), R.id.fragment_frame, true);
                //getObject() = RealmDao.instance().get(productResponse.getProduct().getId(), Specified.class);
            } else {
                baseUserFragment.activity(HomeActivity.class).showSnackbar(productResponse.getMessage(), v1 -> {});
            }
        }, throwable -> {
            baseUserFragment.activity(HomeActivity.class).showSnackbar(throwable, v1 -> {});
        }, false);
    }

    
//    public static final String TAG = ChatViewHolder.class.getName();
//
//    @BindView(R.id.sticky_layout) LinearLayout stickyLayout ;
//    @BindView(R.id.date_tv) TextView datetTextView ;
//
//    private BaseUserFragment baseUserFragment ;
//
//
//    private boolean addHeader ;
//    View view ;
//
//    public ChatViewHolder(BaseUserFragment baseUserFragment, ViewGroup parent, boolean addHeader) {
//        super(LayoutInflater.from(baseUserFragment.activity(HomeActivity.class)).inflate(R.layout.viewholder_chat_container, parent, false));
//        ButterKnife.bind(this, itemView);
//        this.baseUserFragment = baseUserFragment ;
//        this.addHeader = addHeader ;
//    }
//
//    public void showData(Conversation conversation){
//
//        RelativeLayout containerRelativeLayout = (RelativeLayout) itemView.findViewById(R.id.container_viewholder);
//        if(view==null){
//            if (conversation.getPartner().isSender()) {
//                view = LayoutInflater.from(baseUserFragment.getActivity()).inflate(R.layout.layout_chat_left, null);
//            } else {
//                view = LayoutInflater.from(baseUserFragment.getActivity()).inflate(R.layout.layout_chat_right, null);
//            }
//
//            containerRelativeLayout.addView(view);
//        }else{
//            containerRelativeLayout.removeView(view);
//            if (conversation.getPartner().isSender()) {
//                view = LayoutInflater.from(baseUserFragment.getActivity()).inflate(R.layout.layout_chat_left, null);
//            } else {
//                view = LayoutInflater.from(baseUserFragment.getActivity()).inflate(R.layout.layout_chat_right, null);
//            }
//
//            containerRelativeLayout.addView(view);
//
//        }
//
//        if(addHeader && conversation.getCreated()!=null && !conversation.getCreated().isEmpty()){
//            stickyLayout.setVisibility(View.VISIBLE);
//            datetTextView.setText(conversation.getCreated().split(" ")[0]);
//        }else{
//            stickyLayout.setVisibility(View.GONE);
//        }
//        View productLayout = view.findViewById(R.id.product_layout);
//        View lineView = view.findViewById(R.id.line_view);
//        View messageLayout = view.findViewById(R.id.message_layout);
//        if(conversation.getProductAttachment()!=null){
//            ProductAttachment productAttachment = conversation.getProductAttachment();
//            productLayout.setVisibility(View.VISIBLE);
//            lineView.setVisibility(View.VISIBLE);
//            TextView nameTextView = (TextView) view.findViewById(R.id.item_name_tv);
//            ImageView imageView = (ImageView) view.findViewById(R.id.item_iv);
//            nameTextView.setText(productAttachment.getName());
//            GlideApp.with(baseUserFragment).load(StringUtils.split(productAttachment.getImage(), ",")[0])
//                    .placeholder(R.drawable.ic_product_default)
//                    .error(R.drawable.ic_product_default)
//                    .listener(new RequestListener<Drawable>() {
//                        @Override
//                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
//                            e.printStackTrace();
//                            return false;
//                        }
//
//                        @Override
//                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
//                            return false;
//                        }
//                    }).diskCacheStrategy(DiskCacheStrategy.ALL).into(imageView);
//        }else{
//            productLayout.setVisibility(View.GONE);
//            lineView.setVisibility(View.GONE);
//        }
//
//        EmojiconTextView messageTextView = (EmojiconTextView) view.findViewById(R.id.message_tv);
//        if(conversation.getMessage()==null || conversation.getMessage().equals("")){
//            messageLayout.setVisibility(View.GONE);
//        }else{
//            messageLayout.setVisibility(View.VISIBLE);
//            messageTextView.setText(StringEscapeUtils.unescapeJava(conversation.getMessage()));
//        }
//        if(conversation.getCreated()!=null){
//            TextView timeTextView = (TextView) view.findViewById(R.id.time_tv);
//            timeTextView.setText(conversation.getCreated().split(" ")[1]);
//        }
//    }
}
