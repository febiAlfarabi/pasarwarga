//package com.pasarwarga.market.holder.recyclerview.chat;
//
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.ViewGroup;
//import android.widget.TextView;
//
//import com.alfarabi.alfalibs.tools.GlideApp;
//import com.alfarabi.alfalibs.tools.WindowFlow;
//import com.alfarabi.base.fragment.BaseUserFragment;
//import com.alfarabi.base.model.chat.UserConversation;
//import com.hendraanggrian.bundler.Bundler;
//import com.pasarwarga.market.R;
//import com.pasarwarga.market.activity.home.HomeActivity;
//import com.pasarwarga.market.fragment.chat.ConversationListFragment;
//
//import org.apache.commons.lang.StringEscapeUtils;
//
//import butterknife.BindView;
//import butterknife.ButterKnife;
//import de.hdodenhof.circleimageview.CircleImageView;
//
///**
// * Created by Alfarabi on 10/6/17.
// */
//
//public class UserArchiveChatViewHolder extends RecyclerView.ViewHolder {
//
//
//
//    @BindView(R.id.user_image_iv) CircleImageView userImageView ;
//    @BindView(R.id.date_tv) TextView dateTextView ;
//    @BindView(R.id.username_tv) TextView usernameTextView ;
//    @BindView(R.id.seller_or_user_tv) TextView sellerOrUserTextView ;
//    @BindView(R.id.message_tv) TextView messageTextview ;
//
//    private BaseUserFragment baseUserFragment ;
//
//    public UserArchiveChatViewHolder(BaseUserFragment baseUserFragment, ViewGroup parent) {
//        super(LayoutInflater.from(baseUserFragment.activity()).inflate(R.layout.viewholder_user_conversation, parent, false));
//        ButterKnife.bind(this, itemView);
//        this.baseUserFragment = baseUserFragment ;
//    }
//
//    public void showData(UserConversation userConversation){
//        dateTextView.setText(userConversation.getCreated());
//        if(userConversation.getPartner().isAsSeller()){
//            usernameTextView.setText(userConversation.getPartner().getChatSellerInfo().getName());
//            sellerOrUserTextView.setText(baseUserFragment.getString(R.string.seller));
//            messageTextview.setText(StringEscapeUtils.unescapeJava(userConversation.getMessage()));
//
//            GlideApp.with(baseUserFragment.activity()).load(userConversation.getPartner().getChatSellerInfo().getImage())
//                    .placeholder(R.drawable.thumbnail_profile).error(R.drawable.thumbnail_profile).into(userImageView);
//        }else{
//            usernameTextView.setText(userConversation.getPartner().getChatUser().getFirstname()+" "+userConversation.getPartner().getChatUser().getLastname());
//            sellerOrUserTextView.setText(baseUserFragment.getString(R.string.buyer));
//            messageTextview.setText(StringEscapeUtils.unescapeJava(userConversation.getMessage()));
//
//            GlideApp.with(baseUserFragment.activity()).load(userConversation.getPartner().getChatUser().getImage())
//                    .placeholder(R.drawable.thumbnail_profile).error(R.drawable.thumbnail_profile).into(userImageView);
//        }
//
//        itemView.setOnClickListener(v -> {
////            WindowFlow.goTo(baseUserFragment.activity(HomeActivity.class), ConversationListFragment.TAG, R.id.fragment_frame, true, true);
//            WindowFlow.goTo(baseUserFragment.activity(HomeActivity.class), ConversationListFragment.TAG, Bundler.wrap(ConversationListFragment.class, userConversation), R.id.fragment_frame, true, true);
//
//        });
//    }
//}
