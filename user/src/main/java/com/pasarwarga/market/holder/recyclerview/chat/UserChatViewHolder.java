package com.pasarwarga.market.holder.recyclerview.chat;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.fragment.BaseUserFragment;
import com.alfarabi.base.model.chat.Conversation;
import com.alfarabi.base.model.chat.UserConversation;
import com.alfarabi.base.realm.RealmDao;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.hendraanggrian.bundler.Bundler;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.fragment.chat.ConversationListFragment;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

/**
 * Created by Alfarabi on 10/6/17.
 */

public class UserChatViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.user_image_iv) CircleImageView userImageView ;
    @BindView(R.id.date_tv) TextView dateTextView ;
    @BindView(R.id.username_tv) TextView usernameTextView ;
    @BindView(R.id.seller_or_user_tv) TextView sellerOrUserTextView ;
    @BindView(R.id.message_tv) EmojiconTextView messageTextview ;
    @BindView(R.id.notif_count) TextView notifCount ;
    @BindView(R.id.notif_layout) LinearLayout notifLayout ;


    private BaseUserFragment baseUserFragment ;


    public UserChatViewHolder(BaseUserFragment baseUserFragment, ViewGroup parent) {
        super(LayoutInflater.from(baseUserFragment.activity()).inflate(R.layout.viewholder_user_conversation, parent, false));
        ButterKnife.bind(this, itemView);
        this.baseUserFragment = baseUserFragment ;



    }

    public void showData(UserConversation userConversation){
        dateTextView.setText(userConversation.getCreated());

        usernameTextView.setText(userConversation.getPartner().getChatUser().getFirstname()+" "+userConversation.getPartner().getChatUser().getLastname());
//        sellerOrUserTextView.setText(baseUserFragment.getString(R.string.buyer));
        if(userConversation.getProductAttachment()!=null){
            messageTextview.setText(baseUserFragment.getString(R.string.product_attachment));
        }else{
            if(userConversation.getMessage()!=null && !userConversation.getMessage().equals("")){
                String message = StringEscapeUtils.unescapeJava(userConversation.getMessage());
                if(message.length()>30){
                    message = StringUtils.rightPad(message.substring(0,27),30, ".");
                }
                messageTextview.setText(message);
            }
        }

        GlideApp.with(baseUserFragment).load(userConversation.getPartner().getChatUser().getImage())
                .placeholder(R.drawable.thumbnail_profile).error(R.drawable.thumbnail_profile)
                .skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL).into(userImageView);
        int unReadCount = RealmDao.getRealm()
                .where(Conversation.class).equalTo("hasBeenRead", false).equalTo("partner.id", userConversation.getPartner().getId()).findAll().size();

        if(unReadCount==0){
            notifLayout.setVisibility(View.GONE);
        }else {
            notifLayout.setVisibility(View.VISIBLE);
            notifCount.setText(String.valueOf(unReadCount));
        }

//        if(userConversation.getPartner().isAsSeller()){
//            usernameTextView.setText(userConversation.getPartner().getChatSellerInfo().getName());
//            sellerOrUserTextView.setText(baseUserFragment.getString(R.string.seller));
//            messageTextview.setText(StringEscapeUtils.unescapeJava(userConversation.getMessage()));
//            GlideApp.with(baseUserFragment.activity()).load(userConversation.getPartner().getChatSellerInfo().getImage())
//                    .placeholder(R.drawable.thumbnail_profile).error(R.drawable.thumbnail_profile).into(userImageView);
//            int unReadCount = RealmDao.getRealm()
//                    .where(Conversation.class).equalTo("hasBeenRead", false).equalTo("partner.id", userConversation.getPartner().getId()).findAll().size();
//            if(unReadCount==0){
//                notifLayout.setVisibility(View.GONE);
//            }else{
//                notifLayout.setVisibility(View.VISIBLE);
//                notifCount.setText(String.valueOf(unReadCount));
//            }
//        }else{
//            usernameTextView.setText(userConversation.getPartner().getChatUser().getFirstname()+" "+userConversation.getPartner().getChatUser().getLastname());
//            sellerOrUserTextView.setText(baseUserFragment.getString(R.string.buyer));
//            messageTextview.setText(StringEscapeUtils.unescapeJava(userConversation.getMessage()));
//            GlideApp.with(baseUserFragment.activity()).load(userConversation.getPartner().getChatUser().getImage())
//                    .placeholder(R.drawable.thumbnail_profile).error(R.drawable.thumbnail_profile).into(userImageView);
//            int unReadCount = RealmDao.getRealm()
//                    .where(Conversation.class).equalTo("hasBeenRead", false).equalTo("partner.id", userConversation.getPartner().getId()).findAll().size();
//
//            if(unReadCount==0){
//                notifLayout.setVisibility(View.GONE);
//            }else{
//                notifLayout.setVisibility(View.VISIBLE);
//                notifCount.setText(String.valueOf(unReadCount));
//            }
//        }

        itemView.setOnClickListener(v -> {
            WindowFlow.goTo(baseUserFragment.activity(HomeActivity.class), ConversationListFragment.TAG, Bundler.wrap(ConversationListFragment.class, userConversation), R.id.fragment_frame, true, false);
        });

    }
}
