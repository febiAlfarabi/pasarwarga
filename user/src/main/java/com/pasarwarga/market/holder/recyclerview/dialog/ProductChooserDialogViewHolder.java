package com.pasarwarga.market.holder.recyclerview.dialog;

import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.SimpleViewHolder;
import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.http.basemarket.service.FavoriteService;
import com.alfarabi.base.model.Product;
import com.alfarabi.base.realm.RealmDao;
import com.alfarabi.base.tools.ResponseTools;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.hanks.library.AnimateCheckBox;
import com.hendraanggrian.bundler.Bundler;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.fragment.dialog.ProductChooserDialogFragment;
import com.pasarwarga.market.fragment.store.product.ProductDetailFragment;
import com.pasarwarga.market.fragment.store.product.ProductListFragment;
import com.pasarwarga.market.tools.SystemApp;
import com.tuyenmonkey.mkloader.MKLoader;

import net.soroushjavdan.customfontwidgets.CTextView;

import org.apache.commons.lang.StringUtils;

import java.text.NumberFormat;
import java.util.HashMap;

import butterknife.BindView;

/**
 * Created by Alfarabi on 6/19/17.
 */

public class ProductChooserDialogViewHolder extends SimpleViewHolder<ProductChooserDialogFragment, Product, String> {


    @BindView(R.id.viewholder_layout) LinearLayout viewHolderLayout;
    @BindView(R.id.imageview) ImageView imageView;
    @BindView(R.id.name_tv) TextView nameTextView;
    @BindView(R.id.original_price_tv) TextView originalPriceTextView;
    @BindView(R.id.price_tv) TextView priceTextView;
    @BindView(R.id.discount_tv) CTextView discountTextView;
    @BindView(R.id.loader) MKLoader loader;
    @BindView(R.id.checkbox)AnimateCheckBox checkBox ;
    @BindView(R.id.unpublished_masker)
    RelativeLayout unpublishedMaskerLayout ;


    public ProductChooserDialogViewHolder(ProductChooserDialogFragment fragment, ViewGroup viewGroup) {
        super(fragment, R.layout.viewholder_product_dialog, viewGroup);
    }

    @Override
    public void showData(Product object) {
        super.showData(object);
        nameTextView.setText(object.getName());
        if (object.getDiscount() == 0) {
            originalPriceTextView.setPaintFlags(Paint.LINEAR_TEXT_FLAG);
            originalPriceTextView.setText(getString(R.string.curr_symb) + NumberFormat.getInstance().format(object.getBasePrice()));
            priceTextView.setText(R.string.z);
            discountTextView.setVisibility(View.GONE);
        } else {
            discountTextView.setVisibility(View.VISIBLE);
            originalPriceTextView.setPaintFlags(originalPriceTextView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            originalPriceTextView.setText(getString(R.string.curr_symb) + NumberFormat.getInstance().format(object.getBasePrice()));
            priceTextView.setText(getString(R.string.curr_symb) + NumberFormat.getInstance().format(object.getPriceAfterDiscount()));
            discountTextView.setText(String.valueOf(object.getDiscount()+ getString(R.string.percent_symb))+" "+getString(R.string.off));
        }

        drawingImage();
        checkBox.setClickable(false);
        checkBox.setFocusable(false);
        itemView.setOnClickListener(v -> {
            if(!object.isPublish()){
                Toast.makeText(getFragment().activity(), getString(R.string.publish_before_attach), Toast.LENGTH_SHORT).show();
//                getFragment().activity(HomeActivity.class).showSnackbar(getString(R.string.publish_before_attach), v1 -> {});
                return;
            }
            getFragment().setSelectedProduct(getAdapterPosition());
            int previousSelectedPos = getFragment().getSelectedProduct();
            if(previousSelectedPos!=-1){
                getFragment().getSimpleRecyclerAdapter().notifyItemChanged(previousSelectedPos);
            }
            getFragment().setSelectedProduct(getAdapterPosition());
            getFragment().getSimpleRecyclerAdapter().notifyItemChanged(getAdapterPosition());
            getFragment().getSimpleRecyclerAdapter().notifyDataSetChanged();
        });

        if(object.isPublish()){
            unpublishedMaskerLayout.setVisibility(View.GONE);
        }else{
            unpublishedMaskerLayout.setVisibility(View.VISIBLE);
        }


        if(getFragment().getSelectedProduct()==getAdapterPosition()){
            checkBox.setChecked(true);
        }else{
            checkBox.setChecked(false);
        }



//        itemView.setOnClickListener(v -> {
//            getFragment().getSortFloatingMenuButton().closeMenu();
//            getFragment().getSortFloatingMenuButton().postDelayed(() ->
//                    WindowFlow.goTo(getFragment().getActivity(), ProductDetailFragment.TAG, Bundler.wrap(ProductDetailFragment.class, object), R.id.fragment_frame, true), 200);
//        });
    }

    @Override
    public void find(String s) {
    }

    private void drawingImage() {
        if(getObject().isFavourite()){
//            whislistImageView.setImageDrawable(getFragment().getResources().getDrawable(R.drawable.ic_wishlist_2));
        }else{
//            whislistImageView.setImageDrawable(getFragment().getResources().getDrawable(R.drawable.ic_wishlist_1));
        }
//        whislistImageView.setOnClickListener(v -> {
//            if( SystemApp.checkLogin(getFragment().activity(), true)){
//                loader.setVisibility(View.VISIBLE);
//                HashMap<String, String> hashMap = new HashMap<String, String >();
//                hashMap.put("product_id", String.valueOf(getObject().getId()));
//                HttpInstance.call(HttpInstance.create(FavoriteService.class).changeFavorite(hashMap), changeFavoriteResponse ->{
//                    ResponseTools.validate(getFragment().activity(), changeFavoriteResponse);
//                    loader.setVisibility(View.INVISIBLE);
//                    RealmDao.instance().insertOrUpdate(changeFavoriteResponse.getProduct());
//                    setObject(changeFavoriteResponse.getProduct());
//                    if(getObject().isFavourite()){
//                        whislistImageView.setImageDrawable(getFragment().getResources().getDrawable(R.drawable.ic_wishlist_2));
////                        Caster.activity(getFragment().activity(), HomeActivity.class).increaseFavorite();
//                    }else{
//                        whislistImageView.setImageDrawable(getFragment().getResources().getDrawable(R.drawable.ic_wishlist_1));
////                        Caster.activity(getFragment().activity(), HomeActivity.class).decreaseFavorite();
//                    }
//
//                }, throwable -> {
//                    getFragment().activity(HomeActivity.class).showSnackbar(throwable, v1 -> {});
//                    loader.setVisibility(View.INVISIBLE);
//                },false);
//            }
//        });
        loader.setVisibility(View.VISIBLE);
        imageView.setVisibility(ImageView.INVISIBLE);

        GlideApp.with(getFragment()).load(StringUtils.split(getObject().getImage(), ",")[0])
                .placeholder(R.drawable.ic_product_default)
                .error(R.drawable.ic_product_default)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        e.printStackTrace();
                        loader.setVisibility(View.GONE);
                        imageView.setVisibility(ImageView.VISIBLE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        loader.setVisibility(View.GONE);
                        imageView.setVisibility(ImageView.VISIBLE);
                        return false;
                    }
                }).into(imageView);
    }
}