package com.pasarwarga.market.holder.recyclerview.filter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.SimpleViewHolder;
import com.alfarabi.base.model.location.Category;
import com.pasarwarga.market.R;
import com.pasarwarga.market.fragment.filter.CategoryFilterDialog;

import butterknife.BindView;

/**
 * Created by Alfarabi on 6/19/17.
 */

public class CategoryFilterViewHolder extends SimpleViewHolder<CategoryFilterDialog, Category, String> implements View.OnClickListener{

    @BindView(R.id.row_1_tv) TextView row1TextView;
    @BindView(R.id.checklist_iv) ImageView checklistImageView ;

    public CategoryFilterViewHolder(CategoryFilterDialog fragment, ViewGroup viewGroup) {
        super(fragment, R.layout.viewholder_filter_category, viewGroup);
    }

    @Override
    public void showData(Category object) {
        super.showData(object);
        row1TextView.setText(object.getName());
        if(getFragment().getSelectedCategoryHashMap().containsKey(object.getId()) && getFragment().getSelectedCategoryHashMap().get(object.getId())){
            checklistImageView.setVisibility(View.VISIBLE);
        }else{
            checklistImageView.setVisibility(View.INVISIBLE);
        }

        itemView.setOnClickListener(v -> {
            if(checklistImageView.getVisibility()==View.VISIBLE){
                checklistImageView.setVisibility(View.INVISIBLE);
                getFragment().getSelectedCategoryHashMap().put(object.getId(), false);
            }else{
                checklistImageView.setVisibility(View.VISIBLE);
                getFragment().getSelectedCategoryHashMap().put(object.getId(), true);

            }
        });

    }

    @Override
    public void find(String s) {

    }

    @Override
    public void onClick(View v) {
        if (v == itemView ) {
//            getItem(getAdapterPosition());
//            onSortClickListener.onSortClicked(selectedSort);
//            notifyDataSetChanged();
        }
    }
}
