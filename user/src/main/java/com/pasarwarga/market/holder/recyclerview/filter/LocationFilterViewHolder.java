package com.pasarwarga.market.holder.recyclerview.filter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.SimpleViewHolder;
import com.alfarabi.base.model.location.City;
import com.pasarwarga.market.R;
import com.pasarwarga.market.fragment.filter.LocationFilterDialog;

import butterknife.BindView;

/**
 * Created by Alfarabi on 6/19/17.
 */

public class LocationFilterViewHolder extends SimpleViewHolder<LocationFilterDialog,City, String> implements View.OnClickListener{

    @BindView(R.id.row_1_tv) TextView row1TextView;
    @BindView(R.id.checklist_iv) ImageView checklistImageView ;



    public LocationFilterViewHolder(LocationFilterDialog fragment, ViewGroup viewGroup) {
        super(fragment, R.layout.viewholder_filter_location, viewGroup);

    }

    @Override
    public void showData(City object) {
        super.showData(object);
        row1TextView.setText(object.getName());
        if(getFragment().getSelectedCityHashMap().containsKey(object.getName()) && getFragment().getSelectedCityHashMap().get(object.getName())){
            checklistImageView.setVisibility(View.VISIBLE);
        }else{
            checklistImageView.setVisibility(View.INVISIBLE);
        }
        itemView.setOnClickListener(v -> {
            if(checklistImageView.getVisibility()==View.VISIBLE){
                checklistImageView.setVisibility(View.INVISIBLE);
                getFragment().getSelectedCityHashMap().put(object.getName(), false);
            }else{
                checklistImageView.setVisibility(View.VISIBLE);
                getFragment().getSelectedCityHashMap().put(object.getName(), true);
            }
        });

    }

    @Override
    public void find(String s) {

    }

    @Override
    public void onClick(View v) {
        if (v == itemView ) {
//            getItem(getAdapterPosition());
//            onSortClickListener.onSortClicked(selectedSort);
//            notifyDataSetChanged();
        }
    }
}
