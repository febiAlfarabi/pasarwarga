package com.pasarwarga.market.holder.recyclerview.filter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.SimpleViewHolder;
import com.alfarabi.base.model.Shipping;
import com.pasarwarga.market.R;
import com.pasarwarga.market.fragment.filter.ShippingFilterDialog;

import butterknife.BindView;

/**
 * Created by Alfarabi on 6/19/17.
 */

public class ShippingFilterViewHolder extends SimpleViewHolder<ShippingFilterDialog, Shipping, String> implements View.OnClickListener{

    @BindView(R.id.row_1_tv) TextView row1TextView;
    @BindView(R.id.checklist_iv) ImageView checklistImageView ;

    public ShippingFilterViewHolder(ShippingFilterDialog fragment, ViewGroup viewGroup) {
        super(fragment, R.layout.viewholder_filter_shipping, viewGroup);
    }

    @Override
    public void showData(Shipping object) {
        super.showData(object);
        row1TextView.setText(object.getShippingName());
        if(getFragment().getSelectedShippingHashMap().containsKey(object.getCode()) && getFragment().getSelectedShippingHashMap().get(object.getCode())){
            checklistImageView.setVisibility(View.VISIBLE);
        }else{
            checklistImageView.setVisibility(View.INVISIBLE);
        }

        itemView.setOnClickListener(v -> {
            if(checklistImageView.getVisibility()==View.VISIBLE){
                checklistImageView.setVisibility(View.INVISIBLE);
                getFragment().getSelectedShippingHashMap().put(object.getCode(), false);
            }else{
                checklistImageView.setVisibility(View.VISIBLE);
                getFragment().getSelectedShippingHashMap().put(object.getCode(), true);

            }
        });

    }

    @Override
    public void find(String s) {

    }

    @Override
    public void onClick(View v) {
        if (v == itemView ) {
//            getItem(getAdapterPosition());
//            onSortClickListener.onSortClicked(selectedSort);
//            notifyDataSetChanged();
        }
    }
}
