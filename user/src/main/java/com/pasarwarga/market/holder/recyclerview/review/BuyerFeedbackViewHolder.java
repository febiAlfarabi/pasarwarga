package com.pasarwarga.market.holder.recyclerview.review;

import android.view.ViewGroup;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.SimpleViewHolder;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.model.review.Feedback;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.hendraanggrian.bundler.Bundler;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.fragment.review.BuyerFeedbackDetailFragment;
import com.pasarwarga.market.fragment.review.BuyerFeedbackFragment;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Alfarabi on 6/19/17.
 */

public class BuyerFeedbackViewHolder extends SimpleViewHolder<BuyerFeedbackFragment, Feedback, String> {


    @BindView(R.id.imageview) CircleImageView imageView ;
    @BindView(R.id.name_tv) TextView nameTextView ;
    @BindView(R.id.transaction_id_tv) TextView transactionIdTextView ;
    @BindView(R.id.comment_tv) TextView commentTextView ;
    @BindView(R.id.date_tv) TextView dateTextView ;


    public BuyerFeedbackViewHolder(BuyerFeedbackFragment fragment, ViewGroup viewGroup) {
        super(fragment, R.layout.viewholder_buyer_feedback, viewGroup);

    }

    @Override
    public void find(String s) {

    }

    @Override
    public void showData(Feedback object) {
        super.showData(object);
        GlideApp.with(getFragment()).load(object.getProductImage()).placeholder(R.drawable.ic_pasarwarga).error(R.drawable.ic_pasarwarga)
                .skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL).into(imageView);
        nameTextView.setText(object.getProductName());
        transactionIdTextView.setText(getString(R.string.transaction_id)+" # "+object.getDealCodeNumber());
        commentTextView.setText(object.getDescription());
        dateTextView.setText(object.getCreatedAt());
        itemView.setOnClickListener(v -> {
            WindowFlow.goTo(getFragment().activity(HomeActivity.class), BuyerFeedbackDetailFragment.TAG, Bundler.wrap(BuyerFeedbackDetailFragment.class, object), R.id.fragment_frame, true, false);
        });
    }
}
