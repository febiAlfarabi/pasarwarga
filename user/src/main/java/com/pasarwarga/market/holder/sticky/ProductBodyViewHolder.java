package com.pasarwarga.market.holder.sticky;

import android.support.v7.widget.GridLayoutManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.HorizontalRecyclerAdapter;
import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.SimpleStickyBodyViewHolder;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.alfalibs.views.AlfaRecyclerView;
import com.alfarabi.base.model.Landing;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.fragment.home.HomeFragment;
import com.pasarwarga.market.fragment.store.product.ProductListFragment;
import com.pasarwarga.market.fragment.store.product.ProductPromoListFragment;
import com.pasarwarga.market.holder.recyclerview.HomeProductViewHolder;
import com.pasarwarga.market.holder.recyclerview.HomeShopViewHolder;
import com.pasarwarga.market.holder.recyclerview.MediumBannerViewHolder;

import butterknife.BindView;

/**
 * Created by Alfarabi on 6/22/17.
 */

public class ProductBodyViewHolder<F extends HomeFragment> extends SimpleStickyBodyViewHolder<F, String, String> {

    @BindView(R.id.recyclerview) AlfaRecyclerView recyclerView ;
    @BindView(R.id.title_tv) TextView textView ;
    @BindView(R.id.header_layout) LinearLayout headerLayout ;
    @BindView(R.id.see_all_button) LinearLayout seeAllButton ;
    private F fragment ;

    private Landing landing ;

    public ProductBodyViewHolder(F fragment, ViewGroup viewGroup) {
        super(fragment, R.layout.viewholder_product_stickybody, viewGroup);
        this.fragment = fragment ;
    }

    @Override
    public void showData(String object) {
        super.showData(object);
        textView.setText(object);
        landing = fragment.getLanding();
        headerLayout.setVisibility(View.GONE);
        if(object.equals(fragment.getString(R.string.featured_product)) && landing.getFeaturedProducts().size()>0){
            headerLayout.setVisibility(View.VISIBLE);
            seeAllButton.setVisibility(View.GONE);
            if(landing.getFeaturedProducts().size()==0){
                recyclerView.setVisibility(View.GONE);
            }else{
                recyclerView.setVisibility(View.VISIBLE);
                new HorizontalRecyclerAdapter(fragment, HomeProductViewHolder.class, landing.getFeaturedProducts()).initRecyclerView(recyclerView);
            }
        }else if(object.equals(fragment.getString(R.string.today_deals)) && landing.getTodayDeals().size()>0){
            headerLayout.setVisibility(View.VISIBLE);
            if(landing.getTodayDeals().size()==0){
                recyclerView.setVisibility(View.GONE);
            }else{
                recyclerView.setVisibility(View.VISIBLE);
                seeAllButton.setOnClickListener(v -> {
                    WindowFlow.withAnim(R.anim.enter_from_right, R.anim.exit_to_left).goTo(fragment.activity(HomeActivity.class), ProductPromoListFragment.TAG, R.id.fragment_frame, true, false);
                });
                new HorizontalRecyclerAdapter(fragment, HomeProductViewHolder.class, landing.getTodayDeals()).initRecyclerView(recyclerView);
            }
        }else if(object.equals(fragment.getString(R.string.latest_product)) && landing.getLatestProducts().size()>0){
            headerLayout.setVisibility(View.VISIBLE);
            if(landing.getLatestProducts().size()==0){
                recyclerView.setVisibility(View.GONE);
            }else{
                recyclerView.setVisibility(View.VISIBLE);
                seeAllButton.setOnClickListener(v -> {
                    WindowFlow.withAnim(R.anim.enter_from_right, R.anim.exit_to_left).goTo(fragment.activity(HomeActivity.class), ProductListFragment.TAG, R.id.fragment_frame, true, false);
                });
                new HorizontalRecyclerAdapter(fragment, HomeProductViewHolder.class, landing.getLatestProducts()).initRecyclerView(recyclerView);
            }
        }else if(object.equals(fragment.getString(R.string.featured_shop)) && landing.getFeaturedShops().size()>0){
            headerLayout.setVisibility(View.VISIBLE);
            seeAllButton.setVisibility(View.GONE);
            if(landing.getFeaturedShops().size()==0){
                recyclerView.setVisibility(View.GONE);
            }else{
                recyclerView.setVisibility(View.VISIBLE);
                new HorizontalRecyclerAdapter(fragment, HomeShopViewHolder.class, landing.getFeaturedShops()).initRecyclerView(recyclerView, new GridLayoutManager(fragment.getActivity(), 2, GridLayoutManager.HORIZONTAL, false));
            }
        }else if(object.equals(fragment.getString(R.string.medium_banner)) && landing.getMediumBanners().size()>0){
            if(landing.getMediumBanners().size()==0){
                recyclerView.setVisibility(View.GONE);
            }else{
                recyclerView.setVisibility(View.VISIBLE);
                new HorizontalRecyclerAdapter(fragment, MediumBannerViewHolder.class, landing.getMediumBanners()).initRecyclerView(recyclerView);
            }
        }
    }

    @Override
    public void find(String s) {

    }
}
