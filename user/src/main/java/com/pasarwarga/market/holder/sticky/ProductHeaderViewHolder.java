package com.pasarwarga.market.holder.sticky;

import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alfarabi.alfalibs.adapters.recyclerview.viewholder.SimpleStickyHeaderViewHolder;
import com.pasarwarga.market.R;
import com.pasarwarga.market.fragment.home.HomeFragment;

import butterknife.BindView;

/**
 * Created by Alfarabi on 6/22/17.
 */

public class ProductHeaderViewHolder<F extends HomeFragment> extends SimpleStickyHeaderViewHolder<F, String, String> {

    @BindView(R.id.stickyheader_tv) TextView stickyheaderTv ;
    @BindView(R.id.sticky_layout) LinearLayout stickyLayout ;

    public ProductHeaderViewHolder(F fragment, ViewGroup viewGroup) {
        super(fragment, R.layout.viewholder_product_stickyheader, viewGroup);

    }

    @Override
    public void showData(String object) {
        super.showData(object);
        stickyheaderTv.setText(object);
//        if(object.equals(getFragment().getString(R.string.featured_product))){
//            if(getFragment().getLanding().getFeaturedProducts()==null || getFragment().getLanding().getFeaturedProducts().size()==0){
//                stickyLayout.setVisibility(View.GONE);
//            }else{
//                stickyLayout.setVisibility(View.VISIBLE);
//            }
//        }else if(object.equals(getFragment().getString(R.string.today_deals))){
//            if(getFragment().getLanding().getTodayDeals()==null || getFragment().getLanding().getTodayDeals().size()==0){
//                stickyLayout.setVisibility(View.GONE);
//            }else{
//                stickyLayout.setVisibility(View.VISIBLE);
//            }
//        }else if(object.equals(getFragment().getString(R.string.latest_product))){
//            if(getFragment().getLanding().getLatestProducts()==null || getFragment().getLanding().getLatestProducts().size()==0){
//                stickyLayout.setVisibility(View.GONE);
//            }else{
//                stickyLayout.setVisibility(View.VISIBLE);
//            }
//        }else if(object.equals(getFragment().getString(R.string.featured_shop))){
//            if(getFragment().getLanding().getFeaturedShops()==null || getFragment().getLanding().getFeaturedShops().size()==0){
//                stickyLayout.setVisibility(View.GONE);
//            }else{
//                stickyLayout.setVisibility(View.VISIBLE);
//            }
//        }else if(object.equals(getFragment().getString(R.string.medium_banner))){
//            stickyLayout.setVisibility(View.GONE);
//        }
        // Use less, use below according to mockup
        stickyLayout.setVisibility(View.GONE);

    }

    @Override
    public void find(String s) {

    }
}
