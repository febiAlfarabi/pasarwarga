package com.pasarwarga.market.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.alfarabi.alfalibs.http.HttpInstance;
import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.alfalibs.tools.WLog;
import com.alfarabi.base.model.MyProfile;
import com.alfarabi.base.model.Product;
import com.alfarabi.base.model.chat.Conversation;
import com.alfarabi.base.model.chat.UserConversation;
import com.alfarabi.base.realm.RealmDao;
import com.alfarabi.base.service.BaseFirebaseMessagingService;
import com.alfarabi.base.tools.Initial;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.pasarwarga.market.R;
import com.pasarwarga.market.activity.SplashActivity;
import com.pasarwarga.market.activity.home.HomeActivity;
import com.pasarwarga.market.tools.SystemApp;

import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;

/**
 * Created by Alfarabi on 10/9/17.
 */

public class MyFirebaseMessagingService extends BaseFirebaseMessagingService {

    public static final String TAG = MyFirebaseMessagingService.class.getName();
    NotificationManager mNotifyMgr ;


    private static final int NOTIF_ORDER_ID = 11;

    @Override
    public void onCreate() {
        super.onCreate();
        mNotifyMgr = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        WLog.i(TAG, "onMessageReceived " + HttpInstance.getGson().toJson(remoteMessage.getData()));
        WLog.i(TAG, "onMessageReceived Message Id " + remoteMessage.getMessageId());
        WLog.i(TAG, "onMessageReceived Type " + remoteMessage.getMessageType());
        WLog.i(TAG, "onMessageReceived From " + remoteMessage.getFrom());
        WLog.i(TAG, "onMessageReceived Json " + new Gson().toJson(remoteMessage.getNotification()));
//        WLog.i(TAG, "onMessageReceived Body " + remoteMessage.getNotification()!=null ?remoteMessage.getNotification().getBody():"NULL");
//        WLog.i(TAG, "onMessageReceived Title " + remoteMessage.getNotification()!=null ?remoteMessage.getNotification().getTitle():"NULL");
        Realm realmDao = Realm.getDefaultInstance();

        try {
            final Map<String, String> map = remoteMessage.getData();
            map.putAll(remoteMessage.getData());
            com.alfarabi.base.model.notification.Notification notification = HttpInstance.getGson().fromJson(map.get("data"), com.alfarabi.base.model.notification.Notification.class);
            Intent resultIntent = new Intent(this, HomeActivity.class);
            resultIntent.setAction(Initial.FLAG_CHAT_FRAGMENT);
            resultIntent.putExtra(Initial.FLAG_CHAT_FRAGMENT, 1);
            resultIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);

            if (notification.getType() != null && notification.getType().equals("chat")) {
                if(realmDao.where(MyProfile.class).findFirst()==null){
                    return;
                }
                PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                resultIntent.setAction(Initial.FLAG_CHAT_FRAGMENT);
                Conversation conversation = HttpInstance.getGson().fromJson(notification.getDetail(), Conversation.class);

                realmDao.executeTransaction(realm -> {
                    notification.setId(realm.where(com.alfarabi.base.model.notification.Notification.class).max("id")==null ? 1: realm.where(com.alfarabi.base.model.notification.Notification.class).max("id").intValue()+1);
                    realm.insertOrUpdate(notification);
                    realm.insertOrUpdate(conversation);

                    UserConversation userConversation = null ;
                    if(realm.where(UserConversation.class).equalTo("partner.id", conversation.getPartner().getId()).findFirst()!=null){
                        userConversation = realm.where(UserConversation.class).equalTo("partner.id", conversation.getPartner().getId()).findFirst();
                        userConversation = realm.copyFromRealm(userConversation);
                    }else{
                        userConversation = new UserConversation();
                        userConversation.setId(conversation.getId());
                    }
                    userConversation.setChat(conversation.getChat());
                    userConversation.setMessage(conversation.getMessage());
                    userConversation.setLinkAttachment(conversation.getLinkAttachment());
                    userConversation.setProductAttachment(conversation.getProductAttachment());
                    userConversation.setPartner(conversation.getPartner());
                    userConversation.setCreated(conversation.getCreated());
                    userConversation.setArchive(conversation.isArchive());
                    realm.insertOrUpdate(userConversation);

                    int unReadCount = realm
                            .where(Conversation.class).equalTo("hasBeenRead", false).equalTo("partner.id", userConversation.getPartner().getId()).findAll().size();
                    WLog.i(TAG, "Total message from same person #### "+String.valueOf(unReadCount));
                    NotificationCompat.Builder notificationCompat = new NotificationCompat.Builder(MyFirebaseMessagingService.this).setSmallIcon(com.alfarabi.base.R.drawable.ic_pasarwarga)
                            .setContentTitle(conversation.getPartner().getChatUser().getFirstname()+" "+conversation.getPartner().getChatUser().getLastname())
                            .setContentText(conversation.getMessage())
                            .setSubText(conversation.getCreated())
                            .setNumber(unReadCount)
                            .setContentIntent(resultPendingIntent)
                            .setSound(notifSound).setDefaults(Notification.DEFAULT_VIBRATE | Notification.DEFAULT_SOUND);
                    mNotifyMgr.notify(Integer.valueOf(conversation.getPartner().getId()), notificationCompat.build());
                });





            }else if (notification.getType() != null && notification.getType().equals("order")){

                String productJson = HttpInstance.getGson().toJson(map.get("product"));
                String dealCodeNumber = map.get("deal_code_number");
                Product product = HttpInstance.getGson().fromJson(productJson, Product.class);

                GlideApp.with(this).asBitmap().load(product.getImage()).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL).listener(new RequestListener<Bitmap>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                        PendingIntent resultPendingIntent = PendingIntent.getActivity(MyFirebaseMessagingService.this, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                        NotificationCompat.Builder notificationCompat = new NotificationCompat.Builder(MyFirebaseMessagingService.this)
                                .setSmallIcon(com.alfarabi.base.R.drawable.ic_pasarwarga)
                                .setContentTitle(map.get("body")).setContentText(getString(R.string.receipt_number)+" #"+dealCodeNumber)
                                .setSubText(product.getName())
                                .setContentIntent(resultPendingIntent)
                                .setSound(notifSound).setDefaults(Notification.DEFAULT_VIBRATE | Notification.DEFAULT_SOUND);

                        mNotifyMgr.notify((int)Long.parseLong(dealCodeNumber), notificationCompat.build());
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                        PendingIntent resultPendingIntent = PendingIntent.getActivity(MyFirebaseMessagingService.this, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                        NotificationCompat.Builder notificationCompat = new NotificationCompat.Builder(MyFirebaseMessagingService.this)
                                .setSmallIcon(com.alfarabi.base.R.drawable.ic_pasarwarga)
                                .setLargeIcon(Bitmap.createScaledBitmap(resource, getResources().getInteger(com.alfarabi.base.R.integer.radius_100),
                                        getResources().getInteger(com.alfarabi.base.R.integer.radius_100), false))
                                .setContentTitle(map.get("body")).setContentText(getString(R.string.receipt_number)+" #"+dealCodeNumber)
                                .setSubText(product.getName())
                                .setContentIntent(resultPendingIntent)
                                .setSound(notifSound).setDefaults(Notification.DEFAULT_VIBRATE | Notification.DEFAULT_SOUND);

                        mNotifyMgr.notify((int)Long.parseLong(dealCodeNumber), notificationCompat.build());
                        return false;
                    }
                });
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
