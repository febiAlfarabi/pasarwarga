//package com.pasarwarga.market.tools;
//
//import android.content.Context;
//
//import com.alfarabi.base.R;
//import com.alfarabi.base.model.MyProfile;
//import com.alfarabi.base.realm.RealmDao;
//import com.alfarabi.base.tools.Initial;
//import com.appeaser.sublimenavigationviewlibrary.SublimeBaseMenuItem;
//import com.appeaser.sublimenavigationviewlibrary.SublimeMenu;
//import com.appeaser.sublimenavigationviewlibrary.SublimeNavigationView;
//
//import java.io.Serializable;
//import java.util.HashMap;
//import java.util.HashSet;
//
//import lombok.Getter;
//
///**
// * Created by Alfarabi on 8/18/17.
// */
//
//public class MenuManagement implements Serializable{
//
//    private static final long serialVersionUID = 4490421837290279678L;
//
//    private HashSet<String> authenticatedUserMenus = new HashSet<>();
//    private HashSet<String> authenticatedSellerMenus = new HashSet<>();
//    private HashSet<String> unauthenticatedUserMenus = new HashSet<>();
//    private @Getter HashMap<String, SublimeBaseMenuItem> sublimeMenus = new HashMap<>();
//
//    private static MenuManagement instance;
//
//    public static MenuManagement init(Context context) {
//        if(instance==null){
//            instance = new MenuManagement();
//            instance.initMenu(context);
//        }
//        return instance;
//    }
//
//    public static MenuManagement getInstance() {
//        if(instance==null){
//            throw new RuntimeException("Instance Null It should be init on first time before menu initialize");
//        }
//        return instance;
//    }
//
//    public void initMenu(Context context){
//
//        /*These menus used for loged in User */
//        authenticatedUserMenus.add(context.getString(R.string.home));
//        authenticatedUserMenus.add(context.getString(R.string.register_store));
//        authenticatedUserMenus.add(context.getString(R.string.message));
//        authenticatedUserMenus.add(context.getString(R.string.review));
//        authenticatedUserMenus.add(context.getString(R.string.favorite));
//        authenticatedUserMenus.add(context.getString(R.string.promo));
//        authenticatedUserMenus.add(context.getString(R.string.category));
//        authenticatedUserMenus.add(context.getString(R.string.purchase));
//        authenticatedUserMenus.add(context.getString(R.string.account_setting));
//        authenticatedUserMenus.add(context.getString(R.string.help));
//        authenticatedUserMenus.add(context.getString(R.string.logout));
//        /*==============================================================*/
//
//        /*These menus used for User which registered as Seller also*/
//        authenticatedSellerMenus.add(context.getString(R.string.home));
//
//        authenticatedSellerMenus.add(context.getString(R.string.my_store));
//        authenticatedSellerMenus.add(context.getString(R.string.order_list));
//        authenticatedSellerMenus.add(context.getString(R.string.product_list));
//        authenticatedSellerMenus.add(context.getString(R.string.setting));
//
//        authenticatedSellerMenus.add(context.getString(R.string.message));
//        authenticatedSellerMenus.add(context.getString(R.string.review));
//        authenticatedSellerMenus.add(context.getString(R.string.favorite));
//
////        authenticatedSellerMenus.add(context.getString(R.string.home));
//        authenticatedSellerMenus.add(context.getString(R.string.promo));
//        authenticatedSellerMenus.add(context.getString(R.string.category));
//        authenticatedSellerMenus.add(context.getString(R.string.purchase));
//        authenticatedSellerMenus.add(context.getString(R.string.account_setting));
//        authenticatedSellerMenus.add(context.getString(R.string.help));
//        authenticatedSellerMenus.add(context.getString(R.string.logout));
//        /*==============================================================*/
//
//
//        /*These menus used for User which not loged in yet*/
//        unauthenticatedUserMenus.add(context.getString(R.string.home));
//        unauthenticatedUserMenus.add(context.getString(R.string.login));
//        unauthenticatedUserMenus.add(context.getString(R.string.register));
//        unauthenticatedUserMenus.add(context.getString(R.string.promo));
//        unauthenticatedUserMenus.add(context.getString(R.string.category));
//        unauthenticatedUserMenus.add(context.getString(R.string.help));
//        unauthenticatedUserMenus.add(context.getString(R.string.about_pasarwarga));
//        /*==============================================================*/
//    }
//
//
//
//    public boolean navMenuVisibility(Context context, String menu){
//        MyProfile myProfile = RealmDao.instance().get(MyProfile.class);
//        if(myProfile!=null){
//            if(myProfile.getUser().isSeller() && myProfile.getUser().getSellerStatus()!=null && myProfile.getUser().getSellerStatus().equalsIgnoreCase(Initial.ACTIVE)){
//                if(authenticatedSellerMenus.contains(menu)){
//                    return true ;
//                }
//            }else{
//                if(authenticatedUserMenus.contains(menu)){
//                    return true ;
//                }
//            }
//
//        }else{
//            if(unauthenticatedUserMenus.contains(menu)){
//                return true ;
//            }
//        }
//        return false ;
//    }
//
//    public void navMenuVisibility(Context context, SublimeMenu sublimeMenu){
//        MyProfile myProfile = RealmDao.instance().get(MyProfile.class);
//        if(myProfile!=null){
////            sublimeMenu.getGroup(R.id.transaction_group_item).setVisible(false);
////            sublimeMenu.getGroup(R.id.seller_group_item).setVisible(false);
//            sublimeMenu.getGroup(R.id.my_store_group_item).setVisible(false);
//            sublimeMenu.getGroup(R.id.notification_group_item).setVisible(true);
//            if(myProfile.getUser().isSeller() && myProfile.getUser().getSellerStatus()!=null && myProfile.getUser().getSellerStatus().equalsIgnoreCase(Initial.ACTIVE)){
//                sublimeMenu.getGroup(R.id.my_store_group_item).setVisible(true);
//            }
//        }else{
////            sublimeMenu.getGroup(R.id.transaction_group_item).setVisible(false);
////            sublimeMenu.getGroup(R.id.seller_group_item).setVisible(false);
//            sublimeMenu.getGroup(R.id.my_store_group_item).setVisible(false);
//            sublimeMenu.getGroup(R.id.notification_group_item).setVisible(false);
//        }
//    }
//
//    public void reset(SublimeNavigationView sublimeNavigationView){
//        sublimeNavigationView.switchMenuTo(com.alfarabi.base.R.menu.left_navigation_seller_menu);
////        sublimeNavigationView.getMenu().getGroup(R.id.transaction_group_item).setVisible(true);
////        sublimeNavigationView.getMenu().getGroup(R.id.seller_group_item).setVisible(true);
//        sublimeNavigationView.getMenu().getGroup(R.id.my_store_group_item).setVisible(true);
//        sublimeNavigationView.getMenu().getGroup(R.id.notification_group_item).setVisible(true);
//    }
//
//    public void initVisibleItem(SublimeNavigationView sublimeNavigationView){
//        sublimeMenus.clear();
//        if(sublimeMenus==null || sublimeMenus.size()==0) {
//            for (SublimeBaseMenuItem sublimeMenu : sublimeNavigationView.getMenu().getVisibleItems()) {
//                sublimeMenus.put(sublimeMenu.getTitle().toString(), sublimeMenu);
//            }
//        }
//    }
//
//    public void invisibleAll(SublimeNavigationView sublimeNavigationView){
////        sublimeNavigationView.getMenu().getGroup(R.id.transaction_group_item).setVisible(true);
////        sublimeNavigationView.getMenu().getGroup(R.id.seller_group_item).setVisible(true);
//        if(sublimeMenus!=null && sublimeMenus.size()>0) {
//            sublimeMenus.forEach((s, sublimeBaseMenuItem) -> {
//                sublimeBaseMenuItem.setVisible(false);
//            });
//        }
//    }
//
//
//}
