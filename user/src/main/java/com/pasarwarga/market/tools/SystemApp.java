package com.pasarwarga.market.tools;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;

import com.alfarabi.alfalibs.tools.GlideApp;
import com.alfarabi.alfalibs.tools.WLog;
import com.alfarabi.alfalibs.tools.WindowFlow;
import com.alfarabi.base.model.MyProfile;
import com.alfarabi.base.model.User;
import com.alfarabi.base.model.chat.Conversation;
import com.alfarabi.base.model.chat.UserConversation;
import com.alfarabi.base.model.shop.SellerInfo;
import com.alfarabi.base.realm.RealmDao;
import com.pasarwarga.market.activity.publix.LoginActivity;
import com.pasarwarga.market.activity.publix.VerificationActivity;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.realm.RealmObject;

/**
 * Created by Alfarabi on 8/15/17.
 */

public class SystemApp {
    
    public static final String TAG = SystemApp.class.getName();

    public static <T extends RealmObject> boolean checkLogin(Context context, boolean direct){
        if(RealmDao.instance().get(MyProfile.class)!=null){
            return true ;
        }else{
            if(direct){
                WindowFlow.startActivity(context, LoginActivity.class, false);
            }
            return false ;
        }
    }

    public static <T extends RealmObject> boolean checkVerified(Context context){
        if(RealmDao.instance().get(MyProfile.class)!=null){
            if(RealmDao.instance().get(MyProfile.class).getUser().isPhoneVerified()){
                return true ;
            }else{
                WindowFlow.startActivity(context, VerificationActivity.class, false);
            }
        }
        return false ;
    }

    public static <T extends RealmObject> boolean checkLogin(Context context){
        if(RealmDao.instance().get(MyProfile.class)!=null){
            return true ;
        }else{
            return false ;
        }
    }
    public static MyProfile myProfile(){
        return RealmDao.unmanage(RealmDao.instance().get(MyProfile.class));
    }

    public static void setProfile(MyProfile myProfile){
        deleteProfile();
//
        RealmDao.instance().insertOrUpdate(myProfile);

//        deleteSellerInfo();
    }

    public static void deleteProfile(){
        RealmDao.instance().deleteAll(MyProfile.class);
        RealmDao.instance().deleteAll(UserConversation.class);
        RealmDao.instance().deleteAll(Conversation.class);
    }

    public static void deleteGlide(Activity activity){
        GlideApp.get(activity).clearMemory();
        AsyncTask.execute(() -> {
            GlideApp.get(activity).clearDiskCache();
            WLog.d(TAG, "### FREE UP MEMORY ###");
        });
    }
    public static void deleteUser(){
        RealmDao.instance().deleteAll(User.class);
    }
    public static void deleteSellerInfo(){
        RealmDao.instance().deleteAll(SellerInfo.class);
    }
    public static void deleteChat(){
        RealmDao.instance().deleteAll(UserConversation.class);
        RealmDao.instance().deleteAll(Conversation.class);

    }


}
